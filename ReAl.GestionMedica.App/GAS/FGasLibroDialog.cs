﻿namespace ReAl.GestionMedica.App.GAS
{
    public partial class FGasLibroDialog : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public FGasLibroDialog()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnGasto_Click(object sender, EventArgs e)
        {
            var frm = new FGasLibro("G", true);
            frm.ShowDialog();
            this.DialogResult = frm.DialogResult;
        }

        private void btnIngreso_Click(object sender, EventArgs e)
        {
            var frm = new FGasLibro("I", true);
            frm.ShowDialog();
            this.DialogResult = frm.DialogResult;
        }

        #endregion Methods
    }
}