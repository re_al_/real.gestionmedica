﻿using ReAl.GestionMedica.App.RPT;

namespace ReAl.GestionMedica.App.GAS
{
    public partial class FGasRptTendencia : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public FGasRptTendencia()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var frm = new RptViewerGas("T", dtpFecIni.DateTime, dtpFecFin.DateTime);
            frm.ShowDialog();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fGasRptTendencia_Load(object sender, EventArgs e)
        {
            dtpFecIni.DateTime = DateTime.Now;
            dtpFecFin.DateTime = DateTime.Now;
        }

        #endregion Methods
    }
}