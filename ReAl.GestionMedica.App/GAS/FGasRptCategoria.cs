﻿using System.Data;
using ReAl.GestionMedica.App.RPT;

namespace ReAl.GestionMedica.App.GAS
{
    public partial class FGasRptCategoria : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public FGasRptCategoria()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var frm = new RptViewerGas(cmbTipo.EditValue.ToString(), dtpFecIni.DateTime, dtpFecFin.DateTime);
            frm.ShowDialog();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CargarCmbTipoReporte()
        {
            try
            {
                var dtReportType = new DataTable();
                var dcId = new DataColumn("id", typeof(string));
                dtReportType.Columns.Add(dcId);
                var dcGender = new DataColumn("type", typeof(string));
                dtReportType.Columns.Add(dcGender);

                var dr = dtReportType.NewRow();
                dr["id"] = "I";
                dr["type"] = "Ingreso";
                dtReportType.Rows.Add(dr);
                dr = dtReportType.NewRow();
                dr["id"] = "G";
                dr["type"] = "Gasto";
                dtReportType.Rows.Add(dr);

                cmbTipo.Properties.DataSource = dtReportType;
                if (dtReportType.Rows.Count > 0)
                {
                    cmbTipo.Properties.ValueMember = "id";
                    cmbTipo.Properties.DisplayMember = "type";
                    cmbTipo.Properties.PopulateColumns();
                    cmbTipo.Properties.ShowHeader = false;
                    cmbTipo.Properties.Columns["id"].Visible = false;

                    cmbTipo.ItemIndex = 0;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void fGasRptCategoria_Load(object sender, EventArgs e)
        {
            dtpFecIni.DateTime = DateTime.Now;
            dtpFecFin.DateTime = DateTime.Now;

            CargarCmbTipoReporte();
        }

        #endregion Methods
    }
}