﻿using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;

namespace ReAl.GestionMedica.App.GAS
{
    public partial class FGasCobros : DevExpress.XtraEditors.XtraForm
    {
        #region Fields

        private readonly FinAccounts _myObj = new FinAccounts();

        #endregion Fields

        #region Constructors

        public FGasCobros(FinAccounts obj, string strApellido)
        {
            InitializeComponent();

            _myObj = obj;
            lblPaciente.Text = "Paciente: " + strApellido;
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                if (_myObj != null)
                {
                    var rn = new FinAccountsController();
                    rn.Payment(_myObj);

                    bProcede = true;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }

            if (bProcede)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void fGasCobros_Load(object sender, EventArgs e)
        {
            var rn = new FinAccountsController();
            nudCosto.EditValue = _myObj.Amount;

            nudEfectivo.Focus();
        }

        private void nudEfectivo_EditValueChanged(object sender, EventArgs e)
        {
            var decEfectivo = double.Parse(this.nudEfectivo.EditValue.ToString());
            if (decEfectivo != null)
            {
                var decCosto = double.Parse(this.nudCosto.EditValue.ToString());
                if (decCosto != null)
                {
                    this.nudCambio.EditValue = (decEfectivo - decCosto) * 1.00;
                }
            }
        }

        #endregion Methods
    }
}