


namespace ReAl.GestionMedica.App.GAS
{
	partial class LGasCategoria
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.gcGasCategoria = new DevExpress.XtraGrid.GridControl();
            this.gvGasCategoria = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lbldescripciongca = new System.Windows.Forms.Label();
            this.txtdescripciongca = new DevExpress.XtraEditors.TextEdit();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnAuditoría = new DevExpress.XtraEditors.SimpleButton();
            this.btnEliminar = new DevExpress.XtraEditors.SimpleButton();
            this.btnModificar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcGasCategoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvGasCategoria)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripciongca.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcGasCategoria
            // 
            this.gcGasCategoria.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcGasCategoria.Location = new System.Drawing.Point(0, 165);
            this.gcGasCategoria.MainView = this.gvGasCategoria;
            this.gcGasCategoria.Name = "gcGasCategoria";
            this.gcGasCategoria.Size = new System.Drawing.Size(784, 345);
            this.gcGasCategoria.TabIndex = 5;
            this.gcGasCategoria.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvGasCategoria});
            this.gcGasCategoria.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.gcGasCategoria_MouseDoubleClick);
            // 
            // gvGasCategoria
            // 
            this.gvGasCategoria.GridControl = this.gcGasCategoria;
            this.gvGasCategoria.Name = "gvGasCategoria";
            this.gvGasCategoria.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gvGasCategoria_InitNewRow);
            this.gvGasCategoria.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gvGasCategoria_RowUpdated);
            this.gvGasCategoria.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gcGasCategoria_KeyDown);
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblTitulo.Location = new System.Drawing.Point(12, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(617, 53);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Administracion de Categorias";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbldescripciongca
            // 
            this.lbldescripciongca.AutoSize = true;
            this.lbldescripciongca.Location = new System.Drawing.Point(12, 103);
            this.lbldescripciongca.Name = "lbldescripciongca";
            this.lbldescripciongca.Size = new System.Drawing.Size(65, 13);
            this.lbldescripciongca.TabIndex = 1;
            this.lbldescripciongca.Text = "Descripción:";
            // 
            // txtdescripciongca
            // 
            this.txtdescripciongca.Location = new System.Drawing.Point(83, 100);
            this.txtdescripciongca.Name = "txtdescripciongca";
            this.txtdescripciongca.Properties.MaxLength = 500;
            this.txtdescripciongca.Size = new System.Drawing.Size(204, 20);
            this.txtdescripciongca.TabIndex = 2;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnAceptar.Appearance.Options.UseBackColor = true;
            this.btnAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAceptar.Location = new System.Drawing.Point(126, 125);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(108, 34);
            this.btnAceptar.TabIndex = 4;
            this.btnAceptar.Text = "&Registrar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Location = new System.Drawing.Point(12, 125);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(108, 34);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAuditoría
            // 
            this.btnAuditoría.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAuditoría.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAuditoría.Location = new System.Drawing.Point(436, 516);
            this.btnAuditoría.Name = "btnAuditoría";
            this.btnAuditoría.Size = new System.Drawing.Size(108, 34);
            this.btnAuditoría.TabIndex = 6;
            this.btnAuditoría.Text = "&Auditoría";
            this.btnAuditoría.Click += new System.EventHandler(this.btnAuditoría_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEliminar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Danger;
            this.btnEliminar.Appearance.Options.UseBackColor = true;
            this.btnEliminar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnEliminar.Location = new System.Drawing.Point(550, 516);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(108, 34);
            this.btnEliminar.TabIndex = 7;
            this.btnEliminar.Text = "&Eliminar";
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnModificar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Primary;
            this.btnModificar.Appearance.Options.UseBackColor = true;
            this.btnModificar.Location = new System.Drawing.Point(664, 516);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(108, 34);
            this.btnModificar.TabIndex = 8;
            this.btnModificar.Text = "&Modificar";
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // LGasCategoria
            // 
            this.AcceptButton = this.btnAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnAuditoría);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lbldescripciongca);
            this.Controls.Add(this.txtdescripciongca);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.gcGasCategoria);
            this.Name = "LGasCategoria";
            this.Text = "Administracion de Categorias";
            this.Load += new System.EventHandler(this.lGasCategoria_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcGasCategoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvGasCategoria)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripciongca.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		
		#endregion
		
		private DevExpress.XtraGrid.GridControl gcGasCategoria;
		private DevExpress.XtraGrid.Views.Grid.GridView gvGasCategoria;
        internal System.Windows.Forms.Label lblTitulo;
		internal System.Windows.Forms.Label lbldescripciongca;
		private DevExpress.XtraEditors.TextEdit txtdescripciongca;
		private DevExpress.XtraEditors.SimpleButton btnAceptar;
		private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.SimpleButton btnAuditoría;
        private DevExpress.XtraEditors.SimpleButton btnEliminar;
        private DevExpress.XtraEditors.SimpleButton btnModificar;
	}
}

