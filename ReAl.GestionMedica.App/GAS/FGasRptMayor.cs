﻿using ReAl.GestionMedica.App.RPT;

namespace ReAl.GestionMedica.App.GAS
{
    public partial class FGasRptMayor : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public FGasRptMayor()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var frm = new RptViewerGas("M", dtpFecIni.DateTime, dtpFecFin.DateTime);
            frm.ShowDialog();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fGasRptMayor_Load(object sender, EventArgs e)
        {
            dtpFecIni.DateTime = DateTime.Now;
            dtpFecFin.DateTime = DateTime.Now;
        }

        #endregion Methods
    }
}