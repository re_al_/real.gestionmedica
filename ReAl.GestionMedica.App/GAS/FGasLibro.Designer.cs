


namespace ReAl.GestionMedica.App.GAS
{
	partial class FGasLibro
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FGasLibro));
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblnombregli = new System.Windows.Forms.Label();
            this.txtnombregli = new DevExpress.XtraEditors.TextEdit();
            this.lblidgca = new System.Windows.Forms.Label();
            this.lbldescripciongli = new System.Windows.Forms.Label();
            this.lblfechagli = new System.Windows.Forms.Label();
            this.dtpfechagli = new DevExpress.XtraEditors.DateEdit();
            this.lblmonto = new System.Windows.Forms.Label();
            this.nudmonto = new DevExpress.XtraEditors.TextEdit();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.txtdescripciongli = new DevExpress.XtraEditors.MemoEdit();
            this.btnNuevo = new DevExpress.XtraEditors.SimpleButton();
            this.cmbidgca = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnombregli.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpfechagli.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpfechagli.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudmonto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripciongli.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbidgca.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblTitulo.Location = new System.Drawing.Point(12, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(197, 53);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Transacción";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblnombregli
            // 
            this.lblnombregli.AutoSize = true;
            this.lblnombregli.Location = new System.Drawing.Point(14, 68);
            this.lblnombregli.Name = "lblnombregli";
            this.lblnombregli.Size = new System.Drawing.Size(48, 13);
            this.lblnombregli.TabIndex = 1;
            this.lblnombregli.Text = "Nombre:";
            // 
            // txtnombregli
            // 
            this.txtnombregli.Location = new System.Drawing.Point(85, 65);
            this.txtnombregli.Name = "txtnombregli";
            this.txtnombregli.Properties.MaxLength = 150;
            this.txtnombregli.Size = new System.Drawing.Size(267, 20);
            this.txtnombregli.TabIndex = 2;
            // 
            // lblidgca
            // 
            this.lblidgca.AutoSize = true;
            this.lblidgca.Location = new System.Drawing.Point(14, 93);
            this.lblidgca.Name = "lblidgca";
            this.lblidgca.Size = new System.Drawing.Size(58, 13);
            this.lblidgca.TabIndex = 3;
            this.lblidgca.Text = "Categoria:";
            // 
            // lbldescripciongli
            // 
            this.lbldescripciongli.AutoSize = true;
            this.lbldescripciongli.Location = new System.Drawing.Point(14, 152);
            this.lbldescripciongli.Name = "lbldescripciongli";
            this.lbldescripciongli.Size = new System.Drawing.Size(65, 13);
            this.lbldescripciongli.TabIndex = 5;
            this.lbldescripciongli.Text = "Descripción:";
            // 
            // lblfechagli
            // 
            this.lblfechagli.AutoSize = true;
            this.lblfechagli.Location = new System.Drawing.Point(14, 216);
            this.lblfechagli.Name = "lblfechagli";
            this.lblfechagli.Size = new System.Drawing.Size(40, 13);
            this.lblfechagli.TabIndex = 7;
            this.lblfechagli.Text = "Fecha:";
            // 
            // dtpfechagli
            // 
            this.dtpfechagli.EditValue = new System.DateTime(2013, 6, 29, 0, 0, 0, 0);
            this.dtpfechagli.Location = new System.Drawing.Point(85, 213);
            this.dtpfechagli.Name = "dtpfechagli";
            this.dtpfechagli.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpfechagli.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpfechagli.Size = new System.Drawing.Size(267, 20);
            this.dtpfechagli.TabIndex = 8;
            // 
            // lblmonto
            // 
            this.lblmonto.AutoSize = true;
            this.lblmonto.Location = new System.Drawing.Point(14, 251);
            this.lblmonto.Name = "lblmonto";
            this.lblmonto.Size = new System.Drawing.Size(49, 13);
            this.lblmonto.TabIndex = 9;
            this.lblmonto.Text = "Importe:";
            // 
            // nudmonto
            // 
            this.nudmonto.EditValue = "0.00";
            this.nudmonto.Location = new System.Drawing.Point(85, 238);
            this.nudmonto.Name = "nudmonto";
            this.nudmonto.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.nudmonto.Properties.Appearance.Options.UseFont = true;
            this.nudmonto.Properties.Mask.EditMask = "n2";
            this.nudmonto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.nudmonto.Properties.NullText = "0.00";
            this.nudmonto.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.nudmonto.Size = new System.Drawing.Size(267, 32);
            this.nudmonto.TabIndex = 10;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnAceptar.Appearance.Options.UseBackColor = true;
            this.btnAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAceptar.Location = new System.Drawing.Point(244, 276);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(108, 34);
            this.btnAceptar.TabIndex = 13;
            this.btnAceptar.Text = "&Registrar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Location = new System.Drawing.Point(16, 276);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(108, 34);
            this.btnCancelar.TabIndex = 11;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // txtdescripciongli
            // 
            this.txtdescripciongli.Location = new System.Drawing.Point(85, 115);
            this.txtdescripciongli.Name = "txtdescripciongli";
            this.txtdescripciongli.Size = new System.Drawing.Size(267, 92);
            this.txtdescripciongli.TabIndex = 6;
            // 
            // btnNuevo
            // 
            this.btnNuevo.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnNuevo.Appearance.Options.UseBackColor = true;
            this.btnNuevo.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnNuevo.Location = new System.Drawing.Point(130, 276);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(108, 34);
            this.btnNuevo.TabIndex = 12;
            this.btnNuevo.Text = "Registrar y &Nuevo";
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // cmbidgca
            // 
            this.cmbidgca.Location = new System.Drawing.Point(85, 90);
            this.cmbidgca.Name = "cmbidgca";
            this.cmbidgca.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbidgca.Size = new System.Drawing.Size(267, 20);
            this.cmbidgca.TabIndex = 4;
            // 
            // FGasLibro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(364, 321);
            this.Controls.Add(this.cmbidgca);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.txtdescripciongli);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lblnombregli);
            this.Controls.Add(this.txtnombregli);
            this.Controls.Add(this.lblidgca);
            this.Controls.Add(this.lbldescripciongli);
            this.Controls.Add(this.lblfechagli);
            this.Controls.Add(this.dtpfechagli);
            this.Controls.Add(this.lblmonto);
            this.Controls.Add(this.nudmonto);
            this.Controls.Add(this.lblTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("FGasLibro.IconOptions.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FGasLibro";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Transacción";
            this.Load += new System.EventHandler(this.fGasLibro_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtnombregli.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpfechagli.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpfechagli.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudmonto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripciongli.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbidgca.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		
		#endregion

        internal System.Windows.Forms.Label lblTitulo;
		internal System.Windows.Forms.Label lblnombregli;
		private DevExpress.XtraEditors.TextEdit txtnombregli;
        internal System.Windows.Forms.Label lblidgca;
        internal System.Windows.Forms.Label lbldescripciongli;
		internal System.Windows.Forms.Label lblfechagli;
        private DevExpress.XtraEditors.DateEdit dtpfechagli;
		internal System.Windows.Forms.Label lblmonto;
        private DevExpress.XtraEditors.TextEdit nudmonto;
		private DevExpress.XtraEditors.SimpleButton btnAceptar;
		private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.MemoEdit txtdescripciongli;
        private DevExpress.XtraEditors.SimpleButton btnNuevo;
        private DevExpress.XtraEditors.LookUpEdit cmbidgca;
	}
}

