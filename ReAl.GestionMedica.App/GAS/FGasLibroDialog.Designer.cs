﻿


namespace ReAl.GestionMedica.App.GAS
{
    partial class FGasLibroDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FGasLibroDialog));
            this.btnIngreso = new DevExpress.XtraEditors.SimpleButton();
            this.btnGasto = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // btnIngreso
            // 
            this.btnIngreso.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnIngreso.Appearance.Options.UseFont = true;
            this.btnIngreso.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.plus_icon_peq;
            this.btnIngreso.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnIngreso.Location = new System.Drawing.Point(12, 12);
            this.btnIngreso.Name = "btnIngreso";
            this.btnIngreso.Size = new System.Drawing.Size(110, 100);
            this.btnIngreso.TabIndex = 0;
            this.btnIngreso.Text = "Ingreso";
            this.btnIngreso.Click += new System.EventHandler(this.btnIngreso_Click);
            // 
            // btnGasto
            // 
            this.btnGasto.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnGasto.Appearance.Options.UseFont = true;
            this.btnGasto.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.minus_icon_peq;
            this.btnGasto.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnGasto.Location = new System.Drawing.Point(128, 12);
            this.btnGasto.Name = "btnGasto";
            this.btnGasto.Size = new System.Drawing.Size(110, 100);
            this.btnGasto.TabIndex = 1;
            this.btnGasto.Text = "Gasto";
            this.btnGasto.Click += new System.EventHandler(this.btnGasto_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Appearance.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btnCancelar.Appearance.Options.UseFont = true;
            this.btnCancelar.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.cross_icon_peq;
            this.btnCancelar.ImageOptions.Location = DevExpress.XtraEditors.ImageLocation.TopCenter;
            this.btnCancelar.Location = new System.Drawing.Point(244, 12);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(110, 100);
            this.btnCancelar.TabIndex = 2;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // FGasLibroDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 130);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnGasto);
            this.Controls.Add(this.btnIngreso);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("FGasLibroDialog.IconOptions.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FGasLibroDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Elegir Tipo de Transacción";
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnIngreso;
        private DevExpress.XtraEditors.SimpleButton btnGasto;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
    }
}