﻿


namespace ReAl.GestionMedica.App.GAS
{
    partial class FGasCobros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FGasCobros));
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.nudEfectivo = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.nudCambio = new DevExpress.XtraEditors.TextEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.nudCosto = new DevExpress.XtraEditors.TextEdit();
            this.lblPaciente = new System.Windows.Forms.Label();
            this.lblaltocpl = new System.Windows.Forms.Label();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudEfectivo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCambio.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCosto.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.nudEfectivo);
            this.groupControl1.Controls.Add(this.label2);
            this.groupControl1.Controls.Add(this.nudCambio);
            this.groupControl1.Controls.Add(this.label1);
            this.groupControl1.Controls.Add(this.nudCosto);
            this.groupControl1.Controls.Add(this.lblPaciente);
            this.groupControl1.Controls.Add(this.lblaltocpl);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(522, 231);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Cobro";
            // 
            // nudEfectivo
            // 
            this.nudEfectivo.EditValue = "0.00";
            this.nudEfectivo.Location = new System.Drawing.Point(224, 123);
            this.nudEfectivo.Name = "nudEfectivo";
            this.nudEfectivo.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.nudEfectivo.Properties.Appearance.Options.UseFont = true;
            this.nudEfectivo.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.nudEfectivo.Properties.Mask.EditMask = "n2";
            this.nudEfectivo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.nudEfectivo.Properties.NullText = "0";
            this.nudEfectivo.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.nudEfectivo.Size = new System.Drawing.Size(170, 32);
            this.nudEfectivo.TabIndex = 4;
            this.nudEfectivo.EditValueChanged += new System.EventHandler(this.nudEfectivo_EditValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 16F);
            this.label2.Location = new System.Drawing.Point(128, 126);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 27);
            this.label2.TabIndex = 3;
            this.label2.Text = "Efectivo:";
            // 
            // nudCambio
            // 
            this.nudCambio.EditValue = "0.00";
            this.nudCambio.Location = new System.Drawing.Point(224, 182);
            this.nudCambio.Name = "nudCambio";
            this.nudCambio.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.nudCambio.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.nudCambio.Properties.Appearance.Options.UseBackColor = true;
            this.nudCambio.Properties.Appearance.Options.UseFont = true;
            this.nudCambio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.nudCambio.Properties.Mask.EditMask = "n2";
            this.nudCambio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.nudCambio.Properties.NullText = "0";
            this.nudCambio.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.nudCambio.Size = new System.Drawing.Size(170, 32);
            this.nudCambio.TabIndex = 6;
            this.nudCambio.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 16F);
            this.label1.Location = new System.Drawing.Point(128, 185);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 27);
            this.label1.TabIndex = 5;
            this.label1.Text = "Cambio:";
            // 
            // nudCosto
            // 
            this.nudCosto.EditValue = "0.00";
            this.nudCosto.Location = new System.Drawing.Point(224, 85);
            this.nudCosto.Name = "nudCosto";
            this.nudCosto.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.nudCosto.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.nudCosto.Properties.Appearance.Options.UseBackColor = true;
            this.nudCosto.Properties.Appearance.Options.UseFont = true;
            this.nudCosto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.nudCosto.Properties.Mask.EditMask = "n2";
            this.nudCosto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.nudCosto.Properties.NullText = "0";
            this.nudCosto.Properties.ReadOnly = true;
            this.nudCosto.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.nudCosto.Size = new System.Drawing.Size(170, 32);
            this.nudCosto.TabIndex = 2;
            this.nudCosto.TabStop = false;
            // 
            // lblPaciente
            // 
            this.lblPaciente.Font = new System.Drawing.Font("Tahoma", 18F);
            this.lblPaciente.Location = new System.Drawing.Point(5, 45);
            this.lblPaciente.Name = "lblPaciente";
            this.lblPaciente.Size = new System.Drawing.Size(512, 27);
            this.lblPaciente.TabIndex = 0;
            this.lblPaciente.Text = "Paciente: Reynaldo Alonzo Vera Arias";
            this.lblPaciente.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblaltocpl
            // 
            this.lblaltocpl.AutoSize = true;
            this.lblaltocpl.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblaltocpl.Location = new System.Drawing.Point(128, 88);
            this.lblaltocpl.Name = "lblaltocpl";
            this.lblaltocpl.Size = new System.Drawing.Size(74, 27);
            this.lblaltocpl.TabIndex = 1;
            this.lblaltocpl.Text = "Costo:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnCancelar.Location = new System.Drawing.Point(312, 249);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(108, 34);
            this.btnCancelar.TabIndex = 1;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnAceptar.Appearance.Options.UseBackColor = true;
            this.btnAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAceptar.Location = new System.Drawing.Point(426, 249);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(108, 34);
            this.btnAceptar.TabIndex = 2;
            this.btnAceptar.Text = "&Aceptar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // FGasCobros
            // 
            this.AcceptButton = this.btnAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(546, 295);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.groupControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("FGasCobros.IconOptions.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FGasCobros";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cobro";
            this.Load += new System.EventHandler(this.fGasCobros_Load);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudEfectivo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCambio.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudCosto.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.TextEdit nudEfectivo;
        internal System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit nudCambio;
        internal System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit nudCosto;
        internal System.Windows.Forms.Label lblaltocpl;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        internal System.Windows.Forms.Label lblPaciente;
    }
}