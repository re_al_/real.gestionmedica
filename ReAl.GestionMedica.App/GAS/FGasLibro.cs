using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;

namespace ReAl.GestionMedica.App.GAS
{
    public partial class FGasLibro : DevExpress.XtraEditors.XtraForm
    {
        #region Fields

        private readonly FinAccounts _myObj = null;
        private readonly string _strTipoTransaccion = "G";

        #endregion Fields

        #region Constructors

        public FGasLibro(string strTipo, bool bMostrarNuevo)
        {
            InitializeComponent();

            _strTipoTransaccion = strTipo;
            btnNuevo.Enabled = bMostrarNuevo;
        }

        public FGasLibro(FinAccounts obj)
        {
            InitializeComponent();
            _myObj = obj;
            _strTipoTransaccion = _myObj.Type;
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var bProcede = false;
            var rn = new FinAccountsController();
            try
            {
                if (_myObj == null)
                {
                    //Insertamos el registro
                    var obj = new FinAccounts();
                    obj.Name = txtnombregli.Text;
                    obj.IdCategories = int.Parse(cmbidgca.EditValue.ToString());
                    obj.Description = txtdescripciongli.Text;
                    obj.AccountDate = dtpfechagli.DateTime;
                    obj.Type = _strTipoTransaccion;
                    obj.Amount = decimal.Parse(nudmonto.EditValue.ToString() ?? "0");
                    rn.Create(obj);
                    bProcede = true;
                }
                else
                {
                    _myObj.Name = txtnombregli.Text;
                    _myObj.IdCategories = int.Parse(cmbidgca.EditValue.ToString());
                    _myObj.Description = txtdescripciongli.Text;
                    _myObj.AccountDate = dtpfechagli.DateTime;
                    _myObj.Type = _strTipoTransaccion;
                    _myObj.Amount = Decimal.Parse(nudmonto.EditValue.ToString());
                    _myObj.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                    rn.Update(_myObj);
                    bProcede = true;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
            if (bProcede)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var bProcede = false;
            var rn = new FinAccountsController();
            try
            {
                if (_myObj == null)
                {
                    //Insertamos el registro
                    var obj = new FinAccounts();
                    obj.Name = txtnombregli.Text;
                    obj.IdCategories = int.Parse(cmbidgca.EditValue.ToString());
                    obj.Description = txtdescripciongli.Text;
                    obj.AccountDate = dtpfechagli.DateTime;
                    obj.Type = _strTipoTransaccion;
                    obj.Amount = Decimal.Parse(nudmonto.EditValue.ToString());
                    rn.Create(obj);
                    bProcede = true;
                }
                else
                {
                    _myObj.Name = txtnombregli.Text;
                    _myObj.IdCategories = int.Parse(cmbidgca.EditValue.ToString());
                    _myObj.Description = txtdescripciongli.Text;
                    _myObj.AccountDate = dtpfechagli.DateTime;
                    _myObj.Type = _strTipoTransaccion;
                    _myObj.Amount = Decimal.Parse(nudmonto.EditValue.ToString());
                    _myObj.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                    rn.Update(_myObj);
                    bProcede = true;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
            if (bProcede)
            {
                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
        }

        private void CargarCmbidgca()
        {
            try
            {
                var rn = new FinCategoriesController();
                var dt = rn.GetActive();

                this.cmbidgca.Properties.DataSource = dt;

                if (dt.Count > 0)
                {
                    this.cmbidgca.Properties.ValueMember = FinCategories.Fields.IdCategories.ToString();
                    this.cmbidgca.Properties.DisplayMember = FinCategories.Fields.Description.ToString();
                    this.cmbidgca.Properties.PopulateColumns();
                    this.cmbidgca.Properties.ShowHeader = false;
                    this.cmbidgca.Properties.Columns[FinCategories.Fields.IdCategories.ToString()].Visible = false;
                    this.cmbidgca.Properties.ForceInitialize();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void CargarDatos()
        {
            try
            {
                if (_myObj != null)
                {
                    txtnombregli.Text = _myObj.Name;
                    cmbidgca.EditValue = _myObj.IdCategories;
                    txtdescripciongli.Text = _myObj.Description;
                    dtpfechagli.EditValue = _myObj.AccountDate;
                    nudmonto.EditValue = _myObj.Amount;

                    btnNuevo.Enabled = false;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void fGasLibro_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            dtpfechagli.DateTime = DateTime.Now;

            CargarCmbidgca();

            switch (_strTipoTransaccion)
            {
                case "I":
                    lblTitulo.Text = "Ingreso";
                    break;

                case "G":
                    lblTitulo.Text = "Gasto";
                    break;
            }

            if (_myObj != null) CargarDatos();
        }

        #endregion Methods
    }
}