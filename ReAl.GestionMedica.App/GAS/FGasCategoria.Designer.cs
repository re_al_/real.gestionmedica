


namespace ReAl.GestionMedica.App.GAS
{
	partial class FGasCategoria
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FGasCategoria));
            this.lbldescripciongca = new System.Windows.Forms.Label();
            this.txtdescripciongca = new DevExpress.XtraEditors.TextEdit();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripciongca.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbldescripciongca
            // 
            this.lbldescripciongca.AutoSize = true;
            this.lbldescripciongca.Location = new System.Drawing.Point(6, 28);
            this.lbldescripciongca.Name = "lbldescripciongca";
            this.lbldescripciongca.Size = new System.Drawing.Size(65, 13);
            this.lbldescripciongca.TabIndex = 0;
            this.lbldescripciongca.Text = "Descripción:";
            // 
            // txtdescripciongca
            // 
            this.txtdescripciongca.Location = new System.Drawing.Point(77, 25);
            this.txtdescripciongca.Name = "txtdescripciongca";
            this.txtdescripciongca.Properties.MaxLength = 500;
            this.txtdescripciongca.Size = new System.Drawing.Size(219, 20);
            this.txtdescripciongca.TabIndex = 1;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnAceptar.Appearance.Options.UseBackColor = true;
            this.btnAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAceptar.Location = new System.Drawing.Point(208, 74);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(108, 34);
            this.btnAceptar.TabIndex = 2;
            this.btnAceptar.Text = "&Registrar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Location = new System.Drawing.Point(94, 74);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(108, 34);
            this.btnCancelar.TabIndex = 1;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.txtdescripciongca);
            this.groupControl1.Controls.Add(this.lbldescripciongca);
            this.groupControl1.Location = new System.Drawing.Point(15, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(301, 56);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Registro de Categoría";
            // 
            // FGasCategoria
            // 
            this.AcceptButton = this.btnAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(328, 120);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.btnCancelar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("FGasCategoria.IconOptions.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FGasCategoria";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Registro de Nueva Categoría";
            this.Load += new System.EventHandler(this.fGasCategoria_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripciongca.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);

		}
		
		#endregion

        internal System.Windows.Forms.Label lbldescripciongca;
		private DevExpress.XtraEditors.TextEdit txtdescripciongca;
		private DevExpress.XtraEditors.SimpleButton btnAceptar;
		private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.GroupControl groupControl1;
	}
}

