using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
using ReAl.GestionMedica.Class;

namespace ReAl.GestionMedica.App.GAS
{
    public partial class LGasCategoria : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public LGasCategoria()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        public void CargarListado()
        {
            try
            {
                var rn = new FinCategoriesController();
                var dt = rn.GetActive();
                gcGasCategoria.DataSource = dt;

                if (dt.Count > 0)
                {
                    this.gvGasCategoria.PopulateColumns();
                    this.gvGasCategoria.OptionsBehavior.Editable = false;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void EliminarRegistro()
        {
            try
            {
                if (gvGasCategoria.RowCount <= 0) return;
                if (MessageBox.Show("¿Desea ELIMINAR el registro seleccionado?", "Eliminar registro",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) ==
                    DialogResult.Yes)
                {
                    //Obtenemos el objeto
                    var rn = new FinCategoriesController();
                    int intidgca = int.Parse(gvGasCategoria.GetFocusedRowCellValue(FinCategories.Fields.IdCategories.ToString()).ToString());
                    var obj = rn.GetById(intidgca);

                    if (obj != null)
                    {
                        //Eliminamos el registro seleccionado
                        rn.Delete(obj.IdCategories);
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void ModificarRegistro()
        {
            try
            {
                if (gvGasCategoria.RowCount <= 0) return;
                
                //Obtenemos el objeto
                var rn = new FinCategoriesController();
                int intidgca = int.Parse(gvGasCategoria.GetFocusedRowCellValue(FinCategories.Fields.IdCategories.ToString()).ToString());
                var obj = rn.GetById(intidgca);

                if (obj != null)
                {
                    //Abrimos la AUDITORIA
                    var frm = new FGasCategoria(obj);
                    frm.ShowDialog();

                    if (frm.DialogResult == DialogResult.OK)
                        this.CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            //Insertamos el registro
            var rn = new FinCategoriesController();
            var obj = new FinCategories();
            obj.Description = txtdescripciongca.Text;
            rn.Create(obj);
            this.CargarListado();
            this.DialogResult = DialogResult.OK;
            //this.Close();
        }

        private void btnAuditoría_Click(object sender, EventArgs e)
        {
            try
            {
                if (gvGasCategoria.RowCount <= 0) return;
                var rn = new FinCategoriesController();
                int intidgca = int.Parse(gvGasCategoria.GetFocusedRowCellValue(FinCategories.Fields.IdCategories.ToString()).ToString());

                //Obtenemos el objeto
                var obj = rn.GetById(intidgca);

                if (obj != null)
                {
                    //Abrimos la AUDITORIA
                    var frm = new FAuditaria(obj.ApiUsucre, obj.ApiFeccre, obj.ApiUsumod, obj.ApiFecmod, obj.ApiStatus);
                    frm.ShowDialog();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarRegistro();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            ModificarRegistro();
        }

        private void gcGasCategoria_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                EliminarRegistro();
            }
        }

        private void gcGasCategoria_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ModificarRegistro();
        }

        private void gvGasCategoria_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            //gvGasCategoria.SetFocusedRowCellValue(EntGasCategoria.Fields.usucre.ToString(),"postgres");
        }

        private void gvGasCategoria_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            //Eliminamos el registro seleccionado
            var rn = new FinCategoriesController();

            //Apropiamos los valores del Grid
            int intidgca = int.Parse(gvGasCategoria.GetFocusedRowCellValue(FinCategories.Fields.IdCategories.ToString()).ToString());

            var objExistente = rn.GetById(intidgca);

            if (objExistente != null)
            {
                //Actualizamos el registro
                objExistente.Description = gvGasCategoria.GetFocusedRowCellValue(FinCategories.Fields.Description.ToString()).ToString();
                objExistente.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                rn.Update(objExistente);
                this.CargarListado();
            }
        }

        private void lGasCategoria_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            CargarListado();
        }

        #endregion Methods
    }
}