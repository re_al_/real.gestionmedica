using DevExpress.Data;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;

namespace ReAl.GestionMedica.App.GAS
{
    public partial class LGasCobros : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public LGasCobros()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        public void CargarListado()
        {
            try
            {
                var rn = new FinAccountsController();
                var dt = rn.GetCobros();
                gcGasLibro.DataSource = dt;
                if (dt != null && dt.Count > 0)
                {
                    this.gvGasLibro.PopulateColumns();
                    this.gvGasLibro.OptionsBehavior.Editable = false;
                    
                    //gvGasLibro.Columns["Fecha"].GroupIndex = 1;
                    gvGasLibro.OptionsView.ShowFooter = true;
                    gvGasLibro.Columns[FinAccounts.Fields.TimeStart.ToString()].Visible = false;
                    gvGasLibro.Columns[FinAccounts.Fields.Name.ToString()].Visible = false;
                    gvGasLibro.Columns[FinAccounts.Fields.Description.ToString()].Visible = false;
                    gvGasLibro.Columns[FinAccounts.Fields.Type.ToString()].Visible = false;
                    gvGasLibro.Columns[FinAccounts.Fields.Amount.ToString()].SummaryItem.SummaryType = SummaryItemType.Sum;
                    gvGasLibro.Columns[FinAccounts.Fields.Amount.ToString()].SummaryItem.FieldName = FinAccounts.Fields.Amount.ToString();
                    gvGasLibro.Columns[FinAccounts.Fields.Amount.ToString()].SummaryItem.DisplayFormat = "Total: {0:n2}";

                    //gvGasLibro.ExpandAllGroups();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void ModificarRegistro()
        {
            try
            {
                if (gvGasLibro.RowCount <= 0) return;
                var rn = new FinAccountsController();

                //Apropiamos los valores del Grid
                Int64 intidgli = int.Parse(gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.IdAccounts.ToString()).ToString());
                var strApellido = gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.LastName.ToString()).ToString();

                //Obtenemos el objeto
                var obj = rn.GetById(intidgli);

                if (obj != null)
                {
                    var frm = new FGasCobros(obj, strApellido);
                    frm.ShowDialog();
                    if (frm.DialogResult == DialogResult.OK)
                    {
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            this.CargarListado();
        }

        private void btnAuditoría_Click(object sender, EventArgs e)
        {
            if (gvGasLibro.RowCount <= 0) return;
            var rn = new FinAccountsController();

            //Apropiamos los valores del Grid
            Int64 intidgli = int.Parse(gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.IdAccounts.ToString()).ToString());

            //Obtenemos el objeto
            var obj = rn.GetById(intidgli);

            if (obj != null)
            {
                //Abrimos la AUDITORIA
                var frm = new FAuditaria(obj.ApiUsucre, obj.ApiFeccre, obj.ApiUsumod, obj.ApiFecmod, obj.ApiStatus);
                frm.ShowDialog();
            }
        }

        private void btnVer_Click(object sender, EventArgs e)
        {
            try
            {
                if (gvGasLibro.RowCount <= 0) return;
                var rn = new FinAccountsController();

                //Apropiamos los valores del Grid
                Int64 intidgli = int.Parse(gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.IdAccounts.ToString()).ToString());
                var strApellido = gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.LastName.ToString()).ToString();

                //Obtenemos el objeto
                var obj = rn.GetById(intidgli);

                if (obj != null)
                {
                    var frm = new FGasCobros(obj, strApellido);
                    frm.ShowDialog();
                    if (frm.DialogResult == DialogResult.OK)
                    {
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void gcGasLibro_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ModificarRegistro();
        }

        private void lGasCobros_Activated(object sender, EventArgs e)
        {
            this.CargarListado();
        }

        private void lGasCobros_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            CargarListado();
        }

        #endregion Methods
    }
}