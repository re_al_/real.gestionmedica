


namespace ReAl.GestionMedica.App.GAS
{
	partial class LGasCobros
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.gcGasLibro = new DevExpress.XtraGrid.GridControl();
            this.gvGasLibro = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.btnAuditoría = new DevExpress.XtraEditors.SimpleButton();
            this.btnVer = new DevExpress.XtraEditors.SimpleButton();
            this.btnActualizar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcGasLibro)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvGasLibro)).BeginInit();
            this.SuspendLayout();
            // 
            // gcGasLibro
            // 
            this.gcGasLibro.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcGasLibro.Location = new System.Drawing.Point(3, 68);
            this.gcGasLibro.MainView = this.gvGasLibro;
            this.gcGasLibro.Name = "gcGasLibro";
            this.gcGasLibro.Size = new System.Drawing.Size(781, 442);
            this.gcGasLibro.TabIndex = 1;
            this.gcGasLibro.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvGasLibro});
            this.gcGasLibro.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.gcGasLibro_MouseDoubleClick);
            // 
            // gvGasLibro
            // 
            this.gvGasLibro.GridControl = this.gcGasLibro;
            this.gvGasLibro.Name = "gvGasLibro";
            // 
            // lblTitulo
            // 
            this.lblTitulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblTitulo.Location = new System.Drawing.Point(0, 0);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(784, 63);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Cobros Pendientes";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnAuditoría
            // 
            this.btnAuditoría.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAuditoría.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAuditoría.Location = new System.Drawing.Point(436, 516);
            this.btnAuditoría.Name = "btnAuditoría";
            this.btnAuditoría.Size = new System.Drawing.Size(108, 34);
            this.btnAuditoría.TabIndex = 2;
            this.btnAuditoría.Text = "&Auditoría";
            this.btnAuditoría.Click += new System.EventHandler(this.btnAuditoría_Click);
            // 
            // btnVer
            // 
            this.btnVer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVer.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnVer.Appearance.Options.UseBackColor = true;
            this.btnVer.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnVer.Location = new System.Drawing.Point(664, 516);
            this.btnVer.Name = "btnVer";
            this.btnVer.Size = new System.Drawing.Size(108, 34);
            this.btnVer.TabIndex = 4;
            this.btnVer.Text = "&Ver";
            this.btnVer.Click += new System.EventHandler(this.btnVer_Click);
            // 
            // btnActualizar
            // 
            this.btnActualizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnActualizar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Question;
            this.btnActualizar.Appearance.Options.UseBackColor = true;
            this.btnActualizar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnActualizar.Location = new System.Drawing.Point(550, 516);
            this.btnActualizar.Name = "btnActualizar";
            this.btnActualizar.Size = new System.Drawing.Size(108, 34);
            this.btnActualizar.TabIndex = 3;
            this.btnActualizar.Text = "Actualizar";
            this.btnActualizar.Click += new System.EventHandler(this.btnActualizar_Click);
            // 
            // LGasCobros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.btnActualizar);
            this.Controls.Add(this.btnAuditoría);
            this.Controls.Add(this.btnVer);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.gcGasLibro);
            this.Name = "LGasCobros";
            this.Text = "Cobros Pendientes";
            this.Activated += new System.EventHandler(this.lGasCobros_Activated);
            this.Load += new System.EventHandler(this.lGasCobros_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcGasLibro)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvGasLibro)).EndInit();
            this.ResumeLayout(false);

		}
		
		#endregion
		
		private DevExpress.XtraGrid.GridControl gcGasLibro;
		private DevExpress.XtraGrid.Views.Grid.GridView gvGasLibro;
        internal System.Windows.Forms.Label lblTitulo;
        private DevExpress.XtraEditors.SimpleButton btnAuditoría;
        private DevExpress.XtraEditors.SimpleButton btnVer;
        private DevExpress.XtraEditors.SimpleButton btnActualizar;
	}
}

