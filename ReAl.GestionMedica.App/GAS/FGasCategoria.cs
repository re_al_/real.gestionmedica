using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;

namespace ReAl.GestionMedica.App.GAS
{
    public partial class FGasCategoria : DevExpress.XtraEditors.XtraForm
    {
        #region Fields

        private readonly FinCategories _myObj = null;

        #endregion Fields

        #region Constructors

        public FGasCategoria(FinCategories obj)
        {
            InitializeComponent();

            _myObj = obj;
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                if (_myObj != null)
                {
                    //Insertamos el registro
                    var rn = new FinCategoriesController();
                    _myObj.Description = txtdescripciongca.Text;
                    rn.Update(_myObj);
                    bProcede = true;
                }
            }
            catch (Exception exp)
            {
                bProcede = false;
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
            if (bProcede)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void fGasCategoria_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            if (_myObj != null) txtdescripciongca.Text = _myObj.Description;
        }

        #endregion Methods
    }
}