using System.Collections;
using DevExpress.Data;
using DevExpress.XtraGrid.Views.Grid;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;

namespace ReAl.GestionMedica.App.GAS
{
    public partial class LGasLibro : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public LGasLibro()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        public void CargarListado()
        {
            try
            {
                var filter = new FilterDates();
                var listDates = new List<string>();
                foreach (DateTime fecha in dateNavigator1.Selection)
                {
                    listDates.Add(fecha.ToString("yyyyMMdd"));
                }

                filter.Dates = listDates;
                var rn = new FinAccountsController();
                var lista = rn.GetByDates(filter);
                gcGasLibro.DataSource = lista;
                if (lista != null && lista.Count > 0)
                {
                    this.gvGasLibro.PopulateColumns();
                    this.gvGasLibro.OptionsBehavior.Editable = false;
                    this.gvGasLibro.Columns[FinAccounts.Fields.IdAccounts.ToString()].Visible = false;
                    this.gvGasLibro.Columns[FinAccounts.Fields.Type.ToString()].Visible = false;

                    //gvGasLibro.GroupSummary.Add(new GridSummaryItem(SummaryItemType.Sum, EntGasLibro.Fields.monto.ToString(),"Total: {0:c2}"));

                    gvGasLibro.Columns[FinAccounts.Fields.AccountDate.ToString()].GroupIndex = 1;
                    gvGasLibro.OptionsView.ShowFooter = true;
                    gvGasLibro.Columns[FinAccounts.Fields.Amount.ToString()].SummaryItem.SummaryType = SummaryItemType.Sum;
                    gvGasLibro.Columns[FinAccounts.Fields.Amount.ToString()].SummaryItem.FieldName = FinAccounts.Fields.Amount.ToString();
                    gvGasLibro.Columns[FinAccounts.Fields.Amount.ToString()].SummaryItem.DisplayFormat = "Total: {0:n2}";

                    gvGasLibro.ExpandAllGroups();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void EliminarRegistro()
        {
            try
            {
                if (gvGasLibro.RowCount <= 0) return;
                if (MessageBox.Show("¿Desea ELIMINAR el registro seleccionado?", "Eliminar registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    //Eliminamos el registro seleccionado
                    var rn = new FinAccountsController();

                    //Apropiamos los valores del Grid
                    Int64 intidgli = int.Parse(gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.IdAccounts.ToString()).ToString());

                    //Obtenemos el objeto
                    var obj = rn.GetById(intidgli);

                    if (obj != null)
                    {
                        rn.Delete(obj.IdAccounts);
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void ModificarRegistro()
        {
            try
            {
                if (gvGasLibro.RowCount <= 0) return;
                var rn = new FinAccountsController();

                //Apropiamos los valores del Grid
                Int64 intidgli = int.Parse(gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.IdAccounts.ToString()).ToString());

                //Obtenemos el objeto
                var obj = rn.GetById(intidgli);

                if (obj != null)
                {
                    var frm = new FGasLibro(obj);
                    var resultado = frm.ShowDialog();
                    if (resultado == DialogResult.OK)
                    {
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void btnAuditoría_Click(object sender, EventArgs e)
        {
            if (gvGasLibro.RowCount <= 0) return;
            var rn = new FinAccountsController();

            //Apropiamos los valores del Grid
            Int64 intidgli = int.Parse(gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.IdAccounts.ToString()).ToString());

            //Obtenemos el objeto
            var obj = rn.GetById(intidgli);

            if (obj != null)
            {
                //Abrimos la AUDITORIA
                var frm = new FAuditaria(obj.ApiUsucre, obj.ApiFeccre, obj.ApiUsumod, obj.ApiFecmod, obj.ApiStatus);
                frm.ShowDialog();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarRegistro();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            ModificarRegistro();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var res = DialogResult.Yes;

            while (res == DialogResult.Yes)
            {
                var frm = new FGasLibroDialog();
                frm.ShowDialog();
                res = frm.DialogResult;
            }
            this.CargarListado();
        }

        private void dateNavigator1_EditDateModified(object sender, EventArgs e)
        {
            this.CargarListado();
        }

        private void gcGasLibro_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                EliminarRegistro();
            }
        }

        private void gcGasLibro_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ModificarRegistro();
        }

        private void gvGasLibro_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            //gvGasLibro.SetFocusedRowCellValue(EntGasLibro.Fields.usucre.ToString(),"postgres");
        }

        private void gvGasLibro_RowStyle(object sender, DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs e)
        {
            var view = sender as GridView;
            if (e.RowHandle >= 0)
            {
                var strTipoGasto = view.GetRowCellDisplayText(e.RowHandle, view.Columns[FinAccounts.Fields.Type.ToString()]);

                if (strTipoGasto == "I")
                {
                    e.Appearance.BackColor = Color.ForestGreen;
                    e.Appearance.BackColor2 = Color.LightGreen;
                }
                if (strTipoGasto == "G")
                {
                    e.Appearance.BackColor = Color.Salmon;
                    e.Appearance.BackColor2 = Color.SeaShell;
                }
            }
        }

        private void gvGasLibro_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            //Eliminamos el registro seleccionado
            var rn = new FinAccountsController();

            //Apropiamos los valores del Grid
            Int64 intidgli = int.Parse(gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.IdAccounts.ToString()).ToString());

            var objExistente = rn.GetById(intidgli);

            if (objExistente != null)
            {
                objExistente.Name = gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.Name.ToString()).ToString();
                objExistente.IdCategories = int.Parse(gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.IdCategories.ToString()).ToString());
                objExistente.Description = gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.Description.ToString()).ToString();
                objExistente.AccountDate = DateTime.Parse(gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.AccountDate.ToString()).ToString());
                objExistente.Type = gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.Type.ToString()).ToString();
                objExistente.Amount = decimal.Parse(gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.Amount.ToString()).ToString());
                objExistente.IdAppointments = int.Parse(gvGasLibro.GetFocusedRowCellValue(FinAccounts.Fields.IdAppointments.ToString()).ToString());
                objExistente.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                //Actualizamos el registro
                rn.Update(objExistente);
                this.CargarListado();
            }
        }

        private void lGasLibro_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            dateNavigator1.DateTime = DateTime.Now;

            CargarListado();
        }

        #endregion Methods
    }
}