﻿namespace ReAl.GestionMedica.App
{
    public partial class FAbout : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public FAbout()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fAbout_Load(object sender, EventArgs e)
        {
            if (DateTime.Now.Year == 2013)
                labelControl3.Text = "© 2013 ReAl Systems";
            else
                labelControl3.Text = "© 2013 - " + DateTime.Now.Year + " ReAl Systems";
        }

        #endregion Methods
    }
}