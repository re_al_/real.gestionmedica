using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;

namespace ReAl.GestionMedica.App.CLA
{
    public partial class LClaTipoplantillas : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public LClaTipoplantillas()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        public void CargarListado()
        {
            var rn = new ClaTemplatestypeController();
            gcClaTipoplantillas.DataSource = rn.GetActive();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            //Insertamos el registro
            var rn = new ClaTemplatestypeController();
            var obj = new ClaTemplatestype();
            obj.Name= txtnombrectp.Text;
            obj.Description = txtdescripcionctp.Text;
            obj.ApiUsucre = cParametrosApp.AppRestUser.Login;

            rn.Create(obj);
            this.CargarListado();
            this.DialogResult = DialogResult.OK;
            //this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void fClaTipoplantillas_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            CargarListado();
        }

        private void gcClaTipoplantillas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                //Eliminamos el registro seleccionado
                var rn = new ClaTemplatestypeController();

                //Apropiamos los valores del Grid
                int intidctp = int.Parse(gvClaTipoplantillas.GetFocusedRowCellValue(ClaTemplatestype.Fields.IdTemplatesType.ToString()).ToString());

                //Obtenemos el objeto
                var obj = rn.GetById(intidctp);

                if (obj != null)
                {
                    rn.Delete(obj.IdTemplatesType);
                    this.CargarListado();
                }
            }
        }

        private void gvClaTipoplantillas_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            //gvClaTipoplantillas.SetFocusedRowCellValue(EntClaTipoplantillas.Fields.usucre.ToString(),"postgres");
        }

        private void gvClaTipoplantillas_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            //Eliminamos el registro seleccionado
            var rn = new ClaTemplatestypeController();

            //Apropiamos los valores del Grid
            int intidctp = int.Parse(gvClaTipoplantillas.GetFocusedRowCellValue(ClaTemplatestype.Fields.IdTemplatesType.ToString()).ToString());

            var objExistente = rn.GetById(intidctp);

            if (objExistente != null)
            {
                objExistente.Name = gvClaTipoplantillas.GetFocusedRowCellValue("nombrectp").ToString();
                objExistente.Description = gvClaTipoplantillas.GetFocusedRowCellValue("descripcionctp").ToString();

                objExistente.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                objExistente.ApiUsumod = cParametrosApp.AppRestUser.Login;

                //Actualizamos el registro
                rn.Update(objExistente);
                this.CargarListado();
            }
        }

        #endregion Methods
    }
}