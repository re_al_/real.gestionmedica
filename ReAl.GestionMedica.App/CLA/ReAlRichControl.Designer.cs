﻿namespace ReAl.GestionMedica.App.CLA
{
    partial class ReAlRichControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            richEditControl1 = new DevExpress.XtraRichEdit.RichEditControl();
            barManager1 = new DevExpress.XtraBars.BarManager(components);
            clipboardBar1 = new DevExpress.XtraRichEdit.UI.ClipboardBar();
            pasteItem1 = new DevExpress.XtraRichEdit.UI.PasteItem();
            cutItem1 = new DevExpress.XtraRichEdit.UI.CutItem();
            copyItem1 = new DevExpress.XtraRichEdit.UI.CopyItem();
            pasteSpecialItem1 = new DevExpress.XtraRichEdit.UI.PasteSpecialItem();
            fontBar1 = new DevExpress.XtraRichEdit.UI.FontBar();
            changeFontNameItem1 = new DevExpress.XtraRichEdit.UI.ChangeFontNameItem();
            repositoryItemFontEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemFontEdit();
            changeFontSizeItem1 = new DevExpress.XtraRichEdit.UI.ChangeFontSizeItem();
            repositoryItemRichEditFontSizeEdit1 = new DevExpress.XtraRichEdit.Design.RepositoryItemRichEditFontSizeEdit();
            fontSizeIncreaseItem1 = new DevExpress.XtraRichEdit.UI.FontSizeIncreaseItem();
            fontSizeDecreaseItem1 = new DevExpress.XtraRichEdit.UI.FontSizeDecreaseItem();
            toggleFontBoldItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontBoldItem();
            toggleFontItalicItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontItalicItem();
            toggleFontUnderlineItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontUnderlineItem();
            toggleFontDoubleUnderlineItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontDoubleUnderlineItem();
            toggleFontStrikeoutItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontStrikeoutItem();
            toggleFontDoubleStrikeoutItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontDoubleStrikeoutItem();
            toggleFontSuperscriptItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontSuperscriptItem();
            toggleFontSubscriptItem1 = new DevExpress.XtraRichEdit.UI.ToggleFontSubscriptItem();
            changeFontColorItem1 = new DevExpress.XtraRichEdit.UI.ChangeFontColorItem();
            changeFontHighlightColorItem1 = new DevExpress.XtraRichEdit.UI.ChangeFontHighlightColorItem();
            changeTextCaseItem1 = new DevExpress.XtraRichEdit.UI.ChangeTextCaseItem();
            makeTextUpperCaseItem1 = new DevExpress.XtraRichEdit.UI.MakeTextUpperCaseItem();
            makeTextLowerCaseItem1 = new DevExpress.XtraRichEdit.UI.MakeTextLowerCaseItem();
            capitalizeEachWordCaseItem1 = new DevExpress.XtraRichEdit.UI.CapitalizeEachWordCaseItem();
            toggleTextCaseItem1 = new DevExpress.XtraRichEdit.UI.ToggleTextCaseItem();
            clearFormattingItem1 = new DevExpress.XtraRichEdit.UI.ClearFormattingItem();
            showFontFormItem1 = new DevExpress.XtraRichEdit.UI.ShowFontFormItem();
            paragraphBar1 = new DevExpress.XtraRichEdit.UI.ParagraphBar();
            toggleBulletedListItem1 = new DevExpress.XtraRichEdit.UI.ToggleBulletedListItem();
            toggleNumberingListItem1 = new DevExpress.XtraRichEdit.UI.ToggleNumberingListItem();
            toggleMultiLevelListItem1 = new DevExpress.XtraRichEdit.UI.ToggleMultiLevelListItem();
            decreaseIndentItem1 = new DevExpress.XtraRichEdit.UI.DecreaseIndentItem();
            increaseIndentItem1 = new DevExpress.XtraRichEdit.UI.IncreaseIndentItem();
            rtlToggleBulletedListItem1 = new DevExpress.XtraRichEdit.UI.RtlToggleBulletedListItem();
            rtlToggleNumberingListItem1 = new DevExpress.XtraRichEdit.UI.RtlToggleNumberingListItem();
            rtlToggleMultiLevelListItem1 = new DevExpress.XtraRichEdit.UI.RtlToggleMultiLevelListItem();
            rtlDecreaseIndentItem1 = new DevExpress.XtraRichEdit.UI.RtlDecreaseIndentItem();
            rtlIncreaseIndentItem1 = new DevExpress.XtraRichEdit.UI.RtlIncreaseIndentItem();
            toggleParagraphLeftToRightItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphLeftToRightItem();
            toggleParagraphRightToLeftItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphRightToLeftItem();
            toggleParagraphAlignmentLeftItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentLeftItem();
            toggleParagraphAlignmentCenterItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentCenterItem();
            toggleParagraphAlignmentRightItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentRightItem();
            toggleParagraphAlignmentJustifyItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentJustifyItem();
            toggleParagraphAlignmentArabicJustifyGroupItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentArabicJustifyGroupItem();
            toggleParagraphAlignmentArabicJustifyItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentArabicJustifyItem();
            toggleParagraphAlignmentJustifyLowItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentJustifyLowItem();
            toggleParagraphAlignmentJustifyMediumItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentJustifyMediumItem();
            toggleParagraphAlignmentJustifyHighItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentJustifyHighItem();
            toggleParagraphAlignmentDistributeItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentDistributeItem();
            toggleParagraphAlignmentThaiDistributeItem1 = new DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentThaiDistributeItem();
            toggleShowWhitespaceItem1 = new DevExpress.XtraRichEdit.UI.ToggleShowWhitespaceItem();
            changeParagraphLineSpacingItem1 = new DevExpress.XtraRichEdit.UI.ChangeParagraphLineSpacingItem();
            setSingleParagraphSpacingItem1 = new DevExpress.XtraRichEdit.UI.SetSingleParagraphSpacingItem();
            setSesquialteralParagraphSpacingItem1 = new DevExpress.XtraRichEdit.UI.SetSesquialteralParagraphSpacingItem();
            setDoubleParagraphSpacingItem1 = new DevExpress.XtraRichEdit.UI.SetDoubleParagraphSpacingItem();
            showLineSpacingFormItem1 = new DevExpress.XtraRichEdit.UI.ShowLineSpacingFormItem();
            addSpacingBeforeParagraphItem1 = new DevExpress.XtraRichEdit.UI.AddSpacingBeforeParagraphItem();
            removeSpacingBeforeParagraphItem1 = new DevExpress.XtraRichEdit.UI.RemoveSpacingBeforeParagraphItem();
            addSpacingAfterParagraphItem1 = new DevExpress.XtraRichEdit.UI.AddSpacingAfterParagraphItem();
            removeSpacingAfterParagraphItem1 = new DevExpress.XtraRichEdit.UI.RemoveSpacingAfterParagraphItem();
            changeParagraphBackColorItem1 = new DevExpress.XtraRichEdit.UI.ChangeParagraphBackColorItem();
            showParagraphFormItem1 = new DevExpress.XtraRichEdit.UI.ShowParagraphFormItem();
            stylesBar1 = new DevExpress.XtraRichEdit.UI.StylesBar();
            changeStyleItem1 = new DevExpress.XtraRichEdit.UI.ChangeStyleItem();
            repositoryItemRichEditStyleEdit1 = new DevExpress.XtraRichEdit.Design.RepositoryItemRichEditStyleEdit();
            showEditStyleFormItem1 = new DevExpress.XtraRichEdit.UI.ShowEditStyleFormItem();
            editingBar1 = new DevExpress.XtraRichEdit.UI.EditingBar();
            findItem1 = new DevExpress.XtraRichEdit.UI.FindItem();
            replaceItem1 = new DevExpress.XtraRichEdit.UI.ReplaceItem();
            barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            richEditBarController1 = new DevExpress.XtraRichEdit.UI.RichEditBarController(components);
            ((System.ComponentModel.ISupportInitialize)barManager1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)repositoryItemFontEdit1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)repositoryItemRichEditFontSizeEdit1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)repositoryItemRichEditStyleEdit1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)richEditBarController1).BeginInit();
            SuspendLayout();
            // 
            // richEditControl1
            // 
            richEditControl1.ActiveViewType = DevExpress.XtraRichEdit.RichEditViewType.Simple;
            richEditControl1.Dock = DockStyle.Fill;
            richEditControl1.LayoutUnit = DevExpress.XtraRichEdit.DocumentLayoutUnit.Pixel;
            richEditControl1.Location = new Point(0, 74);
            richEditControl1.MenuManager = barManager1;
            richEditControl1.Name = "richEditControl1";
            richEditControl1.Options.Printing.PrintPreviewFormKind = DevExpress.XtraRichEdit.PrintPreviewFormKind.Bars;
            richEditControl1.Size = new Size(958, 292);
            richEditControl1.TabIndex = 0;
            // 
            // barManager1
            // 
            barManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] { clipboardBar1, fontBar1, paragraphBar1, stylesBar1, editingBar1 });
            barManager1.DockControls.Add(barDockControlTop);
            barManager1.DockControls.Add(barDockControlBottom);
            barManager1.DockControls.Add(barDockControlLeft);
            barManager1.DockControls.Add(barDockControlRight);
            barManager1.Form = this;
            barManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] { pasteItem1, cutItem1, copyItem1, pasteSpecialItem1, changeFontNameItem1, changeFontSizeItem1, fontSizeIncreaseItem1, fontSizeDecreaseItem1, toggleFontBoldItem1, toggleFontItalicItem1, toggleFontUnderlineItem1, toggleFontDoubleUnderlineItem1, toggleFontStrikeoutItem1, toggleFontDoubleStrikeoutItem1, toggleFontSuperscriptItem1, toggleFontSubscriptItem1, changeFontColorItem1, changeFontHighlightColorItem1, changeTextCaseItem1, makeTextUpperCaseItem1, makeTextLowerCaseItem1, capitalizeEachWordCaseItem1, toggleTextCaseItem1, clearFormattingItem1, showFontFormItem1, toggleBulletedListItem1, toggleNumberingListItem1, toggleMultiLevelListItem1, decreaseIndentItem1, increaseIndentItem1, rtlToggleBulletedListItem1, rtlToggleNumberingListItem1, rtlToggleMultiLevelListItem1, rtlDecreaseIndentItem1, rtlIncreaseIndentItem1, toggleParagraphLeftToRightItem1, toggleParagraphRightToLeftItem1, toggleParagraphAlignmentLeftItem1, toggleParagraphAlignmentCenterItem1, toggleParagraphAlignmentRightItem1, toggleParagraphAlignmentJustifyItem1, toggleParagraphAlignmentArabicJustifyGroupItem1, toggleParagraphAlignmentArabicJustifyItem1, toggleParagraphAlignmentJustifyLowItem1, toggleParagraphAlignmentJustifyMediumItem1, toggleParagraphAlignmentJustifyHighItem1, toggleParagraphAlignmentDistributeItem1, toggleParagraphAlignmentThaiDistributeItem1, toggleShowWhitespaceItem1, changeParagraphLineSpacingItem1, setSingleParagraphSpacingItem1, setSesquialteralParagraphSpacingItem1, setDoubleParagraphSpacingItem1, showLineSpacingFormItem1, addSpacingBeforeParagraphItem1, removeSpacingBeforeParagraphItem1, addSpacingAfterParagraphItem1, removeSpacingAfterParagraphItem1, changeParagraphBackColorItem1, showParagraphFormItem1, changeStyleItem1, showEditStyleFormItem1, findItem1, replaceItem1 });
            barManager1.MaxItemId = 64;
            barManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] { repositoryItemFontEdit1, repositoryItemRichEditFontSizeEdit1, repositoryItemRichEditStyleEdit1 });
            // 
            // clipboardBar1
            // 
            clipboardBar1.Control = richEditControl1;
            clipboardBar1.DockCol = 0;
            clipboardBar1.DockRow = 0;
            clipboardBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            clipboardBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, pasteItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "V", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, cutItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "X", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, copyItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "C", ""), new DevExpress.XtraBars.LinkPersistInfo(pasteSpecialItem1) });
            // 
            // pasteItem1
            // 
            pasteItem1.Id = 0;
            pasteItem1.Name = "pasteItem1";
            // 
            // cutItem1
            // 
            cutItem1.Id = 1;
            cutItem1.Name = "cutItem1";
            // 
            // copyItem1
            // 
            copyItem1.Id = 2;
            copyItem1.Name = "copyItem1";
            // 
            // pasteSpecialItem1
            // 
            pasteSpecialItem1.Id = 3;
            pasteSpecialItem1.Name = "pasteSpecialItem1";
            // 
            // fontBar1
            // 
            fontBar1.Control = richEditControl1;
            fontBar1.DockCol = 0;
            fontBar1.DockRow = 2;
            fontBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            fontBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, changeFontNameItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "FF", ""), new DevExpress.XtraBars.LinkPersistInfo(changeFontSizeItem1), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, fontSizeIncreaseItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "FG", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, fontSizeDecreaseItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "FK", ""), new DevExpress.XtraBars.LinkPersistInfo(toggleFontBoldItem1), new DevExpress.XtraBars.LinkPersistInfo(toggleFontItalicItem1), new DevExpress.XtraBars.LinkPersistInfo(toggleFontUnderlineItem1), new DevExpress.XtraBars.LinkPersistInfo(toggleFontDoubleUnderlineItem1), new DevExpress.XtraBars.LinkPersistInfo(toggleFontStrikeoutItem1), new DevExpress.XtraBars.LinkPersistInfo(toggleFontDoubleStrikeoutItem1), new DevExpress.XtraBars.LinkPersistInfo(toggleFontSuperscriptItem1), new DevExpress.XtraBars.LinkPersistInfo(toggleFontSubscriptItem1), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, changeFontColorItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "FC", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, changeFontHighlightColorItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "I", ""), new DevExpress.XtraBars.LinkPersistInfo(changeTextCaseItem1), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, clearFormattingItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "E", ""), new DevExpress.XtraBars.LinkPersistInfo(showFontFormItem1) });
            // 
            // changeFontNameItem1
            // 
            changeFontNameItem1.Edit = repositoryItemFontEdit1;
            changeFontNameItem1.Id = 4;
            changeFontNameItem1.Name = "changeFontNameItem1";
            // 
            // repositoryItemFontEdit1
            // 
            repositoryItemFontEdit1.AutoHeight = false;
            repositoryItemFontEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            repositoryItemFontEdit1.Name = "repositoryItemFontEdit1";
            // 
            // changeFontSizeItem1
            // 
            changeFontSizeItem1.Edit = repositoryItemRichEditFontSizeEdit1;
            changeFontSizeItem1.Id = 5;
            changeFontSizeItem1.Name = "changeFontSizeItem1";
            // 
            // repositoryItemRichEditFontSizeEdit1
            // 
            repositoryItemRichEditFontSizeEdit1.AutoHeight = false;
            repositoryItemRichEditFontSizeEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            repositoryItemRichEditFontSizeEdit1.Control = richEditControl1;
            repositoryItemRichEditFontSizeEdit1.Name = "repositoryItemRichEditFontSizeEdit1";
            // 
            // fontSizeIncreaseItem1
            // 
            fontSizeIncreaseItem1.Id = 6;
            fontSizeIncreaseItem1.Name = "fontSizeIncreaseItem1";
            // 
            // fontSizeDecreaseItem1
            // 
            fontSizeDecreaseItem1.Id = 7;
            fontSizeDecreaseItem1.Name = "fontSizeDecreaseItem1";
            // 
            // toggleFontBoldItem1
            // 
            toggleFontBoldItem1.Id = 8;
            toggleFontBoldItem1.Name = "toggleFontBoldItem1";
            // 
            // toggleFontItalicItem1
            // 
            toggleFontItalicItem1.Id = 9;
            toggleFontItalicItem1.Name = "toggleFontItalicItem1";
            // 
            // toggleFontUnderlineItem1
            // 
            toggleFontUnderlineItem1.Id = 10;
            toggleFontUnderlineItem1.Name = "toggleFontUnderlineItem1";
            // 
            // toggleFontDoubleUnderlineItem1
            // 
            toggleFontDoubleUnderlineItem1.Id = 11;
            toggleFontDoubleUnderlineItem1.Name = "toggleFontDoubleUnderlineItem1";
            // 
            // toggleFontStrikeoutItem1
            // 
            toggleFontStrikeoutItem1.Id = 12;
            toggleFontStrikeoutItem1.Name = "toggleFontStrikeoutItem1";
            // 
            // toggleFontDoubleStrikeoutItem1
            // 
            toggleFontDoubleStrikeoutItem1.Id = 13;
            toggleFontDoubleStrikeoutItem1.Name = "toggleFontDoubleStrikeoutItem1";
            // 
            // toggleFontSuperscriptItem1
            // 
            toggleFontSuperscriptItem1.Id = 14;
            toggleFontSuperscriptItem1.Name = "toggleFontSuperscriptItem1";
            // 
            // toggleFontSubscriptItem1
            // 
            toggleFontSubscriptItem1.Id = 15;
            toggleFontSubscriptItem1.Name = "toggleFontSubscriptItem1";
            // 
            // changeFontColorItem1
            // 
            changeFontColorItem1.Id = 16;
            changeFontColorItem1.Name = "changeFontColorItem1";
            // 
            // changeFontHighlightColorItem1
            // 
            changeFontHighlightColorItem1.Id = 17;
            changeFontHighlightColorItem1.Name = "changeFontHighlightColorItem1";
            // 
            // changeTextCaseItem1
            // 
            changeTextCaseItem1.Id = 18;
            changeTextCaseItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(makeTextUpperCaseItem1), new DevExpress.XtraBars.LinkPersistInfo(makeTextLowerCaseItem1), new DevExpress.XtraBars.LinkPersistInfo(capitalizeEachWordCaseItem1), new DevExpress.XtraBars.LinkPersistInfo(toggleTextCaseItem1) });
            changeTextCaseItem1.Name = "changeTextCaseItem1";
            // 
            // makeTextUpperCaseItem1
            // 
            makeTextUpperCaseItem1.Id = 19;
            makeTextUpperCaseItem1.Name = "makeTextUpperCaseItem1";
            // 
            // makeTextLowerCaseItem1
            // 
            makeTextLowerCaseItem1.Id = 20;
            makeTextLowerCaseItem1.Name = "makeTextLowerCaseItem1";
            // 
            // capitalizeEachWordCaseItem1
            // 
            capitalizeEachWordCaseItem1.Id = 21;
            capitalizeEachWordCaseItem1.Name = "capitalizeEachWordCaseItem1";
            // 
            // toggleTextCaseItem1
            // 
            toggleTextCaseItem1.Id = 22;
            toggleTextCaseItem1.Name = "toggleTextCaseItem1";
            // 
            // clearFormattingItem1
            // 
            clearFormattingItem1.Id = 23;
            clearFormattingItem1.Name = "clearFormattingItem1";
            // 
            // showFontFormItem1
            // 
            showFontFormItem1.Id = 24;
            showFontFormItem1.Name = "showFontFormItem1";
            // 
            // paragraphBar1
            // 
            paragraphBar1.Control = richEditControl1;
            paragraphBar1.DockCol = 0;
            paragraphBar1.DockRow = 1;
            paragraphBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            paragraphBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, toggleBulletedListItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "U", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, toggleNumberingListItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "N", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, toggleMultiLevelListItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "M", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, decreaseIndentItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "AO", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, increaseIndentItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "AI", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, rtlToggleBulletedListItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "U", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, rtlToggleNumberingListItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "N", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, rtlToggleMultiLevelListItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "M", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, rtlDecreaseIndentItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "AO", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, rtlIncreaseIndentItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "AI", ""), new DevExpress.XtraBars.LinkPersistInfo(toggleParagraphLeftToRightItem1), new DevExpress.XtraBars.LinkPersistInfo(toggleParagraphRightToLeftItem1), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, toggleParagraphAlignmentLeftItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "AL", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, toggleParagraphAlignmentCenterItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "AC", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, toggleParagraphAlignmentRightItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "AR", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, toggleParagraphAlignmentJustifyItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "AJ", ""), new DevExpress.XtraBars.LinkPersistInfo(toggleParagraphAlignmentArabicJustifyGroupItem1), new DevExpress.XtraBars.LinkPersistInfo(toggleParagraphAlignmentDistributeItem1), new DevExpress.XtraBars.LinkPersistInfo(toggleParagraphAlignmentThaiDistributeItem1), new DevExpress.XtraBars.LinkPersistInfo(toggleShowWhitespaceItem1), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, changeParagraphLineSpacingItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "K", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, changeParagraphBackColorItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "H", ""), new DevExpress.XtraBars.LinkPersistInfo(showParagraphFormItem1) });
            // 
            // toggleBulletedListItem1
            // 
            toggleBulletedListItem1.Id = 25;
            toggleBulletedListItem1.Name = "toggleBulletedListItem1";
            // 
            // toggleNumberingListItem1
            // 
            toggleNumberingListItem1.Id = 26;
            toggleNumberingListItem1.Name = "toggleNumberingListItem1";
            // 
            // toggleMultiLevelListItem1
            // 
            toggleMultiLevelListItem1.Id = 27;
            toggleMultiLevelListItem1.Name = "toggleMultiLevelListItem1";
            // 
            // decreaseIndentItem1
            // 
            decreaseIndentItem1.Id = 28;
            decreaseIndentItem1.Name = "decreaseIndentItem1";
            // 
            // increaseIndentItem1
            // 
            increaseIndentItem1.Id = 29;
            increaseIndentItem1.Name = "increaseIndentItem1";
            // 
            // rtlToggleBulletedListItem1
            // 
            rtlToggleBulletedListItem1.Id = 30;
            rtlToggleBulletedListItem1.Name = "rtlToggleBulletedListItem1";
            // 
            // rtlToggleNumberingListItem1
            // 
            rtlToggleNumberingListItem1.Id = 31;
            rtlToggleNumberingListItem1.Name = "rtlToggleNumberingListItem1";
            // 
            // rtlToggleMultiLevelListItem1
            // 
            rtlToggleMultiLevelListItem1.Id = 32;
            rtlToggleMultiLevelListItem1.Name = "rtlToggleMultiLevelListItem1";
            // 
            // rtlDecreaseIndentItem1
            // 
            rtlDecreaseIndentItem1.Id = 33;
            rtlDecreaseIndentItem1.Name = "rtlDecreaseIndentItem1";
            // 
            // rtlIncreaseIndentItem1
            // 
            rtlIncreaseIndentItem1.Id = 34;
            rtlIncreaseIndentItem1.Name = "rtlIncreaseIndentItem1";
            // 
            // toggleParagraphLeftToRightItem1
            // 
            toggleParagraphLeftToRightItem1.Id = 35;
            toggleParagraphLeftToRightItem1.Name = "toggleParagraphLeftToRightItem1";
            // 
            // toggleParagraphRightToLeftItem1
            // 
            toggleParagraphRightToLeftItem1.Id = 36;
            toggleParagraphRightToLeftItem1.Name = "toggleParagraphRightToLeftItem1";
            // 
            // toggleParagraphAlignmentLeftItem1
            // 
            toggleParagraphAlignmentLeftItem1.Id = 37;
            toggleParagraphAlignmentLeftItem1.Name = "toggleParagraphAlignmentLeftItem1";
            // 
            // toggleParagraphAlignmentCenterItem1
            // 
            toggleParagraphAlignmentCenterItem1.Id = 38;
            toggleParagraphAlignmentCenterItem1.Name = "toggleParagraphAlignmentCenterItem1";
            // 
            // toggleParagraphAlignmentRightItem1
            // 
            toggleParagraphAlignmentRightItem1.Id = 39;
            toggleParagraphAlignmentRightItem1.Name = "toggleParagraphAlignmentRightItem1";
            // 
            // toggleParagraphAlignmentJustifyItem1
            // 
            toggleParagraphAlignmentJustifyItem1.Id = 40;
            toggleParagraphAlignmentJustifyItem1.Name = "toggleParagraphAlignmentJustifyItem1";
            // 
            // toggleParagraphAlignmentArabicJustifyGroupItem1
            // 
            toggleParagraphAlignmentArabicJustifyGroupItem1.Id = 41;
            toggleParagraphAlignmentArabicJustifyGroupItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, toggleParagraphAlignmentArabicJustifyItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "AJ", ""), new DevExpress.XtraBars.LinkPersistInfo(toggleParagraphAlignmentJustifyLowItem1), new DevExpress.XtraBars.LinkPersistInfo(toggleParagraphAlignmentJustifyMediumItem1), new DevExpress.XtraBars.LinkPersistInfo(toggleParagraphAlignmentJustifyHighItem1) });
            toggleParagraphAlignmentArabicJustifyGroupItem1.Name = "toggleParagraphAlignmentArabicJustifyGroupItem1";
            // 
            // toggleParagraphAlignmentArabicJustifyItem1
            // 
            toggleParagraphAlignmentArabicJustifyItem1.Id = 42;
            toggleParagraphAlignmentArabicJustifyItem1.Name = "toggleParagraphAlignmentArabicJustifyItem1";
            // 
            // toggleParagraphAlignmentJustifyLowItem1
            // 
            toggleParagraphAlignmentJustifyLowItem1.Id = 43;
            toggleParagraphAlignmentJustifyLowItem1.Name = "toggleParagraphAlignmentJustifyLowItem1";
            // 
            // toggleParagraphAlignmentJustifyMediumItem1
            // 
            toggleParagraphAlignmentJustifyMediumItem1.Id = 44;
            toggleParagraphAlignmentJustifyMediumItem1.Name = "toggleParagraphAlignmentJustifyMediumItem1";
            // 
            // toggleParagraphAlignmentJustifyHighItem1
            // 
            toggleParagraphAlignmentJustifyHighItem1.Id = 45;
            toggleParagraphAlignmentJustifyHighItem1.Name = "toggleParagraphAlignmentJustifyHighItem1";
            // 
            // toggleParagraphAlignmentDistributeItem1
            // 
            toggleParagraphAlignmentDistributeItem1.Id = 46;
            toggleParagraphAlignmentDistributeItem1.Name = "toggleParagraphAlignmentDistributeItem1";
            // 
            // toggleParagraphAlignmentThaiDistributeItem1
            // 
            toggleParagraphAlignmentThaiDistributeItem1.Id = 47;
            toggleParagraphAlignmentThaiDistributeItem1.Name = "toggleParagraphAlignmentThaiDistributeItem1";
            // 
            // toggleShowWhitespaceItem1
            // 
            toggleShowWhitespaceItem1.Id = 48;
            toggleShowWhitespaceItem1.Name = "toggleShowWhitespaceItem1";
            // 
            // changeParagraphLineSpacingItem1
            // 
            changeParagraphLineSpacingItem1.Id = 49;
            changeParagraphLineSpacingItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(setSingleParagraphSpacingItem1), new DevExpress.XtraBars.LinkPersistInfo(setSesquialteralParagraphSpacingItem1), new DevExpress.XtraBars.LinkPersistInfo(setDoubleParagraphSpacingItem1), new DevExpress.XtraBars.LinkPersistInfo(showLineSpacingFormItem1), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, addSpacingBeforeParagraphItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "B", ""), new DevExpress.XtraBars.LinkPersistInfo(removeSpacingBeforeParagraphItem1), new DevExpress.XtraBars.LinkPersistInfo(addSpacingAfterParagraphItem1), new DevExpress.XtraBars.LinkPersistInfo(removeSpacingAfterParagraphItem1) });
            changeParagraphLineSpacingItem1.Name = "changeParagraphLineSpacingItem1";
            // 
            // setSingleParagraphSpacingItem1
            // 
            setSingleParagraphSpacingItem1.Id = 50;
            setSingleParagraphSpacingItem1.Name = "setSingleParagraphSpacingItem1";
            // 
            // setSesquialteralParagraphSpacingItem1
            // 
            setSesquialteralParagraphSpacingItem1.Id = 51;
            setSesquialteralParagraphSpacingItem1.Name = "setSesquialteralParagraphSpacingItem1";
            // 
            // setDoubleParagraphSpacingItem1
            // 
            setDoubleParagraphSpacingItem1.Id = 52;
            setDoubleParagraphSpacingItem1.Name = "setDoubleParagraphSpacingItem1";
            // 
            // showLineSpacingFormItem1
            // 
            showLineSpacingFormItem1.Id = 53;
            showLineSpacingFormItem1.Name = "showLineSpacingFormItem1";
            // 
            // addSpacingBeforeParagraphItem1
            // 
            addSpacingBeforeParagraphItem1.Id = 54;
            addSpacingBeforeParagraphItem1.Name = "addSpacingBeforeParagraphItem1";
            // 
            // removeSpacingBeforeParagraphItem1
            // 
            removeSpacingBeforeParagraphItem1.Id = 55;
            removeSpacingBeforeParagraphItem1.Name = "removeSpacingBeforeParagraphItem1";
            // 
            // addSpacingAfterParagraphItem1
            // 
            addSpacingAfterParagraphItem1.Id = 56;
            addSpacingAfterParagraphItem1.Name = "addSpacingAfterParagraphItem1";
            // 
            // removeSpacingAfterParagraphItem1
            // 
            removeSpacingAfterParagraphItem1.Id = 57;
            removeSpacingAfterParagraphItem1.Name = "removeSpacingAfterParagraphItem1";
            // 
            // changeParagraphBackColorItem1
            // 
            changeParagraphBackColorItem1.Id = 58;
            changeParagraphBackColorItem1.Name = "changeParagraphBackColorItem1";
            // 
            // showParagraphFormItem1
            // 
            showParagraphFormItem1.Id = 59;
            showParagraphFormItem1.Name = "showParagraphFormItem1";
            // 
            // stylesBar1
            // 
            stylesBar1.Control = richEditControl1;
            stylesBar1.DockCol = 1;
            stylesBar1.DockRow = 0;
            stylesBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            stylesBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(changeStyleItem1), new DevExpress.XtraBars.LinkPersistInfo(showEditStyleFormItem1) });
            // 
            // changeStyleItem1
            // 
            changeStyleItem1.Edit = repositoryItemRichEditStyleEdit1;
            changeStyleItem1.Id = 60;
            changeStyleItem1.Name = "changeStyleItem1";
            // 
            // repositoryItemRichEditStyleEdit1
            // 
            repositoryItemRichEditStyleEdit1.AutoHeight = false;
            repositoryItemRichEditStyleEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            repositoryItemRichEditStyleEdit1.Control = richEditControl1;
            repositoryItemRichEditStyleEdit1.Name = "repositoryItemRichEditStyleEdit1";
            // 
            // showEditStyleFormItem1
            // 
            showEditStyleFormItem1.Id = 61;
            showEditStyleFormItem1.Name = "showEditStyleFormItem1";
            // 
            // editingBar1
            // 
            editingBar1.Control = richEditControl1;
            editingBar1.DockCol = 2;
            editingBar1.DockRow = 0;
            editingBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            editingBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] { new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, findItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "FD", ""), new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.KeyTip, replaceItem1, "", false, true, true, 0, null, DevExpress.XtraBars.BarItemPaintStyle.Standard, "R", "") });
            // 
            // findItem1
            // 
            findItem1.Id = 62;
            findItem1.Name = "findItem1";
            // 
            // replaceItem1
            // 
            replaceItem1.Id = 63;
            replaceItem1.Name = "replaceItem1";
            // 
            // barDockControlTop
            // 
            barDockControlTop.CausesValidation = false;
            barDockControlTop.Dock = DockStyle.Top;
            barDockControlTop.Location = new Point(0, 0);
            barDockControlTop.Manager = barManager1;
            barDockControlTop.Size = new Size(958, 74);
            // 
            // barDockControlBottom
            // 
            barDockControlBottom.CausesValidation = false;
            barDockControlBottom.Dock = DockStyle.Bottom;
            barDockControlBottom.Location = new Point(0, 366);
            barDockControlBottom.Manager = barManager1;
            barDockControlBottom.Size = new Size(958, 0);
            // 
            // barDockControlLeft
            // 
            barDockControlLeft.CausesValidation = false;
            barDockControlLeft.Dock = DockStyle.Left;
            barDockControlLeft.Location = new Point(0, 74);
            barDockControlLeft.Manager = barManager1;
            barDockControlLeft.Size = new Size(0, 292);
            // 
            // barDockControlRight
            // 
            barDockControlRight.CausesValidation = false;
            barDockControlRight.Dock = DockStyle.Right;
            barDockControlRight.Location = new Point(958, 74);
            barDockControlRight.Manager = barManager1;
            barDockControlRight.Size = new Size(0, 292);
            // 
            // richEditBarController1
            // 
            richEditBarController1.BarItems.Add(pasteItem1);
            richEditBarController1.BarItems.Add(cutItem1);
            richEditBarController1.BarItems.Add(copyItem1);
            richEditBarController1.BarItems.Add(pasteSpecialItem1);
            richEditBarController1.BarItems.Add(changeFontNameItem1);
            richEditBarController1.BarItems.Add(changeFontSizeItem1);
            richEditBarController1.BarItems.Add(fontSizeIncreaseItem1);
            richEditBarController1.BarItems.Add(fontSizeDecreaseItem1);
            richEditBarController1.BarItems.Add(toggleFontBoldItem1);
            richEditBarController1.BarItems.Add(toggleFontItalicItem1);
            richEditBarController1.BarItems.Add(toggleFontUnderlineItem1);
            richEditBarController1.BarItems.Add(toggleFontDoubleUnderlineItem1);
            richEditBarController1.BarItems.Add(toggleFontStrikeoutItem1);
            richEditBarController1.BarItems.Add(toggleFontDoubleStrikeoutItem1);
            richEditBarController1.BarItems.Add(toggleFontSuperscriptItem1);
            richEditBarController1.BarItems.Add(toggleFontSubscriptItem1);
            richEditBarController1.BarItems.Add(changeFontColorItem1);
            richEditBarController1.BarItems.Add(changeFontHighlightColorItem1);
            richEditBarController1.BarItems.Add(makeTextUpperCaseItem1);
            richEditBarController1.BarItems.Add(makeTextLowerCaseItem1);
            richEditBarController1.BarItems.Add(capitalizeEachWordCaseItem1);
            richEditBarController1.BarItems.Add(toggleTextCaseItem1);
            richEditBarController1.BarItems.Add(changeTextCaseItem1);
            richEditBarController1.BarItems.Add(clearFormattingItem1);
            richEditBarController1.BarItems.Add(showFontFormItem1);
            richEditBarController1.BarItems.Add(toggleBulletedListItem1);
            richEditBarController1.BarItems.Add(toggleNumberingListItem1);
            richEditBarController1.BarItems.Add(toggleMultiLevelListItem1);
            richEditBarController1.BarItems.Add(decreaseIndentItem1);
            richEditBarController1.BarItems.Add(increaseIndentItem1);
            richEditBarController1.BarItems.Add(rtlToggleBulletedListItem1);
            richEditBarController1.BarItems.Add(rtlToggleNumberingListItem1);
            richEditBarController1.BarItems.Add(rtlToggleMultiLevelListItem1);
            richEditBarController1.BarItems.Add(rtlDecreaseIndentItem1);
            richEditBarController1.BarItems.Add(rtlIncreaseIndentItem1);
            richEditBarController1.BarItems.Add(toggleParagraphLeftToRightItem1);
            richEditBarController1.BarItems.Add(toggleParagraphRightToLeftItem1);
            richEditBarController1.BarItems.Add(toggleParagraphAlignmentLeftItem1);
            richEditBarController1.BarItems.Add(toggleParagraphAlignmentCenterItem1);
            richEditBarController1.BarItems.Add(toggleParagraphAlignmentRightItem1);
            richEditBarController1.BarItems.Add(toggleParagraphAlignmentJustifyItem1);
            richEditBarController1.BarItems.Add(toggleParagraphAlignmentArabicJustifyItem1);
            richEditBarController1.BarItems.Add(toggleParagraphAlignmentJustifyLowItem1);
            richEditBarController1.BarItems.Add(toggleParagraphAlignmentJustifyMediumItem1);
            richEditBarController1.BarItems.Add(toggleParagraphAlignmentJustifyHighItem1);
            richEditBarController1.BarItems.Add(toggleParagraphAlignmentArabicJustifyGroupItem1);
            richEditBarController1.BarItems.Add(toggleParagraphAlignmentDistributeItem1);
            richEditBarController1.BarItems.Add(toggleParagraphAlignmentThaiDistributeItem1);
            richEditBarController1.BarItems.Add(toggleShowWhitespaceItem1);
            richEditBarController1.BarItems.Add(setSingleParagraphSpacingItem1);
            richEditBarController1.BarItems.Add(setSesquialteralParagraphSpacingItem1);
            richEditBarController1.BarItems.Add(setDoubleParagraphSpacingItem1);
            richEditBarController1.BarItems.Add(showLineSpacingFormItem1);
            richEditBarController1.BarItems.Add(addSpacingBeforeParagraphItem1);
            richEditBarController1.BarItems.Add(removeSpacingBeforeParagraphItem1);
            richEditBarController1.BarItems.Add(addSpacingAfterParagraphItem1);
            richEditBarController1.BarItems.Add(removeSpacingAfterParagraphItem1);
            richEditBarController1.BarItems.Add(changeParagraphLineSpacingItem1);
            richEditBarController1.BarItems.Add(changeParagraphBackColorItem1);
            richEditBarController1.BarItems.Add(showParagraphFormItem1);
            richEditBarController1.BarItems.Add(changeStyleItem1);
            richEditBarController1.BarItems.Add(showEditStyleFormItem1);
            richEditBarController1.BarItems.Add(findItem1);
            richEditBarController1.BarItems.Add(replaceItem1);
            richEditBarController1.Control = richEditControl1;
            // 
            // ReAlRichControl
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(richEditControl1);
            Controls.Add(barDockControlLeft);
            Controls.Add(barDockControlRight);
            Controls.Add(barDockControlBottom);
            Controls.Add(barDockControlTop);
            Name = "ReAlRichControl";
            Size = new Size(958, 366);
            ((System.ComponentModel.ISupportInitialize)barManager1).EndInit();
            ((System.ComponentModel.ISupportInitialize)repositoryItemFontEdit1).EndInit();
            ((System.ComponentModel.ISupportInitialize)repositoryItemRichEditFontSizeEdit1).EndInit();
            ((System.ComponentModel.ISupportInitialize)repositoryItemRichEditStyleEdit1).EndInit();
            ((System.ComponentModel.ISupportInitialize)richEditBarController1).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        internal DevExpress.XtraRichEdit.RichEditControl richEditControl1;
        private DevExpress.XtraBars.BarManager barManager1;
        private DevExpress.XtraRichEdit.UI.ClipboardBar clipboardBar1;
        private DevExpress.XtraRichEdit.UI.PasteItem pasteItem1;
        private DevExpress.XtraRichEdit.UI.CutItem cutItem1;
        private DevExpress.XtraRichEdit.UI.CopyItem copyItem1;
        private DevExpress.XtraRichEdit.UI.PasteSpecialItem pasteSpecialItem1;
        private DevExpress.XtraRichEdit.UI.FontBar fontBar1;
        private DevExpress.XtraRichEdit.UI.ChangeFontNameItem changeFontNameItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemFontEdit repositoryItemFontEdit1;
        private DevExpress.XtraRichEdit.UI.ChangeFontSizeItem changeFontSizeItem1;
        private DevExpress.XtraRichEdit.Design.RepositoryItemRichEditFontSizeEdit repositoryItemRichEditFontSizeEdit1;
        private DevExpress.XtraRichEdit.UI.FontSizeIncreaseItem fontSizeIncreaseItem1;
        private DevExpress.XtraRichEdit.UI.FontSizeDecreaseItem fontSizeDecreaseItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontBoldItem toggleFontBoldItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontItalicItem toggleFontItalicItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontUnderlineItem toggleFontUnderlineItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontDoubleUnderlineItem toggleFontDoubleUnderlineItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontStrikeoutItem toggleFontStrikeoutItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontDoubleStrikeoutItem toggleFontDoubleStrikeoutItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontSuperscriptItem toggleFontSuperscriptItem1;
        private DevExpress.XtraRichEdit.UI.ToggleFontSubscriptItem toggleFontSubscriptItem1;
        private DevExpress.XtraRichEdit.UI.ChangeFontColorItem changeFontColorItem1;
        private DevExpress.XtraRichEdit.UI.ChangeFontHighlightColorItem changeFontHighlightColorItem1;
        private DevExpress.XtraRichEdit.UI.ChangeTextCaseItem changeTextCaseItem1;
        private DevExpress.XtraRichEdit.UI.MakeTextUpperCaseItem makeTextUpperCaseItem1;
        private DevExpress.XtraRichEdit.UI.MakeTextLowerCaseItem makeTextLowerCaseItem1;
        private DevExpress.XtraRichEdit.UI.CapitalizeEachWordCaseItem capitalizeEachWordCaseItem1;
        private DevExpress.XtraRichEdit.UI.ToggleTextCaseItem toggleTextCaseItem1;
        private DevExpress.XtraRichEdit.UI.ClearFormattingItem clearFormattingItem1;
        private DevExpress.XtraRichEdit.UI.ShowFontFormItem showFontFormItem1;
        private DevExpress.XtraRichEdit.UI.ParagraphBar paragraphBar1;
        private DevExpress.XtraRichEdit.UI.ToggleBulletedListItem toggleBulletedListItem1;
        private DevExpress.XtraRichEdit.UI.ToggleNumberingListItem toggleNumberingListItem1;
        private DevExpress.XtraRichEdit.UI.ToggleMultiLevelListItem toggleMultiLevelListItem1;
        private DevExpress.XtraRichEdit.UI.DecreaseIndentItem decreaseIndentItem1;
        private DevExpress.XtraRichEdit.UI.IncreaseIndentItem increaseIndentItem1;
        private DevExpress.XtraRichEdit.UI.RtlToggleBulletedListItem rtlToggleBulletedListItem1;
        private DevExpress.XtraRichEdit.UI.RtlToggleNumberingListItem rtlToggleNumberingListItem1;
        private DevExpress.XtraRichEdit.UI.RtlToggleMultiLevelListItem rtlToggleMultiLevelListItem1;
        private DevExpress.XtraRichEdit.UI.RtlDecreaseIndentItem rtlDecreaseIndentItem1;
        private DevExpress.XtraRichEdit.UI.RtlIncreaseIndentItem rtlIncreaseIndentItem1;
        private DevExpress.XtraRichEdit.UI.ToggleParagraphLeftToRightItem toggleParagraphLeftToRightItem1;
        private DevExpress.XtraRichEdit.UI.ToggleParagraphRightToLeftItem toggleParagraphRightToLeftItem1;
        private DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentLeftItem toggleParagraphAlignmentLeftItem1;
        private DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentCenterItem toggleParagraphAlignmentCenterItem1;
        private DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentRightItem toggleParagraphAlignmentRightItem1;
        private DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentJustifyItem toggleParagraphAlignmentJustifyItem1;
        private DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentArabicJustifyGroupItem toggleParagraphAlignmentArabicJustifyGroupItem1;
        private DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentArabicJustifyItem toggleParagraphAlignmentArabicJustifyItem1;
        private DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentJustifyLowItem toggleParagraphAlignmentJustifyLowItem1;
        private DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentJustifyMediumItem toggleParagraphAlignmentJustifyMediumItem1;
        private DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentJustifyHighItem toggleParagraphAlignmentJustifyHighItem1;
        private DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentDistributeItem toggleParagraphAlignmentDistributeItem1;
        private DevExpress.XtraRichEdit.UI.ToggleParagraphAlignmentThaiDistributeItem toggleParagraphAlignmentThaiDistributeItem1;
        private DevExpress.XtraRichEdit.UI.ToggleShowWhitespaceItem toggleShowWhitespaceItem1;
        private DevExpress.XtraRichEdit.UI.ChangeParagraphLineSpacingItem changeParagraphLineSpacingItem1;
        private DevExpress.XtraRichEdit.UI.SetSingleParagraphSpacingItem setSingleParagraphSpacingItem1;
        private DevExpress.XtraRichEdit.UI.SetSesquialteralParagraphSpacingItem setSesquialteralParagraphSpacingItem1;
        private DevExpress.XtraRichEdit.UI.SetDoubleParagraphSpacingItem setDoubleParagraphSpacingItem1;
        private DevExpress.XtraRichEdit.UI.ShowLineSpacingFormItem showLineSpacingFormItem1;
        private DevExpress.XtraRichEdit.UI.AddSpacingBeforeParagraphItem addSpacingBeforeParagraphItem1;
        private DevExpress.XtraRichEdit.UI.RemoveSpacingBeforeParagraphItem removeSpacingBeforeParagraphItem1;
        private DevExpress.XtraRichEdit.UI.AddSpacingAfterParagraphItem addSpacingAfterParagraphItem1;
        private DevExpress.XtraRichEdit.UI.RemoveSpacingAfterParagraphItem removeSpacingAfterParagraphItem1;
        private DevExpress.XtraRichEdit.UI.ChangeParagraphBackColorItem changeParagraphBackColorItem1;
        private DevExpress.XtraRichEdit.UI.ShowParagraphFormItem showParagraphFormItem1;
        private DevExpress.XtraRichEdit.UI.StylesBar stylesBar1;
        private DevExpress.XtraRichEdit.UI.ChangeStyleItem changeStyleItem1;
        private DevExpress.XtraRichEdit.Design.RepositoryItemRichEditStyleEdit repositoryItemRichEditStyleEdit1;
        private DevExpress.XtraRichEdit.UI.ShowEditStyleFormItem showEditStyleFormItem1;
        private DevExpress.XtraRichEdit.UI.EditingBar editingBar1;
        private DevExpress.XtraRichEdit.UI.FindItem findItem1;
        private DevExpress.XtraRichEdit.UI.ReplaceItem replaceItem1;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraRichEdit.UI.RichEditBarController richEditBarController1;
    }
}
