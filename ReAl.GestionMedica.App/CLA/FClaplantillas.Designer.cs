


namespace ReAl.GestionMedica.App.CLA
{
	partial class FClaPlantillas
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FClaPlantillas));
            lblTitulo = new Label();
            lblidctp = new Label();
            cmbidctp = new DevExpress.XtraEditors.LookUpEdit();
            lblnombrecpl = new Label();
            txtnombrecpl = new DevExpress.XtraEditors.TextEdit();
            lbldescripcioncpl = new Label();
            txtdescripcioncpl = new DevExpress.XtraEditors.TextEdit();
            lblaltocpl = new Label();
            nudaltocpl = new DevExpress.XtraEditors.TextEdit();
            lblanchocpl = new Label();
            nudanchocpl = new DevExpress.XtraEditors.TextEdit();
            lblmargensupcpl = new Label();
            nudmargensupcpl = new DevExpress.XtraEditors.TextEdit();
            lblmargeninfcpl = new Label();
            nudmargeninfcpl = new DevExpress.XtraEditors.TextEdit();
            lblmargendercpl = new Label();
            nudmargendercpl = new DevExpress.XtraEditors.TextEdit();
            lblmargenizqcpl = new Label();
            nudmargenizqcpl = new DevExpress.XtraEditors.TextEdit();
            btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            btnPreview = new DevExpress.XtraEditors.SimpleButton();
            btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            label1 = new Label();
            ReAlRichControl1 = new ReAlRichControl();
            ((System.ComponentModel.ISupportInitialize)cmbidctp.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtnombrecpl.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtdescripcioncpl.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)nudaltocpl.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)nudanchocpl.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)nudmargensupcpl.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)nudmargeninfcpl.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)nudmargendercpl.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)nudmargenizqcpl.Properties).BeginInit();
            SuspendLayout();
            // 
            // lblTitulo
            // 
            lblTitulo.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            lblTitulo.Font = new Font("Microsoft Sans Serif", 9.25F, FontStyle.Bold | FontStyle.Underline);
            lblTitulo.Location = new Point(9, 9);
            lblTitulo.Name = "lblTitulo";
            lblTitulo.Size = new Size(554, 53);
            lblTitulo.TabIndex = 0;
            lblTitulo.Text = "Administracion de Plantillas";
            lblTitulo.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // lblidctp
            // 
            lblidctp.AutoSize = true;
            lblidctp.Location = new Point(14, 68);
            lblidctp.Name = "lblidctp";
            lblidctp.Size = new Size(70, 13);
            lblidctp.TabIndex = 1;
            lblidctp.Text = "Tipo Plantilla:";
            // 
            // cmbidctp
            // 
            cmbidctp.Location = new Point(120, 65);
            cmbidctp.Name = "cmbidctp";
            cmbidctp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            cmbidctp.Size = new Size(204, 20);
            cmbidctp.TabIndex = 2;
            // 
            // lblnombrecpl
            // 
            lblnombrecpl.AutoSize = true;
            lblnombrecpl.Location = new Point(14, 93);
            lblnombrecpl.Name = "lblnombrecpl";
            lblnombrecpl.Size = new Size(48, 13);
            lblnombrecpl.TabIndex = 3;
            lblnombrecpl.Text = "Nombre:";
            // 
            // txtnombrecpl
            // 
            txtnombrecpl.Location = new Point(120, 90);
            txtnombrecpl.Name = "txtnombrecpl";
            txtnombrecpl.Properties.MaxLength = 50;
            txtnombrecpl.Size = new Size(204, 20);
            txtnombrecpl.TabIndex = 4;
            // 
            // lbldescripcioncpl
            // 
            lbldescripcioncpl.AutoSize = true;
            lbldescripcioncpl.Location = new Point(400, 93);
            lbldescripcioncpl.Name = "lbldescripcioncpl";
            lbldescripcioncpl.Size = new Size(65, 13);
            lbldescripcioncpl.TabIndex = 5;
            lbldescripcioncpl.Text = "Descripción:";
            // 
            // txtdescripcioncpl
            // 
            txtdescripcioncpl.Location = new Point(506, 90);
            txtdescripcioncpl.Name = "txtdescripcioncpl";
            txtdescripcioncpl.Properties.MaxLength = 500;
            txtdescripcioncpl.Size = new Size(204, 20);
            txtdescripcioncpl.TabIndex = 6;
            // 
            // lblaltocpl
            // 
            lblaltocpl.AutoSize = true;
            lblaltocpl.Location = new Point(14, 118);
            lblaltocpl.Name = "lblaltocpl";
            lblaltocpl.Size = new Size(65, 13);
            lblaltocpl.TabIndex = 7;
            lblaltocpl.Text = "Alto Página:";
            // 
            // nudaltocpl
            // 
            nudaltocpl.Location = new Point(120, 115);
            nudaltocpl.Name = "nudaltocpl";
            nudaltocpl.Properties.Mask.EditMask = "n2";
            nudaltocpl.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            nudaltocpl.Size = new Size(204, 20);
            nudaltocpl.TabIndex = 8;
            // 
            // lblanchocpl
            // 
            lblanchocpl.AutoSize = true;
            lblanchocpl.Location = new Point(400, 118);
            lblanchocpl.Name = "lblanchocpl";
            lblanchocpl.Size = new Size(76, 13);
            lblanchocpl.TabIndex = 9;
            lblanchocpl.Text = "Ancho Página:";
            // 
            // nudanchocpl
            // 
            nudanchocpl.Location = new Point(506, 115);
            nudanchocpl.Name = "nudanchocpl";
            nudanchocpl.Properties.Mask.EditMask = "n2";
            nudanchocpl.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            nudanchocpl.Size = new Size(204, 20);
            nudanchocpl.TabIndex = 10;
            // 
            // lblmargensupcpl
            // 
            lblmargensupcpl.AutoSize = true;
            lblmargensupcpl.Location = new Point(14, 143);
            lblmargensupcpl.Name = "lblmargensupcpl";
            lblmargensupcpl.Size = new Size(72, 13);
            lblmargensupcpl.TabIndex = 11;
            lblmargensupcpl.Text = "Margen Sup.:";
            // 
            // nudmargensupcpl
            // 
            nudmargensupcpl.Location = new Point(120, 140);
            nudmargensupcpl.Name = "nudmargensupcpl";
            nudmargensupcpl.Properties.Mask.EditMask = "n2";
            nudmargensupcpl.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            nudmargensupcpl.Size = new Size(204, 20);
            nudmargensupcpl.TabIndex = 12;
            // 
            // lblmargeninfcpl
            // 
            lblmargeninfcpl.AutoSize = true;
            lblmargeninfcpl.Location = new Point(400, 143);
            lblmargeninfcpl.Name = "lblmargeninfcpl";
            lblmargeninfcpl.Size = new Size(64, 13);
            lblmargeninfcpl.TabIndex = 13;
            lblmargeninfcpl.Text = "Margen Inf.";
            // 
            // nudmargeninfcpl
            // 
            nudmargeninfcpl.Location = new Point(506, 140);
            nudmargeninfcpl.Name = "nudmargeninfcpl";
            nudmargeninfcpl.Properties.Mask.EditMask = "n2";
            nudmargeninfcpl.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            nudmargeninfcpl.Size = new Size(204, 20);
            nudmargeninfcpl.TabIndex = 14;
            // 
            // lblmargendercpl
            // 
            lblmargendercpl.AutoSize = true;
            lblmargendercpl.Location = new Point(14, 169);
            lblmargendercpl.Name = "lblmargendercpl";
            lblmargendercpl.Size = new Size(71, 13);
            lblmargendercpl.TabIndex = 15;
            lblmargendercpl.Text = "Margen Der.:";
            // 
            // nudmargendercpl
            // 
            nudmargendercpl.Location = new Point(120, 166);
            nudmargendercpl.Name = "nudmargendercpl";
            nudmargendercpl.Properties.Mask.EditMask = "n2";
            nudmargendercpl.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            nudmargendercpl.Size = new Size(204, 20);
            nudmargendercpl.TabIndex = 16;
            // 
            // lblmargenizqcpl
            // 
            lblmargenizqcpl.AutoSize = true;
            lblmargenizqcpl.Location = new Point(400, 169);
            lblmargenizqcpl.Name = "lblmargenizqcpl";
            lblmargenizqcpl.Size = new Size(69, 13);
            lblmargenizqcpl.TabIndex = 17;
            lblmargenizqcpl.Text = "Margen Izq.:";
            // 
            // nudmargenizqcpl
            // 
            nudmargenizqcpl.Location = new Point(506, 166);
            nudmargenizqcpl.Name = "nudmargenizqcpl";
            nudmargenizqcpl.Properties.Mask.EditMask = "n2";
            nudmargenizqcpl.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            nudmargenizqcpl.Size = new Size(204, 20);
            nudmargenizqcpl.TabIndex = 18;
            // 
            // btnCancelar
            // 
            btnCancelar.DialogResult = DialogResult.OK;
            btnCancelar.Location = new Point(488, 516);
            btnCancelar.Name = "btnCancelar";
            btnCancelar.Size = new Size(108, 34);
            btnCancelar.TabIndex = 23;
            btnCancelar.Text = "&Cancelar";
            btnCancelar.Click += btnCancelar_Click_1;
            // 
            // btnPreview
            // 
            btnPreview.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Question;
            btnPreview.Appearance.Options.UseBackColor = true;
            btnPreview.DialogResult = DialogResult.OK;
            btnPreview.Location = new Point(12, 516);
            btnPreview.Name = "btnPreview";
            btnPreview.Size = new Size(108, 34);
            btnPreview.TabIndex = 22;
            btnPreview.Text = "&Vista Previa";
            btnPreview.Visible = false;
            btnPreview.Click += btnPreview_Click;
            // 
            // btnAceptar
            // 
            btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            btnAceptar.Appearance.Options.UseBackColor = true;
            btnAceptar.Location = new Point(602, 516);
            btnAceptar.Name = "btnAceptar";
            btnAceptar.Size = new Size(108, 34);
            btnAceptar.TabIndex = 24;
            btnAceptar.Text = "&Registrar";
            btnAceptar.Click += btnAceptar_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Tahoma", 8.25F);
            label1.ForeColor = Color.DarkRed;
            label1.Location = new Point(9, 485);
            label1.Name = "label1";
            label1.Size = new Size(685, 13);
            label1.TabIndex = 21;
            label1.Text = "* Puede utilizar el comodin %PACIENTE% para que ese valor sea reemplazado por el Apellido del Paciente al que se le asignara lel documento";
            // 
            // ReAlRichControl1
            // 
            ReAlRichControl1.Location = new Point(12, 192);
            ReAlRichControl1.Name = "ReAlRichControl1";
            ReAlRichControl1.Size = new Size(698, 290);
            ReAlRichControl1.TabIndex = 19;
            // 
            // FClaPlantillas
            // 
            AutoScaleDimensions = new SizeF(6F, 13F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(721, 562);
            Controls.Add(ReAlRichControl1);
            Controls.Add(btnAceptar);
            Controls.Add(btnPreview);
            Controls.Add(btnCancelar);
            Controls.Add(label1);
            Controls.Add(lblidctp);
            Controls.Add(cmbidctp);
            Controls.Add(lblnombrecpl);
            Controls.Add(txtnombrecpl);
            Controls.Add(lbldescripcioncpl);
            Controls.Add(txtdescripcioncpl);
            Controls.Add(lblaltocpl);
            Controls.Add(nudaltocpl);
            Controls.Add(lblanchocpl);
            Controls.Add(nudanchocpl);
            Controls.Add(lblmargensupcpl);
            Controls.Add(nudmargensupcpl);
            Controls.Add(lblmargeninfcpl);
            Controls.Add(nudmargeninfcpl);
            Controls.Add(lblmargendercpl);
            Controls.Add(nudmargendercpl);
            Controls.Add(lblmargenizqcpl);
            Controls.Add(nudmargenizqcpl);
            Controls.Add(lblTitulo);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            IconOptions.Icon = (Icon)resources.GetObject("FClaPlantillas.IconOptions.Icon");
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FClaPlantillas";
            ShowInTaskbar = false;
            StartPosition = FormStartPosition.CenterParent;
            Text = "Administracion de Plantillas";
            Load += fClaPlantillas_Load;
            ((System.ComponentModel.ISupportInitialize)cmbidctp.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtnombrecpl.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtdescripcioncpl.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)nudaltocpl.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)nudanchocpl.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)nudmargensupcpl.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)nudmargeninfcpl.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)nudmargendercpl.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)nudmargenizqcpl.Properties).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        internal System.Windows.Forms.Label lblTitulo;
		internal System.Windows.Forms.Label lblidctp;
		private DevExpress.XtraEditors.LookUpEdit cmbidctp;
		internal System.Windows.Forms.Label lblnombrecpl;
		private DevExpress.XtraEditors.TextEdit txtnombrecpl;
		internal System.Windows.Forms.Label lbldescripcioncpl;
		private DevExpress.XtraEditors.TextEdit txtdescripcioncpl;
		internal System.Windows.Forms.Label lblaltocpl;
		private DevExpress.XtraEditors.TextEdit nudaltocpl;
		internal System.Windows.Forms.Label lblanchocpl;
		private DevExpress.XtraEditors.TextEdit nudanchocpl;
		internal System.Windows.Forms.Label lblmargensupcpl;
		private DevExpress.XtraEditors.TextEdit nudmargensupcpl;
		internal System.Windows.Forms.Label lblmargeninfcpl;
		private DevExpress.XtraEditors.TextEdit nudmargeninfcpl;
		internal System.Windows.Forms.Label lblmargendercpl;
		private DevExpress.XtraEditors.TextEdit nudmargendercpl;
		internal System.Windows.Forms.Label lblmargenizqcpl;
        private DevExpress.XtraEditors.TextEdit nudmargenizqcpl;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.SimpleButton btnPreview;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        internal System.Windows.Forms.Label label1;
        private ReAlRichControl ReAlRichControl1;
    }
}

