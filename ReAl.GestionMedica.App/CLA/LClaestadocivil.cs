using ReAl.GestionMedica.BackendConnector.Controller;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;

namespace ReAl.GestionMedica.App.CLA
{
    public partial class LClaEstadocivil : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public LClaEstadocivil()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        public void CargarListado()
        {
            var rn = new ClaMaritalController();
            gcClaEstadocivil.DataSource = rn.GetActive();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                //Insertamos el registro
                var rn = new ClaMaritalController();
                var obj = new ClaMarital();
                obj.Description = txtdescripcioncec.Text;
                obj.ApiUsucre = cParametrosApp.AppRestUser.Login;

                rn.Create(obj);
                this.CargarListado();
                this.DialogResult = DialogResult.OK;
                //this.Close();
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void btnAuditoría_Click(object sender, EventArgs e)
        {
            var rn = new ClaMaritalController();

            //Apropiamos los valores del Grid
            int intidcec = int.Parse(gvClaEstadocivil.GetFocusedRowCellValue(ClaMarital.Fields.IdMarital.ToString()).ToString());

            //Obtenemos el objeto
            var obj = rn.GetById(intidcec);

            if (obj != null)
            {
                //Abrimos la AUDITORIA
                var frm = new FAuditaria(obj.ApiUsucre, obj.ApiFeccre, obj.ApiUsumod, obj.ApiFecmod, obj.ApiStatus);
                frm.ShowDialog();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("¿Desea ELIMINAR el registro seleccionado?", "Eliminar registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    //Eliminamos el registro seleccionado
                    var rn = new ClaMaritalController();

                    //Apropiamos los valores del Grid
                    int intidcec = int.Parse(gvClaEstadocivil.GetFocusedRowCellValue(ClaMarital.Fields.IdMarital.ToString()).ToString());

                    //Obtenemos el objeto
                    var obj = rn.GetById(intidcec);

                    if (obj != null)
                    {
                        rn.Delete(obj.IdMarital);
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void fClaEstadocivil_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            CargarListado();
        }

        private void gcClaEstadocivil_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (MessageBox.Show("¿Desea ELIMINAR el registro seleccionado?", "Eliminar registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    //Eliminamos el registro seleccionado
                    var rn = new ClaMaritalController();

                    //Apropiamos los valores del Grid
                    int intidcec = int.Parse(gvClaEstadocivil.GetFocusedRowCellValue(ClaMarital.Fields.IdMarital.ToString()).ToString());

                    //Obtenemos el objeto
                    var obj = rn.GetById(intidcec);

                    if (obj != null)
                    {
                        rn.Delete(obj.IdMarital);
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void gvClaEstadocivil_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            //gvClaEstadocivil.SetFocusedRowCellValue(EntClaEstadocivil.Fields.usucre.ToString(),"postgres");
        }

        private void gvClaEstadocivil_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            //Eliminamos el registro seleccionado
            var rn = new ClaMaritalController();

            //Apropiamos los valores del Grid
            int intidcec = int.Parse(gvClaEstadocivil.GetFocusedRowCellValue(ClaMarital.Fields.IdMarital.ToString()).ToString());

            var objExistente = rn.GetById(intidcec);

            if (objExistente != null)
            {
                objExistente.Description = gvClaEstadocivil.GetFocusedRowCellValue(ClaMarital.Fields.Description.ToString()).ToString();

                objExistente.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                objExistente.ApiUsumod = cParametrosApp.AppRestUser.Login;

                //Actualizamos el registro
                rn.Update(objExistente);
                this.CargarListado();
            }
        }

        #endregion Methods
    }
}