using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Controller;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
using ReAl.GestionMedica.BackendConnector.Utils;
using ReAl.GestionMedica.Class;

namespace ReAl.GestionMedica.App.CLA
{
    public partial class LClaTiposangre : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public LClaTiposangre()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        public void CargarListado()
        {
            var rn = new ClaBloodtypesController();
            var list = rn.GetActive();
            gcClaTiposangre.DataSource = list;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                //Insertamos el registro
                var service = new ClaBloodtypesController();
                var obj = new ClaBloodtypes();
                obj.Description = txtdescripcioncts.Text;
                if (service.Create(obj))
                {
                    this.CargarListado();
                    this.DialogResult = DialogResult.OK;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void btnAuditoría_Click(object sender, EventArgs e)
        {
            //Apropiamos los valores del Grid
            int intidcts = int.Parse(gvClaTiposangre.GetFocusedRowCellValue(ClaBloodtypes.Fields.IdBloodTypes.ToString()).ToString());

            //Obtenemos el objeto
            var rn = new ClaBloodtypesController();
            var obj = rn.GetById(intidcts);

            if (obj != null)
            {
                //Abrimos la AUDITORIA
                var frm = new FAuditaria(obj.ApiUsucre, obj.ApiFeccre, obj.ApiUsumod, obj.ApiFecmod, obj.ApiStatus);
                frm.ShowDialog();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("¿Desea ELIMINAR el registro seleccionado?", "Eliminar registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    //Eliminamos el registro seleccionado
                    var rn = new ClaBloodtypesController();

                    //Apropiamos los valores del Grid
                    int intidcts = int.Parse(gvClaTiposangre.GetFocusedRowCellValue(ClaBloodtypes.Fields.IdBloodTypes.ToString()).ToString());

                    //Obtenemos el objeto
                    var obj = rn.GetById(intidcts);

                    if (obj != null)
                    {
                        rn.Delete(obj.IdBloodTypes);
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void fClaTiposangre_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            CargarListado();
        }

        private void gcClaTiposangre_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                //Eliminamos el registro seleccionado
                var rn = new ClaBloodtypesController();

                //Apropiamos los valores del Grid
                int intidcts = int.Parse(gvClaTiposangre.GetFocusedRowCellValue(ClaBloodtypes.Fields.IdBloodTypes.ToString()).ToString());

                //Obtenemos el objeto
                var obj = rn.GetById(intidcts);

                if (obj != null)
                {
                    rn.Delete(obj.IdBloodTypes);
                    this.CargarListado();
                }
            }
        }

        private void gvClaTiposangre_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            //gvClaTiposangre.SetFocusedRowCellValue(EntClaTiposangre.Fields.usucre.ToString(),"postgres");
        }

        private void gvClaTiposangre_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            //Eliminamos el registro seleccionado
            var rn = new ClaBloodtypesController();

            //Apropiamos los valores del Grid
            int intidcts = int.Parse(gvClaTiposangre.GetFocusedRowCellValue(ClaBloodtypes.Fields.IdBloodTypes.ToString()).ToString());

            var objExistente = rn.GetById(intidcts);

            if (objExistente != null)
            {
                objExistente.Description = gvClaTiposangre.GetFocusedRowCellValue(ClaBloodtypes.Fields.Description.ToString()).ToString();
                objExistente.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                objExistente.ApiUsumod = cParametrosApp.AppRestUser.Login;

                //Actualizamos el registro
                rn.Update(objExistente);
                this.CargarListado();
            }
        }

        #endregion Methods
    }
}