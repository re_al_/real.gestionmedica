


namespace ReAl.GestionMedica.App.CLA
{
	partial class LClaTipoplantillas
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.gcClaTipoplantillas = new DevExpress.XtraGrid.GridControl();
            this.gvClaTipoplantillas = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblnombrectp = new System.Windows.Forms.Label();
            this.txtnombrectp = new DevExpress.XtraEditors.TextEdit();
            this.lbldescripcionctp = new System.Windows.Forms.Label();
            this.txtdescripcionctp = new DevExpress.XtraEditors.TextEdit();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcClaTipoplantillas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvClaTipoplantillas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnombrectp.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcionctp.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcClaTipoplantillas
            // 
            this.gcClaTipoplantillas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcClaTipoplantillas.Location = new System.Drawing.Point(0, 190);
            this.gcClaTipoplantillas.MainView = this.gvClaTipoplantillas;
            this.gcClaTipoplantillas.Name = "gcClaTipoplantillas";
            this.gcClaTipoplantillas.Size = new System.Drawing.Size(784, 373);
            this.gcClaTipoplantillas.TabIndex = 7;
            this.gcClaTipoplantillas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvClaTipoplantillas});
            // 
            // gvClaTipoplantillas
            // 
            this.gvClaTipoplantillas.GridControl = this.gcClaTipoplantillas;
            this.gvClaTipoplantillas.Name = "gvClaTipoplantillas";
            this.gvClaTipoplantillas.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvClaTipoplantillas.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvClaTipoplantillas.OptionsBehavior.Editable = false;
            this.gvClaTipoplantillas.OptionsBehavior.ReadOnly = true;
            this.gvClaTipoplantillas.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gvClaTipoplantillas_InitNewRow);
            this.gvClaTipoplantillas.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gvClaTipoplantillas_RowUpdated);
            this.gvClaTipoplantillas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gcClaTipoplantillas_KeyDown);
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblTitulo.Location = new System.Drawing.Point(12, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(617, 53);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Administracion de Tipos de Plantilla";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblnombrectp
            // 
            this.lblnombrectp.AutoSize = true;
            this.lblnombrectp.Location = new System.Drawing.Point(14, 103);
            this.lblnombrectp.Name = "lblnombrectp";
            this.lblnombrectp.Size = new System.Drawing.Size(48, 13);
            this.lblnombrectp.TabIndex = 1;
            this.lblnombrectp.Text = "Nombre:";
            // 
            // txtnombrectp
            // 
            this.txtnombrectp.Location = new System.Drawing.Point(120, 100);
            this.txtnombrectp.Name = "txtnombrectp";
            this.txtnombrectp.Properties.MaxLength = 100;
            this.txtnombrectp.Size = new System.Drawing.Size(204, 20);
            this.txtnombrectp.TabIndex = 2;
            // 
            // lbldescripcionctp
            // 
            this.lbldescripcionctp.AutoSize = true;
            this.lbldescripcionctp.Location = new System.Drawing.Point(14, 128);
            this.lbldescripcionctp.Name = "lbldescripcionctp";
            this.lbldescripcionctp.Size = new System.Drawing.Size(65, 13);
            this.lbldescripcionctp.TabIndex = 3;
            this.lbldescripcionctp.Text = "Descripcion:";
            // 
            // txtdescripcionctp
            // 
            this.txtdescripcionctp.Location = new System.Drawing.Point(120, 125);
            this.txtdescripcionctp.Name = "txtdescripcionctp";
            this.txtdescripcionctp.Properties.MaxLength = 1000;
            this.txtdescripcionctp.Size = new System.Drawing.Size(204, 20);
            this.txtdescripcionctp.TabIndex = 4;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnAceptar.Appearance.Options.UseBackColor = true;
            this.btnAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAceptar.Location = new System.Drawing.Point(126, 150);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(108, 34);
            this.btnAceptar.TabIndex = 6;
            this.btnAceptar.Text = "&Registrar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Location = new System.Drawing.Point(12, 150);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(108, 34);
            this.btnCancelar.TabIndex = 5;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // LClaTipoplantillas
            // 
            this.AcceptButton = this.btnAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lblnombrectp);
            this.Controls.Add(this.txtnombrectp);
            this.Controls.Add(this.lbldescripcionctp);
            this.Controls.Add(this.txtdescripcionctp);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.gcClaTipoplantillas);
            this.Name = "LClaTipoplantillas";
            this.Text = "Administracion de clatipoplantillas";
            this.Load += new System.EventHandler(this.fClaTipoplantillas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcClaTipoplantillas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvClaTipoplantillas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtnombrectp.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcionctp.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		
		#endregion
		
		private DevExpress.XtraGrid.GridControl gcClaTipoplantillas;
		private DevExpress.XtraGrid.Views.Grid.GridView gvClaTipoplantillas;
        internal System.Windows.Forms.Label lblTitulo;
		internal System.Windows.Forms.Label lblnombrectp;
		private DevExpress.XtraEditors.TextEdit txtnombrectp;
		internal System.Windows.Forms.Label lbldescripcionctp;
		private DevExpress.XtraEditors.TextEdit txtdescripcionctp;
		private DevExpress.XtraEditors.SimpleButton btnAceptar;
		private DevExpress.XtraEditors.SimpleButton btnCancelar;
	}
}

