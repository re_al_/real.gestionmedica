


namespace ReAl.GestionMedica.App.CLA
{
	partial class LClaEstadocivil
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.gcClaEstadocivil = new DevExpress.XtraGrid.GridControl();
            this.gvClaEstadocivil = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lbldescripcioncec = new System.Windows.Forms.Label();
            this.txtdescripcioncec = new DevExpress.XtraEditors.TextEdit();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnAuditoría = new DevExpress.XtraEditors.SimpleButton();
            this.btnEliminar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcClaEstadocivil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvClaEstadocivil)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcioncec.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcClaEstadocivil
            // 
            this.gcClaEstadocivil.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcClaEstadocivil.Location = new System.Drawing.Point(0, 165);
            this.gcClaEstadocivil.MainView = this.gvClaEstadocivil;
            this.gcClaEstadocivil.Name = "gcClaEstadocivil";
            this.gcClaEstadocivil.Size = new System.Drawing.Size(784, 345);
            this.gcClaEstadocivil.TabIndex = 5;
            this.gcClaEstadocivil.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvClaEstadocivil});
            // 
            // gvClaEstadocivil
            // 
            this.gvClaEstadocivil.GridControl = this.gcClaEstadocivil;
            this.gvClaEstadocivil.Name = "gvClaEstadocivil";
            this.gvClaEstadocivil.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gvClaEstadocivil_InitNewRow);
            this.gvClaEstadocivil.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gvClaEstadocivil_RowUpdated);
            this.gvClaEstadocivil.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gcClaEstadocivil_KeyDown);
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblTitulo.Location = new System.Drawing.Point(12, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(617, 53);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Administracion de Estado Civil";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbldescripcioncec
            // 
            this.lbldescripcioncec.AutoSize = true;
            this.lbldescripcioncec.Location = new System.Drawing.Point(12, 102);
            this.lbldescripcioncec.Name = "lbldescripcioncec";
            this.lbldescripcioncec.Size = new System.Drawing.Size(65, 13);
            this.lbldescripcioncec.TabIndex = 1;
            this.lbldescripcioncec.Text = "Descripcion:";
            // 
            // txtdescripcioncec
            // 
            this.txtdescripcioncec.Location = new System.Drawing.Point(118, 99);
            this.txtdescripcioncec.Name = "txtdescripcioncec";
            this.txtdescripcioncec.Properties.MaxLength = 25;
            this.txtdescripcioncec.Size = new System.Drawing.Size(204, 20);
            this.txtdescripcioncec.TabIndex = 2;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnAceptar.Appearance.Options.UseBackColor = true;
            this.btnAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAceptar.Location = new System.Drawing.Point(126, 125);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(108, 34);
            this.btnAceptar.TabIndex = 4;
            this.btnAceptar.Text = "&Registrar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Location = new System.Drawing.Point(12, 125);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(108, 34);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAuditoría
            // 
            this.btnAuditoría.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAuditoría.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAuditoría.Location = new System.Drawing.Point(550, 516);
            this.btnAuditoría.Name = "btnAuditoría";
            this.btnAuditoría.Size = new System.Drawing.Size(108, 34);
            this.btnAuditoría.TabIndex = 6;
            this.btnAuditoría.Text = "&Auditoría";
            this.btnAuditoría.Click += new System.EventHandler(this.btnAuditoría_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEliminar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Danger;
            this.btnEliminar.Appearance.Options.UseBackColor = true;
            this.btnEliminar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnEliminar.Location = new System.Drawing.Point(664, 516);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(108, 34);
            this.btnEliminar.TabIndex = 7;
            this.btnEliminar.Text = "&Eliminar";
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // LClaEstadocivil
            // 
            this.AcceptButton = this.btnAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.btnAuditoría);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lbldescripcioncec);
            this.Controls.Add(this.txtdescripcioncec);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.gcClaEstadocivil);
            this.Name = "LClaEstadocivil";
            this.Text = "Administracion de Estado Civil";
            this.Load += new System.EventHandler(this.fClaEstadocivil_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcClaEstadocivil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvClaEstadocivil)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcioncec.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		
		#endregion
		
		private DevExpress.XtraGrid.GridControl gcClaEstadocivil;
		private DevExpress.XtraGrid.Views.Grid.GridView gvClaEstadocivil;
        internal System.Windows.Forms.Label lblTitulo;
		internal System.Windows.Forms.Label lbldescripcioncec;
		private DevExpress.XtraEditors.TextEdit txtdescripcioncec;
		private DevExpress.XtraEditors.SimpleButton btnAceptar;
		private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.SimpleButton btnAuditoría;
        private DevExpress.XtraEditors.SimpleButton btnEliminar;
	}
}

