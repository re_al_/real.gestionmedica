using System.Text;
using DevExpress.XtraRichEdit;
using ReAl.GestionMedica.App.RPT;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;
using ReAl.Utils;

namespace ReAl.GestionMedica.App.CLA
{
    public partial class FClaPlantillas : DevExpress.XtraEditors.XtraForm
    {
        #region Fields

        private ClaTemplates _myObj = null;

        #endregion Fields

        #region Constructors

        public FClaPlantillas()
        {
            InitializeComponent();
        }

        public FClaPlantillas(ClaTemplates obj)
        {
            InitializeComponent();

            _myObj = obj;
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var rn = new ClaTemplatesController();

                if (_myObj == null)
                {
                    ReAlRichControl1.richEditControl1.SaveDocument("temp.rtf", DocumentFormat.Rtf);
                    //Insertamos el registro
                    _myObj = new ClaTemplates();
                    _myObj.IdTemplatesType = int.Parse(cmbidctp.EditValue.ToString());
                    _myObj.Name = txtnombrecpl.Text;
                    _myObj.Description = txtdescripcioncpl.Text;
                    _myObj.Height = decimal.Parse(nudaltocpl.EditValue.ToString());
                    _myObj.Width = decimal.Parse(nudanchocpl.EditValue.ToString());
                    _myObj.MarginTop = decimal.Parse(nudmargensupcpl.EditValue.ToString());
                    _myObj.MarginBottom = decimal.Parse(nudmargeninfcpl.EditValue.ToString());
                    _myObj.MarginRight = decimal.Parse(nudmargendercpl.EditValue.ToString());
                    _myObj.MarginLeft = decimal.Parse(nudmargenizqcpl.EditValue.ToString());
                    _myObj.TextFormat = ReAlRichControl1.richEditControl1.Document.Text;
                    _myObj.HtmlFormat = ReAlRichControl1.richEditControl1.Document.HtmlText;
                    _myObj.RtfFormat = Convert.ToBase64String(cFuncionesImagenes.ImageReadBinaryFile("temp.rtf"));
                    _myObj.ApiUsucre = cParametrosApp.AppRestUser.Login;

                    rn.Create(_myObj);
                    bProcede = true;
                }
                else
                {
                    ReAlRichControl1.richEditControl1.SaveDocument("temp.rtf", DocumentFormat.Rtf);

                    //Actualizamos
                    _myObj.Name = txtnombrecpl.Text;
                    _myObj.Description = txtdescripcioncpl.Text;
                    _myObj.Height = decimal.Parse(nudaltocpl.EditValue.ToString());
                    _myObj.Width = decimal.Parse(nudanchocpl.EditValue.ToString());
                    _myObj.MarginTop = decimal.Parse(nudmargensupcpl.EditValue.ToString());
                    _myObj.MarginBottom = decimal.Parse(nudmargeninfcpl.EditValue.ToString());
                    _myObj.MarginRight = decimal.Parse(nudmargendercpl.EditValue.ToString());
                    _myObj.MarginLeft = decimal.Parse(nudmargenizqcpl.EditValue.ToString());
                    _myObj.TextFormat = ReAlRichControl1.richEditControl1.Document.Text;
                    _myObj.HtmlFormat = ReAlRichControl1.richEditControl1.Document.HtmlText;
                    _myObj.RtfFormat = Convert.ToBase64String(cFuncionesImagenes.ImageReadBinaryFile("temp.rtf"));
                    _myObj.ApiUsumod = cParametrosApp.AppRestUser.Login;
                    _myObj.ApiTransaction = CApi.Transaccion.UPDATE.ToString();

                    rn.Update(_myObj);
                    bProcede = true;
                }
            }
            catch (Exception exp)
            {
                bProcede = false;
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }

            if (bProcede)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnCancelar_Click_1(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            var frm = new RptViewerCla(_myObj);
            frm.ShowDialog();
        }

        private void CargarCmbidctp()
        {
            try
            {
                var rn = new ClaTemplatestypeController();
                var dt = rn.GetActive(); ;
                cmbidctp.Properties.DataSource = dt;
                if (dt.Count > 0)
                {
                    cmbidctp.Properties.ValueMember = ClaTemplatestype.Fields.IdTemplatesType.ToString();
                    cmbidctp.Properties.DisplayMember = ClaTemplatestype.Fields.Description.ToString();
                    cmbidctp.Properties.PopulateColumns();
                    cmbidctp.Properties.ShowHeader = false;
                    cmbidctp.Properties.Columns[ClaTemplatestype.Fields.IdTemplatesType.ToString()].Visible = false;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        private void CargarDatos()
        {
            try
            {
                if (_myObj != null)
                {
                    cmbidctp.EditValue = _myObj.IdTemplatesType;
                    txtnombrecpl.Text = _myObj.Name;
                    txtdescripcioncpl.Text = _myObj.Description;
                    nudaltocpl.EditValue = _myObj.Height;
                    nudanchocpl.EditValue = _myObj.Width;
                    nudmargensupcpl.EditValue = _myObj.MarginTop;
                    nudmargeninfcpl.EditValue = _myObj.MarginBottom;
                    nudmargendercpl.EditValue = _myObj.MarginRight;
                    nudmargenizqcpl.EditValue = _myObj.MarginLeft;

                    var enc = new ASCIIEncoding();
                    //txtcuerpocpl.Rtf = enc.GetString(_myObj.RtfFormatArrayByte, 0, _myObj.RtfFormat.Length);
                    ReAlRichControl1.richEditControl1.Document.HtmlText = _myObj.HtmlFormat;
                    ReAlRichControl1.richEditControl1.ActiveViewType = RichEditViewType.Simple;

                    //txtcuerpocpl.Text = _myObj.textocpl;

                    btnPreview.Visible = true;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void fClaPlantillas_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            CargarCmbidctp();

            if (_myObj != null)
            {
                CargarDatos();
            }
        }

        #endregion Methods
    }
}