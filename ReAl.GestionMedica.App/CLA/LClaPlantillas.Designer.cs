


namespace ReAl.GestionMedica.App.CLA
{
	partial class LClaPlantillas
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.gcClaPlantillas = new DevExpress.XtraGrid.GridControl();
            this.gvClaPlantillas = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.btnNuevo = new DevExpress.XtraEditors.SimpleButton();
            this.lblidctp = new System.Windows.Forms.Label();
            this.cmbidctp = new DevExpress.XtraEditors.LookUpEdit();
            this.btnModificar = new DevExpress.XtraEditors.SimpleButton();
            this.btnEliminar = new DevExpress.XtraEditors.SimpleButton();
            this.btnAuditoría = new DevExpress.XtraEditors.SimpleButton();
            this.btnPreview = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcClaPlantillas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvClaPlantillas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbidctp.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcClaPlantillas
            // 
            this.gcClaPlantillas.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcClaPlantillas.Location = new System.Drawing.Point(0, 126);
            this.gcClaPlantillas.MainView = this.gvClaPlantillas;
            this.gcClaPlantillas.Name = "gcClaPlantillas";
            this.gcClaPlantillas.Size = new System.Drawing.Size(784, 384);
            this.gcClaPlantillas.TabIndex = 3;
            this.gcClaPlantillas.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvClaPlantillas});
            this.gcClaPlantillas.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.gcClaPlantillas_MouseDoubleClick);
            // 
            // gvClaPlantillas
            // 
            this.gvClaPlantillas.GridControl = this.gcClaPlantillas;
            this.gvClaPlantillas.Name = "gvClaPlantillas";
            this.gvClaPlantillas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gcClaPlantillas_KeyDown);
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblTitulo.Location = new System.Drawing.Point(12, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(617, 53);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Administracion de Plantillas";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnNuevo
            // 
            this.btnNuevo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNuevo.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnNuevo.Appearance.Options.UseBackColor = true;
            this.btnNuevo.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnNuevo.Location = new System.Drawing.Point(664, 516);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(108, 34);
            this.btnNuevo.TabIndex = 8;
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // lblidctp
            // 
            this.lblidctp.AutoSize = true;
            this.lblidctp.Location = new System.Drawing.Point(14, 103);
            this.lblidctp.Name = "lblidctp";
            this.lblidctp.Size = new System.Drawing.Size(70, 13);
            this.lblidctp.TabIndex = 1;
            this.lblidctp.Text = "Tipo Plantilla:";
            // 
            // cmbidctp
            // 
            this.cmbidctp.Location = new System.Drawing.Point(90, 100);
            this.cmbidctp.Name = "cmbidctp";
            this.cmbidctp.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbidctp.Size = new System.Drawing.Size(204, 20);
            this.cmbidctp.TabIndex = 2;
            this.cmbidctp.EditValueChanged += new System.EventHandler(this.cmbidctp_EditValueChanged);
            // 
            // btnModificar
            // 
            this.btnModificar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnModificar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Primary;
            this.btnModificar.Appearance.Options.UseBackColor = true;
            this.btnModificar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnModificar.Location = new System.Drawing.Point(550, 516);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(108, 34);
            this.btnModificar.TabIndex = 7;
            this.btnModificar.Text = "&Modificar";
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEliminar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Danger;
            this.btnEliminar.Appearance.Options.UseBackColor = true;
            this.btnEliminar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnEliminar.Location = new System.Drawing.Point(436, 516);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(108, 34);
            this.btnEliminar.TabIndex = 6;
            this.btnEliminar.Text = "&Eliminar";
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnAuditoría
            // 
            this.btnAuditoría.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAuditoría.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAuditoría.Location = new System.Drawing.Point(322, 516);
            this.btnAuditoría.Name = "btnAuditoría";
            this.btnAuditoría.Size = new System.Drawing.Size(108, 34);
            this.btnAuditoría.TabIndex = 5;
            this.btnAuditoría.Text = "&Auditoría";
            this.btnAuditoría.Click += new System.EventHandler(this.btnAuditoría_Click);
            // 
            // btnPreview
            // 
            this.btnPreview.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPreview.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Question;
            this.btnPreview.Appearance.Options.UseBackColor = true;
            this.btnPreview.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnPreview.Location = new System.Drawing.Point(12, 516);
            this.btnPreview.Name = "btnPreview";
            this.btnPreview.Size = new System.Drawing.Size(108, 34);
            this.btnPreview.TabIndex = 4;
            this.btnPreview.Text = "&Vista Previa";
            this.btnPreview.Visible = false;
            this.btnPreview.Click += new System.EventHandler(this.btnPreview_Click);
            // 
            // LClaPlantillas
            // 
            this.AcceptButton = this.btnNuevo;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.btnPreview);
            this.Controls.Add(this.btnAuditoría);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.lblidctp);
            this.Controls.Add(this.cmbidctp);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.gcClaPlantillas);
            this.Name = "LClaPlantillas";
            this.Text = "Administracion de Plantillas";
            this.Load += new System.EventHandler(this.lClaPlantillas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcClaPlantillas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvClaPlantillas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbidctp.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		
		#endregion
		
		private DevExpress.XtraGrid.GridControl gcClaPlantillas;
		private DevExpress.XtraGrid.Views.Grid.GridView gvClaPlantillas;
        internal System.Windows.Forms.Label lblTitulo;
        private DevExpress.XtraEditors.SimpleButton btnNuevo;
        internal System.Windows.Forms.Label lblidctp;
        private DevExpress.XtraEditors.LookUpEdit cmbidctp;
        private DevExpress.XtraEditors.SimpleButton btnModificar;
        private DevExpress.XtraEditors.SimpleButton btnEliminar;
        private DevExpress.XtraEditors.SimpleButton btnAuditoría;
        private DevExpress.XtraEditors.SimpleButton btnPreview;
	}
}

