


namespace ReAl.GestionMedica.App.CLA
{
	partial class LClaTiposangre
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.gcClaTiposangre = new DevExpress.XtraGrid.GridControl();
            this.gvClaTiposangre = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lbldescripcioncts = new System.Windows.Forms.Label();
            this.txtdescripcioncts = new DevExpress.XtraEditors.TextEdit();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnAuditoría = new DevExpress.XtraEditors.SimpleButton();
            this.btnEliminar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcClaTiposangre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvClaTiposangre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcioncts.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gcClaTiposangre
            // 
            this.gcClaTiposangre.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcClaTiposangre.Location = new System.Drawing.Point(0, 165);
            this.gcClaTiposangre.MainView = this.gvClaTiposangre;
            this.gcClaTiposangre.Name = "gcClaTiposangre";
            this.gcClaTiposangre.Size = new System.Drawing.Size(784, 345);
            this.gcClaTiposangre.TabIndex = 5;
            this.gcClaTiposangre.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvClaTiposangre});
            // 
            // gvClaTiposangre
            // 
            this.gvClaTiposangre.GridControl = this.gcClaTiposangre;
            this.gvClaTiposangre.Name = "gvClaTiposangre";
            this.gvClaTiposangre.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gvClaTiposangre_InitNewRow);
            this.gvClaTiposangre.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gvClaTiposangre_RowUpdated);
            this.gvClaTiposangre.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gcClaTiposangre_KeyDown);
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblTitulo.Location = new System.Drawing.Point(12, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(617, 53);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Administracion de Tipos de Sangre";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbldescripcioncts
            // 
            this.lbldescripcioncts.AutoSize = true;
            this.lbldescripcioncts.Location = new System.Drawing.Point(12, 103);
            this.lbldescripcioncts.Name = "lbldescripcioncts";
            this.lbldescripcioncts.Size = new System.Drawing.Size(65, 13);
            this.lbldescripcioncts.TabIndex = 1;
            this.lbldescripcioncts.Text = "Descripción:";
            // 
            // txtdescripcioncts
            // 
            this.txtdescripcioncts.Location = new System.Drawing.Point(118, 100);
            this.txtdescripcioncts.Name = "txtdescripcioncts";
            this.txtdescripcioncts.Properties.MaxLength = 25;
            this.txtdescripcioncts.Size = new System.Drawing.Size(204, 20);
            this.txtdescripcioncts.TabIndex = 2;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnAceptar.Appearance.Options.UseBackColor = true;
            this.btnAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAceptar.Location = new System.Drawing.Point(126, 125);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(108, 34);
            this.btnAceptar.TabIndex = 4;
            this.btnAceptar.Text = "&Registrar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Location = new System.Drawing.Point(12, 125);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(108, 34);
            this.btnCancelar.TabIndex = 3;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAuditoría
            // 
            this.btnAuditoría.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAuditoría.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAuditoría.Location = new System.Drawing.Point(550, 516);
            this.btnAuditoría.Name = "btnAuditoría";
            this.btnAuditoría.Size = new System.Drawing.Size(108, 34);
            this.btnAuditoría.TabIndex = 6;
            this.btnAuditoría.Text = "&Auditoría";
            this.btnAuditoría.Click += new System.EventHandler(this.btnAuditoría_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEliminar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Danger;
            this.btnEliminar.Appearance.Options.UseBackColor = true;
            this.btnEliminar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnEliminar.Location = new System.Drawing.Point(664, 516);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(108, 34);
            this.btnEliminar.TabIndex = 7;
            this.btnEliminar.Text = "&Eliminar";
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // LClaTiposangre
            // 
            this.AcceptButton = this.btnAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.btnAuditoría);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lbldescripcioncts);
            this.Controls.Add(this.txtdescripcioncts);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.gcClaTiposangre);
            this.Name = "LClaTiposangre";
            this.Text = "Administracion de clatiposangre";
            this.Load += new System.EventHandler(this.fClaTiposangre_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcClaTiposangre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvClaTiposangre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdescripcioncts.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		
		#endregion
		
		private DevExpress.XtraGrid.GridControl gcClaTiposangre;
		private DevExpress.XtraGrid.Views.Grid.GridView gvClaTiposangre;
        internal System.Windows.Forms.Label lblTitulo;
		internal System.Windows.Forms.Label lbldescripcioncts;
		private DevExpress.XtraEditors.TextEdit txtdescripcioncts;
		private DevExpress.XtraEditors.SimpleButton btnAceptar;
		private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.SimpleButton btnAuditoría;
        private DevExpress.XtraEditors.SimpleButton btnEliminar;
	}
}

