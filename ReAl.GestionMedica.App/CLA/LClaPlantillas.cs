using System.Collections;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraRichEdit;
using ReAl.GestionMedica.App.RPT;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;

namespace ReAl.GestionMedica.App.CLA
{
    public partial class LClaPlantillas : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public LClaPlantillas()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        public void CargarListado()
        {
            try
            {
                var rn = new ClaTemplatesController();
                var dt = rn.GetByTemplateTypeId(int.Parse(cmbidctp.EditValue.ToString()));
                gcClaPlantillas.DataSource = dt;
                if (dt.Count > 0)
                {
                    gvClaPlantillas.PopulateColumns();
                    gvClaPlantillas.OptionsBehavior.Editable = false;
                    gvClaPlantillas.Columns[ClaTemplates.Fields.IdTemplates.ToString()].Visible = false;

                    RepositoryItemRichTextEdit editorForRichEditDisplay = new RepositoryItemRichTextEdit();
                    editorForRichEditDisplay.DocumentFormat = DocumentFormat.Html;
                    gcClaPlantillas.RepositoryItems.Add(editorForRichEditDisplay);
                    gvClaPlantillas.Columns[ClaTemplates.Fields.HtmlFormat.ToString()].ColumnEdit = editorForRichEditDisplay;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void EliminarRegistro()
        {
            try
            {
                if (gvClaPlantillas.RowCount <= 0) return;
                if (MessageBox.Show("¿Desea ELIMINAR el registro seleccionado?", "Eliminar registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    //Eliminamos el registro seleccionado
                    var rn = new ClaTemplatesController();

                    //Apropiamos los valores del Grid
                    int intidcpl = int.Parse(
                        gvClaPlantillas.GetFocusedRowCellValue(ClaTemplates.Fields.IdTemplates.ToString()).ToString());

                    //Obtenemos el objeto
                    var obj = rn.GetById(intidcpl);

                    if (obj != null)
                    {
                        rn.Delete(obj.IdTemplates);
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void ModificarRegistro()
        {
            try
            {
                if (gvClaPlantillas.RowCount <= 0) return;
                var rn = new ClaTemplatesController();

                //Apropiamos los valores del Grid
                int intidcpl = int.Parse(gvClaPlantillas.GetFocusedRowCellValue(ClaTemplates.Fields.IdTemplates.ToString()).ToString());

                //Obtenemos el objeto
                var obj = rn.GetById(intidcpl);

                if (obj != null)
                {
                    //Abrimos la AUDITORIA
                    var frm = new FClaPlantillas(obj);
                    var resultado = frm.ShowDialog();
                    if (resultado == DialogResult.OK)
                    {
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void btnAuditoría_Click(object sender, EventArgs e)
        {
            if (gvClaPlantillas.RowCount <= 0) return;
            var rn = new ClaTemplatesController();

            //Apropiamos los valores del Grid
            int intidcpl = int.Parse(gvClaPlantillas.GetFocusedRowCellValue(ClaTemplates.Fields.IdTemplates.ToString()).ToString());

            //Obtenemos el objeto
            var obj = rn.GetById(intidcpl);

            if (obj != null)
            {
                //Abrimos la AUDITORIA
                var frm = new FAuditaria(obj.ApiUsucre, obj.ApiFeccre, obj.ApiUsumod, obj.ApiFecmod, obj.ApiStatus);
                frm.ShowDialog();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarRegistro();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            ModificarRegistro();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frm = new FClaPlantillas();
            var resultado = frm.ShowDialog();
            if (resultado == DialogResult.OK)
            {
                this.CargarListado();
            }
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            var rn = new ClaTemplatesController();

            //Apropiamos los valores del Grid
            int intidcpl = int.Parse(gvClaPlantillas.GetFocusedRowCellValue(ClaTemplates.Fields.IdTemplates.ToString()).ToString());

            //Obtenemos el objeto
            var obj = rn.GetById(intidcpl);

            if (obj != null)
            {
                //Abrimos la AUDITORIA
                var frm = new RptViewerCla(obj);
                frm.ShowDialog();
            }
        }

        private void CargarCmbidctp()
        {
            try
            {
                var rn = new ClaTemplatestypeController();
                var dt = rn.GetActive();
                cmbidctp.Properties.DataSource = dt;
                if (dt.Count > 0)
                {
                    cmbidctp.Properties.ValueMember = ClaTemplatestype.Fields.IdTemplatesType.ToString();
                    cmbidctp.Properties.DisplayMember = ClaTemplatestype.Fields.Name.ToString();
                    cmbidctp.Properties.PopulateColumns();
                    cmbidctp.Properties.ShowHeader = false;
                    cmbidctp.Properties.Columns[ClaTemplatestype.Fields.IdTemplatesType.ToString()].Visible = false;
                    //cmbidctp.Properties.Columns[EntClaTipoplantillas.Fields.usucre.ToString()].Visible = false;
                    //cmbidctp.Properties.Columns[EntClaTipoplantillas.Fields.feccre.ToString()].Visible = false;
                    //cmbidctp.Properties.Columns[EntClaTipoplantillas.Fields.usumod.ToString()].Visible = false;
                    //cmbidctp.Properties.Columns[EntClaTipoplantillas.Fields.fecmod.ToString()].Visible = false;
                    //cmbidctp.Properties.Columns[EntClaTipoplantillas.Fields.apiestado.ToString()].Visible = false;
                    //cmbidctp.Properties.Columns[EntClaTipoplantillas.Fields.apitransaccion.ToString()].Visible = false;

                    cmbidctp.ItemIndex = 0;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            try
            {
                cmbidctp.EditValue = 0;
            }
            catch (Exception)
            {
            }
        }

        private void cmbidctp_EditValueChanged(object sender, EventArgs e)
        {
            this.CargarListado();
        }

        private void gcClaPlantillas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                EliminarRegistro();
            }
        }

        private void gcClaPlantillas_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ModificarRegistro();
        }

        private void lClaPlantillas_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            CargarCmbidctp();

            CargarListado();
        }

        #endregion Methods
    }
}