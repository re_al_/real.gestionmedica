﻿namespace ReAl.GestionMedica.App.CAL
{
    partial class LCalAgenda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            DevExpress.XtraScheduler.TimeRuler timeRuler1 = new DevExpress.XtraScheduler.TimeRuler();
            DevExpress.XtraScheduler.TimeRuler timeRuler2 = new DevExpress.XtraScheduler.TimeRuler();
            ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            viewNavigatorBackwardItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorBackwardItem();
            viewNavigatorForwardItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorForwardItem();
            viewNavigatorTodayItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorTodayItem();
            viewNavigatorZoomInItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorZoomInItem();
            viewNavigatorZoomOutItem1 = new DevExpress.XtraScheduler.UI.ViewNavigatorZoomOutItem();
            viewSelectorItem1 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            viewSelectorItem2 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            viewSelectorItem3 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            viewSelectorItem4 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            viewSelectorItem5 = new DevExpress.XtraScheduler.UI.ViewSelectorItem();
            barButtonImprimir = new DevExpress.XtraBars.BarButtonItem();
            schedulerControl1 = new DevExpress.XtraScheduler.SchedulerControl();
            schedulerStorage1 = new DevExpress.XtraScheduler.SchedulerDataStorage(components);
            dateNavigator1 = new DevExpress.XtraScheduler.DateNavigator();
            groupControl1 = new DevExpress.XtraEditors.GroupControl();
            gcPendientes = new DevExpress.XtraGrid.GridControl();
            gvPendientes = new DevExpress.XtraGrid.Views.Grid.GridView();
            btnActualizar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)ribbon).BeginInit();
            ((System.ComponentModel.ISupportInitialize)schedulerControl1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)schedulerStorage1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dateNavigator1).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dateNavigator1.CalendarTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)groupControl1).BeginInit();
            groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)gcPendientes).BeginInit();
            ((System.ComponentModel.ISupportInitialize)gvPendientes).BeginInit();
            SuspendLayout();
            // 
            // ribbonStatusBar
            // 
            ribbonStatusBar.Location = new Point(0, 743);
            ribbonStatusBar.Name = "ribbonStatusBar";
            ribbonStatusBar.Ribbon = ribbon;
            ribbonStatusBar.Size = new Size(1016, 24);
            // 
            // ribbon
            // 
            ribbon.ApplicationButtonText = null;
            ribbon.ExpandCollapseItem.Id = 0;
            ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] { ribbon.ExpandCollapseItem, ribbon.SearchEditItem, viewNavigatorBackwardItem1, viewNavigatorForwardItem1, viewNavigatorTodayItem1, viewNavigatorZoomInItem1, viewNavigatorZoomOutItem1, viewSelectorItem1, viewSelectorItem2, viewSelectorItem3, viewSelectorItem4, viewSelectorItem5, barButtonImprimir });
            ribbon.Location = new Point(0, 0);
            ribbon.MaxItemId = 13;
            ribbon.Name = "ribbon";
            ribbon.Size = new Size(1016, 58);
            ribbon.StatusBar = ribbonStatusBar;
            // 
            // viewNavigatorBackwardItem1
            // 
            viewNavigatorBackwardItem1.GroupIndex = 1;
            viewNavigatorBackwardItem1.Id = 2;
            viewNavigatorBackwardItem1.LargeWidth = 70;
            viewNavigatorBackwardItem1.Name = "viewNavigatorBackwardItem1";
            // 
            // viewNavigatorForwardItem1
            // 
            viewNavigatorForwardItem1.GroupIndex = 1;
            viewNavigatorForwardItem1.Id = 3;
            viewNavigatorForwardItem1.LargeWidth = 70;
            viewNavigatorForwardItem1.Name = "viewNavigatorForwardItem1";
            // 
            // viewNavigatorTodayItem1
            // 
            viewNavigatorTodayItem1.Caption = "";
            viewNavigatorTodayItem1.GroupIndex = 1;
            viewNavigatorTodayItem1.Id = 4;
            viewNavigatorTodayItem1.LargeWidth = 70;
            viewNavigatorTodayItem1.Name = "viewNavigatorTodayItem1";
            // 
            // viewNavigatorZoomInItem1
            // 
            viewNavigatorZoomInItem1.GroupIndex = 1;
            viewNavigatorZoomInItem1.Id = 5;
            viewNavigatorZoomInItem1.LargeWidth = 70;
            viewNavigatorZoomInItem1.Name = "viewNavigatorZoomInItem1";
            // 
            // viewNavigatorZoomOutItem1
            // 
            viewNavigatorZoomOutItem1.GroupIndex = 1;
            viewNavigatorZoomOutItem1.Id = 6;
            viewNavigatorZoomOutItem1.LargeWidth = 70;
            viewNavigatorZoomOutItem1.Name = "viewNavigatorZoomOutItem1";
            // 
            // viewSelectorItem1
            // 
            viewSelectorItem1.GroupIndex = 1;
            viewSelectorItem1.Id = 7;
            viewSelectorItem1.LargeWidth = 70;
            viewSelectorItem1.Name = "viewSelectorItem1";
            viewSelectorItem1.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Day;
            // 
            // viewSelectorItem2
            // 
            viewSelectorItem2.GroupIndex = 1;
            viewSelectorItem2.Id = 8;
            viewSelectorItem2.LargeWidth = 70;
            viewSelectorItem2.Name = "viewSelectorItem2";
            viewSelectorItem2.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.WorkWeek;
            // 
            // viewSelectorItem3
            // 
            viewSelectorItem3.GroupIndex = 1;
            viewSelectorItem3.Id = 9;
            viewSelectorItem3.LargeWidth = 70;
            viewSelectorItem3.Name = "viewSelectorItem3";
            viewSelectorItem3.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Week;
            // 
            // viewSelectorItem4
            // 
            viewSelectorItem4.GroupIndex = 1;
            viewSelectorItem4.Id = 10;
            viewSelectorItem4.LargeWidth = 70;
            viewSelectorItem4.Name = "viewSelectorItem4";
            viewSelectorItem4.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Month;
            // 
            // viewSelectorItem5
            // 
            viewSelectorItem5.GroupIndex = 1;
            viewSelectorItem5.Id = 11;
            viewSelectorItem5.LargeWidth = 70;
            viewSelectorItem5.Name = "viewSelectorItem5";
            viewSelectorItem5.SchedulerViewType = DevExpress.XtraScheduler.SchedulerViewType.Timeline;
            // 
            // barButtonImprimir
            // 
            barButtonImprimir.Caption = "Imprimir";
            barButtonImprimir.Id = 12;
            barButtonImprimir.ImageOptions.Image = PrintRibbonControllerResources.RibbonPrintPreview_PrintDirectLarge;
            barButtonImprimir.LargeWidth = 70;
            barButtonImprimir.Name = "barButtonImprimir";
            barButtonImprimir.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            barButtonImprimir.ItemClick += barButtonImprimir_ItemClick;
            // 
            // schedulerControl1
            // 
            schedulerControl1.DataStorage = schedulerStorage1;
            schedulerControl1.Dock = DockStyle.Fill;
            schedulerControl1.Location = new Point(200, 58);
            schedulerControl1.MenuManager = ribbon;
            schedulerControl1.Name = "schedulerControl1";
            schedulerControl1.OptionsCustomization.AllowAppointmentDrag = DevExpress.XtraScheduler.UsedAppointmentType.None;
            schedulerControl1.OptionsCustomization.AllowAppointmentDragBetweenResources = DevExpress.XtraScheduler.UsedAppointmentType.None;
            schedulerControl1.OptionsCustomization.AllowInplaceEditor = DevExpress.XtraScheduler.UsedAppointmentType.None;
            schedulerControl1.Size = new Size(816, 685);
            schedulerControl1.Start = new DateTime(2024, 1, 18, 0, 0, 0, 0);
            schedulerControl1.TabIndex = 1;
            schedulerControl1.Text = "schedulerControl1";
            schedulerControl1.Views.DayView.TimeRulers.Add(timeRuler1);
            schedulerControl1.Views.DayView.TimeScale = TimeSpan.Parse("00:15:00");
            schedulerControl1.Views.DayView.VisibleTime = new DevExpress.XtraScheduler.TimeOfDayInterval(TimeSpan.Parse("06:00:00"), TimeSpan.Parse("23:00:00"));
            schedulerControl1.Views.GanttView.Enabled = false;
            schedulerControl1.Views.TimelineView.Enabled = false;
            schedulerControl1.Views.WorkWeekView.TimeRulers.Add(timeRuler2);
            schedulerControl1.Views.YearView.Enabled = false;
            schedulerControl1.Views.YearView.UseOptimizedScrolling = false;
            schedulerControl1.VisibleIntervalChanged += schedulerControl1_VisibleIntervalChanged;
            schedulerControl1.ActiveViewChanged += schedulerControl1_ActiveViewChanged;
            schedulerControl1.AppointmentResized += schedulerControl1_AppointmentResized;
            schedulerControl1.PopupMenuShowing += schedulerControl1_PopupMenuShowing;
            schedulerControl1.EditAppointmentFormShowing += schedulerControl1_EditAppointmentFormShowing;
            // 
            // schedulerStorage1
            // 
            // 
            // 
            // 
            schedulerStorage1.Appointments.Labels.CreateNewLabel(0, "None", "&None", SystemColors.Window);
            schedulerStorage1.Appointments.Labels.CreateNewLabel(1, "Important", "&Important", Color.FromArgb(255, 194, 190));
            schedulerStorage1.Appointments.Labels.CreateNewLabel(2, "Business", "&Business", Color.FromArgb(168, 213, 255));
            schedulerStorage1.Appointments.Labels.CreateNewLabel(3, "Personal", "&Personal", Color.FromArgb(193, 244, 156));
            schedulerStorage1.Appointments.Labels.CreateNewLabel(4, "Vacation", "&Vacation", Color.FromArgb(243, 228, 199));
            schedulerStorage1.Appointments.Labels.CreateNewLabel(5, "Must Attend", "Must &Attend", Color.FromArgb(244, 206, 147));
            schedulerStorage1.Appointments.Labels.CreateNewLabel(6, "Travel Required", "&Travel Required", Color.FromArgb(199, 244, 255));
            schedulerStorage1.Appointments.Labels.CreateNewLabel(7, "Needs Preparation", "&Needs Preparation", Color.FromArgb(207, 219, 152));
            schedulerStorage1.Appointments.Labels.CreateNewLabel(8, "Birthday", "&Birthday", Color.FromArgb(224, 207, 233));
            schedulerStorage1.Appointments.Labels.CreateNewLabel(9, "Anniversary", "&Anniversary", Color.FromArgb(141, 233, 223));
            schedulerStorage1.Appointments.Labels.CreateNewLabel(10, "Phone Call", "Phone &Call", Color.FromArgb(255, 247, 165));
            // 
            // dateNavigator1
            // 
            dateNavigator1.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            dateNavigator1.CellPadding = new Padding(2);
            dateNavigator1.Dock = DockStyle.Top;
            dateNavigator1.FirstDayOfWeek = DayOfWeek.Monday;
            dateNavigator1.Location = new Point(2, 66);
            dateNavigator1.Name = "dateNavigator1";
            dateNavigator1.SchedulerControl = schedulerControl1;
            dateNavigator1.Size = new Size(196, 318);
            dateNavigator1.TabIndex = 1;
            // 
            // groupControl1
            // 
            groupControl1.Controls.Add(gcPendientes);
            groupControl1.Controls.Add(dateNavigator1);
            groupControl1.Controls.Add(btnActualizar);
            groupControl1.Dock = DockStyle.Left;
            groupControl1.Location = new Point(0, 58);
            groupControl1.Name = "groupControl1";
            groupControl1.Size = new Size(200, 685);
            groupControl1.TabIndex = 0;
            groupControl1.Text = "Pendientes";
            // 
            // gcPendientes
            // 
            gcPendientes.Dock = DockStyle.Fill;
            gcPendientes.Location = new Point(2, 384);
            gcPendientes.MainView = gvPendientes;
            gcPendientes.MenuManager = ribbon;
            gcPendientes.Name = "gcPendientes";
            gcPendientes.Size = new Size(196, 299);
            gcPendientes.TabIndex = 2;
            gcPendientes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { gvPendientes });
            gcPendientes.MouseDoubleClick += gcPendientes_MouseDoubleClick;
            // 
            // gvPendientes
            // 
            gvPendientes.GridControl = gcPendientes;
            gvPendientes.Name = "gvPendientes";
            // 
            // btnActualizar
            // 
            btnActualizar.DialogResult = DialogResult.OK;
            btnActualizar.Dock = DockStyle.Top;
            btnActualizar.Location = new Point(2, 23);
            btnActualizar.Name = "btnActualizar";
            btnActualizar.Size = new Size(196, 43);
            btnActualizar.TabIndex = 0;
            btnActualizar.Text = "&Actualizar Calendario";
            btnActualizar.Click += btnActualizar_Click;
            // 
            // LCalAgenda
            // 
            AutoScaleDimensions = new SizeF(6F, 13F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1016, 767);
            Controls.Add(schedulerControl1);
            Controls.Add(groupControl1);
            Controls.Add(ribbonStatusBar);
            Controls.Add(ribbon);
            Name = "LCalAgenda";
            Ribbon = ribbon;
            StartPosition = FormStartPosition.CenterParent;
            StatusBar = ribbonStatusBar;
            Text = "Mi Agenda";
            Activated += lCalAgenda_Activated;
            Load += lCalAgenda_Load;
            ((System.ComponentModel.ISupportInitialize)ribbon).EndInit();
            ((System.ComponentModel.ISupportInitialize)schedulerControl1).EndInit();
            ((System.ComponentModel.ISupportInitialize)schedulerStorage1).EndInit();
            ((System.ComponentModel.ISupportInitialize)dateNavigator1.CalendarTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)dateNavigator1).EndInit();
            ((System.ComponentModel.ISupportInitialize)groupControl1).EndInit();
            groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)gcPendientes).EndInit();
            ((System.ComponentModel.ISupportInitialize)gvPendientes).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraScheduler.SchedulerControl schedulerControl1;
        private DevExpress.XtraScheduler.SchedulerDataStorage schedulerStorage1;
        private DevExpress.XtraScheduler.DateNavigator dateNavigator1;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraGrid.GridControl gcPendientes;
        private DevExpress.XtraGrid.Views.Grid.GridView gvPendientes;
        private DevExpress.XtraEditors.SimpleButton btnActualizar;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraScheduler.UI.ViewNavigatorBackwardItem viewNavigatorBackwardItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorForwardItem viewNavigatorForwardItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorTodayItem viewNavigatorTodayItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorZoomInItem viewNavigatorZoomInItem1;
        private DevExpress.XtraScheduler.UI.ViewNavigatorZoomOutItem viewNavigatorZoomOutItem1;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem1;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem2;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem3;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem4;
        private DevExpress.XtraScheduler.UI.ViewSelectorItem viewSelectorItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonImprimir;
    }
}