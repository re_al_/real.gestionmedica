﻿using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
using ReAl.GestionMedica.BackendConnector.Utils;
using ReAl.GestionMedica.Class;

namespace ReAl.GestionMedica.App.CAL
{
    public partial class FCalAgendaAusencia : DevExpress.XtraEditors.XtraForm
    {
        #region Fields

        private readonly PatAppointments _myObj = null;

        #endregion Fields

        #region Constructors

        public FCalAgendaAusencia(PatAppointments obj)
        {
            InitializeComponent();

            _myObj = obj;
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                if (_myObj != null)
                {
                    var service = new PatAppointmentsController();
                    _myObj.Labels = 1;
                    _myObj.LeavingReason = txtAusencia.Text;
                    _myObj.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                    _myObj.ApiUsumod = cParametrosApp.AppRestUser.Login;
                    if (service.Update(_myObj))
                    {
                        bProcede = true;
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
            if (bProcede)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void CargarDatos()
        {
            if (_myObj != null)
            {
                txtAusencia.Text = _myObj.LeavingReason;
            }
        }

        private void fCalAgendaAusencia_Load(object sender, EventArgs e)
        {
            if (_myObj != null)
            {
                CargarDatos();
            }
        }

        #endregion Methods
    }
}