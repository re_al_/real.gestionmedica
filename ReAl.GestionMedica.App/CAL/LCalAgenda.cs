﻿using DevExpress.XtraBars;
using DevExpress.XtraScheduler;
using ReAl.GestionMedica.App.PAC;
using ReAl.GestionMedica.App.RPT;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;

namespace ReAl.GestionMedica.App.CAL
{
    public partial class LCalAgenda : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        #region Constructors

        public LCalAgenda()
        {
            this.InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        private void barButtonImprimir_ItemClick(object sender, ItemClickEventArgs e)
        {
            var frm = new RptViewerCal(this.schedulerControl1);
            frm.ShowDialog();
        }

        private void btnActualizar_Click(object sender, EventArgs e)
        {
            this.CargarListado();
            this.CargarAgenda(this.schedulerControl1.Start);
        }

        private void CargarAgenda(DateTime dateFilter)
        {
            try
            {
                var rn = new PatAppointmentsController();
                var dt = new List<PatAppointments>();
                switch (schedulerControl1.ActiveView.Type)
                {
                    case SchedulerViewType.Day:
                    case SchedulerViewType.Agenda:
                        dt = rn.GetDayPendings(dateFilter.ToString("yyyyMMdd"));
                        break;
                    case SchedulerViewType.Week:
                    case SchedulerViewType.FullWeek:
                    case SchedulerViewType.WorkWeek:
                        dt = rn.GetWeekPendings(dateFilter.ToString("yyyyMMdd"));
                        break;
                    case SchedulerViewType.Month:
                        dt = rn.GetMonthPendings(dateFilter.ToString("yyyyMMdd"));
                        break;
                    default:
                        break;
                }

                var resourceMappings = this.schedulerStorage1.Resources.Mappings;
                var appointmentMappings = this.schedulerStorage1.Appointments.Mappings;

                var idApt = new AppointmentCustomFieldMapping(PatAppointments.Fields.IdAppointments.ToString(), PatAppointments.Fields.IdAppointments.ToString());
                this.schedulerStorage1.Appointments.CustomFieldMappings.Clear();
                this.schedulerStorage1.Appointments.CustomFieldMappings.Add(idApt);

                resourceMappings.Id = PatAppointments.Fields.IdAppointments.ToString();
                resourceMappings.Caption = PatAppointments.Fields.Subject.ToString();
                

                //appointmentMappings.AppointmentId = PatAppointments.Fields.idcci.ToString();
                appointmentMappings.Start = PatAppointments.Fields.TimeStart.ToString();
                appointmentMappings.End = PatAppointments.Fields.TimeEnd.ToString();
                appointmentMappings.Subject = PatAppointments.Fields.Appointment.ToString();
                appointmentMappings.Description = PatAppointments.Fields.Description.ToString();
                appointmentMappings.Location = PatAppointments.Fields.Location.ToString();
                appointmentMappings.Label = PatAppointments.Fields.Labels.ToString();
                this.schedulerStorage1.Appointments.ResourceSharing = true;

                this.schedulerStorage1.Appointments.DataSource = dt;
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void CargarListado()
        {
            try
            {
                var rn = new PatAppointmentsController();
                var dt = rn.GetPending();
                this.gcPendientes.DataSource = dt;
                if (dt != null && dt.Count > 0)
                {
                    this.gvPendientes.PopulateColumns();
                    this.gvPendientes.OptionsBehavior.Editable = false;
                    this.gvPendientes.Columns[PatAppointments.Fields.IdAppointments.ToString()].Visible = false;
                    this.gvPendientes.Columns[PatAppointments.Fields.Subject.ToString()].Visible = false;
                    this.gvPendientes.Columns[PatAppointments.Fields.Description.ToString()].Visible = false;
                    this.gvPendientes.Columns[PatAppointments.Fields.TimeStart.ToString()].Visible = false;
                    this.gvPendientes.Columns[PatAppointments.Fields.TimeEnd.ToString()].Visible = false;
                    this.gvPendientes.Columns[PatAppointments.Fields.Location.ToString()].Visible = false;
                    this.gvPendientes.Columns[PatAppointments.Fields.Labels.ToString()].Visible = false;
                    this.gvPendientes.Columns[PatAppointments.Fields.Doctor.ToString()].Visible = false;
                    this.gvPendientes.Columns[PatAppointments.Fields.Amount.ToString()].Visible = false;
                    //this.gvPendientes.Columns[PatAppointments.Fields.Appointment.ToString()].Visible = true;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void gcPendientes_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var rn = new PatAppointmentsController();

            //Apropiamos los valores del Grid
            Int64 intidcci = int.Parse(this.gvPendientes.GetFocusedRowCellValue(PatAppointments.Fields.IdAppointments.ToString()).ToString());

            //Obtenemos el objeto
            var obj = rn.GetById(intidcci);

            if (obj != null)
            {
                //Creamos el Form
                var frm = new FCalAgenda(schedulerControl1, obj);

                //Necesario para los skins
                frm.LookAndFeel.ParentLookAndFeel = this.schedulerControl1.LookAndFeel;
                frm.ShowDialog();
                if (frm.DialogResult == DialogResult.OK)
                {
                    this.CargarAgenda(obj.TimeEnd);
                    this.CargarListado();
                    this.schedulerControl1.Refresh();
                }
                else if (frm.DialogResult == DialogResult.Retry)
                {
                    //Tratamos de cerrar las pantallas
                    var miParent = this.MdiParent;
                    try
                    {
                        var frm2 = (FPrincipal)miParent;

                        foreach (var mdiChild in frm2.MdiChildren)
                        {
                            if (mdiChild.GetType() == typeof(FPacHistoriaClinica)) mdiChild.Close();

                            if (mdiChild.GetType() == typeof(LPacConsultas)) mdiChild.Close();

                            if (mdiChild.GetType() == typeof(LPacRecetas)) mdiChild.Close();

                            if (mdiChild.GetType() == typeof(LPacCirugias)) mdiChild.Close();

                            if (mdiChild.GetType() == typeof(LPacInformes)) mdiChild.Close();

                            if (mdiChild.GetType() == typeof(LPacMultimedia)) mdiChild.Close();
                        }
                        frm2.CargarDatosPaciente();
                        frm2.CargarDatosCita();
                        frm2.ribbon.SelectedPage = frm2.rpPacientes;
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        private void lCalAgenda_Activated(object sender, EventArgs e)
        {
            this.CargarAgenda(this.schedulerControl1.Start);
            this.CargarListado();
            this.schedulerControl1.Refresh();
        }

        private void lCalAgenda_Load(object sender, EventArgs e)
        {
            //this.CargarListado();
            //this.CargarAgenda();
            this.schedulerControl1.Start = DateTime.Today;

            //this.schedulerStorage1.Resources.Clear();
            //this.schedulerStorage1.Resources.Add(new Resource(1, "Programa de Citas Médicas - Neuro Diagonóstico"));
            //this.schedulerStorage1.EnableReminders = false;

            //Appointment apt = new Appointment(AppointmentType.Normal);
            //apt.AllDay = false;
            //apt.Description = "Cita con la Paciente Juanita Perez";
            //apt.Start = DateTime.Now;
            //apt.End = DateTime.Now.AddMinutes(30);
            //apt.Subject = "CONSULTA PACIENTE";
            //apt.Location = "Consultorio";
            //apt.StatusId = 2; //Ocupado
            //apt.LabelId = 2; //Trabajo

            //this.schedulerStorage1.Appointments.Add(apt);
        }

        private void schedulerControl1_AppointmentResized(object sender, AppointmentResizeEventArgs e)
        {
            try
            {
                var apt = e.EditedAppointment;
                var strAlo = apt.CustomFields[PatAppointments.Fields.IdAppointments.ToString()].ToString();
                if (!string.IsNullOrEmpty(strAlo))
                {
                    var idApt = 0;
                    if (int.TryParse(strAlo, out idApt))
                    {
                        if (idApt > 0)
                        {
                            var rn = new PatAppointmentsController();
                            var obj = rn.GetById(idApt);
                            if (obj != null)
                            {
                                //Actualizamos la Cita
                                obj.Description = apt.Description;
                                obj.TimeStart = apt.Start;
                                obj.TimeEnd = apt.End;
                                obj.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                                rn.Update(obj);
                            }
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        //Modificamos la llamada a los Forms
        private void schedulerControl1_EditAppointmentFormShowing(object sender, AppointmentFormEventArgs e)
        {
            try
            {
                var apt = e.Appointment;

                var openRecurrenceForm = apt.IsRecurring && this.schedulerStorage1.Appointments.IsNewAppointment(apt);

                //Creamos el Form
                var frm = new FCalAgenda((SchedulerControl)sender, apt, openRecurrenceForm);

                //Necesario para los skins
                frm.LookAndFeel.ParentLookAndFeel = this.schedulerControl1.LookAndFeel;
                e.DialogResult = frm.ShowDialog();
                if (e.DialogResult == DialogResult.OK)
                {
                    this.CargarAgenda(apt.Start);
                    this.CargarListado();
                    this.schedulerControl1.Refresh();
                }
                else if (e.DialogResult == DialogResult.Retry)
                {
                    //Tratamos de cerrar las pantallas
                    var miParent = this.MdiParent;
                    try
                    {
                        var frm2 = (FPrincipal)miParent;

                        foreach (var mdiChild in frm2.MdiChildren)
                        {
                            if (mdiChild.GetType() == typeof(FPacHistoriaClinica)) mdiChild.Close();

                            if (mdiChild.GetType() == typeof(LPacConsultas)) mdiChild.Close();

                            if (mdiChild.GetType() == typeof(LPacRecetas)) mdiChild.Close();

                            if (mdiChild.GetType() == typeof(LPacCirugias)) mdiChild.Close();

                            if (mdiChild.GetType() == typeof(LPacInformes)) mdiChild.Close();

                            if (mdiChild.GetType() == typeof(LPacMultimedia)) mdiChild.Close();
                        }
                        frm2.CargarDatosPaciente();
                        frm2.CargarDatosCita();
                        frm2.ribbon.SelectedPage = frm2.rpPacientes;
                    }
                    catch (Exception)
                    {
                    }
                }
                e.Handled = true;
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void schedulerControl1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu.Id == SchedulerMenuItemId.DefaultMenu)
            {
                e.Menu.RemoveMenuItem(SchedulerMenuItemId.NewAllDayEvent);
                e.Menu.RemoveMenuItem(SchedulerMenuItemId.EditSeries);
            }
            if (e.Menu.Id == SchedulerMenuItemId.AppointmentMenu)
            {
                e.Menu.RemoveMenuItem(SchedulerMenuItemId.LabelSubMenu);
                e.Menu.RemoveMenuItem(SchedulerMenuItemId.StatusSubMenu);
                e.Menu.RemoveMenuItem(SchedulerMenuItemId.DeleteAppointment);
            }
        }

        #endregion Methods

        private void schedulerControl1_ActiveViewChanged(object sender, EventArgs e)
        {
            
        }

        private void schedulerControl1_VisibleIntervalChanged(object sender, EventArgs e)
        {
            TimeInterval visibleInterval = (sender as DevExpress.XtraScheduler.SchedulerControl).ActiveView.GetVisibleIntervals().Interval;
            CargarAgenda(visibleInterval.Start);
        }
    }
}