﻿


namespace ReAl.GestionMedica.App.CAL
{
    partial class FCalAgenda
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FCalAgenda));
            ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            barButtonGuardar = new DevExpress.XtraBars.BarButtonItem();
            barButtonEliminar = new DevExpress.XtraBars.BarButtonItem();
            barButtonCancelar = new DevExpress.XtraBars.BarButtonItem();
            barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            barButtonPaciente = new DevExpress.XtraBars.BarButtonItem();
            barButtonCosto = new DevExpress.XtraBars.BarButtonItem();
            barButtonFalta = new DevExpress.XtraBars.BarButtonItem();
            ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            lblasuntocci = new Label();
            txtasuntocci = new DevExpress.XtraEditors.TextEdit();
            lbldescripcioncci = new Label();
            txtdescripcioncci = new DevExpress.XtraEditors.MemoEdit();
            groupBox1 = new GroupBox();
            groupBox2 = new GroupBox();
            cmbLabel = new DevExpress.XtraEditors.LookUpEdit();
            lblLabel = new Label();
            label3 = new Label();
            cmbLugarcci = new DevExpress.XtraEditors.LookUpEdit();
            dtpHorFinal = new DevExpress.XtraEditors.DateEdit();
            dtpFecFinal = new DevExpress.XtraEditors.DateEdit();
            label1 = new Label();
            dtpHorInicio = new DevExpress.XtraEditors.DateEdit();
            dtpFecInicio = new DevExpress.XtraEditors.DateEdit();
            lbliniciocci = new Label();
            groupBox3 = new GroupBox();
            cmbid_patients = new DevExpress.XtraEditors.LookUpEdit();
            label2 = new Label();
            groupBox4 = new GroupBox();
            cmbusuasignacion = new DevExpress.XtraEditors.LookUpEdit();
            label4 = new Label();
            ((System.ComponentModel.ISupportInitialize)ribbon).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtasuntocci.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtdescripcioncci.Properties).BeginInit();
            groupBox1.SuspendLayout();
            groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)cmbLabel.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)cmbLugarcci.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dtpHorFinal.Properties.CalendarTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dtpHorFinal.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dtpFecFinal.Properties.CalendarTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dtpFecFinal.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dtpHorInicio.Properties.CalendarTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dtpHorInicio.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dtpFecInicio.Properties.CalendarTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dtpFecInicio.Properties).BeginInit();
            groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)cmbid_patients.Properties).BeginInit();
            groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)cmbusuasignacion.Properties).BeginInit();
            SuspendLayout();
            // 
            // ribbon
            // 
            ribbon.ApplicationButtonImageOptions.Image = Properties.Resources.ReAlMed;
            ribbon.ApplicationButtonText = null;
            ribbon.ExpandCollapseItem.Id = 0;
            ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] { ribbon.ExpandCollapseItem, ribbon.SearchEditItem, barButtonGuardar, barButtonEliminar, barButtonCancelar, barButtonItem4, barButtonPaciente, barButtonCosto, barButtonFalta });
            ribbon.Location = new Point(0, 0);
            ribbon.MaxItemId = 8;
            ribbon.Name = "ribbon";
            ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] { ribbonPage1 });
            ribbon.Size = new Size(590, 158);
            ribbon.StatusBar = ribbonStatusBar;
            // 
            // barButtonGuardar
            // 
            barButtonGuardar.Caption = "Guardar";
            barButtonGuardar.Id = 1;
            barButtonGuardar.ImageOptions.LargeImage = PrintRibbonControllerResources.RibbonPrintPreview_SaveLarge;
            barButtonGuardar.LargeWidth = 75;
            barButtonGuardar.Name = "barButtonGuardar";
            barButtonGuardar.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            barButtonGuardar.ItemClick += barButtonGuardar_ItemClick;
            // 
            // barButtonEliminar
            // 
            barButtonEliminar.Caption = "Eliminar";
            barButtonEliminar.Enabled = false;
            barButtonEliminar.Id = 2;
            barButtonEliminar.ImageOptions.LargeImage = PrintRibbonControllerResources.RibbonPrintPreview_ClosePreviewLarge;
            barButtonEliminar.LargeWidth = 75;
            barButtonEliminar.Name = "barButtonEliminar";
            barButtonEliminar.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            barButtonEliminar.ItemClick += barButtonEliminar_ItemClick;
            // 
            // barButtonCancelar
            // 
            barButtonCancelar.Caption = "Cancelar Cambios";
            barButtonCancelar.Id = 3;
            barButtonCancelar.ImageOptions.LargeImage = Properties.Resources.Undo;
            barButtonCancelar.LargeWidth = 75;
            barButtonCancelar.Name = "barButtonCancelar";
            barButtonCancelar.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            barButtonCancelar.ItemClick += barButtonCancelar_ItemClick;
            // 
            // barButtonItem4
            // 
            barButtonItem4.Caption = "barButtonItem4";
            barButtonItem4.Id = 4;
            barButtonItem4.LargeWidth = 75;
            barButtonItem4.Name = "barButtonItem4";
            barButtonItem4.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // barButtonPaciente
            // 
            barButtonPaciente.Caption = "Seleccionar Paciente";
            barButtonPaciente.Enabled = false;
            barButtonPaciente.Id = 5;
            barButtonPaciente.ImageOptions.LargeImage = Properties.Resources.pacientes;
            barButtonPaciente.LargeWidth = 75;
            barButtonPaciente.Name = "barButtonPaciente";
            barButtonPaciente.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            barButtonPaciente.ItemClick += barButtonPaciente_ItemClick;
            // 
            // barButtonCosto
            // 
            barButtonCosto.Caption = "Establecer Costo";
            barButtonCosto.Enabled = false;
            barButtonCosto.Id = 6;
            barButtonCosto.ImageOptions.LargeImage = Properties.Resources.wallet;
            barButtonCosto.LargeWidth = 75;
            barButtonCosto.Name = "barButtonCosto";
            barButtonCosto.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            barButtonCosto.ItemClick += barButtonCosto_ItemClick;
            // 
            // barButtonFalta
            // 
            barButtonFalta.Caption = "Motivo de Falta";
            barButtonFalta.Enabled = false;
            barButtonFalta.Id = 7;
            barButtonFalta.ImageOptions.LargeImage = Properties.Resources.trash;
            barButtonFalta.LargeWidth = 75;
            barButtonFalta.Name = "barButtonFalta";
            barButtonFalta.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            barButtonFalta.ItemClick += barButtonFalta_ItemClick;
            // 
            // ribbonPage1
            // 
            ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] { ribbonPageGroup1, ribbonPageGroup2 });
            ribbonPage1.Name = "ribbonPage1";
            ribbonPage1.Text = "Citas";
            // 
            // ribbonPageGroup1
            // 
            ribbonPageGroup1.ItemLinks.Add(barButtonGuardar);
            ribbonPageGroup1.ItemLinks.Add(barButtonCancelar);
            ribbonPageGroup1.ItemLinks.Add(barButtonEliminar);
            ribbonPageGroup1.Name = "ribbonPageGroup1";
            ribbonPageGroup1.Text = "Opciones";
            // 
            // ribbonPageGroup2
            // 
            ribbonPageGroup2.ItemLinks.Add(barButtonPaciente);
            ribbonPageGroup2.ItemLinks.Add(barButtonCosto);
            ribbonPageGroup2.ItemLinks.Add(barButtonFalta);
            ribbonPageGroup2.Name = "ribbonPageGroup2";
            ribbonPageGroup2.Text = "Paciente";
            // 
            // ribbonStatusBar
            // 
            ribbonStatusBar.Location = new Point(0, 485);
            ribbonStatusBar.Name = "ribbonStatusBar";
            ribbonStatusBar.Ribbon = ribbon;
            ribbonStatusBar.Size = new Size(590, 24);
            // 
            // lblasuntocci
            // 
            lblasuntocci.AutoSize = true;
            lblasuntocci.Location = new Point(6, 17);
            lblasuntocci.Name = "lblasuntocci";
            lblasuntocci.Size = new Size(45, 13);
            lblasuntocci.TabIndex = 0;
            lblasuntocci.Text = "Asunto:";
            // 
            // txtasuntocci
            // 
            txtasuntocci.Location = new Point(73, 14);
            txtasuntocci.Name = "txtasuntocci";
            txtasuntocci.Properties.MaxLength = 500;
            txtasuntocci.Size = new Size(487, 20);
            txtasuntocci.TabIndex = 1;
            // 
            // lbldescripcioncci
            // 
            lbldescripcioncci.AutoSize = true;
            lbldescripcioncci.Location = new Point(6, 43);
            lbldescripcioncci.Name = "lbldescripcioncci";
            lbldescripcioncci.Size = new Size(65, 13);
            lbldescripcioncci.TabIndex = 2;
            lbldescripcioncci.Text = "Descripcion:";
            // 
            // txtdescripcioncci
            // 
            txtdescripcioncci.Location = new Point(73, 40);
            txtdescripcioncci.MenuManager = ribbon;
            txtdescripcioncci.Name = "txtdescripcioncci";
            txtdescripcioncci.Size = new Size(487, 86);
            txtdescripcioncci.TabIndex = 3;
            // 
            // groupBox1
            // 
            groupBox1.Controls.Add(txtasuntocci);
            groupBox1.Controls.Add(txtdescripcioncci);
            groupBox1.Controls.Add(lblasuntocci);
            groupBox1.Controls.Add(lbldescripcioncci);
            groupBox1.Location = new Point(12, 264);
            groupBox1.Name = "groupBox1";
            groupBox1.Size = new Size(566, 132);
            groupBox1.TabIndex = 2;
            groupBox1.TabStop = false;
            // 
            // groupBox2
            // 
            groupBox2.Controls.Add(cmbLabel);
            groupBox2.Controls.Add(lblLabel);
            groupBox2.Controls.Add(label3);
            groupBox2.Controls.Add(cmbLugarcci);
            groupBox2.Controls.Add(dtpHorFinal);
            groupBox2.Controls.Add(dtpFecFinal);
            groupBox2.Controls.Add(label1);
            groupBox2.Controls.Add(dtpHorInicio);
            groupBox2.Controls.Add(dtpFecInicio);
            groupBox2.Controls.Add(lbliniciocci);
            groupBox2.Location = new Point(12, 402);
            groupBox2.Name = "groupBox2";
            groupBox2.Size = new Size(566, 77);
            groupBox2.TabIndex = 3;
            groupBox2.TabStop = false;
            // 
            // cmbLabel
            // 
            cmbLabel.Location = new Point(363, 46);
            cmbLabel.Name = "cmbLabel";
            cmbLabel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            cmbLabel.Size = new Size(197, 20);
            cmbLabel.TabIndex = 9;
            cmbLabel.Visible = false;
            cmbLabel.EditValueChanged += cmbLabel_EditValueChanged;
            // 
            // lblLabel
            // 
            lblLabel.AutoSize = true;
            lblLabel.Location = new Point(308, 49);
            lblLabel.Name = "lblLabel";
            lblLabel.Size = new Size(44, 13);
            lblLabel.TabIndex = 8;
            lblLabel.Text = "Estado:";
            lblLabel.Visible = false;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(308, 23);
            label3.Name = "label3";
            label3.Size = new Size(38, 13);
            label3.TabIndex = 3;
            label3.Text = "Lugar:";
            // 
            // cmbLugarcci
            // 
            cmbLugarcci.Location = new Point(363, 20);
            cmbLugarcci.Name = "cmbLugarcci";
            cmbLugarcci.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            cmbLugarcci.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            cmbLugarcci.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard;
            cmbLugarcci.Size = new Size(197, 20);
            cmbLugarcci.TabIndex = 4;
            // 
            // dtpHorFinal
            // 
            dtpHorFinal.EditValue = new DateTime(2013, 6, 10, 16, 58, 48, 0);
            dtpHorFinal.Location = new Point(179, 46);
            dtpHorFinal.MenuManager = ribbon;
            dtpHorFinal.Name = "dtpHorFinal";
            dtpHorFinal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            dtpHorFinal.Properties.CalendarDateEditing = false;
            dtpHorFinal.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            dtpHorFinal.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            dtpHorFinal.Properties.DisplayFormat.FormatString = "t";
            dtpHorFinal.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            dtpHorFinal.Properties.EditFormat.FormatString = "t";
            dtpHorFinal.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            dtpHorFinal.Properties.MaskSettings.Set("mask", "t");
            dtpHorFinal.Size = new Size(91, 20);
            dtpHorFinal.TabIndex = 7;
            // 
            // dtpFecFinal
            // 
            dtpFecFinal.EditValue = new DateTime(2013, 6, 10, 16, 58, 56, 0);
            dtpFecFinal.Location = new Point(73, 46);
            dtpFecFinal.MenuManager = ribbon;
            dtpFecFinal.Name = "dtpFecFinal";
            dtpFecFinal.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            dtpFecFinal.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            dtpFecFinal.Size = new Size(100, 20);
            dtpFecFinal.TabIndex = 6;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(6, 49);
            label1.Name = "label1";
            label1.Size = new Size(33, 13);
            label1.TabIndex = 5;
            label1.Text = "Final:";
            // 
            // dtpHorInicio
            // 
            dtpHorInicio.EditValue = new DateTime(2013, 6, 10, 16, 58, 42, 0);
            dtpHorInicio.Location = new Point(179, 20);
            dtpHorInicio.MenuManager = ribbon;
            dtpHorInicio.Name = "dtpHorInicio";
            dtpHorInicio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            dtpHorInicio.Properties.CalendarDateEditing = false;
            dtpHorInicio.Properties.CalendarTimeEditing = DevExpress.Utils.DefaultBoolean.True;
            dtpHorInicio.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            dtpHorInicio.Properties.DisplayFormat.FormatString = "t";
            dtpHorInicio.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            dtpHorInicio.Properties.EditFormat.FormatString = "t";
            dtpHorInicio.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            dtpHorInicio.Properties.MaskSettings.Set("mask", "t");
            dtpHorInicio.Size = new Size(91, 20);
            dtpHorInicio.TabIndex = 2;
            // 
            // dtpFecInicio
            // 
            dtpFecInicio.EditValue = new DateTime(2013, 6, 10, 16, 58, 52, 0);
            dtpFecInicio.Location = new Point(73, 20);
            dtpFecInicio.MenuManager = ribbon;
            dtpFecInicio.Name = "dtpFecInicio";
            dtpFecInicio.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            dtpFecInicio.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            dtpFecInicio.Size = new Size(100, 20);
            dtpFecInicio.TabIndex = 1;
            // 
            // lbliniciocci
            // 
            lbliniciocci.AutoSize = true;
            lbliniciocci.Location = new Point(6, 23);
            lbliniciocci.Name = "lbliniciocci";
            lbliniciocci.Size = new Size(36, 13);
            lbliniciocci.TabIndex = 0;
            lbliniciocci.Text = "Inicio:";
            // 
            // groupBox3
            // 
            groupBox3.Controls.Add(cmbid_patients);
            groupBox3.Controls.Add(label2);
            groupBox3.Location = new Point(12, 214);
            groupBox3.Name = "groupBox3";
            groupBox3.Size = new Size(566, 44);
            groupBox3.TabIndex = 1;
            groupBox3.TabStop = false;
            // 
            // cmbid_patients
            // 
            cmbid_patients.Location = new Point(73, 15);
            cmbid_patients.Name = "cmbid_patients";
            cmbid_patients.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            cmbid_patients.Size = new Size(487, 20);
            cmbid_patients.TabIndex = 1;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(6, 18);
            label2.Name = "label2";
            label2.Size = new Size(52, 13);
            label2.TabIndex = 0;
            label2.Text = "Paciente:";
            // 
            // groupBox4
            // 
            groupBox4.Controls.Add(cmbusuasignacion);
            groupBox4.Controls.Add(label4);
            groupBox4.Location = new Point(12, 164);
            groupBox4.Name = "groupBox4";
            groupBox4.Size = new Size(566, 44);
            groupBox4.TabIndex = 0;
            groupBox4.TabStop = false;
            // 
            // cmbusuasignacion
            // 
            cmbusuasignacion.Location = new Point(73, 15);
            cmbusuasignacion.Name = "cmbusuasignacion";
            cmbusuasignacion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            cmbusuasignacion.Size = new Size(487, 20);
            cmbusuasignacion.TabIndex = 1;
            cmbusuasignacion.TabStop = false;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(6, 18);
            label4.Name = "label4";
            label4.Size = new Size(43, 13);
            label4.TabIndex = 0;
            label4.Text = "Doctor:";
            // 
            // FCalAgenda
            // 
            AutoScaleDimensions = new SizeF(6F, 13F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(590, 509);
            Controls.Add(groupBox4);
            Controls.Add(groupBox3);
            Controls.Add(groupBox2);
            Controls.Add(groupBox1);
            Controls.Add(ribbonStatusBar);
            Controls.Add(ribbon);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            IconOptions.Icon = (Icon)resources.GetObject("FCalAgenda.IconOptions.Icon");
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FCalAgenda";
            Ribbon = ribbon;
            ShowInTaskbar = false;
            StartPosition = FormStartPosition.CenterParent;
            StatusBar = ribbonStatusBar;
            Text = "Cita";
            Activated += fCitAgendaDialog_Activated;
            Load += fCitAgendaDialog_Load;
            ((System.ComponentModel.ISupportInitialize)ribbon).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtasuntocci.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtdescripcioncci.Properties).EndInit();
            groupBox1.ResumeLayout(false);
            groupBox1.PerformLayout();
            groupBox2.ResumeLayout(false);
            groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)cmbLabel.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)cmbLugarcci.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)dtpHorFinal.Properties.CalendarTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)dtpHorFinal.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)dtpFecFinal.Properties.CalendarTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)dtpFecFinal.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)dtpHorInicio.Properties.CalendarTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)dtpHorInicio.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)dtpFecInicio.Properties.CalendarTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)dtpFecInicio.Properties).EndInit();
            groupBox3.ResumeLayout(false);
            groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)cmbid_patients.Properties).EndInit();
            groupBox4.ResumeLayout(false);
            groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)cmbusuasignacion.Properties).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarButtonItem barButtonGuardar;
        private DevExpress.XtraBars.BarButtonItem barButtonEliminar;
        private DevExpress.XtraBars.BarButtonItem barButtonCancelar;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        internal System.Windows.Forms.Label lblasuntocci;
        private DevExpress.XtraEditors.TextEdit txtasuntocci;
        internal System.Windows.Forms.Label lbldescripcioncci;
        private DevExpress.XtraEditors.MemoEdit txtdescripcioncci;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        internal System.Windows.Forms.Label lbliniciocci;
        private DevExpress.XtraEditors.DateEdit dtpHorInicio;
        private DevExpress.XtraEditors.DateEdit dtpFecInicio;
        private DevExpress.XtraEditors.DateEdit dtpHorFinal;
        private DevExpress.XtraEditors.DateEdit dtpFecFinal;
        internal System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraEditors.LookUpEdit cmbid_patients;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.LookUpEdit cmbLugarcci;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barButtonPaciente;
        internal System.Windows.Forms.Label lblLabel;
        private DevExpress.XtraEditors.LookUpEdit cmbLabel;
        private DevExpress.XtraBars.BarButtonItem barButtonCosto;
        private DevExpress.XtraBars.BarButtonItem barButtonFalta;
        private System.Windows.Forms.GroupBox groupBox4;
        private DevExpress.XtraEditors.LookUpEdit cmbusuasignacion;
        internal System.Windows.Forms.Label label4;
    }
}