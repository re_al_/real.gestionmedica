﻿using System.Data;
using DevExpress.XtraBars;
using DevExpress.XtraMap.Native;
using DevExpress.XtraScheduler;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
using ReAl.GestionMedica.BackendConnector.Utils;
using ReAl.GestionMedica.Class;
using static DevExpress.Xpo.Helpers.AssociatedCollectionCriteriaHelper;

namespace ReAl.GestionMedica.App.CAL
{
    public partial class FCalAgenda : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        #region Fields

        private readonly Appointment _apt = null;

        private readonly SchedulerControl _control;

        private readonly PatAppointments _myObj = null;

        private bool _openRecurrenceForm = false;

        private long _patientId = -1;

        #endregion Fields

        #region Constructors

        public FCalAgenda()
        {
            this.InitializeComponent();
        }

        public FCalAgenda(PatPatients obj)
        {
            this.InitializeComponent();

            this._openRecurrenceForm = false;
            this._apt = null;
            this._myObj = null;

            this.cmbid_patients.EditValue = Int64.Parse(obj.IdPatients.ToString());
        }

        public FCalAgenda(SchedulerControl control, PatAppointments obj)
        {
            this.InitializeComponent();

            //var myApt = new Appointment();
            this._control = control;

            var myApt = _control.DataStorage.CreateAppointment(AppointmentType.Normal);
            myApt.Start = obj.TimeStart;
            myApt.End = obj.TimeEnd;
            myApt.Subject = obj.Subject;
            myApt.Description = obj.Description;
            myApt.CustomFields[PatAppointments.Fields.IdAppointments.ToString()] = obj.IdAppointments;
            myApt.StatusId = obj.ApiStatus == CApi.Estado.ACTIVE.ToString() ? 2 : 1;
            myApt.LabelId = obj.Labels;

            this._openRecurrenceForm = false;
            this._apt = myApt;
            this._myObj = obj;
            this._patientId = obj.IdPatients;

            this.cmbid_patients.ItemIndex = 0;
        }

        public FCalAgenda(SchedulerControl control, Appointment myApt, bool openRecurrenceForm)
        {
            this.InitializeComponent();

            this._openRecurrenceForm = openRecurrenceForm;
            this._apt = myApt;
            this._control = control;

            if (this._apt.CustomFields[PatAppointments.Fields.IdAppointments.ToString()] != null)
            {
                var strAlo = this._apt.CustomFields[PatAppointments.Fields.IdAppointments.ToString()].ToString();
                try
                {
                    if (!string.IsNullOrEmpty(strAlo))
                    {
                        long idApt = 0;
                        if (long.TryParse(strAlo, out idApt))
                        {
                            if (idApt > 0)
                            {
                                var service = new PatAppointmentsController();
                                var obj = service.GetById(idApt);
                                if (obj != null)
                                {
                                    this._myObj = obj;
                                    this._patientId = obj.IdPatients;
                                }
                            }
                        }
                    }
                }
                catch (Exception)
                {
                    this._myObj = null;
                }
            }

            this.cmbid_patients.ItemIndex = 0;
        }

        #endregion Constructors

        #region Properties

        protected AppointmentStorage appointments
        {
            get
            {
                return this._control.Storage.Appointments;
            }
        }

        #endregion Properties

        #region Methods

        private void barButtonCancelar_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }

        private void barButtonCosto_ItemClick(object sender, ItemClickEventArgs e)
        {
            var frmCosto = new FCalAgendaCosto(_myObj);
            frmCosto.ShowDialog();
        }

        private void barButtonEliminar_ItemClick(object sender, ItemClickEventArgs e)
        {
            var bProcede = false;
            try
            {
                if (_myObj != null)
                {
                    //Eliminamos el registro
                    var service = new PatPatientsController();
                    service.Delete(_myObj.IdAppointments);
                    bProcede = true;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
            if (bProcede)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void barButtonFalta_ItemClick(object sender, ItemClickEventArgs e)
        {
            var frmFalta = new FCalAgendaAusencia(_myObj);
            frmFalta.ShowDialog();
        }

        private void barButtonGuardar_ItemClick(object sender, ItemClickEventArgs e)
        {
            var bProcede = false;
            try
            {
                if (this.cmbid_patients.EditValue == null)
                    throw new Exception("Debe elegir un Paciente para crear la Cita");
                if (this.cmbid_patients.ItemIndex == -1)
                    throw new Exception("Debe elegir un Paciente para crear la Cita");

                var dtpInicio = this.dtpFecInicio.DateTime.Date + this.dtpHorInicio.DateTime.TimeOfDay;
                var dtpFinal = this.dtpFecFinal.DateTime.Date + this.dtpHorFinal.DateTime.TimeOfDay;

                var rn = new PatAppointmentsController();
                if (this._myObj == null)
                {
                    //Insertamos el registro
                    var obj = new PatAppointments();
                    obj.IdPatients = long.Parse(this.cmbid_patients.EditValue.ToString());
                    obj.IdUsers = long.Parse(this.cmbusuasignacion.EditValue.ToString());
                    obj.Subject = this.txtasuntocci.Text;
                    obj.Description = this.txtdescripcioncci.Text;
                    obj.TimeStart = dtpInicio;
                    obj.TimeEnd = dtpFinal;
                    obj.Location = this.cmbLugarcci.Text;
                    obj.Labels = 2; //Programado
                    obj.LeavingReason = "";
                    obj.ApiUsucre = cParametrosApp.AppRestUser.Login;
                    if (rn.Create(obj))
                    {
                        bProcede = true;
                    }
                }
                else
                {
                    //Actualizamos el registro
                    this._myObj.IdPatients = long.Parse(this.cmbid_patients.EditValue.ToString());
                    this._myObj.IdUsers = long.Parse(this.cmbusuasignacion.EditValue.ToString());
                    this._myObj.Subject = this.txtasuntocci.Text;
                    this._myObj.Description = this.txtdescripcioncci.Text;
                    this._myObj.TimeStart = dtpInicio;
                    this._myObj.TimeEnd = dtpFinal;
                    this._myObj.Location = this.cmbLugarcci.Text;
                    this._myObj.Labels = int.Parse(this.cmbLabel.EditValue.ToString());
                    this._myObj.ApiUsumod = cParametrosApp.AppRestUser.Login;
                    this._myObj.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                    if (rn.Update(_myObj))
                    {
                        bProcede = true;
                    }

                    switch (this.cmbLabel.EditValue.ToString())
                    {
                        case "1": //Ausencia
                            var frmFalta = new FCalAgendaAusencia(_myObj);
                            frmFalta.ShowDialog();
                            break;

                        case "3": //Si vino
                            var frmCosto = new FCalAgendaCosto(_myObj);
                            frmCosto.ShowDialog();
                            break;
                    }

                    bProcede = true;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
            if (bProcede)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void barButtonPaciente_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (this._myObj != null)
            {
                if (cmbid_patients.EditValue.ToString() != _myObj.IdPatients.ToString())
                {
                    MessageBox.Show(
                        "Ha realizado cambios en la Cita que primero deben ser Guardados" + Environment.NewLine
                        + "Presione el Botón GUARDAR e intente nuevamente",
                        "Cambios realizados",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                else
                {
                    var service = new PatPatientsController();
                    var obj = service.GetById(int.Parse(_myObj.IdPatients.ToString()));
                    if (obj != null)
                    {
                        cParametrosApp.AppCita = _myObj;
                        cParametrosApp.AppPaciente = obj;

                        this.DialogResult = DialogResult.Retry;
                        this.Close();
                    }
                }
            }
        }

        private void CargarCmbStatus()
        {
            var dtStatus = new DataTable();
            var dcId = new DataColumn("id", typeof(int));
            dtStatus.Columns.Add(dcId);
            var dcGender = new DataColumn("status", typeof(string));
            dtStatus.Columns.Add(dcGender);

            var dr = dtStatus.NewRow();
            dr["id"] = 0;
            dr["status"] = "Solicitado";
            dtStatus.Rows.Add(dr);
            dr = dtStatus.NewRow();
            dr["id"] = 1;
            dr["status"] = "No se presentó";
            dtStatus.Rows.Add(dr);
            dr = dtStatus.NewRow();
            dr["id"] = 2;
            dr["status"] = "Programado";
            dtStatus.Rows.Add(dr);
            dr = dtStatus.NewRow();
            dr["id"] = 3;
            dr["status"] = "Atendido";
            dtStatus.Rows.Add(dr);

            this.cmbLabel.Properties.DataSource = dtStatus;

            if (dtStatus.Rows.Count > 0)
            {
                this.cmbLabel.Properties.ValueMember = "id";
                this.cmbLabel.Properties.DisplayMember = "status";
                this.cmbLabel.Properties.PopulateColumns();
                this.cmbLabel.Properties.ShowHeader = false;
                if (this.cmbLabel.Properties.Columns["id"] != null)
                    this.cmbLabel.Properties.Columns["id"].Visible = false;
                this.cmbLabel.Properties.ForceInitialize();
            }
        }

        private void CargarDatos()
        {
            try
            {
                if (this._myObj != null)
                {
                    this.cmbLabel.Visible = true;
                    this.lblLabel.Visible = true;

                    this.cmbid_patients.EditValue = long.Parse(this._myObj.IdPatients.ToString());
                    this.cmbusuasignacion.EditValue = this._myObj.IdUsers.ToString();
                    this.txtasuntocci.Text = this._myObj.Subject;
                    this.txtdescripcioncci.Text = this._myObj.Description;
                    this.dtpFecInicio.DateTime = this._myObj.TimeStart;
                    this.dtpHorInicio.DateTime = this._myObj.TimeStart;
                    this.dtpFecFinal.DateTime = this._myObj.TimeEnd;
                    this.dtpHorFinal.DateTime = this._myObj.TimeEnd;
                    this.cmbLugarcci.Text = this._myObj.Location;
                    this.cmbLabel.EditValue = this._myObj.Labels;

                    this.barButtonEliminar.Enabled = true;
                    this.barButtonPaciente.Enabled = true;

                    switch (_myObj.Labels)
                    {
                        case 0: //Solicitada
                            barButtonFalta.Enabled = true;
                            barButtonCosto.Enabled = false;
                            barButtonGuardar.Enabled = true;
                            barButtonEliminar.Enabled = false;
                            break;

                        case 1: //No vino
                            barButtonFalta.Enabled = true;
                            barButtonCosto.Enabled = false;
                            barButtonGuardar.Enabled = false;
                            barButtonEliminar.Enabled = false;
                            break;

                        case 2: //Programada
                            barButtonFalta.Enabled = false;
                            barButtonCosto.Enabled = false;
                            barButtonGuardar.Enabled = true;
                            barButtonEliminar.Enabled = true;
                            break;

                        case 3: //Se Presento
                            barButtonFalta.Enabled = false;
                            barButtonCosto.Enabled = true;
                            barButtonGuardar.Enabled = false;
                            barButtonEliminar.Enabled = false;
                            break;
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void CargarDoctores()
        {
            try
            {
                var rn = new SegUsersController();
                var list = rn.GetDoctors();
                if (list != null)
                {
                    this.cmbusuasignacion.Properties.DataSource = list;
                    if (list.Count > 0)
                    {
                        this.cmbusuasignacion.Properties.ValueMember = SegUsers.Fields.IdUsers.ToString();
                        this.cmbusuasignacion.Properties.DisplayMember = SegUsers.Fields.FullName.ToString();
                        this.cmbusuasignacion.Properties.PopulateColumns();
                        this.cmbusuasignacion.Properties.ShowHeader = false;
                        this.cmbusuasignacion.Properties.Columns[SegUsers.Fields.IdUsers.ToString()].Visible = false;
                        this.cmbusuasignacion.Properties.ForceInitialize();
                        this.cmbusuasignacion.ItemIndex = 0;
                    }
                }
               
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void CargarPacientes()
        {
            try
            {
                var rn = new PatPatientsController();
                var list = new List<PatPatients>();
                if (_patientId > 0)
                {
                    var obj = rn.GetById(_patientId);
                    list.Add(obj);
                }
                else
                {
                    list = cParametrosApp.AppPatientsList;
                }
                if (list != null)
                {
                    this.cmbid_patients.Properties.DataSource = list;

                    if (list.Count > 0)
                    {
                        this.cmbid_patients.Properties.ValueMember = PatPatients.Fields.IdPatients.ToString();
                        this.cmbid_patients.Properties.DisplayMember = PatPatients.Fields.FullName.ToString();
                        this.cmbid_patients.Properties.PopulateColumns();
                        this.cmbid_patients.Properties.ShowHeader = false;
                        this.cmbid_patients.Properties.Columns[PatPatients.Fields.FullName.ToString()].Visible = false;
                        this.cmbid_patients.Properties.Columns[PatPatients.Fields.IdPatients.ToString()].Visible = false;
                        this.cmbid_patients.Properties.ForceInitialize();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void cmbLabel_EditValueChanged(object sender, EventArgs e)
        {
            //switch (cmbLabel.EditValue.ToString())
            //{
            //    case "1": //No vino
            //        barButtonFalta.Enabled = true;
            //        barButtonCosto.Enabled = false;
            //        break;
            //    case "2":
            //        barButtonFalta.Enabled = false;
            //        barButtonCosto.Enabled = false;
            //        break;
            //    case "3": //Vino
            //        barButtonFalta.Enabled = false;
            //        barButtonCosto.Enabled = true;
            //        break;

            //}
        }

        private void fCitAgendaDialog_Activated(object sender, EventArgs e)
        {
            if (this._openRecurrenceForm)
            {
                this._openRecurrenceForm = false;
            }
        }

        private void fCitAgendaDialog_Load(object sender, EventArgs e)
        {
            this.CargarCmbStatus();
            this.CargarDoctores();
            this.CargarPacientes();
            this.cmbLugarcci.Text = "Consultorio";
            this.cmbLabel.ItemIndex = 1; //Programado

            //cargarCmbLugares();

            if (this._apt != null)
            {
                this.dtpFecInicio.EditValue = this._apt.Start.Date;
                this.dtpHorInicio.EditValue = this._apt.Start;
                this.dtpFecFinal.EditValue = this._apt.Start.Date;
                this.dtpHorFinal.EditValue = this._apt.Start.AddMinutes(30);
            }
            else
            {
                this.dtpFecInicio.EditValue = DateTime.Now;
                this.dtpHorInicio.EditValue = DateTime.Now;
                this.dtpFecFinal.EditValue = DateTime.Now;
                this.dtpHorFinal.EditValue = DateTime.Now.AddMinutes(30);
            }

            if (this._myObj != null) this.CargarDatos();
        }

        #endregion Methods
    }
}