﻿


namespace ReAl.GestionMedica.App.CAL
{
    partial class FCalAgendaCosto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FCalAgendaCosto));
            this.lblaltocpl = new System.Windows.Forms.Label();
            this.nudCosto = new DevExpress.XtraEditors.TextEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.nudCosto.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblaltocpl
            // 
            this.lblaltocpl.AutoSize = true;
            this.lblaltocpl.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblaltocpl.Location = new System.Drawing.Point(5, 28);
            this.lblaltocpl.Name = "lblaltocpl";
            this.lblaltocpl.Size = new System.Drawing.Size(74, 27);
            this.lblaltocpl.TabIndex = 0;
            this.lblaltocpl.Text = "Costo:";
            // 
            // nudCosto
            // 
            this.nudCosto.EditValue = "0.00";
            this.nudCosto.Location = new System.Drawing.Point(101, 25);
            this.nudCosto.Name = "nudCosto";
            this.nudCosto.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.nudCosto.Properties.Appearance.Options.UseFont = true;
            this.nudCosto.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.nudCosto.Properties.Mask.EditMask = "n2";
            this.nudCosto.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.nudCosto.Properties.NullText = "0";
            this.nudCosto.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.nudCosto.Size = new System.Drawing.Size(170, 32);
            this.nudCosto.TabIndex = 1;
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.nudCosto);
            this.groupControl1.Controls.Add(this.lblaltocpl);
            this.groupControl1.Location = new System.Drawing.Point(12, 12);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(276, 67);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Especifique el Costo de la Consulta";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnCancelar.Location = new System.Drawing.Point(66, 85);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(108, 34);
            this.btnCancelar.TabIndex = 1;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAceptar.Location = new System.Drawing.Point(180, 85);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(108, 34);
            this.btnAceptar.TabIndex = 2;
            this.btnAceptar.Text = "&Aceptar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // FCalAgendaCosto
            // 
            this.AcceptButton = this.btnAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(300, 131);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.btnAceptar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("FCalAgendaCosto.IconOptions.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FCalAgendaCosto";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Costo de la Consulta";
            this.Load += new System.EventHandler(this.fCalAgendaCosto_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudCosto.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.Label lblaltocpl;
        private DevExpress.XtraEditors.TextEdit nudCosto;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
    }
}