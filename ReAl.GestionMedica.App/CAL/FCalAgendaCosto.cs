﻿using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
using ReAl.GestionMedica.BackendConnector.Utils;
using ReAl.GestionMedica.Class;

namespace ReAl.GestionMedica.App.CAL
{
    public partial class FCalAgendaCosto : DevExpress.XtraEditors.XtraForm
    {
        #region Fields

        private readonly PatAppointments _myObj = null;

        private bool _bExisteCobro = false;

        #endregion Fields

        #region Constructors

        public FCalAgendaCosto(PatAppointments obj)
        {
            InitializeComponent();
            _myObj = obj;
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var bProcede = false;

            try
            {
                //Obtenemos los datos del Paciente
                var servicePat = new PatPatientsController();
                var objPat = servicePat.GetById(long.Parse(_myObj.IdPatients.ToString()));
                if (objPat != null)
                {
                    //Creamos el Pago pendiente
                    var serviceFin = new FinAccountsController();
                    var objFin = new FinAccounts();
                    if (_bExisteCobro)
                    {
                        objFin = serviceFin.GetbyAppointmentId(_myObj.IdAppointments);
                        if (objFin != null)
                        {
                            objFin.Amount = Decimal.Parse(nudCosto.EditValue.ToString());
                            objFin.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                            objFin.ApiUsumod = cParametrosApp.AppRestUser.Login;
                            serviceFin.Update(objFin);
                        }
                    }
                    else
                    {
                        var obj = new FinAccounts();
                        obj.Name = "Consulta Paciente";
                        obj.IdAppointments = _myObj.IdAppointments;
                        obj.IdCategories = 1;
                        obj.Description = "Paciente " + objPat.FullName;
                        obj.AccountDate = _myObj.TimeStart;
                        obj.Type = "I";
                        obj.Amount = Decimal.Parse(nudCosto.EditValue.ToString());
                        obj.ApiUsucre = cParametrosApp.AppRestUser.Login;
                        serviceFin.CreateCharge(obj);
                    }

                    //Cambiamos el registro de la Cita
                    var serviceAppoint = new PatAppointmentsController();
                    _myObj.Labels = 3;
                    _myObj.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                    _myObj.ApiUsumod = cParametrosApp.AppRestUser.Login;
                    if (serviceAppoint.Update(_myObj))
                    {
                        bProcede = true;
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }

            if (bProcede)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void CargarDatos()
        {
            try
            {
                //Obtenemos el Costo SI ES QUE EXISTE
                var service = new FinAccountsController();
                var obj = service.GetbyAppointmentId(_myObj.IdAppointments);
                if (obj != null)
                {
                    nudCosto.EditValue = obj.Amount;
                    _bExisteCobro = true;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void fCalAgendaCosto_Load(object sender, EventArgs e)
        {
            if (_myObj != null)
            {
                CargarDatos();
            }
        }

        #endregion Methods
    }
}