﻿using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.Json;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
using ReAl.Utils;

namespace ReAl.GestionMedica.App
{
    public partial class FLogin : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public FLogin()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods
        private bool AutenticarUsuarioRest(ref string strValidacion)
        {
            bool success = false;
            var authServices = new AuthServices();
            var usersServices = new SegUsersController();
            var resultData = authServices.Login(out success, txtUsuario.Text, txtPass.Text);
            if (success)
            {
                //Login
                var options = new JsonSerializerOptions()
                {
                    PropertyNameCaseInsensitive = true,
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                };
                string response = resultData.data.ToString();
                Login user = System.Text.Json.JsonSerializer.Deserialize<Login>(response, options)!;

                //Get Current user data
                var segUsers = usersServices.Current();
                cParametrosApp.AppRestUser = segUsers;
            }
            else
            {
                strValidacion = resultData;
            }

            return success;
        }
        
        private void btnAceptar_Click(object sender, EventArgs e)
        {
            try
            {
                var strTextoValidacion = "";
                if (this.ValidarUsuario(ref strTextoValidacion) && this.AutenticarUsuarioRest(ref strTextoValidacion))
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else
                {
                    MessageBox.Show(strTextoValidacion, "Ingreso al Sistema", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog(this);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Application.Exit();
        }

        private void fLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void fLogin_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F11)
                {
                    /*
                    var frm = new FConfConn();
                    frm.ShowDialog();

                    if (frm.DialogResult == DialogResult.OK) cargarConfiguracionCOnexion();
                    */
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void fLogin_Load(object sender, EventArgs e)
        {
            try
            {
                //Get version app
                object[] customAttributes = this.GetType().Assembly.GetCustomAttributes(false);
                string assemblyGuid = string.Empty;
                foreach (object attribute in customAttributes)
                {
                    if (attribute.GetType() == typeof(AssemblyFileVersionAttribute))
                    {
                        assemblyGuid = ((AssemblyFileVersionAttribute)attribute).Version;
                        break;
                    }
                }
                lblVersion.Text = "Build: " + assemblyGuid;

                //if (CParametros.BConfigConectionWithFile)
                //    cargarConfiguracionCOnexion();
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        /*
        private void FuncionesExtras()
        {
            try
            {
                if (1 == 2)
                {
                    var inicio1 = DateTime.Now;
                    var final1 = DateTime.Now;
                    var inicio2 = DateTime.Now;
                    var final2 = DateTime.Now;
                    try
                    {
                        var myConn = new SqlConnection();
                        myConn.ConnectionString = "Data Source=" + CParametros.Server + ";Initial Catalog="
                                                  + CParametros.Bd + ";User ID=" + CParametros.UserDefault
                                                  + ";Password=" + CParametros.PassDefault;

                        var dt = new DataTable();

                        var query = "SELECT top 200000 * FROM TablaPrueba";
                        var command = new SqlCommand(query, myConn);

                        inicio1 = DateTime.Now;
                        command.Connection.Open();
                        var dr = (DbDataReader)command.ExecuteReader(CommandBehavior.CloseConnection);
                        //dt.Load(dr);
                        dr.Close();
                        if (command.Connection.State != ConnectionState.Closed) command.Connection.Close();
                        final1 = DateTime.Now;

                        inicio2 = DateTime.Now;
                        var da = new SqlDataAdapter();
                        da = new SqlDataAdapter(command);
                        da.Fill(dt);
                        final2 = DateTime.Now;

                        var strMensaje = "DATA READER:" + Environment.NewLine + "Inicio: " + inicio1.Ticks
                                         + Environment.NewLine + "Final: " + final1.Ticks + Environment.NewLine
                                         + "Diferencia: " + (final1.Ticks - inicio1.Ticks) + Environment.NewLine
                                         + Environment.NewLine + "DATA ADAPTER:" + Environment.NewLine + "Inicio: "
                                         + inicio2.Ticks + Environment.NewLine + "Final: " + final2.Ticks
                                         + Environment.NewLine + "Diferencia: " + (final2.Ticks - inicio2.Ticks);
                        Clipboard.SetText(strMensaje);
                        MessageBox.Show(strMensaje);
                    }
                    catch (Exception exp)
                    {
                        MessageBox.Show(exp.Message + Environment.NewLine + "DATA READER:" + Environment.NewLine + "Inicio: " + inicio1.ToString() + Environment.NewLine + "Final: " + final1.ToString() +
                                        Environment.NewLine + Environment.NewLine + "DATA ADAPTER:" + Environment.NewLine +
                                        "Inicio: " + inicio2.ToString() + Environment.NewLine + "Final: " + final2.ToString());
                    }
                }

                if (1 == 2)
                {
                    var directorio = cFuncionesFicheros.GenerarListaArchivos("D:\\Recetas\\", "*.rft");

                    var imagen = new byte[0];
                    var rn = new RnClaPlantillas();
                    var ent = new ClaTemplates();

                    var rtfAux = new RichTextBox();

                    foreach (var archivo in directorio)
                    {
                        imagen = cFuncionesImagenes.ImageReadBinaryFile(archivo);

                        var rtfText = File.ReadAllText(archivo);
                        rtfAux.Rtf = rtfText;

                        ent = new ClaTemplates();
                        ent.altocpl = 21;
                        ent.anchocpl = (decimal)13.7;

                        ent.rtfcpl = imagen;
                        ent.textocpl = rtfAux.Text;
                        ent.margensupcpl = (decimal)3.5;
                        ent.margendercpl = 1;
                        ent.margenizqcpl = (decimal)5.08;
                        ent.margeninfcpl = 0;

                        ent.nombrecpl = archivo.Substring(
                            archivo.LastIndexOf("\\") + 1, archivo.Length - archivo.LastIndexOf("\\") - 5);
                        ent.descripcioncpl = archivo;
                        ent.usucre = "postgres";
                        rn.Insert(ent);
                    }
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }
        */

        private void txtPass_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.F11)
                {
                    /*
                    var frm = new FConfConn();
                    frm.ShowDialog();

                    if (frm.DialogResult == DialogResult.OK) cargarConfiguracionCOnexion();
                    */
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void txtUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                /*
                if (e.KeyCode == Keys.F11)
                {
                    var frm = new FConfConn();
                    frm.ShowDialog();

                    if (frm.DialogResult == DialogResult.OK) cargarConfiguracionCOnexion();
                }
                */
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private bool ValidarUsuario(ref string strValidacion)
        {
            var bProcede = true;
            var pTieneFoco = false;

            //Validamos el Login
            if (string.IsNullOrEmpty(txtUsuario.Text))
            {
                strValidacion = Environment.NewLine + "Debe ingresar el Nombre de Usuario";
                bProcede = false;
                if (!pTieneFoco) txtUsuario.Focus();
            }
            //Validamos el Password
            if (string.IsNullOrEmpty(txtPass.Text))
            {
                strValidacion = Environment.NewLine + "Debe ingresar su Contraseña";
                bProcede = false;
                if (!pTieneFoco) txtPass.Focus();
            }

            return bProcede;
        }

        #endregion Methods

        public GraphicsPath CreateFormRegion(int cornerRadius)
        {
            GraphicsPath GrpRect = new GraphicsPath();
            int width = Width + 1;
            int height = Height + 1;
            GrpRect.AddArc(new Rectangle(0, 0, cornerRadius * 2, cornerRadius * 2), 180f, 90f);//left-top
            GrpRect.AddArc(new Rectangle((width - cornerRadius * 2) - 1, 0, cornerRadius * 2, cornerRadius * 2), -90f, 90f);//right-top
            GrpRect.AddArc(new Rectangle((width - cornerRadius * 2) - 1, (height - cornerRadius * 2) - 1, cornerRadius * 2, cornerRadius * 2), 0f, 90f);//right-bottom
            GrpRect.AddArc(new Rectangle(0, (height - cornerRadius * 2) - 1, cornerRadius * 2, cornerRadius * 2), 90f, 90f);//left-bottom
            GrpRect.CloseAllFigures();
            return GrpRect;
        }

        protected override void OnShown(EventArgs e)
        {
            base.OnShown(e);
            UpdateRegion();
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            UpdateRegion();
        }

        private void UpdateRegion()
        {
            Region prevRgn = Region;
            Region = new Region(CreateFormRegion(8));
            if (prevRgn != null)
                prevRgn.Dispose();
        }
    }
}