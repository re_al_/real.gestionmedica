using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;
using ReAl.Utils;

namespace ReAl.GestionMedica.App.SEG
{
    public partial class LSegUsuarios : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public LSegUsuarios()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        public void CargarListado()
        {
            try
            {
                var rn = new SegUsersController();
                var dt = rn.GetActive();
                gcSegUsuarios.DataSource = dt;
                if (dt.Count > 0)
                {
                    gvSegUsuarios.PopulateColumns();
                    gvSegUsuarios.OptionsBehavior.Editable = false;
                    gvSegUsuarios.Columns[SegUsers.Fields.IdUsers.ToString()].Visible = false;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void EliminarRegistro()
        {
            try
            {
                if (gvSegUsuarios.RowCount <= 0) return;
                var rn = new SegUsersController();

                //Apropiamos los valores del Grid
                int idsus = int.Parse(gvSegUsuarios.GetFocusedRowCellValue(SegUsers.Fields.IdUsers.ToString()).ToString());

                //Obtenemos el objeto
                var obj = rn.GetById(idsus);

                if (obj != null)
                {
                    var bProcede = false;
                    try
                    {
                        if (MessageBox.Show("¿Desea resetear la Contraseña del usuario \"" + obj.Login + "\"?", "Cambio de Contraseña", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            //Cambiamos el Pass:
                            obj.Password = cFuncionesEncriptacion.GenerarMd5(obj.Login + "01").ToUpper();
                            obj.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                            rn.Update(obj);
                            bProcede = true;
                        }
                    }
                    catch (Exception exp)
                    {
                        var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                        frm.ShowDialog();
                    }
                    if (bProcede)
                        MessageBox.Show(
                            "Se ha cambiado la contraseña del Usuario \"" + obj.Login + "\" a \"" + obj.Login + "01"
                            + "\".",
                            "Cambio de COntraseña",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void ModificarRegistro()
        {
            try
            {
                if (gvSegUsuarios.RowCount <= 0) return;
                var rn = new SegUsersController();

                //Apropiamos los valores del Grid
                int idsus = int.Parse(gvSegUsuarios.GetFocusedRowCellValue(SegUsers.Fields.IdUsers.ToString()).ToString());

                //Obtenemos el objeto
                var obj = rn.GetById(idsus);

                if (obj != null)
                {
                    var frm = new FSegUsuarios(obj);
                    frm.ShowDialog();
                    if (frm.DialogResult == DialogResult.OK) this.CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void btnAuditoría_Click(object sender, EventArgs e)
        {
            if (gvSegUsuarios.RowCount <= 0) return;
            var rn = new SegUsersController();

            //Apropiamos los valores del Grid
            int idsus = int.Parse(gvSegUsuarios.GetFocusedRowCellValue(SegUsers.Fields.IdUsers.ToString()).ToString());

            //Obtenemos el objeto
            var obj = rn.GetById(idsus);

            if (obj != null)
            {
                //Abrimos la AUDITORIA
                var frm = new FAuditaria(obj.ApiUsucre, obj.ApiFeccre, obj.ApiUsumod, obj.ApiFecmod, obj.ApiStatus);
                frm.ShowDialog();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarRegistro();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frm = new FSegUsuarios();
            frm.ShowDialog();
            if (frm.DialogResult == DialogResult.OK) this.CargarListado();
        }

        private void gcSegUsuarios_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                EliminarRegistro();
            }
        }

        private void gcSegUsuarios_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ModificarRegistro();
        }

        private void gvSegUsuarios_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            //gvSegUsuarios.SetFocusedRowCellValue(EntSegUsuarios.Fields.usucre.ToString(),"postgres");
        }

        private void LSegUsuarios_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            CargarListado();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            ModificarRegistro();
        }

        #endregion Methods
    }
}