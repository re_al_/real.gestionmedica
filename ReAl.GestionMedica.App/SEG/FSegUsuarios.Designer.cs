


namespace ReAl.GestionMedica.App.SEG
{
	partial class FSegUsuarios
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSegUsuarios));
            lblTitulo = new Label();
            lblloginsus = new Label();
            txtloginsus = new DevExpress.XtraEditors.TextEdit();
            lblpasssus = new Label();
            txtpasssus = new DevExpress.XtraEditors.TextEdit();
            lblnombresus = new Label();
            txtnombresus = new DevExpress.XtraEditors.TextEdit();
            lblapellidosus = new Label();
            txtapellidosus = new DevExpress.XtraEditors.TextEdit();
            btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            lblidctp = new Label();
            cmbRol = new DevExpress.XtraEditors.LookUpEdit();
            ((System.ComponentModel.ISupportInitialize)txtloginsus.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtpasssus.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtnombresus.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtapellidosus.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)cmbRol.Properties).BeginInit();
            SuspendLayout();
            // 
            // lblTitulo
            // 
            lblTitulo.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            lblTitulo.Font = new Font("Microsoft Sans Serif", 9.25F, FontStyle.Bold | FontStyle.Underline);
            lblTitulo.Location = new Point(12, 9);
            lblTitulo.Name = "lblTitulo";
            lblTitulo.Size = new Size(312, 53);
            lblTitulo.TabIndex = 0;
            lblTitulo.Text = "Administracion de Usuarios";
            lblTitulo.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // lblloginsus
            // 
            lblloginsus.AutoSize = true;
            lblloginsus.Location = new Point(14, 68);
            lblloginsus.Name = "lblloginsus";
            lblloginsus.Size = new Size(36, 13);
            lblloginsus.TabIndex = 1;
            lblloginsus.Text = "Login:";
            // 
            // txtloginsus
            // 
            txtloginsus.Location = new Point(102, 65);
            txtloginsus.Name = "txtloginsus";
            txtloginsus.Properties.MaxLength = 15;
            txtloginsus.Size = new Size(222, 20);
            txtloginsus.TabIndex = 2;
            // 
            // lblpasssus
            // 
            lblpasssus.AutoSize = true;
            lblpasssus.Location = new Point(14, 93);
            lblpasssus.Name = "lblpasssus";
            lblpasssus.Size = new Size(57, 13);
            lblpasssus.TabIndex = 3;
            lblpasssus.Text = "Password:";
            // 
            // txtpasssus
            // 
            txtpasssus.Location = new Point(102, 90);
            txtpasssus.Name = "txtpasssus";
            txtpasssus.Properties.MaxLength = 256;
            txtpasssus.Properties.PasswordChar = '*';
            txtpasssus.Size = new Size(222, 20);
            txtpasssus.TabIndex = 4;
            // 
            // lblnombresus
            // 
            lblnombresus.AutoSize = true;
            lblnombresus.Location = new Point(14, 118);
            lblnombresus.Name = "lblnombresus";
            lblnombresus.Size = new Size(53, 13);
            lblnombresus.TabIndex = 5;
            lblnombresus.Text = "Nombres:";
            // 
            // txtnombresus
            // 
            txtnombresus.Location = new Point(102, 115);
            txtnombresus.Name = "txtnombresus";
            txtnombresus.Properties.MaxLength = 256;
            txtnombresus.Size = new Size(222, 20);
            txtnombresus.TabIndex = 6;
            // 
            // lblapellidosus
            // 
            lblapellidosus.AutoSize = true;
            lblapellidosus.Location = new Point(14, 143);
            lblapellidosus.Name = "lblapellidosus";
            lblapellidosus.Size = new Size(48, 13);
            lblapellidosus.TabIndex = 7;
            lblapellidosus.Text = "Apellido:";
            // 
            // txtapellidosus
            // 
            txtapellidosus.Location = new Point(102, 140);
            txtapellidosus.Name = "txtapellidosus";
            txtapellidosus.Properties.MaxLength = 256;
            txtapellidosus.Size = new Size(222, 20);
            txtapellidosus.TabIndex = 8;
            // 
            // btnCancelar
            // 
            btnCancelar.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            btnCancelar.DialogResult = DialogResult.Cancel;
            btnCancelar.Location = new Point(102, 193);
            btnCancelar.Name = "btnCancelar";
            btnCancelar.Size = new Size(108, 34);
            btnCancelar.TabIndex = 14;
            btnCancelar.Text = "&Cancelar";
            btnCancelar.Click += btnCancelar_Click;
            // 
            // btnAceptar
            // 
            btnAceptar.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            btnAceptar.Appearance.Options.UseBackColor = true;
            btnAceptar.Location = new Point(216, 193);
            btnAceptar.Name = "btnAceptar";
            btnAceptar.Size = new Size(108, 34);
            btnAceptar.TabIndex = 15;
            btnAceptar.Text = "&Registrar";
            btnAceptar.Click += btnAceptar_Click;
            // 
            // lblidctp
            // 
            lblidctp.AutoSize = true;
            lblidctp.Location = new Point(14, 168);
            lblidctp.Name = "lblidctp";
            lblidctp.Size = new Size(30, 13);
            lblidctp.TabIndex = 9;
            lblidctp.Text = "Rol::";
            // 
            // cmbRol
            // 
            cmbRol.Location = new Point(102, 165);
            cmbRol.Name = "cmbRol";
            cmbRol.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            cmbRol.Size = new Size(222, 20);
            cmbRol.TabIndex = 10;
            // 
            // FSegUsuarios
            // 
            AutoScaleDimensions = new SizeF(6F, 13F);
            AutoScaleMode = AutoScaleMode.Font;
            CancelButton = btnCancelar;
            ClientSize = new Size(337, 236);
            Controls.Add(lblidctp);
            Controls.Add(cmbRol);
            Controls.Add(btnAceptar);
            Controls.Add(btnCancelar);
            Controls.Add(lblloginsus);
            Controls.Add(txtloginsus);
            Controls.Add(lblpasssus);
            Controls.Add(txtpasssus);
            Controls.Add(lblnombresus);
            Controls.Add(txtnombresus);
            Controls.Add(lblapellidosus);
            Controls.Add(txtapellidosus);
            Controls.Add(lblTitulo);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            IconOptions.Icon = (Icon)resources.GetObject("FSegUsuarios.IconOptions.Icon");
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FSegUsuarios";
            ShowInTaskbar = false;
            StartPosition = FormStartPosition.CenterParent;
            Text = "Administracion de Usuarios";
            Load += fSegUsuarios_Load;
            ((System.ComponentModel.ISupportInitialize)txtloginsus.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtpasssus.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtnombresus.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtapellidosus.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)cmbRol.Properties).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        internal System.Windows.Forms.Label lblTitulo;
		internal System.Windows.Forms.Label lblloginsus;
		private DevExpress.XtraEditors.TextEdit txtloginsus;
		internal System.Windows.Forms.Label lblpasssus;
		private DevExpress.XtraEditors.TextEdit txtpasssus;
		internal System.Windows.Forms.Label lblnombresus;
		private DevExpress.XtraEditors.TextEdit txtnombresus;
		internal System.Windows.Forms.Label lblapellidosus;
        private DevExpress.XtraEditors.TextEdit txtapellidosus;
		private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        internal System.Windows.Forms.Label lblidctp;
        private DevExpress.XtraEditors.LookUpEdit cmbRol;
	}
}

