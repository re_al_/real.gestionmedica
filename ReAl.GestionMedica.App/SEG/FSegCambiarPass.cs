﻿using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;
using ReAl.Utils;

namespace ReAl.GestionMedica.App.SEG
{
    public partial class FSegCambiarPass : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public FSegCambiarPass()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var rn = new SegUsersController();

                if (txtNuevoPass.Text == txtRepitePass.Text)
                {
                    //Cambiamos el Pass
                    var pass = new FilterChangePassword();
                    pass.OldPassword = cFuncionesEncriptacion.GenerarMd5(txtPassActual.Text).ToUpper();
                    pass.NewPassword = cFuncionesEncriptacion.GenerarMd5(txtNuevoPass.Text).ToUpper();
                    pass.IdUsers = cParametrosApp.AppRestUser.IdUsers;

                    if (rn.UpdatePassword(pass)){
                        bProcede = true;
                    }
                    else
                    {
                        MessageBox.Show(
                           "Su contraseña actual no Coincide.",
                           "Error al cambiar Contraseña",
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show(
                           "No ha repetido correctamente su NUEVA contraseña",
                           "Error al cambiar Contraseña",
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Error);
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog(this);
            }

            if (bProcede) this.Close();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion Methods
    }
}