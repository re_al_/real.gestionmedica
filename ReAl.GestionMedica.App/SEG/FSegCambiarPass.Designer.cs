﻿


namespace ReAl.GestionMedica.App.SEG
{
    partial class FSegCambiarPass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FSegCambiarPass));
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.lblpasssus = new System.Windows.Forms.Label();
            this.txtPassActual = new DevExpress.XtraEditors.TextEdit();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNuevoPass = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRepitePass = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPassActual.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuevoPass.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepitePass.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAceptar
            // 
            this.btnAceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnAceptar.Appearance.Options.UseBackColor = true;
            this.btnAceptar.Location = new System.Drawing.Point(394, 232);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(108, 34);
            this.btnAceptar.TabIndex = 8;
            this.btnAceptar.Text = "&Registrar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Location = new System.Drawing.Point(280, 232);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(108, 34);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // lblpasssus
            // 
            this.lblpasssus.AutoSize = true;
            this.lblpasssus.Font = new System.Drawing.Font("Tahoma", 16F);
            this.lblpasssus.Location = new System.Drawing.Point(14, 93);
            this.lblpasssus.Name = "lblpasssus";
            this.lblpasssus.Size = new System.Drawing.Size(178, 27);
            this.lblpasssus.TabIndex = 1;
            this.lblpasssus.Text = "Password Actual:";
            // 
            // txtPassActual
            // 
            this.txtPassActual.Location = new System.Drawing.Point(270, 90);
            this.txtPassActual.Name = "txtPassActual";
            this.txtPassActual.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtPassActual.Properties.Appearance.Options.UseFont = true;
            this.txtPassActual.Properties.MaxLength = 256;
            this.txtPassActual.Properties.PasswordChar = '*';
            this.txtPassActual.Size = new System.Drawing.Size(232, 32);
            this.txtPassActual.TabIndex = 2;
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblTitulo.Location = new System.Drawing.Point(12, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(490, 53);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Cambiar la Contraseña de acceso al Sistema";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 16F);
            this.label1.Location = new System.Drawing.Point(14, 159);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 27);
            this.label1.TabIndex = 3;
            this.label1.Text = "Nuevo Password:";
            // 
            // txtNuevoPass
            // 
            this.txtNuevoPass.Location = new System.Drawing.Point(270, 156);
            this.txtNuevoPass.Name = "txtNuevoPass";
            this.txtNuevoPass.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtNuevoPass.Properties.Appearance.Options.UseFont = true;
            this.txtNuevoPass.Properties.MaxLength = 256;
            this.txtNuevoPass.Properties.PasswordChar = '*';
            this.txtNuevoPass.Size = new System.Drawing.Size(232, 32);
            this.txtNuevoPass.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 16F);
            this.label2.Location = new System.Drawing.Point(14, 197);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(250, 27);
            this.label2.TabIndex = 5;
            this.label2.Text = "Repita Nuevo Password:";
            // 
            // txtRepitePass
            // 
            this.txtRepitePass.Location = new System.Drawing.Point(270, 194);
            this.txtRepitePass.Name = "txtRepitePass";
            this.txtRepitePass.Properties.Appearance.Font = new System.Drawing.Font("Tahoma", 16F);
            this.txtRepitePass.Properties.Appearance.Options.UseFont = true;
            this.txtRepitePass.Properties.MaxLength = 256;
            this.txtRepitePass.Properties.PasswordChar = '*';
            this.txtRepitePass.Size = new System.Drawing.Size(232, 32);
            this.txtRepitePass.TabIndex = 6;
            // 
            // FSegCambiarPass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 278);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtRepitePass);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNuevoPass);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lblpasssus);
            this.Controls.Add(this.txtPassActual);
            this.Controls.Add(this.lblTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("FSegCambiarPass.IconOptions.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FSegCambiarPass";
            this.ShowInTaskbar = false;
            this.Text = "Cambiar Password";
            ((System.ComponentModel.ISupportInitialize)(this.txtPassActual.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNuevoPass.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtRepitePass.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        internal System.Windows.Forms.Label lblpasssus;
        private DevExpress.XtraEditors.TextEdit txtPassActual;
        internal System.Windows.Forms.Label lblTitulo;
        internal System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txtNuevoPass;
        internal System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit txtRepitePass;
    }
}