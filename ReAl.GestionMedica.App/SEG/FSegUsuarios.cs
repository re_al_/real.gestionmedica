using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;
using ReAl.Utils;

namespace ReAl.GestionMedica.App.SEG
{
    public partial class FSegUsuarios : DevExpress.XtraEditors.XtraForm
    {
        #region Fields

        private readonly SegUsers _myObj = null;

        #endregion Fields

        #region Constructors

        public FSegUsuarios(SegUsers obj)
        {
            InitializeComponent();

            _myObj = obj;
        }

        public FSegUsuarios()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var rn = new SegUsersController();
                var rnRol = new SegUsersrestrictionController();
                if (_myObj == null)
                {
                    //Insertamos el registro
                    var obj = new SegUsers();
                    obj.Login = txtloginsus.Text;
                    obj.Password = cFuncionesEncriptacion.GenerarMd5(txtpasssus.Text).ToUpper();
                    obj.FirstName = txtnombresus.Text;
                    obj.LastName = txtapellidosus.Text;
                    rn.Create(obj);

                    //Obtenemos el Usuario
                    obj = rn.GetByLogin(obj.Login);

                    if (obj != null)
                    {
                        //Ahora insertamos el Rol
                        var objRol = new SegUsersrestriction();
                        objRol.IdUsers = obj.IdUsers;
                        objRol.IdRoles = int.Parse(cmbRol.EditValue.ToString());
                        objRol.RoleActive = 1;
                        rnRol.Create(objRol);
                    }
                    bProcede = true;
                }
                else
                {
                    //Actualizamos
                    _myObj.Login = txtloginsus.Text;
                    _myObj.Password = cFuncionesEncriptacion.GenerarMd5(txtpasssus.Text).ToUpper();
                    _myObj.FirstName = txtnombresus.Text;
                    _myObj.LastName = txtapellidosus.Text;
                    _myObj.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                    rn.Update(_myObj);

                    //Actualizamos el rol el Rol
                    var objRol = new SegUsersrestriction();
                    objRol.IdUsers = _myObj.IdUsers;
                    objRol.IdRoles = int.Parse(cmbRol.EditValue.ToString());
                    objRol.RoleActive = 1;
                    objRol.ApiTransaction = CApi.Transaccion.UPDATE.ToString();

                    rnRol.UpdateByUser(objRol);
                
                    bProcede = true;
                }
            }
            catch (Exception exp)
            {
                bProcede = false;
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }

            if (bProcede)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void CargarCmbRol()
        {
            try
            {
                var rn = new SegRolesController();
                var dt = rn.GetActive();
                cmbRol.Properties.DataSource = dt;
                if (dt.Count > 0)
                {
                    cmbRol.Properties.ValueMember = SegRoles.Fields.IdRoles.ToString();
                    cmbRol.Properties.DisplayMember = SegRoles.Fields.Code.ToString();
                    cmbRol.Properties.PopulateColumns();
                    cmbRol.Properties.ShowHeader = false;
                    cmbRol.Properties.Columns[SegRoles.Fields.Description.ToString()].Visible = false;
                    cmbRol.Properties.Columns[SegRoles.Fields.IdRoles.ToString()].Visible = false;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void CargarDatos()
        {
            try
            {
                txtloginsus.Text = _myObj.Login;
                txtpasssus.Text = _myObj.Password;
                txtpasssus.Properties.ReadOnly = true;
                txtnombresus.Text = _myObj.FirstName;
                txtapellidosus.Text = _myObj.LastName;
                cmbRol.EditValue = _myObj.SegUsersRestriction.First().IdRoles;
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void fSegUsuarios_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");
            CargarCmbRol();

            if (_myObj != null) CargarDatos();
        }

        #endregion Methods
    }
}