﻿using System.Collections;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using DevExpress.DataAccess.Native.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;
using ReAl.GestionMedica.App.App_Class;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.Utils;

namespace ReAl.GestionMedica.App
{
    public partial class FErrores : DevExpress.XtraEditors.XtraForm
    {
        #region Fields

        private const int AltoConDetalles = 331;

        private const int AltoSinDetalles = 186;

        private readonly string _errorDirectory = string.Format(@"{0}\Errors", Environment.CurrentDirectory);

        private readonly ListView _listAuxiliar = new ListView();

        private readonly Exception _myExp;

        private readonly tipoError _myTipoError;

        private readonly string _myTitulo;

        private string _strErrorLog = "";

        private string _strTrazaError;

        #endregion Fields

        #region Constructors

        public FErrores(Exception exp, tipoError tipo, string titulo)
        {
            InitializeComponent();

            _myExp = exp;
            _myTipoError = tipo;
            _myTitulo = titulo;
        }

        #endregion Constructors

        #region Enums

        public enum tipoError
        {
            error,
            advertencia,
            info
        }

        #endregion Enums

        #region Methods

        [DllImport("kernel32.dll")] internal static extern uint GetTickCount();

        private string Br()
        {
            return "<br />";
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnVerDetalles_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.Size.Height == AltoSinDetalles)
                {
                    this.Size = new Size(this.Size.Width, AltoConDetalles);
                    btnVerDetalles.Text = "Ocultar &Detalles <<";
                }
                else
                {
                    this.Size = new Size(this.Size.Width, AltoSinDetalles);
                    btnVerDetalles.Text = "Ver &Detalles >>";
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private void CargarError()
        {
            try
            {
                var rn = new SegErrorsController();
                var obj = new SegErrors();
                var causa = "";
                var accion = "";
                var comentario = "";
                var origen = "";

                var resources = new ComponentResourceManager(typeof(FErrores));
                switch (_myTipoError)
                {
                    case tipoError.error:
                        pbIcono.Image = global::ReAl.GestionMedica.App.Properties.Resources.error;
                        break;

                    case tipoError.advertencia:
                        pbIcono.Image = global::ReAl.GestionMedica.App.Properties.Resources.alerta;
                        break;

                    case tipoError.info:
                        pbIcono.Image = global::ReAl.GestionMedica.App.Properties.Resources.mensaje;
                        break;
                }

                if (_myExp.GetType() == typeof(ValidationException))
                {
                    causa = StrongWeb("Causa: ") + "Se ha encontrado alertas en la información que intenta registrar.";
                    accion = StrongWeb("Acción: ") + "Debe revisar las alertas realizadas y corregirlas en el formulario.";
                    comentario = StrongWeb("Comentario: ") + "";
                    origen = StrongWeb("Origen: ") + "Información ingresada por el usuario";

                    lblMensaje.Text = _myExp.Message;
                    pbIcono.Image = global::ReAl.GestionMedica.App.Properties.Resources.alerta;
                    Text = "Atención";
                    webDetalles.DocumentText = CuerpoWeb(causa + Br() + accion + Br() + comentario + Br() + origen);
                }
                else if (_myExp.GetType() == typeof(NoNullAllowedException))
                {
                    causa = StrongWeb("Causa: ") + "Un valor del formulario está vacío o nulo.";
                    accion = StrongWeb("Acción: ") + "Debe proporcionar un dato para el valor observado";
                    comentario = StrongWeb("Comentario: ") + "";
                    origen = StrongWeb("Origen: ") + "Información ingresada por el usuario";

                    lblMensaje.Text = _myExp.Message;
                    pbIcono.Image = global::ReAl.GestionMedica.App.Properties.Resources.alerta;
                    Text = "Atención";
                    webDetalles.DocumentText = CuerpoWeb(causa + Br() + accion + Br() + comentario + Br() + origen);
                }
                else if (_myExp.GetType() == typeof(PostgresException))
                {
                    var sqlExp = (PostgresException)this._myExp;
                    var strCodigoError = "--";
                    if (sqlExp.SqlState == "P0001")
                    {
                        //Desde trigger
                        if (sqlExp.Message.Replace("P0001: ", "").Length >= 9)
                            strCodigoError = sqlExp.Message.Replace("P0001: ", "").Substring(0, 9);
                        else
                            strCodigoError = sqlExp.Message.Replace("P0001: ", "");
                    }
                    else
                    {
                        strCodigoError = "PG-" + sqlExp.SqlState;
                    }

                    obj = rn.GetByCode(strCodigoError);

                    if (obj != null)
                    {
                        causa = StrongWeb("Causa: ") + obj.Causes;
                        accion = StrongWeb("Acción: ") + obj.Actions;
                        comentario = StrongWeb("Comentario: ") + obj.Comments;
                        origen = StrongWeb("Origen: ") + sqlExp.MessageText;

                        string descripcionMsg = obj.Description;
                        var errorMsg = _myExp.Message;
                        while (descripcionMsg.Contains("%n"))
                        {
                            var indiceInicio = errorMsg.IndexOf("##");
                            errorMsg = cFuncionesStrings.ReplaceFirst(errorMsg, "##", " ");
                            var indiceFinal = errorMsg.IndexOf("##");
                            if (indiceFinal < 0)
                            {
                                indiceFinal = errorMsg.Length;
                            }
                            var strVariable = errorMsg.Substring(indiceInicio, indiceFinal - indiceInicio);
                            descripcionMsg = cFuncionesStrings.ReplaceFirst(descripcionMsg, "%n", strVariable);
                        }

                        lblMensaje.Text = obj.Code + ": " + descripcionMsg;
                        pbIcono.Image = global::ReAl.GestionMedica.App.Properties.Resources.alerta;
                        Text = "Atención";
                        webDetalles.DocumentText = CuerpoWeb(causa + Br() + accion + Br() + comentario + Br() + origen);
                    }
                    else
                    {
                        lblMensaje.Text = "Error en Base de Datos: " + sqlExp.Message + Environment.NewLine + Environment.NewLine + sqlExp.Hint;
                        webDetalles.DocumentText = CuerpoWeb(sqlExp.StackTrace);
                        CJiraHelper.AddIssueOnJira(_myExp);
                    }
                }
                else
                {
                    if (IsValidJson(_myExp.Message))
                    {
                        pbIcono.Image = global::ReAl.GestionMedica.App.Properties.Resources.alerta;
                        var validacion = new StringBuilder();
                        var errores = JsonConvert.DeserializeObject<dynamic>(_myExp.Message);
                        foreach (dynamic error in errores)
                        {
                            validacion.AppendLine(error.ToString().Replace("\"","").Split(":")[1] + "<br>");
                        }

                        this.Text = _myTitulo;
                        lblMensaje.Text = "Algunos campos del formulario no cumplen con la validación del sistema";
                        webDetalles.DocumentText = validacion.ToString();
                        CJiraHelper.AddIssueOnJira(_myExp);
                    }
                    else
                    {
                        obj = rn.GetByCode(_myExp.Message.Substring(0, 9));

                        if (obj != null)
                        {
                            var descripcionMsg = obj.Description;
                            var errorMsg = _myExp.Message;

                            while (descripcionMsg.Contains("%n"))
                            {
                                var indiceInicio = errorMsg.IndexOf("##");
                                errorMsg = cFuncionesStrings.ReplaceFirst(errorMsg, "##", " ");
                                var indiceFinal = errorMsg.IndexOf("##");
                                if (indiceFinal < 0)
                                {
                                    indiceFinal = errorMsg.Length;
                                }
                                var strVariable = errorMsg.Substring(indiceInicio, indiceFinal - indiceInicio);
                                descripcionMsg = cFuncionesStrings.ReplaceFirst(descripcionMsg, "%n", strVariable);
                            }

                            causa = StrongWeb("Causa: ") + obj.Causes;
                            accion = StrongWeb("Acción: ") + obj.Actions;
                            comentario = StrongWeb("Comentario: ") + obj.Comments;
                            origen = StrongWeb("Origen: ") + obj.Origin;

                            lblMensaje.Text = obj.Code + ": " + descripcionMsg;
                            pbIcono.Image = global::ReAl.GestionMedica.App.Properties.Resources.alerta;
                            Text = "Atención";
                            webDetalles.DocumentText = CuerpoWeb(causa + Br() + accion + Br() + comentario + Br() + origen);
                        }
                        else
                        {
                            this.Text = _myTitulo;
                            lblMensaje.Text = _myExp.Message + Environment.NewLine + Environment.NewLine + _myExp.StackTrace;
                            webDetalles.DocumentText = CuerpoWeb(_myExp.StackTrace);
                            CJiraHelper.AddIssueOnJira(_myExp);
                        }
                    }
                }
            }
            catch (Exception)
            {
                this.Text = _myTitulo;
                lblMensaje.Text = _myExp.Message + Environment.NewLine + Environment.NewLine + _myExp.StackTrace;
                webDetalles.DocumentText = CuerpoWeb(_myExp.StackTrace);
            }
        }

        private void CrearEntornoExcepcion(Exception e)
        {
            var strBuildTime = new DateTime(2000, 1, 1).AddDays(System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Build).ToShortDateString();

            // Gets program uptime
            var timeSpanProcTime = Process.GetCurrentProcess().TotalProcessorTime;

            // Used to get disk space
            var driveInfo = new DriveInfo(Directory.GetDirectoryRoot(Application.ExecutablePath));

            _listAuxiliar.Items.Add(new ListViewItem(new[] { "Current Date/Time", DateTime.Now.ToString() }));
            _listAuxiliar.Items.Add(new ListViewItem(new[] { "Exec. Date/Time", Process.GetCurrentProcess().StartTime.ToString() }));
            _listAuxiliar.Items.Add(new ListViewItem(new[] { "Build Date", strBuildTime }));
            _listAuxiliar.Items.Add(new ListViewItem(new[] { "OS", Environment.OSVersion.VersionString }));
            _listAuxiliar.Items.Add(new ListViewItem(new[] { "Plataform", Environment.OSVersion.Platform.ToString() }));
            _listAuxiliar.Items.Add(new ListViewItem(new[] { "Language", Application.CurrentInputLanguage.LayoutName }));
            _listAuxiliar.Items.Add(new ListViewItem(new[] { "System Uptime", string.Format("{0} Days {1} Hours {2} Mins {3} Secs", Math.Round((decimal)GetTickCount() / 86400000), Math.Round((decimal)GetTickCount() / 3600000 % 24), Math.Round((decimal)GetTickCount() / 120000 % 60), Math.Round((decimal)GetTickCount() / 1000 % 60)) }));
            _listAuxiliar.Items.Add(new ListViewItem(new[] { "Program Uptime", string.Format("{0} hours {1} mins {2} secs", timeSpanProcTime.TotalHours.ToString("0"), timeSpanProcTime.TotalMinutes.ToString("0"), timeSpanProcTime.TotalSeconds.ToString("0")) }));
            _listAuxiliar.Items.Add(new ListViewItem(new[] { "PID", Process.GetCurrentProcess().Id.ToString() }));
            _listAuxiliar.Items.Add(new ListViewItem(new[] { "Thread Count", Process.GetCurrentProcess().Threads.Count.ToString() }));
            _listAuxiliar.Items.Add(new ListViewItem(new[] { "Thread Id", System.Threading.Thread.CurrentThread.ManagedThreadId.ToString() }));
            _listAuxiliar.Items.Add(new ListViewItem(new[] { "Executable", Application.ExecutablePath }));
            _listAuxiliar.Items.Add(new ListViewItem(new[] { "Process Name", Process.GetCurrentProcess().ProcessName }));
            _listAuxiliar.Items.Add(new ListViewItem(new[] { "Version", Application.ProductVersion }));
            _listAuxiliar.Items.Add(new ListViewItem(new[] { "CLR Version", Environment.Version.ToString() }));

            var ex = e;
            for (var i = 0; ex != null; ex = ex.InnerException, i++)
            {
                _listAuxiliar.Items.Add(new ListViewItem(new[] { "Type #" + i, ex.GetType().ToString() }));
                if (!string.IsNullOrEmpty(ex.Message))
                {
                    _listAuxiliar.Items.Add(new ListViewItem(new[] { "Message #" + i, ex.Message }));
                }
                if (!string.IsNullOrEmpty(ex.Source))
                {
                    _listAuxiliar.Items.Add(new ListViewItem(new[] { "Source #" + i, ex.Source }));
                }
                if (!string.IsNullOrEmpty(ex.HelpLink))
                {
                    _listAuxiliar.Items.Add(new ListViewItem(new[] { "Help Link #" + i, ex.HelpLink }));
                }
                if (ex.TargetSite != null)
                {
                    //listException.Items.Add(new ListViewItem(new[] { "Target Site #" + i, ex.TargetSite.ToString() }));
                    _listAuxiliar.Items.Add(new ListViewItem(new[] { "Target Site #" + i, ex.TargetSite.ToString() }));
                }
                if (ex.Data != null)
                {
                    foreach (DictionaryEntry de in ex.Data)
                    {
                        _listAuxiliar.Items.Add(new ListViewItem(new[] { de.Key.ToString(), de.Value.ToString() }));
                    }
                }
            }

            _strTrazaError = e.StackTrace;

            _strErrorLog = string.Format("{0}\\{1:yyyy}_{1:MM}_{1:dd}_{1:HH}{1:mm}{1:ss}.log", _errorDirectory, DateTime.Now);
            CreateErrorLog();
        }

        /// <summary>
        /// Creates error log file
        /// </summary>
        /// <returns>True on success</returns>
        private void CreateErrorLog()
        {
            StreamWriter streamErrorLog;

            // Create directory if it doesnt exist
            if (!Directory.Exists(_errorDirectory))
                Directory.CreateDirectory(_errorDirectory);

            try
            {
                streamErrorLog = File.CreateText(_strErrorLog);

                int i;

                streamErrorLog.WriteLine();

                for (i = 0; i < _listAuxiliar.Items.Count; i++)
                {
                    var strDesc = _listAuxiliar.Items[i].SubItems[0].Text;
                    var strValue = _listAuxiliar.Items[i].SubItems[1].Text;

                    streamErrorLog.WriteLine(string.Format("{0}: {1}", strDesc, strValue));
                }

                streamErrorLog.WriteLine("Stack Trace:");
                streamErrorLog.WriteLine(_strTrazaError);
                streamErrorLog.Close();
            }
            catch
            {
                return;
            }
            return;
        }

        private string CuerpoWeb(string texto)
        {
            return "<html><body bgcolor=\"#FFFFFF\">" + texto + "</body></html>";
        }

        private void fErrores_Load(object sender, EventArgs e)
        {
            CargarError();

            try
            {
                _myExp.Data.Add("Nombre Equipo", Environment.MachineName);
                _myExp.Data.Add("Nombre Dominio", Environment.UserDomainName);
                _myExp.Data.Add("Nombre Usuario SO", Environment.UserName);

                var strHostName = Dns.GetHostName();
                var iphostentry = Dns.GetHostByName(strHostName);
                var i = 1;
                foreach (var ipaddress in iphostentry.AddressList)
                {
                    _myExp.Data.Add("Direccion IP " + i, ipaddress.ToString());
                    i++;
                }
                CrearEntornoExcepcion(_myExp);
            }
            catch (Exception)
            {
            }
        }

        private string StrongWeb(string texto)
        {
            return "<strong>" + texto + "</strong>";
        }

        private bool IsValidJson(string strInput)
        {
            if (string.IsNullOrWhiteSpace(strInput)) { return false; }
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    //Exception in parsing json
                    Console.WriteLine(jex.Message);
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    Console.WriteLine(ex.ToString());
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        #endregion Methods
    }
}