﻿using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using DevExpress.XtraBars;
using Firebase.Auth;
using Firebase.Auth.Providers;
using Firebase.Storage;
using ReAl.GestionMedica.App.App_Class;
using ReAl.GestionMedica.App.CAL;
using ReAl.GestionMedica.App.CLA;
using ReAl.GestionMedica.App.GAS;
using ReAl.GestionMedica.App.PAC;
using ReAl.GestionMedica.App.SEG;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.Utils;

namespace ReAl.GestionMedica.App
{
    public partial class FPrincipal : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        #region Constructors

        public FPrincipal()
        {
            InitializeComponent();

            // Create a new object, representing the German culture.
            CultureInfo culture = CultureInfo.CreateSpecificCulture("es-BO");

            // The following line provides localization for the application's user interface.
            Thread.CurrentThread.CurrentUICulture = culture;

            // The following line provides localization for data formats.
            Thread.CurrentThread.CurrentCulture = culture;

            // Set this culture as the default culture for all threads in this application.
            // Note: The following properties are supported in the .NET Framework 4.5+
            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;
        }

        #endregion Constructors

        #region Methods

        public void CargarDatosCita()
        {
            try
            {
                if (cParametrosApp.AppCita.IdPatients > 0)
                {
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void CargarDatosPaciente()
        {
            try
            {
                if (cParametrosApp.AppPaciente.IdPatients > 0)
                {
                    var rn = new PatPatientsController();
                    var obj = rn.GetById(cParametrosApp.AppPaciente.IdPatients);

                    if (obj != null)
                    {
                        bbiNombresPaciente.Caption = obj.FirstName;
                        bbiApellidosPaciente.Caption = obj.LastName;
                        bbiHistoriaClinica.Enabled = true;
                        bbiConsultas.Enabled = true;
                        bbiRecetas.Enabled = true;
                        bbiCirugias.Enabled = true;
                        bbiCertificados.Enabled = true;
                        bbiInformes.Enabled = true;
                        bbiMultimedia.Enabled = true;
                        bbiTerminar.Enabled = true;
                    }
                    else
                    {
                        bbiNombresPaciente.Caption = "Paciente No encontrado";
                        bbiApellidosPaciente.Caption = "";
                        bbiHistoriaClinica.Enabled = false;
                        bbiConsultas.Enabled = false;
                        bbiRecetas.Enabled = false;
                        bbiCirugias.Enabled = false;
                        bbiCertificados.Enabled = false;
                        bbiInformes.Enabled = false;
                        bbiMultimedia.Enabled = false;
                        bbiTerminar.Enabled = false;
                    }
                }
                else
                {
                    bbiNombresPaciente.Caption = "";
                    bbiApellidosPaciente.Caption = "";
                    bbiHistoriaClinica.Enabled = false;
                    bbiConsultas.Enabled = false;
                    bbiRecetas.Enabled = false;
                    bbiCirugias.Enabled = false;
                    bbiCertificados.Enabled = false;
                    bbiInformes.Enabled = false;
                    bbiMultimedia.Enabled = false;
                    bbiTerminar.Enabled = false;

                    //cerramos los forms
                    foreach (var mdiChild in this.MdiChildren)
                    {
                        if (mdiChild.GetType() == typeof(FPacHistoriaClinica)) mdiChild.Close();

                        if (mdiChild.GetType() == typeof(LPacConsultas)) mdiChild.Close();

                        if (mdiChild.GetType() == typeof(LPacRecetas)) mdiChild.Close();

                        if (mdiChild.GetType() == typeof(LPacCirugias)) mdiChild.Close();

                        if (mdiChild.GetType() == typeof(LPacInformes)) mdiChild.Close();

                        if (mdiChild.GetType() == typeof(LPacMultimedia)) mdiChild.Close();

                        if (mdiChild.GetType() == typeof(LPacCertificados)) mdiChild.Close();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void HabilitarAcciones()
        {
            try
            {
                switch (cParametrosApp.AppRestUser.SegUsersRestriction[0].SegRol.Code.ToUpper())
                //switch (cParametrosApp.AppRol.siglasro.ToUpper())
                {
                    case "ADMINISTRADOR":
                        rpgAgenda.Visible = true;
                        rpgPaciente.Visible = true;
                        bbiListadoPacientes.Enabled = true;

                        bbiHistoriaClinica.Visibility = BarItemVisibility.Always;
                        bbiConsultas.Visibility = BarItemVisibility.Always;
                        bbiRecetas.Visibility = BarItemVisibility.Always;
                        bbiCirugias.Visibility = BarItemVisibility.Always;
                        bbiInformes.Visibility = BarItemVisibility.Always;
                        bbiCertificados.Visibility = BarItemVisibility.Always;
                        bbiMultimedia.Visibility = BarItemVisibility.Always;
                        bbiTerminar.Visibility = BarItemVisibility.Always;
                        bbiMigracion.Visibility = BarItemVisibility.Always;

                        rpgGastos.Visible = true;
                        rpgCategoriasGastos.Visible = true;

                        rpgPlantillas.Visible = true;
                        bbiTiposPlantilla.Visibility = BarItemVisibility.Always;

                        rpgCatalogos.Visible = true;

                        rpgUsuarios.Visible = true;

                        rpReportes.Visible = true;
                        break;

                    case "DOCTOR":
                        rpgAgenda.Visible = true;
                        rpgPaciente.Visible = true;
                        bbiListadoPacientes.Enabled = true;

                        bbiHistoriaClinica.Visibility = BarItemVisibility.Always;
                        bbiConsultas.Visibility = BarItemVisibility.Always;
                        bbiRecetas.Visibility = BarItemVisibility.Always;
                        bbiCirugias.Visibility = BarItemVisibility.Always;
                        bbiInformes.Visibility = BarItemVisibility.Always;
                        bbiCertificados.Visibility = BarItemVisibility.Always;
                        bbiMultimedia.Visibility = BarItemVisibility.Always;
                        bbiTerminar.Visibility = BarItemVisibility.Always;
                        bbiMigracion.Visibility = BarItemVisibility.Never;

                        rpgGastos.Visible = true;
                        rpgCategoriasGastos.Visible = true;

                        rpgPlantillas.Visible = true;
                        bbiTiposPlantilla.Visibility = BarItemVisibility.Never;

                        rpgCatalogos.Visible = false;

                        rpgUsuarios.Visible = true;

                        rpReportes.Visible = true;
                        break;

                    case "SECRETARIA":
                        rpgAgenda.Visible = true;
                        rpgPaciente.Visible = true;
                        bbiListadoPacientes.Enabled = true;

                        bbiHistoriaClinica.Visibility = BarItemVisibility.Never;
                        bbiConsultas.Visibility = BarItemVisibility.Never;
                        bbiRecetas.Visibility = BarItemVisibility.Never;
                        bbiCirugias.Visibility = BarItemVisibility.Never;
                        bbiInformes.Visibility = BarItemVisibility.Never;
                        bbiCertificados.Visibility = BarItemVisibility.Never;
                        bbiMultimedia.Visibility = BarItemVisibility.Never;
                        bbiTerminar.Visibility = BarItemVisibility.Never;
                        bbiMigracion.Visibility = BarItemVisibility.Never;

                        rpgGastos.Visible = true;
                        rpgCategoriasGastos.Visible = true;

                        rpParametros.Visible = false;

                        rpgCatalogos.Visible = false;

                        rpgUsuarios.Visible = false;

                        rpReportes.Visible = false;

                        bbiBackups.Visibility = BarItemVisibility.Never;
                        break;

                    case "ASISTENTE":
                        rpgAgenda.Visible = false;
                        rpgPaciente.Visible = true;
                        bbiListadoPacientes.Enabled = true;

                        bbiHistoriaClinica.Visibility = BarItemVisibility.Always;
                        bbiConsultas.Visibility = BarItemVisibility.Always;
                        bbiRecetas.Visibility = BarItemVisibility.Never;
                        bbiInformes.Visibility = BarItemVisibility.Never;
                        bbiCertificados.Visibility = BarItemVisibility.Never;
                        bbiCirugias.Visibility = BarItemVisibility.Never;
                        bbiCertificados.Visibility = BarItemVisibility.Never;
                        bbiMultimedia.Visibility = BarItemVisibility.Never;
                        bbiTerminar.Visibility = BarItemVisibility.Never;
                        bbiMigracion.Visibility = BarItemVisibility.Never;

                        rpgGastos.Visible = false;
                        rpgCategoriasGastos.Visible = false;

                        rpParametros.Visible = false;

                        rpgCatalogos.Visible = false;

                        rpgUsuarios.Visible = false;

                        rpReportes.Visible = false;

                        bbiBackups.Visibility = BarItemVisibility.Never;
                        break;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiAcercaDe_ItemClick(object sender, ItemClickEventArgs e)
        {
            var frm = new FAbout();
            frm.ShowDialog();
        }

        private void bbiAgenda_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LCalAgenda))
                    {
                        bProcede = false;
                        mdiChild.Activate();

                        //Ahora activamos el ribbon
                        //ribbon.SelectedPage = ribbon.MergedPages["Mi Agenda"];
                    }
                }
                if (bProcede)
                {
                    var frm = new LCalAgenda();
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiApellidosPaciente_ItemDoubleClick(object sender, ItemClickEventArgs e)
        {
            var frm = new FPacPacientes(cParametrosApp.AppPaciente);
            var resultado = frm.ShowDialog();
        }

        private void bbiBackups_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                /*
                var bProcede = false;

                while (!bProcede)
                {
                    bProcede = cFuncionesPostgres.CrearBackupBd(RnVista.strServidor, RnVista.strUsuario, RnVista.strPass, RnVista.strNameBd);
                }

                if (bProcede)
                {
                    if (MessageBox.Show("Se ha creado el Backup de su Base de Datos" + Environment.NewLine + "¿Desea abrir la capeta de Backups?", "Backup", MessageBoxButtons.YesNo,
                                    MessageBoxIcon.Information) == DialogResult.Yes)
                    {
                        var strPath = Application.StartupPath + "\\backups\\";
                        Process.Start("explorer.exe", strPath);
                    }
                }
                */
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiCambiarPassword_ItemClick(object sender, ItemClickEventArgs e)
        {
            var frm = new FSegCambiarPass();
            frm.ShowDialog();
        }

        private void bbiCategorias_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LGasCategoria))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new LGasCategoria();
                    frm.MdiParent = this;
                    frm.Show();
                    ((LGasCategoria)frm).CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiCertificados_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LPacCertificados))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new LPacCertificados();
                    frm.Text = "Certificados - " + bbiNombresPaciente.Caption;
                    frm.MdiParent = this;
                    frm.Show();
                    ((LPacCertificados)frm).CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiCirugias_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LPacCirugias))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new LPacCirugias();
                    frm.Text = "Cirugías - " + bbiNombresPaciente.Caption;
                    frm.MdiParent = this;
                    frm.Show();
                    ((LPacCirugias)frm).CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiCobros_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LGasCobros))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new LGasCobros();
                    frm.MdiParent = this;
                    frm.Show();
                    ((LGasCobros)frm).CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiConsultas_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LPacConsultas))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new LPacConsultas();
                    frm.Text = "Consultas - " + bbiNombresPaciente.Caption;
                    frm.MdiParent = this;
                    frm.Show();
                    ((LPacConsultas)frm).CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiEstadoCivil_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LClaEstadocivil))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new LClaEstadocivil();
                    frm.MdiParent = this;
                    frm.Show();
                    ((LClaEstadocivil)frm).CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiGasto_ItemClick(object sender, ItemClickEventArgs e)
        {
            var frm = new FGasLibro("G", false);
            frm.ShowDialog();
        }

        private void bbiHistoriaClinica_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(FPacHistoriaClinica))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new FPacHistoriaClinica();
                    frm.Text = "Historia Clinica - " + bbiNombresPaciente.Caption;
                    frm.MdiParent = this;
                    frm.Show();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiInformes_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LPacInformes))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new LPacInformes();
                    frm.Text = "Informes - " + bbiNombresPaciente.Caption;
                    frm.MdiParent = this;
                    frm.Show();
                    ((LPacInformes)frm).CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiIngreso_ItemClick(object sender, ItemClickEventArgs e)
        {
            var frm = new FGasLibro("I", false);
            frm.ShowDialog();
        }

        private void bbiLibro_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LGasLibro))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new LGasLibro();
                    frm.MdiParent = this;
                    frm.Show();
                    ((LGasLibro)frm).CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiListadoPacientes_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LPacPacientes))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new LPacPacientes();
                    frm.MdiParent = this;
                    frm.Show();
                    ((LPacPacientes)frm).CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiMigracion_ItemClick(object sender, ItemClickEventArgs e)
        {
            
        }

        private void bbiMultimedia_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LPacMultimedia))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new LPacMultimedia();
                    frm.Text = "Imágenes y Videos - " + bbiNombresPaciente.Caption;
                    frm.MdiParent = this;
                    frm.Show();
                    ((LPacMultimedia)frm).CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiNombresPaciente_ItemDoubleClick(object sender, ItemClickEventArgs e)
        {
            var frm = new FPacPacientes(cParametrosApp.AppPaciente);
            var resultado = frm.ShowDialog();
        }

        private void bbiPlantillas_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LClaPlantillas))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new LClaPlantillas();
                    frm.MdiParent = this;
                    frm.Show();
                    ((LClaPlantillas)frm).CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiRecetas_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LPacRecetas))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new LPacRecetas();
                    frm.Text = "Recetas - " + bbiNombresPaciente.Caption;
                    frm.MdiParent = this;
                    frm.Show();
                    ((LPacRecetas)frm).CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiRepCategoria_ItemClick(object sender, ItemClickEventArgs e)
        {
            var frm = new FGasRptCategoria();
            frm.ShowDialog();
        }

        private void bbiRepTendencias_ItemClick(object sender, ItemClickEventArgs e)
        {
            var frm = new FGasRptTendencia();
            frm.ShowDialog();
        }

        private void bbiRptAusencias_ItemClick(object sender, ItemClickEventArgs e)
        {
            var frm = new FPacRptAusencias();
            frm.ShowDialog();
        }

        private void bbiRptGastos_ItemClick(object sender, ItemClickEventArgs e)
        {
            var frm = new FGasRptMayor();
            frm.ShowDialog();
        }

        private void bbiRptPacientesFrecuentes_ItemClick(object sender, ItemClickEventArgs e)
        {
            var frm = new FPacRptFrecuencia();
            frm.ShowDialog();
        }

        private void bbiRptTendenciaPacientes_ItemClick(object sender, ItemClickEventArgs e)
        {
            var frm = new FCalRptTendenciaAtencion();
            frm.ShowDialog();
        }

        private void bbiSalir_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Application.Exit();
        }

        private void bbiTerminar_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                //Verificamos si tiene Cita y Costo
                if (cParametrosApp.AppCita.IdAppointments > 0)
                {
                    var frmCosto = new FCalAgendaCosto(cParametrosApp.AppCita);
                    frmCosto.ShowDialog();

                    if (frmCosto.DialogResult == DialogResult.OK)
                    {
                        cParametrosApp.AppCita = new PatAppointments();
                        cParametrosApp.AppCita.IdAppointments = 0;

                        //Liberamos al Paciente
                        cParametrosApp.AppPaciente = new PatPatients();
                        cParametrosApp.AppPaciente.IdPatients = 0;
                    }
                }
                else
                {
                    //Liberamos al Paciente
                    cParametrosApp.AppPaciente = new PatPatients();
                    cParametrosApp.AppPaciente.IdPatients = 0;
                }
                CargarDatosPaciente();
                CargarDatosCita();
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiTipoSangre_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LClaTiposangre))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new LClaTiposangre();
                    frm.Text = "Tipos de Sangre";
                    frm.MdiParent = this;
                    frm.Show();
                    ((LClaTiposangre)frm).CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiTiposPlantilla_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LClaTipoplantillas))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new LClaTipoplantillas();
                    frm.MdiParent = this;
                    frm.Show();
                    ((LClaTipoplantillas)frm).CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void bbiUsuarios_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var bProcede = true;
                foreach (var mdiChild in this.MdiChildren)
                {
                    if (mdiChild.GetType() == typeof(LSegUsuarios))
                    {
                        bProcede = false;
                        mdiChild.Activate();
                    }
                }
                if (bProcede)
                {
                    var frm = new LSegUsuarios();
                    frm.MdiParent = this;
                    frm.Show();
                    ((LSegUsuarios)frm).CargarListado();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void fPrincipal_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                if (cParametrosApp.AppPaciente.IdPatients > 0)
                {
                    if (e.Data.GetDataPresent(DataFormats.FileDrop))
                    {
                        var filePaths = (string[])(e.Data.GetData(DataFormats.FileDrop));
                        foreach (var fileLoc in filePaths)
                        {
                            using (var frm = new FPacMultimedia(fileLoc))
                            {
                                frm.ShowDialog();
                            }
                        }
                    }

                    //Mostramos o activamos el Formulario
                    var bProcede = true;
                    foreach (var mdiChild in this.MdiChildren)
                    {
                        if (mdiChild.GetType() == typeof(LPacMultimedia))
                        {
                            bProcede = false;
                            mdiChild.Activate();
                        }
                    }
                    if (bProcede)
                    {
                        var frm = new LPacMultimedia();
                        frm.Text = "Imágenes y Videos - " + bbiNombresPaciente.Caption;
                        frm.MdiParent = this;
                        frm.Show();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void fPrincipal_DragEnter(object sender, DragEventArgs e)
        {
            if (cParametrosApp.AppPaciente.IdPatients > 0)
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    e.Effect = DragDropEffects.Copy;
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                }
            }
        }

        private void fPrincipal_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            //Las Leyendas del Pie de Pagina
            if (DateTime.Now.Year == 2013)
                bsiCopyrights.Caption = "© 2013 ReAl Systems - Todos los Derechos reservados";
            else
                bsiCopyrights.Caption = "© 2013 - " + DateTime.Now.Year + " ReAl Systems - Todos los Derechos reservados";

            //Habilitamos el Drag and Drop
            this.AllowDrop = true;
            this.DragEnter += fPrincipal_DragEnter;
            this.DragDrop += fPrincipal_DragDrop;

            HabilitarAcciones();

            //abrimos mi Agenda
            var frm = new LCalAgenda();
            frm.MdiParent = this;
            frm.Show();
        }

        private void tmrRefresh_Tick(object sender, EventArgs e)
        {
            //this.CargarDatosPaciente();
            //this.CargarDatosCita();

            //El Dia y la Fecha
            bsiFechaHora.Caption = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
            bbiListadoPacientes.Enabled = cParametrosApp.AppPatientsList.Count > 0;
        }

        #endregion Methods
    }
}