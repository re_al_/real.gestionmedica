﻿using DevExpress.Pdf.Native.BouncyCastle.Security.Certificates;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ReAl.GestionMedica.BackendConnector.Controllers;
using Serilog;
using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Security.Permissions;
using System.Threading;
using System.Windows.Forms;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.App.App_Class;
using System.Drawing.Imaging;
using ReAl.Utils;

namespace ReAl.GestionMedica.App
{
    internal static class Program
    {
        /// <summary>
        /// Dependencia de inyección e inicio de LOGs
        /// </summary>
        /// <returns>HostBuilder</returns>
        private static IHostBuilder Init()
        {

            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //SetAppSkin();

            //Generate Host Builder and Register the Services for DI
            var builder = new HostBuilder()
               .ConfigureServices((_, services) =>
               {
                   services.AddScoped<FPrincipal>();
                   services.AddScoped<FLogin>();

                   //Add Serilog
                   var serilogLogger = new LoggerConfiguration()
                   .MinimumLevel.Information()
                   .WriteTo.File(@"C:/ND-Logs/log-.log", rollingInterval: RollingInterval.Day)
                   .CreateLogger();

                   services.AddLogging(x =>
                   {
                       x.AddSerilog(
                           logger: serilogLogger,
                           dispose: true
                       );
                   });
               });

            return builder;
        }

        /// <summary>
        /// Mostrar el formulario de Login y obtener un Nombre de Usuario v�lido
        /// </summary>
        private static void Login(IServiceProvider services)
        {
            using var loginForm = services.GetRequiredService<FLogin>();
            DialogResult results;
            do
            {
                results = loginForm.ShowDialog();
                if (results == DialogResult.Cancel)
                    Environment.Exit(1);
            } while (results != DialogResult.OK);
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main()
        {
            //Validate multiple instances
            if (Process.GetProcessesByName(Process.GetCurrentProcess().ProcessName).Length > 1)
            {
                MessageBox.Show(@"Solo se permite una instancia de la aplicación", @"Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            var builder = Init();
            Start(builder);
        }

        
        /// <summary>
        /// Inicio de la aplicaci�n
        /// </summary>
        /// <param name="builder">Instancia de HostBuilder</param>
        private static void Start(IHostBuilder builder)
        {
            var host = builder.Build();

            using var serviceScope = host.Services.CreateScope();
            var services = serviceScope.ServiceProvider;
            try
            {
                // Create a new object, representing the German culture.
                CultureInfo culture = CultureInfo.CreateSpecificCulture("es-AR");

                // The following line provides localization for the application's user interface.
                Thread.CurrentThread.CurrentUICulture = culture;

                // The following line provides localization for data formats.
                Thread.CurrentThread.CurrentCulture = culture;

                // Set this culture as the default culture for all threads in this application.
                // Note: The following properties are supported in the .NET Framework 4.5+
                CultureInfo.DefaultThreadCurrentCulture = culture;
                CultureInfo.DefaultThreadCurrentUICulture = culture;

                Login(services);

                //Init Task to GetPatients
                Task<bool> taskPatients = Task.Run(() =>
                {
                    var service = new PatPatientsController();
                    cParametrosApp.AppPatientsList = service.GetActive();
                    return true;
                });

                var frmMain = services.GetRequiredService<FPrincipal>();
                Application.Run(frmMain);
                Console.WriteLine(@"Success");
            }

            catch (Exception ex)
            {
                Console.WriteLine(@"Error occurred: " + ex.Message);
            }
        }
    }
}