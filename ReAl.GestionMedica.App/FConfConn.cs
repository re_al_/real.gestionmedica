﻿using System.Data;
using System.IO;
using System.Text;
using Npgsql;
using ReAl.Utils;

namespace ReAl.GestionMedica.App
{
    public partial class FConfConn : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public FConfConn()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            Application.Exit();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            var bProcede = false;
            var myServer = txtServidor.Text;
            var myPuerto = txtPuerto.Text;
            var myUserDefault = txtUsuario.Text;
            var myPassDefault = txtPassword.Text;
            var myBd = txtBaseDatos.Text;
            try
            {
                var myConexionBd = new NpgsqlConnection();
                myConexionBd.ConnectionString = "Server=" + myServer +
                                                    ";Database=" + myBd +
                                                    ";User ID=" + myUserDefault +
                                                    ";Port=" + myPuerto +
                                                    ";Password=" + myPassDefault +
                                                    ";Pooling=false";

                myConexionBd.Open();

                if (myConexionBd.State == ConnectionState.Open)
                    myConexionBd.Close();

                bProcede = true;
            }
            catch (Exception exp)
            {
                bProcede = false;
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }

            if (bProcede)
            {
                try
                {
                    bProcede = false;

                    //Guardamos la Conexion
                    var dt = new DataTable("Conexion");
                    var dc1 = new DataColumn("Campo", typeof(String));
                    var dc2 = new DataColumn("Valor", typeof(String));

                    dt.Columns.Add(dc1);
                    dt.Columns.Add(dc2);

                    //añadimos los valores
                    var dr = dt.NewRow();
                    dr[0] = "Server";
                    dr[1] = myServer;
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr[0] = "Bd";
                    dr[1] = myBd;
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr[0] = "UserDefault";
                    dr[1] = myUserDefault;
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr[0] = "Puerto";
                    dr[1] = myPuerto;
                    dt.Rows.Add(dr);

                    dr = dt.NewRow();
                    dr[0] = "PassDefault";
                    dr[1] = myPassDefault;
                    dt.Rows.Add(dr);

                    var strFileName = "GestionMedica.App.ConfConn";
                    dt.WriteXml(strFileName);

                    var strLlave = "ReAlGestionMedica";
                    var myEncoding = new UTF8Encoding();
                    var myLlave = myEncoding.GetBytes(strLlave);

                    cFuncionesEncriptacion.CifrarXor(strFileName, myLlave);

                    if (File.Exists(strFileName))
                        File.Delete(strFileName);

                    bProcede = true;
                }
                catch (Exception exp)
                {
                    bProcede = false;
                    var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                    frm.ShowDialog();
                }

                if (bProcede)
                {
                    MessageBox.Show(
                        "Se ha cambiado la Conexión al Servidor",
                        "Exito!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
        }

        #endregion Methods
    }
}