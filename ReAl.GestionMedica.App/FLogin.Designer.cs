﻿


namespace ReAl.GestionMedica.App
{
    partial class FLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FLogin));
            txtUsuario = new TextBox();
            btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            label1 = new Label();
            label2 = new Label();
            txtPass = new DevExpress.XtraEditors.TextEdit();
            lblVersion = new LinkLabel();
            label3 = new Label();
            ((System.ComponentModel.ISupportInitialize)txtPass.Properties).BeginInit();
            SuspendLayout();
            // 
            // txtUsuario
            // 
            txtUsuario.BackColor = Color.White;
            txtUsuario.BorderStyle = BorderStyle.FixedSingle;
            txtUsuario.Font = new Font("Tahoma", 10F, FontStyle.Regular, GraphicsUnit.Point);
            txtUsuario.Location = new Point(341, 84);
            txtUsuario.Name = "txtUsuario";
            txtUsuario.Size = new Size(163, 24);
            txtUsuario.TabIndex = 2;
            txtUsuario.KeyDown += txtUsuario_KeyDown;
            // 
            // btnAceptar
            // 
            btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Primary;
            btnAceptar.Appearance.Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Regular, GraphicsUnit.Point);
            btnAceptar.Appearance.Options.UseBackColor = true;
            btnAceptar.Appearance.Options.UseFont = true;
            btnAceptar.Location = new Point(262, 170);
            btnAceptar.LookAndFeel.UseWindowsXPTheme = true;
            btnAceptar.Name = "btnAceptar";
            btnAceptar.Size = new Size(242, 36);
            btnAceptar.TabIndex = 5;
            btnAceptar.Text = "Aceptar";
            btnAceptar.Click += btnAceptar_Click;
            // 
            // btnCancelar
            // 
            btnCancelar.DialogResult = DialogResult.Cancel;
            btnCancelar.Location = new Point(185, 401);
            btnCancelar.Name = "btnCancelar";
            btnCancelar.Size = new Size(75, 23);
            btnCancelar.TabIndex = 7;
            btnCancelar.TabStop = false;
            btnCancelar.Text = "Cancelar";
            btnCancelar.Click += btnCancelar_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.BackColor = Color.White;
            label1.Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point);
            label1.Location = new Point(259, 86);
            label1.Name = "label1";
            label1.Size = new Size(61, 17);
            label1.TabIndex = 1;
            label1.Text = "Usuario:";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.BackColor = Color.White;
            label2.Font = new Font("Microsoft Sans Serif", 10F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(259, 128);
            label2.Name = "label2";
            label2.Size = new Size(85, 17);
            label2.TabIndex = 3;
            label2.Text = "Contraseña:";
            // 
            // txtPass
            // 
            txtPass.Location = new Point(341, 125);
            txtPass.Name = "txtPass";
            txtPass.Properties.Appearance.Font = new Font("Tahoma", 10F, FontStyle.Regular, GraphicsUnit.Point);
            txtPass.Properties.Appearance.Options.UseFont = true;
            txtPass.Properties.AppearanceFocused.BackColor = SystemColors.GradientInactiveCaption;
            txtPass.Properties.AppearanceFocused.Options.UseBackColor = true;
            txtPass.Properties.PasswordChar = '*';
            txtPass.Size = new Size(163, 22);
            txtPass.TabIndex = 4;
            txtPass.KeyDown += txtPass_KeyDown;
            // 
            // lblVersion
            // 
            lblVersion.AutoSize = true;
            lblVersion.BackColor = Color.White;
            lblVersion.Location = new Point(428, 209);
            lblVersion.Name = "lblVersion";
            lblVersion.Size = new Size(76, 13);
            lblVersion.TabIndex = 6;
            lblVersion.TabStop = true;
            lblVersion.Text = "Build 24.02.11";
            // 
            // label3
            // 
            label3.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            label3.BackColor = Color.White;
            label3.Font = new Font("Microsoft Sans Serif", 14F, FontStyle.Bold, GraphicsUnit.Point);
            label3.ForeColor = SystemColors.Highlight;
            label3.Location = new Point(262, 27);
            label3.Name = "label3";
            label3.Size = new Size(242, 27);
            label3.TabIndex = 0;
            label3.Text = "NEURODIAGNOSTICO";
            label3.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // FLogin
            // 
            AcceptButton = btnAceptar;
            AutoScaleDimensions = new SizeF(6F, 13F);
            AutoScaleMode = AutoScaleMode.Font;
            BackgroundImageLayoutStore = ImageLayout.Stretch;
            BackgroundImageStore = Properties.Resources.Banner2;
            CancelButton = btnCancelar;
            ClientSize = new Size(516, 229);
            Controls.Add(lblVersion);
            Controls.Add(txtPass);
            Controls.Add(label2);
            Controls.Add(label3);
            Controls.Add(label1);
            Controls.Add(btnCancelar);
            Controls.Add(btnAceptar);
            Controls.Add(txtUsuario);
            DoubleBuffered = true;
            FormBorderStyle = FormBorderStyle.None;
            IconOptions.Icon = (Icon)resources.GetObject("FLogin.IconOptions.Icon");
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FLogin";
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Ingreso al Sistema";
            FormClosing += fLogin_FormClosing;
            Load += fLogin_Load;
            KeyDown += fLogin_KeyDown;
            ((System.ComponentModel.ISupportInitialize)txtPass.Properties).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        internal System.Windows.Forms.TextBox txtUsuario;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit txtPass;
        private System.Windows.Forms.LinkLabel lblVersion;
        private System.Windows.Forms.Label label3;
    }
}