﻿using Firebase.Auth.Providers;
using Firebase.Auth;
using Firebase.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReAl.GestionMedica.App.App_Class
{
    internal class CFirebaseHelper
    {
        internal static UserCredential FirebaseLogin()
        {
            string ApiKey = "AIzaSyDPTkw-jCh73yY1UhzDb7vM_WSq6qm5BPs";
            string AuthEmail = "neuro.diagnostico.bo@gmail.com";
            //string AuthPassword = "ReAl5989513@";
            string AuthPassword = "rvzjwbkssyqqkarm";
            string AuthDomain = "neurodiagnostico-fa1d5.firebaseapp.com";
            string StorageUrl = "neurodiagnostico-fa1d5.appspot.com";
            string projectId = "neurodiagnostico-fa1d5";
            string messagingSenderId = "541411835176";
            string appId = "1:541411835176:web:d67d23ba528243cf7b241d";
            string measurementId = "G-562F7VJLQ8";

            var config = new FirebaseAuthConfig
            {
                ApiKey = ApiKey,
                AuthDomain = "neurodiagnostico-fa1d5.firebaseapp.com",
                Providers = new FirebaseAuthProvider[] {
                    new EmailProvider()
                },
            };
            var client = new FirebaseAuthClient(config);

            var emailUser = client.SignInWithEmailAndPasswordAsync(AuthEmail, AuthPassword).Result;

            return emailUser;
        }

        internal static async Task<string> FirebaseUpload(UserCredential emailUser, Stream archivo, string nombre, string folder = "")
        {
            string StorageUrl = "neurodiagnostico-fa1d5.appspot.com";
            // Constructr FirebaseStorage, path to where you want to upload the file and Put it there

            FirebaseStorageTask task;
            Task taskDelete;
            if (string.IsNullOrEmpty(folder))
            {
                try
                {
                    taskDelete = new FirebaseStorage(StorageUrl,
                            new FirebaseStorageOptions
                            {
                                AuthTokenAsyncFactory = () => Task.FromResult(emailUser.User.Credential.IdToken),
                                ThrowOnCancel = true
                            })
                        .Child(nombre)
                        .DeleteAsync();

                    taskDelete.Wait();
                }
                catch (Exception e)
                {
                    
                }

                task = new FirebaseStorage(StorageUrl,
                        new FirebaseStorageOptions
                        {
                            AuthTokenAsyncFactory = () => Task.FromResult(emailUser.User.Credential.IdToken),
                            ThrowOnCancel = true
                        })
                    .Child(nombre)
                    .PutAsync(archivo);
            }
            else
            {
                try
                {
                    taskDelete = new FirebaseStorage(StorageUrl,
                            new FirebaseStorageOptions
                            {
                                AuthTokenAsyncFactory = () => Task.FromResult(emailUser.User.Credential.IdToken),
                                ThrowOnCancel = true
                            })
                        .Child(folder)
                        .Child(nombre)
                        .DeleteAsync();

                    taskDelete.Wait();
                }
                catch (Exception e)
                {
                    
                }

                task = new FirebaseStorage(StorageUrl,
                        new FirebaseStorageOptions
                        {
                            AuthTokenAsyncFactory = () => Task.FromResult(emailUser.User.Credential.IdToken),
                            ThrowOnCancel = true,
                        })
                    .Child(folder)
                    .Child(nombre)
                    .PutAsync(archivo);
            }
            

            // await the task to wait until upload completes and get the download url
            // Track progress of the upload
            task.Progress.ProgressChanged += (s, e) => Console.WriteLine($"Progress: {e.Percentage} %");

            // await the task to wait until upload completes and get the download url
            var downloadUrl = await task;
            return downloadUrl + "&alt=media";
        }
    }
}
