namespace ReAl.GestionMedica.Class
{
    public static class CApi
    {
        public enum Estado
        {
            ACTIVE,
            DELETED
        }

        public enum Transaccion
        {
            DELETE,
            UPDATE,
            CREATE
        }
    }
}