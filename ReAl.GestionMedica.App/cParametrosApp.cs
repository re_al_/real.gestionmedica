﻿using ReAl.GestionMedica.BackendConnector.Entities;

namespace ReAl.GestionMedica.App
{
    public static class cParametrosApp
    {
        #region Fields

        public static PatAppointments AppCita = new PatAppointments();
        public static PatPatients AppPaciente = new PatPatients();
        public static SegUsers AppRestUser = new SegUsers();
        public static string StrTempFolder = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "\\";
        public static string CompanyName = "NeuroDiagnóstico";

        public static List<PatPatients> AppPatientsList = new List<PatPatients>();

        public static string ParFormatoFecha = "dd/MM/yyyy";

        //Otros parametros
        public static string ParFormatoFechaHora = "dd/MM/yyyy HH:mm:ss";

        #endregion Fields
    }

    public class MyRtfObject
    {
        #region Properties

        public string RtfSplitContent { get; set; }

        #endregion Properties
    }
}