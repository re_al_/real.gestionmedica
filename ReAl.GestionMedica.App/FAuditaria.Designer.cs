﻿


namespace ReAl.GestionMedica.App
{
    partial class FAuditaria
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupBox3 = new System.Windows.Forms.GroupBox();
            this.txtApiEstado = new System.Windows.Forms.TextBox();
            this.Label5 = new System.Windows.Forms.Label();
            this.GroupBox2 = new System.Windows.Forms.GroupBox();
            this.txtMRol = new System.Windows.Forms.TextBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.txtMApellido = new System.Windows.Forms.TextBox();
            this.Label10 = new System.Windows.Forms.Label();
            this.txtMNombre = new System.Windows.Forms.TextBox();
            this.Label11 = new System.Windows.Forms.Label();
            this.txtFecMod = new System.Windows.Forms.TextBox();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.txtUsuMod = new System.Windows.Forms.TextBox();
            this.GroupBox1 = new System.Windows.Forms.GroupBox();
            this.txtCRol = new System.Windows.Forms.TextBox();
            this.Label8 = new System.Windows.Forms.Label();
            this.txtCApellido = new System.Windows.Forms.TextBox();
            this.Label7 = new System.Windows.Forms.Label();
            this.txtCNombre = new System.Windows.Forms.TextBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.txtFecCre = new System.Windows.Forms.TextBox();
            this.Label1 = new System.Windows.Forms.Label();
            this.Label2 = new System.Windows.Forms.Label();
            this.txtUsuCre = new System.Windows.Forms.TextBox();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.GroupBox3.SuspendLayout();
            this.GroupBox2.SuspendLayout();
            this.GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GroupBox3
            // 
            this.GroupBox3.Controls.Add(this.txtApiEstado);
            this.GroupBox3.Controls.Add(this.Label5);
            this.GroupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.GroupBox3.Location = new System.Drawing.Point(14, 222);
            this.GroupBox3.Name = "GroupBox3";
            this.GroupBox3.Size = new System.Drawing.Size(452, 52);
            this.GroupBox3.TabIndex = 2;
            this.GroupBox3.TabStop = false;
            this.GroupBox3.Text = "Estado";
            // 
            // txtApiEstado
            // 
            this.txtApiEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtApiEstado.Location = new System.Drawing.Point(141, 19);
            this.txtApiEstado.Name = "txtApiEstado";
            this.txtApiEstado.ReadOnly = true;
            this.txtApiEstado.Size = new System.Drawing.Size(132, 20);
            this.txtApiEstado.TabIndex = 1;
            // 
            // Label5
            // 
            this.Label5.AutoSize = true;
            this.Label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label5.Location = new System.Drawing.Point(9, 22);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(126, 13);
            this.Label5.TabIndex = 0;
            this.Label5.Text = "Estado actual del registro";
            // 
            // GroupBox2
            // 
            this.GroupBox2.Controls.Add(this.txtMRol);
            this.GroupBox2.Controls.Add(this.Label3);
            this.GroupBox2.Controls.Add(this.txtMApellido);
            this.GroupBox2.Controls.Add(this.Label10);
            this.GroupBox2.Controls.Add(this.txtMNombre);
            this.GroupBox2.Controls.Add(this.Label11);
            this.GroupBox2.Controls.Add(this.txtFecMod);
            this.GroupBox2.Controls.Add(this.Label12);
            this.GroupBox2.Controls.Add(this.Label13);
            this.GroupBox2.Controls.Add(this.txtUsuMod);
            this.GroupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox2.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.GroupBox2.Location = new System.Drawing.Point(14, 116);
            this.GroupBox2.Name = "GroupBox2";
            this.GroupBox2.Size = new System.Drawing.Size(452, 100);
            this.GroupBox2.TabIndex = 1;
            this.GroupBox2.TabStop = false;
            this.GroupBox2.Text = "Modificación";
            // 
            // txtMRol
            // 
            this.txtMRol.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMRol.Location = new System.Drawing.Point(68, 71);
            this.txtMRol.Name = "txtMRol";
            this.txtMRol.ReadOnly = true;
            this.txtMRol.Size = new System.Drawing.Size(151, 20);
            this.txtMRol.TabIndex = 9;
            this.txtMRol.Visible = false;
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label3.Location = new System.Drawing.Point(11, 74);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(23, 13);
            this.Label3.TabIndex = 8;
            this.Label3.Text = "Rol";
            this.Label3.Visible = false;
            // 
            // txtMApellido
            // 
            this.txtMApellido.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMApellido.Location = new System.Drawing.Point(294, 45);
            this.txtMApellido.Name = "txtMApellido";
            this.txtMApellido.ReadOnly = true;
            this.txtMApellido.Size = new System.Drawing.Size(148, 20);
            this.txtMApellido.TabIndex = 7;
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label10.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label10.Location = new System.Drawing.Point(237, 48);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(44, 13);
            this.Label10.TabIndex = 6;
            this.Label10.Text = "Apellido";
            // 
            // txtMNombre
            // 
            this.txtMNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMNombre.Location = new System.Drawing.Point(68, 45);
            this.txtMNombre.Name = "txtMNombre";
            this.txtMNombre.ReadOnly = true;
            this.txtMNombre.Size = new System.Drawing.Size(151, 20);
            this.txtMNombre.TabIndex = 5;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label11.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label11.Location = new System.Drawing.Point(11, 48);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(44, 13);
            this.Label11.TabIndex = 4;
            this.Label11.Text = "Nombre";
            // 
            // txtFecMod
            // 
            this.txtFecMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFecMod.Location = new System.Drawing.Point(294, 19);
            this.txtFecMod.Name = "txtFecMod";
            this.txtFecMod.ReadOnly = true;
            this.txtFecMod.Size = new System.Drawing.Size(148, 20);
            this.txtFecMod.TabIndex = 3;
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label12.Location = new System.Drawing.Point(11, 22);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(43, 13);
            this.Label12.TabIndex = 0;
            this.Label12.Text = "Usuario";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label13.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label13.Location = new System.Drawing.Point(237, 22);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(37, 13);
            this.Label13.TabIndex = 2;
            this.Label13.Text = "Fecha";
            // 
            // txtUsuMod
            // 
            this.txtUsuMod.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuMod.Location = new System.Drawing.Point(68, 19);
            this.txtUsuMod.Name = "txtUsuMod";
            this.txtUsuMod.ReadOnly = true;
            this.txtUsuMod.Size = new System.Drawing.Size(151, 20);
            this.txtUsuMod.TabIndex = 1;
            // 
            // GroupBox1
            // 
            this.GroupBox1.Controls.Add(this.txtCRol);
            this.GroupBox1.Controls.Add(this.Label8);
            this.GroupBox1.Controls.Add(this.txtCApellido);
            this.GroupBox1.Controls.Add(this.Label7);
            this.GroupBox1.Controls.Add(this.txtCNombre);
            this.GroupBox1.Controls.Add(this.Label6);
            this.GroupBox1.Controls.Add(this.txtFecCre);
            this.GroupBox1.Controls.Add(this.Label1);
            this.GroupBox1.Controls.Add(this.Label2);
            this.GroupBox1.Controls.Add(this.txtUsuCre);
            this.GroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GroupBox1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.GroupBox1.Location = new System.Drawing.Point(14, 10);
            this.GroupBox1.Name = "GroupBox1";
            this.GroupBox1.Size = new System.Drawing.Size(452, 100);
            this.GroupBox1.TabIndex = 0;
            this.GroupBox1.TabStop = false;
            this.GroupBox1.Text = "Creación";
            // 
            // txtCRol
            // 
            this.txtCRol.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCRol.Location = new System.Drawing.Point(68, 71);
            this.txtCRol.Name = "txtCRol";
            this.txtCRol.ReadOnly = true;
            this.txtCRol.Size = new System.Drawing.Size(151, 20);
            this.txtCRol.TabIndex = 9;
            this.txtCRol.Visible = false;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label8.Location = new System.Drawing.Point(11, 71);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(23, 13);
            this.Label8.TabIndex = 8;
            this.Label8.Text = "Rol";
            this.Label8.Visible = false;
            // 
            // txtCApellido
            // 
            this.txtCApellido.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCApellido.Location = new System.Drawing.Point(294, 45);
            this.txtCApellido.Name = "txtCApellido";
            this.txtCApellido.ReadOnly = true;
            this.txtCApellido.Size = new System.Drawing.Size(148, 20);
            this.txtCApellido.TabIndex = 7;
            // 
            // Label7
            // 
            this.Label7.AutoSize = true;
            this.Label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label7.Location = new System.Drawing.Point(237, 45);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(44, 13);
            this.Label7.TabIndex = 5;
            this.Label7.Text = "Apellido";
            // 
            // txtCNombre
            // 
            this.txtCNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCNombre.Location = new System.Drawing.Point(68, 45);
            this.txtCNombre.Name = "txtCNombre";
            this.txtCNombre.ReadOnly = true;
            this.txtCNombre.Size = new System.Drawing.Size(151, 20);
            this.txtCNombre.TabIndex = 4;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label6.Location = new System.Drawing.Point(11, 48);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(44, 13);
            this.Label6.TabIndex = 6;
            this.Label6.Text = "Nombre";
            // 
            // txtFecCre
            // 
            this.txtFecCre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFecCre.Location = new System.Drawing.Point(294, 19);
            this.txtFecCre.Name = "txtFecCre";
            this.txtFecCre.ReadOnly = true;
            this.txtFecCre.Size = new System.Drawing.Size(148, 20);
            this.txtFecCre.TabIndex = 3;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label1.Location = new System.Drawing.Point(11, 22);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(43, 13);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "Usuario";
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Label2.Location = new System.Drawing.Point(237, 19);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(37, 13);
            this.Label2.TabIndex = 0;
            this.Label2.Text = "Fecha";
            // 
            // txtUsuCre
            // 
            this.txtUsuCre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuCre.Location = new System.Drawing.Point(68, 19);
            this.txtUsuCre.Name = "txtUsuCre";
            this.txtUsuCre.ReadOnly = true;
            this.txtUsuCre.Size = new System.Drawing.Size(151, 20);
            this.txtUsuCre.TabIndex = 2;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAceptar.Location = new System.Drawing.Point(358, 280);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(108, 34);
            this.btnAceptar.TabIndex = 3;
            this.btnAceptar.Text = "&Aceptar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // FAuditaria
            // 
            this.AcceptButton = this.btnAceptar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnAceptar;
            this.ClientSize = new System.Drawing.Size(476, 328);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.GroupBox3);
            this.Controls.Add(this.GroupBox2);
            this.Controls.Add(this.GroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.IconOptions.ShowIcon = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FAuditaria";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Auditaría del Registro";
            this.Load += new System.EventHandler(this.fAuditaria_Load);
            this.GroupBox3.ResumeLayout(false);
            this.GroupBox3.PerformLayout();
            this.GroupBox2.ResumeLayout(false);
            this.GroupBox2.PerformLayout();
            this.GroupBox1.ResumeLayout(false);
            this.GroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.GroupBox GroupBox3;
        internal System.Windows.Forms.TextBox txtApiEstado;
        internal System.Windows.Forms.Label Label5;
        internal System.Windows.Forms.GroupBox GroupBox2;
        internal System.Windows.Forms.TextBox txtMRol;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox txtMApellido;
        internal System.Windows.Forms.Label Label10;
        internal System.Windows.Forms.TextBox txtMNombre;
        internal System.Windows.Forms.Label Label11;
        internal System.Windows.Forms.TextBox txtFecMod;
        internal System.Windows.Forms.Label Label12;
        internal System.Windows.Forms.Label Label13;
        internal System.Windows.Forms.TextBox txtUsuMod;
        internal System.Windows.Forms.GroupBox GroupBox1;
        internal System.Windows.Forms.TextBox txtCRol;
        internal System.Windows.Forms.Label Label8;
        internal System.Windows.Forms.TextBox txtCApellido;
        internal System.Windows.Forms.Label Label7;
        internal System.Windows.Forms.TextBox txtCNombre;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.TextBox txtFecCre;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Label Label2;
        internal System.Windows.Forms.TextBox txtUsuCre;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
    }
}