﻿using ReAl.GestionMedica.BackendConnector.Controllers;

namespace ReAl.GestionMedica.App
{
    public partial class FAuditaria : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public FAuditaria(string usuCre, DateTime fecCre, string usuMod, DateTime? fecMod, string apiEstado)
        {
            InitializeComponent();

            this.txtUsuCre.Text = usuCre;
            this.txtFecCre.Text = fecCre.ToString(cParametrosApp.ParFormatoFechaHora);
            if (string.IsNullOrEmpty(usuMod))
                usuMod = "";
            else this.txtUsuMod.Text = usuMod;

            if (fecMod == null)
                this.txtFecMod.Text = "";
            else
                this.txtFecMod.Text = Convert.ToDateTime(fecMod.Value).ToString(cParametrosApp.ParFormatoFechaHora);

            this.txtApiEstado.Text = apiEstado;
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fAuditaria_Load(object sender, EventArgs e)
        {
            try
            {
                //Obtenemos los Daots del Usuario
                var rn = new SegUsersController();

                if (!string.IsNullOrEmpty(txtUsuCre.Text.Trim()))
                {
                    var objCre = rn.GetByLogin(txtUsuCre.Text);
                    if (objCre != null)
                    {
                        txtCNombre.Text = objCre.FirstName;
                        txtCApellido.Text = objCre.LastName;
                    }
                }

                if (!string.IsNullOrEmpty(txtUsuMod.Text.Trim()))
                {
                    var objMod = rn.GetByLogin(txtUsuMod.Text);
                    if (objMod != null)
                    {
                        txtMNombre.Text = objMod.FirstName;
                        txtMApellido.Text = objMod.LastName;
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        #endregion Methods
    }
}