﻿
namespace ReAl.GestionMedica.App
{
    partial class FPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPrincipal));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.applicationMenu1 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.bbiAgenda = new DevExpress.XtraBars.BarButtonItem();
            this.bbiListadoPacientes = new DevExpress.XtraBars.BarButtonItem();
            this.bbiAcercaDe = new DevExpress.XtraBars.BarButtonItem();
            this.bbiSalir = new DevExpress.XtraBars.BarButtonItem();
            this.bbiHistoriaClinica = new DevExpress.XtraBars.BarButtonItem();
            this.bbiConsultas = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRecetas = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCirugias = new DevExpress.XtraBars.BarButtonItem();
            this.bbiInformes = new DevExpress.XtraBars.BarButtonItem();
            this.bbiMultimedia = new DevExpress.XtraBars.BarButtonItem();
            this.bbiVentanas = new DevExpress.XtraBars.BarMdiChildrenListItem();
            this.bbiTerminar = new DevExpress.XtraBars.BarButtonItem();
            this.bbiNombresPaciente = new DevExpress.XtraBars.BarStaticItem();
            this.bbiEstadoCivil = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTipoSangre = new DevExpress.XtraBars.BarButtonItem();
            this.bbiTiposPlantilla = new DevExpress.XtraBars.BarButtonItem();
            this.bbiPlantillas = new DevExpress.XtraBars.BarButtonItem();
            this.bbiLibro = new DevExpress.XtraBars.BarButtonItem();
            this.bbiIngreso = new DevExpress.XtraBars.BarButtonItem();
            this.bbiGasto = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCategorias = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRepCategoria = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRepTendencias = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCobros = new DevExpress.XtraBars.BarButtonItem();
            this.bbiUsuarios = new DevExpress.XtraBars.BarButtonItem();
            this.bsiFechaHora = new DevExpress.XtraBars.BarStaticItem();
            this.bsiCopyrights = new DevExpress.XtraBars.BarStaticItem();
            this.bbiRptTendenciaPacientes = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRptGastos = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRptPacientesFrecuentes = new DevExpress.XtraBars.BarButtonItem();
            this.bbiRptAusencias = new DevExpress.XtraBars.BarButtonItem();
            this.bbiMigracion = new DevExpress.XtraBars.BarButtonItem();
            this.bbiApellidosPaciente = new DevExpress.XtraBars.BarStaticItem();
            this.bbiCambiarPassword = new DevExpress.XtraBars.BarButtonItem();
            this.bbiBackups = new DevExpress.XtraBars.BarButtonItem();
            this.bbiCertificados = new DevExpress.XtraBars.BarButtonItem();
            this.rpPacientes = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgAgenda = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgPaciente = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgSistema1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpIngresos = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgGastos = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCategoriasGastos = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgSistema2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpParametros = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgPlantillas = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgCatalogos = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgUsuarios = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgSeguridad = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgSistema3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpReportes = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.rpgRptCitas = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgRptPacientes = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgRptGastos = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.rpgSistema4 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.tmrRefresh = new System.Windows.Forms.Timer(this.components);
            this.skinDropDownButtonItem1 = new DevExpress.XtraBars.SkinDropDownButtonItem();
            this.skinPaletteDropDownButtonItem1 = new DevExpress.XtraBars.SkinPaletteDropDownButtonItem();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ApplicationButtonDropDownControl = this.applicationMenu1;
            this.ribbon.ApplicationButtonImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.Logo;
            this.ribbon.ApplicationButtonText = null;
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.ribbon.SearchEditItem,
            this.bbiAgenda,
            this.bbiListadoPacientes,
            this.bbiHistoriaClinica,
            this.bbiConsultas,
            this.bbiRecetas,
            this.bbiCirugias,
            this.bbiInformes,
            this.bbiMultimedia,
            this.bbiVentanas,
            this.bbiTerminar,
            this.bbiNombresPaciente,
            this.bbiEstadoCivil,
            this.bbiTipoSangre,
            this.bbiTiposPlantilla,
            this.bbiPlantillas,
            this.bbiLibro,
            this.bbiIngreso,
            this.bbiGasto,
            this.bbiCategorias,
            this.bbiRepCategoria,
            this.bbiRepTendencias,
            this.bbiCobros,
            this.bbiUsuarios,
            this.bsiFechaHora,
            this.bsiCopyrights,
            this.bbiSalir,
            this.bbiRptTendenciaPacientes,
            this.bbiRptGastos,
            this.bbiRptPacientesFrecuentes,
            this.bbiRptAusencias,
            this.bbiMigracion,
            this.bbiApellidosPaciente,
            this.bbiAcercaDe,
            this.bbiCambiarPassword,
            this.bbiBackups,
            this.bbiCertificados,
            this.skinDropDownButtonItem1,
            this.skinPaletteDropDownButtonItem1});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 51;
            this.ribbon.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.rpPacientes,
            this.rpIngresos,
            this.rpParametros,
            this.rpReportes});
            this.ribbon.Size = new System.Drawing.Size(1016, 158);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            // 
            // applicationMenu1
            // 
            this.applicationMenu1.ItemLinks.Add(this.bbiAgenda);
            this.applicationMenu1.ItemLinks.Add(this.bbiListadoPacientes);
            this.applicationMenu1.ItemLinks.Add(this.bbiAcercaDe);
            this.applicationMenu1.ItemLinks.Add(this.bbiSalir);
            this.applicationMenu1.Name = "applicationMenu1";
            this.applicationMenu1.Ribbon = this.ribbon;
            // 
            // bbiAgenda
            // 
            this.bbiAgenda.Caption = "Mi Agenda";
            this.bbiAgenda.Id = 1;
            this.bbiAgenda.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.agenda;
            this.bbiAgenda.LargeWidth = 65;
            this.bbiAgenda.Name = "bbiAgenda";
            this.bbiAgenda.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAgenda.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAgenda_ItemClick);
            // 
            // bbiListadoPacientes
            // 
            this.bbiListadoPacientes.Caption = "Mis Pacientes";
            this.bbiListadoPacientes.Id = 2;
            this.bbiListadoPacientes.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.pacientes;
            this.bbiListadoPacientes.LargeWidth = 65;
            this.bbiListadoPacientes.Name = "bbiListadoPacientes";
            this.bbiListadoPacientes.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiListadoPacientes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiListadoPacientes_ItemClick);
            // 
            // bbiAcercaDe
            // 
            this.bbiAcercaDe.Caption = "Acerca de...";
            this.bbiAcercaDe.Id = 45;
            this.bbiAcercaDe.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.Logo;
            this.bbiAcercaDe.LargeWidth = 75;
            this.bbiAcercaDe.Name = "bbiAcercaDe";
            this.bbiAcercaDe.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiAcercaDe.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiAcercaDe_ItemClick);
            // 
            // bbiSalir
            // 
            this.bbiSalir.Caption = "&Salir";
            this.bbiSalir.Id = 35;
            this.bbiSalir.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.exit;
            this.bbiSalir.Name = "bbiSalir";
            this.bbiSalir.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiSalir.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiSalir_ItemClick);
            // 
            // bbiHistoriaClinica
            // 
            this.bbiHistoriaClinica.Caption = "Historia Clínica";
            this.bbiHistoriaClinica.Enabled = false;
            this.bbiHistoriaClinica.Id = 3;
            this.bbiHistoriaClinica.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.historia;
            this.bbiHistoriaClinica.LargeWidth = 65;
            this.bbiHistoriaClinica.Name = "bbiHistoriaClinica";
            this.bbiHistoriaClinica.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiHistoriaClinica.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiHistoriaClinica_ItemClick);
            // 
            // bbiConsultas
            // 
            this.bbiConsultas.Caption = "Consultas";
            this.bbiConsultas.Enabled = false;
            this.bbiConsultas.Id = 4;
            this.bbiConsultas.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.consultas;
            this.bbiConsultas.LargeWidth = 65;
            this.bbiConsultas.Name = "bbiConsultas";
            this.bbiConsultas.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiConsultas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiConsultas_ItemClick);
            // 
            // bbiRecetas
            // 
            this.bbiRecetas.Caption = "Recetas";
            this.bbiRecetas.Enabled = false;
            this.bbiRecetas.Id = 5;
            this.bbiRecetas.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.receta;
            this.bbiRecetas.LargeWidth = 65;
            this.bbiRecetas.Name = "bbiRecetas";
            this.bbiRecetas.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiRecetas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRecetas_ItemClick);
            // 
            // bbiCirugias
            // 
            this.bbiCirugias.Caption = "Cirugías";
            this.bbiCirugias.Enabled = false;
            this.bbiCirugias.Id = 6;
            this.bbiCirugias.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.cirujias;
            this.bbiCirugias.LargeWidth = 65;
            this.bbiCirugias.Name = "bbiCirugias";
            this.bbiCirugias.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiCirugias.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCirugias_ItemClick);
            // 
            // bbiInformes
            // 
            this.bbiInformes.Caption = "Informes Médicos";
            this.bbiInformes.Enabled = false;
            this.bbiInformes.Id = 7;
            this.bbiInformes.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.informes;
            this.bbiInformes.LargeWidth = 65;
            this.bbiInformes.Name = "bbiInformes";
            this.bbiInformes.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiInformes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiInformes_ItemClick);
            // 
            // bbiMultimedia
            // 
            this.bbiMultimedia.Caption = "Imágenes y Video";
            this.bbiMultimedia.Enabled = false;
            this.bbiMultimedia.Id = 8;
            this.bbiMultimedia.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.iamgenes;
            this.bbiMultimedia.LargeWidth = 65;
            this.bbiMultimedia.Name = "bbiMultimedia";
            this.bbiMultimedia.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiMultimedia.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiMultimedia_ItemClick);
            // 
            // bbiVentanas
            // 
            this.bbiVentanas.Caption = "Ventanas Activas";
            this.bbiVentanas.Id = 9;
            this.bbiVentanas.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.windowlist;
            this.bbiVentanas.LargeWidth = 70;
            this.bbiVentanas.Name = "bbiVentanas";
            this.bbiVentanas.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // bbiTerminar
            // 
            this.bbiTerminar.Caption = "Liberar Paciente";
            this.bbiTerminar.Enabled = false;
            this.bbiTerminar.Id = 10;
            this.bbiTerminar.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.liberar;
            this.bbiTerminar.LargeWidth = 65;
            this.bbiTerminar.Name = "bbiTerminar";
            this.bbiTerminar.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiTerminar.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTerminar_ItemClick);
            // 
            // bbiNombresPaciente
            // 
            this.bbiNombresPaciente.Id = 11;
            this.bbiNombresPaciente.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bbiNombresPaciente.ItemAppearance.Normal.Options.UseFont = true;
            this.bbiNombresPaciente.Name = "bbiNombresPaciente";
            this.bbiNombresPaciente.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiNombresPaciente_ItemDoubleClick);
            // 
            // bbiEstadoCivil
            // 
            this.bbiEstadoCivil.Caption = "Estado Civil";
            this.bbiEstadoCivil.Id = 19;
            this.bbiEstadoCivil.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiEstadoCivil.ImageOptions.LargeImage")));
            this.bbiEstadoCivil.LargeWidth = 75;
            this.bbiEstadoCivil.Name = "bbiEstadoCivil";
            this.bbiEstadoCivil.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiEstadoCivil.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiEstadoCivil_ItemClick);
            // 
            // bbiTipoSangre
            // 
            this.bbiTipoSangre.Caption = "Tipo Sangre";
            this.bbiTipoSangre.Id = 20;
            this.bbiTipoSangre.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiTipoSangre.ImageOptions.LargeImage")));
            this.bbiTipoSangre.LargeWidth = 75;
            this.bbiTipoSangre.Name = "bbiTipoSangre";
            this.bbiTipoSangre.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiTipoSangre.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTipoSangre_ItemClick);
            // 
            // bbiTiposPlantilla
            // 
            this.bbiTiposPlantilla.Caption = "Tipos de Plantillas";
            this.bbiTiposPlantilla.Id = 21;
            this.bbiTiposPlantilla.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiTiposPlantilla.ImageOptions.LargeImage")));
            this.bbiTiposPlantilla.LargeWidth = 75;
            this.bbiTiposPlantilla.Name = "bbiTiposPlantilla";
            this.bbiTiposPlantilla.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiTiposPlantilla.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiTiposPlantilla_ItemClick);
            // 
            // bbiPlantillas
            // 
            this.bbiPlantillas.Caption = "Plantillas";
            this.bbiPlantillas.Id = 22;
            this.bbiPlantillas.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("bbiPlantillas.ImageOptions.LargeImage")));
            this.bbiPlantillas.LargeWidth = 75;
            this.bbiPlantillas.Name = "bbiPlantillas";
            this.bbiPlantillas.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiPlantillas.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiPlantillas_ItemClick);
            // 
            // bbiLibro
            // 
            this.bbiLibro.Caption = "Libro de Cuentas";
            this.bbiLibro.Id = 24;
            this.bbiLibro.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.wallet;
            this.bbiLibro.LargeWidth = 70;
            this.bbiLibro.Name = "bbiLibro";
            this.bbiLibro.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiLibro.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiLibro_ItemClick);
            // 
            // bbiIngreso
            // 
            this.bbiIngreso.Caption = "Nuevo Ingreso";
            this.bbiIngreso.Id = 25;
            this.bbiIngreso.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.plus_icon;
            this.bbiIngreso.LargeWidth = 70;
            this.bbiIngreso.Name = "bbiIngreso";
            this.bbiIngreso.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiIngreso.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiIngreso_ItemClick);
            // 
            // bbiGasto
            // 
            this.bbiGasto.Caption = "Nuevo Gasto";
            this.bbiGasto.Id = 26;
            this.bbiGasto.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.minus_icon;
            this.bbiGasto.LargeWidth = 70;
            this.bbiGasto.Name = "bbiGasto";
            this.bbiGasto.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiGasto.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiGasto_ItemClick);
            // 
            // bbiCategorias
            // 
            this.bbiCategorias.Caption = "Categorías";
            this.bbiCategorias.Id = 27;
            this.bbiCategorias.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.Cash_register_icon;
            this.bbiCategorias.LargeWidth = 70;
            this.bbiCategorias.Name = "bbiCategorias";
            this.bbiCategorias.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiCategorias.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCategorias_ItemClick);
            // 
            // bbiRepCategoria
            // 
            this.bbiRepCategoria.Caption = "Gráficos por Categoría";
            this.bbiRepCategoria.Id = 28;
            this.bbiRepCategoria.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.rptPieChart;
            this.bbiRepCategoria.LargeWidth = 70;
            this.bbiRepCategoria.Name = "bbiRepCategoria";
            this.bbiRepCategoria.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiRepCategoria.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRepCategoria_ItemClick);
            // 
            // bbiRepTendencias
            // 
            this.bbiRepTendencias.Caption = "Tendencias de Movimiento";
            this.bbiRepTendencias.Id = 29;
            this.bbiRepTendencias.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.rptBarChart;
            this.bbiRepTendencias.LargeWidth = 70;
            this.bbiRepTendencias.Name = "bbiRepTendencias";
            this.bbiRepTendencias.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiRepTendencias.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRepTendencias_ItemClick);
            // 
            // bbiCobros
            // 
            this.bbiCobros.Caption = "Cobros Pacientes";
            this.bbiCobros.Id = 30;
            this.bbiCobros.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.payment_icon;
            this.bbiCobros.LargeWidth = 65;
            this.bbiCobros.Name = "bbiCobros";
            this.bbiCobros.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiCobros.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCobros_ItemClick);
            // 
            // bbiUsuarios
            // 
            this.bbiUsuarios.Caption = "Usuarios";
            this.bbiUsuarios.Id = 31;
            this.bbiUsuarios.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.card_file;
            this.bbiUsuarios.LargeWidth = 75;
            this.bbiUsuarios.Name = "bbiUsuarios";
            this.bbiUsuarios.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiUsuarios.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiUsuarios_ItemClick);
            // 
            // bsiFechaHora
            // 
            this.bsiFechaHora.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.bsiFechaHora.Caption = "--";
            this.bsiFechaHora.Id = 32;
            this.bsiFechaHora.Name = "bsiFechaHora";
            this.bsiFechaHora.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // bsiCopyrights
            // 
            this.bsiCopyrights.Caption = "Copyright";
            this.bsiCopyrights.Id = 33;
            this.bsiCopyrights.Name = "bsiCopyrights";
            this.bsiCopyrights.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            // 
            // bbiRptTendenciaPacientes
            // 
            this.bbiRptTendenciaPacientes.Caption = "Tendencia de Atención";
            this.bbiRptTendenciaPacientes.Id = 39;
            this.bbiRptTendenciaPacientes.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.rptBarChart;
            this.bbiRptTendenciaPacientes.LargeWidth = 75;
            this.bbiRptTendenciaPacientes.Name = "bbiRptTendenciaPacientes";
            this.bbiRptTendenciaPacientes.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiRptTendenciaPacientes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRptTendenciaPacientes_ItemClick);
            // 
            // bbiRptGastos
            // 
            this.bbiRptGastos.Caption = "Libro Mayor";
            this.bbiRptGastos.Id = 40;
            this.bbiRptGastos.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.wallet;
            this.bbiRptGastos.LargeWidth = 75;
            this.bbiRptGastos.Name = "bbiRptGastos";
            this.bbiRptGastos.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiRptGastos.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRptGastos_ItemClick);
            // 
            // bbiRptPacientesFrecuentes
            // 
            this.bbiRptPacientesFrecuentes.Caption = "Pacientes Frecuentes";
            this.bbiRptPacientesFrecuentes.Id = 41;
            this.bbiRptPacientesFrecuentes.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.pacientes;
            this.bbiRptPacientesFrecuentes.LargeWidth = 75;
            this.bbiRptPacientesFrecuentes.Name = "bbiRptPacientesFrecuentes";
            this.bbiRptPacientesFrecuentes.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiRptPacientesFrecuentes.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRptPacientesFrecuentes_ItemClick);
            // 
            // bbiRptAusencias
            // 
            this.bbiRptAusencias.Caption = "Ausencia de Pacientes";
            this.bbiRptAusencias.Id = 42;
            this.bbiRptAusencias.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.trash;
            this.bbiRptAusencias.LargeWidth = 75;
            this.bbiRptAusencias.Name = "bbiRptAusencias";
            this.bbiRptAusencias.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiRptAusencias.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiRptAusencias_ItemClick);
            // 
            // bbiMigracion
            // 
            this.bbiMigracion.Caption = "Herramientas de Migración";
            this.bbiMigracion.Id = 43;
            this.bbiMigracion.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.db_comit_256;
            this.bbiMigracion.LargeWidth = 75;
            this.bbiMigracion.Name = "bbiMigracion";
            this.bbiMigracion.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiMigracion.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiMigracion_ItemClick);
            // 
            // bbiApellidosPaciente
            // 
            this.bbiApellidosPaciente.Id = 44;
            this.bbiApellidosPaciente.ItemAppearance.Normal.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Bold);
            this.bbiApellidosPaciente.ItemAppearance.Normal.Options.UseFont = true;
            this.bbiApellidosPaciente.Name = "bbiApellidosPaciente";
            this.bbiApellidosPaciente.ItemDoubleClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiApellidosPaciente_ItemDoubleClick);
            // 
            // bbiCambiarPassword
            // 
            this.bbiCambiarPassword.Caption = "Cambiar Password";
            this.bbiCambiarPassword.Id = 46;
            this.bbiCambiarPassword.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.Money_Safe_icon;
            this.bbiCambiarPassword.LargeWidth = 75;
            this.bbiCambiarPassword.Name = "bbiCambiarPassword";
            this.bbiCambiarPassword.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiCambiarPassword.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCambiarPassword_ItemClick);
            // 
            // bbiBackups
            // 
            this.bbiBackups.Caption = "Copia de Seguridad";
            this.bbiBackups.Id = 47;
            this.bbiBackups.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.bd_backup;
            this.bbiBackups.LargeWidth = 75;
            this.bbiBackups.Name = "bbiBackups";
            this.bbiBackups.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiBackups.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiBackups_ItemClick);
            // 
            // bbiCertificados
            // 
            this.bbiCertificados.Caption = "Certificados Médicos";
            this.bbiCertificados.Enabled = false;
            this.bbiCertificados.Id = 48;
            this.bbiCertificados.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.certificado;
            this.bbiCertificados.LargeWidth = 65;
            this.bbiCertificados.Name = "bbiCertificados";
            this.bbiCertificados.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.bbiCertificados.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.bbiCertificados_ItemClick);
            // 
            // rpPacientes
            // 
            this.rpPacientes.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgAgenda,
            this.rpgPaciente,
            this.rpgSistema1});
            this.rpPacientes.Name = "rpPacientes";
            this.rpPacientes.Text = "Sistema de Gestion de Pacientes";
            // 
            // rpgAgenda
            // 
            this.rpgAgenda.ItemLinks.Add(this.bbiAgenda);
            this.rpgAgenda.ItemLinks.Add(this.bbiCobros);
            this.rpgAgenda.Name = "rpgAgenda";
            this.rpgAgenda.Text = "Citas";
            // 
            // rpgPaciente
            // 
            this.rpgPaciente.ItemLinks.Add(this.bbiListadoPacientes);
            this.rpgPaciente.ItemLinks.Add(this.bbiNombresPaciente);
            this.rpgPaciente.ItemLinks.Add(this.bbiApellidosPaciente);
            this.rpgPaciente.ItemLinks.Add(this.bbiHistoriaClinica);
            this.rpgPaciente.ItemLinks.Add(this.bbiConsultas);
            this.rpgPaciente.ItemLinks.Add(this.bbiRecetas);
            this.rpgPaciente.ItemLinks.Add(this.bbiCirugias);
            this.rpgPaciente.ItemLinks.Add(this.bbiInformes);
            this.rpgPaciente.ItemLinks.Add(this.bbiMultimedia);
            this.rpgPaciente.ItemLinks.Add(this.bbiCertificados);
            this.rpgPaciente.ItemLinks.Add(this.bbiTerminar);
            this.rpgPaciente.Name = "rpgPaciente";
            this.rpgPaciente.Text = "Paciente";
            // 
            // rpgSistema1
            // 
            this.rpgSistema1.ImageOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.windowlist;
            this.rpgSistema1.ItemLinks.Add(this.bbiVentanas);
            this.rpgSistema1.Name = "rpgSistema1";
            this.rpgSistema1.Text = "Ventanas";
            // 
            // rpIngresos
            // 
            this.rpIngresos.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgGastos,
            this.rpgCategoriasGastos,
            this.rpgSistema2});
            this.rpIngresos.Name = "rpIngresos";
            this.rpIngresos.Text = "Ingresos y Gastos";
            // 
            // rpgGastos
            // 
            this.rpgGastos.ItemLinks.Add(this.bbiLibro);
            this.rpgGastos.ItemLinks.Add(this.bbiIngreso);
            this.rpgGastos.ItemLinks.Add(this.bbiGasto);
            this.rpgGastos.Name = "rpgGastos";
            this.rpgGastos.Text = "Ingresos y Gastos";
            // 
            // rpgCategoriasGastos
            // 
            this.rpgCategoriasGastos.ItemLinks.Add(this.bbiCategorias);
            this.rpgCategoriasGastos.Name = "rpgCategoriasGastos";
            this.rpgCategoriasGastos.Text = "Categorías";
            // 
            // rpgSistema2
            // 
            this.rpgSistema2.ItemLinks.Add(this.bbiVentanas);
            this.rpgSistema2.Name = "rpgSistema2";
            this.rpgSistema2.Text = "Ventanas";
            // 
            // rpParametros
            // 
            this.rpParametros.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgPlantillas,
            this.rpgCatalogos,
            this.rpgUsuarios,
            this.rpgSeguridad,
            this.rpgSistema3});
            this.rpParametros.Name = "rpParametros";
            this.rpParametros.Text = "Parámetros del Sistema";
            // 
            // rpgPlantillas
            // 
            this.rpgPlantillas.ItemLinks.Add(this.bbiTiposPlantilla);
            this.rpgPlantillas.ItemLinks.Add(this.bbiPlantillas);
            this.rpgPlantillas.Name = "rpgPlantillas";
            this.rpgPlantillas.Text = "Plantillas";
            // 
            // rpgCatalogos
            // 
            this.rpgCatalogos.ItemLinks.Add(this.bbiEstadoCivil);
            this.rpgCatalogos.ItemLinks.Add(this.bbiTipoSangre);
            this.rpgCatalogos.Name = "rpgCatalogos";
            this.rpgCatalogos.Text = "Catálogos";
            // 
            // rpgUsuarios
            // 
            this.rpgUsuarios.ItemLinks.Add(this.bbiUsuarios);
            this.rpgUsuarios.ItemLinks.Add(this.bbiMigracion);
            this.rpgUsuarios.Name = "rpgUsuarios";
            this.rpgUsuarios.Text = "Usuarios del Sistema";
            // 
            // rpgSeguridad
            // 
            this.rpgSeguridad.ItemLinks.Add(this.bbiCambiarPassword);
            this.rpgSeguridad.ItemLinks.Add(this.bbiBackups);
            this.rpgSeguridad.ItemLinks.Add(this.bbiAcercaDe);
            this.rpgSeguridad.Name = "rpgSeguridad";
            this.rpgSeguridad.Text = "Seguridad";
            // 
            // rpgSistema3
            // 
            this.rpgSistema3.ItemLinks.Add(this.bbiVentanas);
            this.rpgSistema3.Name = "rpgSistema3";
            this.rpgSistema3.Text = "Ventanas";
            // 
            // rpReportes
            // 
            this.rpReportes.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.rpgRptCitas,
            this.rpgRptPacientes,
            this.rpgRptGastos,
            this.rpgSistema4});
            this.rpReportes.Name = "rpReportes";
            this.rpReportes.Text = "Reportes";
            // 
            // rpgRptCitas
            // 
            this.rpgRptCitas.ItemLinks.Add(this.bbiRptTendenciaPacientes);
            this.rpgRptCitas.Name = "rpgRptCitas";
            this.rpgRptCitas.Text = "Citas";
            // 
            // rpgRptPacientes
            // 
            this.rpgRptPacientes.ItemLinks.Add(this.bbiRptAusencias);
            this.rpgRptPacientes.ItemLinks.Add(this.bbiRptPacientesFrecuentes);
            this.rpgRptPacientes.Name = "rpgRptPacientes";
            this.rpgRptPacientes.Text = "Pacientes";
            // 
            // rpgRptGastos
            // 
            this.rpgRptGastos.ItemLinks.Add(this.bbiRepTendencias);
            this.rpgRptGastos.ItemLinks.Add(this.bbiRepCategoria);
            this.rpgRptGastos.ItemLinks.Add(this.bbiRptGastos);
            this.rpgRptGastos.Name = "rpgRptGastos";
            this.rpgRptGastos.Text = "Gastos";
            // 
            // rpgSistema4
            // 
            this.rpgSistema4.ItemLinks.Add(this.bbiVentanas);
            this.rpgSistema4.Name = "rpgSistema4";
            this.rpgSistema4.Text = "Ventanas";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.skinDropDownButtonItem1);
            this.ribbonStatusBar.ItemLinks.Add(this.skinPaletteDropDownButtonItem1);
            this.ribbonStatusBar.ItemLinks.Add(this.bsiFechaHora);
            this.ribbonStatusBar.ItemLinks.Add(this.bsiCopyrights);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 743);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(1016, 24);
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.MdiParent = this;
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Blue";
            // 
            // tmrRefresh
            // 
            this.tmrRefresh.Enabled = true;
            this.tmrRefresh.Interval = 1000;
            this.tmrRefresh.Tick += new System.EventHandler(this.tmrRefresh_Tick);
            // 
            // skinDropDownButtonItem1
            // 
            this.skinDropDownButtonItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.skinDropDownButtonItem1.Id = 49;
            this.skinDropDownButtonItem1.Name = "skinDropDownButtonItem1";
            // 
            // skinPaletteDropDownButtonItem1
            // 
            this.skinPaletteDropDownButtonItem1.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.skinPaletteDropDownButtonItem1.Id = 50;
            this.skinPaletteDropDownButtonItem1.Name = "skinPaletteDropDownButtonItem1";
            // 
            // FPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1016, 767);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.ribbon);
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("FPrincipal.IconOptions.Icon")));
            this.IconOptions.Image = global::ReAl.GestionMedica.App.Properties.Resources.Logo;
            this.IsMdiContainer = true;
            this.Name = "FPrincipal";
            this.Ribbon = this.ribbon;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "Gestión de Pacientes";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.fPrincipal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        public DevExpress.XtraBars.Ribbon.RibbonPage rpPacientes;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgAgenda;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpIngresos;
        private DevExpress.XtraBars.BarButtonItem bbiAgenda;
        private DevExpress.XtraBars.BarButtonItem bbiListadoPacientes;
        private DevExpress.XtraBars.BarButtonItem bbiHistoriaClinica;
        private DevExpress.XtraBars.BarButtonItem bbiConsultas;
        private DevExpress.XtraBars.BarButtonItem bbiRecetas;
        private DevExpress.XtraBars.BarButtonItem bbiCirugias;
        private DevExpress.XtraBars.BarButtonItem bbiInformes;
        private DevExpress.XtraBars.BarButtonItem bbiMultimedia;
        private DevExpress.XtraBars.BarMdiChildrenListItem bbiVentanas;
        private DevExpress.XtraBars.BarButtonItem bbiTerminar;
        private DevExpress.XtraBars.BarStaticItem bbiNombresPaciente;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgPaciente;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSistema1;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSistema2;
        private System.Windows.Forms.Timer tmrRefresh;
        private DevExpress.XtraBars.BarButtonItem bbiEstadoCivil;
        private DevExpress.XtraBars.BarButtonItem bbiTipoSangre;
        private DevExpress.XtraBars.BarButtonItem bbiTiposPlantilla;
        private DevExpress.XtraBars.BarButtonItem bbiPlantillas;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpParametros;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgPlantillas;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCatalogos;
        private DevExpress.XtraBars.BarButtonItem bbiLibro;
        private DevExpress.XtraBars.BarButtonItem bbiIngreso;
        private DevExpress.XtraBars.BarButtonItem bbiGasto;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgGastos;
        private DevExpress.XtraBars.BarButtonItem bbiCategorias;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgCategoriasGastos;
        private DevExpress.XtraBars.BarButtonItem bbiRepCategoria;
        private DevExpress.XtraBars.BarButtonItem bbiRepTendencias;
        private DevExpress.XtraBars.BarButtonItem bbiCobros;
        private DevExpress.XtraBars.BarButtonItem bbiUsuarios;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgUsuarios;
        private DevExpress.XtraBars.Ribbon.RibbonPage rpReportes;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgRptCitas;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgRptPacientes;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgRptGastos;
        private DevExpress.XtraBars.BarStaticItem bsiFechaHora;
        private DevExpress.XtraBars.BarStaticItem bsiCopyrights;
        private DevExpress.XtraBars.BarButtonItem bbiSalir;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu1;
        private DevExpress.XtraBars.BarButtonItem bbiRptTendenciaPacientes;
        private DevExpress.XtraBars.BarButtonItem bbiRptGastos;
        private DevExpress.XtraBars.BarButtonItem bbiRptPacientesFrecuentes;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSistema3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSistema4;
        private DevExpress.XtraBars.BarButtonItem bbiRptAusencias;
        private DevExpress.XtraBars.BarButtonItem bbiMigracion;
        private DevExpress.XtraBars.BarStaticItem bbiApellidosPaciente;
        private DevExpress.XtraBars.BarButtonItem bbiAcercaDe;
        private DevExpress.XtraBars.BarButtonItem bbiCambiarPassword;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup rpgSeguridad;
        private DevExpress.XtraBars.BarButtonItem bbiBackups;
        private DevExpress.XtraBars.BarButtonItem bbiCertificados;
        private DevExpress.XtraBars.SkinDropDownButtonItem skinDropDownButtonItem1;
        private DevExpress.XtraBars.SkinPaletteDropDownButtonItem skinPaletteDropDownButtonItem1;
    }
}