﻿using System.Drawing.Printing;
using System.Text;
using DevExpress.XtraReports.UI;
using ReAl.GestionMedica.App.RPT.CLA;
using ReAl.GestionMedica.BackendConnector.Entities;

namespace ReAl.GestionMedica.App.RPT
{
    public partial class RptViewerCla : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        #region Fields

        private readonly ClaTemplates _myCpl = null;

        #endregion Fields

        #region Constructors

        public RptViewerCla(ClaTemplates obj)
        {
            InitializeComponent();

            _myCpl = obj;
        }

        #endregion Constructors

        #region Methods

        private void rptViewerCla_Load(object sender, EventArgs e)
        {
            var reporte = new rptCpl();

            //Parametros para no imprimir los WARNINGS del Reporte
            reporte.DefaultPrinterSettingsUsing.UseLandscape = false;
            reporte.DefaultPrinterSettingsUsing.UseMargins = false;
            reporte.DefaultPrinterSettingsUsing.UsePaperKind = false;
            reporte.ReportUnit = ReportUnit.TenthsOfAMillimeter;
            reporte.PaperKind = PaperKind.Custom;

            reporte.PageWidth = int.Parse((Math.Round(_myCpl.Width * 100)).ToString());
            reporte.PageHeight = int.Parse((Math.Round(_myCpl.Height * 100)).ToString());
            reporte.Margins.Bottom = int.Parse((Math.Round(_myCpl.MarginBottom* 100 + 100)).ToString());
            reporte.BottomMargin.Height = int.Parse((Math.Round(_myCpl.MarginBottom * 100 + 100)).ToString());
            reporte.Margins.Top = int.Parse((Math.Round(_myCpl.MarginTop * 100)).ToString());
            reporte.TopMargin.Height = int.Parse((Math.Round(_myCpl.MarginTop * 100)).ToString());
            reporte.Margins.Left = int.Parse((Math.Round(_myCpl.MarginLeft * 100)).ToString());
            reporte.Margins.Right = int.Parse((Math.Round(_myCpl.MarginRight * 100)).ToString());

            reporte.txtTexto.WidthF = reporte.PageWidth - reporte.Margins.Left - reporte.Margins.Right - 10;
            reporte.txtTexto.Width = reporte.PageWidth - reporte.Margins.Left - reporte.Margins.Right - 10;
            reporte.txtTexto.HeightF = int.Parse((Math.Round(_myCpl.Height * 100)).ToString()) - int.Parse((Math.Round(_myCpl.MarginBottom * 100 + 100)).ToString()) - int.Parse((Math.Round(_myCpl.MarginTop * 100)).ToString()) - 10;
            reporte.txtTexto.Height = int.Parse((Math.Round(_myCpl.Height * 100)).ToString()) - int.Parse((Math.Round(_myCpl.MarginBottom * 100 + 100)).ToString()) - int.Parse((Math.Round(_myCpl.MarginTop * 100)).ToString()) - 10;

            //El Cuerpo
            var enc = new ASCIIEncoding();
            reporte.txtTexto.Html = _myCpl.HtmlFormat;

            reporte.RequestParameters = false;
            reporte.ShowPrintMarginsWarning = false;
            reporte.PrintingSystem.ShowMarginsWarning = false;

            printControl1.PrintingSystem = reporte.PrintingSystem;

            reporte.CreateDocument();
        }

        #endregion Methods
    }
}