﻿using System.Collections;
using DevExpress.XtraScheduler;
using DevExpress.XtraScheduler.Reporting;
using ReAl.GestionMedica.App.RPT.CAL;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;

namespace ReAl.GestionMedica.App.RPT
{
    public partial class RptViewerCal : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        #region Fields

        private readonly DateTime _dateFecFin = DateTime.Now;
        private readonly DateTime _dateFecIni = DateTime.Now;
        private readonly SchedulerControl _schedulerControl1 = null;
        private readonly string _strTipRep = "";

        #endregion Fields

        #region Constructors

        public RptViewerCal(SchedulerControl rep)
        {
            InitializeComponent();

            _strTipRep = "A";
            _schedulerControl1 = rep;
        }

        public RptViewerCal(DateTime fecIni, DateTime fecFin)
        {
            InitializeComponent();

            _strTipRep = "T";
            _dateFecIni = fecIni;
            _dateFecFin = fecFin;
        }

        #endregion Constructors

        #region Methods

        private void CargarReporteAgenda()
        {
            //SchedulerControlPrintAdapter scPrintAdapter = new SchedulerControlPrintAdapter(this.schedulerControl1);
            //reporte.SchedulerAdapter = scPrintAdapter;
            //printControl1.PrintingSystem = _myReporte.PrintingSystem;

            //this.CreateDocument();
            var reporte = new rptCal();
            var scPrintAdapter = new SchedulerControlPrintAdapter(_schedulerControl1);
            reporte.SchedulerAdapter = scPrintAdapter;

            printControl1.PrintingSystem = reporte.PrintingSystem;
            reporte.CreateDocument(true);
        }

        private void CargarReporteTendencia()
        {
            var rn = new DashboardController();
            var dt = rn.GetAppointments(_dateFecIni.ToString("yyyyMMdd"), _dateFecFin.ToString("yyyyMMdd"));
            var rep = new rptTendenciaCitas();

            //Creamos la serie para el Reporte
            rep.xrChart1.Series[0].DataSource = dt;
            rep.xrChart1.Series[0].ArgumentDataMember = "fecha";
            rep.xrChart1.Series[0].ValueDataMembers[0] = "atendidos";

            rep.xrChart1.Series[1].DataSource = dt;
            rep.xrChart1.Series[1].ArgumentDataMember = "fecha";
            rep.xrChart1.Series[1].ValueDataMembers[0] = "ausentes";

            rep.xrChart1.Series[2].DataSource = dt;
            rep.xrChart1.Series[2].ArgumentDataMember = "fecha";
            rep.xrChart1.Series[2].ValueDataMembers[0] = "programados";

            printControl1.PrintingSystem = rep.PrintingSystem;
            rep.CreateDocument();
        }

        private void rptViewerCal_Load(object sender, EventArgs e)
        {
            switch (_strTipRep)
            {
                case "A":
                    CargarReporteAgenda();
                    break;

                case "T":
                    this.CargarReporteTendencia();
                    break;
            }
        }

        #endregion Methods
    }
}