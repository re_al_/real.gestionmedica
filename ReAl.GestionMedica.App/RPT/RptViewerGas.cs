﻿using System.Collections;
using DevExpress.XtraCharts;
using ReAl.GestionMedica.App.RPT.GAS;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;

namespace ReAl.GestionMedica.App.RPT
{
    public partial class RptViewerGas : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        #region Fields

        private readonly DateTime _dateFecFin = DateTime.Now;
        private readonly DateTime _dateFecIni = DateTime.Now;
        private readonly string _strTipoRep = "";

        #endregion Fields

        #region Constructors

        public RptViewerGas(string strRep, DateTime fecIni, DateTime fecFin)
        {
            InitializeComponent();

            _dateFecIni = fecIni;
            _dateFecFin = fecFin;
            _strTipoRep = strRep;
        }

        #endregion Constructors

        #region Methods

        private void CargarReporteCategoria()
        {
            var rn = new DashboardController();
            var dt = rn.GetCategory(_dateFecIni.ToString("yyyyMMdd"), _dateFecFin.ToString("yyyyMMdd"), _strTipoRep);

            var rep = new rptCategoria();

            //Creamos la serie para el Reporte
            var pie3DSeriesView1 = new Pie3DSeriesView();
            rep.xrChart1.Series[0].DataSource = dt;
            rep.xrChart1.Series[0].ArgumentDataMember = "categoria";
            rep.xrChart1.Series[0].ValueDataMembers[0] = "monto";
            rep.xrChart1.Series[0].View = pie3DSeriesView1;

            printControl1.PrintingSystem = rep.PrintingSystem;
            rep.CreateDocument();
        }

        private void CargarReporteMayor()
        {
            var rn = new DashboardController();
            var dt = rn.GetMayor(_dateFecIni.ToString("yyyyMMdd"), _dateFecFin.ToString("yyyyMMdd"));

            var reporte = new rptMayor();
            reporte.DataSource = dt;
            printControl1.PrintingSystem = reporte.PrintingSystem;
            reporte.CreateDocument();
        }

        private void CargarReporteTendencia()
        {
            var rn = new DashboardController();
            var dt = rn.GetTrend(_dateFecIni.ToString("yyyyMMdd"), _dateFecFin.ToString("yyyyMMdd"));

            var rep = new rptTendencia();

            //Creamos la serie para el Reporte
            rep.xrChart1.Series[0].DataSource = dt;
            rep.xrChart1.Series[0].ArgumentDataMember = "fecha";
            rep.xrChart1.Series[0].ValueDataMembers[0] = "ingreso";

            rep.xrChart1.Series[1].DataSource = dt;
            rep.xrChart1.Series[1].ArgumentDataMember = "fecha";
            rep.xrChart1.Series[1].ValueDataMembers[0] = "gasto";

            printControl1.PrintingSystem = rep.PrintingSystem;
            rep.CreateDocument();
        }

        private void rptViewerGas_Load(object sender, EventArgs e)
        {
            switch (_strTipoRep)
            {
                case "I":
                    CargarReporteCategoria();
                    break;

                case "G":
                    CargarReporteCategoria();
                    break;

                case "T":
                    CargarReporteTendencia();
                    break;

                case "M":
                    CargarReporteMayor();
                    break;
            }
        }

        #endregion Methods
    }
}