namespace ReAl.GestionMedica.App.RPT.PAC
{
    public partial class rptPreview : DevExpress.XtraReports.UI.XtraReport
    {
        #region Constructors

        public rptPreview()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        private void rptPac_AfterPrint(object sender, EventArgs e)
        {
            //this.PrintingSystem.Document.AutoFitToPagesWidth = 1;
        }

        #endregion Methods
    }
}