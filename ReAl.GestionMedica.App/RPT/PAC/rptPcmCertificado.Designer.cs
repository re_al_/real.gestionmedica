﻿
namespace ReAl.GestionMedica.App.RPT.PAC
{
    partial class rptPcmCertificado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptPcmCertificado));
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.txtTexto = new DevExpress.XtraReports.UI.XRRichText();
            this.txtFecha = new DevExpress.XtraReports.UI.XRLabel();
            this.txtNombreMedico = new DevExpress.XtraReports.UI.XRLabel();
            this.txtMatricula = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.txtTexto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 48.95833F;
            this.TopMargin.Name = "TopMargin";
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 50F;
            this.BottomMargin.Name = "BottomMargin";
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.txtMatricula,
            this.txtNombreMedico,
            this.txtFecha,
            this.txtTexto,
            this.xrPictureBox1});
            this.Detail.HeightF = 922.9166F;
            this.Detail.Name = "Detail";
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.ImageSource = new DevExpress.XtraPrinting.Drawing.ImageSource(global::ReAl.GestionMedica.App.Properties.Resources.pcmCertificado, true);
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(750F, 922.9166F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.ZoomImage;
            // 
            // txtTexto
            // 
            this.txtTexto.AnchorVertical = ((DevExpress.XtraReports.UI.VerticalAnchorStyles)((DevExpress.XtraReports.UI.VerticalAnchorStyles.Top | DevExpress.XtraReports.UI.VerticalAnchorStyles.Bottom)));
            this.txtTexto.CanGrow = false;
            this.txtTexto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTexto.LocationFloat = new DevExpress.Utils.PointFloat(9.999998F, 406.25F);
            this.txtTexto.Name = "txtTexto";
            this.txtTexto.SerializableRtfString = resources.GetString("txtTexto.SerializableRtfString");
            this.txtTexto.SizeF = new System.Drawing.SizeF(730F, 353.0101F);
            this.txtTexto.StylePriority.UseFont = false;
            // 
            // txtFecha
            // 
            this.txtFecha.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFecha.LocationFloat = new DevExpress.Utils.PointFloat(380.5117F, 197.9167F);
            this.txtFecha.Name = "txtFecha";
            this.txtFecha.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtFecha.SizeF = new System.Drawing.SizeF(359.4882F, 29.25F);
            this.txtFecha.StylePriority.UseFont = false;
            this.txtFecha.StylePriority.UseTextAlignment = false;
            this.txtFecha.Text = "txtFecha";
            this.txtFecha.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txtNombreMedico
            // 
            this.txtNombreMedico.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNombreMedico.LocationFloat = new DevExpress.Utils.PointFloat(380.5117F, 250.625F);
            this.txtNombreMedico.Name = "txtNombreMedico";
            this.txtNombreMedico.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtNombreMedico.SizeF = new System.Drawing.SizeF(359.4882F, 29.25F);
            this.txtNombreMedico.StylePriority.UseFont = false;
            this.txtNombreMedico.StylePriority.UseTextAlignment = false;
            this.txtNombreMedico.Text = "txtNombreMedico";
            this.txtNombreMedico.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // txtMatricula
            // 
            this.txtMatricula.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMatricula.LocationFloat = new DevExpress.Utils.PointFloat(380.5117F, 304.7917F);
            this.txtMatricula.Name = "txtMatricula";
            this.txtMatricula.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.txtMatricula.SizeF = new System.Drawing.SizeF(159.5554F, 29.25F);
            this.txtMatricula.StylePriority.UseFont = false;
            this.txtMatricula.StylePriority.UseTextAlignment = false;
            this.txtMatricula.Text = "txtMatricula";
            this.txtMatricula.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // rptPcmCertificado
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.TopMargin,
            this.BottomMargin,
            this.Detail});
            this.Font = new System.Drawing.Font("Arial", 9.75F);
            this.Margins = new System.Drawing.Printing.Margins(48, 52, 49, 50);
            this.Version = "20.2";
            ((System.ComponentModel.ISupportInitialize)(this.txtTexto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        public DevExpress.XtraReports.UI.XRRichText txtTexto;
        public DevExpress.XtraReports.UI.XRLabel txtMatricula;
        public DevExpress.XtraReports.UI.XRLabel txtNombreMedico;
        public DevExpress.XtraReports.UI.XRLabel txtFecha;
    }
}
