﻿using System.Collections;
using System.Drawing.Printing;
using System.Text;
using DevExpress.XtraReports.UI;
using DevExpress.XtraRichEdit;
using DevExpress.XtraRichEdit.API.Native;
using ReAl.GestionMedica.App.RPT.PAC;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.Utils;

namespace ReAl.GestionMedica.App.RPT
{
    public partial class RptViewerPac : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        #region Fields

        private readonly DateTime _dateFecFin = DateTime.Now;
        private readonly DateTime _dateFecIni = DateTime.Now;
        private readonly ClaTemplates _myCpl = null;
        private readonly PatSurgeries _myPci = null;
        private readonly PatCertifications _myPcm = null;
        private readonly PatReports _myPin = null;
        private readonly PatPrescriptions _myPre = null;
        private readonly string _strTipRep = "";

        #endregion Fields

        #region Constructors

        public RptViewerPac(PatPrescriptions obj)
        {
            InitializeComponent();

            _strTipRep = "R";
            _myPre = obj;

            var rn = new ClaTemplatesController();
            _myCpl = rn.GetById(obj.IdTemplates);
        }

        public RptViewerPac(PatCertifications obj)
        {
            InitializeComponent();

            _strTipRep = "M";
            _myPcm = obj;
        }

        public RptViewerPac(PatSurgeries obj)
        {
            InitializeComponent();

            _strTipRep = "C";
            _myPci = obj;

            var rn = new ClaTemplatesController();
            _myCpl = rn.GetById(obj.IdTemplates);
        }

        public RptViewerPac(PatReports obj)
        {
            InitializeComponent();

            _strTipRep = "I";
            _myPin = obj;

            var rn = new ClaTemplatesController();
            _myCpl = rn.GetById(obj.IdTemplates);
        }

        public RptViewerPac(string strTipoRep, DateTime fecIni, DateTime fecFin)
        {
            InitializeComponent();

            _strTipRep = strTipoRep;
            _dateFecIni = fecIni;
            _dateFecFin = fecFin;
        }

        #endregion Constructors

        #region Methods

        private void CargarReporteAusencias()
        {
            var rn = new DashboardController();
            var dt = rn.GetAbsence(_dateFecIni.ToString("yyyyMMdd"), _dateFecFin.ToString("yyyyMMdd"));
            var reporte = new rptPpaAusencias();
            reporte.DataSource = dt;
            printControl1.PrintingSystem = reporte.PrintingSystem;
            reporte.CreateDocument();
        }

        private void CargarReporteCertificadoMedico()
        {
            var reporte = new rptPcmCertificadoSinMarca();

            //Parametros para no imprimir los WARNINGS del Reporte
            reporte.DefaultPrinterSettingsUsing.UseLandscape = false;
            reporte.DefaultPrinterSettingsUsing.UseMargins = false;
            reporte.DefaultPrinterSettingsUsing.UsePaperKind = false;
            reporte.RequestParameters = false;
            reporte.ShowPrintMarginsWarning = false;
            reporte.PrintingSystem.ShowMarginsWarning = false;

            //El Cuerpo
            var enc = new ASCIIEncoding();
            reporte.txtTexto.Html = _myPcm.HtmlFormat;
            reporte.txtFecha.Text = "La Paz, " + cFuncionesFechas.GetDateLiteral((DateTime)_myPcm.CertificationDate);
            reporte.txtMatricula.Text = _myPcm.Registration;
            reporte.txtNombreMedico.Text = cParametrosApp.AppRestUser.FirstName + " " + cParametrosApp.AppRestUser.LastName;
            printControl1.PrintingSystem = reporte.PrintingSystem;
            reporte.CreateDocument();
        }

        private void CargarReportePacientesFrecuentes()
        {
            //var dt = rn.ObtenerDatosProcAlm("SpPcoSelRptFrecuencia", arrNomParam, arrValParam);
            var rn = new DashboardController();
            var dt = rn.GetFrequency(_dateFecIni.ToString("yyyyMMdd"), _dateFecFin.ToString("yyyyMMdd"));

            var reporte = new rptPcoFrecuencia();
            reporte.DataSource = dt;
            printControl1.PrintingSystem = reporte.PrintingSystem;
            reporte.CreateDocument();
        }

        private void CargarReporteVistaPrevia()
        {
            var reporte = new rptPreviewMulti();
            reporte.ReportUnit = ReportUnit.TenthsOfAMillimeter;
            reporte.PaperKind = PaperKind.Custom;
            reporte.PageWidth = int.Parse((Math.Round(_myCpl.Width * 100)).ToString());
            reporte.PageHeight = int.Parse((Math.Round(_myCpl.Height * 100)).ToString());
            reporte.Margins.Bottom = int.Parse((Math.Round(_myCpl.MarginBottom * 100 + 100)).ToString());
            reporte.BottomMargin.Height = int.Parse((Math.Round(_myCpl.MarginBottom * 100 + 100)).ToString());
            reporte.Margins.Top = int.Parse((Math.Round(_myCpl.MarginTop * 100)).ToString());
            reporte.TopMargin.Height = int.Parse((Math.Round(_myCpl.MarginTop * 100)).ToString());
            reporte.Margins.Left = int.Parse((Math.Round(_myCpl.MarginLeft * 100)).ToString());
            reporte.Margins.Right = int.Parse((Math.Round(_myCpl.MarginRight * 100)).ToString());

            //Parametros para no imprimir los WARNINGS del Reporte
            reporte.DefaultPrinterSettingsUsing.UseLandscape = false;
            reporte.DefaultPrinterSettingsUsing.UseMargins = false;
            reporte.DefaultPrinterSettingsUsing.UsePaperKind = false;
            reporte.RequestParameters = false;
            reporte.ShowPrintMarginsWarning = false;
            reporte.PrintingSystem.ShowMarginsWarning = false;

            //Tamaño del TXT
            //reporte.txtTexto.WidthF = reporte.PageWidth - reporte.Margins.Left - reporte.Margins.Right - 20;
            //reporte.txtTexto.Width = reporte.PageWidth - reporte.Margins.Left - reporte.Margins.Right - 20;
            //reporte.txtTexto.HeightF = int.Parse((Math.Round(_myCpl.altocpl * 100)).ToString()) - int.Parse((Math.Round(_myCpl.margeninfcpl * 100 + 100)).ToString()) - int.Parse((Math.Round(_myCpl.margensupcpl * 100)).ToString()) - 30;
            //reporte.txtTexto.Height = int.Parse((Math.Round(_myCpl.altocpl * 100)).ToString()) - int.Parse((Math.Round(_myCpl.margeninfcpl * 100 + 100)).ToString()) - int.Parse((Math.Round(_myCpl.margensupcpl * 100)).ToString()) - 30;

            var enc = new ASCIIEncoding();
            RichEditDocumentServer server = new RichEditDocumentServer();
            DocumentRange[] ranges = null;
            DocumentPosition dp = null;
            List<MyRtfObject> collection = null;
            DocumentRange tmpRange2 = null;
            switch (_strTipRep)
            {
                case "R":
                    /*
                    reporte.txtTexto.CanGrow = true;
                    reporte.txtTexto.Rtf = enc.GetString(_myPre.rtfpre, 0, _myPre.rtfpre.Length);
                    reporte.txtFecha.Text = cFuncionesFechas.GetDateLiteral((DateTime)_myPre.fechapre);
                    printControl1.PrintingSystem = reporte.PrintingSystem;
                    printControl1.UpdatePageView();
                    reporte.CreateDocument();
                    */

                    //server.LoadDocument("Plantilla.rtf");
                    server.HtmlText = _myPre.HtmlFormat;
                    ranges = server.Document.FindAll(DevExpress.Office.Characters.PageBreak.ToString(), DevExpress.XtraRichEdit.API.Native.SearchOptions.None);
                    dp = server.Document.Paragraphs[0].Range.Start;

                    collection = new List<MyRtfObject>();
                    foreach (DocumentRange dr in ranges)
                    {
                        DocumentRange tmpRange = server.Document.CreateRange(dp, dr.Start.ToInt() - dp.ToInt());
                        collection.Add(new MyRtfObject() { RtfSplitContent = server.Document.GetRtfText(tmpRange) });
                        dp = dr.End;
                    }
                    tmpRange2 = server.Document.CreateRange(dp, server.Document.Paragraphs[server.Document.Paragraphs.Count - 1].Range.End.ToInt() - dp.ToInt());
                    collection.Add(new MyRtfObject() { RtfSplitContent = server.Document.GetRtfText(tmpRange2) });

                    reporte.DataSource = collection;

                    reporte.txtFecha.Text = cFuncionesFechas.GetDateLiteral((DateTime)_myPre.PrescriptionDate);
                    reporte.txtFecha.Visible = true;
                    printControl1.PrintingSystem = reporte.PrintingSystem;
                    reporte.CreateDocument();
                    break;

                case "C":
                    /*
                    reporte.txtTexto.CanGrow = true;
                    reporte.txtTexto.Rtf = enc.GetString(_myPci.rtfpci, 0, _myPci.rtfpci.Length);
                    reporte.txtFecha.Text = cFuncionesFechas.GetDateLiteral((DateTime)_myPci.fechapci);
                    reporte.txtFecha.Visible = false;
                    printControl1.PrintingSystem = reporte.PrintingSystem;
                    printControl1.UpdatePageView();
                    reporte.CreateDocument();
                    */

                    //server.LoadDocument("Plantilla.rtf");
                    server.HtmlText = _myPci.HtmlFormat;
                    ranges = server.Document.FindAll(DevExpress.Office.Characters.PageBreak.ToString(), DevExpress.XtraRichEdit.API.Native.SearchOptions.None);
                    dp = server.Document.Paragraphs[0].Range.Start;

                    collection = new List<MyRtfObject>();
                    foreach (DocumentRange dr in ranges)
                    {
                        DocumentRange tmpRange = server.Document.CreateRange(dp, dr.Start.ToInt() - dp.ToInt());
                        collection.Add(new MyRtfObject() { RtfSplitContent = server.Document.GetRtfText(tmpRange) });
                        dp = dr.End;
                    }
                    tmpRange2 = server.Document.CreateRange(dp, server.Document.Paragraphs[server.Document.Paragraphs.Count - 1].Range.End.ToInt() - dp.ToInt());
                    collection.Add(new MyRtfObject() { RtfSplitContent = server.Document.GetRtfText(tmpRange2) });

                    reporte.DataSource = collection;

                    reporte.txtFecha.Text = cFuncionesFechas.GetDateLiteral((DateTime)_myPci.SurgeryDate);
                    reporte.txtFecha.Visible = false;
                    printControl1.PrintingSystem = reporte.PrintingSystem;
                    reporte.CreateDocument();
                    break;

                case "I":
                    /*
                    reporte.txtTexto.CanGrow = true;
                    reporte.txtTexto.CanShrink = true;
                    reporte.txtTexto.KeepTogether = true;
                    reporte.txtTexto.AnchorHorizontal = HorizontalAnchorStyles.Both;
                    reporte.txtTexto.AnchorVertical = VerticalAnchorStyles.Both;
                    reporte.txtTexto.Rtf = enc.GetString(_myPin.rtfpin, 0, _myPin.rtfpin.Length);
                    */

                    //server.LoadDocument("Plantilla.rtf");
                    server.HtmlText = _myPin.HtmlFormat;
                    ranges = server.Document.FindAll(DevExpress.Office.Characters.PageBreak.ToString(), DevExpress.XtraRichEdit.API.Native.SearchOptions.None);
                    dp = server.Document.Paragraphs[0].Range.Start;

                    collection = new List<MyRtfObject>();
                    foreach (DocumentRange dr in ranges)
                    {
                        DocumentRange tmpRange = server.Document.CreateRange(dp, dr.Start.ToInt() - dp.ToInt());
                        collection.Add(new MyRtfObject() { RtfSplitContent = server.Document.GetRtfText(tmpRange) });
                        dp = dr.End;
                    }
                    tmpRange2 = server.Document.CreateRange(dp, server.Document.Paragraphs[server.Document.Paragraphs.Count - 1].Range.End.ToInt() - dp.ToInt());
                    collection.Add(new MyRtfObject() { RtfSplitContent = server.Document.GetRtfText(tmpRange2) });

                    reporte.DataSource = collection;

                    reporte.txtFecha.Text = cFuncionesFechas.GetDateLiteral((DateTime)_myPin.ReportDate);
                    //reporte.txtFecha.Visible = false;
                    printControl1.PrintingSystem = reporte.PrintingSystem;
                    reporte.CreateDocument();
                    break;
            }
        }

        private void CargarReporteVistaPreviaSimple()
        {
            var reporte = new rptPreview();
            reporte.ReportUnit = ReportUnit.TenthsOfAMillimeter;
            reporte.PaperKind = PaperKind.Custom;
            reporte.PageWidth = int.Parse((Math.Round(_myCpl.Width * 100)).ToString());
            reporte.PageHeight = int.Parse((Math.Round(_myCpl.Height * 100)).ToString());
            reporte.Margins.Bottom = int.Parse((Math.Round(_myCpl.MarginBottom* 100 + 100)).ToString());
            reporte.BottomMargin.Height = int.Parse((Math.Round(_myCpl.MarginBottom * 100 + 100)).ToString());
            reporte.Margins.Top = int.Parse((Math.Round(_myCpl.MarginTop * 100)).ToString());
            reporte.TopMargin.Height = int.Parse((Math.Round(_myCpl.MarginTop * 100)).ToString());
            reporte.Margins.Left = int.Parse((Math.Round(_myCpl.MarginLeft * 100)).ToString());
            reporte.Margins.Right = int.Parse((Math.Round(_myCpl.MarginRight * 100)).ToString());

            //Parametros para no imprimir los WARNINGS del Reporte
            reporte.DefaultPrinterSettingsUsing.UseLandscape = false;
            reporte.DefaultPrinterSettingsUsing.UseMargins = false;
            reporte.DefaultPrinterSettingsUsing.UsePaperKind = false;
            reporte.RequestParameters = false;
            reporte.ShowPrintMarginsWarning = false;
            reporte.PrintingSystem.ShowMarginsWarning = false;

            //Tamaño del TXT
            reporte.txtTexto.WidthF = reporte.PageWidth - reporte.Margins.Left - reporte.Margins.Right - 20;
            reporte.txtTexto.Width = reporte.PageWidth - reporte.Margins.Left - reporte.Margins.Right - 20;
            reporte.txtTexto.HeightF = int.Parse((Math.Round(_myCpl.Height * 100)).ToString()) - int.Parse((Math.Round(_myCpl.MarginBottom * 100 + 100)).ToString()) - int.Parse((Math.Round(_myCpl.MarginTop * 100)).ToString()) - 30;
            reporte.txtTexto.Height = int.Parse((Math.Round(_myCpl.Height * 100)).ToString()) - int.Parse((Math.Round(_myCpl.MarginBottom * 100 + 100)).ToString()) - int.Parse((Math.Round(_myCpl.MarginTop * 100)).ToString()) - 30;

            var enc = new ASCIIEncoding();
            RichEditDocumentServer server = new RichEditDocumentServer();
            DocumentRange[] ranges = null;
            DocumentPosition dp = null;
            List<MyRtfObject> collection = null;
            DocumentRange tmpRange2 = null;
            switch (_strTipRep)
            {
                case "R":

                    reporte.txtTexto.CanGrow = true;
                    reporte.txtTexto.Html = _myPre.HtmlFormat;
                    reporte.txtFecha.Text = cFuncionesFechas.GetDateLiteral((DateTime)_myPre.PrescriptionDate);
                    printControl1.PrintingSystem = reporte.PrintingSystem;
                    printControl1.UpdatePageView();
                    reporte.CreateDocument();
                    reporte.txtFecha.Text = cFuncionesFechas.GetDateLiteral((DateTime)_myPre.PrescriptionDate);
                    reporte.txtFecha.Visible = true;
                    printControl1.PrintingSystem = reporte.PrintingSystem;
                    reporte.CreateDocument();
                    break;

                case "C":
                    reporte.txtTexto.CanGrow = true;
                    reporte.txtTexto.Html = _myPci.HtmlFormat;
                    reporte.txtFecha.Text = cFuncionesFechas.GetDateLiteral((DateTime)_myPci.SurgeryDate);
                    reporte.txtFecha.Visible = false;
                    printControl1.PrintingSystem = reporte.PrintingSystem;
                    printControl1.UpdatePageView();
                    reporte.CreateDocument();

                    break;

                case "I":

                    reporte.txtTexto.CanGrow = true;
                    reporte.txtTexto.CanShrink = true;
                    reporte.txtTexto.KeepTogether = true;
                    reporte.txtTexto.AnchorHorizontal = HorizontalAnchorStyles.Both;
                    reporte.txtTexto.AnchorVertical = VerticalAnchorStyles.Both;
                    reporte.txtTexto.Html = _myPin.HtmlFormat;
                    reporte.txtFecha.Text = cFuncionesFechas.GetDateLiteral((DateTime)_myPin.ReportDate);
                    reporte.txtFecha.Visible = true;
                    printControl1.PrintingSystem = reporte.PrintingSystem;
                    reporte.CreateDocument();
                    break;
            }
        }

        private void rptViewerPac_Load(object sender, EventArgs e)
        {
            switch (_strTipRep)
            {
                case "A":
                    this.CargarReporteAusencias();
                    break;

                case "M":
                    this.CargarReporteCertificadoMedico();
                    break;

                case "R":
                    this.CargarReporteVistaPreviaSimple();
                    break;

                case "C":
                    this.CargarReporteVistaPrevia();
                    break;

                case "I":
                    this.CargarReporteVistaPrevia();
                    break;

                case "F":
                    this.CargarReportePacientesFrecuentes();
                    break;
            }
        }

        #endregion Methods
    }
}