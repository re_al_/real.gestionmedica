﻿


namespace ReAl.GestionMedica.App.PAC
{
    partial class FPacHistoriaClinica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPacHistoriaClinica));
            this.xtraTabPage7 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraScrollableControl6 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.groupControl20 = new DevExpress.XtraEditors.GroupControl();
            this.txth7tratamientophc = new DevExpress.XtraEditors.MemoEdit();
            this.groupControl19 = new DevExpress.XtraEditors.GroupControl();
            this.txth7conductaphc = new DevExpress.XtraEditors.MemoEdit();
            this.groupControl18 = new DevExpress.XtraEditors.GroupControl();
            this.txth7diagnosticosphc = new DevExpress.XtraEditors.MemoEdit();
            this.groupControl17 = new DevExpress.XtraEditors.GroupControl();
            this.txth7resumenphc = new DevExpress.XtraEditors.MemoEdit();
            this.groupControl16 = new DevExpress.XtraEditors.GroupControl();
            this.txth7colvertebralphc = new DevExpress.XtraEditors.MemoEdit();
            this.groupControl15 = new DevExpress.XtraEditors.GroupControl();
            this.txth7nerviosperifphc = new DevExpress.XtraEditors.MemoEdit();
            this.groupControl14 = new DevExpress.XtraEditors.GroupControl();
            this.txth7pulcarotideosphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh7pulcarotideosphc = new System.Windows.Forms.Label();
            this.lblh7cuellophc = new System.Windows.Forms.Label();
            this.lblh7cabezaphc = new System.Windows.Forms.Label();
            this.txth7cuellophc = new DevExpress.XtraEditors.TextEdit();
            this.lblh7pultemporalesphc = new System.Windows.Forms.Label();
            this.txth7cabezaphc = new DevExpress.XtraEditors.TextEdit();
            this.txth7pultemporalesphc = new DevExpress.XtraEditors.TextEdit();
            this.groupControl13 = new DevExpress.XtraEditors.GroupControl();
            this.txth7rigideznucaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh7hiperestesiaphc = new System.Windows.Forms.Label();
            this.lblh7sigbrudzinskiphc = new System.Windows.Forms.Label();
            this.txth7hiperestesiaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh7fotofobiaphc = new System.Windows.Forms.Label();
            this.txth7sigbrudzinskiphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh7sigkerningphc = new System.Windows.Forms.Label();
            this.txth7fotofobiaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh7rigideznucaphc = new System.Windows.Forms.Label();
            this.txth7sigkerningphc = new DevExpress.XtraEditors.TextEdit();
            this.xtraTabPage6 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraScrollableControl5 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.groupControl12 = new DevExpress.XtraEditors.GroupControl();
            this.lblh6dolorprofphc = new DevExpress.XtraEditors.LabelControl();
            this.label4 = new System.Windows.Forms.Label();
            this.txth6localizacionphc = new DevExpress.XtraEditors.MemoEdit();
            this.txth6grafestesiaphc = new DevExpress.XtraEditors.MemoEdit();
            this.txth6estereognosiaphc = new DevExpress.XtraEditors.MemoEdit();
            this.txth6extincionphc = new DevExpress.XtraEditors.MemoEdit();
            this.txth6dospuntosphc = new DevExpress.XtraEditors.MemoEdit();
            this.txth6vibracionphc = new DevExpress.XtraEditors.MemoEdit();
            this.txth6posicionphc = new DevExpress.XtraEditors.MemoEdit();
            this.txth6dolorprofphc = new DevExpress.XtraEditors.MemoEdit();
            this.btnRecargar = new DevExpress.XtraEditors.SimpleButton();
            this.txth6sistsensitivophc = new DevExpress.XtraEditors.TextEdit();
            this.picSilueta = new System.Windows.Forms.PictureBox();
            this.lblh6extincionphc = new System.Windows.Forms.Label();
            this.lblh6estereognosiaphc = new System.Windows.Forms.Label();
            this.lblh6dospuntosphc = new System.Windows.Forms.Label();
            this.lblh6grafestesiaphc = new System.Windows.Forms.Label();
            this.lblh6vibracionphc = new System.Windows.Forms.Label();
            this.lblh6localizacionphc = new System.Windows.Forms.Label();
            this.lblh6posicionphc = new System.Windows.Forms.Label();
            this.groupControl11 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox33 = new System.Windows.Forms.GroupBox();
            this.txth6otrmaxilarphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh6otrmaxilarphc = new System.Windows.Forms.Label();
            this.txth6otrglabelarphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh6otrglabelarphc = new System.Windows.Forms.Label();
            this.groupBox32 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txth6supbicderphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh6supbicderphc = new System.Windows.Forms.Label();
            this.txth6supbicizqphc = new DevExpress.XtraEditors.TextEdit();
            this.txth6suptriderphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh6suptriderphc = new System.Windows.Forms.Label();
            this.txth6suptriizqphc = new DevExpress.XtraEditors.TextEdit();
            this.txth6supestderphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh6supestderphc = new System.Windows.Forms.Label();
            this.txth6supestizqphc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox31 = new System.Windows.Forms.GroupBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txth6patpladerphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh6patpladerphc = new System.Windows.Forms.Label();
            this.txth6patplaizqphc = new DevExpress.XtraEditors.TextEdit();
            this.txth6pathofderphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh6pathofderphc = new System.Windows.Forms.Label();
            this.txth6pathofizqphc = new DevExpress.XtraEditors.TextEdit();
            this.txth6pattroderphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh6pattroderphc = new System.Windows.Forms.Label();
            this.txth6pattroizqphc = new DevExpress.XtraEditors.TextEdit();
            this.txth6patprederphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh6patprederphc = new System.Windows.Forms.Label();
            this.lblh6patpmenderphc = new System.Windows.Forms.Label();
            this.txth6patpmenizqphc = new DevExpress.XtraEditors.TextEdit();
            this.txth6patpreizqphc = new DevExpress.XtraEditors.TextEdit();
            this.txth6patpmenderphc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox30 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txth6miotbicderphc = new DevExpress.XtraEditors.TextEdit();
            this.txth6miotbicizqphc = new DevExpress.XtraEditors.TextEdit();
            this.txth6miottriderphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh6miotbicderphc = new System.Windows.Forms.Label();
            this.txth6miottriizqphc = new DevExpress.XtraEditors.TextEdit();
            this.txth6miotestderphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh6miottriderphc = new System.Windows.Forms.Label();
            this.txth6miotestizqphc = new DevExpress.XtraEditors.TextEdit();
            this.txth6miotpatderphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh6miotestderphc = new System.Windows.Forms.Label();
            this.txth6miotpatizqphc = new DevExpress.XtraEditors.TextEdit();
            this.txth6miotaquderphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh6miotaquderphc = new System.Windows.Forms.Label();
            this.lblh6miotpatderphc = new System.Windows.Forms.Label();
            this.txth6miotaquizqphc = new DevExpress.XtraEditors.TextEdit();
            this.txth6reflejos01phc = new DevExpress.XtraEditors.TextEdit();
            this.txth6reflejos02phc = new DevExpress.XtraEditors.TextEdit();
            this.txth6reflejos03phc = new DevExpress.XtraEditors.TextEdit();
            this.txth6reflejos04phc = new DevExpress.XtraEditors.TextEdit();
            this.txth6reflejos05phc = new DevExpress.XtraEditors.TextEdit();
            this.txth6reflejos06phc = new DevExpress.XtraEditors.TextEdit();
            this.txth6reflejos07phc = new DevExpress.XtraEditors.TextEdit();
            this.txth6reflejos08phc = new DevExpress.XtraEditors.TextEdit();
            this.txth6reflejos09phc = new DevExpress.XtraEditors.TextEdit();
            this.txth6reflejos10phc = new DevExpress.XtraEditors.TextEdit();
            this.picManiqui = new System.Windows.Forms.PictureBox();
            this.xtraTabPage5 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraScrollableControl4 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.groupControl10 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox29 = new System.Windows.Forms.GroupBox();
            this.txth5movinvoluntariosphc = new DevExpress.XtraEditors.MemoEdit();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.txth5fuemuscularphc = new DevExpress.XtraEditors.MemoEdit();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txth5habilidadespecifphc = new DevExpress.XtraEditors.MemoEdit();
            this.txth5rebotephc = new DevExpress.XtraEditors.MemoEdit();
            this.txth5movrapidphc = new DevExpress.XtraEditors.MemoEdit();
            this.lblh5rebotephc = new System.Windows.Forms.Label();
            this.txth5talrodizqphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5talrodizqphc = new System.Windows.Forms.Label();
            this.txth5talrodderphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5talrodderphc = new System.Windows.Forms.Label();
            this.txth5deddedizqphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5deddedizqphc = new System.Windows.Forms.Label();
            this.txth5deddedderphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5deddedderphc = new System.Windows.Forms.Label();
            this.txth5dednarizqphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5dednarizqphc = new System.Windows.Forms.Label();
            this.txth5dednarderphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5dednarderphc = new System.Windows.Forms.Label();
            this.txth5rombergphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5rombergphc = new System.Windows.Forms.Label();
            this.txth5equilibratoriaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5equilibratoriaphc = new System.Windows.Forms.Label();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.txth5fasciculacionesphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5fasciculacionesphc = new System.Windows.Forms.Label();
            this.txth5volumenphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5volumenphc = new System.Windows.Forms.Label();
            this.txth5tonophc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5tonophc = new System.Windows.Forms.Label();
            this.txth5marchaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5marchaphc = new System.Windows.Forms.Label();
            this.groupControl9 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.lblh5desviacionphc = new System.Windows.Forms.Label();
            this.txth5desviacionphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5atrofiaphc = new System.Windows.Forms.Label();
            this.txth5atrofiaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5fasciculacionphc = new System.Windows.Forms.Label();
            this.txth5fasciculacionphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5fuerzaphc = new System.Windows.Forms.Label();
            this.txth5fuerzaphc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.txth5esternocleidomastoideophc = new DevExpress.XtraEditors.TextEdit();
            this.txth5trapeciophc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5trapeciophc = new System.Windows.Forms.Label();
            this.lblh5esternocleidomastoideophc = new System.Windows.Forms.Label();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.txth5elevpaladarphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5elevpaladarphc = new System.Windows.Forms.Label();
            this.txth5refnauceosophc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5refnauceosophc = new System.Windows.Forms.Label();
            this.txth5uvulaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5uvulaphc = new System.Windows.Forms.Label();
            this.txth5deglucionphc = new DevExpress.XtraEditors.TextEdit();
            this.txth5tonovozphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5tonovozphc = new System.Windows.Forms.Label();
            this.lblh5deglucionphc = new System.Windows.Forms.Label();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.txth5otoscopiaphc = new DevExpress.XtraEditors.TextEdit();
            this.txth5aguaudiderphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5otoscopiaphc = new System.Windows.Forms.Label();
            this.lblh5aguaudiderphc = new System.Windows.Forms.Label();
            this.txth5aguaudiizqphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5aguaudiizqphc = new System.Windows.Forms.Label();
            this.txth5weberphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5weberphc = new System.Windows.Forms.Label();
            this.txth5rinnephc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5rinnephc = new System.Windows.Forms.Label();
            this.txth5pruebaslabphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh5pruebaslabphc = new System.Windows.Forms.Label();
            this.xtraTabPage3 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraScrollableControl2 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.xtraScrollableControl8 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.groupControl7 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.txth3aprdesatencionphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3aprdesatencionphc = new System.Windows.Forms.Label();
            this.lblh3anosognosiaphc = new System.Windows.Forms.Label();
            this.txth3anosognosiaphc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.lblh3aprideacionalphc = new System.Windows.Forms.Label();
            this.txth3aprideacionalphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3aprideomotoraphc = new System.Windows.Forms.Label();
            this.txth3aprideomotoraphc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.lblh3agnauditivaphc = new System.Windows.Forms.Label();
            this.txth3agnauditivaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3agnvisualphc = new System.Windows.Forms.Label();
            this.txth3agnvisualphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3agntactilphc = new System.Windows.Forms.Label();
            this.txth3agntactilphc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.txth3lpdagrafiaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3lpdacalculiaphc = new System.Windows.Forms.Label();
            this.txth3lpdagnosiaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3lpdagnosiaphc = new System.Windows.Forms.Label();
            this.txth3lpdacalculiaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3lpdagrafiaphc = new System.Windows.Forms.Label();
            this.txth3lpddesorientacionphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3lpddesorientacionphc = new System.Windows.Forms.Label();
            this.groupControl6 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.lblh3lesagrafiaphc = new System.Windows.Forms.Label();
            this.txth3lesagrafiaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3lesalexiaphc = new System.Windows.Forms.Label();
            this.txth3lesalexiaphc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.groupBox38 = new System.Windows.Forms.GroupBox();
            this.lblh3cgeexpresionphc = new System.Windows.Forms.Label();
            this.txth3cgeexpresionphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3cgenominacionphc = new System.Windows.Forms.Label();
            this.txth3cgenominacionphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3cgerepeticionphc = new System.Windows.Forms.Label();
            this.txth3cgerepeticionphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3cgecomprensionphc = new System.Windows.Forms.Label();
            this.txth3cgecomprensionphc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox37 = new System.Windows.Forms.GroupBox();
            this.lblh3aprmotoraphc = new System.Windows.Forms.Label();
            this.txth3aprmotoraphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3aprsensorialphc = new System.Windows.Forms.Label();
            this.txth3aprsensorialphc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox36 = new System.Windows.Forms.GroupBox();
            this.txth3atacomprensionphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3ataexpresionphc = new System.Windows.Forms.Label();
            this.txth3ataexpresionphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3atacomprensionphc = new System.Windows.Forms.Label();
            this.lblh3atamixtaphc = new System.Windows.Forms.Label();
            this.txth3atamixtaphc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox35 = new System.Windows.Forms.GroupBox();
            this.txth3atrmotoraphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3atrmotoraphc = new System.Windows.Forms.Label();
            this.lblh3atrsensitivaphc = new System.Windows.Forms.Label();
            this.txth3atrsensitivaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3atrmixtaphc = new System.Windows.Forms.Label();
            this.txth3atrmixtaphc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox34 = new System.Windows.Forms.GroupBox();
            this.txth3apebrocaphc = new DevExpress.XtraEditors.TextEdit();
            this.txth3apeconduccionphc = new DevExpress.XtraEditors.TextEdit();
            this.txth3apewernickephc = new DevExpress.XtraEditors.TextEdit();
            this.txth3apeglobalphc = new DevExpress.XtraEditors.TextEdit();
            this.txth3apeanomiaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3apebrocaphc = new System.Windows.Forms.Label();
            this.lblh3apeconduccionphc = new System.Windows.Forms.Label();
            this.lblh3apewernickephc = new System.Windows.Forms.Label();
            this.lblh3apeglobalphc = new System.Windows.Forms.Label();
            this.lblh3apeanomiaphc = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.lblh3hdoizquierdophc = new System.Windows.Forms.Label();
            this.txth3hdoizquierdophc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3hdoderechophc = new System.Windows.Forms.Label();
            this.txth3hdoderechophc = new DevExpress.XtraEditors.TextEdit();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lblh3conrecientephc = new System.Windows.Forms.Label();
            this.txth3conrecientephc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3conremotaphc = new System.Windows.Forms.Label();
            this.txth3conremotaphc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txth3afeplanophc = new DevExpress.XtraEditors.TextEdit();
            this.txth3afeeuforicophc = new DevExpress.XtraEditors.TextEdit();
            this.txth3afeinadecuadophc = new DevExpress.XtraEditors.TextEdit();
            this.txth3afehostilphc = new DevExpress.XtraEditors.TextEdit();
            this.txth3afelabilphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3afeplanophc = new System.Windows.Forms.Label();
            this.txth3afedeprimidophc = new DevExpress.XtraEditors.TextEdit();
            this.lblh3afeeuforicophc = new System.Windows.Forms.Label();
            this.lblh3afedeprimidophc = new System.Windows.Forms.Label();
            this.lblh3afelabilphc = new System.Windows.Forms.Label();
            this.lblh3afehostilphc = new System.Windows.Forms.Label();
            this.lblh3afeinadecuadophc = new System.Windows.Forms.Label();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraScrollableControl1 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupControl22 = new DevExpress.XtraEditors.GroupControl();
            this.txth2paedelusionesphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh2paeilsuionesphc = new System.Windows.Forms.Label();
            this.lblh2paedelusionesphc = new System.Windows.Forms.Label();
            this.txth2paeilsuionesphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh2paealucinacionesphc = new System.Windows.Forms.Label();
            this.txth2paealucinacionesphc = new DevExpress.XtraEditors.TextEdit();
            this.groupControl21 = new DevExpress.XtraEditors.GroupControl();
            this.txth2pagconfusionphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh2pagconfusionphc = new System.Windows.Forms.Label();
            this.lblh2pagdeliriophc = new System.Windows.Forms.Label();
            this.txth2pagdeliriophc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txth2atgdesatentophc = new DevExpress.XtraEditors.TextEdit();
            this.lblh2atgdesatentophc = new System.Windows.Forms.Label();
            this.txth2atgnegligentephc = new DevExpress.XtraEditors.TextEdit();
            this.lblh2atgnegligentephc = new System.Windows.Forms.Label();
            this.txth2atgnegativophc = new DevExpress.XtraEditors.TextEdit();
            this.lblh2atgnegativophc = new System.Windows.Forms.Label();
            this.txth2atgfluctuantephc = new DevExpress.XtraEditors.TextEdit();
            this.lblh2atgfluctuantephc = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txth2conocimientosgenerales = new DevExpress.XtraEditors.MemoEdit();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txth2razideaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh2razideaphc = new System.Windows.Forms.Label();
            this.txth2razjuiciophc = new DevExpress.XtraEditors.TextEdit();
            this.lblh2razjuiciophc = new System.Windows.Forms.Label();
            this.lblh2razabstraccionphc = new System.Windows.Forms.Label();
            this.txth2razabstraccionphc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txth2orilugarphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh2orilugarphc = new System.Windows.Forms.Label();
            this.txth2oriespaciophc = new DevExpress.XtraEditors.TextEdit();
            this.lblh2oriespaciophc = new System.Windows.Forms.Label();
            this.txth2oripersonaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh2oripersonaphc = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblh2conconfusionphc = new System.Windows.Forms.Label();
            this.txth2conalertaphc = new DevExpress.XtraEditors.TextEdit();
            this.txth2concomaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh2concomaphc = new System.Windows.Forms.Label();
            this.txth2conestuforphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh2conestuforphc = new System.Windows.Forms.Label();
            this.lblh2conalertaphc = new System.Windows.Forms.Label();
            this.txth2consomnolenciaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh2consomnolenciaphc = new System.Windows.Forms.Label();
            this.txth2conconfusionphc = new DevExpress.XtraEditors.TextEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.txth2extremidadesphc = new DevExpress.XtraEditors.MemoEdit();
            this.txth2genitourinariophc = new DevExpress.XtraEditors.MemoEdit();
            this.txth2abdomenphc = new DevExpress.XtraEditors.MemoEdit();
            this.txth2toraxphc = new DevExpress.XtraEditors.MemoEdit();
            this.txth2cuellophc = new DevExpress.XtraEditors.MemoEdit();
            this.txth2cabezaphc = new DevExpress.XtraEditors.MemoEdit();
            this.lblh2cabezaphc = new System.Windows.Forms.Label();
            this.lblh2extremidadesphc = new System.Windows.Forms.Label();
            this.lblh2cuellophc = new System.Windows.Forms.Label();
            this.lblh2genitourinariophc = new System.Windows.Forms.Label();
            this.lblh2toraxphc = new System.Windows.Forms.Label();
            this.lblh2abdomenphc = new System.Windows.Forms.Label();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraScrollableControl7 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.txth1examenfisicophc = new DevExpress.XtraEditors.MemoEdit();
            this.txth1revisionphc = new DevExpress.XtraEditors.MemoEdit();
            this.txth1otrosphc = new DevExpress.XtraEditors.MemoEdit();
            this.txth1Cirugiasphc = new DevExpress.XtraEditors.TextEdit();
            this.txth1pcphc = new DevExpress.XtraEditors.TextEdit();
            this.labelPC = new System.Windows.Forms.Label();
            this.txth1pesophc = new DevExpress.XtraEditors.TextEdit();
            this.labelPESO = new System.Windows.Forms.Label();
            this.labelCirugias = new System.Windows.Forms.Label();
            this.txth1tallaphc = new DevExpress.XtraEditors.TextEdit();
            this.labelTALLA = new System.Windows.Forms.Label();
            this.labelInfecciones = new System.Windows.Forms.Label();
            this.txth1tempphc = new DevExpress.XtraEditors.TextEdit();
            this.txth1infeccionesphc = new DevExpress.XtraEditors.TextEdit();
            this.labelTEMP = new System.Windows.Forms.Label();
            this.labelTraumatismos = new System.Windows.Forms.Label();
            this.txth1frphc = new DevExpress.XtraEditors.TextEdit();
            this.txth1traumatismosphc = new DevExpress.XtraEditors.TextEdit();
            this.labelFR = new System.Windows.Forms.Label();
            this.labelCardiovascular = new System.Windows.Forms.Label();
            this.txth1fcphc = new DevExpress.XtraEditors.TextEdit();
            this.txth1cardiovascularphc = new DevExpress.XtraEditors.TextEdit();
            this.labelFC = new System.Windows.Forms.Label();
            this.labelMetabolico = new System.Windows.Forms.Label();
            this.txth1taphc = new DevExpress.XtraEditors.TextEdit();
            this.txth1metabolicophc = new DevExpress.XtraEditors.TextEdit();
            this.labelTA = new System.Windows.Forms.Label();
            this.labelToxico = new System.Windows.Forms.Label();
            this.txth1toxicophc = new DevExpress.XtraEditors.TextEdit();
            this.labelExamenFisicoGeneral = new System.Windows.Forms.Label();
            this.labelFamiliar = new System.Windows.Forms.Label();
            this.txth1familiarphc = new DevExpress.XtraEditors.TextEdit();
            this.labelRevisionPorSistemas = new System.Windows.Forms.Label();
            this.labelGinecoObstetrico = new System.Windows.Forms.Label();
            this.txth1fupphc = new DevExpress.XtraEditors.TextEdit();
            this.txth1ginecoobsphc = new DevExpress.XtraEditors.TextEdit();
            this.labelFUP = new System.Windows.Forms.Label();
            this.labelOtros = new System.Windows.Forms.Label();
            this.txth1fumphc = new DevExpress.XtraEditors.TextEdit();
            this.labelFUM = new System.Windows.Forms.Label();
            this.labelEmbarazos = new System.Windows.Forms.Label();
            this.txth1mesntruacionesphc = new DevExpress.XtraEditors.TextEdit();
            this.nudh1embarazosphc = new DevExpress.XtraEditors.TextEdit();
            this.labelMesntruaciones = new System.Windows.Forms.Label();
            this.labelGesta = new System.Windows.Forms.Label();
            this.nudh1abortosphc = new DevExpress.XtraEditors.TextEdit();
            this.nudh1gestaphc = new DevExpress.XtraEditors.TextEdit();
            this.labelAbortos = new System.Windows.Forms.Label();
            this.txth1enfermedadphc = new DevExpress.XtraEditors.MemoEdit();
            this.txth1motivophc = new DevExpress.XtraEditors.MemoEdit();
            this.labelMotivoDeConsulta = new System.Windows.Forms.Label();
            this.labelEnfermedadActual = new System.Windows.Forms.Label();
            this.tabHistoriaClinica = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage4 = new DevExpress.XtraTab.XtraTabPage();
            this.xtraScrollableControl3 = new DevExpress.XtraEditors.XtraScrollableControl();
            this.groupControl8 = new DevExpress.XtraEditors.GroupControl();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.txth4facsimetriaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4facsimetriaphc = new System.Windows.Forms.Label();
            this.txth4facmovimientosphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4facmovimientosphc = new System.Windows.Forms.Label();
            this.txth4faccentralphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4facparalisisphc = new System.Windows.Forms.Label();
            this.lblh4faccentralphc = new System.Windows.Forms.Label();
            this.txth4facparalisisphc = new DevExpress.XtraEditors.TextEdit();
            this.txth4facperifericaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4facgustophc = new System.Windows.Forms.Label();
            this.lblh4facperifericaphc = new System.Windows.Forms.Label();
            this.txth4facgustophc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.lblh4movmasticatoriosphc = new DevExpress.XtraEditors.LabelControl();
            this.txth4senfacializqphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4senfacialderphc = new System.Windows.Forms.Label();
            this.txth4senfacialderphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4senfacializqphc = new System.Windows.Forms.Label();
            this.lblh4refmentonianophc = new System.Windows.Forms.Label();
            this.txth4movmasticatoriosphc = new DevExpress.XtraEditors.MemoEdit();
            this.txth4reflejoizqphc = new DevExpress.XtraEditors.TextEdit();
            this.txth4refmentonianophc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4reflejoizqphc = new System.Windows.Forms.Label();
            this.txth4reflejoderphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4reflejoderphc = new System.Windows.Forms.Label();
            this.txth4aberturabocaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4aberturabocaphc = new System.Windows.Forms.Label();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.txth4nistagmuxphc = new DevExpress.XtraEditors.TextEdit();
            this.txth4posicionojosphc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox39 = new System.Windows.Forms.GroupBox();
            this.txth4acomodacionodphc = new DevExpress.XtraEditors.TextEdit();
            this.txth4acomodacionoiphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4acomodacionoiphc = new System.Windows.Forms.Label();
            this.lblh4acomodacionodphc = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.txth4diplopia1phc = new DevExpress.XtraEditors.TextEdit();
            this.txth4diplopia9phc = new DevExpress.XtraEditors.TextEdit();
            this.txth4diplopia8phc = new DevExpress.XtraEditors.TextEdit();
            this.txth4diplopia7phc = new DevExpress.XtraEditors.TextEdit();
            this.txth4diplopia6phc = new DevExpress.XtraEditors.TextEdit();
            this.txth4diplopia5phc = new DevExpress.XtraEditors.TextEdit();
            this.txth4diplopia4phc = new DevExpress.XtraEditors.TextEdit();
            this.txth4diplopia3phc = new DevExpress.XtraEditors.TextEdit();
            this.txth4diplopia2phc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4posicionojosphc = new System.Windows.Forms.Label();
            this.lblh4exoftalmiaphc = new System.Windows.Forms.Label();
            this.lblh4hornerphc = new System.Windows.Forms.Label();
            this.txth4hornerphc = new DevExpress.XtraEditors.TextEdit();
            this.txth4exoftalmiaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4movocularesphc = new System.Windows.Forms.Label();
            this.txth4movocularesphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4nistagmuxphc = new System.Windows.Forms.Label();
            this.lblh4prosisphc = new System.Windows.Forms.Label();
            this.txth4prosisphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4convergenciaphc = new System.Windows.Forms.Label();
            this.txth4convergenciaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4pupulasodphc = new System.Windows.Forms.Label();
            this.txth4pupulasodphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4pupulasoiphc = new System.Windows.Forms.Label();
            this.txth4pupulasoiphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4formaphc = new System.Windows.Forms.Label();
            this.txth4formaphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4fotomotorodphc = new System.Windows.Forms.Label();
            this.txth4fotomotorodphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4fotomotoroiphc = new System.Windows.Forms.Label();
            this.txth4fotomotoroiphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4consensualdaiphc = new System.Windows.Forms.Label();
            this.txth4consensualdaiphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4consensualiadphc = new System.Windows.Forms.Label();
            this.txth4consensualiadphc = new DevExpress.XtraEditors.TextEdit();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.txth4optccodphc = new DevExpress.XtraEditors.TextEdit();
            this.txth4optccoiphc = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txth4campimetriaphc = new DevExpress.XtraEditors.MemoEdit();
            this.lblh4optscodphc = new System.Windows.Forms.Label();
            this.txth4optscodphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4optscoiphc = new System.Windows.Forms.Label();
            this.txth4optscoiphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4optccodphc = new System.Windows.Forms.Label();
            this.lblh4optccoiphc = new System.Windows.Forms.Label();
            this.lblh4fondoodphc = new System.Windows.Forms.Label();
            this.txth4fondoodphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4fondooiphc = new System.Windows.Forms.Label();
            this.txth4fondooiphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4campimetriaphc = new System.Windows.Forms.Label();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.lblh4olfnormalphc = new System.Windows.Forms.Label();
            this.txth4olfnormalphc = new DevExpress.XtraEditors.TextEdit();
            this.lblh4olfanosmiaphc = new System.Windows.Forms.Label();
            this.txth4olfanosmiaphc = new DevExpress.XtraEditors.TextEdit();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.btnAuditoría = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabPage7.SuspendLayout();
            this.xtraScrollableControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl20)).BeginInit();
            this.groupControl20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth7tratamientophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl19)).BeginInit();
            this.groupControl19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth7conductaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl18)).BeginInit();
            this.groupControl18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth7diagnosticosphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl17)).BeginInit();
            this.groupControl17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth7resumenphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl16)).BeginInit();
            this.groupControl16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth7colvertebralphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl15)).BeginInit();
            this.groupControl15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth7nerviosperifphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl14)).BeginInit();
            this.groupControl14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth7pulcarotideosphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth7cuellophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth7cabezaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth7pultemporalesphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl13)).BeginInit();
            this.groupControl13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth7rigideznucaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth7hiperestesiaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth7sigbrudzinskiphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth7fotofobiaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth7sigkerningphc.Properties)).BeginInit();
            this.xtraTabPage6.SuspendLayout();
            this.xtraScrollableControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl12)).BeginInit();
            this.groupControl12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth6localizacionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6grafestesiaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6estereognosiaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6extincionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6dospuntosphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6vibracionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6posicionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6dolorprofphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6sistsensitivophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSilueta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).BeginInit();
            this.groupControl11.SuspendLayout();
            this.groupBox33.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth6otrmaxilarphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6otrglabelarphc.Properties)).BeginInit();
            this.groupBox32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth6supbicderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6supbicizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6suptriderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6suptriizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6supestderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6supestizqphc.Properties)).BeginInit();
            this.groupBox31.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth6patpladerphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6patplaizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6pathofderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6pathofizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6pattroderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6pattroizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6patprederphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6patpmenizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6patpreizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6patpmenderphc.Properties)).BeginInit();
            this.groupBox30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotbicderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotbicizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miottriderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miottriizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotestderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotestizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotpatderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotpatizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotaquderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotaquizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos01phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos02phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos03phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos04phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos05phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos06phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos07phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos08phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos09phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos10phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManiqui)).BeginInit();
            this.xtraTabPage5.SuspendLayout();
            this.xtraScrollableControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).BeginInit();
            this.groupControl10.SuspendLayout();
            this.groupBox29.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth5movinvoluntariosphc.Properties)).BeginInit();
            this.groupBox28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth5fuemuscularphc.Properties)).BeginInit();
            this.groupBox27.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth5habilidadespecifphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5rebotephc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5movrapidphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5talrodizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5talrodderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5deddedizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5deddedderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5dednarizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5dednarderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5rombergphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5equilibratoriaphc.Properties)).BeginInit();
            this.groupBox26.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth5fasciculacionesphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5volumenphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5tonophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5marchaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).BeginInit();
            this.groupControl9.SuspendLayout();
            this.groupBox25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth5desviacionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5atrofiaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5fasciculacionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5fuerzaphc.Properties)).BeginInit();
            this.groupBox24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth5esternocleidomastoideophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5trapeciophc.Properties)).BeginInit();
            this.groupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth5elevpaladarphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5refnauceosophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5uvulaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5deglucionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5tonovozphc.Properties)).BeginInit();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth5otoscopiaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5aguaudiderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5aguaudiizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5weberphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5rinnephc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5pruebaslabphc.Properties)).BeginInit();
            this.xtraTabPage3.SuspendLayout();
            this.xtraScrollableControl2.SuspendLayout();
            this.xtraScrollableControl8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).BeginInit();
            this.groupControl7.SuspendLayout();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3aprdesatencionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3anosognosiaphc.Properties)).BeginInit();
            this.groupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3aprideacionalphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3aprideomotoraphc.Properties)).BeginInit();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3agnauditivaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3agnvisualphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3agntactilphc.Properties)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3lpdagrafiaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3lpdagnosiaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3lpdacalculiaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3lpddesorientacionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).BeginInit();
            this.groupControl6.SuspendLayout();
            this.groupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3lesagrafiaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3lesalexiaphc.Properties)).BeginInit();
            this.groupBox10.SuspendLayout();
            this.groupBox38.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3cgeexpresionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3cgenominacionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3cgerepeticionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3cgecomprensionphc.Properties)).BeginInit();
            this.groupBox37.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3aprmotoraphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3aprsensorialphc.Properties)).BeginInit();
            this.groupBox36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3atacomprensionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3ataexpresionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3atamixtaphc.Properties)).BeginInit();
            this.groupBox35.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3atrmotoraphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3atrsensitivaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3atrmixtaphc.Properties)).BeginInit();
            this.groupBox34.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3apebrocaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3apeconduccionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3apewernickephc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3apeglobalphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3apeanomiaphc.Properties)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3hdoizquierdophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3hdoderechophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3conrecientephc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3conremotaphc.Properties)).BeginInit();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3afeplanophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3afeeuforicophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3afeinadecuadophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3afehostilphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3afelabilphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3afedeprimidophc.Properties)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            this.xtraScrollableControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl22)).BeginInit();
            this.groupControl22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth2paedelusionesphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2paeilsuionesphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2paealucinacionesphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl21)).BeginInit();
            this.groupControl21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth2pagconfusionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2pagdeliriophc.Properties)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth2atgdesatentophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2atgnegligentephc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2atgnegativophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2atgfluctuantephc.Properties)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth2conocimientosgenerales.Properties)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth2razideaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2razjuiciophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2razabstraccionphc.Properties)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth2orilugarphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2oriespaciophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2oripersonaphc.Properties)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth2conalertaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2concomaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2conestuforphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2consomnolenciaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2conconfusionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth2extremidadesphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2genitourinariophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2abdomenphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2toraxphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2cuellophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2cabezaphc.Properties)).BeginInit();
            this.xtraTabPage1.SuspendLayout();
            this.xtraScrollableControl7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth1examenfisicophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1revisionphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1otrosphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1Cirugiasphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1pcphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1pesophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1tallaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1tempphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1infeccionesphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1frphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1traumatismosphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1fcphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1cardiovascularphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1taphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1metabolicophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1toxicophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1familiarphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1fupphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1ginecoobsphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1fumphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1mesntruacionesphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudh1embarazosphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudh1abortosphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudh1gestaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1enfermedadphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1motivophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabHistoriaClinica)).BeginInit();
            this.tabHistoriaClinica.SuspendLayout();
            this.xtraTabPage4.SuspendLayout();
            this.xtraScrollableControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).BeginInit();
            this.groupControl8.SuspendLayout();
            this.groupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth4facsimetriaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4facmovimientosphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4faccentralphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4facparalisisphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4facperifericaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4facgustophc.Properties)).BeginInit();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth4senfacializqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4senfacialderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4movmasticatoriosphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4reflejoizqphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4refmentonianophc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4reflejoderphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4aberturabocaphc.Properties)).BeginInit();
            this.groupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth4nistagmuxphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4posicionojosphc.Properties)).BeginInit();
            this.groupBox39.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth4acomodacionodphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4acomodacionoiphc.Properties)).BeginInit();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia1phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia9phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia8phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia7phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia6phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia5phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia4phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia3phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia2phc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4hornerphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4exoftalmiaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4movocularesphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4prosisphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4convergenciaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4pupulasodphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4pupulasoiphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4formaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4fotomotorodphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4fotomotoroiphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4consensualdaiphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4consensualiadphc.Properties)).BeginInit();
            this.groupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth4optccodphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4optccoiphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4campimetriaphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4optscodphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4optscoiphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4fondoodphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4fondooiphc.Properties)).BeginInit();
            this.groupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth4olfnormalphc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4olfanosmiaphc.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabPage7
            // 
            this.xtraTabPage7.Controls.Add(this.xtraScrollableControl6);
            this.xtraTabPage7.Name = "xtraTabPage7";
            this.xtraTabPage7.Size = new System.Drawing.Size(982, 594);
            this.xtraTabPage7.Text = "Hoja 7";
            // 
            // xtraScrollableControl6
            // 
            this.xtraScrollableControl6.Controls.Add(this.groupControl20);
            this.xtraScrollableControl6.Controls.Add(this.groupControl19);
            this.xtraScrollableControl6.Controls.Add(this.groupControl18);
            this.xtraScrollableControl6.Controls.Add(this.groupControl17);
            this.xtraScrollableControl6.Controls.Add(this.groupControl16);
            this.xtraScrollableControl6.Controls.Add(this.groupControl15);
            this.xtraScrollableControl6.Controls.Add(this.groupControl14);
            this.xtraScrollableControl6.Controls.Add(this.groupControl13);
            this.xtraScrollableControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl6.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl6.Name = "xtraScrollableControl6";
            this.xtraScrollableControl6.Size = new System.Drawing.Size(982, 594);
            this.xtraScrollableControl6.TabIndex = 0;
            // 
            // groupControl20
            // 
            this.groupControl20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl20.Controls.Add(this.txth7tratamientophc);
            this.groupControl20.Location = new System.Drawing.Point(3, 530);
            this.groupControl20.Name = "groupControl20";
            this.groupControl20.Size = new System.Drawing.Size(959, 60);
            this.groupControl20.TabIndex = 7;
            this.groupControl20.Text = "TRATAMIENTO";
            // 
            // txth7tratamientophc
            // 
            this.txth7tratamientophc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth7tratamientophc.Location = new System.Drawing.Point(5, 22);
            this.txth7tratamientophc.Name = "txth7tratamientophc";
            this.txth7tratamientophc.Size = new System.Drawing.Size(949, 35);
            this.txth7tratamientophc.TabIndex = 0;
            // 
            // groupControl19
            // 
            this.groupControl19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl19.Controls.Add(this.txth7conductaphc);
            this.groupControl19.Location = new System.Drawing.Point(3, 469);
            this.groupControl19.Name = "groupControl19";
            this.groupControl19.Size = new System.Drawing.Size(959, 60);
            this.groupControl19.TabIndex = 6;
            this.groupControl19.Text = "CONDUCTA";
            // 
            // txth7conductaphc
            // 
            this.txth7conductaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth7conductaphc.Location = new System.Drawing.Point(5, 22);
            this.txth7conductaphc.Name = "txth7conductaphc";
            this.txth7conductaphc.Size = new System.Drawing.Size(949, 35);
            this.txth7conductaphc.TabIndex = 0;
            // 
            // groupControl18
            // 
            this.groupControl18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl18.Controls.Add(this.txth7diagnosticosphc);
            this.groupControl18.Location = new System.Drawing.Point(3, 406);
            this.groupControl18.Name = "groupControl18";
            this.groupControl18.Size = new System.Drawing.Size(959, 60);
            this.groupControl18.TabIndex = 5;
            this.groupControl18.Text = "DIAGNOSTICOS";
            // 
            // txth7diagnosticosphc
            // 
            this.txth7diagnosticosphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth7diagnosticosphc.Location = new System.Drawing.Point(5, 22);
            this.txth7diagnosticosphc.Name = "txth7diagnosticosphc";
            this.txth7diagnosticosphc.Size = new System.Drawing.Size(949, 35);
            this.txth7diagnosticosphc.TabIndex = 0;
            // 
            // groupControl17
            // 
            this.groupControl17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl17.Controls.Add(this.txth7resumenphc);
            this.groupControl17.Location = new System.Drawing.Point(3, 343);
            this.groupControl17.Name = "groupControl17";
            this.groupControl17.Size = new System.Drawing.Size(959, 60);
            this.groupControl17.TabIndex = 4;
            this.groupControl17.Text = "RESUMEN";
            // 
            // txth7resumenphc
            // 
            this.txth7resumenphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth7resumenphc.Location = new System.Drawing.Point(5, 22);
            this.txth7resumenphc.Name = "txth7resumenphc";
            this.txth7resumenphc.Size = new System.Drawing.Size(949, 35);
            this.txth7resumenphc.TabIndex = 0;
            // 
            // groupControl16
            // 
            this.groupControl16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl16.Controls.Add(this.txth7colvertebralphc);
            this.groupControl16.Location = new System.Drawing.Point(3, 280);
            this.groupControl16.Name = "groupControl16";
            this.groupControl16.Size = new System.Drawing.Size(959, 60);
            this.groupControl16.TabIndex = 3;
            this.groupControl16.Text = "COLUMNA VERTEBRAL";
            // 
            // txth7colvertebralphc
            // 
            this.txth7colvertebralphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth7colvertebralphc.Location = new System.Drawing.Point(5, 22);
            this.txth7colvertebralphc.Name = "txth7colvertebralphc";
            this.txth7colvertebralphc.Size = new System.Drawing.Size(949, 35);
            this.txth7colvertebralphc.TabIndex = 0;
            // 
            // groupControl15
            // 
            this.groupControl15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl15.Controls.Add(this.txth7nerviosperifphc);
            this.groupControl15.Location = new System.Drawing.Point(3, 217);
            this.groupControl15.Name = "groupControl15";
            this.groupControl15.Size = new System.Drawing.Size(959, 60);
            this.groupControl15.TabIndex = 2;
            this.groupControl15.Text = "EXAMEN DE NERVIOS PERIFERICOS";
            // 
            // txth7nerviosperifphc
            // 
            this.txth7nerviosperifphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth7nerviosperifphc.Location = new System.Drawing.Point(5, 22);
            this.txth7nerviosperifphc.Name = "txth7nerviosperifphc";
            this.txth7nerviosperifphc.Size = new System.Drawing.Size(949, 35);
            this.txth7nerviosperifphc.TabIndex = 0;
            // 
            // groupControl14
            // 
            this.groupControl14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl14.Controls.Add(this.txth7pulcarotideosphc);
            this.groupControl14.Controls.Add(this.lblh7pulcarotideosphc);
            this.groupControl14.Controls.Add(this.lblh7cuellophc);
            this.groupControl14.Controls.Add(this.lblh7cabezaphc);
            this.groupControl14.Controls.Add(this.txth7cuellophc);
            this.groupControl14.Controls.Add(this.lblh7pultemporalesphc);
            this.groupControl14.Controls.Add(this.txth7cabezaphc);
            this.groupControl14.Controls.Add(this.txth7pultemporalesphc);
            this.groupControl14.Location = new System.Drawing.Point(3, 99);
            this.groupControl14.Name = "groupControl14";
            this.groupControl14.Size = new System.Drawing.Size(959, 115);
            this.groupControl14.TabIndex = 1;
            this.groupControl14.Text = "EXAMEN VASCULAR";
            // 
            // txth7pulcarotideosphc
            // 
            this.txth7pulcarotideosphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth7pulcarotideosphc.Location = new System.Drawing.Point(179, 25);
            this.txth7pulcarotideosphc.Name = "txth7pulcarotideosphc";
            this.txth7pulcarotideosphc.Properties.MaxLength = 1000;
            this.txth7pulcarotideosphc.Size = new System.Drawing.Size(775, 20);
            this.txth7pulcarotideosphc.TabIndex = 1;
            // 
            // lblh7pulcarotideosphc
            // 
            this.lblh7pulcarotideosphc.AutoSize = true;
            this.lblh7pulcarotideosphc.Location = new System.Drawing.Point(14, 28);
            this.lblh7pulcarotideosphc.Name = "lblh7pulcarotideosphc";
            this.lblh7pulcarotideosphc.Size = new System.Drawing.Size(159, 13);
            this.lblh7pulcarotideosphc.TabIndex = 0;
            this.lblh7pulcarotideosphc.Text = "Palpación de Pulsos Carotídeos:";
            // 
            // lblh7cuellophc
            // 
            this.lblh7cuellophc.AutoSize = true;
            this.lblh7cuellophc.Location = new System.Drawing.Point(14, 94);
            this.lblh7cuellophc.Name = "lblh7cuellophc";
            this.lblh7cuellophc.Size = new System.Drawing.Size(118, 13);
            this.lblh7cuellophc.TabIndex = 6;
            this.lblh7cuellophc.Text = "Auscultación de Cuello:";
            // 
            // lblh7cabezaphc
            // 
            this.lblh7cabezaphc.AutoSize = true;
            this.lblh7cabezaphc.Location = new System.Drawing.Point(14, 72);
            this.lblh7cabezaphc.Name = "lblh7cabezaphc";
            this.lblh7cabezaphc.Size = new System.Drawing.Size(125, 13);
            this.lblh7cabezaphc.TabIndex = 4;
            this.lblh7cabezaphc.Text = "Auscultación de Cabeza:";
            // 
            // txth7cuellophc
            // 
            this.txth7cuellophc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth7cuellophc.Location = new System.Drawing.Point(145, 91);
            this.txth7cuellophc.Name = "txth7cuellophc";
            this.txth7cuellophc.Properties.MaxLength = 1000;
            this.txth7cuellophc.Size = new System.Drawing.Size(809, 20);
            this.txth7cuellophc.TabIndex = 7;
            // 
            // lblh7pultemporalesphc
            // 
            this.lblh7pultemporalesphc.AutoSize = true;
            this.lblh7pultemporalesphc.Location = new System.Drawing.Point(14, 50);
            this.lblh7pultemporalesphc.Name = "lblh7pultemporalesphc";
            this.lblh7pultemporalesphc.Size = new System.Drawing.Size(225, 13);
            this.lblh7pultemporalesphc.TabIndex = 2;
            this.lblh7pultemporalesphc.Text = "Palpación de Pulsos Temporales Superficiales:";
            // 
            // txth7cabezaphc
            // 
            this.txth7cabezaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth7cabezaphc.Location = new System.Drawing.Point(145, 69);
            this.txth7cabezaphc.Name = "txth7cabezaphc";
            this.txth7cabezaphc.Properties.MaxLength = 1000;
            this.txth7cabezaphc.Size = new System.Drawing.Size(809, 20);
            this.txth7cabezaphc.TabIndex = 5;
            // 
            // txth7pultemporalesphc
            // 
            this.txth7pultemporalesphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth7pultemporalesphc.Location = new System.Drawing.Point(245, 47);
            this.txth7pultemporalesphc.Name = "txth7pultemporalesphc";
            this.txth7pultemporalesphc.Properties.MaxLength = 1000;
            this.txth7pultemporalesphc.Size = new System.Drawing.Size(709, 20);
            this.txth7pultemporalesphc.TabIndex = 3;
            // 
            // groupControl13
            // 
            this.groupControl13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl13.Controls.Add(this.txth7rigideznucaphc);
            this.groupControl13.Controls.Add(this.lblh7hiperestesiaphc);
            this.groupControl13.Controls.Add(this.lblh7sigbrudzinskiphc);
            this.groupControl13.Controls.Add(this.txth7hiperestesiaphc);
            this.groupControl13.Controls.Add(this.lblh7fotofobiaphc);
            this.groupControl13.Controls.Add(this.txth7sigbrudzinskiphc);
            this.groupControl13.Controls.Add(this.lblh7sigkerningphc);
            this.groupControl13.Controls.Add(this.txth7fotofobiaphc);
            this.groupControl13.Controls.Add(this.lblh7rigideznucaphc);
            this.groupControl13.Controls.Add(this.txth7sigkerningphc);
            this.groupControl13.Location = new System.Drawing.Point(3, 3);
            this.groupControl13.Name = "groupControl13";
            this.groupControl13.Size = new System.Drawing.Size(959, 93);
            this.groupControl13.TabIndex = 0;
            this.groupControl13.Text = "SIGNOS MENINGEOS";
            // 
            // txth7rigideznucaphc
            // 
            this.txth7rigideznucaphc.Location = new System.Drawing.Point(104, 25);
            this.txth7rigideznucaphc.Name = "txth7rigideznucaphc";
            this.txth7rigideznucaphc.Properties.MaxLength = 1000;
            this.txth7rigideznucaphc.Size = new System.Drawing.Size(383, 20);
            this.txth7rigideznucaphc.TabIndex = 1;
            // 
            // lblh7hiperestesiaphc
            // 
            this.lblh7hiperestesiaphc.AutoSize = true;
            this.lblh7hiperestesiaphc.Location = new System.Drawing.Point(14, 72);
            this.lblh7hiperestesiaphc.Name = "lblh7hiperestesiaphc";
            this.lblh7hiperestesiaphc.Size = new System.Drawing.Size(165, 13);
            this.lblh7hiperestesiaphc.TabIndex = 8;
            this.lblh7hiperestesiaphc.Text = "Hiperestesia de Globos Oculares:";
            // 
            // lblh7sigbrudzinskiphc
            // 
            this.lblh7sigbrudzinskiphc.AutoSize = true;
            this.lblh7sigbrudzinskiphc.Location = new System.Drawing.Point(496, 50);
            this.lblh7sigbrudzinskiphc.Name = "lblh7sigbrudzinskiphc";
            this.lblh7sigbrudzinskiphc.Size = new System.Drawing.Size(102, 13);
            this.lblh7sigbrudzinskiphc.TabIndex = 6;
            this.lblh7sigbrudzinskiphc.Text = "Signo de Brudzinski:";
            // 
            // txth7hiperestesiaphc
            // 
            this.txth7hiperestesiaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth7hiperestesiaphc.Location = new System.Drawing.Point(179, 69);
            this.txth7hiperestesiaphc.Name = "txth7hiperestesiaphc";
            this.txth7hiperestesiaphc.Properties.MaxLength = 1000;
            this.txth7hiperestesiaphc.Size = new System.Drawing.Size(775, 20);
            this.txth7hiperestesiaphc.TabIndex = 9;
            // 
            // lblh7fotofobiaphc
            // 
            this.lblh7fotofobiaphc.AutoSize = true;
            this.lblh7fotofobiaphc.Location = new System.Drawing.Point(14, 50);
            this.lblh7fotofobiaphc.Name = "lblh7fotofobiaphc";
            this.lblh7fotofobiaphc.Size = new System.Drawing.Size(57, 13);
            this.lblh7fotofobiaphc.TabIndex = 4;
            this.lblh7fotofobiaphc.Text = "Fotofobia:";
            // 
            // txth7sigbrudzinskiphc
            // 
            this.txth7sigbrudzinskiphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth7sigbrudzinskiphc.Location = new System.Drawing.Point(598, 47);
            this.txth7sigbrudzinskiphc.Name = "txth7sigbrudzinskiphc";
            this.txth7sigbrudzinskiphc.Properties.MaxLength = 1000;
            this.txth7sigbrudzinskiphc.Size = new System.Drawing.Size(356, 20);
            this.txth7sigbrudzinskiphc.TabIndex = 7;
            // 
            // lblh7sigkerningphc
            // 
            this.lblh7sigkerningphc.AutoSize = true;
            this.lblh7sigkerningphc.Location = new System.Drawing.Point(496, 28);
            this.lblh7sigkerningphc.Name = "lblh7sigkerningphc";
            this.lblh7sigkerningphc.Size = new System.Drawing.Size(91, 13);
            this.lblh7sigkerningphc.TabIndex = 2;
            this.lblh7sigkerningphc.Text = "Signo de Kerning:";
            // 
            // txth7fotofobiaphc
            // 
            this.txth7fotofobiaphc.Location = new System.Drawing.Point(104, 47);
            this.txth7fotofobiaphc.Name = "txth7fotofobiaphc";
            this.txth7fotofobiaphc.Properties.MaxLength = 1000;
            this.txth7fotofobiaphc.Size = new System.Drawing.Size(383, 20);
            this.txth7fotofobiaphc.TabIndex = 5;
            // 
            // lblh7rigideznucaphc
            // 
            this.lblh7rigideznucaphc.AutoSize = true;
            this.lblh7rigideznucaphc.Location = new System.Drawing.Point(14, 28);
            this.lblh7rigideznucaphc.Name = "lblh7rigideznucaphc";
            this.lblh7rigideznucaphc.Size = new System.Drawing.Size(87, 13);
            this.lblh7rigideznucaphc.TabIndex = 0;
            this.lblh7rigideznucaphc.Text = "Rigidez de Nuca:";
            // 
            // txth7sigkerningphc
            // 
            this.txth7sigkerningphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth7sigkerningphc.Location = new System.Drawing.Point(598, 25);
            this.txth7sigkerningphc.Name = "txth7sigkerningphc";
            this.txth7sigkerningphc.Properties.MaxLength = 1000;
            this.txth7sigkerningphc.Size = new System.Drawing.Size(356, 20);
            this.txth7sigkerningphc.TabIndex = 3;
            // 
            // xtraTabPage6
            // 
            this.xtraTabPage6.Controls.Add(this.xtraScrollableControl5);
            this.xtraTabPage6.Name = "xtraTabPage6";
            this.xtraTabPage6.Size = new System.Drawing.Size(982, 594);
            this.xtraTabPage6.Text = "Hoja 6";
            // 
            // xtraScrollableControl5
            // 
            this.xtraScrollableControl5.Controls.Add(this.groupControl12);
            this.xtraScrollableControl5.Controls.Add(this.groupControl11);
            this.xtraScrollableControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl5.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl5.Name = "xtraScrollableControl5";
            this.xtraScrollableControl5.Size = new System.Drawing.Size(982, 594);
            this.xtraScrollableControl5.TabIndex = 0;
            // 
            // groupControl12
            // 
            this.groupControl12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl12.Controls.Add(this.lblh6dolorprofphc);
            this.groupControl12.Controls.Add(this.label4);
            this.groupControl12.Controls.Add(this.txth6localizacionphc);
            this.groupControl12.Controls.Add(this.txth6grafestesiaphc);
            this.groupControl12.Controls.Add(this.txth6estereognosiaphc);
            this.groupControl12.Controls.Add(this.txth6extincionphc);
            this.groupControl12.Controls.Add(this.txth6dospuntosphc);
            this.groupControl12.Controls.Add(this.txth6vibracionphc);
            this.groupControl12.Controls.Add(this.txth6posicionphc);
            this.groupControl12.Controls.Add(this.txth6dolorprofphc);
            this.groupControl12.Controls.Add(this.btnRecargar);
            this.groupControl12.Controls.Add(this.txth6sistsensitivophc);
            this.groupControl12.Controls.Add(this.picSilueta);
            this.groupControl12.Controls.Add(this.lblh6extincionphc);
            this.groupControl12.Controls.Add(this.lblh6estereognosiaphc);
            this.groupControl12.Controls.Add(this.lblh6dospuntosphc);
            this.groupControl12.Controls.Add(this.lblh6grafestesiaphc);
            this.groupControl12.Controls.Add(this.lblh6vibracionphc);
            this.groupControl12.Controls.Add(this.lblh6localizacionphc);
            this.groupControl12.Controls.Add(this.lblh6posicionphc);
            this.groupControl12.Location = new System.Drawing.Point(3, 198);
            this.groupControl12.Name = "groupControl12";
            this.groupControl12.Size = new System.Drawing.Size(958, 392);
            this.groupControl12.TabIndex = 1;
            this.groupControl12.Text = "SISTEMA SENSITIVO";
            // 
            // lblh6dolorprofphc
            // 
            this.lblh6dolorprofphc.Location = new System.Drawing.Point(341, 64);
            this.lblh6dolorprofphc.Name = "lblh6dolorprofphc";
            this.lblh6dolorprofphc.Size = new System.Drawing.Size(88, 26);
            this.lblh6dolorprofphc.TabIndex = 2;
            this.lblh6dolorprofphc.Text = "Dolor Profundo:\r\n(Aquiles-Testículo)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(338, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(304, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Use los siguientes símbolos:  Dolor = D, Tacto = T, Temp = TP";
            // 
            // txth6localizacionphc
            // 
            this.txth6localizacionphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth6localizacionphc.Location = new System.Drawing.Point(430, 348);
            this.txth6localizacionphc.Name = "txth6localizacionphc";
            this.txth6localizacionphc.Size = new System.Drawing.Size(520, 38);
            this.txth6localizacionphc.TabIndex = 18;
            // 
            // txth6grafestesiaphc
            // 
            this.txth6grafestesiaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth6grafestesiaphc.Location = new System.Drawing.Point(430, 307);
            this.txth6grafestesiaphc.Name = "txth6grafestesiaphc";
            this.txth6grafestesiaphc.Size = new System.Drawing.Size(520, 38);
            this.txth6grafestesiaphc.TabIndex = 16;
            // 
            // txth6estereognosiaphc
            // 
            this.txth6estereognosiaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth6estereognosiaphc.Location = new System.Drawing.Point(430, 266);
            this.txth6estereognosiaphc.Name = "txth6estereognosiaphc";
            this.txth6estereognosiaphc.Size = new System.Drawing.Size(520, 38);
            this.txth6estereognosiaphc.TabIndex = 13;
            // 
            // txth6extincionphc
            // 
            this.txth6extincionphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth6extincionphc.Location = new System.Drawing.Point(430, 225);
            this.txth6extincionphc.Name = "txth6extincionphc";
            this.txth6extincionphc.Size = new System.Drawing.Size(520, 38);
            this.txth6extincionphc.TabIndex = 11;
            // 
            // txth6dospuntosphc
            // 
            this.txth6dospuntosphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth6dospuntosphc.Location = new System.Drawing.Point(430, 184);
            this.txth6dospuntosphc.Name = "txth6dospuntosphc";
            this.txth6dospuntosphc.Size = new System.Drawing.Size(520, 38);
            this.txth6dospuntosphc.TabIndex = 9;
            // 
            // txth6vibracionphc
            // 
            this.txth6vibracionphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth6vibracionphc.Location = new System.Drawing.Point(430, 143);
            this.txth6vibracionphc.Name = "txth6vibracionphc";
            this.txth6vibracionphc.Size = new System.Drawing.Size(520, 38);
            this.txth6vibracionphc.TabIndex = 7;
            // 
            // txth6posicionphc
            // 
            this.txth6posicionphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth6posicionphc.Location = new System.Drawing.Point(430, 102);
            this.txth6posicionphc.Name = "txth6posicionphc";
            this.txth6posicionphc.Size = new System.Drawing.Size(520, 38);
            this.txth6posicionphc.TabIndex = 5;
            // 
            // txth6dolorprofphc
            // 
            this.txth6dolorprofphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth6dolorprofphc.Location = new System.Drawing.Point(430, 61);
            this.txth6dolorprofphc.Name = "txth6dolorprofphc";
            this.txth6dolorprofphc.Size = new System.Drawing.Size(520, 38);
            this.txth6dolorprofphc.TabIndex = 3;
            // 
            // btnRecargar
            // 
            this.btnRecargar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnRecargar.Location = new System.Drawing.Point(117, 274);
            this.btnRecargar.Name = "btnRecargar";
            this.btnRecargar.Size = new System.Drawing.Size(108, 34);
            this.btnRecargar.TabIndex = 14;
            this.btnRecargar.Text = "&Recargar Imagen";
            this.btnRecargar.Click += new System.EventHandler(this.btnRecargar_Click);
            // 
            // txth6sistsensitivophc
            // 
            this.txth6sistsensitivophc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth6sistsensitivophc.Location = new System.Drawing.Point(8, 24);
            this.txth6sistsensitivophc.Name = "txth6sistsensitivophc";
            this.txth6sistsensitivophc.Properties.MaxLength = 2000;
            this.txth6sistsensitivophc.Size = new System.Drawing.Size(942, 20);
            this.txth6sistsensitivophc.TabIndex = 0;
            // 
            // picSilueta
            // 
            this.picSilueta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picSilueta.Image = global::ReAl.GestionMedica.App.Properties.Resources.Silueta;
            this.picSilueta.InitialImage = global::ReAl.GestionMedica.App.Properties.Resources.Silueta;
            this.picSilueta.Location = new System.Drawing.Point(8, 50);
            this.picSilueta.Name = "picSilueta";
            this.picSilueta.Size = new System.Drawing.Size(309, 258);
            this.picSilueta.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picSilueta.TabIndex = 120;
            this.picSilueta.TabStop = false;
            this.picSilueta.MouseDown += new System.Windows.Forms.MouseEventHandler(this.picSilueta_MouseDown);
            this.picSilueta.MouseMove += new System.Windows.Forms.MouseEventHandler(this.picSilueta_MouseMove);
            this.picSilueta.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picSilueta_MouseUp);
            // 
            // lblh6extincionphc
            // 
            this.lblh6extincionphc.AutoSize = true;
            this.lblh6extincionphc.Location = new System.Drawing.Point(339, 228);
            this.lblh6extincionphc.Name = "lblh6extincionphc";
            this.lblh6extincionphc.Size = new System.Drawing.Size(54, 13);
            this.lblh6extincionphc.TabIndex = 10;
            this.lblh6extincionphc.Text = "Extinción:";
            // 
            // lblh6estereognosiaphc
            // 
            this.lblh6estereognosiaphc.AutoSize = true;
            this.lblh6estereognosiaphc.Location = new System.Drawing.Point(338, 269);
            this.lblh6estereognosiaphc.Name = "lblh6estereognosiaphc";
            this.lblh6estereognosiaphc.Size = new System.Drawing.Size(79, 13);
            this.lblh6estereognosiaphc.TabIndex = 12;
            this.lblh6estereognosiaphc.Text = "Estereognosia:";
            // 
            // lblh6dospuntosphc
            // 
            this.lblh6dospuntosphc.AutoSize = true;
            this.lblh6dospuntosphc.Location = new System.Drawing.Point(339, 187);
            this.lblh6dospuntosphc.Name = "lblh6dospuntosphc";
            this.lblh6dospuntosphc.Size = new System.Drawing.Size(65, 13);
            this.lblh6dospuntosphc.TabIndex = 8;
            this.lblh6dospuntosphc.Text = "Dos Puntos:";
            // 
            // lblh6grafestesiaphc
            // 
            this.lblh6grafestesiaphc.AutoSize = true;
            this.lblh6grafestesiaphc.Location = new System.Drawing.Point(338, 310);
            this.lblh6grafestesiaphc.Name = "lblh6grafestesiaphc";
            this.lblh6grafestesiaphc.Size = new System.Drawing.Size(66, 13);
            this.lblh6grafestesiaphc.TabIndex = 15;
            this.lblh6grafestesiaphc.Text = "Grafestesia:";
            // 
            // lblh6vibracionphc
            // 
            this.lblh6vibracionphc.AutoSize = true;
            this.lblh6vibracionphc.Location = new System.Drawing.Point(339, 146);
            this.lblh6vibracionphc.Name = "lblh6vibracionphc";
            this.lblh6vibracionphc.Size = new System.Drawing.Size(54, 13);
            this.lblh6vibracionphc.TabIndex = 6;
            this.lblh6vibracionphc.Text = "Vibración:";
            // 
            // lblh6localizacionphc
            // 
            this.lblh6localizacionphc.AutoSize = true;
            this.lblh6localizacionphc.Location = new System.Drawing.Point(338, 351);
            this.lblh6localizacionphc.Name = "lblh6localizacionphc";
            this.lblh6localizacionphc.Size = new System.Drawing.Size(67, 13);
            this.lblh6localizacionphc.TabIndex = 17;
            this.lblh6localizacionphc.Text = "Localización:";
            // 
            // lblh6posicionphc
            // 
            this.lblh6posicionphc.AutoSize = true;
            this.lblh6posicionphc.Location = new System.Drawing.Point(338, 109);
            this.lblh6posicionphc.Name = "lblh6posicionphc";
            this.lblh6posicionphc.Size = new System.Drawing.Size(49, 13);
            this.lblh6posicionphc.TabIndex = 4;
            this.lblh6posicionphc.Text = "Posición:";
            // 
            // groupControl11
            // 
            this.groupControl11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl11.Controls.Add(this.groupBox33);
            this.groupControl11.Controls.Add(this.groupBox32);
            this.groupControl11.Controls.Add(this.groupBox31);
            this.groupControl11.Controls.Add(this.groupBox30);
            this.groupControl11.Controls.Add(this.txth6reflejos01phc);
            this.groupControl11.Controls.Add(this.txth6reflejos02phc);
            this.groupControl11.Controls.Add(this.txth6reflejos03phc);
            this.groupControl11.Controls.Add(this.txth6reflejos04phc);
            this.groupControl11.Controls.Add(this.txth6reflejos05phc);
            this.groupControl11.Controls.Add(this.txth6reflejos06phc);
            this.groupControl11.Controls.Add(this.txth6reflejos07phc);
            this.groupControl11.Controls.Add(this.txth6reflejos08phc);
            this.groupControl11.Controls.Add(this.txth6reflejos09phc);
            this.groupControl11.Controls.Add(this.txth6reflejos10phc);
            this.groupControl11.Controls.Add(this.picManiqui);
            this.groupControl11.Location = new System.Drawing.Point(3, 4);
            this.groupControl11.Name = "groupControl11";
            this.groupControl11.Size = new System.Drawing.Size(958, 190);
            this.groupControl11.TabIndex = 0;
            this.groupControl11.Text = "REFLEJOS";
            // 
            // groupBox33
            // 
            this.groupBox33.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox33.Controls.Add(this.txth6otrmaxilarphc);
            this.groupBox33.Controls.Add(this.lblh6otrmaxilarphc);
            this.groupBox33.Controls.Add(this.txth6otrglabelarphc);
            this.groupBox33.Controls.Add(this.lblh6otrglabelarphc);
            this.groupBox33.Location = new System.Drawing.Point(727, 117);
            this.groupBox33.Name = "groupBox33";
            this.groupBox33.Size = new System.Drawing.Size(226, 67);
            this.groupBox33.TabIndex = 13;
            this.groupBox33.TabStop = false;
            this.groupBox33.Text = "OTROS";
            // 
            // txth6otrmaxilarphc
            // 
            this.txth6otrmaxilarphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth6otrmaxilarphc.Location = new System.Drawing.Point(57, 20);
            this.txth6otrmaxilarphc.Name = "txth6otrmaxilarphc";
            this.txth6otrmaxilarphc.Properties.MaxLength = 100;
            this.txth6otrmaxilarphc.Size = new System.Drawing.Size(163, 20);
            this.txth6otrmaxilarphc.TabIndex = 1;
            // 
            // lblh6otrmaxilarphc
            // 
            this.lblh6otrmaxilarphc.AutoSize = true;
            this.lblh6otrmaxilarphc.Location = new System.Drawing.Point(4, 23);
            this.lblh6otrmaxilarphc.Name = "lblh6otrmaxilarphc";
            this.lblh6otrmaxilarphc.Size = new System.Drawing.Size(45, 13);
            this.lblh6otrmaxilarphc.TabIndex = 0;
            this.lblh6otrmaxilarphc.Text = "Maxilar:";
            // 
            // txth6otrglabelarphc
            // 
            this.txth6otrglabelarphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth6otrglabelarphc.Location = new System.Drawing.Point(57, 42);
            this.txth6otrglabelarphc.Name = "txth6otrglabelarphc";
            this.txth6otrglabelarphc.Properties.MaxLength = 100;
            this.txth6otrglabelarphc.Size = new System.Drawing.Size(163, 20);
            this.txth6otrglabelarphc.TabIndex = 3;
            // 
            // lblh6otrglabelarphc
            // 
            this.lblh6otrglabelarphc.AutoSize = true;
            this.lblh6otrglabelarphc.Location = new System.Drawing.Point(4, 45);
            this.lblh6otrglabelarphc.Name = "lblh6otrglabelarphc";
            this.lblh6otrglabelarphc.Size = new System.Drawing.Size(50, 13);
            this.lblh6otrglabelarphc.TabIndex = 2;
            this.lblh6otrglabelarphc.Text = "Glabelar:";
            // 
            // groupBox32
            // 
            this.groupBox32.Controls.Add(this.label29);
            this.groupBox32.Controls.Add(this.label30);
            this.groupBox32.Controls.Add(this.label27);
            this.groupBox32.Controls.Add(this.label28);
            this.groupBox32.Controls.Add(this.label25);
            this.groupBox32.Controls.Add(this.label26);
            this.groupBox32.Controls.Add(this.txth6supbicderphc);
            this.groupBox32.Controls.Add(this.lblh6supbicderphc);
            this.groupBox32.Controls.Add(this.txth6supbicizqphc);
            this.groupBox32.Controls.Add(this.txth6suptriderphc);
            this.groupBox32.Controls.Add(this.lblh6suptriderphc);
            this.groupBox32.Controls.Add(this.txth6suptriizqphc);
            this.groupBox32.Controls.Add(this.txth6supestderphc);
            this.groupBox32.Controls.Add(this.lblh6supestderphc);
            this.groupBox32.Controls.Add(this.txth6supestizqphc);
            this.groupBox32.Location = new System.Drawing.Point(727, 25);
            this.groupBox32.Name = "groupBox32";
            this.groupBox32.Size = new System.Drawing.Size(240, 92);
            this.groupBox32.TabIndex = 4;
            this.groupBox32.TabStop = false;
            this.groupBox32.Text = "SUPERFICIALES";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(154, 69);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(11, 13);
            this.label29.TabIndex = 13;
            this.label29.Text = "I";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(57, 69);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(14, 13);
            this.label30.TabIndex = 11;
            this.label30.Text = "D";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(154, 46);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(11, 13);
            this.label27.TabIndex = 8;
            this.label27.Text = "I";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(57, 46);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(14, 13);
            this.label28.TabIndex = 6;
            this.label28.Text = "D";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(154, 23);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(11, 13);
            this.label25.TabIndex = 3;
            this.label25.Text = "I";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(57, 23);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(14, 13);
            this.label26.TabIndex = 1;
            this.label26.Text = "D";
            // 
            // txth6supbicderphc
            // 
            this.txth6supbicderphc.Location = new System.Drawing.Point(73, 20);
            this.txth6supbicderphc.Name = "txth6supbicderphc";
            this.txth6supbicderphc.Properties.MaxLength = 10;
            this.txth6supbicderphc.Size = new System.Drawing.Size(67, 20);
            this.txth6supbicderphc.TabIndex = 2;
            // 
            // lblh6supbicderphc
            // 
            this.lblh6supbicderphc.AutoSize = true;
            this.lblh6supbicderphc.Location = new System.Drawing.Point(6, 23);
            this.lblh6supbicderphc.Name = "lblh6supbicderphc";
            this.lblh6supbicderphc.Size = new System.Drawing.Size(41, 13);
            this.lblh6supbicderphc.TabIndex = 0;
            this.lblh6supbicderphc.Text = "Biceps:";
            // 
            // txth6supbicizqphc
            // 
            this.txth6supbicizqphc.Location = new System.Drawing.Point(167, 20);
            this.txth6supbicizqphc.Name = "txth6supbicizqphc";
            this.txth6supbicizqphc.Properties.MaxLength = 10;
            this.txth6supbicizqphc.Size = new System.Drawing.Size(67, 20);
            this.txth6supbicizqphc.TabIndex = 4;
            // 
            // txth6suptriderphc
            // 
            this.txth6suptriderphc.Location = new System.Drawing.Point(73, 43);
            this.txth6suptriderphc.Name = "txth6suptriderphc";
            this.txth6suptriderphc.Properties.MaxLength = 10;
            this.txth6suptriderphc.Size = new System.Drawing.Size(67, 20);
            this.txth6suptriderphc.TabIndex = 7;
            // 
            // lblh6suptriderphc
            // 
            this.lblh6suptriderphc.AutoSize = true;
            this.lblh6suptriderphc.Location = new System.Drawing.Point(6, 46);
            this.lblh6suptriderphc.Name = "lblh6suptriderphc";
            this.lblh6suptriderphc.Size = new System.Drawing.Size(45, 13);
            this.lblh6suptriderphc.TabIndex = 5;
            this.lblh6suptriderphc.Text = "Triceps:";
            // 
            // txth6suptriizqphc
            // 
            this.txth6suptriizqphc.Location = new System.Drawing.Point(167, 43);
            this.txth6suptriizqphc.Name = "txth6suptriizqphc";
            this.txth6suptriizqphc.Properties.MaxLength = 10;
            this.txth6suptriizqphc.Size = new System.Drawing.Size(67, 20);
            this.txth6suptriizqphc.TabIndex = 9;
            // 
            // txth6supestderphc
            // 
            this.txth6supestderphc.Location = new System.Drawing.Point(73, 66);
            this.txth6supestderphc.Name = "txth6supestderphc";
            this.txth6supestderphc.Properties.MaxLength = 10;
            this.txth6supestderphc.Size = new System.Drawing.Size(67, 20);
            this.txth6supestderphc.TabIndex = 12;
            // 
            // lblh6supestderphc
            // 
            this.lblh6supestderphc.AutoSize = true;
            this.lblh6supestderphc.Location = new System.Drawing.Point(6, 69);
            this.lblh6supestderphc.Name = "lblh6supestderphc";
            this.lblh6supestderphc.Size = new System.Drawing.Size(52, 13);
            this.lblh6supestderphc.TabIndex = 10;
            this.lblh6supestderphc.Text = "Estilorad:";
            // 
            // txth6supestizqphc
            // 
            this.txth6supestizqphc.Location = new System.Drawing.Point(167, 66);
            this.txth6supestizqphc.Name = "txth6supestizqphc";
            this.txth6supestizqphc.Properties.MaxLength = 10;
            this.txth6supestizqphc.Size = new System.Drawing.Size(67, 20);
            this.txth6supestizqphc.TabIndex = 14;
            // 
            // groupBox31
            // 
            this.groupBox31.Controls.Add(this.label23);
            this.groupBox31.Controls.Add(this.label24);
            this.groupBox31.Controls.Add(this.label21);
            this.groupBox31.Controls.Add(this.label22);
            this.groupBox31.Controls.Add(this.label19);
            this.groupBox31.Controls.Add(this.label20);
            this.groupBox31.Controls.Add(this.label17);
            this.groupBox31.Controls.Add(this.label18);
            this.groupBox31.Controls.Add(this.label15);
            this.groupBox31.Controls.Add(this.label16);
            this.groupBox31.Controls.Add(this.txth6patpladerphc);
            this.groupBox31.Controls.Add(this.lblh6patpladerphc);
            this.groupBox31.Controls.Add(this.txth6patplaizqphc);
            this.groupBox31.Controls.Add(this.txth6pathofderphc);
            this.groupBox31.Controls.Add(this.lblh6pathofderphc);
            this.groupBox31.Controls.Add(this.txth6pathofizqphc);
            this.groupBox31.Controls.Add(this.txth6pattroderphc);
            this.groupBox31.Controls.Add(this.lblh6pattroderphc);
            this.groupBox31.Controls.Add(this.txth6pattroizqphc);
            this.groupBox31.Controls.Add(this.txth6patprederphc);
            this.groupBox31.Controls.Add(this.lblh6patprederphc);
            this.groupBox31.Controls.Add(this.lblh6patpmenderphc);
            this.groupBox31.Controls.Add(this.txth6patpmenizqphc);
            this.groupBox31.Controls.Add(this.txth6patpreizqphc);
            this.groupBox31.Controls.Add(this.txth6patpmenderphc);
            this.groupBox31.Location = new System.Drawing.Point(481, 25);
            this.groupBox31.Name = "groupBox31";
            this.groupBox31.Size = new System.Drawing.Size(240, 159);
            this.groupBox31.TabIndex = 3;
            this.groupBox31.TabStop = false;
            this.groupBox31.Text = "PATOLÓGICOS";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(154, 137);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(11, 13);
            this.label23.TabIndex = 23;
            this.label23.Text = "I";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(57, 137);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(14, 13);
            this.label24.TabIndex = 21;
            this.label24.Text = "D";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(154, 111);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(11, 13);
            this.label21.TabIndex = 18;
            this.label21.Text = "I";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(57, 111);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(14, 13);
            this.label22.TabIndex = 16;
            this.label22.Text = "D";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(154, 85);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(11, 13);
            this.label19.TabIndex = 13;
            this.label19.Text = "I";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(57, 85);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(14, 13);
            this.label20.TabIndex = 11;
            this.label20.Text = "D";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(154, 59);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(11, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "I";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(57, 59);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(14, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "D";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(154, 33);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(11, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "I";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(57, 33);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(14, 13);
            this.label16.TabIndex = 1;
            this.label16.Text = "D";
            // 
            // txth6patpladerphc
            // 
            this.txth6patpladerphc.Location = new System.Drawing.Point(73, 30);
            this.txth6patpladerphc.Name = "txth6patpladerphc";
            this.txth6patpladerphc.Properties.MaxLength = 10;
            this.txth6patpladerphc.Size = new System.Drawing.Size(67, 20);
            this.txth6patpladerphc.TabIndex = 2;
            // 
            // lblh6patpladerphc
            // 
            this.lblh6patpladerphc.AutoSize = true;
            this.lblh6patpladerphc.Location = new System.Drawing.Point(6, 33);
            this.lblh6patpladerphc.Name = "lblh6patpladerphc";
            this.lblh6patpladerphc.Size = new System.Drawing.Size(45, 13);
            this.lblh6patpladerphc.TabIndex = 0;
            this.lblh6patpladerphc.Text = "Plantar:";
            // 
            // txth6patplaizqphc
            // 
            this.txth6patplaizqphc.Location = new System.Drawing.Point(167, 30);
            this.txth6patplaizqphc.Name = "txth6patplaizqphc";
            this.txth6patplaizqphc.Properties.MaxLength = 10;
            this.txth6patplaizqphc.Size = new System.Drawing.Size(67, 20);
            this.txth6patplaizqphc.TabIndex = 4;
            // 
            // txth6pathofderphc
            // 
            this.txth6pathofderphc.Location = new System.Drawing.Point(73, 56);
            this.txth6pathofderphc.Name = "txth6pathofderphc";
            this.txth6pathofderphc.Properties.MaxLength = 10;
            this.txth6pathofderphc.Size = new System.Drawing.Size(67, 20);
            this.txth6pathofderphc.TabIndex = 7;
            // 
            // lblh6pathofderphc
            // 
            this.lblh6pathofderphc.AutoSize = true;
            this.lblh6pathofderphc.Location = new System.Drawing.Point(6, 59);
            this.lblh6pathofderphc.Name = "lblh6pathofderphc";
            this.lblh6pathofderphc.Size = new System.Drawing.Size(52, 13);
            this.lblh6pathofderphc.TabIndex = 5;
            this.lblh6pathofderphc.Text = "Hoffman:";
            // 
            // txth6pathofizqphc
            // 
            this.txth6pathofizqphc.Location = new System.Drawing.Point(167, 56);
            this.txth6pathofizqphc.Name = "txth6pathofizqphc";
            this.txth6pathofizqphc.Properties.MaxLength = 10;
            this.txth6pathofizqphc.Size = new System.Drawing.Size(67, 20);
            this.txth6pathofizqphc.TabIndex = 9;
            // 
            // txth6pattroderphc
            // 
            this.txth6pattroderphc.Location = new System.Drawing.Point(73, 82);
            this.txth6pattroderphc.Name = "txth6pattroderphc";
            this.txth6pattroderphc.Properties.MaxLength = 10;
            this.txth6pattroderphc.Size = new System.Drawing.Size(67, 20);
            this.txth6pattroderphc.TabIndex = 12;
            // 
            // lblh6pattroderphc
            // 
            this.lblh6pattroderphc.AutoSize = true;
            this.lblh6pattroderphc.Location = new System.Drawing.Point(6, 85);
            this.lblh6pattroderphc.Name = "lblh6pattroderphc";
            this.lblh6pattroderphc.Size = new System.Drawing.Size(47, 13);
            this.lblh6pattroderphc.TabIndex = 10;
            this.lblh6pattroderphc.Text = "Trompa:";
            // 
            // txth6pattroizqphc
            // 
            this.txth6pattroizqphc.Location = new System.Drawing.Point(167, 82);
            this.txth6pattroizqphc.Name = "txth6pattroizqphc";
            this.txth6pattroizqphc.Properties.MaxLength = 10;
            this.txth6pattroizqphc.Size = new System.Drawing.Size(67, 20);
            this.txth6pattroizqphc.TabIndex = 14;
            // 
            // txth6patprederphc
            // 
            this.txth6patprederphc.Location = new System.Drawing.Point(73, 108);
            this.txth6patprederphc.Name = "txth6patprederphc";
            this.txth6patprederphc.Properties.MaxLength = 10;
            this.txth6patprederphc.Size = new System.Drawing.Size(67, 20);
            this.txth6patprederphc.TabIndex = 17;
            // 
            // lblh6patprederphc
            // 
            this.lblh6patprederphc.AutoSize = true;
            this.lblh6patprederphc.Location = new System.Drawing.Point(6, 111);
            this.lblh6patprederphc.Name = "lblh6patprederphc";
            this.lblh6patprederphc.Size = new System.Drawing.Size(52, 13);
            this.lblh6patprederphc.TabIndex = 15;
            this.lblh6patprederphc.Text = "Prensión:";
            // 
            // lblh6patpmenderphc
            // 
            this.lblh6patpmenderphc.AutoSize = true;
            this.lblh6patpmenderphc.Location = new System.Drawing.Point(6, 137);
            this.lblh6patpmenderphc.Name = "lblh6patpmenderphc";
            this.lblh6patpmenderphc.Size = new System.Drawing.Size(57, 13);
            this.lblh6patpmenderphc.TabIndex = 20;
            this.lblh6patpmenderphc.Text = "Palm-Men:";
            // 
            // txth6patpmenizqphc
            // 
            this.txth6patpmenizqphc.Location = new System.Drawing.Point(167, 134);
            this.txth6patpmenizqphc.Name = "txth6patpmenizqphc";
            this.txth6patpmenizqphc.Properties.MaxLength = 10;
            this.txth6patpmenizqphc.Size = new System.Drawing.Size(67, 20);
            this.txth6patpmenizqphc.TabIndex = 24;
            // 
            // txth6patpreizqphc
            // 
            this.txth6patpreizqphc.Location = new System.Drawing.Point(167, 108);
            this.txth6patpreizqphc.Name = "txth6patpreizqphc";
            this.txth6patpreizqphc.Properties.MaxLength = 10;
            this.txth6patpreizqphc.Size = new System.Drawing.Size(67, 20);
            this.txth6patpreizqphc.TabIndex = 19;
            // 
            // txth6patpmenderphc
            // 
            this.txth6patpmenderphc.Location = new System.Drawing.Point(73, 134);
            this.txth6patpmenderphc.Name = "txth6patpmenderphc";
            this.txth6patpmenderphc.Properties.MaxLength = 10;
            this.txth6patpmenderphc.Size = new System.Drawing.Size(67, 20);
            this.txth6patpmenderphc.TabIndex = 22;
            // 
            // groupBox30
            // 
            this.groupBox30.Controls.Add(this.label13);
            this.groupBox30.Controls.Add(this.label14);
            this.groupBox30.Controls.Add(this.label11);
            this.groupBox30.Controls.Add(this.label12);
            this.groupBox30.Controls.Add(this.label9);
            this.groupBox30.Controls.Add(this.label10);
            this.groupBox30.Controls.Add(this.label7);
            this.groupBox30.Controls.Add(this.label8);
            this.groupBox30.Controls.Add(this.label6);
            this.groupBox30.Controls.Add(this.label5);
            this.groupBox30.Controls.Add(this.txth6miotbicderphc);
            this.groupBox30.Controls.Add(this.txth6miotbicizqphc);
            this.groupBox30.Controls.Add(this.txth6miottriderphc);
            this.groupBox30.Controls.Add(this.lblh6miotbicderphc);
            this.groupBox30.Controls.Add(this.txth6miottriizqphc);
            this.groupBox30.Controls.Add(this.txth6miotestderphc);
            this.groupBox30.Controls.Add(this.lblh6miottriderphc);
            this.groupBox30.Controls.Add(this.txth6miotestizqphc);
            this.groupBox30.Controls.Add(this.txth6miotpatderphc);
            this.groupBox30.Controls.Add(this.lblh6miotestderphc);
            this.groupBox30.Controls.Add(this.txth6miotpatizqphc);
            this.groupBox30.Controls.Add(this.txth6miotaquderphc);
            this.groupBox30.Controls.Add(this.lblh6miotaquderphc);
            this.groupBox30.Controls.Add(this.lblh6miotpatderphc);
            this.groupBox30.Controls.Add(this.txth6miotaquizqphc);
            this.groupBox30.Location = new System.Drawing.Point(235, 25);
            this.groupBox30.Name = "groupBox30";
            this.groupBox30.Size = new System.Drawing.Size(240, 159);
            this.groupBox30.TabIndex = 2;
            this.groupBox30.TabStop = false;
            this.groupBox30.Text = "MIOTATICOS";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(154, 137);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(11, 13);
            this.label13.TabIndex = 23;
            this.label13.Text = "I";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(57, 137);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "D";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(154, 111);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(11, 13);
            this.label11.TabIndex = 18;
            this.label11.Text = "I";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(57, 111);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "D";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(154, 85);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(11, 13);
            this.label9.TabIndex = 13;
            this.label9.Text = "I";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(57, 85);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(14, 13);
            this.label10.TabIndex = 11;
            this.label10.Text = "D";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(154, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(11, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "I";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(57, 59);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "D";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(154, 33);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "I";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(57, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(14, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "D";
            // 
            // txth6miotbicderphc
            // 
            this.txth6miotbicderphc.Location = new System.Drawing.Point(73, 30);
            this.txth6miotbicderphc.Name = "txth6miotbicderphc";
            this.txth6miotbicderphc.Properties.MaxLength = 10;
            this.txth6miotbicderphc.Size = new System.Drawing.Size(67, 20);
            this.txth6miotbicderphc.TabIndex = 2;
            // 
            // txth6miotbicizqphc
            // 
            this.txth6miotbicizqphc.Location = new System.Drawing.Point(167, 30);
            this.txth6miotbicizqphc.Name = "txth6miotbicizqphc";
            this.txth6miotbicizqphc.Properties.MaxLength = 10;
            this.txth6miotbicizqphc.Size = new System.Drawing.Size(67, 20);
            this.txth6miotbicizqphc.TabIndex = 4;
            // 
            // txth6miottriderphc
            // 
            this.txth6miottriderphc.Location = new System.Drawing.Point(73, 56);
            this.txth6miottriderphc.Name = "txth6miottriderphc";
            this.txth6miottriderphc.Properties.MaxLength = 10;
            this.txth6miottriderphc.Size = new System.Drawing.Size(67, 20);
            this.txth6miottriderphc.TabIndex = 7;
            // 
            // lblh6miotbicderphc
            // 
            this.lblh6miotbicderphc.AutoSize = true;
            this.lblh6miotbicderphc.Location = new System.Drawing.Point(6, 33);
            this.lblh6miotbicderphc.Name = "lblh6miotbicderphc";
            this.lblh6miotbicderphc.Size = new System.Drawing.Size(41, 13);
            this.lblh6miotbicderphc.TabIndex = 0;
            this.lblh6miotbicderphc.Text = "Biceps:";
            // 
            // txth6miottriizqphc
            // 
            this.txth6miottriizqphc.Location = new System.Drawing.Point(167, 56);
            this.txth6miottriizqphc.Name = "txth6miottriizqphc";
            this.txth6miottriizqphc.Properties.MaxLength = 10;
            this.txth6miottriizqphc.Size = new System.Drawing.Size(67, 20);
            this.txth6miottriizqphc.TabIndex = 9;
            // 
            // txth6miotestderphc
            // 
            this.txth6miotestderphc.Location = new System.Drawing.Point(73, 82);
            this.txth6miotestderphc.Name = "txth6miotestderphc";
            this.txth6miotestderphc.Properties.MaxLength = 10;
            this.txth6miotestderphc.Size = new System.Drawing.Size(67, 20);
            this.txth6miotestderphc.TabIndex = 12;
            // 
            // lblh6miottriderphc
            // 
            this.lblh6miottriderphc.AutoSize = true;
            this.lblh6miottriderphc.Location = new System.Drawing.Point(6, 59);
            this.lblh6miottriderphc.Name = "lblh6miottriderphc";
            this.lblh6miottriderphc.Size = new System.Drawing.Size(45, 13);
            this.lblh6miottriderphc.TabIndex = 5;
            this.lblh6miottriderphc.Text = "Triceps:";
            // 
            // txth6miotestizqphc
            // 
            this.txth6miotestizqphc.Location = new System.Drawing.Point(167, 82);
            this.txth6miotestizqphc.Name = "txth6miotestizqphc";
            this.txth6miotestizqphc.Properties.MaxLength = 10;
            this.txth6miotestizqphc.Size = new System.Drawing.Size(67, 20);
            this.txth6miotestizqphc.TabIndex = 14;
            // 
            // txth6miotpatderphc
            // 
            this.txth6miotpatderphc.Location = new System.Drawing.Point(73, 108);
            this.txth6miotpatderphc.Name = "txth6miotpatderphc";
            this.txth6miotpatderphc.Properties.MaxLength = 10;
            this.txth6miotpatderphc.Size = new System.Drawing.Size(67, 20);
            this.txth6miotpatderphc.TabIndex = 17;
            // 
            // lblh6miotestderphc
            // 
            this.lblh6miotestderphc.AutoSize = true;
            this.lblh6miotestderphc.Location = new System.Drawing.Point(6, 85);
            this.lblh6miotestderphc.Name = "lblh6miotestderphc";
            this.lblh6miotestderphc.Size = new System.Drawing.Size(52, 13);
            this.lblh6miotestderphc.TabIndex = 10;
            this.lblh6miotestderphc.Text = "Estilorad:";
            // 
            // txth6miotpatizqphc
            // 
            this.txth6miotpatizqphc.Location = new System.Drawing.Point(167, 108);
            this.txth6miotpatizqphc.Name = "txth6miotpatizqphc";
            this.txth6miotpatizqphc.Properties.MaxLength = 10;
            this.txth6miotpatizqphc.Size = new System.Drawing.Size(67, 20);
            this.txth6miotpatizqphc.TabIndex = 19;
            // 
            // txth6miotaquderphc
            // 
            this.txth6miotaquderphc.Location = new System.Drawing.Point(73, 134);
            this.txth6miotaquderphc.Name = "txth6miotaquderphc";
            this.txth6miotaquderphc.Properties.MaxLength = 10;
            this.txth6miotaquderphc.Size = new System.Drawing.Size(67, 20);
            this.txth6miotaquderphc.TabIndex = 22;
            // 
            // lblh6miotaquderphc
            // 
            this.lblh6miotaquderphc.AutoSize = true;
            this.lblh6miotaquderphc.Location = new System.Drawing.Point(6, 137);
            this.lblh6miotaquderphc.Name = "lblh6miotaquderphc";
            this.lblh6miotaquderphc.Size = new System.Drawing.Size(54, 13);
            this.lblh6miotaquderphc.TabIndex = 20;
            this.lblh6miotaquderphc.Text = "Aquiliano:";
            // 
            // lblh6miotpatderphc
            // 
            this.lblh6miotpatderphc.AutoSize = true;
            this.lblh6miotpatderphc.Location = new System.Drawing.Point(6, 111);
            this.lblh6miotpatderphc.Name = "lblh6miotpatderphc";
            this.lblh6miotpatderphc.Size = new System.Drawing.Size(45, 13);
            this.lblh6miotpatderphc.TabIndex = 15;
            this.lblh6miotpatderphc.Text = "Patelar:";
            // 
            // txth6miotaquizqphc
            // 
            this.txth6miotaquizqphc.Location = new System.Drawing.Point(167, 134);
            this.txth6miotaquizqphc.Name = "txth6miotaquizqphc";
            this.txth6miotaquizqphc.Properties.MaxLength = 10;
            this.txth6miotaquizqphc.Size = new System.Drawing.Size(67, 20);
            this.txth6miotaquizqphc.TabIndex = 24;
            // 
            // txth6reflejos01phc
            // 
            this.txth6reflejos01phc.Location = new System.Drawing.Point(16, 52);
            this.txth6reflejos01phc.Name = "txth6reflejos01phc";
            this.txth6reflejos01phc.Properties.MaxLength = 10;
            this.txth6reflejos01phc.Size = new System.Drawing.Size(44, 20);
            this.txth6reflejos01phc.TabIndex = 0;
            // 
            // txth6reflejos02phc
            // 
            this.txth6reflejos02phc.Location = new System.Drawing.Point(178, 52);
            this.txth6reflejos02phc.Name = "txth6reflejos02phc";
            this.txth6reflejos02phc.Properties.MaxLength = 10;
            this.txth6reflejos02phc.Size = new System.Drawing.Size(44, 20);
            this.txth6reflejos02phc.TabIndex = 1;
            // 
            // txth6reflejos03phc
            // 
            this.txth6reflejos03phc.Location = new System.Drawing.Point(38, 98);
            this.txth6reflejos03phc.Name = "txth6reflejos03phc";
            this.txth6reflejos03phc.Properties.MaxLength = 10;
            this.txth6reflejos03phc.Size = new System.Drawing.Size(44, 20);
            this.txth6reflejos03phc.TabIndex = 5;
            // 
            // txth6reflejos04phc
            // 
            this.txth6reflejos04phc.Location = new System.Drawing.Point(153, 98);
            this.txth6reflejos04phc.Name = "txth6reflejos04phc";
            this.txth6reflejos04phc.Properties.MaxLength = 10;
            this.txth6reflejos04phc.Size = new System.Drawing.Size(44, 20);
            this.txth6reflejos04phc.TabIndex = 6;
            // 
            // txth6reflejos05phc
            // 
            this.txth6reflejos05phc.Location = new System.Drawing.Point(46, 130);
            this.txth6reflejos05phc.Name = "txth6reflejos05phc";
            this.txth6reflejos05phc.Properties.MaxLength = 10;
            this.txth6reflejos05phc.Size = new System.Drawing.Size(44, 20);
            this.txth6reflejos05phc.TabIndex = 8;
            // 
            // txth6reflejos06phc
            // 
            this.txth6reflejos06phc.Location = new System.Drawing.Point(96, 116);
            this.txth6reflejos06phc.Name = "txth6reflejos06phc";
            this.txth6reflejos06phc.Properties.MaxLength = 10;
            this.txth6reflejos06phc.Size = new System.Drawing.Size(44, 20);
            this.txth6reflejos06phc.TabIndex = 7;
            // 
            // txth6reflejos07phc
            // 
            this.txth6reflejos07phc.Location = new System.Drawing.Point(146, 130);
            this.txth6reflejos07phc.Name = "txth6reflejos07phc";
            this.txth6reflejos07phc.Properties.MaxLength = 10;
            this.txth6reflejos07phc.Size = new System.Drawing.Size(44, 20);
            this.txth6reflejos07phc.TabIndex = 9;
            // 
            // txth6reflejos08phc
            // 
            this.txth6reflejos08phc.Location = new System.Drawing.Point(96, 142);
            this.txth6reflejos08phc.Name = "txth6reflejos08phc";
            this.txth6reflejos08phc.Properties.MaxLength = 10;
            this.txth6reflejos08phc.Size = new System.Drawing.Size(44, 20);
            this.txth6reflejos08phc.TabIndex = 10;
            // 
            // txth6reflejos09phc
            // 
            this.txth6reflejos09phc.Location = new System.Drawing.Point(22, 156);
            this.txth6reflejos09phc.Name = "txth6reflejos09phc";
            this.txth6reflejos09phc.Properties.MaxLength = 10;
            this.txth6reflejos09phc.Size = new System.Drawing.Size(44, 20);
            this.txth6reflejos09phc.TabIndex = 11;
            // 
            // txth6reflejos10phc
            // 
            this.txth6reflejos10phc.Location = new System.Drawing.Point(173, 156);
            this.txth6reflejos10phc.Name = "txth6reflejos10phc";
            this.txth6reflejos10phc.Properties.MaxLength = 10;
            this.txth6reflejos10phc.Size = new System.Drawing.Size(44, 20);
            this.txth6reflejos10phc.TabIndex = 12;
            // 
            // picManiqui
            // 
            this.picManiqui.Image = ((System.Drawing.Image)(resources.GetObject("picManiqui.Image")));
            this.picManiqui.Location = new System.Drawing.Point(5, 25);
            this.picManiqui.Name = "picManiqui";
            this.picManiqui.Size = new System.Drawing.Size(226, 159);
            this.picManiqui.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picManiqui.TabIndex = 0;
            this.picManiqui.TabStop = false;
            // 
            // xtraTabPage5
            // 
            this.xtraTabPage5.Controls.Add(this.xtraScrollableControl4);
            this.xtraTabPage5.Name = "xtraTabPage5";
            this.xtraTabPage5.Size = new System.Drawing.Size(982, 594);
            this.xtraTabPage5.Text = "Hoja 5";
            // 
            // xtraScrollableControl4
            // 
            this.xtraScrollableControl4.Controls.Add(this.groupControl10);
            this.xtraScrollableControl4.Controls.Add(this.groupControl9);
            this.xtraScrollableControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl4.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl4.Name = "xtraScrollableControl4";
            this.xtraScrollableControl4.Size = new System.Drawing.Size(982, 594);
            this.xtraScrollableControl4.TabIndex = 0;
            // 
            // groupControl10
            // 
            this.groupControl10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl10.Controls.Add(this.groupBox29);
            this.groupControl10.Controls.Add(this.groupBox28);
            this.groupControl10.Controls.Add(this.groupBox27);
            this.groupControl10.Controls.Add(this.groupBox26);
            this.groupControl10.Controls.Add(this.txth5marchaphc);
            this.groupControl10.Controls.Add(this.lblh5marchaphc);
            this.groupControl10.Location = new System.Drawing.Point(3, 305);
            this.groupControl10.Name = "groupControl10";
            this.groupControl10.Size = new System.Drawing.Size(958, 284);
            this.groupControl10.TabIndex = 1;
            this.groupControl10.Text = "SISTEMA MOTOR";
            // 
            // groupBox29
            // 
            this.groupBox29.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox29.Controls.Add(this.txth5movinvoluntariosphc);
            this.groupBox29.Location = new System.Drawing.Point(487, 89);
            this.groupBox29.Name = "groupBox29";
            this.groupBox29.Size = new System.Drawing.Size(466, 59);
            this.groupBox29.TabIndex = 4;
            this.groupBox29.TabStop = false;
            this.groupBox29.Text = "MOVIMIENTOS INVOLUNTARIOS";
            // 
            // txth5movinvoluntariosphc
            // 
            this.txth5movinvoluntariosphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth5movinvoluntariosphc.Location = new System.Drawing.Point(6, 16);
            this.txth5movinvoluntariosphc.Name = "txth5movinvoluntariosphc";
            this.txth5movinvoluntariosphc.Size = new System.Drawing.Size(454, 39);
            this.txth5movinvoluntariosphc.TabIndex = 0;
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.txth5fuemuscularphc);
            this.groupBox28.Location = new System.Drawing.Point(5, 89);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(480, 59);
            this.groupBox28.TabIndex = 3;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "FUERZA MUSCULAR";
            // 
            // txth5fuemuscularphc
            // 
            this.txth5fuemuscularphc.Location = new System.Drawing.Point(6, 16);
            this.txth5fuemuscularphc.Name = "txth5fuemuscularphc";
            this.txth5fuemuscularphc.Size = new System.Drawing.Size(470, 39);
            this.txth5fuemuscularphc.TabIndex = 0;
            // 
            // groupBox27
            // 
            this.groupBox27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox27.Controls.Add(this.labelControl2);
            this.groupBox27.Controls.Add(this.labelControl1);
            this.groupBox27.Controls.Add(this.txth5habilidadespecifphc);
            this.groupBox27.Controls.Add(this.txth5rebotephc);
            this.groupBox27.Controls.Add(this.txth5movrapidphc);
            this.groupBox27.Controls.Add(this.lblh5rebotephc);
            this.groupBox27.Controls.Add(this.txth5talrodizqphc);
            this.groupBox27.Controls.Add(this.lblh5talrodizqphc);
            this.groupBox27.Controls.Add(this.txth5talrodderphc);
            this.groupBox27.Controls.Add(this.lblh5talrodderphc);
            this.groupBox27.Controls.Add(this.txth5deddedizqphc);
            this.groupBox27.Controls.Add(this.lblh5deddedizqphc);
            this.groupBox27.Controls.Add(this.txth5deddedderphc);
            this.groupBox27.Controls.Add(this.lblh5deddedderphc);
            this.groupBox27.Controls.Add(this.txth5dednarizqphc);
            this.groupBox27.Controls.Add(this.lblh5dednarizqphc);
            this.groupBox27.Controls.Add(this.txth5dednarderphc);
            this.groupBox27.Controls.Add(this.lblh5dednarderphc);
            this.groupBox27.Controls.Add(this.txth5rombergphc);
            this.groupBox27.Controls.Add(this.lblh5rombergphc);
            this.groupBox27.Controls.Add(this.txth5equilibratoriaphc);
            this.groupBox27.Controls.Add(this.lblh5equilibratoriaphc);
            this.groupBox27.Location = new System.Drawing.Point(5, 150);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(948, 129);
            this.groupBox27.TabIndex = 5;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "COORDINACION";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(664, 85);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(55, 26);
            this.labelControl2.TabIndex = 20;
            this.labelControl2.Text = "Habilidad\r\nEspecificar:";
            // 
            // labelControl1
            // 
            this.labelControl1.Location = new System.Drawing.Point(21, 85);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(85, 26);
            this.labelControl1.TabIndex = 16;
            this.labelControl1.Text = "Movimientos\r\nRápidos Alternos:";
            // 
            // txth5habilidadespecifphc
            // 
            this.txth5habilidadespecifphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth5habilidadespecifphc.Location = new System.Drawing.Point(721, 82);
            this.txth5habilidadespecifphc.Name = "txth5habilidadespecifphc";
            this.txth5habilidadespecifphc.Size = new System.Drawing.Size(221, 41);
            this.txth5habilidadespecifphc.TabIndex = 21;
            // 
            // txth5rebotephc
            // 
            this.txth5rebotephc.Location = new System.Drawing.Point(411, 82);
            this.txth5rebotephc.Name = "txth5rebotephc";
            this.txth5rebotephc.Size = new System.Drawing.Size(235, 41);
            this.txth5rebotephc.TabIndex = 18;
            // 
            // txth5movrapidphc
            // 
            this.txth5movrapidphc.Location = new System.Drawing.Point(108, 82);
            this.txth5movrapidphc.Name = "txth5movrapidphc";
            this.txth5movrapidphc.Size = new System.Drawing.Size(235, 41);
            this.txth5movrapidphc.TabIndex = 17;
            // 
            // lblh5rebotephc
            // 
            this.lblh5rebotephc.AutoSize = true;
            this.lblh5rebotephc.Location = new System.Drawing.Point(366, 85);
            this.lblh5rebotephc.Name = "lblh5rebotephc";
            this.lblh5rebotephc.Size = new System.Drawing.Size(46, 13);
            this.lblh5rebotephc.TabIndex = 19;
            this.lblh5rebotephc.Text = "Rebote:";
            // 
            // txth5talrodizqphc
            // 
            this.txth5talrodizqphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth5talrodizqphc.Location = new System.Drawing.Point(618, 60);
            this.txth5talrodizqphc.Name = "txth5talrodizqphc";
            this.txth5talrodizqphc.Properties.MaxLength = 200;
            this.txth5talrodizqphc.Size = new System.Drawing.Size(324, 20);
            this.txth5talrodizqphc.TabIndex = 15;
            // 
            // lblh5talrodizqphc
            // 
            this.lblh5talrodizqphc.AutoSize = true;
            this.lblh5talrodizqphc.Location = new System.Drawing.Point(556, 63);
            this.lblh5talrodizqphc.Name = "lblh5talrodizqphc";
            this.lblh5talrodizqphc.Size = new System.Drawing.Size(56, 13);
            this.lblh5talrodizqphc.TabIndex = 14;
            this.lblh5talrodizqphc.Text = "Izquierdo:";
            // 
            // txth5talrodderphc
            // 
            this.txth5talrodderphc.Location = new System.Drawing.Point(143, 60);
            this.txth5talrodderphc.Name = "txth5talrodderphc";
            this.txth5talrodderphc.Properties.MaxLength = 200;
            this.txth5talrodderphc.Size = new System.Drawing.Size(338, 20);
            this.txth5talrodderphc.TabIndex = 12;
            // 
            // lblh5talrodderphc
            // 
            this.lblh5talrodderphc.AutoSize = true;
            this.lblh5talrodderphc.Location = new System.Drawing.Point(18, 63);
            this.lblh5talrodderphc.Name = "lblh5talrodderphc";
            this.lblh5talrodderphc.Size = new System.Drawing.Size(127, 13);
            this.lblh5talrodderphc.TabIndex = 13;
            this.lblh5talrodderphc.Text = "Talón - Rodilla   Derecho:";
            // 
            // txth5deddedizqphc
            // 
            this.txth5deddedizqphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth5deddedizqphc.Location = new System.Drawing.Point(816, 37);
            this.txth5deddedizqphc.Name = "txth5deddedizqphc";
            this.txth5deddedizqphc.Properties.MaxLength = 200;
            this.txth5deddedizqphc.Size = new System.Drawing.Size(126, 20);
            this.txth5deddedizqphc.TabIndex = 10;
            // 
            // lblh5deddedizqphc
            // 
            this.lblh5deddedizqphc.AutoSize = true;
            this.lblh5deddedizqphc.Location = new System.Drawing.Point(761, 40);
            this.lblh5deddedizqphc.Name = "lblh5deddedizqphc";
            this.lblh5deddedizqphc.Size = new System.Drawing.Size(56, 13);
            this.lblh5deddedizqphc.TabIndex = 11;
            this.lblh5deddedizqphc.Text = "Izquierdo:";
            // 
            // txth5deddedderphc
            // 
            this.txth5deddedderphc.Location = new System.Drawing.Point(618, 37);
            this.txth5deddedderphc.Name = "txth5deddedderphc";
            this.txth5deddedderphc.Properties.MaxLength = 200;
            this.txth5deddedderphc.Size = new System.Drawing.Size(140, 20);
            this.txth5deddedderphc.TabIndex = 9;
            // 
            // lblh5deddedderphc
            // 
            this.lblh5deddedderphc.AutoSize = true;
            this.lblh5deddedderphc.Location = new System.Drawing.Point(492, 40);
            this.lblh5deddedderphc.Name = "lblh5deddedderphc";
            this.lblh5deddedderphc.Size = new System.Drawing.Size(120, 13);
            this.lblh5deddedderphc.TabIndex = 8;
            this.lblh5deddedderphc.Text = "Dedo - Dedo   Derecho:";
            // 
            // txth5dednarizqphc
            // 
            this.txth5dednarizqphc.Location = new System.Drawing.Point(341, 37);
            this.txth5dednarizqphc.Name = "txth5dednarizqphc";
            this.txth5dednarizqphc.Properties.MaxLength = 100;
            this.txth5dednarizqphc.Size = new System.Drawing.Size(140, 20);
            this.txth5dednarizqphc.TabIndex = 7;
            // 
            // lblh5dednarizqphc
            // 
            this.lblh5dednarizqphc.AutoSize = true;
            this.lblh5dednarizqphc.Location = new System.Drawing.Point(283, 40);
            this.lblh5dednarizqphc.Name = "lblh5dednarizqphc";
            this.lblh5dednarizqphc.Size = new System.Drawing.Size(56, 13);
            this.lblh5dednarizqphc.TabIndex = 6;
            this.lblh5dednarizqphc.Text = "Izquierdo:";
            // 
            // txth5dednarderphc
            // 
            this.txth5dednarderphc.Location = new System.Drawing.Point(143, 37);
            this.txth5dednarderphc.Name = "txth5dednarderphc";
            this.txth5dednarderphc.Properties.MaxLength = 100;
            this.txth5dednarderphc.Size = new System.Drawing.Size(140, 20);
            this.txth5dednarderphc.TabIndex = 5;
            // 
            // lblh5dednarderphc
            // 
            this.lblh5dednarderphc.AutoSize = true;
            this.lblh5dednarderphc.Location = new System.Drawing.Point(18, 40);
            this.lblh5dednarderphc.Name = "lblh5dednarderphc";
            this.lblh5dednarderphc.Size = new System.Drawing.Size(119, 13);
            this.lblh5dednarderphc.TabIndex = 4;
            this.lblh5dednarderphc.Text = "Dedo - Nariz   Derecho:";
            // 
            // txth5rombergphc
            // 
            this.txth5rombergphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth5rombergphc.Location = new System.Drawing.Point(591, 14);
            this.txth5rombergphc.Name = "txth5rombergphc";
            this.txth5rombergphc.Properties.MaxLength = 200;
            this.txth5rombergphc.Size = new System.Drawing.Size(351, 20);
            this.txth5rombergphc.TabIndex = 3;
            // 
            // lblh5rombergphc
            // 
            this.lblh5rombergphc.AutoSize = true;
            this.lblh5rombergphc.Location = new System.Drawing.Point(527, 17);
            this.lblh5rombergphc.Name = "lblh5rombergphc";
            this.lblh5rombergphc.Size = new System.Drawing.Size(54, 13);
            this.lblh5rombergphc.TabIndex = 2;
            this.lblh5rombergphc.Text = "Romberg:";
            // 
            // txth5equilibratoriaphc
            // 
            this.txth5equilibratoriaphc.Location = new System.Drawing.Point(116, 14);
            this.txth5equilibratoriaphc.Name = "txth5equilibratoriaphc";
            this.txth5equilibratoriaphc.Properties.MaxLength = 200;
            this.txth5equilibratoriaphc.Size = new System.Drawing.Size(365, 20);
            this.txth5equilibratoriaphc.TabIndex = 1;
            // 
            // lblh5equilibratoriaphc
            // 
            this.lblh5equilibratoriaphc.AutoSize = true;
            this.lblh5equilibratoriaphc.Location = new System.Drawing.Point(18, 17);
            this.lblh5equilibratoriaphc.Name = "lblh5equilibratoriaphc";
            this.lblh5equilibratoriaphc.Size = new System.Drawing.Size(73, 13);
            this.lblh5equilibratoriaphc.TabIndex = 0;
            this.lblh5equilibratoriaphc.Text = "Equilibratoria:";
            // 
            // groupBox26
            // 
            this.groupBox26.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox26.Controls.Add(this.txth5fasciculacionesphc);
            this.groupBox26.Controls.Add(this.lblh5fasciculacionesphc);
            this.groupBox26.Controls.Add(this.txth5volumenphc);
            this.groupBox26.Controls.Add(this.lblh5volumenphc);
            this.groupBox26.Controls.Add(this.txth5tonophc);
            this.groupBox26.Controls.Add(this.lblh5tonophc);
            this.groupBox26.Location = new System.Drawing.Point(5, 47);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(948, 40);
            this.groupBox26.TabIndex = 2;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "ESTADO MUSCULAR";
            // 
            // txth5fasciculacionesphc
            // 
            this.txth5fasciculacionesphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth5fasciculacionesphc.Location = new System.Drawing.Point(711, 15);
            this.txth5fasciculacionesphc.Name = "txth5fasciculacionesphc";
            this.txth5fasciculacionesphc.Properties.MaxLength = 200;
            this.txth5fasciculacionesphc.Size = new System.Drawing.Size(231, 20);
            this.txth5fasciculacionesphc.TabIndex = 4;
            // 
            // lblh5fasciculacionesphc
            // 
            this.lblh5fasciculacionesphc.AutoSize = true;
            this.lblh5fasciculacionesphc.Location = new System.Drawing.Point(630, 18);
            this.lblh5fasciculacionesphc.Name = "lblh5fasciculacionesphc";
            this.lblh5fasciculacionesphc.Size = new System.Drawing.Size(84, 13);
            this.lblh5fasciculacionesphc.TabIndex = 5;
            this.lblh5fasciculacionesphc.Text = "Fasciculaciones:";
            // 
            // txth5volumenphc
            // 
            this.txth5volumenphc.Location = new System.Drawing.Point(383, 14);
            this.txth5volumenphc.Name = "txth5volumenphc";
            this.txth5volumenphc.Properties.MaxLength = 200;
            this.txth5volumenphc.Size = new System.Drawing.Size(245, 20);
            this.txth5volumenphc.TabIndex = 2;
            // 
            // lblh5volumenphc
            // 
            this.lblh5volumenphc.AutoSize = true;
            this.lblh5volumenphc.Location = new System.Drawing.Point(336, 17);
            this.lblh5volumenphc.Name = "lblh5volumenphc";
            this.lblh5volumenphc.Size = new System.Drawing.Size(51, 13);
            this.lblh5volumenphc.TabIndex = 3;
            this.lblh5volumenphc.Text = "Volumen:";
            // 
            // txth5tonophc
            // 
            this.txth5tonophc.Location = new System.Drawing.Point(83, 14);
            this.txth5tonophc.Name = "txth5tonophc";
            this.txth5tonophc.Properties.MaxLength = 200;
            this.txth5tonophc.Size = new System.Drawing.Size(245, 20);
            this.txth5tonophc.TabIndex = 1;
            // 
            // lblh5tonophc
            // 
            this.lblh5tonophc.AutoSize = true;
            this.lblh5tonophc.Location = new System.Drawing.Point(18, 17);
            this.lblh5tonophc.Name = "lblh5tonophc";
            this.lblh5tonophc.Size = new System.Drawing.Size(35, 13);
            this.lblh5tonophc.TabIndex = 0;
            this.lblh5tonophc.Text = "Tono:";
            // 
            // txth5marchaphc
            // 
            this.txth5marchaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth5marchaphc.Location = new System.Drawing.Point(88, 25);
            this.txth5marchaphc.Name = "txth5marchaphc";
            this.txth5marchaphc.Properties.MaxLength = 1000;
            this.txth5marchaphc.Size = new System.Drawing.Size(859, 20);
            this.txth5marchaphc.TabIndex = 1;
            // 
            // lblh5marchaphc
            // 
            this.lblh5marchaphc.AutoSize = true;
            this.lblh5marchaphc.Location = new System.Drawing.Point(23, 28);
            this.lblh5marchaphc.Name = "lblh5marchaphc";
            this.lblh5marchaphc.Size = new System.Drawing.Size(46, 13);
            this.lblh5marchaphc.TabIndex = 0;
            this.lblh5marchaphc.Text = "Marcha:";
            // 
            // groupControl9
            // 
            this.groupControl9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl9.Controls.Add(this.groupBox25);
            this.groupControl9.Controls.Add(this.groupBox24);
            this.groupControl9.Controls.Add(this.groupBox23);
            this.groupControl9.Controls.Add(this.groupBox22);
            this.groupControl9.Location = new System.Drawing.Point(3, 4);
            this.groupControl9.Name = "groupControl9";
            this.groupControl9.Size = new System.Drawing.Size(958, 400);
            this.groupControl9.TabIndex = 0;
            // 
            // groupBox25
            // 
            this.groupBox25.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox25.Controls.Add(this.lblh5desviacionphc);
            this.groupBox25.Controls.Add(this.txth5desviacionphc);
            this.groupBox25.Controls.Add(this.lblh5atrofiaphc);
            this.groupBox25.Controls.Add(this.txth5atrofiaphc);
            this.groupBox25.Controls.Add(this.lblh5fasciculacionphc);
            this.groupBox25.Controls.Add(this.txth5fasciculacionphc);
            this.groupBox25.Controls.Add(this.lblh5fuerzaphc);
            this.groupBox25.Controls.Add(this.txth5fuerzaphc);
            this.groupBox25.Location = new System.Drawing.Point(5, 225);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(946, 66);
            this.groupBox25.TabIndex = 3;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "XII. HIPOGLOSO";
            // 
            // lblh5desviacionphc
            // 
            this.lblh5desviacionphc.AutoSize = true;
            this.lblh5desviacionphc.Location = new System.Drawing.Point(18, 19);
            this.lblh5desviacionphc.Name = "lblh5desviacionphc";
            this.lblh5desviacionphc.Size = new System.Drawing.Size(62, 13);
            this.lblh5desviacionphc.TabIndex = 0;
            this.lblh5desviacionphc.Text = "Desviacion:";
            // 
            // txth5desviacionphc
            // 
            this.txth5desviacionphc.Location = new System.Drawing.Point(108, 16);
            this.txth5desviacionphc.Name = "txth5desviacionphc";
            this.txth5desviacionphc.Properties.MaxLength = 200;
            this.txth5desviacionphc.Size = new System.Drawing.Size(365, 20);
            this.txth5desviacionphc.TabIndex = 1;
            // 
            // lblh5atrofiaphc
            // 
            this.lblh5atrofiaphc.AutoSize = true;
            this.lblh5atrofiaphc.Location = new System.Drawing.Point(527, 19);
            this.lblh5atrofiaphc.Name = "lblh5atrofiaphc";
            this.lblh5atrofiaphc.Size = new System.Drawing.Size(44, 13);
            this.lblh5atrofiaphc.TabIndex = 2;
            this.lblh5atrofiaphc.Text = "Atrofia:";
            // 
            // txth5atrofiaphc
            // 
            this.txth5atrofiaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth5atrofiaphc.Location = new System.Drawing.Point(591, 16);
            this.txth5atrofiaphc.Name = "txth5atrofiaphc";
            this.txth5atrofiaphc.Properties.MaxLength = 200;
            this.txth5atrofiaphc.Size = new System.Drawing.Size(349, 20);
            this.txth5atrofiaphc.TabIndex = 3;
            // 
            // lblh5fasciculacionphc
            // 
            this.lblh5fasciculacionphc.AutoSize = true;
            this.lblh5fasciculacionphc.Location = new System.Drawing.Point(18, 42);
            this.lblh5fasciculacionphc.Name = "lblh5fasciculacionphc";
            this.lblh5fasciculacionphc.Size = new System.Drawing.Size(84, 13);
            this.lblh5fasciculacionphc.TabIndex = 4;
            this.lblh5fasciculacionphc.Text = "Fasciculaciones:";
            // 
            // txth5fasciculacionphc
            // 
            this.txth5fasciculacionphc.Location = new System.Drawing.Point(108, 39);
            this.txth5fasciculacionphc.Name = "txth5fasciculacionphc";
            this.txth5fasciculacionphc.Properties.MaxLength = 200;
            this.txth5fasciculacionphc.Size = new System.Drawing.Size(365, 20);
            this.txth5fasciculacionphc.TabIndex = 5;
            // 
            // lblh5fuerzaphc
            // 
            this.lblh5fuerzaphc.AutoSize = true;
            this.lblh5fuerzaphc.Location = new System.Drawing.Point(527, 42);
            this.lblh5fuerzaphc.Name = "lblh5fuerzaphc";
            this.lblh5fuerzaphc.Size = new System.Drawing.Size(44, 13);
            this.lblh5fuerzaphc.TabIndex = 6;
            this.lblh5fuerzaphc.Text = "Fuerza:";
            // 
            // txth5fuerzaphc
            // 
            this.txth5fuerzaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth5fuerzaphc.Location = new System.Drawing.Point(591, 39);
            this.txth5fuerzaphc.Name = "txth5fuerzaphc";
            this.txth5fuerzaphc.Properties.MaxLength = 200;
            this.txth5fuerzaphc.Size = new System.Drawing.Size(349, 20);
            this.txth5fuerzaphc.TabIndex = 7;
            // 
            // groupBox24
            // 
            this.groupBox24.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox24.Controls.Add(this.txth5esternocleidomastoideophc);
            this.groupBox24.Controls.Add(this.txth5trapeciophc);
            this.groupBox24.Controls.Add(this.lblh5trapeciophc);
            this.groupBox24.Controls.Add(this.lblh5esternocleidomastoideophc);
            this.groupBox24.Location = new System.Drawing.Point(5, 183);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(946, 40);
            this.groupBox24.TabIndex = 2;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "XI. ESPINAL";
            // 
            // txth5esternocleidomastoideophc
            // 
            this.txth5esternocleidomastoideophc.Location = new System.Drawing.Point(143, 14);
            this.txth5esternocleidomastoideophc.Name = "txth5esternocleidomastoideophc";
            this.txth5esternocleidomastoideophc.Properties.MaxLength = 200;
            this.txth5esternocleidomastoideophc.Size = new System.Drawing.Size(365, 20);
            this.txth5esternocleidomastoideophc.TabIndex = 1;
            // 
            // txth5trapeciophc
            // 
            this.txth5trapeciophc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth5trapeciophc.Location = new System.Drawing.Point(591, 14);
            this.txth5trapeciophc.Name = "txth5trapeciophc";
            this.txth5trapeciophc.Properties.MaxLength = 200;
            this.txth5trapeciophc.Size = new System.Drawing.Size(349, 20);
            this.txth5trapeciophc.TabIndex = 3;
            // 
            // lblh5trapeciophc
            // 
            this.lblh5trapeciophc.AutoSize = true;
            this.lblh5trapeciophc.Location = new System.Drawing.Point(527, 17);
            this.lblh5trapeciophc.Name = "lblh5trapeciophc";
            this.lblh5trapeciophc.Size = new System.Drawing.Size(52, 13);
            this.lblh5trapeciophc.TabIndex = 2;
            this.lblh5trapeciophc.Text = "Trapecio:";
            // 
            // lblh5esternocleidomastoideophc
            // 
            this.lblh5esternocleidomastoideophc.AutoSize = true;
            this.lblh5esternocleidomastoideophc.Location = new System.Drawing.Point(18, 17);
            this.lblh5esternocleidomastoideophc.Name = "lblh5esternocleidomastoideophc";
            this.lblh5esternocleidomastoideophc.Size = new System.Drawing.Size(124, 13);
            this.lblh5esternocleidomastoideophc.TabIndex = 0;
            this.lblh5esternocleidomastoideophc.Text = "Esternocleidomastoideo:";
            // 
            // groupBox23
            // 
            this.groupBox23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox23.Controls.Add(this.txth5elevpaladarphc);
            this.groupBox23.Controls.Add(this.lblh5elevpaladarphc);
            this.groupBox23.Controls.Add(this.txth5refnauceosophc);
            this.groupBox23.Controls.Add(this.lblh5refnauceosophc);
            this.groupBox23.Controls.Add(this.txth5uvulaphc);
            this.groupBox23.Controls.Add(this.lblh5uvulaphc);
            this.groupBox23.Controls.Add(this.txth5deglucionphc);
            this.groupBox23.Controls.Add(this.txth5tonovozphc);
            this.groupBox23.Controls.Add(this.lblh5tonovozphc);
            this.groupBox23.Controls.Add(this.lblh5deglucionphc);
            this.groupBox23.Location = new System.Drawing.Point(5, 115);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(946, 66);
            this.groupBox23.TabIndex = 1;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "IX. - X. GLOSOFARINGERO Y VAGO";
            // 
            // txth5elevpaladarphc
            // 
            this.txth5elevpaladarphc.Location = new System.Drawing.Point(130, 17);
            this.txth5elevpaladarphc.Name = "txth5elevpaladarphc";
            this.txth5elevpaladarphc.Properties.MaxLength = 200;
            this.txth5elevpaladarphc.Size = new System.Drawing.Size(220, 20);
            this.txth5elevpaladarphc.TabIndex = 1;
            // 
            // lblh5elevpaladarphc
            // 
            this.lblh5elevpaladarphc.AutoSize = true;
            this.lblh5elevpaladarphc.Location = new System.Drawing.Point(18, 20);
            this.lblh5elevpaladarphc.Name = "lblh5elevpaladarphc";
            this.lblh5elevpaladarphc.Size = new System.Drawing.Size(112, 13);
            this.lblh5elevpaladarphc.TabIndex = 0;
            this.lblh5elevpaladarphc.Text = "Elevación del Paladar:";
            // 
            // txth5refnauceosophc
            // 
            this.txth5refnauceosophc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth5refnauceosophc.Location = new System.Drawing.Point(736, 17);
            this.txth5refnauceosophc.Name = "txth5refnauceosophc";
            this.txth5refnauceosophc.Properties.MaxLength = 200;
            this.txth5refnauceosophc.Size = new System.Drawing.Size(204, 20);
            this.txth5refnauceosophc.TabIndex = 5;
            // 
            // lblh5refnauceosophc
            // 
            this.lblh5refnauceosophc.AutoSize = true;
            this.lblh5refnauceosophc.Location = new System.Drawing.Point(639, 20);
            this.lblh5refnauceosophc.Name = "lblh5refnauceosophc";
            this.lblh5refnauceosophc.Size = new System.Drawing.Size(95, 13);
            this.lblh5refnauceosophc.TabIndex = 4;
            this.lblh5refnauceosophc.Text = "Reflejo Nauceoso:";
            // 
            // txth5uvulaphc
            // 
            this.txth5uvulaphc.Location = new System.Drawing.Point(402, 17);
            this.txth5uvulaphc.Name = "txth5uvulaphc";
            this.txth5uvulaphc.Properties.MaxLength = 200;
            this.txth5uvulaphc.Size = new System.Drawing.Size(220, 20);
            this.txth5uvulaphc.TabIndex = 3;
            // 
            // lblh5uvulaphc
            // 
            this.lblh5uvulaphc.AutoSize = true;
            this.lblh5uvulaphc.Location = new System.Drawing.Point(362, 20);
            this.lblh5uvulaphc.Name = "lblh5uvulaphc";
            this.lblh5uvulaphc.Size = new System.Drawing.Size(38, 13);
            this.lblh5uvulaphc.TabIndex = 2;
            this.lblh5uvulaphc.Text = "Úvula:";
            // 
            // txth5deglucionphc
            // 
            this.txth5deglucionphc.Location = new System.Drawing.Point(83, 40);
            this.txth5deglucionphc.Name = "txth5deglucionphc";
            this.txth5deglucionphc.Properties.MaxLength = 200;
            this.txth5deglucionphc.Size = new System.Drawing.Size(390, 20);
            this.txth5deglucionphc.TabIndex = 7;
            // 
            // txth5tonovozphc
            // 
            this.txth5tonovozphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth5tonovozphc.Location = new System.Drawing.Point(566, 40);
            this.txth5tonovozphc.Name = "txth5tonovozphc";
            this.txth5tonovozphc.Properties.MaxLength = 200;
            this.txth5tonovozphc.Size = new System.Drawing.Size(374, 20);
            this.txth5tonovozphc.TabIndex = 9;
            // 
            // lblh5tonovozphc
            // 
            this.lblh5tonovozphc.AutoSize = true;
            this.lblh5tonovozphc.Location = new System.Drawing.Point(479, 43);
            this.lblh5tonovozphc.Name = "lblh5tonovozphc";
            this.lblh5tonovozphc.Size = new System.Drawing.Size(81, 13);
            this.lblh5tonovozphc.TabIndex = 8;
            this.lblh5tonovozphc.Text = "Tono de la Voz:";
            // 
            // lblh5deglucionphc
            // 
            this.lblh5deglucionphc.AutoSize = true;
            this.lblh5deglucionphc.Location = new System.Drawing.Point(18, 43);
            this.lblh5deglucionphc.Name = "lblh5deglucionphc";
            this.lblh5deglucionphc.Size = new System.Drawing.Size(57, 13);
            this.lblh5deglucionphc.TabIndex = 6;
            this.lblh5deglucionphc.Text = "Deglución:";
            // 
            // groupBox22
            // 
            this.groupBox22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox22.Controls.Add(this.txth5otoscopiaphc);
            this.groupBox22.Controls.Add(this.txth5aguaudiderphc);
            this.groupBox22.Controls.Add(this.lblh5otoscopiaphc);
            this.groupBox22.Controls.Add(this.lblh5aguaudiderphc);
            this.groupBox22.Controls.Add(this.txth5aguaudiizqphc);
            this.groupBox22.Controls.Add(this.lblh5aguaudiizqphc);
            this.groupBox22.Controls.Add(this.txth5weberphc);
            this.groupBox22.Controls.Add(this.lblh5weberphc);
            this.groupBox22.Controls.Add(this.txth5rinnephc);
            this.groupBox22.Controls.Add(this.lblh5rinnephc);
            this.groupBox22.Controls.Add(this.txth5pruebaslabphc);
            this.groupBox22.Controls.Add(this.lblh5pruebaslabphc);
            this.groupBox22.Location = new System.Drawing.Point(5, 25);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(946, 88);
            this.groupBox22.TabIndex = 0;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "VIII: AUDITIVO Y VESTIBULAR";
            // 
            // txth5otoscopiaphc
            // 
            this.txth5otoscopiaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth5otoscopiaphc.Location = new System.Drawing.Point(83, 16);
            this.txth5otoscopiaphc.Name = "txth5otoscopiaphc";
            this.txth5otoscopiaphc.Properties.MaxLength = 1000;
            this.txth5otoscopiaphc.Size = new System.Drawing.Size(857, 20);
            this.txth5otoscopiaphc.TabIndex = 1;
            // 
            // txth5aguaudiderphc
            // 
            this.txth5aguaudiderphc.Location = new System.Drawing.Point(166, 39);
            this.txth5aguaudiderphc.Name = "txth5aguaudiderphc";
            this.txth5aguaudiderphc.Properties.MaxLength = 200;
            this.txth5aguaudiderphc.Size = new System.Drawing.Size(220, 20);
            this.txth5aguaudiderphc.TabIndex = 3;
            // 
            // lblh5otoscopiaphc
            // 
            this.lblh5otoscopiaphc.AutoSize = true;
            this.lblh5otoscopiaphc.Location = new System.Drawing.Point(18, 19);
            this.lblh5otoscopiaphc.Name = "lblh5otoscopiaphc";
            this.lblh5otoscopiaphc.Size = new System.Drawing.Size(59, 13);
            this.lblh5otoscopiaphc.TabIndex = 0;
            this.lblh5otoscopiaphc.Text = "Otoscopía:";
            // 
            // lblh5aguaudiderphc
            // 
            this.lblh5aguaudiderphc.AutoSize = true;
            this.lblh5aguaudiderphc.Location = new System.Drawing.Point(18, 42);
            this.lblh5aguaudiderphc.Name = "lblh5aguaudiderphc";
            this.lblh5aguaudiderphc.Size = new System.Drawing.Size(144, 13);
            this.lblh5aguaudiderphc.TabIndex = 2;
            this.lblh5aguaudiderphc.Text = "Agudeza Auditiva   Derecha:";
            // 
            // txth5aguaudiizqphc
            // 
            this.txth5aguaudiizqphc.Location = new System.Drawing.Point(459, 39);
            this.txth5aguaudiizqphc.Name = "txth5aguaudiizqphc";
            this.txth5aguaudiizqphc.Properties.MaxLength = 200;
            this.txth5aguaudiizqphc.Size = new System.Drawing.Size(220, 20);
            this.txth5aguaudiizqphc.TabIndex = 5;
            // 
            // lblh5aguaudiizqphc
            // 
            this.lblh5aguaudiizqphc.AutoSize = true;
            this.lblh5aguaudiizqphc.Location = new System.Drawing.Point(397, 42);
            this.lblh5aguaudiizqphc.Name = "lblh5aguaudiizqphc";
            this.lblh5aguaudiizqphc.Size = new System.Drawing.Size(56, 13);
            this.lblh5aguaudiizqphc.TabIndex = 4;
            this.lblh5aguaudiizqphc.Text = "Izquierda:";
            // 
            // txth5weberphc
            // 
            this.txth5weberphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth5weberphc.Location = new System.Drawing.Point(736, 39);
            this.txth5weberphc.Name = "txth5weberphc";
            this.txth5weberphc.Properties.MaxLength = 200;
            this.txth5weberphc.Size = new System.Drawing.Size(204, 20);
            this.txth5weberphc.TabIndex = 7;
            // 
            // lblh5weberphc
            // 
            this.lblh5weberphc.AutoSize = true;
            this.lblh5weberphc.Location = new System.Drawing.Point(687, 42);
            this.lblh5weberphc.Name = "lblh5weberphc";
            this.lblh5weberphc.Size = new System.Drawing.Size(43, 13);
            this.lblh5weberphc.TabIndex = 6;
            this.lblh5weberphc.Text = "Weber:";
            // 
            // txth5rinnephc
            // 
            this.txth5rinnephc.Location = new System.Drawing.Point(83, 62);
            this.txth5rinnephc.Name = "txth5rinnephc";
            this.txth5rinnephc.Properties.MaxLength = 200;
            this.txth5rinnephc.Size = new System.Drawing.Size(390, 20);
            this.txth5rinnephc.TabIndex = 9;
            // 
            // lblh5rinnephc
            // 
            this.lblh5rinnephc.AutoSize = true;
            this.lblh5rinnephc.Location = new System.Drawing.Point(18, 65);
            this.lblh5rinnephc.Name = "lblh5rinnephc";
            this.lblh5rinnephc.Size = new System.Drawing.Size(38, 13);
            this.lblh5rinnephc.TabIndex = 8;
            this.lblh5rinnephc.Text = "Rinné:";
            // 
            // txth5pruebaslabphc
            // 
            this.txth5pruebaslabphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth5pruebaslabphc.Location = new System.Drawing.Point(591, 62);
            this.txth5pruebaslabphc.Name = "txth5pruebaslabphc";
            this.txth5pruebaslabphc.Properties.MaxLength = 200;
            this.txth5pruebaslabphc.Size = new System.Drawing.Size(349, 20);
            this.txth5pruebaslabphc.TabIndex = 11;
            // 
            // lblh5pruebaslabphc
            // 
            this.lblh5pruebaslabphc.AutoSize = true;
            this.lblh5pruebaslabphc.Location = new System.Drawing.Point(479, 65);
            this.lblh5pruebaslabphc.Name = "lblh5pruebaslabphc";
            this.lblh5pruebaslabphc.Size = new System.Drawing.Size(110, 13);
            this.lblh5pruebaslabphc.TabIndex = 10;
            this.lblh5pruebaslabphc.Text = "Pruebas Laberínticas:";
            // 
            // xtraTabPage3
            // 
            this.xtraTabPage3.Controls.Add(this.xtraScrollableControl2);
            this.xtraTabPage3.Name = "xtraTabPage3";
            this.xtraTabPage3.ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            this.xtraTabPage3.Size = new System.Drawing.Size(982, 594);
            this.xtraTabPage3.Text = "Hoja 3";
            // 
            // xtraScrollableControl2
            // 
            this.xtraScrollableControl2.Controls.Add(this.xtraScrollableControl8);
            this.xtraScrollableControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl2.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl2.Name = "xtraScrollableControl2";
            this.xtraScrollableControl2.Size = new System.Drawing.Size(982, 594);
            this.xtraScrollableControl2.TabIndex = 0;
            // 
            // xtraScrollableControl8
            // 
            this.xtraScrollableControl8.Controls.Add(this.groupControl7);
            this.xtraScrollableControl8.Controls.Add(this.groupControl6);
            this.xtraScrollableControl8.Controls.Add(this.groupControl5);
            this.xtraScrollableControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl8.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl8.Name = "xtraScrollableControl8";
            this.xtraScrollableControl8.Size = new System.Drawing.Size(982, 594);
            this.xtraScrollableControl8.TabIndex = 0;
            // 
            // groupControl7
            // 
            this.groupControl7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl7.Controls.Add(this.groupBox15);
            this.groupControl7.Controls.Add(this.groupBox14);
            this.groupControl7.Controls.Add(this.groupBox13);
            this.groupControl7.Controls.Add(this.groupBox12);
            this.groupControl7.Location = new System.Drawing.Point(3, 422);
            this.groupControl7.Name = "groupControl7";
            this.groupControl7.Size = new System.Drawing.Size(975, 169);
            this.groupControl7.TabIndex = 2;
            this.groupControl7.Text = "IV. Funciones Cognoscitivas Localizadas";
            // 
            // groupBox15
            // 
            this.groupBox15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox15.Controls.Add(this.txth3aprdesatencionphc);
            this.groupBox15.Controls.Add(this.lblh3aprdesatencionphc);
            this.groupBox15.Controls.Add(this.lblh3anosognosiaphc);
            this.groupBox15.Controls.Add(this.txth3anosognosiaphc);
            this.groupBox15.Location = new System.Drawing.Point(5, 130);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(965, 35);
            this.groupBox15.TabIndex = 3;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "D. Anosognosia";
            // 
            // txth3aprdesatencionphc
            // 
            this.txth3aprdesatencionphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth3aprdesatencionphc.Location = new System.Drawing.Point(576, 11);
            this.txth3aprdesatencionphc.Name = "txth3aprdesatencionphc";
            this.txth3aprdesatencionphc.Properties.MaxLength = 200;
            this.txth3aprdesatencionphc.Size = new System.Drawing.Size(383, 20);
            this.txth3aprdesatencionphc.TabIndex = 2;
            // 
            // lblh3aprdesatencionphc
            // 
            this.lblh3aprdesatencionphc.AutoSize = true;
            this.lblh3aprdesatencionphc.Location = new System.Drawing.Point(480, 14);
            this.lblh3aprdesatencionphc.Name = "lblh3aprdesatencionphc";
            this.lblh3aprdesatencionphc.Size = new System.Drawing.Size(98, 13);
            this.lblh3aprdesatencionphc.TabIndex = 3;
            this.lblh3aprdesatencionphc.Text = "Desatención Táctil:";
            // 
            // lblh3anosognosiaphc
            // 
            this.lblh3anosognosiaphc.AutoSize = true;
            this.lblh3anosognosiaphc.Location = new System.Drawing.Point(26, 14);
            this.lblh3anosognosiaphc.Name = "lblh3anosognosiaphc";
            this.lblh3anosognosiaphc.Size = new System.Drawing.Size(72, 13);
            this.lblh3anosognosiaphc.TabIndex = 0;
            this.lblh3anosognosiaphc.Text = "Anosognosia:";
            // 
            // txth3anosognosiaphc
            // 
            this.txth3anosognosiaphc.Location = new System.Drawing.Point(102, 11);
            this.txth3anosognosiaphc.Name = "txth3anosognosiaphc";
            this.txth3anosognosiaphc.Properties.MaxLength = 200;
            this.txth3anosognosiaphc.Size = new System.Drawing.Size(380, 20);
            this.txth3anosognosiaphc.TabIndex = 1;
            // 
            // groupBox14
            // 
            this.groupBox14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox14.Controls.Add(this.lblh3aprideacionalphc);
            this.groupBox14.Controls.Add(this.txth3aprideacionalphc);
            this.groupBox14.Controls.Add(this.lblh3aprideomotoraphc);
            this.groupBox14.Controls.Add(this.txth3aprideomotoraphc);
            this.groupBox14.Location = new System.Drawing.Point(5, 94);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(965, 35);
            this.groupBox14.TabIndex = 2;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "C. Apraxias";
            // 
            // lblh3aprideacionalphc
            // 
            this.lblh3aprideacionalphc.AutoSize = true;
            this.lblh3aprideacionalphc.Location = new System.Drawing.Point(16, 14);
            this.lblh3aprideacionalphc.Name = "lblh3aprideacionalphc";
            this.lblh3aprideacionalphc.Size = new System.Drawing.Size(60, 13);
            this.lblh3aprideacionalphc.TabIndex = 0;
            this.lblh3aprideacionalphc.Text = "Ideacional:";
            // 
            // txth3aprideacionalphc
            // 
            this.txth3aprideacionalphc.Location = new System.Drawing.Point(102, 11);
            this.txth3aprideacionalphc.Name = "txth3aprideacionalphc";
            this.txth3aprideacionalphc.Properties.MaxLength = 200;
            this.txth3aprideacionalphc.Size = new System.Drawing.Size(380, 20);
            this.txth3aprideacionalphc.TabIndex = 1;
            // 
            // lblh3aprideomotoraphc
            // 
            this.lblh3aprideomotoraphc.AutoSize = true;
            this.lblh3aprideomotoraphc.Location = new System.Drawing.Point(490, 14);
            this.lblh3aprideomotoraphc.Name = "lblh3aprideomotoraphc";
            this.lblh3aprideomotoraphc.Size = new System.Drawing.Size(67, 13);
            this.lblh3aprideomotoraphc.TabIndex = 2;
            this.lblh3aprideomotoraphc.Text = "Ideomotora:";
            // 
            // txth3aprideomotoraphc
            // 
            this.txth3aprideomotoraphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth3aprideomotoraphc.Location = new System.Drawing.Point(576, 11);
            this.txth3aprideomotoraphc.Name = "txth3aprideomotoraphc";
            this.txth3aprideomotoraphc.Properties.MaxLength = 200;
            this.txth3aprideomotoraphc.Size = new System.Drawing.Size(383, 20);
            this.txth3aprideomotoraphc.TabIndex = 3;
            // 
            // groupBox13
            // 
            this.groupBox13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox13.Controls.Add(this.lblh3agnauditivaphc);
            this.groupBox13.Controls.Add(this.txth3agnauditivaphc);
            this.groupBox13.Controls.Add(this.lblh3agnvisualphc);
            this.groupBox13.Controls.Add(this.txth3agnvisualphc);
            this.groupBox13.Controls.Add(this.lblh3agntactilphc);
            this.groupBox13.Controls.Add(this.txth3agntactilphc);
            this.groupBox13.Location = new System.Drawing.Point(5, 58);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(965, 35);
            this.groupBox13.TabIndex = 1;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "B. Agnosias";
            // 
            // lblh3agnauditivaphc
            // 
            this.lblh3agnauditivaphc.AutoSize = true;
            this.lblh3agnauditivaphc.Location = new System.Drawing.Point(16, 14);
            this.lblh3agnauditivaphc.Name = "lblh3agnauditivaphc";
            this.lblh3agnauditivaphc.Size = new System.Drawing.Size(50, 13);
            this.lblh3agnauditivaphc.TabIndex = 0;
            this.lblh3agnauditivaphc.Text = "Auditiva:";
            // 
            // txth3agnauditivaphc
            // 
            this.txth3agnauditivaphc.Location = new System.Drawing.Point(102, 11);
            this.txth3agnauditivaphc.Name = "txth3agnauditivaphc";
            this.txth3agnauditivaphc.Properties.MaxLength = 100;
            this.txth3agnauditivaphc.Size = new System.Drawing.Size(240, 20);
            this.txth3agnauditivaphc.TabIndex = 1;
            // 
            // lblh3agnvisualphc
            // 
            this.lblh3agnvisualphc.AutoSize = true;
            this.lblh3agnvisualphc.Location = new System.Drawing.Point(373, 14);
            this.lblh3agnvisualphc.Name = "lblh3agnvisualphc";
            this.lblh3agnvisualphc.Size = new System.Drawing.Size(38, 13);
            this.lblh3agnvisualphc.TabIndex = 2;
            this.lblh3agnvisualphc.Text = "Visual:";
            // 
            // txth3agnvisualphc
            // 
            this.txth3agnvisualphc.Location = new System.Drawing.Point(417, 11);
            this.txth3agnvisualphc.Name = "txth3agnvisualphc";
            this.txth3agnvisualphc.Properties.MaxLength = 100;
            this.txth3agnvisualphc.Size = new System.Drawing.Size(240, 20);
            this.txth3agnvisualphc.TabIndex = 3;
            // 
            // lblh3agntactilphc
            // 
            this.lblh3agntactilphc.AutoSize = true;
            this.lblh3agntactilphc.Location = new System.Drawing.Point(668, 14);
            this.lblh3agntactilphc.Name = "lblh3agntactilphc";
            this.lblh3agntactilphc.Size = new System.Drawing.Size(36, 13);
            this.lblh3agntactilphc.TabIndex = 4;
            this.lblh3agntactilphc.Text = "Táctil:";
            // 
            // txth3agntactilphc
            // 
            this.txth3agntactilphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth3agntactilphc.Location = new System.Drawing.Point(710, 11);
            this.txth3agntactilphc.Name = "txth3agntactilphc";
            this.txth3agntactilphc.Properties.MaxLength = 100;
            this.txth3agntactilphc.Size = new System.Drawing.Size(249, 20);
            this.txth3agntactilphc.TabIndex = 5;
            // 
            // groupBox12
            // 
            this.groupBox12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox12.Controls.Add(this.txth3lpdagrafiaphc);
            this.groupBox12.Controls.Add(this.lblh3lpdacalculiaphc);
            this.groupBox12.Controls.Add(this.txth3lpdagnosiaphc);
            this.groupBox12.Controls.Add(this.lblh3lpdagnosiaphc);
            this.groupBox12.Controls.Add(this.txth3lpdacalculiaphc);
            this.groupBox12.Controls.Add(this.lblh3lpdagrafiaphc);
            this.groupBox12.Controls.Add(this.txth3lpddesorientacionphc);
            this.groupBox12.Controls.Add(this.lblh3lpddesorientacionphc);
            this.groupBox12.Location = new System.Drawing.Point(5, 22);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(965, 35);
            this.groupBox12.TabIndex = 0;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "A. Lóbulo Parietal Dominante";
            // 
            // txth3lpdagrafiaphc
            // 
            this.txth3lpdagrafiaphc.Location = new System.Drawing.Point(299, 12);
            this.txth3lpdagrafiaphc.Name = "txth3lpdagrafiaphc";
            this.txth3lpdagrafiaphc.Properties.MaxLength = 100;
            this.txth3lpdagrafiaphc.Size = new System.Drawing.Size(145, 20);
            this.txth3lpdagrafiaphc.TabIndex = 5;
            // 
            // lblh3lpdacalculiaphc
            // 
            this.lblh3lpdacalculiaphc.AutoSize = true;
            this.lblh3lpdacalculiaphc.Location = new System.Drawing.Point(16, 14);
            this.lblh3lpdacalculiaphc.Name = "lblh3lpdacalculiaphc";
            this.lblh3lpdacalculiaphc.Size = new System.Drawing.Size(52, 13);
            this.lblh3lpdacalculiaphc.TabIndex = 0;
            this.lblh3lpdacalculiaphc.Text = "Acalculia:";
            // 
            // txth3lpdagnosiaphc
            // 
            this.txth3lpdagnosiaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth3lpdagnosiaphc.Location = new System.Drawing.Point(811, 11);
            this.txth3lpdagnosiaphc.Name = "txth3lpdagnosiaphc";
            this.txth3lpdagnosiaphc.Properties.MaxLength = 100;
            this.txth3lpdagnosiaphc.Size = new System.Drawing.Size(148, 20);
            this.txth3lpdagnosiaphc.TabIndex = 4;
            // 
            // lblh3lpdagnosiaphc
            // 
            this.lblh3lpdagnosiaphc.AutoSize = true;
            this.lblh3lpdagnosiaphc.Location = new System.Drawing.Point(732, 14);
            this.lblh3lpdagnosiaphc.Name = "lblh3lpdagnosiaphc";
            this.lblh3lpdagnosiaphc.Size = new System.Drawing.Size(81, 13);
            this.lblh3lpdagnosiaphc.TabIndex = 7;
            this.lblh3lpdagnosiaphc.Text = "Agnosia Digital:";
            // 
            // txth3lpdacalculiaphc
            // 
            this.txth3lpdacalculiaphc.Location = new System.Drawing.Point(74, 10);
            this.txth3lpdacalculiaphc.Name = "txth3lpdacalculiaphc";
            this.txth3lpdacalculiaphc.Properties.MaxLength = 100;
            this.txth3lpdacalculiaphc.Size = new System.Drawing.Size(145, 20);
            this.txth3lpdacalculiaphc.TabIndex = 1;
            // 
            // lblh3lpdagrafiaphc
            // 
            this.lblh3lpdagrafiaphc.AutoSize = true;
            this.lblh3lpdagrafiaphc.Location = new System.Drawing.Point(235, 14);
            this.lblh3lpdagrafiaphc.Name = "lblh3lpdagrafiaphc";
            this.lblh3lpdagrafiaphc.Size = new System.Drawing.Size(46, 13);
            this.lblh3lpdagrafiaphc.TabIndex = 2;
            this.lblh3lpdagrafiaphc.Text = "Agrafia:";
            // 
            // txth3lpddesorientacionphc
            // 
            this.txth3lpddesorientacionphc.Location = new System.Drawing.Point(575, 11);
            this.txth3lpddesorientacionphc.Name = "txth3lpddesorientacionphc";
            this.txth3lpddesorientacionphc.Properties.MaxLength = 100;
            this.txth3lpddesorientacionphc.Size = new System.Drawing.Size(145, 20);
            this.txth3lpddesorientacionphc.TabIndex = 3;
            // 
            // lblh3lpddesorientacionphc
            // 
            this.lblh3lpddesorientacionphc.AutoSize = true;
            this.lblh3lpddesorientacionphc.Location = new System.Drawing.Point(450, 14);
            this.lblh3lpddesorientacionphc.Name = "lblh3lpddesorientacionphc";
            this.lblh3lpddesorientacionphc.Size = new System.Drawing.Size(121, 13);
            this.lblh3lpddesorientacionphc.TabIndex = 6;
            this.lblh3lpddesorientacionphc.Text = "Desorientación Der-Izq:";
            // 
            // groupControl6
            // 
            this.groupControl6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl6.Controls.Add(this.groupBox11);
            this.groupControl6.Controls.Add(this.groupBox10);
            this.groupControl6.Controls.Add(this.groupBox9);
            this.groupControl6.Location = new System.Drawing.Point(3, 109);
            this.groupControl6.Name = "groupControl6";
            this.groupControl6.Size = new System.Drawing.Size(975, 307);
            this.groupControl6.TabIndex = 1;
            this.groupControl6.Text = "III. LENGUAJE Y FUNCIONES RELACIONADAS";
            // 
            // groupBox11
            // 
            this.groupBox11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox11.Controls.Add(this.lblh3lesagrafiaphc);
            this.groupBox11.Controls.Add(this.txth3lesagrafiaphc);
            this.groupBox11.Controls.Add(this.lblh3lesalexiaphc);
            this.groupBox11.Controls.Add(this.txth3lesalexiaphc);
            this.groupBox11.Location = new System.Drawing.Point(5, 267);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(965, 35);
            this.groupBox11.TabIndex = 2;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "C. Lenguaje Escrito";
            // 
            // lblh3lesagrafiaphc
            // 
            this.lblh3lesagrafiaphc.AutoSize = true;
            this.lblh3lesagrafiaphc.Location = new System.Drawing.Point(509, 14);
            this.lblh3lesagrafiaphc.Name = "lblh3lesagrafiaphc";
            this.lblh3lesagrafiaphc.Size = new System.Drawing.Size(46, 13);
            this.lblh3lesagrafiaphc.TabIndex = 2;
            this.lblh3lesagrafiaphc.Text = "Agrafia:";
            // 
            // txth3lesagrafiaphc
            // 
            this.txth3lesagrafiaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth3lesagrafiaphc.Location = new System.Drawing.Point(570, 11);
            this.txth3lesagrafiaphc.Name = "txth3lesagrafiaphc";
            this.txth3lesagrafiaphc.Properties.MaxLength = 200;
            this.txth3lesagrafiaphc.Size = new System.Drawing.Size(383, 20);
            this.txth3lesagrafiaphc.TabIndex = 3;
            // 
            // lblh3lesalexiaphc
            // 
            this.lblh3lesalexiaphc.AutoSize = true;
            this.lblh3lesalexiaphc.Location = new System.Drawing.Point(16, 14);
            this.lblh3lesalexiaphc.Name = "lblh3lesalexiaphc";
            this.lblh3lesalexiaphc.Size = new System.Drawing.Size(40, 13);
            this.lblh3lesalexiaphc.TabIndex = 0;
            this.lblh3lesalexiaphc.Text = "Alexia:";
            // 
            // txth3lesalexiaphc
            // 
            this.txth3lesalexiaphc.Location = new System.Drawing.Point(102, 11);
            this.txth3lesalexiaphc.Name = "txth3lesalexiaphc";
            this.txth3lesalexiaphc.Properties.MaxLength = 200;
            this.txth3lesalexiaphc.Size = new System.Drawing.Size(380, 20);
            this.txth3lesalexiaphc.TabIndex = 1;
            // 
            // groupBox10
            // 
            this.groupBox10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox10.Controls.Add(this.groupBox38);
            this.groupBox10.Controls.Add(this.groupBox37);
            this.groupBox10.Controls.Add(this.groupBox36);
            this.groupBox10.Controls.Add(this.groupBox35);
            this.groupBox10.Controls.Add(this.groupBox34);
            this.groupBox10.Location = new System.Drawing.Point(5, 61);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(965, 210);
            this.groupBox10.TabIndex = 1;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "B. Lenguaje Auditivo";
            // 
            // groupBox38
            // 
            this.groupBox38.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox38.Controls.Add(this.lblh3cgeexpresionphc);
            this.groupBox38.Controls.Add(this.txth3cgeexpresionphc);
            this.groupBox38.Controls.Add(this.lblh3cgenominacionphc);
            this.groupBox38.Controls.Add(this.txth3cgenominacionphc);
            this.groupBox38.Controls.Add(this.lblh3cgerepeticionphc);
            this.groupBox38.Controls.Add(this.txth3cgerepeticionphc);
            this.groupBox38.Controls.Add(this.lblh3cgecomprensionphc);
            this.groupBox38.Controls.Add(this.txth3cgecomprensionphc);
            this.groupBox38.Location = new System.Drawing.Point(19, 11);
            this.groupBox38.Name = "groupBox38";
            this.groupBox38.Size = new System.Drawing.Size(940, 56);
            this.groupBox38.TabIndex = 0;
            this.groupBox38.TabStop = false;
            this.groupBox38.Text = "1. Características Generales";
            // 
            // lblh3cgeexpresionphc
            // 
            this.lblh3cgeexpresionphc.AutoSize = true;
            this.lblh3cgeexpresionphc.Location = new System.Drawing.Point(21, 13);
            this.lblh3cgeexpresionphc.Name = "lblh3cgeexpresionphc";
            this.lblh3cgeexpresionphc.Size = new System.Drawing.Size(58, 13);
            this.lblh3cgeexpresionphc.TabIndex = 0;
            this.lblh3cgeexpresionphc.Text = "Expresión:";
            // 
            // txth3cgeexpresionphc
            // 
            this.txth3cgeexpresionphc.EditValue = "";
            this.txth3cgeexpresionphc.Location = new System.Drawing.Point(83, 10);
            this.txth3cgeexpresionphc.Name = "txth3cgeexpresionphc";
            this.txth3cgeexpresionphc.Properties.MaxLength = 100;
            this.txth3cgeexpresionphc.Size = new System.Drawing.Size(380, 20);
            this.txth3cgeexpresionphc.TabIndex = 1;
            // 
            // lblh3cgenominacionphc
            // 
            this.lblh3cgenominacionphc.AutoSize = true;
            this.lblh3cgenominacionphc.Location = new System.Drawing.Point(471, 13);
            this.lblh3cgenominacionphc.Name = "lblh3cgenominacionphc";
            this.lblh3cgenominacionphc.Size = new System.Drawing.Size(65, 13);
            this.lblh3cgenominacionphc.TabIndex = 2;
            this.lblh3cgenominacionphc.Text = "Nominación:";
            // 
            // txth3cgenominacionphc
            // 
            this.txth3cgenominacionphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth3cgenominacionphc.EditValue = "";
            this.txth3cgenominacionphc.Location = new System.Drawing.Point(551, 10);
            this.txth3cgenominacionphc.Name = "txth3cgenominacionphc";
            this.txth3cgenominacionphc.Properties.MaxLength = 100;
            this.txth3cgenominacionphc.Size = new System.Drawing.Size(383, 20);
            this.txth3cgenominacionphc.TabIndex = 3;
            // 
            // lblh3cgerepeticionphc
            // 
            this.lblh3cgerepeticionphc.AutoSize = true;
            this.lblh3cgerepeticionphc.Location = new System.Drawing.Point(21, 35);
            this.lblh3cgerepeticionphc.Name = "lblh3cgerepeticionphc";
            this.lblh3cgerepeticionphc.Size = new System.Drawing.Size(61, 13);
            this.lblh3cgerepeticionphc.TabIndex = 4;
            this.lblh3cgerepeticionphc.Text = "Repetición:";
            // 
            // txth3cgerepeticionphc
            // 
            this.txth3cgerepeticionphc.Location = new System.Drawing.Point(83, 32);
            this.txth3cgerepeticionphc.Name = "txth3cgerepeticionphc";
            this.txth3cgerepeticionphc.Properties.MaxLength = 100;
            this.txth3cgerepeticionphc.Size = new System.Drawing.Size(380, 20);
            this.txth3cgerepeticionphc.TabIndex = 5;
            // 
            // lblh3cgecomprensionphc
            // 
            this.lblh3cgecomprensionphc.AutoSize = true;
            this.lblh3cgecomprensionphc.Location = new System.Drawing.Point(471, 35);
            this.lblh3cgecomprensionphc.Name = "lblh3cgecomprensionphc";
            this.lblh3cgecomprensionphc.Size = new System.Drawing.Size(73, 13);
            this.lblh3cgecomprensionphc.TabIndex = 6;
            this.lblh3cgecomprensionphc.Text = "Comprensión:";
            // 
            // txth3cgecomprensionphc
            // 
            this.txth3cgecomprensionphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth3cgecomprensionphc.Location = new System.Drawing.Point(551, 32);
            this.txth3cgecomprensionphc.Name = "txth3cgecomprensionphc";
            this.txth3cgecomprensionphc.Properties.MaxLength = 100;
            this.txth3cgecomprensionphc.Size = new System.Drawing.Size(383, 20);
            this.txth3cgecomprensionphc.TabIndex = 7;
            // 
            // groupBox37
            // 
            this.groupBox37.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox37.Controls.Add(this.lblh3aprmotoraphc);
            this.groupBox37.Controls.Add(this.txth3aprmotoraphc);
            this.groupBox37.Controls.Add(this.lblh3aprsensorialphc);
            this.groupBox37.Controls.Add(this.txth3aprsensorialphc);
            this.groupBox37.Location = new System.Drawing.Point(19, 171);
            this.groupBox37.Name = "groupBox37";
            this.groupBox37.Size = new System.Drawing.Size(940, 35);
            this.groupBox37.TabIndex = 4;
            this.groupBox37.TabStop = false;
            this.groupBox37.Text = "5. Aprosodias";
            // 
            // lblh3aprmotoraphc
            // 
            this.lblh3aprmotoraphc.AutoSize = true;
            this.lblh3aprmotoraphc.Location = new System.Drawing.Point(21, 14);
            this.lblh3aprmotoraphc.Name = "lblh3aprmotoraphc";
            this.lblh3aprmotoraphc.Size = new System.Drawing.Size(45, 13);
            this.lblh3aprmotoraphc.TabIndex = 0;
            this.lblh3aprmotoraphc.Text = "Motora:";
            // 
            // txth3aprmotoraphc
            // 
            this.txth3aprmotoraphc.Location = new System.Drawing.Point(83, 11);
            this.txth3aprmotoraphc.Name = "txth3aprmotoraphc";
            this.txth3aprmotoraphc.Properties.MaxLength = 200;
            this.txth3aprmotoraphc.Size = new System.Drawing.Size(380, 20);
            this.txth3aprmotoraphc.TabIndex = 1;
            // 
            // lblh3aprsensorialphc
            // 
            this.lblh3aprsensorialphc.AutoSize = true;
            this.lblh3aprsensorialphc.Location = new System.Drawing.Point(490, 14);
            this.lblh3aprsensorialphc.Name = "lblh3aprsensorialphc";
            this.lblh3aprsensorialphc.Size = new System.Drawing.Size(54, 13);
            this.lblh3aprsensorialphc.TabIndex = 2;
            this.lblh3aprsensorialphc.Text = "Sensorial:";
            // 
            // txth3aprsensorialphc
            // 
            this.txth3aprsensorialphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth3aprsensorialphc.Location = new System.Drawing.Point(551, 11);
            this.txth3aprsensorialphc.Name = "txth3aprsensorialphc";
            this.txth3aprsensorialphc.Properties.MaxLength = 200;
            this.txth3aprsensorialphc.Size = new System.Drawing.Size(383, 20);
            this.txth3aprsensorialphc.TabIndex = 3;
            // 
            // groupBox36
            // 
            this.groupBox36.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox36.Controls.Add(this.txth3atacomprensionphc);
            this.groupBox36.Controls.Add(this.lblh3ataexpresionphc);
            this.groupBox36.Controls.Add(this.txth3ataexpresionphc);
            this.groupBox36.Controls.Add(this.lblh3atacomprensionphc);
            this.groupBox36.Controls.Add(this.lblh3atamixtaphc);
            this.groupBox36.Controls.Add(this.txth3atamixtaphc);
            this.groupBox36.Location = new System.Drawing.Point(19, 137);
            this.groupBox36.Name = "groupBox36";
            this.groupBox36.Size = new System.Drawing.Size(940, 35);
            this.groupBox36.TabIndex = 3;
            this.groupBox36.TabStop = false;
            this.groupBox36.Text = "4. Afasia Talámica";
            // 
            // txth3atacomprensionphc
            // 
            this.txth3atacomprensionphc.Location = new System.Drawing.Point(398, 11);
            this.txth3atacomprensionphc.Name = "txth3atacomprensionphc";
            this.txth3atacomprensionphc.Properties.MaxLength = 100;
            this.txth3atacomprensionphc.Size = new System.Drawing.Size(240, 20);
            this.txth3atacomprensionphc.TabIndex = 2;
            // 
            // lblh3ataexpresionphc
            // 
            this.lblh3ataexpresionphc.AutoSize = true;
            this.lblh3ataexpresionphc.Location = new System.Drawing.Point(21, 14);
            this.lblh3ataexpresionphc.Name = "lblh3ataexpresionphc";
            this.lblh3ataexpresionphc.Size = new System.Drawing.Size(58, 13);
            this.lblh3ataexpresionphc.TabIndex = 0;
            this.lblh3ataexpresionphc.Text = "Expresión:";
            // 
            // txth3ataexpresionphc
            // 
            this.txth3ataexpresionphc.Location = new System.Drawing.Point(83, 11);
            this.txth3ataexpresionphc.Name = "txth3ataexpresionphc";
            this.txth3ataexpresionphc.Properties.MaxLength = 100;
            this.txth3ataexpresionphc.Size = new System.Drawing.Size(240, 20);
            this.txth3ataexpresionphc.TabIndex = 1;
            // 
            // lblh3atacomprensionphc
            // 
            this.lblh3atacomprensionphc.AutoSize = true;
            this.lblh3atacomprensionphc.Location = new System.Drawing.Point(330, 14);
            this.lblh3atacomprensionphc.Name = "lblh3atacomprensionphc";
            this.lblh3atacomprensionphc.Size = new System.Drawing.Size(73, 13);
            this.lblh3atacomprensionphc.TabIndex = 3;
            this.lblh3atacomprensionphc.Text = "Comprensión:";
            // 
            // lblh3atamixtaphc
            // 
            this.lblh3atamixtaphc.AutoSize = true;
            this.lblh3atamixtaphc.Location = new System.Drawing.Point(645, 14);
            this.lblh3atamixtaphc.Name = "lblh3atamixtaphc";
            this.lblh3atamixtaphc.Size = new System.Drawing.Size(37, 13);
            this.lblh3atamixtaphc.TabIndex = 4;
            this.lblh3atamixtaphc.Text = "Mixta:";
            // 
            // txth3atamixtaphc
            // 
            this.txth3atamixtaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth3atamixtaphc.Location = new System.Drawing.Point(691, 11);
            this.txth3atamixtaphc.Name = "txth3atamixtaphc";
            this.txth3atamixtaphc.Properties.MaxLength = 100;
            this.txth3atamixtaphc.Size = new System.Drawing.Size(243, 20);
            this.txth3atamixtaphc.TabIndex = 5;
            // 
            // groupBox35
            // 
            this.groupBox35.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox35.Controls.Add(this.txth3atrmotoraphc);
            this.groupBox35.Controls.Add(this.lblh3atrmotoraphc);
            this.groupBox35.Controls.Add(this.lblh3atrsensitivaphc);
            this.groupBox35.Controls.Add(this.txth3atrsensitivaphc);
            this.groupBox35.Controls.Add(this.lblh3atrmixtaphc);
            this.groupBox35.Controls.Add(this.txth3atrmixtaphc);
            this.groupBox35.Location = new System.Drawing.Point(19, 103);
            this.groupBox35.Name = "groupBox35";
            this.groupBox35.Size = new System.Drawing.Size(940, 35);
            this.groupBox35.TabIndex = 2;
            this.groupBox35.TabStop = false;
            this.groupBox35.Text = "3. Afasias Transcorticales";
            // 
            // txth3atrmotoraphc
            // 
            this.txth3atrmotoraphc.Location = new System.Drawing.Point(83, 11);
            this.txth3atrmotoraphc.Name = "txth3atrmotoraphc";
            this.txth3atrmotoraphc.Properties.MaxLength = 100;
            this.txth3atrmotoraphc.Size = new System.Drawing.Size(240, 20);
            this.txth3atrmotoraphc.TabIndex = 1;
            // 
            // lblh3atrmotoraphc
            // 
            this.lblh3atrmotoraphc.AutoSize = true;
            this.lblh3atrmotoraphc.Location = new System.Drawing.Point(21, 14);
            this.lblh3atrmotoraphc.Name = "lblh3atrmotoraphc";
            this.lblh3atrmotoraphc.Size = new System.Drawing.Size(45, 13);
            this.lblh3atrmotoraphc.TabIndex = 0;
            this.lblh3atrmotoraphc.Text = "Motora:";
            // 
            // lblh3atrsensitivaphc
            // 
            this.lblh3atrsensitivaphc.AutoSize = true;
            this.lblh3atrsensitivaphc.Location = new System.Drawing.Point(330, 14);
            this.lblh3atrsensitivaphc.Name = "lblh3atrsensitivaphc";
            this.lblh3atrsensitivaphc.Size = new System.Drawing.Size(54, 13);
            this.lblh3atrsensitivaphc.TabIndex = 2;
            this.lblh3atrsensitivaphc.Text = "Sensitiva:";
            // 
            // txth3atrsensitivaphc
            // 
            this.txth3atrsensitivaphc.Location = new System.Drawing.Point(398, 11);
            this.txth3atrsensitivaphc.Name = "txth3atrsensitivaphc";
            this.txth3atrsensitivaphc.Properties.MaxLength = 100;
            this.txth3atrsensitivaphc.Size = new System.Drawing.Size(240, 20);
            this.txth3atrsensitivaphc.TabIndex = 3;
            // 
            // lblh3atrmixtaphc
            // 
            this.lblh3atrmixtaphc.AutoSize = true;
            this.lblh3atrmixtaphc.Location = new System.Drawing.Point(645, 14);
            this.lblh3atrmixtaphc.Name = "lblh3atrmixtaphc";
            this.lblh3atrmixtaphc.Size = new System.Drawing.Size(37, 13);
            this.lblh3atrmixtaphc.TabIndex = 4;
            this.lblh3atrmixtaphc.Text = "Mixta:";
            // 
            // txth3atrmixtaphc
            // 
            this.txth3atrmixtaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth3atrmixtaphc.Location = new System.Drawing.Point(691, 11);
            this.txth3atrmixtaphc.Name = "txth3atrmixtaphc";
            this.txth3atrmixtaphc.Properties.MaxLength = 100;
            this.txth3atrmixtaphc.Size = new System.Drawing.Size(243, 20);
            this.txth3atrmixtaphc.TabIndex = 5;
            // 
            // groupBox34
            // 
            this.groupBox34.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox34.Controls.Add(this.txth3apebrocaphc);
            this.groupBox34.Controls.Add(this.txth3apeconduccionphc);
            this.groupBox34.Controls.Add(this.txth3apewernickephc);
            this.groupBox34.Controls.Add(this.txth3apeglobalphc);
            this.groupBox34.Controls.Add(this.txth3apeanomiaphc);
            this.groupBox34.Controls.Add(this.lblh3apebrocaphc);
            this.groupBox34.Controls.Add(this.lblh3apeconduccionphc);
            this.groupBox34.Controls.Add(this.lblh3apewernickephc);
            this.groupBox34.Controls.Add(this.lblh3apeglobalphc);
            this.groupBox34.Controls.Add(this.lblh3apeanomiaphc);
            this.groupBox34.Location = new System.Drawing.Point(19, 69);
            this.groupBox34.Name = "groupBox34";
            this.groupBox34.Size = new System.Drawing.Size(940, 35);
            this.groupBox34.TabIndex = 1;
            this.groupBox34.TabStop = false;
            this.groupBox34.Text = "2. Afasias Perisylvianas";
            // 
            // txth3apebrocaphc
            // 
            this.txth3apebrocaphc.Location = new System.Drawing.Point(58, 11);
            this.txth3apebrocaphc.Name = "txth3apebrocaphc";
            this.txth3apebrocaphc.Properties.MaxLength = 100;
            this.txth3apebrocaphc.Size = new System.Drawing.Size(130, 20);
            this.txth3apebrocaphc.TabIndex = 0;
            // 
            // txth3apeconduccionphc
            // 
            this.txth3apeconduccionphc.Location = new System.Drawing.Point(255, 11);
            this.txth3apeconduccionphc.Name = "txth3apeconduccionphc";
            this.txth3apeconduccionphc.Properties.MaxLength = 100;
            this.txth3apeconduccionphc.Size = new System.Drawing.Size(130, 20);
            this.txth3apeconduccionphc.TabIndex = 2;
            // 
            // txth3apewernickephc
            // 
            this.txth3apewernickephc.Location = new System.Drawing.Point(441, 11);
            this.txth3apewernickephc.Name = "txth3apewernickephc";
            this.txth3apewernickephc.Properties.MaxLength = 100;
            this.txth3apewernickephc.Size = new System.Drawing.Size(130, 20);
            this.txth3apewernickephc.TabIndex = 4;
            // 
            // txth3apeglobalphc
            // 
            this.txth3apeglobalphc.Location = new System.Drawing.Point(611, 11);
            this.txth3apeglobalphc.Name = "txth3apeglobalphc";
            this.txth3apeglobalphc.Properties.MaxLength = 100;
            this.txth3apeglobalphc.Size = new System.Drawing.Size(130, 20);
            this.txth3apeglobalphc.TabIndex = 6;
            // 
            // txth3apeanomiaphc
            // 
            this.txth3apeanomiaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth3apeanomiaphc.Location = new System.Drawing.Point(801, 11);
            this.txth3apeanomiaphc.Name = "txth3apeanomiaphc";
            this.txth3apeanomiaphc.Properties.MaxLength = 100;
            this.txth3apeanomiaphc.Size = new System.Drawing.Size(133, 20);
            this.txth3apeanomiaphc.TabIndex = 9;
            // 
            // lblh3apebrocaphc
            // 
            this.lblh3apebrocaphc.AutoSize = true;
            this.lblh3apebrocaphc.Location = new System.Drawing.Point(21, 14);
            this.lblh3apebrocaphc.Name = "lblh3apebrocaphc";
            this.lblh3apebrocaphc.Size = new System.Drawing.Size(38, 13);
            this.lblh3apebrocaphc.TabIndex = 1;
            this.lblh3apebrocaphc.Text = "Broca:";
            // 
            // lblh3apeconduccionphc
            // 
            this.lblh3apeconduccionphc.AutoSize = true;
            this.lblh3apeconduccionphc.Location = new System.Drawing.Point(190, 14);
            this.lblh3apeconduccionphc.Name = "lblh3apeconduccionphc";
            this.lblh3apeconduccionphc.Size = new System.Drawing.Size(66, 13);
            this.lblh3apeconduccionphc.TabIndex = 3;
            this.lblh3apeconduccionphc.Text = "Conducción:";
            // 
            // lblh3apewernickephc
            // 
            this.lblh3apewernickephc.AutoSize = true;
            this.lblh3apewernickephc.Location = new System.Drawing.Point(387, 14);
            this.lblh3apewernickephc.Name = "lblh3apewernickephc";
            this.lblh3apewernickephc.Size = new System.Drawing.Size(55, 13);
            this.lblh3apewernickephc.TabIndex = 5;
            this.lblh3apewernickephc.Text = "Wernicke:";
            // 
            // lblh3apeglobalphc
            // 
            this.lblh3apeglobalphc.AutoSize = true;
            this.lblh3apeglobalphc.Location = new System.Drawing.Point(572, 14);
            this.lblh3apeglobalphc.Name = "lblh3apeglobalphc";
            this.lblh3apeglobalphc.Size = new System.Drawing.Size(40, 13);
            this.lblh3apeglobalphc.TabIndex = 7;
            this.lblh3apeglobalphc.Text = "Global:";
            // 
            // lblh3apeanomiaphc
            // 
            this.lblh3apeanomiaphc.AutoSize = true;
            this.lblh3apeanomiaphc.Location = new System.Drawing.Point(752, 14);
            this.lblh3apeanomiaphc.Name = "lblh3apeanomiaphc";
            this.lblh3apeanomiaphc.Size = new System.Drawing.Size(46, 13);
            this.lblh3apeanomiaphc.TabIndex = 8;
            this.lblh3apeanomiaphc.Text = "Anomia:";
            // 
            // groupBox9
            // 
            this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox9.Controls.Add(this.lblh3hdoizquierdophc);
            this.groupBox9.Controls.Add(this.txth3hdoizquierdophc);
            this.groupBox9.Controls.Add(this.lblh3hdoderechophc);
            this.groupBox9.Controls.Add(this.txth3hdoderechophc);
            this.groupBox9.Location = new System.Drawing.Point(5, 22);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(965, 35);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "A. Hemisferio Dominante";
            // 
            // lblh3hdoizquierdophc
            // 
            this.lblh3hdoizquierdophc.AutoSize = true;
            this.lblh3hdoizquierdophc.Location = new System.Drawing.Point(16, 15);
            this.lblh3hdoizquierdophc.Name = "lblh3hdoizquierdophc";
            this.lblh3hdoizquierdophc.Size = new System.Drawing.Size(56, 13);
            this.lblh3hdoizquierdophc.TabIndex = 0;
            this.lblh3hdoizquierdophc.Text = "Izquierdo:";
            // 
            // txth3hdoizquierdophc
            // 
            this.txth3hdoizquierdophc.Location = new System.Drawing.Point(75, 12);
            this.txth3hdoizquierdophc.Name = "txth3hdoizquierdophc";
            this.txth3hdoizquierdophc.Properties.MaxLength = 100;
            this.txth3hdoizquierdophc.Size = new System.Drawing.Size(390, 20);
            this.txth3hdoizquierdophc.TabIndex = 1;
            // 
            // lblh3hdoderechophc
            // 
            this.lblh3hdoderechophc.AutoSize = true;
            this.lblh3hdoderechophc.Location = new System.Drawing.Point(509, 15);
            this.lblh3hdoderechophc.Name = "lblh3hdoderechophc";
            this.lblh3hdoderechophc.Size = new System.Drawing.Size(51, 13);
            this.lblh3hdoderechophc.TabIndex = 2;
            this.lblh3hdoderechophc.Text = "Derecho:";
            // 
            // txth3hdoderechophc
            // 
            this.txth3hdoderechophc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth3hdoderechophc.Location = new System.Drawing.Point(566, 12);
            this.txth3hdoderechophc.Name = "txth3hdoderechophc";
            this.txth3hdoderechophc.Properties.MaxLength = 100;
            this.txth3hdoderechophc.Size = new System.Drawing.Size(387, 20);
            this.txth3hdoderechophc.TabIndex = 3;
            // 
            // groupControl5
            // 
            this.groupControl5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl5.Controls.Add(this.groupBox8);
            this.groupControl5.Controls.Add(this.groupBox7);
            this.groupControl5.Location = new System.Drawing.Point(3, 4);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(975, 99);
            this.groupControl5.TabIndex = 0;
            this.groupControl5.Text = "II: MEMORIA Y AFECTO";
            // 
            // groupBox8
            // 
            this.groupBox8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox8.Controls.Add(this.lblh3conrecientephc);
            this.groupBox8.Controls.Add(this.txth3conrecientephc);
            this.groupBox8.Controls.Add(this.lblh3conremotaphc);
            this.groupBox8.Controls.Add(this.txth3conremotaphc);
            this.groupBox8.Location = new System.Drawing.Point(5, 23);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(965, 35);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "A. Estado de Conciencia";
            // 
            // lblh3conrecientephc
            // 
            this.lblh3conrecientephc.AutoSize = true;
            this.lblh3conrecientephc.Location = new System.Drawing.Point(16, 15);
            this.lblh3conrecientephc.Name = "lblh3conrecientephc";
            this.lblh3conrecientephc.Size = new System.Drawing.Size(53, 13);
            this.lblh3conrecientephc.TabIndex = 0;
            this.lblh3conrecientephc.Text = "Reciente:";
            // 
            // txth3conrecientephc
            // 
            this.txth3conrecientephc.Location = new System.Drawing.Point(75, 12);
            this.txth3conrecientephc.Name = "txth3conrecientephc";
            this.txth3conrecientephc.Properties.MaxLength = 100;
            this.txth3conrecientephc.Size = new System.Drawing.Size(390, 20);
            this.txth3conrecientephc.TabIndex = 1;
            // 
            // lblh3conremotaphc
            // 
            this.lblh3conremotaphc.AutoSize = true;
            this.lblh3conremotaphc.Location = new System.Drawing.Point(512, 15);
            this.lblh3conremotaphc.Name = "lblh3conremotaphc";
            this.lblh3conremotaphc.Size = new System.Drawing.Size(48, 13);
            this.lblh3conremotaphc.TabIndex = 2;
            this.lblh3conremotaphc.Text = "Remota:";
            // 
            // txth3conremotaphc
            // 
            this.txth3conremotaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth3conremotaphc.Location = new System.Drawing.Point(566, 12);
            this.txth3conremotaphc.Name = "txth3conremotaphc";
            this.txth3conremotaphc.Properties.MaxLength = 100;
            this.txth3conremotaphc.Size = new System.Drawing.Size(393, 20);
            this.txth3conremotaphc.TabIndex = 3;
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.txth3afeplanophc);
            this.groupBox7.Controls.Add(this.txth3afeeuforicophc);
            this.groupBox7.Controls.Add(this.txth3afeinadecuadophc);
            this.groupBox7.Controls.Add(this.txth3afehostilphc);
            this.groupBox7.Controls.Add(this.txth3afelabilphc);
            this.groupBox7.Controls.Add(this.lblh3afeplanophc);
            this.groupBox7.Controls.Add(this.txth3afedeprimidophc);
            this.groupBox7.Controls.Add(this.lblh3afeeuforicophc);
            this.groupBox7.Controls.Add(this.lblh3afedeprimidophc);
            this.groupBox7.Controls.Add(this.lblh3afelabilphc);
            this.groupBox7.Controls.Add(this.lblh3afehostilphc);
            this.groupBox7.Controls.Add(this.lblh3afeinadecuadophc);
            this.groupBox7.Location = new System.Drawing.Point(5, 59);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(965, 35);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "B. Afecto";
            // 
            // txth3afeplanophc
            // 
            this.txth3afeplanophc.Location = new System.Drawing.Point(211, 11);
            this.txth3afeplanophc.Name = "txth3afeplanophc";
            this.txth3afeplanophc.Properties.MaxLength = 100;
            this.txth3afeplanophc.Size = new System.Drawing.Size(110, 20);
            this.txth3afeplanophc.TabIndex = 2;
            // 
            // txth3afeeuforicophc
            // 
            this.txth3afeeuforicophc.Location = new System.Drawing.Point(64, 11);
            this.txth3afeeuforicophc.Name = "txth3afeeuforicophc";
            this.txth3afeeuforicophc.Properties.MaxLength = 100;
            this.txth3afeeuforicophc.Size = new System.Drawing.Size(110, 20);
            this.txth3afeeuforicophc.TabIndex = 0;
            // 
            // txth3afeinadecuadophc
            // 
            this.txth3afeinadecuadophc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth3afeinadecuadophc.Location = new System.Drawing.Point(846, 11);
            this.txth3afeinadecuadophc.Name = "txth3afeinadecuadophc";
            this.txth3afeinadecuadophc.Properties.MaxLength = 100;
            this.txth3afeinadecuadophc.Size = new System.Drawing.Size(113, 20);
            this.txth3afeinadecuadophc.TabIndex = 10;
            // 
            // txth3afehostilphc
            // 
            this.txth3afehostilphc.Location = new System.Drawing.Point(668, 11);
            this.txth3afehostilphc.Name = "txth3afehostilphc";
            this.txth3afehostilphc.Properties.MaxLength = 100;
            this.txth3afehostilphc.Size = new System.Drawing.Size(110, 20);
            this.txth3afehostilphc.TabIndex = 8;
            // 
            // txth3afelabilphc
            // 
            this.txth3afelabilphc.Location = new System.Drawing.Point(521, 11);
            this.txth3afelabilphc.Name = "txth3afelabilphc";
            this.txth3afelabilphc.Properties.MaxLength = 100;
            this.txth3afelabilphc.Size = new System.Drawing.Size(110, 20);
            this.txth3afelabilphc.TabIndex = 6;
            // 
            // lblh3afeplanophc
            // 
            this.lblh3afeplanophc.AutoSize = true;
            this.lblh3afeplanophc.Location = new System.Drawing.Point(176, 14);
            this.lblh3afeplanophc.Name = "lblh3afeplanophc";
            this.lblh3afeplanophc.Size = new System.Drawing.Size(37, 13);
            this.lblh3afeplanophc.TabIndex = 3;
            this.lblh3afeplanophc.Text = "Plano:";
            // 
            // txth3afedeprimidophc
            // 
            this.txth3afedeprimidophc.Location = new System.Drawing.Point(378, 11);
            this.txth3afedeprimidophc.Name = "txth3afedeprimidophc";
            this.txth3afedeprimidophc.Properties.MaxLength = 100;
            this.txth3afedeprimidophc.Size = new System.Drawing.Size(110, 20);
            this.txth3afedeprimidophc.TabIndex = 4;
            // 
            // lblh3afeeuforicophc
            // 
            this.lblh3afeeuforicophc.AutoSize = true;
            this.lblh3afeeuforicophc.Location = new System.Drawing.Point(16, 14);
            this.lblh3afeeuforicophc.Name = "lblh3afeeuforicophc";
            this.lblh3afeeuforicophc.Size = new System.Drawing.Size(50, 13);
            this.lblh3afeeuforicophc.TabIndex = 1;
            this.lblh3afeeuforicophc.Text = "Eufórico:";
            // 
            // lblh3afedeprimidophc
            // 
            this.lblh3afedeprimidophc.AutoSize = true;
            this.lblh3afedeprimidophc.Location = new System.Drawing.Point(323, 14);
            this.lblh3afedeprimidophc.Name = "lblh3afedeprimidophc";
            this.lblh3afedeprimidophc.Size = new System.Drawing.Size(58, 13);
            this.lblh3afedeprimidophc.TabIndex = 5;
            this.lblh3afedeprimidophc.Text = "Deprimido:";
            // 
            // lblh3afelabilphc
            // 
            this.lblh3afelabilphc.AutoSize = true;
            this.lblh3afelabilphc.Location = new System.Drawing.Point(490, 14);
            this.lblh3afelabilphc.Name = "lblh3afelabilphc";
            this.lblh3afelabilphc.Size = new System.Drawing.Size(32, 13);
            this.lblh3afelabilphc.TabIndex = 7;
            this.lblh3afelabilphc.Text = "Lábil:";
            // 
            // lblh3afehostilphc
            // 
            this.lblh3afehostilphc.AutoSize = true;
            this.lblh3afehostilphc.Location = new System.Drawing.Point(633, 14);
            this.lblh3afehostilphc.Name = "lblh3afehostilphc";
            this.lblh3afehostilphc.Size = new System.Drawing.Size(37, 13);
            this.lblh3afehostilphc.TabIndex = 9;
            this.lblh3afehostilphc.Text = "Hostíl:";
            // 
            // lblh3afeinadecuadophc
            // 
            this.lblh3afeinadecuadophc.AutoSize = true;
            this.lblh3afeinadecuadophc.Location = new System.Drawing.Point(780, 14);
            this.lblh3afeinadecuadophc.Name = "lblh3afeinadecuadophc";
            this.lblh3afeinadecuadophc.Size = new System.Drawing.Size(68, 13);
            this.lblh3afeinadecuadophc.TabIndex = 11;
            this.lblh3afeinadecuadophc.Text = "Inadecuado:";
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.xtraScrollableControl1);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            this.xtraTabPage2.Size = new System.Drawing.Size(982, 594);
            this.xtraTabPage2.TabPageWidth = 70;
            this.xtraTabPage2.Text = "Hoja 2";
            // 
            // xtraScrollableControl1
            // 
            this.xtraScrollableControl1.Controls.Add(this.groupControl3);
            this.xtraScrollableControl1.Controls.Add(this.groupControl2);
            this.xtraScrollableControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl1.Name = "xtraScrollableControl1";
            this.xtraScrollableControl1.Size = new System.Drawing.Size(982, 594);
            this.xtraScrollableControl1.TabIndex = 0;
            // 
            // groupControl3
            // 
            this.groupControl3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl3.Controls.Add(this.groupControl4);
            this.groupControl3.Location = new System.Drawing.Point(3, 200);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(975, 390);
            this.groupControl3.TabIndex = 1;
            this.groupControl3.Text = "Examen Neurológico  -  Funciones Cognoscitivas";
            // 
            // groupControl4
            // 
            this.groupControl4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl4.Controls.Add(this.groupBox6);
            this.groupControl4.Controls.Add(this.groupBox5);
            this.groupControl4.Controls.Add(this.groupBox4);
            this.groupControl4.Controls.Add(this.groupBox3);
            this.groupControl4.Controls.Add(this.groupBox2);
            this.groupControl4.Controls.Add(this.groupBox1);
            this.groupControl4.Location = new System.Drawing.Point(5, 26);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(965, 359);
            this.groupControl4.TabIndex = 0;
            this.groupControl4.Text = "I. Funciones Intelectuales Generales";
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.groupControl22);
            this.groupBox6.Controls.Add(this.groupControl21);
            this.groupBox6.Location = new System.Drawing.Point(473, 156);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(487, 198);
            this.groupBox6.TabIndex = 5;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "F. Percepción";
            // 
            // groupControl22
            // 
            this.groupControl22.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl22.Controls.Add(this.txth2paedelusionesphc);
            this.groupControl22.Controls.Add(this.lblh2paeilsuionesphc);
            this.groupControl22.Controls.Add(this.lblh2paedelusionesphc);
            this.groupControl22.Controls.Add(this.txth2paeilsuionesphc);
            this.groupControl22.Controls.Add(this.lblh2paealucinacionesphc);
            this.groupControl22.Controls.Add(this.txth2paealucinacionesphc);
            this.groupControl22.Location = new System.Drawing.Point(6, 94);
            this.groupControl22.Name = "groupControl22";
            this.groupControl22.Size = new System.Drawing.Size(475, 98);
            this.groupControl22.TabIndex = 1;
            this.groupControl22.Text = "2. Alteraciones Específicas";
            // 
            // txth2paedelusionesphc
            // 
            this.txth2paedelusionesphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth2paedelusionesphc.Location = new System.Drawing.Point(129, 25);
            this.txth2paedelusionesphc.Name = "txth2paedelusionesphc";
            this.txth2paedelusionesphc.Properties.MaxLength = 100;
            this.txth2paedelusionesphc.Size = new System.Drawing.Size(337, 20);
            this.txth2paedelusionesphc.TabIndex = 1;
            // 
            // lblh2paeilsuionesphc
            // 
            this.lblh2paeilsuionesphc.AutoSize = true;
            this.lblh2paeilsuionesphc.Location = new System.Drawing.Point(10, 52);
            this.lblh2paeilsuionesphc.Name = "lblh2paeilsuionesphc";
            this.lblh2paeilsuionesphc.Size = new System.Drawing.Size(53, 13);
            this.lblh2paeilsuionesphc.TabIndex = 2;
            this.lblh2paeilsuionesphc.Text = "Ilusiones:";
            // 
            // lblh2paedelusionesphc
            // 
            this.lblh2paedelusionesphc.AutoSize = true;
            this.lblh2paedelusionesphc.Location = new System.Drawing.Point(10, 28);
            this.lblh2paedelusionesphc.Name = "lblh2paedelusionesphc";
            this.lblh2paedelusionesphc.Size = new System.Drawing.Size(62, 13);
            this.lblh2paedelusionesphc.TabIndex = 0;
            this.lblh2paedelusionesphc.Text = "Delusiones:";
            // 
            // txth2paeilsuionesphc
            // 
            this.txth2paeilsuionesphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth2paeilsuionesphc.Location = new System.Drawing.Point(129, 49);
            this.txth2paeilsuionesphc.Name = "txth2paeilsuionesphc";
            this.txth2paeilsuionesphc.Properties.MaxLength = 100;
            this.txth2paeilsuionesphc.Size = new System.Drawing.Size(337, 20);
            this.txth2paeilsuionesphc.TabIndex = 3;
            // 
            // lblh2paealucinacionesphc
            // 
            this.lblh2paealucinacionesphc.AutoSize = true;
            this.lblh2paealucinacionesphc.Location = new System.Drawing.Point(10, 76);
            this.lblh2paealucinacionesphc.Name = "lblh2paealucinacionesphc";
            this.lblh2paealucinacionesphc.Size = new System.Drawing.Size(75, 13);
            this.lblh2paealucinacionesphc.TabIndex = 4;
            this.lblh2paealucinacionesphc.Text = "Alucinaciones:";
            // 
            // txth2paealucinacionesphc
            // 
            this.txth2paealucinacionesphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth2paealucinacionesphc.Location = new System.Drawing.Point(129, 73);
            this.txth2paealucinacionesphc.Name = "txth2paealucinacionesphc";
            this.txth2paealucinacionesphc.Properties.MaxLength = 100;
            this.txth2paealucinacionesphc.Size = new System.Drawing.Size(337, 20);
            this.txth2paealucinacionesphc.TabIndex = 5;
            // 
            // groupControl21
            // 
            this.groupControl21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl21.Controls.Add(this.txth2pagconfusionphc);
            this.groupControl21.Controls.Add(this.lblh2pagconfusionphc);
            this.groupControl21.Controls.Add(this.lblh2pagdeliriophc);
            this.groupControl21.Controls.Add(this.txth2pagdeliriophc);
            this.groupControl21.Location = new System.Drawing.Point(6, 15);
            this.groupControl21.Name = "groupControl21";
            this.groupControl21.Size = new System.Drawing.Size(475, 74);
            this.groupControl21.TabIndex = 0;
            this.groupControl21.Text = "1. Alteraciones Generalizadas";
            // 
            // txth2pagconfusionphc
            // 
            this.txth2pagconfusionphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth2pagconfusionphc.Location = new System.Drawing.Point(129, 25);
            this.txth2pagconfusionphc.Name = "txth2pagconfusionphc";
            this.txth2pagconfusionphc.Properties.MaxLength = 100;
            this.txth2pagconfusionphc.Size = new System.Drawing.Size(337, 20);
            this.txth2pagconfusionphc.TabIndex = 0;
            // 
            // lblh2pagconfusionphc
            // 
            this.lblh2pagconfusionphc.AutoSize = true;
            this.lblh2pagconfusionphc.Location = new System.Drawing.Point(10, 28);
            this.lblh2pagconfusionphc.Name = "lblh2pagconfusionphc";
            this.lblh2pagconfusionphc.Size = new System.Drawing.Size(124, 13);
            this.lblh2pagconfusionphc.TabIndex = 1;
            this.lblh2pagconfusionphc.Text = "Confusión Generalizada:";
            // 
            // lblh2pagdeliriophc
            // 
            this.lblh2pagdeliriophc.AutoSize = true;
            this.lblh2pagdeliriophc.Location = new System.Drawing.Point(10, 51);
            this.lblh2pagdeliriophc.Name = "lblh2pagdeliriophc";
            this.lblh2pagdeliriophc.Size = new System.Drawing.Size(40, 13);
            this.lblh2pagdeliriophc.TabIndex = 2;
            this.lblh2pagdeliriophc.Text = "Delirio:";
            // 
            // txth2pagdeliriophc
            // 
            this.txth2pagdeliriophc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth2pagdeliriophc.Location = new System.Drawing.Point(129, 48);
            this.txth2pagdeliriophc.Name = "txth2pagdeliriophc";
            this.txth2pagdeliriophc.Properties.MaxLength = 100;
            this.txth2pagdeliriophc.Size = new System.Drawing.Size(337, 20);
            this.txth2pagdeliriophc.TabIndex = 3;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.txth2atgdesatentophc);
            this.groupBox5.Controls.Add(this.lblh2atgdesatentophc);
            this.groupBox5.Controls.Add(this.txth2atgnegligentephc);
            this.groupBox5.Controls.Add(this.lblh2atgnegligentephc);
            this.groupBox5.Controls.Add(this.txth2atgnegativophc);
            this.groupBox5.Controls.Add(this.lblh2atgnegativophc);
            this.groupBox5.Controls.Add(this.txth2atgfluctuantephc);
            this.groupBox5.Controls.Add(this.lblh2atgfluctuantephc);
            this.groupBox5.Location = new System.Drawing.Point(5, 244);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(462, 110);
            this.groupBox5.TabIndex = 4;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "E. Atención Global";
            // 
            // txth2atgdesatentophc
            // 
            this.txth2atgdesatentophc.Location = new System.Drawing.Point(81, 15);
            this.txth2atgdesatentophc.Name = "txth2atgdesatentophc";
            this.txth2atgdesatentophc.Properties.MaxLength = 100;
            this.txth2atgdesatentophc.Size = new System.Drawing.Size(372, 20);
            this.txth2atgdesatentophc.TabIndex = 1;
            // 
            // lblh2atgdesatentophc
            // 
            this.lblh2atgdesatentophc.AutoSize = true;
            this.lblh2atgdesatentophc.Location = new System.Drawing.Point(14, 18);
            this.lblh2atgdesatentophc.Name = "lblh2atgdesatentophc";
            this.lblh2atgdesatentophc.Size = new System.Drawing.Size(61, 13);
            this.lblh2atgdesatentophc.TabIndex = 0;
            this.lblh2atgdesatentophc.Text = "Desatento:";
            // 
            // txth2atgnegligentephc
            // 
            this.txth2atgnegligentephc.Location = new System.Drawing.Point(81, 81);
            this.txth2atgnegligentephc.Name = "txth2atgnegligentephc";
            this.txth2atgnegligentephc.Properties.MaxLength = 100;
            this.txth2atgnegligentephc.Size = new System.Drawing.Size(372, 20);
            this.txth2atgnegligentephc.TabIndex = 7;
            // 
            // lblh2atgnegligentephc
            // 
            this.lblh2atgnegligentephc.AutoSize = true;
            this.lblh2atgnegligentephc.Location = new System.Drawing.Point(14, 84);
            this.lblh2atgnegligentephc.Name = "lblh2atgnegligentephc";
            this.lblh2atgnegligentephc.Size = new System.Drawing.Size(62, 13);
            this.lblh2atgnegligentephc.TabIndex = 6;
            this.lblh2atgnegligentephc.Text = "Negligente:";
            // 
            // txth2atgnegativophc
            // 
            this.txth2atgnegativophc.Location = new System.Drawing.Point(81, 59);
            this.txth2atgnegativophc.Name = "txth2atgnegativophc";
            this.txth2atgnegativophc.Properties.MaxLength = 100;
            this.txth2atgnegativophc.Size = new System.Drawing.Size(372, 20);
            this.txth2atgnegativophc.TabIndex = 5;
            // 
            // lblh2atgnegativophc
            // 
            this.lblh2atgnegativophc.AutoSize = true;
            this.lblh2atgnegativophc.Location = new System.Drawing.Point(14, 62);
            this.lblh2atgnegativophc.Name = "lblh2atgnegativophc";
            this.lblh2atgnegativophc.Size = new System.Drawing.Size(54, 13);
            this.lblh2atgnegativophc.TabIndex = 4;
            this.lblh2atgnegativophc.Text = "Negativo:";
            // 
            // txth2atgfluctuantephc
            // 
            this.txth2atgfluctuantephc.Location = new System.Drawing.Point(81, 37);
            this.txth2atgfluctuantephc.Name = "txth2atgfluctuantephc";
            this.txth2atgfluctuantephc.Properties.MaxLength = 100;
            this.txth2atgfluctuantephc.Size = new System.Drawing.Size(372, 20);
            this.txth2atgfluctuantephc.TabIndex = 3;
            // 
            // lblh2atgfluctuantephc
            // 
            this.lblh2atgfluctuantephc.AutoSize = true;
            this.lblh2atgfluctuantephc.Location = new System.Drawing.Point(14, 40);
            this.lblh2atgfluctuantephc.Name = "lblh2atgfluctuantephc";
            this.lblh2atgfluctuantephc.Size = new System.Drawing.Size(62, 13);
            this.lblh2atgfluctuantephc.TabIndex = 2;
            this.lblh2atgfluctuantephc.Text = "Fluctuante:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txth2conocimientosgenerales);
            this.groupBox4.Location = new System.Drawing.Point(5, 156);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(462, 86);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "D. Conocimientos Generales";
            // 
            // txth2conocimientosgenerales
            // 
            this.txth2conocimientosgenerales.EditValue = "";
            this.txth2conocimientosgenerales.Location = new System.Drawing.Point(6, 18);
            this.txth2conocimientosgenerales.Name = "txth2conocimientosgenerales";
            this.txth2conocimientosgenerales.Properties.MaxLength = 5000;
            this.txth2conocimientosgenerales.Size = new System.Drawing.Size(447, 62);
            this.txth2conocimientosgenerales.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.txth2razideaphc);
            this.groupBox3.Controls.Add(this.lblh2razideaphc);
            this.groupBox3.Controls.Add(this.txth2razjuiciophc);
            this.groupBox3.Controls.Add(this.lblh2razjuiciophc);
            this.groupBox3.Controls.Add(this.lblh2razabstraccionphc);
            this.groupBox3.Controls.Add(this.txth2razabstraccionphc);
            this.groupBox3.Location = new System.Drawing.Point(473, 69);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(487, 85);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "C. Razonamiento";
            // 
            // txth2razideaphc
            // 
            this.txth2razideaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth2razideaphc.Location = new System.Drawing.Point(84, 14);
            this.txth2razideaphc.Name = "txth2razideaphc";
            this.txth2razideaphc.Properties.MaxLength = 100;
            this.txth2razideaphc.Size = new System.Drawing.Size(388, 20);
            this.txth2razideaphc.TabIndex = 1;
            // 
            // lblh2razideaphc
            // 
            this.lblh2razideaphc.AutoSize = true;
            this.lblh2razideaphc.Location = new System.Drawing.Point(16, 17);
            this.lblh2razideaphc.Name = "lblh2razideaphc";
            this.lblh2razideaphc.Size = new System.Drawing.Size(33, 13);
            this.lblh2razideaphc.TabIndex = 0;
            this.lblh2razideaphc.Text = "Idea:";
            // 
            // txth2razjuiciophc
            // 
            this.txth2razjuiciophc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth2razjuiciophc.Location = new System.Drawing.Point(84, 36);
            this.txth2razjuiciophc.Name = "txth2razjuiciophc";
            this.txth2razjuiciophc.Properties.MaxLength = 100;
            this.txth2razjuiciophc.Size = new System.Drawing.Size(388, 20);
            this.txth2razjuiciophc.TabIndex = 3;
            // 
            // lblh2razjuiciophc
            // 
            this.lblh2razjuiciophc.AutoSize = true;
            this.lblh2razjuiciophc.Location = new System.Drawing.Point(16, 39);
            this.lblh2razjuiciophc.Name = "lblh2razjuiciophc";
            this.lblh2razjuiciophc.Size = new System.Drawing.Size(37, 13);
            this.lblh2razjuiciophc.TabIndex = 2;
            this.lblh2razjuiciophc.Text = "Juicio:";
            // 
            // lblh2razabstraccionphc
            // 
            this.lblh2razabstraccionphc.AutoSize = true;
            this.lblh2razabstraccionphc.Location = new System.Drawing.Point(16, 61);
            this.lblh2razabstraccionphc.Name = "lblh2razabstraccionphc";
            this.lblh2razabstraccionphc.Size = new System.Drawing.Size(67, 13);
            this.lblh2razabstraccionphc.TabIndex = 4;
            this.lblh2razabstraccionphc.Text = "Abstracción:";
            // 
            // txth2razabstraccionphc
            // 
            this.txth2razabstraccionphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth2razabstraccionphc.Location = new System.Drawing.Point(84, 58);
            this.txth2razabstraccionphc.Name = "txth2razabstraccionphc";
            this.txth2razabstraccionphc.Properties.MaxLength = 100;
            this.txth2razabstraccionphc.Size = new System.Drawing.Size(388, 20);
            this.txth2razabstraccionphc.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txth2orilugarphc);
            this.groupBox2.Controls.Add(this.lblh2orilugarphc);
            this.groupBox2.Controls.Add(this.txth2oriespaciophc);
            this.groupBox2.Controls.Add(this.lblh2oriespaciophc);
            this.groupBox2.Controls.Add(this.txth2oripersonaphc);
            this.groupBox2.Controls.Add(this.lblh2oripersonaphc);
            this.groupBox2.Location = new System.Drawing.Point(5, 69);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(462, 85);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "B. Orientación";
            // 
            // txth2orilugarphc
            // 
            this.txth2orilugarphc.EditValue = "";
            this.txth2orilugarphc.Location = new System.Drawing.Point(68, 36);
            this.txth2orilugarphc.Name = "txth2orilugarphc";
            this.txth2orilugarphc.Properties.MaxLength = 100;
            this.txth2orilugarphc.Size = new System.Drawing.Size(385, 20);
            this.txth2orilugarphc.TabIndex = 3;
            // 
            // lblh2orilugarphc
            // 
            this.lblh2orilugarphc.AutoSize = true;
            this.lblh2orilugarphc.Location = new System.Drawing.Point(14, 39);
            this.lblh2orilugarphc.Name = "lblh2orilugarphc";
            this.lblh2orilugarphc.Size = new System.Drawing.Size(38, 13);
            this.lblh2orilugarphc.TabIndex = 2;
            this.lblh2orilugarphc.Text = "Lugar:";
            // 
            // txth2oriespaciophc
            // 
            this.txth2oriespaciophc.EditValue = "";
            this.txth2oriespaciophc.Location = new System.Drawing.Point(68, 14);
            this.txth2oriespaciophc.Name = "txth2oriespaciophc";
            this.txth2oriespaciophc.Properties.MaxLength = 100;
            this.txth2oriespaciophc.Size = new System.Drawing.Size(385, 20);
            this.txth2oriespaciophc.TabIndex = 1;
            // 
            // lblh2oriespaciophc
            // 
            this.lblh2oriespaciophc.AutoSize = true;
            this.lblh2oriespaciophc.Location = new System.Drawing.Point(14, 17);
            this.lblh2oriespaciophc.Name = "lblh2oriespaciophc";
            this.lblh2oriespaciophc.Size = new System.Drawing.Size(47, 13);
            this.lblh2oriespaciophc.TabIndex = 0;
            this.lblh2oriespaciophc.Text = "Espacio:";
            // 
            // txth2oripersonaphc
            // 
            this.txth2oripersonaphc.Location = new System.Drawing.Point(68, 58);
            this.txth2oripersonaphc.Name = "txth2oripersonaphc";
            this.txth2oripersonaphc.Properties.MaxLength = 100;
            this.txth2oripersonaphc.Size = new System.Drawing.Size(385, 20);
            this.txth2oripersonaphc.TabIndex = 5;
            // 
            // lblh2oripersonaphc
            // 
            this.lblh2oripersonaphc.AutoSize = true;
            this.lblh2oripersonaphc.Location = new System.Drawing.Point(14, 61);
            this.lblh2oripersonaphc.Name = "lblh2oripersonaphc";
            this.lblh2oripersonaphc.Size = new System.Drawing.Size(50, 13);
            this.lblh2oripersonaphc.TabIndex = 4;
            this.lblh2oripersonaphc.Text = "Persona:";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.lblh2conconfusionphc);
            this.groupBox1.Controls.Add(this.txth2conalertaphc);
            this.groupBox1.Controls.Add(this.txth2concomaphc);
            this.groupBox1.Controls.Add(this.lblh2concomaphc);
            this.groupBox1.Controls.Add(this.txth2conestuforphc);
            this.groupBox1.Controls.Add(this.lblh2conestuforphc);
            this.groupBox1.Controls.Add(this.lblh2conalertaphc);
            this.groupBox1.Controls.Add(this.txth2consomnolenciaphc);
            this.groupBox1.Controls.Add(this.lblh2consomnolenciaphc);
            this.groupBox1.Controls.Add(this.txth2conconfusionphc);
            this.groupBox1.Location = new System.Drawing.Point(5, 24);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(955, 44);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "A. Estado de Conciencia";
            // 
            // lblh2conconfusionphc
            // 
            this.lblh2conconfusionphc.AutoSize = true;
            this.lblh2conconfusionphc.Location = new System.Drawing.Point(186, 19);
            this.lblh2conconfusionphc.Name = "lblh2conconfusionphc";
            this.lblh2conconfusionphc.Size = new System.Drawing.Size(59, 13);
            this.lblh2conconfusionphc.TabIndex = 2;
            this.lblh2conconfusionphc.Text = "Confusión:";
            // 
            // txth2conalertaphc
            // 
            this.txth2conalertaphc.Location = new System.Drawing.Point(51, 16);
            this.txth2conalertaphc.Name = "txth2conalertaphc";
            this.txth2conalertaphc.Properties.MaxLength = 100;
            this.txth2conalertaphc.Size = new System.Drawing.Size(120, 20);
            this.txth2conalertaphc.TabIndex = 0;
            // 
            // txth2concomaphc
            // 
            this.txth2concomaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth2concomaphc.Location = new System.Drawing.Point(817, 16);
            this.txth2concomaphc.Name = "txth2concomaphc";
            this.txth2concomaphc.Properties.MaxLength = 100;
            this.txth2concomaphc.Size = new System.Drawing.Size(123, 20);
            this.txth2concomaphc.TabIndex = 8;
            // 
            // lblh2concomaphc
            // 
            this.lblh2concomaphc.AutoSize = true;
            this.lblh2concomaphc.Location = new System.Drawing.Point(780, 19);
            this.lblh2concomaphc.Name = "lblh2concomaphc";
            this.lblh2concomaphc.Size = new System.Drawing.Size(38, 13);
            this.lblh2concomaphc.TabIndex = 9;
            this.lblh2concomaphc.Text = "Coma:";
            // 
            // txth2conestuforphc
            // 
            this.txth2conestuforphc.Location = new System.Drawing.Point(645, 16);
            this.txth2conestuforphc.Name = "txth2conestuforphc";
            this.txth2conestuforphc.Properties.MaxLength = 100;
            this.txth2conestuforphc.Size = new System.Drawing.Size(120, 20);
            this.txth2conestuforphc.TabIndex = 6;
            // 
            // lblh2conestuforphc
            // 
            this.lblh2conestuforphc.AutoSize = true;
            this.lblh2conestuforphc.Location = new System.Drawing.Point(600, 19);
            this.lblh2conestuforphc.Name = "lblh2conestuforphc";
            this.lblh2conestuforphc.Size = new System.Drawing.Size(46, 13);
            this.lblh2conestuforphc.TabIndex = 7;
            this.lblh2conestuforphc.Text = "Estufor:";
            // 
            // lblh2conalertaphc
            // 
            this.lblh2conalertaphc.AutoSize = true;
            this.lblh2conalertaphc.Location = new System.Drawing.Point(14, 19);
            this.lblh2conalertaphc.Name = "lblh2conalertaphc";
            this.lblh2conalertaphc.Size = new System.Drawing.Size(40, 13);
            this.lblh2conalertaphc.TabIndex = 1;
            this.lblh2conalertaphc.Text = "Alerta:";
            // 
            // txth2consomnolenciaphc
            // 
            this.txth2consomnolenciaphc.Location = new System.Drawing.Point(453, 16);
            this.txth2consomnolenciaphc.Name = "txth2consomnolenciaphc";
            this.txth2consomnolenciaphc.Properties.MaxLength = 100;
            this.txth2consomnolenciaphc.Size = new System.Drawing.Size(120, 20);
            this.txth2consomnolenciaphc.TabIndex = 4;
            // 
            // lblh2consomnolenciaphc
            // 
            this.lblh2consomnolenciaphc.AutoSize = true;
            this.lblh2consomnolenciaphc.Location = new System.Drawing.Point(384, 19);
            this.lblh2consomnolenciaphc.Name = "lblh2consomnolenciaphc";
            this.lblh2consomnolenciaphc.Size = new System.Drawing.Size(70, 13);
            this.lblh2consomnolenciaphc.TabIndex = 5;
            this.lblh2consomnolenciaphc.Text = "Somnolencia:";
            // 
            // txth2conconfusionphc
            // 
            this.txth2conconfusionphc.Location = new System.Drawing.Point(245, 16);
            this.txth2conconfusionphc.Name = "txth2conconfusionphc";
            this.txth2conconfusionphc.Properties.MaxLength = 100;
            this.txth2conconfusionphc.Size = new System.Drawing.Size(120, 20);
            this.txth2conconfusionphc.TabIndex = 3;
            // 
            // groupControl2
            // 
            this.groupControl2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl2.Controls.Add(this.txth2extremidadesphc);
            this.groupControl2.Controls.Add(this.txth2genitourinariophc);
            this.groupControl2.Controls.Add(this.txth2abdomenphc);
            this.groupControl2.Controls.Add(this.txth2toraxphc);
            this.groupControl2.Controls.Add(this.txth2cuellophc);
            this.groupControl2.Controls.Add(this.txth2cabezaphc);
            this.groupControl2.Controls.Add(this.lblh2cabezaphc);
            this.groupControl2.Controls.Add(this.lblh2extremidadesphc);
            this.groupControl2.Controls.Add(this.lblh2cuellophc);
            this.groupControl2.Controls.Add(this.lblh2genitourinariophc);
            this.groupControl2.Controls.Add(this.lblh2toraxphc);
            this.groupControl2.Controls.Add(this.lblh2abdomenphc);
            this.groupControl2.Location = new System.Drawing.Point(3, 4);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(975, 191);
            this.groupControl2.TabIndex = 0;
            this.groupControl2.Text = "Examen Físico Segmentario";
            // 
            // txth2extremidadesphc
            // 
            this.txth2extremidadesphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth2extremidadesphc.EditValue = "";
            this.txth2extremidadesphc.Location = new System.Drawing.Point(555, 136);
            this.txth2extremidadesphc.Name = "txth2extremidadesphc";
            this.txth2extremidadesphc.Size = new System.Drawing.Size(413, 48);
            this.txth2extremidadesphc.TabIndex = 10;
            // 
            // txth2genitourinariophc
            // 
            this.txth2genitourinariophc.EditValue = "";
            this.txth2genitourinariophc.Location = new System.Drawing.Point(89, 136);
            this.txth2genitourinariophc.Name = "txth2genitourinariophc";
            this.txth2genitourinariophc.Size = new System.Drawing.Size(390, 48);
            this.txth2genitourinariophc.TabIndex = 9;
            // 
            // txth2abdomenphc
            // 
            this.txth2abdomenphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth2abdomenphc.EditValue = "";
            this.txth2abdomenphc.Location = new System.Drawing.Point(555, 82);
            this.txth2abdomenphc.Name = "txth2abdomenphc";
            this.txth2abdomenphc.Size = new System.Drawing.Size(413, 48);
            this.txth2abdomenphc.TabIndex = 7;
            // 
            // txth2toraxphc
            // 
            this.txth2toraxphc.EditValue = "";
            this.txth2toraxphc.Location = new System.Drawing.Point(89, 82);
            this.txth2toraxphc.Name = "txth2toraxphc";
            this.txth2toraxphc.Size = new System.Drawing.Size(390, 48);
            this.txth2toraxphc.TabIndex = 5;
            // 
            // txth2cuellophc
            // 
            this.txth2cuellophc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth2cuellophc.EditValue = "";
            this.txth2cuellophc.Location = new System.Drawing.Point(555, 28);
            this.txth2cuellophc.Name = "txth2cuellophc";
            this.txth2cuellophc.Size = new System.Drawing.Size(413, 48);
            this.txth2cuellophc.TabIndex = 3;
            // 
            // txth2cabezaphc
            // 
            this.txth2cabezaphc.EditValue = "";
            this.txth2cabezaphc.Location = new System.Drawing.Point(89, 28);
            this.txth2cabezaphc.Name = "txth2cabezaphc";
            this.txth2cabezaphc.Size = new System.Drawing.Size(390, 48);
            this.txth2cabezaphc.TabIndex = 1;
            // 
            // lblh2cabezaphc
            // 
            this.lblh2cabezaphc.AutoSize = true;
            this.lblh2cabezaphc.Location = new System.Drawing.Point(7, 31);
            this.lblh2cabezaphc.Name = "lblh2cabezaphc";
            this.lblh2cabezaphc.Size = new System.Drawing.Size(47, 13);
            this.lblh2cabezaphc.TabIndex = 0;
            this.lblh2cabezaphc.Text = "Cabeza:";
            // 
            // lblh2extremidadesphc
            // 
            this.lblh2extremidadesphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblh2extremidadesphc.AutoSize = true;
            this.lblh2extremidadesphc.Location = new System.Drawing.Point(481, 139);
            this.lblh2extremidadesphc.Name = "lblh2extremidadesphc";
            this.lblh2extremidadesphc.Size = new System.Drawing.Size(76, 13);
            this.lblh2extremidadesphc.TabIndex = 11;
            this.lblh2extremidadesphc.Text = "Extremidades:";
            // 
            // lblh2cuellophc
            // 
            this.lblh2cuellophc.AutoSize = true;
            this.lblh2cuellophc.Location = new System.Drawing.Point(481, 31);
            this.lblh2cuellophc.Name = "lblh2cuellophc";
            this.lblh2cuellophc.Size = new System.Drawing.Size(40, 13);
            this.lblh2cuellophc.TabIndex = 2;
            this.lblh2cuellophc.Text = "Cuello:";
            // 
            // lblh2genitourinariophc
            // 
            this.lblh2genitourinariophc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lblh2genitourinariophc.AutoSize = true;
            this.lblh2genitourinariophc.Location = new System.Drawing.Point(7, 139);
            this.lblh2genitourinariophc.Name = "lblh2genitourinariophc";
            this.lblh2genitourinariophc.Size = new System.Drawing.Size(82, 13);
            this.lblh2genitourinariophc.TabIndex = 8;
            this.lblh2genitourinariophc.Text = "Génito Urinario:";
            // 
            // lblh2toraxphc
            // 
            this.lblh2toraxphc.AutoSize = true;
            this.lblh2toraxphc.Location = new System.Drawing.Point(7, 85);
            this.lblh2toraxphc.Name = "lblh2toraxphc";
            this.lblh2toraxphc.Size = new System.Drawing.Size(39, 13);
            this.lblh2toraxphc.TabIndex = 4;
            this.lblh2toraxphc.Text = "Tórax:";
            // 
            // lblh2abdomenphc
            // 
            this.lblh2abdomenphc.AutoSize = true;
            this.lblh2abdomenphc.Location = new System.Drawing.Point(481, 85);
            this.lblh2abdomenphc.Name = "lblh2abdomenphc";
            this.lblh2abdomenphc.Size = new System.Drawing.Size(56, 13);
            this.lblh2abdomenphc.TabIndex = 6;
            this.lblh2abdomenphc.Text = "Abdomen:";
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.xtraScrollableControl7);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.ShowCloseButton = DevExpress.Utils.DefaultBoolean.False;
            this.xtraTabPage1.Size = new System.Drawing.Size(982, 594);
            this.xtraTabPage1.TabPageWidth = 70;
            this.xtraTabPage1.Text = "Hoja 1";
            // 
            // xtraScrollableControl7
            // 
            this.xtraScrollableControl7.Controls.Add(this.groupControl1);
            this.xtraScrollableControl7.Controls.Add(this.txth1enfermedadphc);
            this.xtraScrollableControl7.Controls.Add(this.txth1motivophc);
            this.xtraScrollableControl7.Controls.Add(this.labelMotivoDeConsulta);
            this.xtraScrollableControl7.Controls.Add(this.labelEnfermedadActual);
            this.xtraScrollableControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl7.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl7.Name = "xtraScrollableControl7";
            this.xtraScrollableControl7.Size = new System.Drawing.Size(982, 594);
            this.xtraScrollableControl7.TabIndex = 0;
            // 
            // groupControl1
            // 
            this.groupControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl1.Controls.Add(this.txth1examenfisicophc);
            this.groupControl1.Controls.Add(this.txth1revisionphc);
            this.groupControl1.Controls.Add(this.txth1otrosphc);
            this.groupControl1.Controls.Add(this.txth1Cirugiasphc);
            this.groupControl1.Controls.Add(this.txth1pcphc);
            this.groupControl1.Controls.Add(this.labelPC);
            this.groupControl1.Controls.Add(this.txth1pesophc);
            this.groupControl1.Controls.Add(this.labelPESO);
            this.groupControl1.Controls.Add(this.labelCirugias);
            this.groupControl1.Controls.Add(this.txth1tallaphc);
            this.groupControl1.Controls.Add(this.labelTALLA);
            this.groupControl1.Controls.Add(this.labelInfecciones);
            this.groupControl1.Controls.Add(this.txth1tempphc);
            this.groupControl1.Controls.Add(this.txth1infeccionesphc);
            this.groupControl1.Controls.Add(this.labelTEMP);
            this.groupControl1.Controls.Add(this.labelTraumatismos);
            this.groupControl1.Controls.Add(this.txth1frphc);
            this.groupControl1.Controls.Add(this.txth1traumatismosphc);
            this.groupControl1.Controls.Add(this.labelFR);
            this.groupControl1.Controls.Add(this.labelCardiovascular);
            this.groupControl1.Controls.Add(this.txth1fcphc);
            this.groupControl1.Controls.Add(this.txth1cardiovascularphc);
            this.groupControl1.Controls.Add(this.labelFC);
            this.groupControl1.Controls.Add(this.labelMetabolico);
            this.groupControl1.Controls.Add(this.txth1taphc);
            this.groupControl1.Controls.Add(this.txth1metabolicophc);
            this.groupControl1.Controls.Add(this.labelTA);
            this.groupControl1.Controls.Add(this.labelToxico);
            this.groupControl1.Controls.Add(this.txth1toxicophc);
            this.groupControl1.Controls.Add(this.labelExamenFisicoGeneral);
            this.groupControl1.Controls.Add(this.labelFamiliar);
            this.groupControl1.Controls.Add(this.txth1familiarphc);
            this.groupControl1.Controls.Add(this.labelRevisionPorSistemas);
            this.groupControl1.Controls.Add(this.labelGinecoObstetrico);
            this.groupControl1.Controls.Add(this.txth1fupphc);
            this.groupControl1.Controls.Add(this.txth1ginecoobsphc);
            this.groupControl1.Controls.Add(this.labelFUP);
            this.groupControl1.Controls.Add(this.labelOtros);
            this.groupControl1.Controls.Add(this.txth1fumphc);
            this.groupControl1.Controls.Add(this.labelFUM);
            this.groupControl1.Controls.Add(this.labelEmbarazos);
            this.groupControl1.Controls.Add(this.txth1mesntruacionesphc);
            this.groupControl1.Controls.Add(this.nudh1embarazosphc);
            this.groupControl1.Controls.Add(this.labelMesntruaciones);
            this.groupControl1.Controls.Add(this.labelGesta);
            this.groupControl1.Controls.Add(this.nudh1abortosphc);
            this.groupControl1.Controls.Add(this.nudh1gestaphc);
            this.groupControl1.Controls.Add(this.labelAbortos);
            this.groupControl1.Location = new System.Drawing.Point(3, 212);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(975, 375);
            this.groupControl1.TabIndex = 4;
            this.groupControl1.Text = "Antecedentes Patológicos";
            // 
            // txth1examenfisicophc
            // 
            this.txth1examenfisicophc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth1examenfisicophc.EditValue = "";
            this.txth1examenfisicophc.Location = new System.Drawing.Point(135, 274);
            this.txth1examenfisicophc.Name = "txth1examenfisicophc";
            this.txth1examenfisicophc.Size = new System.Drawing.Size(835, 73);
            this.txth1examenfisicophc.TabIndex = 33;
            // 
            // txth1revisionphc
            // 
            this.txth1revisionphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth1revisionphc.EditValue = "";
            this.txth1revisionphc.Location = new System.Drawing.Point(135, 198);
            this.txth1revisionphc.Name = "txth1revisionphc";
            this.txth1revisionphc.Size = new System.Drawing.Size(835, 73);
            this.txth1revisionphc.TabIndex = 31;
            // 
            // txth1otrosphc
            // 
            this.txth1otrosphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth1otrosphc.EditValue = "";
            this.txth1otrosphc.Location = new System.Drawing.Point(605, 100);
            this.txth1otrosphc.Name = "txth1otrosphc";
            this.txth1otrosphc.Size = new System.Drawing.Size(365, 94);
            this.txth1otrosphc.TabIndex = 29;
            // 
            // txth1Cirugiasphc
            // 
            this.txth1Cirugiasphc.Location = new System.Drawing.Point(108, 25);
            this.txth1Cirugiasphc.Name = "txth1Cirugiasphc";
            this.txth1Cirugiasphc.Properties.MaxLength = 1000;
            this.txth1Cirugiasphc.Size = new System.Drawing.Size(362, 20);
            this.txth1Cirugiasphc.TabIndex = 1;
            // 
            // txth1pcphc
            // 
            this.txth1pcphc.Location = new System.Drawing.Point(874, 350);
            this.txth1pcphc.Name = "txth1pcphc";
            this.txth1pcphc.Properties.MaxLength = 100;
            this.txth1pcphc.Size = new System.Drawing.Size(93, 20);
            this.txth1pcphc.TabIndex = 47;
            // 
            // labelPC
            // 
            this.labelPC.AutoSize = true;
            this.labelPC.Location = new System.Drawing.Point(848, 353);
            this.labelPC.Name = "labelPC";
            this.labelPC.Size = new System.Drawing.Size(24, 13);
            this.labelPC.TabIndex = 46;
            this.labelPC.Text = "PC:";
            // 
            // txth1pesophc
            // 
            this.txth1pesophc.Location = new System.Drawing.Point(746, 350);
            this.txth1pesophc.Name = "txth1pesophc";
            this.txth1pesophc.Properties.MaxLength = 100;
            this.txth1pesophc.Size = new System.Drawing.Size(93, 20);
            this.txth1pesophc.TabIndex = 45;
            // 
            // labelPESO
            // 
            this.labelPESO.AutoSize = true;
            this.labelPESO.Location = new System.Drawing.Point(707, 353);
            this.labelPESO.Name = "labelPESO";
            this.labelPESO.Size = new System.Drawing.Size(37, 13);
            this.labelPESO.TabIndex = 44;
            this.labelPESO.Text = "PESO:";
            // 
            // labelCirugias
            // 
            this.labelCirugias.AutoSize = true;
            this.labelCirugias.Location = new System.Drawing.Point(11, 28);
            this.labelCirugias.Name = "labelCirugias";
            this.labelCirugias.Size = new System.Drawing.Size(49, 13);
            this.labelCirugias.TabIndex = 0;
            this.labelCirugias.Text = "Cirugías:";
            // 
            // txth1tallaphc
            // 
            this.txth1tallaphc.Location = new System.Drawing.Point(602, 350);
            this.txth1tallaphc.Name = "txth1tallaphc";
            this.txth1tallaphc.Properties.MaxLength = 100;
            this.txth1tallaphc.Size = new System.Drawing.Size(93, 20);
            this.txth1tallaphc.TabIndex = 43;
            // 
            // labelTALLA
            // 
            this.labelTALLA.AutoSize = true;
            this.labelTALLA.Location = new System.Drawing.Point(559, 353);
            this.labelTALLA.Name = "labelTALLA";
            this.labelTALLA.Size = new System.Drawing.Size(41, 13);
            this.labelTALLA.TabIndex = 42;
            this.labelTALLA.Text = "TALLA:";
            // 
            // labelInfecciones
            // 
            this.labelInfecciones.AutoSize = true;
            this.labelInfecciones.Location = new System.Drawing.Point(497, 28);
            this.labelInfecciones.Name = "labelInfecciones";
            this.labelInfecciones.Size = new System.Drawing.Size(66, 13);
            this.labelInfecciones.TabIndex = 2;
            this.labelInfecciones.Text = "Infecciones:";
            // 
            // txth1tempphc
            // 
            this.txth1tempphc.Location = new System.Drawing.Point(454, 350);
            this.txth1tempphc.Name = "txth1tempphc";
            this.txth1tempphc.Properties.MaxLength = 100;
            this.txth1tempphc.Size = new System.Drawing.Size(93, 20);
            this.txth1tempphc.TabIndex = 41;
            // 
            // txth1infeccionesphc
            // 
            this.txth1infeccionesphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth1infeccionesphc.Location = new System.Drawing.Point(605, 25);
            this.txth1infeccionesphc.Name = "txth1infeccionesphc";
            this.txth1infeccionesphc.Properties.MaxLength = 1000;
            this.txth1infeccionesphc.Size = new System.Drawing.Size(365, 20);
            this.txth1infeccionesphc.TabIndex = 3;
            // 
            // labelTEMP
            // 
            this.labelTEMP.AutoSize = true;
            this.labelTEMP.Location = new System.Drawing.Point(415, 353);
            this.labelTEMP.Name = "labelTEMP";
            this.labelTEMP.Size = new System.Drawing.Size(37, 13);
            this.labelTEMP.TabIndex = 40;
            this.labelTEMP.Text = "TEMP:";
            // 
            // labelTraumatismos
            // 
            this.labelTraumatismos.AutoSize = true;
            this.labelTraumatismos.Location = new System.Drawing.Point(11, 53);
            this.labelTraumatismos.Name = "labelTraumatismos";
            this.labelTraumatismos.Size = new System.Drawing.Size(77, 13);
            this.labelTraumatismos.TabIndex = 4;
            this.labelTraumatismos.Text = "Traumatismos:";
            // 
            // txth1frphc
            // 
            this.txth1frphc.Location = new System.Drawing.Point(306, 350);
            this.txth1frphc.Name = "txth1frphc";
            this.txth1frphc.Properties.MaxLength = 100;
            this.txth1frphc.Size = new System.Drawing.Size(93, 20);
            this.txth1frphc.TabIndex = 39;
            // 
            // txth1traumatismosphc
            // 
            this.txth1traumatismosphc.Location = new System.Drawing.Point(108, 50);
            this.txth1traumatismosphc.Name = "txth1traumatismosphc";
            this.txth1traumatismosphc.Properties.MaxLength = 1000;
            this.txth1traumatismosphc.Size = new System.Drawing.Size(362, 20);
            this.txth1traumatismosphc.TabIndex = 5;
            // 
            // labelFR
            // 
            this.labelFR.AutoSize = true;
            this.labelFR.Location = new System.Drawing.Point(276, 353);
            this.labelFR.Name = "labelFR";
            this.labelFR.Size = new System.Drawing.Size(24, 13);
            this.labelFR.TabIndex = 38;
            this.labelFR.Text = "FR:";
            // 
            // labelCardiovascular
            // 
            this.labelCardiovascular.AutoSize = true;
            this.labelCardiovascular.Location = new System.Drawing.Point(497, 53);
            this.labelCardiovascular.Name = "labelCardiovascular";
            this.labelCardiovascular.Size = new System.Drawing.Size(82, 13);
            this.labelCardiovascular.TabIndex = 6;
            this.labelCardiovascular.Text = "Cardiovascular:";
            // 
            // txth1fcphc
            // 
            this.txth1fcphc.Location = new System.Drawing.Point(172, 350);
            this.txth1fcphc.Name = "txth1fcphc";
            this.txth1fcphc.Properties.MaxLength = 100;
            this.txth1fcphc.Size = new System.Drawing.Size(93, 20);
            this.txth1fcphc.TabIndex = 37;
            // 
            // txth1cardiovascularphc
            // 
            this.txth1cardiovascularphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth1cardiovascularphc.Location = new System.Drawing.Point(605, 50);
            this.txth1cardiovascularphc.Name = "txth1cardiovascularphc";
            this.txth1cardiovascularphc.Properties.MaxLength = 1000;
            this.txth1cardiovascularphc.Size = new System.Drawing.Size(365, 20);
            this.txth1cardiovascularphc.TabIndex = 7;
            // 
            // labelFC
            // 
            this.labelFC.AutoSize = true;
            this.labelFC.Location = new System.Drawing.Point(146, 353);
            this.labelFC.Name = "labelFC";
            this.labelFC.Size = new System.Drawing.Size(24, 13);
            this.labelFC.TabIndex = 36;
            this.labelFC.Text = "FC:";
            // 
            // labelMetabolico
            // 
            this.labelMetabolico.AutoSize = true;
            this.labelMetabolico.Location = new System.Drawing.Point(11, 78);
            this.labelMetabolico.Name = "labelMetabolico";
            this.labelMetabolico.Size = new System.Drawing.Size(62, 13);
            this.labelMetabolico.TabIndex = 8;
            this.labelMetabolico.Text = "Metabólico:";
            // 
            // txth1taphc
            // 
            this.txth1taphc.Location = new System.Drawing.Point(41, 350);
            this.txth1taphc.Name = "txth1taphc";
            this.txth1taphc.Properties.MaxLength = 100;
            this.txth1taphc.Size = new System.Drawing.Size(93, 20);
            this.txth1taphc.TabIndex = 35;
            // 
            // txth1metabolicophc
            // 
            this.txth1metabolicophc.Location = new System.Drawing.Point(108, 75);
            this.txth1metabolicophc.Name = "txth1metabolicophc";
            this.txth1metabolicophc.Properties.MaxLength = 1000;
            this.txth1metabolicophc.Size = new System.Drawing.Size(362, 20);
            this.txth1metabolicophc.TabIndex = 9;
            // 
            // labelTA
            // 
            this.labelTA.AutoSize = true;
            this.labelTA.Location = new System.Drawing.Point(11, 353);
            this.labelTA.Name = "labelTA";
            this.labelTA.Size = new System.Drawing.Size(28, 13);
            this.labelTA.TabIndex = 34;
            this.labelTA.Text = "T/A:";
            // 
            // labelToxico
            // 
            this.labelToxico.AutoSize = true;
            this.labelToxico.Location = new System.Drawing.Point(497, 78);
            this.labelToxico.Name = "labelToxico";
            this.labelToxico.Size = new System.Drawing.Size(42, 13);
            this.labelToxico.TabIndex = 10;
            this.labelToxico.Text = "Tóxico:";
            // 
            // txth1toxicophc
            // 
            this.txth1toxicophc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth1toxicophc.Location = new System.Drawing.Point(605, 75);
            this.txth1toxicophc.Name = "txth1toxicophc";
            this.txth1toxicophc.Properties.MaxLength = 1000;
            this.txth1toxicophc.Size = new System.Drawing.Size(365, 20);
            this.txth1toxicophc.TabIndex = 11;
            // 
            // labelExamenFisicoGeneral
            // 
            this.labelExamenFisicoGeneral.AutoSize = true;
            this.labelExamenFisicoGeneral.Location = new System.Drawing.Point(11, 277);
            this.labelExamenFisicoGeneral.Name = "labelExamenFisicoGeneral";
            this.labelExamenFisicoGeneral.Size = new System.Drawing.Size(118, 13);
            this.labelExamenFisicoGeneral.TabIndex = 32;
            this.labelExamenFisicoGeneral.Text = "Examen Físico General:";
            // 
            // labelFamiliar
            // 
            this.labelFamiliar.AutoSize = true;
            this.labelFamiliar.Location = new System.Drawing.Point(11, 103);
            this.labelFamiliar.Name = "labelFamiliar";
            this.labelFamiliar.Size = new System.Drawing.Size(47, 13);
            this.labelFamiliar.TabIndex = 12;
            this.labelFamiliar.Text = "Familiar:";
            // 
            // txth1familiarphc
            // 
            this.txth1familiarphc.Location = new System.Drawing.Point(108, 100);
            this.txth1familiarphc.Name = "txth1familiarphc";
            this.txth1familiarphc.Properties.MaxLength = 1000;
            this.txth1familiarphc.Size = new System.Drawing.Size(362, 20);
            this.txth1familiarphc.TabIndex = 13;
            // 
            // labelRevisionPorSistemas
            // 
            this.labelRevisionPorSistemas.AutoSize = true;
            this.labelRevisionPorSistemas.Location = new System.Drawing.Point(11, 201);
            this.labelRevisionPorSistemas.Name = "labelRevisionPorSistemas";
            this.labelRevisionPorSistemas.Size = new System.Drawing.Size(115, 13);
            this.labelRevisionPorSistemas.TabIndex = 30;
            this.labelRevisionPorSistemas.Text = "Revisión por Sistemas:";
            // 
            // labelGinecoObstetrico
            // 
            this.labelGinecoObstetrico.AutoSize = true;
            this.labelGinecoObstetrico.Location = new System.Drawing.Point(11, 128);
            this.labelGinecoObstetrico.Name = "labelGinecoObstetrico";
            this.labelGinecoObstetrico.Size = new System.Drawing.Size(96, 13);
            this.labelGinecoObstetrico.TabIndex = 15;
            this.labelGinecoObstetrico.Text = "Gineco Obstétrico:";
            // 
            // txth1fupphc
            // 
            this.txth1fupphc.Location = new System.Drawing.Point(389, 173);
            this.txth1fupphc.Name = "txth1fupphc";
            this.txth1fupphc.Properties.MaxLength = 1000;
            this.txth1fupphc.Size = new System.Drawing.Size(81, 20);
            this.txth1fupphc.TabIndex = 24;
            // 
            // txth1ginecoobsphc
            // 
            this.txth1ginecoobsphc.Location = new System.Drawing.Point(108, 125);
            this.txth1ginecoobsphc.Name = "txth1ginecoobsphc";
            this.txth1ginecoobsphc.Properties.MaxLength = 1000;
            this.txth1ginecoobsphc.Size = new System.Drawing.Size(362, 20);
            this.txth1ginecoobsphc.TabIndex = 16;
            // 
            // labelFUP
            // 
            this.labelFUP.AutoSize = true;
            this.labelFUP.Location = new System.Drawing.Point(353, 176);
            this.labelFUP.Name = "labelFUP";
            this.labelFUP.Size = new System.Drawing.Size(30, 13);
            this.labelFUP.TabIndex = 28;
            this.labelFUP.Text = "FUP:";
            // 
            // labelOtros
            // 
            this.labelOtros.AutoSize = true;
            this.labelOtros.Location = new System.Drawing.Point(497, 103);
            this.labelOtros.Name = "labelOtros";
            this.labelOtros.Size = new System.Drawing.Size(38, 13);
            this.labelOtros.TabIndex = 14;
            this.labelOtros.Text = "Otros:";
            // 
            // txth1fumphc
            // 
            this.txth1fumphc.Location = new System.Drawing.Point(247, 173);
            this.txth1fumphc.Name = "txth1fumphc";
            this.txth1fumphc.Properties.MaxLength = 1000;
            this.txth1fumphc.Size = new System.Drawing.Size(80, 20);
            this.txth1fumphc.TabIndex = 27;
            // 
            // labelFUM
            // 
            this.labelFUM.AutoSize = true;
            this.labelFUM.Location = new System.Drawing.Point(202, 176);
            this.labelFUM.Name = "labelFUM";
            this.labelFUM.Size = new System.Drawing.Size(32, 13);
            this.labelFUM.TabIndex = 26;
            this.labelFUM.Text = "FUM:";
            // 
            // labelEmbarazos
            // 
            this.labelEmbarazos.AutoSize = true;
            this.labelEmbarazos.Location = new System.Drawing.Point(11, 152);
            this.labelEmbarazos.Name = "labelEmbarazos";
            this.labelEmbarazos.Size = new System.Drawing.Size(63, 13);
            this.labelEmbarazos.TabIndex = 17;
            this.labelEmbarazos.Text = "Embarazos:";
            // 
            // txth1mesntruacionesphc
            // 
            this.txth1mesntruacionesphc.Location = new System.Drawing.Point(108, 174);
            this.txth1mesntruacionesphc.Name = "txth1mesntruacionesphc";
            this.txth1mesntruacionesphc.Properties.MaxLength = 1000;
            this.txth1mesntruacionesphc.Size = new System.Drawing.Size(80, 20);
            this.txth1mesntruacionesphc.TabIndex = 25;
            // 
            // nudh1embarazosphc
            // 
            this.nudh1embarazosphc.EditValue = ((short)(0));
            this.nudh1embarazosphc.Location = new System.Drawing.Point(108, 149);
            this.nudh1embarazosphc.Name = "nudh1embarazosphc";
            this.nudh1embarazosphc.Properties.Mask.EditMask = "n0";
            this.nudh1embarazosphc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.nudh1embarazosphc.Size = new System.Drawing.Size(80, 20);
            this.nudh1embarazosphc.TabIndex = 18;
            // 
            // labelMesntruaciones
            // 
            this.labelMesntruaciones.AutoSize = true;
            this.labelMesntruaciones.Location = new System.Drawing.Point(11, 177);
            this.labelMesntruaciones.Name = "labelMesntruaciones";
            this.labelMesntruaciones.Size = new System.Drawing.Size(86, 13);
            this.labelMesntruaciones.TabIndex = 23;
            this.labelMesntruaciones.Text = "Mesntruaciones:";
            // 
            // labelGesta
            // 
            this.labelGesta.AutoSize = true;
            this.labelGesta.Location = new System.Drawing.Point(202, 152);
            this.labelGesta.Name = "labelGesta";
            this.labelGesta.Size = new System.Drawing.Size(39, 13);
            this.labelGesta.TabIndex = 19;
            this.labelGesta.Text = "Gesta:";
            // 
            // nudh1abortosphc
            // 
            this.nudh1abortosphc.EditValue = ((short)(0));
            this.nudh1abortosphc.Location = new System.Drawing.Point(390, 149);
            this.nudh1abortosphc.Name = "nudh1abortosphc";
            this.nudh1abortosphc.Properties.Mask.EditMask = "n0";
            this.nudh1abortosphc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.nudh1abortosphc.Size = new System.Drawing.Size(80, 20);
            this.nudh1abortosphc.TabIndex = 22;
            // 
            // nudh1gestaphc
            // 
            this.nudh1gestaphc.EditValue = ((short)(0));
            this.nudh1gestaphc.Location = new System.Drawing.Point(247, 149);
            this.nudh1gestaphc.Name = "nudh1gestaphc";
            this.nudh1gestaphc.Properties.Mask.EditMask = "n0";
            this.nudh1gestaphc.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric;
            this.nudh1gestaphc.Size = new System.Drawing.Size(80, 20);
            this.nudh1gestaphc.TabIndex = 20;
            // 
            // labelAbortos
            // 
            this.labelAbortos.AutoSize = true;
            this.labelAbortos.Location = new System.Drawing.Point(335, 152);
            this.labelAbortos.Name = "labelAbortos";
            this.labelAbortos.Size = new System.Drawing.Size(49, 13);
            this.labelAbortos.TabIndex = 21;
            this.labelAbortos.Text = "Abortos:";
            // 
            // txth1enfermedadphc
            // 
            this.txth1enfermedadphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth1enfermedadphc.EditValue = "";
            this.txth1enfermedadphc.Location = new System.Drawing.Point(106, 108);
            this.txth1enfermedadphc.Name = "txth1enfermedadphc";
            this.txth1enfermedadphc.Size = new System.Drawing.Size(867, 96);
            this.txth1enfermedadphc.TabIndex = 2;
            // 
            // txth1motivophc
            // 
            this.txth1motivophc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth1motivophc.EditValue = "";
            this.txth1motivophc.Location = new System.Drawing.Point(106, 6);
            this.txth1motivophc.Name = "txth1motivophc";
            this.txth1motivophc.Size = new System.Drawing.Size(867, 96);
            this.txth1motivophc.TabIndex = 0;
            // 
            // labelMotivoDeConsulta
            // 
            this.labelMotivoDeConsulta.AutoSize = true;
            this.labelMotivoDeConsulta.Location = new System.Drawing.Point(9, 9);
            this.labelMotivoDeConsulta.Name = "labelMotivoDeConsulta";
            this.labelMotivoDeConsulta.Size = new System.Drawing.Size(103, 13);
            this.labelMotivoDeConsulta.TabIndex = 1;
            this.labelMotivoDeConsulta.Text = "Motivo de Consulta:";
            // 
            // labelEnfermedadActual
            // 
            this.labelEnfermedadActual.AutoSize = true;
            this.labelEnfermedadActual.Location = new System.Drawing.Point(9, 111);
            this.labelEnfermedadActual.Name = "labelEnfermedadActual";
            this.labelEnfermedadActual.Size = new System.Drawing.Size(102, 13);
            this.labelEnfermedadActual.TabIndex = 3;
            this.labelEnfermedadActual.Text = "Enfermedad Actual:";
            // 
            // tabHistoriaClinica
            // 
            this.tabHistoriaClinica.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabHistoriaClinica.Location = new System.Drawing.Point(12, 12);
            this.tabHistoriaClinica.Name = "tabHistoriaClinica";
            this.tabHistoriaClinica.SelectedTabPage = this.xtraTabPage1;
            this.tabHistoriaClinica.Size = new System.Drawing.Size(984, 619);
            this.tabHistoriaClinica.TabIndex = 0;
            this.tabHistoriaClinica.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2,
            this.xtraTabPage3,
            this.xtraTabPage4,
            this.xtraTabPage5,
            this.xtraTabPage6,
            this.xtraTabPage7});
            this.tabHistoriaClinica.TabPageWidth = 70;
            // 
            // xtraTabPage4
            // 
            this.xtraTabPage4.Controls.Add(this.xtraScrollableControl3);
            this.xtraTabPage4.Name = "xtraTabPage4";
            this.xtraTabPage4.Size = new System.Drawing.Size(982, 594);
            this.xtraTabPage4.Text = "Hoja 4";
            // 
            // xtraScrollableControl3
            // 
            this.xtraScrollableControl3.Controls.Add(this.groupControl8);
            this.xtraScrollableControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraScrollableControl3.Location = new System.Drawing.Point(0, 0);
            this.xtraScrollableControl3.Name = "xtraScrollableControl3";
            this.xtraScrollableControl3.Size = new System.Drawing.Size(982, 594);
            this.xtraScrollableControl3.TabIndex = 0;
            // 
            // groupControl8
            // 
            this.groupControl8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControl8.Controls.Add(this.groupBox21);
            this.groupControl8.Controls.Add(this.groupBox20);
            this.groupControl8.Controls.Add(this.groupBox18);
            this.groupControl8.Controls.Add(this.groupBox17);
            this.groupControl8.Controls.Add(this.groupBox16);
            this.groupControl8.Location = new System.Drawing.Point(3, 3);
            this.groupControl8.Name = "groupControl8";
            this.groupControl8.Size = new System.Drawing.Size(975, 587);
            this.groupControl8.TabIndex = 0;
            this.groupControl8.Text = "Pares Craneales";
            // 
            // groupBox21
            // 
            this.groupBox21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox21.Controls.Add(this.txth4facsimetriaphc);
            this.groupBox21.Controls.Add(this.lblh4facsimetriaphc);
            this.groupBox21.Controls.Add(this.txth4facmovimientosphc);
            this.groupBox21.Controls.Add(this.lblh4facmovimientosphc);
            this.groupBox21.Controls.Add(this.txth4faccentralphc);
            this.groupBox21.Controls.Add(this.lblh4facparalisisphc);
            this.groupBox21.Controls.Add(this.lblh4faccentralphc);
            this.groupBox21.Controls.Add(this.txth4facparalisisphc);
            this.groupBox21.Controls.Add(this.txth4facperifericaphc);
            this.groupBox21.Controls.Add(this.lblh4facgustophc);
            this.groupBox21.Controls.Add(this.lblh4facperifericaphc);
            this.groupBox21.Controls.Add(this.txth4facgustophc);
            this.groupBox21.Location = new System.Drawing.Point(5, 515);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(965, 66);
            this.groupBox21.TabIndex = 4;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "VII: FACIAL";
            // 
            // txth4facsimetriaphc
            // 
            this.txth4facsimetriaphc.Location = new System.Drawing.Point(69, 16);
            this.txth4facsimetriaphc.Name = "txth4facsimetriaphc";
            this.txth4facsimetriaphc.Properties.MaxLength = 200;
            this.txth4facsimetriaphc.Size = new System.Drawing.Size(240, 20);
            this.txth4facsimetriaphc.TabIndex = 1;
            // 
            // lblh4facsimetriaphc
            // 
            this.lblh4facsimetriaphc.AutoSize = true;
            this.lblh4facsimetriaphc.Location = new System.Drawing.Point(14, 19);
            this.lblh4facsimetriaphc.Name = "lblh4facsimetriaphc";
            this.lblh4facsimetriaphc.Size = new System.Drawing.Size(49, 13);
            this.lblh4facsimetriaphc.TabIndex = 0;
            this.lblh4facsimetriaphc.Text = "Simetría:";
            // 
            // txth4facmovimientosphc
            // 
            this.txth4facmovimientosphc.Location = new System.Drawing.Point(419, 16);
            this.txth4facmovimientosphc.Name = "txth4facmovimientosphc";
            this.txth4facmovimientosphc.Properties.MaxLength = 500;
            this.txth4facmovimientosphc.Size = new System.Drawing.Size(240, 20);
            this.txth4facmovimientosphc.TabIndex = 2;
            // 
            // lblh4facmovimientosphc
            // 
            this.lblh4facmovimientosphc.AutoSize = true;
            this.lblh4facmovimientosphc.Location = new System.Drawing.Point(313, 19);
            this.lblh4facmovimientosphc.Name = "lblh4facmovimientosphc";
            this.lblh4facmovimientosphc.Size = new System.Drawing.Size(111, 13);
            this.lblh4facmovimientosphc.TabIndex = 3;
            this.lblh4facmovimientosphc.Text = "Movimientos Faciales:";
            // 
            // txth4faccentralphc
            // 
            this.txth4faccentralphc.Location = new System.Drawing.Point(69, 39);
            this.txth4faccentralphc.Name = "txth4faccentralphc";
            this.txth4faccentralphc.Properties.MaxLength = 200;
            this.txth4faccentralphc.Size = new System.Drawing.Size(240, 20);
            this.txth4faccentralphc.TabIndex = 7;
            // 
            // lblh4facparalisisphc
            // 
            this.lblh4facparalisisphc.AutoSize = true;
            this.lblh4facparalisisphc.Location = new System.Drawing.Point(665, 19);
            this.lblh4facparalisisphc.Name = "lblh4facparalisisphc";
            this.lblh4facparalisisphc.Size = new System.Drawing.Size(49, 13);
            this.lblh4facparalisisphc.TabIndex = 4;
            this.lblh4facparalisisphc.Text = "Parálisis:";
            // 
            // lblh4faccentralphc
            // 
            this.lblh4faccentralphc.AutoSize = true;
            this.lblh4faccentralphc.Location = new System.Drawing.Point(14, 42);
            this.lblh4faccentralphc.Name = "lblh4faccentralphc";
            this.lblh4faccentralphc.Size = new System.Drawing.Size(46, 13);
            this.lblh4faccentralphc.TabIndex = 6;
            this.lblh4faccentralphc.Text = "Central:";
            // 
            // txth4facparalisisphc
            // 
            this.txth4facparalisisphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth4facparalisisphc.Location = new System.Drawing.Point(716, 16);
            this.txth4facparalisisphc.Name = "txth4facparalisisphc";
            this.txth4facparalisisphc.Properties.MaxLength = 200;
            this.txth4facparalisisphc.Size = new System.Drawing.Size(241, 20);
            this.txth4facparalisisphc.TabIndex = 5;
            // 
            // txth4facperifericaphc
            // 
            this.txth4facperifericaphc.Location = new System.Drawing.Point(419, 39);
            this.txth4facperifericaphc.Name = "txth4facperifericaphc";
            this.txth4facperifericaphc.Properties.MaxLength = 200;
            this.txth4facperifericaphc.Size = new System.Drawing.Size(240, 20);
            this.txth4facperifericaphc.TabIndex = 9;
            // 
            // lblh4facgustophc
            // 
            this.lblh4facgustophc.AutoSize = true;
            this.lblh4facgustophc.Location = new System.Drawing.Point(665, 42);
            this.lblh4facgustophc.Name = "lblh4facgustophc";
            this.lblh4facgustophc.Size = new System.Drawing.Size(39, 13);
            this.lblh4facgustophc.TabIndex = 10;
            this.lblh4facgustophc.Text = "Gusto:";
            // 
            // lblh4facperifericaphc
            // 
            this.lblh4facperifericaphc.AutoSize = true;
            this.lblh4facperifericaphc.Location = new System.Drawing.Point(313, 42);
            this.lblh4facperifericaphc.Name = "lblh4facperifericaphc";
            this.lblh4facperifericaphc.Size = new System.Drawing.Size(56, 13);
            this.lblh4facperifericaphc.TabIndex = 8;
            this.lblh4facperifericaphc.Text = "Periférica:";
            // 
            // txth4facgustophc
            // 
            this.txth4facgustophc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth4facgustophc.Location = new System.Drawing.Point(716, 39);
            this.txth4facgustophc.Name = "txth4facgustophc";
            this.txth4facgustophc.Properties.MaxLength = 500;
            this.txth4facgustophc.Size = new System.Drawing.Size(241, 20);
            this.txth4facgustophc.TabIndex = 11;
            // 
            // groupBox20
            // 
            this.groupBox20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox20.Controls.Add(this.lblh4movmasticatoriosphc);
            this.groupBox20.Controls.Add(this.txth4senfacializqphc);
            this.groupBox20.Controls.Add(this.lblh4senfacialderphc);
            this.groupBox20.Controls.Add(this.txth4senfacialderphc);
            this.groupBox20.Controls.Add(this.lblh4senfacializqphc);
            this.groupBox20.Controls.Add(this.lblh4refmentonianophc);
            this.groupBox20.Controls.Add(this.txth4movmasticatoriosphc);
            this.groupBox20.Controls.Add(this.txth4reflejoizqphc);
            this.groupBox20.Controls.Add(this.txth4refmentonianophc);
            this.groupBox20.Controls.Add(this.lblh4reflejoizqphc);
            this.groupBox20.Controls.Add(this.txth4reflejoderphc);
            this.groupBox20.Controls.Add(this.lblh4reflejoderphc);
            this.groupBox20.Controls.Add(this.txth4aberturabocaphc);
            this.groupBox20.Controls.Add(this.lblh4aberturabocaphc);
            this.groupBox20.Location = new System.Drawing.Point(5, 396);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(965, 113);
            this.groupBox20.TabIndex = 3;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "VI. TRIGEMIO";
            // 
            // lblh4movmasticatoriosphc
            // 
            this.lblh4movmasticatoriosphc.Location = new System.Drawing.Point(527, 65);
            this.lblh4movmasticatoriosphc.Name = "lblh4movmasticatoriosphc";
            this.lblh4movmasticatoriosphc.Size = new System.Drawing.Size(67, 26);
            this.lblh4movmasticatoriosphc.TabIndex = 10;
            this.lblh4movmasticatoriosphc.Text = "Movimientos\r\nMasticatorios:";
            // 
            // txth4senfacializqphc
            // 
            this.txth4senfacializqphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth4senfacializqphc.Location = new System.Drawing.Point(596, 16);
            this.txth4senfacializqphc.Name = "txth4senfacializqphc";
            this.txth4senfacializqphc.Properties.MaxLength = 200;
            this.txth4senfacializqphc.Size = new System.Drawing.Size(361, 20);
            this.txth4senfacializqphc.TabIndex = 3;
            // 
            // lblh4senfacialderphc
            // 
            this.lblh4senfacialderphc.AutoSize = true;
            this.lblh4senfacialderphc.Location = new System.Drawing.Point(14, 19);
            this.lblh4senfacialderphc.Name = "lblh4senfacialderphc";
            this.lblh4senfacialderphc.Size = new System.Drawing.Size(145, 13);
            this.lblh4senfacialderphc.TabIndex = 0;
            this.lblh4senfacialderphc.Text = "Sensibilidad Facial   Derecha:";
            // 
            // txth4senfacialderphc
            // 
            this.txth4senfacialderphc.Location = new System.Drawing.Point(165, 16);
            this.txth4senfacialderphc.Name = "txth4senfacialderphc";
            this.txth4senfacialderphc.Properties.MaxLength = 200;
            this.txth4senfacialderphc.Size = new System.Drawing.Size(360, 20);
            this.txth4senfacialderphc.TabIndex = 1;
            // 
            // lblh4senfacializqphc
            // 
            this.lblh4senfacializqphc.AutoSize = true;
            this.lblh4senfacializqphc.Location = new System.Drawing.Point(535, 19);
            this.lblh4senfacializqphc.Name = "lblh4senfacializqphc";
            this.lblh4senfacializqphc.Size = new System.Drawing.Size(56, 13);
            this.lblh4senfacializqphc.TabIndex = 2;
            this.lblh4senfacializqphc.Text = "Izquierda:";
            // 
            // lblh4refmentonianophc
            // 
            this.lblh4refmentonianophc.AutoSize = true;
            this.lblh4refmentonianophc.Location = new System.Drawing.Point(14, 88);
            this.lblh4refmentonianophc.Name = "lblh4refmentonianophc";
            this.lblh4refmentonianophc.Size = new System.Drawing.Size(108, 13);
            this.lblh4refmentonianophc.TabIndex = 12;
            this.lblh4refmentonianophc.Text = "Reflejo Mentoneano:";
            // 
            // txth4movmasticatoriosphc
            // 
            this.txth4movmasticatoriosphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth4movmasticatoriosphc.Location = new System.Drawing.Point(596, 62);
            this.txth4movmasticatoriosphc.Name = "txth4movmasticatoriosphc";
            this.txth4movmasticatoriosphc.Size = new System.Drawing.Size(361, 43);
            this.txth4movmasticatoriosphc.TabIndex = 13;
            // 
            // txth4reflejoizqphc
            // 
            this.txth4reflejoizqphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth4reflejoizqphc.Location = new System.Drawing.Point(596, 39);
            this.txth4reflejoizqphc.Name = "txth4reflejoizqphc";
            this.txth4reflejoizqphc.Properties.MaxLength = 200;
            this.txth4reflejoizqphc.Size = new System.Drawing.Size(361, 20);
            this.txth4reflejoizqphc.TabIndex = 7;
            // 
            // txth4refmentonianophc
            // 
            this.txth4refmentonianophc.Location = new System.Drawing.Point(121, 85);
            this.txth4refmentonianophc.Name = "txth4refmentonianophc";
            this.txth4refmentonianophc.Properties.MaxLength = 500;
            this.txth4refmentonianophc.Size = new System.Drawing.Size(360, 20);
            this.txth4refmentonianophc.TabIndex = 11;
            // 
            // lblh4reflejoizqphc
            // 
            this.lblh4reflejoizqphc.AutoSize = true;
            this.lblh4reflejoizqphc.Location = new System.Drawing.Point(535, 42);
            this.lblh4reflejoizqphc.Name = "lblh4reflejoizqphc";
            this.lblh4reflejoizqphc.Size = new System.Drawing.Size(56, 13);
            this.lblh4reflejoizqphc.TabIndex = 6;
            this.lblh4reflejoizqphc.Text = "Izquierdo:";
            // 
            // txth4reflejoderphc
            // 
            this.txth4reflejoderphc.Location = new System.Drawing.Point(165, 39);
            this.txth4reflejoderphc.Name = "txth4reflejoderphc";
            this.txth4reflejoderphc.Properties.MaxLength = 200;
            this.txth4reflejoderphc.Size = new System.Drawing.Size(360, 20);
            this.txth4reflejoderphc.TabIndex = 5;
            // 
            // lblh4reflejoderphc
            // 
            this.lblh4reflejoderphc.AutoSize = true;
            this.lblh4reflejoderphc.Location = new System.Drawing.Point(14, 42);
            this.lblh4reflejoderphc.Name = "lblh4reflejoderphc";
            this.lblh4reflejoderphc.Size = new System.Drawing.Size(144, 13);
            this.lblh4reflejoderphc.TabIndex = 4;
            this.lblh4reflejoderphc.Text = "Reflejo Corneano   Derecho:";
            // 
            // txth4aberturabocaphc
            // 
            this.txth4aberturabocaphc.Location = new System.Drawing.Point(121, 62);
            this.txth4aberturabocaphc.Name = "txth4aberturabocaphc";
            this.txth4aberturabocaphc.Properties.MaxLength = 500;
            this.txth4aberturabocaphc.Size = new System.Drawing.Size(360, 20);
            this.txth4aberturabocaphc.TabIndex = 9;
            // 
            // lblh4aberturabocaphc
            // 
            this.lblh4aberturabocaphc.AutoSize = true;
            this.lblh4aberturabocaphc.Location = new System.Drawing.Point(14, 65);
            this.lblh4aberturabocaphc.Name = "lblh4aberturabocaphc";
            this.lblh4aberturabocaphc.Size = new System.Drawing.Size(106, 13);
            this.lblh4aberturabocaphc.TabIndex = 8;
            this.lblh4aberturabocaphc.Text = "Abertura de la Boca:";
            // 
            // groupBox18
            // 
            this.groupBox18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox18.Controls.Add(this.txth4nistagmuxphc);
            this.groupBox18.Controls.Add(this.txth4posicionojosphc);
            this.groupBox18.Controls.Add(this.groupBox39);
            this.groupBox18.Controls.Add(this.groupBox19);
            this.groupBox18.Controls.Add(this.lblh4posicionojosphc);
            this.groupBox18.Controls.Add(this.lblh4exoftalmiaphc);
            this.groupBox18.Controls.Add(this.lblh4hornerphc);
            this.groupBox18.Controls.Add(this.txth4hornerphc);
            this.groupBox18.Controls.Add(this.txth4exoftalmiaphc);
            this.groupBox18.Controls.Add(this.lblh4movocularesphc);
            this.groupBox18.Controls.Add(this.txth4movocularesphc);
            this.groupBox18.Controls.Add(this.lblh4nistagmuxphc);
            this.groupBox18.Controls.Add(this.lblh4prosisphc);
            this.groupBox18.Controls.Add(this.txth4prosisphc);
            this.groupBox18.Controls.Add(this.lblh4convergenciaphc);
            this.groupBox18.Controls.Add(this.txth4convergenciaphc);
            this.groupBox18.Controls.Add(this.lblh4pupulasodphc);
            this.groupBox18.Controls.Add(this.txth4pupulasodphc);
            this.groupBox18.Controls.Add(this.lblh4pupulasoiphc);
            this.groupBox18.Controls.Add(this.txth4pupulasoiphc);
            this.groupBox18.Controls.Add(this.lblh4formaphc);
            this.groupBox18.Controls.Add(this.txth4formaphc);
            this.groupBox18.Controls.Add(this.lblh4fotomotorodphc);
            this.groupBox18.Controls.Add(this.txth4fotomotorodphc);
            this.groupBox18.Controls.Add(this.lblh4fotomotoroiphc);
            this.groupBox18.Controls.Add(this.txth4fotomotoroiphc);
            this.groupBox18.Controls.Add(this.lblh4consensualdaiphc);
            this.groupBox18.Controls.Add(this.txth4consensualdaiphc);
            this.groupBox18.Controls.Add(this.lblh4consensualiadphc);
            this.groupBox18.Controls.Add(this.txth4consensualiadphc);
            this.groupBox18.Location = new System.Drawing.Point(5, 214);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(965, 176);
            this.groupBox18.TabIndex = 2;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "III. IV. V. MOTOR OCULAR  COMUN -  TROCLEAR  -  MOTOR OCULAR EXTERNO";
            // 
            // txth4nistagmuxphc
            // 
            this.txth4nistagmuxphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth4nistagmuxphc.Location = new System.Drawing.Point(478, 62);
            this.txth4nistagmuxphc.Name = "txth4nistagmuxphc";
            this.txth4nistagmuxphc.Properties.MaxLength = 200;
            this.txth4nistagmuxphc.Size = new System.Drawing.Size(265, 20);
            this.txth4nistagmuxphc.TabIndex = 10;
            // 
            // txth4posicionojosphc
            // 
            this.txth4posicionojosphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth4posicionojosphc.Location = new System.Drawing.Point(569, 16);
            this.txth4posicionojosphc.Name = "txth4posicionojosphc";
            this.txth4posicionojosphc.Properties.MaxLength = 200;
            this.txth4posicionojosphc.Size = new System.Drawing.Size(381, 20);
            this.txth4posicionojosphc.TabIndex = 2;
            // 
            // groupBox39
            // 
            this.groupBox39.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox39.Controls.Add(this.txth4acomodacionodphc);
            this.groupBox39.Controls.Add(this.txth4acomodacionoiphc);
            this.groupBox39.Controls.Add(this.lblh4acomodacionoiphc);
            this.groupBox39.Controls.Add(this.lblh4acomodacionodphc);
            this.groupBox39.Location = new System.Drawing.Point(354, 83);
            this.groupBox39.Name = "groupBox39";
            this.groupBox39.Size = new System.Drawing.Size(389, 40);
            this.groupBox39.TabIndex = 14;
            this.groupBox39.TabStop = false;
            this.groupBox39.Text = "Reflejos de Acomodación";
            // 
            // txth4acomodacionodphc
            // 
            this.txth4acomodacionodphc.Location = new System.Drawing.Point(25, 15);
            this.txth4acomodacionodphc.Name = "txth4acomodacionodphc";
            this.txth4acomodacionodphc.Properties.MaxLength = 200;
            this.txth4acomodacionodphc.Size = new System.Drawing.Size(170, 20);
            this.txth4acomodacionodphc.TabIndex = 0;
            // 
            // txth4acomodacionoiphc
            // 
            this.txth4acomodacionoiphc.Location = new System.Drawing.Point(212, 15);
            this.txth4acomodacionoiphc.Name = "txth4acomodacionoiphc";
            this.txth4acomodacionoiphc.Properties.MaxLength = 200;
            this.txth4acomodacionoiphc.Size = new System.Drawing.Size(170, 20);
            this.txth4acomodacionoiphc.TabIndex = 2;
            // 
            // lblh4acomodacionoiphc
            // 
            this.lblh4acomodacionoiphc.AutoSize = true;
            this.lblh4acomodacionoiphc.Location = new System.Drawing.Point(199, 18);
            this.lblh4acomodacionoiphc.Name = "lblh4acomodacionoiphc";
            this.lblh4acomodacionoiphc.Size = new System.Drawing.Size(15, 13);
            this.lblh4acomodacionoiphc.TabIndex = 3;
            this.lblh4acomodacionoiphc.Text = "I:";
            // 
            // lblh4acomodacionodphc
            // 
            this.lblh4acomodacionodphc.AutoSize = true;
            this.lblh4acomodacionodphc.Location = new System.Drawing.Point(10, 18);
            this.lblh4acomodacionodphc.Name = "lblh4acomodacionodphc";
            this.lblh4acomodacionodphc.Size = new System.Drawing.Size(18, 13);
            this.lblh4acomodacionodphc.TabIndex = 1;
            this.lblh4acomodacionodphc.Text = "D:";
            // 
            // groupBox19
            // 
            this.groupBox19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox19.Controls.Add(this.txth4diplopia1phc);
            this.groupBox19.Controls.Add(this.txth4diplopia9phc);
            this.groupBox19.Controls.Add(this.txth4diplopia8phc);
            this.groupBox19.Controls.Add(this.txth4diplopia7phc);
            this.groupBox19.Controls.Add(this.txth4diplopia6phc);
            this.groupBox19.Controls.Add(this.txth4diplopia5phc);
            this.groupBox19.Controls.Add(this.txth4diplopia4phc);
            this.groupBox19.Controls.Add(this.txth4diplopia3phc);
            this.groupBox19.Controls.Add(this.txth4diplopia2phc);
            this.groupBox19.Location = new System.Drawing.Point(749, 37);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(208, 86);
            this.groupBox19.TabIndex = 15;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Campo de Diplopia";
            // 
            // txth4diplopia1phc
            // 
            this.txth4diplopia1phc.Location = new System.Drawing.Point(6, 20);
            this.txth4diplopia1phc.Name = "txth4diplopia1phc";
            this.txth4diplopia1phc.Properties.MaxLength = 10;
            this.txth4diplopia1phc.Size = new System.Drawing.Size(61, 20);
            this.txth4diplopia1phc.TabIndex = 2;
            // 
            // txth4diplopia9phc
            // 
            this.txth4diplopia9phc.Location = new System.Drawing.Point(140, 62);
            this.txth4diplopia9phc.Name = "txth4diplopia9phc";
            this.txth4diplopia9phc.Properties.MaxLength = 10;
            this.txth4diplopia9phc.Size = new System.Drawing.Size(61, 20);
            this.txth4diplopia9phc.TabIndex = 8;
            // 
            // txth4diplopia8phc
            // 
            this.txth4diplopia8phc.Location = new System.Drawing.Point(73, 62);
            this.txth4diplopia8phc.Name = "txth4diplopia8phc";
            this.txth4diplopia8phc.Properties.MaxLength = 10;
            this.txth4diplopia8phc.Size = new System.Drawing.Size(61, 20);
            this.txth4diplopia8phc.TabIndex = 7;
            // 
            // txth4diplopia7phc
            // 
            this.txth4diplopia7phc.Location = new System.Drawing.Point(6, 62);
            this.txth4diplopia7phc.Name = "txth4diplopia7phc";
            this.txth4diplopia7phc.Properties.MaxLength = 10;
            this.txth4diplopia7phc.Size = new System.Drawing.Size(61, 20);
            this.txth4diplopia7phc.TabIndex = 6;
            // 
            // txth4diplopia6phc
            // 
            this.txth4diplopia6phc.Location = new System.Drawing.Point(140, 41);
            this.txth4diplopia6phc.Name = "txth4diplopia6phc";
            this.txth4diplopia6phc.Properties.MaxLength = 10;
            this.txth4diplopia6phc.Size = new System.Drawing.Size(61, 20);
            this.txth4diplopia6phc.TabIndex = 5;
            // 
            // txth4diplopia5phc
            // 
            this.txth4diplopia5phc.Location = new System.Drawing.Point(73, 41);
            this.txth4diplopia5phc.Name = "txth4diplopia5phc";
            this.txth4diplopia5phc.Properties.MaxLength = 10;
            this.txth4diplopia5phc.Size = new System.Drawing.Size(61, 20);
            this.txth4diplopia5phc.TabIndex = 4;
            // 
            // txth4diplopia4phc
            // 
            this.txth4diplopia4phc.Location = new System.Drawing.Point(6, 41);
            this.txth4diplopia4phc.Name = "txth4diplopia4phc";
            this.txth4diplopia4phc.Properties.MaxLength = 10;
            this.txth4diplopia4phc.Size = new System.Drawing.Size(61, 20);
            this.txth4diplopia4phc.TabIndex = 3;
            // 
            // txth4diplopia3phc
            // 
            this.txth4diplopia3phc.Location = new System.Drawing.Point(140, 19);
            this.txth4diplopia3phc.Name = "txth4diplopia3phc";
            this.txth4diplopia3phc.Properties.MaxLength = 10;
            this.txth4diplopia3phc.Size = new System.Drawing.Size(61, 20);
            this.txth4diplopia3phc.TabIndex = 1;
            // 
            // txth4diplopia2phc
            // 
            this.txth4diplopia2phc.Location = new System.Drawing.Point(73, 19);
            this.txth4diplopia2phc.Name = "txth4diplopia2phc";
            this.txth4diplopia2phc.Properties.MaxLength = 10;
            this.txth4diplopia2phc.Size = new System.Drawing.Size(61, 20);
            this.txth4diplopia2phc.TabIndex = 0;
            // 
            // lblh4posicionojosphc
            // 
            this.lblh4posicionojosphc.AutoSize = true;
            this.lblh4posicionojosphc.Location = new System.Drawing.Point(466, 19);
            this.lblh4posicionojosphc.Name = "lblh4posicionojosphc";
            this.lblh4posicionojosphc.Size = new System.Drawing.Size(105, 13);
            this.lblh4posicionojosphc.TabIndex = 3;
            this.lblh4posicionojosphc.Text = "Posición de los Ojos:";
            // 
            // lblh4exoftalmiaphc
            // 
            this.lblh4exoftalmiaphc.AutoSize = true;
            this.lblh4exoftalmiaphc.Location = new System.Drawing.Point(14, 42);
            this.lblh4exoftalmiaphc.Name = "lblh4exoftalmiaphc";
            this.lblh4exoftalmiaphc.Size = new System.Drawing.Size(61, 13);
            this.lblh4exoftalmiaphc.TabIndex = 4;
            this.lblh4exoftalmiaphc.Text = "Exoftalmia:";
            // 
            // lblh4hornerphc
            // 
            this.lblh4hornerphc.AutoSize = true;
            this.lblh4hornerphc.Location = new System.Drawing.Point(428, 42);
            this.lblh4hornerphc.Name = "lblh4hornerphc";
            this.lblh4hornerphc.Size = new System.Drawing.Size(44, 13);
            this.lblh4hornerphc.TabIndex = 6;
            this.lblh4hornerphc.Text = "Horner:";
            // 
            // txth4hornerphc
            // 
            this.txth4hornerphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth4hornerphc.Location = new System.Drawing.Point(478, 39);
            this.txth4hornerphc.Name = "txth4hornerphc";
            this.txth4hornerphc.Properties.MaxLength = 200;
            this.txth4hornerphc.Size = new System.Drawing.Size(265, 20);
            this.txth4hornerphc.TabIndex = 7;
            // 
            // txth4exoftalmiaphc
            // 
            this.txth4exoftalmiaphc.EditValue = "";
            this.txth4exoftalmiaphc.Location = new System.Drawing.Point(133, 39);
            this.txth4exoftalmiaphc.Name = "txth4exoftalmiaphc";
            this.txth4exoftalmiaphc.Properties.MaxLength = 200;
            this.txth4exoftalmiaphc.Size = new System.Drawing.Size(264, 20);
            this.txth4exoftalmiaphc.TabIndex = 5;
            // 
            // lblh4movocularesphc
            // 
            this.lblh4movocularesphc.AutoSize = true;
            this.lblh4movocularesphc.Location = new System.Drawing.Point(14, 65);
            this.lblh4movocularesphc.Name = "lblh4movocularesphc";
            this.lblh4movocularesphc.Size = new System.Drawing.Size(115, 13);
            this.lblh4movocularesphc.TabIndex = 8;
            this.lblh4movocularesphc.Text = "Movimientos Oculares:";
            // 
            // txth4movocularesphc
            // 
            this.txth4movocularesphc.Location = new System.Drawing.Point(133, 62);
            this.txth4movocularesphc.Name = "txth4movocularesphc";
            this.txth4movocularesphc.Properties.MaxLength = 200;
            this.txth4movocularesphc.Size = new System.Drawing.Size(264, 20);
            this.txth4movocularesphc.TabIndex = 9;
            // 
            // lblh4nistagmuxphc
            // 
            this.lblh4nistagmuxphc.AutoSize = true;
            this.lblh4nistagmuxphc.Location = new System.Drawing.Point(422, 65);
            this.lblh4nistagmuxphc.Name = "lblh4nistagmuxphc";
            this.lblh4nistagmuxphc.Size = new System.Drawing.Size(60, 13);
            this.lblh4nistagmuxphc.TabIndex = 11;
            this.lblh4nistagmuxphc.Text = "Nistagmus:";
            // 
            // lblh4prosisphc
            // 
            this.lblh4prosisphc.AutoSize = true;
            this.lblh4prosisphc.Location = new System.Drawing.Point(14, 19);
            this.lblh4prosisphc.Name = "lblh4prosisphc";
            this.lblh4prosisphc.Size = new System.Drawing.Size(39, 13);
            this.lblh4prosisphc.TabIndex = 0;
            this.lblh4prosisphc.Text = "Prosis:";
            // 
            // txth4prosisphc
            // 
            this.txth4prosisphc.Location = new System.Drawing.Point(83, 16);
            this.txth4prosisphc.Name = "txth4prosisphc";
            this.txth4prosisphc.Properties.MaxLength = 200;
            this.txth4prosisphc.Size = new System.Drawing.Size(380, 20);
            this.txth4prosisphc.TabIndex = 1;
            // 
            // lblh4convergenciaphc
            // 
            this.lblh4convergenciaphc.AutoSize = true;
            this.lblh4convergenciaphc.Location = new System.Drawing.Point(14, 97);
            this.lblh4convergenciaphc.Name = "lblh4convergenciaphc";
            this.lblh4convergenciaphc.Size = new System.Drawing.Size(77, 13);
            this.lblh4convergenciaphc.TabIndex = 12;
            this.lblh4convergenciaphc.Text = "Convergencia:";
            // 
            // txth4convergenciaphc
            // 
            this.txth4convergenciaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth4convergenciaphc.Location = new System.Drawing.Point(133, 94);
            this.txth4convergenciaphc.Name = "txth4convergenciaphc";
            this.txth4convergenciaphc.Properties.MaxLength = 200;
            this.txth4convergenciaphc.Size = new System.Drawing.Size(215, 20);
            this.txth4convergenciaphc.TabIndex = 13;
            // 
            // lblh4pupulasodphc
            // 
            this.lblh4pupulasodphc.AutoSize = true;
            this.lblh4pupulasodphc.Location = new System.Drawing.Point(14, 129);
            this.lblh4pupulasodphc.Name = "lblh4pupulasodphc";
            this.lblh4pupulasodphc.Size = new System.Drawing.Size(106, 13);
            this.lblh4pupulasodphc.TabIndex = 16;
            this.lblh4pupulasodphc.Text = "Pupulas:  Tamaño D:";
            // 
            // txth4pupulasodphc
            // 
            this.txth4pupulasodphc.Location = new System.Drawing.Point(133, 126);
            this.txth4pupulasodphc.Name = "txth4pupulasodphc";
            this.txth4pupulasodphc.Properties.MaxLength = 200;
            this.txth4pupulasodphc.Size = new System.Drawing.Size(230, 20);
            this.txth4pupulasodphc.TabIndex = 17;
            // 
            // lblh4pupulasoiphc
            // 
            this.lblh4pupulasoiphc.AutoSize = true;
            this.lblh4pupulasoiphc.Location = new System.Drawing.Point(388, 129);
            this.lblh4pupulasoiphc.Name = "lblh4pupulasoiphc";
            this.lblh4pupulasoiphc.Size = new System.Drawing.Size(15, 13);
            this.lblh4pupulasoiphc.TabIndex = 18;
            this.lblh4pupulasoiphc.Text = "I:";
            // 
            // txth4pupulasoiphc
            // 
            this.txth4pupulasoiphc.Location = new System.Drawing.Point(409, 126);
            this.txth4pupulasoiphc.Name = "txth4pupulasoiphc";
            this.txth4pupulasoiphc.Properties.MaxLength = 200;
            this.txth4pupulasoiphc.Size = new System.Drawing.Size(230, 20);
            this.txth4pupulasoiphc.TabIndex = 19;
            // 
            // lblh4formaphc
            // 
            this.lblh4formaphc.AutoSize = true;
            this.lblh4formaphc.Location = new System.Drawing.Point(672, 129);
            this.lblh4formaphc.Name = "lblh4formaphc";
            this.lblh4formaphc.Size = new System.Drawing.Size(41, 13);
            this.lblh4formaphc.TabIndex = 20;
            this.lblh4formaphc.Text = "Forma:";
            // 
            // txth4formaphc
            // 
            this.txth4formaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth4formaphc.Location = new System.Drawing.Point(719, 126);
            this.txth4formaphc.Name = "txth4formaphc";
            this.txth4formaphc.Properties.MaxLength = 200;
            this.txth4formaphc.Size = new System.Drawing.Size(231, 20);
            this.txth4formaphc.TabIndex = 21;
            // 
            // lblh4fotomotorodphc
            // 
            this.lblh4fotomotorodphc.AutoSize = true;
            this.lblh4fotomotorodphc.Location = new System.Drawing.Point(14, 152);
            this.lblh4fotomotorodphc.Name = "lblh4fotomotorodphc";
            this.lblh4fotomotorodphc.Size = new System.Drawing.Size(114, 13);
            this.lblh4fotomotorodphc.TabIndex = 22;
            this.lblh4fotomotorodphc.Text = "Reflejo Fotomotor   D:";
            // 
            // txth4fotomotorodphc
            // 
            this.txth4fotomotorodphc.Location = new System.Drawing.Point(133, 149);
            this.txth4fotomotorodphc.Name = "txth4fotomotorodphc";
            this.txth4fotomotorodphc.Properties.MaxLength = 200;
            this.txth4fotomotorodphc.Size = new System.Drawing.Size(230, 20);
            this.txth4fotomotorodphc.TabIndex = 23;
            // 
            // lblh4fotomotoroiphc
            // 
            this.lblh4fotomotoroiphc.AutoSize = true;
            this.lblh4fotomotoroiphc.Location = new System.Drawing.Point(388, 152);
            this.lblh4fotomotoroiphc.Name = "lblh4fotomotoroiphc";
            this.lblh4fotomotoroiphc.Size = new System.Drawing.Size(15, 13);
            this.lblh4fotomotoroiphc.TabIndex = 24;
            this.lblh4fotomotoroiphc.Text = "I:";
            // 
            // txth4fotomotoroiphc
            // 
            this.txth4fotomotoroiphc.Location = new System.Drawing.Point(409, 149);
            this.txth4fotomotoroiphc.Name = "txth4fotomotoroiphc";
            this.txth4fotomotoroiphc.Properties.MaxLength = 200;
            this.txth4fotomotoroiphc.Size = new System.Drawing.Size(230, 20);
            this.txth4fotomotoroiphc.TabIndex = 25;
            // 
            // lblh4consensualdaiphc
            // 
            this.lblh4consensualdaiphc.AutoSize = true;
            this.lblh4consensualdaiphc.Location = new System.Drawing.Point(643, 152);
            this.lblh4consensualdaiphc.Name = "lblh4consensualdaiphc";
            this.lblh4consensualdaiphc.Size = new System.Drawing.Size(135, 13);
            this.lblh4consensualdaiphc.TabIndex = 26;
            this.lblh4consensualdaiphc.Text = "Reflejo Consensual   D a I:";
            // 
            // txth4consensualdaiphc
            // 
            this.txth4consensualdaiphc.Location = new System.Drawing.Point(784, 149);
            this.txth4consensualdaiphc.Name = "txth4consensualdaiphc";
            this.txth4consensualdaiphc.Properties.MaxLength = 200;
            this.txth4consensualdaiphc.Size = new System.Drawing.Size(59, 20);
            this.txth4consensualdaiphc.TabIndex = 27;
            // 
            // lblh4consensualiadphc
            // 
            this.lblh4consensualiadphc.AutoSize = true;
            this.lblh4consensualiadphc.Location = new System.Drawing.Point(849, 152);
            this.lblh4consensualiadphc.Name = "lblh4consensualiadphc";
            this.lblh4consensualiadphc.Size = new System.Drawing.Size(34, 13);
            this.lblh4consensualiadphc.TabIndex = 28;
            this.lblh4consensualiadphc.Text = "I a D:";
            // 
            // txth4consensualiadphc
            // 
            this.txth4consensualiadphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth4consensualiadphc.Location = new System.Drawing.Point(890, 149);
            this.txth4consensualiadphc.Name = "txth4consensualiadphc";
            this.txth4consensualiadphc.Properties.MaxLength = 200;
            this.txth4consensualiadphc.Size = new System.Drawing.Size(60, 20);
            this.txth4consensualiadphc.TabIndex = 29;
            // 
            // groupBox17
            // 
            this.groupBox17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox17.Controls.Add(this.txth4optccodphc);
            this.groupBox17.Controls.Add(this.txth4optccoiphc);
            this.groupBox17.Controls.Add(this.label2);
            this.groupBox17.Controls.Add(this.label3);
            this.groupBox17.Controls.Add(this.label1);
            this.groupBox17.Controls.Add(this.txth4campimetriaphc);
            this.groupBox17.Controls.Add(this.lblh4optscodphc);
            this.groupBox17.Controls.Add(this.txth4optscodphc);
            this.groupBox17.Controls.Add(this.lblh4optscoiphc);
            this.groupBox17.Controls.Add(this.txth4optscoiphc);
            this.groupBox17.Controls.Add(this.lblh4optccodphc);
            this.groupBox17.Controls.Add(this.lblh4optccoiphc);
            this.groupBox17.Controls.Add(this.lblh4fondoodphc);
            this.groupBox17.Controls.Add(this.txth4fondoodphc);
            this.groupBox17.Controls.Add(this.lblh4fondooiphc);
            this.groupBox17.Controls.Add(this.txth4fondooiphc);
            this.groupBox17.Controls.Add(this.lblh4campimetriaphc);
            this.groupBox17.Location = new System.Drawing.Point(5, 66);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(965, 142);
            this.groupBox17.TabIndex = 1;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "II: ÓPTICO";
            // 
            // txth4optccodphc
            // 
            this.txth4optccodphc.Location = new System.Drawing.Point(620, 14);
            this.txth4optccodphc.Name = "txth4optccodphc";
            this.txth4optccodphc.Properties.MaxLength = 200;
            this.txth4optccodphc.Size = new System.Drawing.Size(150, 20);
            this.txth4optccodphc.TabIndex = 7;
            // 
            // txth4optccoiphc
            // 
            this.txth4optccoiphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth4optccoiphc.Location = new System.Drawing.Point(799, 14);
            this.txth4optccoiphc.Name = "txth4optccoiphc";
            this.txth4optccoiphc.Properties.MaxLength = 200;
            this.txth4optccoiphc.Size = new System.Drawing.Size(151, 20);
            this.txth4optccoiphc.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(534, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "C/Correc.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(123, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Derecho:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(148, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "OD:";
            // 
            // txth4campimetriaphc
            // 
            this.txth4campimetriaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth4campimetriaphc.Location = new System.Drawing.Point(178, 60);
            this.txth4campimetriaphc.Name = "txth4campimetriaphc";
            this.txth4campimetriaphc.Size = new System.Drawing.Size(772, 76);
            this.txth4campimetriaphc.TabIndex = 16;
            // 
            // lblh4optscodphc
            // 
            this.lblh4optscodphc.AutoSize = true;
            this.lblh4optscodphc.Location = new System.Drawing.Point(14, 17);
            this.lblh4optscodphc.Name = "lblh4optscodphc";
            this.lblh4optscodphc.Size = new System.Drawing.Size(132, 13);
            this.lblh4optscodphc.TabIndex = 0;
            this.lblh4optscodphc.Text = "Agudez Visual:   S/Correc.";
            // 
            // txth4optscodphc
            // 
            this.txth4optscodphc.Location = new System.Drawing.Point(178, 14);
            this.txth4optscodphc.Name = "txth4optscodphc";
            this.txth4optscodphc.Properties.MaxLength = 200;
            this.txth4optscodphc.Size = new System.Drawing.Size(150, 20);
            this.txth4optscodphc.TabIndex = 2;
            // 
            // lblh4optscoiphc
            // 
            this.lblh4optscoiphc.AutoSize = true;
            this.lblh4optscoiphc.Location = new System.Drawing.Point(341, 17);
            this.lblh4optscoiphc.Name = "lblh4optscoiphc";
            this.lblh4optscoiphc.Size = new System.Drawing.Size(23, 13);
            this.lblh4optscoiphc.TabIndex = 3;
            this.lblh4optscoiphc.Text = "OI:";
            // 
            // txth4optscoiphc
            // 
            this.txth4optscoiphc.Location = new System.Drawing.Point(364, 14);
            this.txth4optscoiphc.Name = "txth4optscoiphc";
            this.txth4optscoiphc.Properties.MaxLength = 200;
            this.txth4optscoiphc.Size = new System.Drawing.Size(150, 20);
            this.txth4optscoiphc.TabIndex = 4;
            // 
            // lblh4optccodphc
            // 
            this.lblh4optccodphc.AutoSize = true;
            this.lblh4optccodphc.Location = new System.Drawing.Point(588, 17);
            this.lblh4optccodphc.Name = "lblh4optccodphc";
            this.lblh4optccodphc.Size = new System.Drawing.Size(26, 13);
            this.lblh4optccodphc.TabIndex = 6;
            this.lblh4optccodphc.Text = "OD:";
            // 
            // lblh4optccoiphc
            // 
            this.lblh4optccoiphc.AutoSize = true;
            this.lblh4optccoiphc.Location = new System.Drawing.Point(770, 17);
            this.lblh4optccoiphc.Name = "lblh4optccoiphc";
            this.lblh4optccoiphc.Size = new System.Drawing.Size(23, 13);
            this.lblh4optccoiphc.TabIndex = 8;
            this.lblh4optccoiphc.Text = "OI:";
            // 
            // lblh4fondoodphc
            // 
            this.lblh4fondoodphc.AutoSize = true;
            this.lblh4fondoodphc.Location = new System.Drawing.Point(14, 40);
            this.lblh4fondoodphc.Name = "lblh4fondoodphc";
            this.lblh4fondoodphc.Size = new System.Drawing.Size(76, 13);
            this.lblh4fondoodphc.TabIndex = 10;
            this.lblh4fondoodphc.Text = "Fondo de Ojo:";
            // 
            // txth4fondoodphc
            // 
            this.txth4fondoodphc.Location = new System.Drawing.Point(178, 37);
            this.txth4fondoodphc.Name = "txth4fondoodphc";
            this.txth4fondoodphc.Properties.MaxLength = 200;
            this.txth4fondoodphc.Size = new System.Drawing.Size(336, 20);
            this.txth4fondoodphc.TabIndex = 12;
            // 
            // lblh4fondooiphc
            // 
            this.lblh4fondooiphc.AutoSize = true;
            this.lblh4fondooiphc.Location = new System.Drawing.Point(558, 40);
            this.lblh4fondooiphc.Name = "lblh4fondooiphc";
            this.lblh4fondooiphc.Size = new System.Drawing.Size(56, 13);
            this.lblh4fondooiphc.TabIndex = 13;
            this.lblh4fondooiphc.Text = "Izquierdo:";
            // 
            // txth4fondooiphc
            // 
            this.txth4fondooiphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth4fondooiphc.Location = new System.Drawing.Point(620, 37);
            this.txth4fondooiphc.Name = "txth4fondooiphc";
            this.txth4fondooiphc.Properties.MaxLength = 200;
            this.txth4fondooiphc.Size = new System.Drawing.Size(330, 20);
            this.txth4fondooiphc.TabIndex = 14;
            // 
            // lblh4campimetriaphc
            // 
            this.lblh4campimetriaphc.AutoSize = true;
            this.lblh4campimetriaphc.Location = new System.Drawing.Point(14, 63);
            this.lblh4campimetriaphc.Name = "lblh4campimetriaphc";
            this.lblh4campimetriaphc.Size = new System.Drawing.Size(160, 13);
            this.lblh4campimetriaphc.TabIndex = 15;
            this.lblh4campimetriaphc.Text = "Campimetría por Confrontación:";
            // 
            // groupBox16
            // 
            this.groupBox16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox16.Controls.Add(this.lblh4olfnormalphc);
            this.groupBox16.Controls.Add(this.txth4olfnormalphc);
            this.groupBox16.Controls.Add(this.lblh4olfanosmiaphc);
            this.groupBox16.Controls.Add(this.txth4olfanosmiaphc);
            this.groupBox16.Location = new System.Drawing.Point(5, 25);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(965, 40);
            this.groupBox16.TabIndex = 0;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "I. OLFATORIO";
            // 
            // lblh4olfnormalphc
            // 
            this.lblh4olfnormalphc.AutoSize = true;
            this.lblh4olfnormalphc.Location = new System.Drawing.Point(14, 17);
            this.lblh4olfnormalphc.Name = "lblh4olfnormalphc";
            this.lblh4olfnormalphc.Size = new System.Drawing.Size(44, 13);
            this.lblh4olfnormalphc.TabIndex = 0;
            this.lblh4olfnormalphc.Text = "Normal:";
            // 
            // txth4olfnormalphc
            // 
            this.txth4olfnormalphc.Location = new System.Drawing.Point(83, 14);
            this.txth4olfnormalphc.Name = "txth4olfnormalphc";
            this.txth4olfnormalphc.Properties.MaxLength = 200;
            this.txth4olfnormalphc.Size = new System.Drawing.Size(380, 20);
            this.txth4olfnormalphc.TabIndex = 1;
            // 
            // lblh4olfanosmiaphc
            // 
            this.lblh4olfanosmiaphc.AutoSize = true;
            this.lblh4olfanosmiaphc.Location = new System.Drawing.Point(484, 17);
            this.lblh4olfanosmiaphc.Name = "lblh4olfanosmiaphc";
            this.lblh4olfanosmiaphc.Size = new System.Drawing.Size(51, 13);
            this.lblh4olfanosmiaphc.TabIndex = 2;
            this.lblh4olfanosmiaphc.Text = "Anosmia:";
            // 
            // txth4olfanosmiaphc
            // 
            this.txth4olfanosmiaphc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txth4olfanosmiaphc.Location = new System.Drawing.Point(569, 14);
            this.txth4olfanosmiaphc.Name = "txth4olfanosmiaphc";
            this.txth4olfanosmiaphc.Properties.MaxLength = 200;
            this.txth4olfanosmiaphc.Size = new System.Drawing.Size(381, 20);
            this.txth4olfanosmiaphc.TabIndex = 3;
            // 
            // btnAceptar
            // 
            this.btnAceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnAceptar.Appearance.Options.UseBackColor = true;
            this.btnAceptar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAceptar.Location = new System.Drawing.Point(888, 637);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(108, 34);
            this.btnAceptar.TabIndex = 3;
            this.btnAceptar.Text = "&Guardar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Location = new System.Drawing.Point(774, 637);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(108, 34);
            this.btnCancelar.TabIndex = 2;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // btnAuditoría
            // 
            this.btnAuditoría.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAuditoría.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAuditoría.Location = new System.Drawing.Point(660, 637);
            this.btnAuditoría.Name = "btnAuditoría";
            this.btnAuditoría.Size = new System.Drawing.Size(108, 34);
            this.btnAuditoría.TabIndex = 1;
            this.btnAuditoría.Text = "&Auditoría";
            this.btnAuditoría.Click += new System.EventHandler(this.btnAuditoría_Click);
            // 
            // FPacHistoriaClinica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 682);
            this.Controls.Add(this.btnAuditoría);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.tabHistoriaClinica);
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("FPacHistoriaClinica.IconOptions.Icon")));
            this.Name = "FPacHistoriaClinica";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Historia Clinica";
            this.Load += new System.EventHandler(this.fPacHistoriaClinica_Load);
            this.xtraTabPage7.ResumeLayout(false);
            this.xtraScrollableControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl20)).EndInit();
            this.groupControl20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txth7tratamientophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl19)).EndInit();
            this.groupControl19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txth7conductaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl18)).EndInit();
            this.groupControl18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txth7diagnosticosphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl17)).EndInit();
            this.groupControl17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txth7resumenphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl16)).EndInit();
            this.groupControl16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txth7colvertebralphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl15)).EndInit();
            this.groupControl15.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txth7nerviosperifphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl14)).EndInit();
            this.groupControl14.ResumeLayout(false);
            this.groupControl14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth7pulcarotideosphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth7cuellophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth7cabezaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth7pultemporalesphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl13)).EndInit();
            this.groupControl13.ResumeLayout(false);
            this.groupControl13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth7rigideznucaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth7hiperestesiaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth7sigbrudzinskiphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth7fotofobiaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth7sigkerningphc.Properties)).EndInit();
            this.xtraTabPage6.ResumeLayout(false);
            this.xtraScrollableControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl12)).EndInit();
            this.groupControl12.ResumeLayout(false);
            this.groupControl12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth6localizacionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6grafestesiaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6estereognosiaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6extincionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6dospuntosphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6vibracionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6posicionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6dolorprofphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6sistsensitivophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picSilueta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl11)).EndInit();
            this.groupControl11.ResumeLayout(false);
            this.groupBox33.ResumeLayout(false);
            this.groupBox33.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth6otrmaxilarphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6otrglabelarphc.Properties)).EndInit();
            this.groupBox32.ResumeLayout(false);
            this.groupBox32.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth6supbicderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6supbicizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6suptriderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6suptriizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6supestderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6supestizqphc.Properties)).EndInit();
            this.groupBox31.ResumeLayout(false);
            this.groupBox31.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth6patpladerphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6patplaizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6pathofderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6pathofizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6pattroderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6pattroizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6patprederphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6patpmenizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6patpreizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6patpmenderphc.Properties)).EndInit();
            this.groupBox30.ResumeLayout(false);
            this.groupBox30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotbicderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotbicizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miottriderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miottriizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotestderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotestizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotpatderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotpatizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotaquderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6miotaquizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos01phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos02phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos03phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos04phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos05phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos06phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos07phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos08phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos09phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth6reflejos10phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picManiqui)).EndInit();
            this.xtraTabPage5.ResumeLayout(false);
            this.xtraScrollableControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl10)).EndInit();
            this.groupControl10.ResumeLayout(false);
            this.groupControl10.PerformLayout();
            this.groupBox29.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txth5movinvoluntariosphc.Properties)).EndInit();
            this.groupBox28.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txth5fuemuscularphc.Properties)).EndInit();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth5habilidadespecifphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5rebotephc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5movrapidphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5talrodizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5talrodderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5deddedizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5deddedderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5dednarizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5dednarderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5rombergphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5equilibratoriaphc.Properties)).EndInit();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth5fasciculacionesphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5volumenphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5tonophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5marchaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl9)).EndInit();
            this.groupControl9.ResumeLayout(false);
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth5desviacionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5atrofiaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5fasciculacionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5fuerzaphc.Properties)).EndInit();
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth5esternocleidomastoideophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5trapeciophc.Properties)).EndInit();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth5elevpaladarphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5refnauceosophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5uvulaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5deglucionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5tonovozphc.Properties)).EndInit();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth5otoscopiaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5aguaudiderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5aguaudiizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5weberphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5rinnephc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth5pruebaslabphc.Properties)).EndInit();
            this.xtraTabPage3.ResumeLayout(false);
            this.xtraScrollableControl2.ResumeLayout(false);
            this.xtraScrollableControl8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl7)).EndInit();
            this.groupControl7.ResumeLayout(false);
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3aprdesatencionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3anosognosiaphc.Properties)).EndInit();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3aprideacionalphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3aprideomotoraphc.Properties)).EndInit();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3agnauditivaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3agnvisualphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3agntactilphc.Properties)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3lpdagrafiaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3lpdagnosiaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3lpdacalculiaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3lpddesorientacionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl6)).EndInit();
            this.groupControl6.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3lesagrafiaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3lesalexiaphc.Properties)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox38.ResumeLayout(false);
            this.groupBox38.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3cgeexpresionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3cgenominacionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3cgerepeticionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3cgecomprensionphc.Properties)).EndInit();
            this.groupBox37.ResumeLayout(false);
            this.groupBox37.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3aprmotoraphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3aprsensorialphc.Properties)).EndInit();
            this.groupBox36.ResumeLayout(false);
            this.groupBox36.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3atacomprensionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3ataexpresionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3atamixtaphc.Properties)).EndInit();
            this.groupBox35.ResumeLayout(false);
            this.groupBox35.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3atrmotoraphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3atrsensitivaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3atrmixtaphc.Properties)).EndInit();
            this.groupBox34.ResumeLayout(false);
            this.groupBox34.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3apebrocaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3apeconduccionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3apewernickephc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3apeglobalphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3apeanomiaphc.Properties)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3hdoizquierdophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3hdoderechophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3conrecientephc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3conremotaphc.Properties)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth3afeplanophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3afeeuforicophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3afeinadecuadophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3afehostilphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3afelabilphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth3afedeprimidophc.Properties)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            this.xtraScrollableControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl22)).EndInit();
            this.groupControl22.ResumeLayout(false);
            this.groupControl22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth2paedelusionesphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2paeilsuionesphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2paealucinacionesphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl21)).EndInit();
            this.groupControl21.ResumeLayout(false);
            this.groupControl21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth2pagconfusionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2pagdeliriophc.Properties)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth2atgdesatentophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2atgnegligentephc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2atgnegativophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2atgfluctuantephc.Properties)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txth2conocimientosgenerales.Properties)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth2razideaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2razjuiciophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2razabstraccionphc.Properties)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth2orilugarphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2oriespaciophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2oripersonaphc.Properties)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth2conalertaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2concomaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2conestuforphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2consomnolenciaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2conconfusionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            this.groupControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth2extremidadesphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2genitourinariophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2abdomenphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2toraxphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2cuellophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth2cabezaphc.Properties)).EndInit();
            this.xtraTabPage1.ResumeLayout(false);
            this.xtraScrollableControl7.ResumeLayout(false);
            this.xtraScrollableControl7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            this.groupControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth1examenfisicophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1revisionphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1otrosphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1Cirugiasphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1pcphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1pesophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1tallaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1tempphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1infeccionesphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1frphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1traumatismosphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1fcphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1cardiovascularphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1taphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1metabolicophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1toxicophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1familiarphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1fupphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1ginecoobsphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1fumphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1mesntruacionesphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudh1embarazosphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudh1abortosphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudh1gestaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1enfermedadphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth1motivophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabHistoriaClinica)).EndInit();
            this.tabHistoriaClinica.ResumeLayout(false);
            this.xtraTabPage4.ResumeLayout(false);
            this.xtraScrollableControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl8)).EndInit();
            this.groupControl8.ResumeLayout(false);
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth4facsimetriaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4facmovimientosphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4faccentralphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4facparalisisphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4facperifericaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4facgustophc.Properties)).EndInit();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth4senfacializqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4senfacialderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4movmasticatoriosphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4reflejoizqphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4refmentonianophc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4reflejoderphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4aberturabocaphc.Properties)).EndInit();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth4nistagmuxphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4posicionojosphc.Properties)).EndInit();
            this.groupBox39.ResumeLayout(false);
            this.groupBox39.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth4acomodacionodphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4acomodacionoiphc.Properties)).EndInit();
            this.groupBox19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia1phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia9phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia8phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia7phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia6phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia5phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia4phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia3phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4diplopia2phc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4hornerphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4exoftalmiaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4movocularesphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4prosisphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4convergenciaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4pupulasodphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4pupulasoiphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4formaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4fotomotorodphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4fotomotoroiphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4consensualdaiphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4consensualiadphc.Properties)).EndInit();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth4optccodphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4optccoiphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4campimetriaphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4optscodphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4optscoiphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4fondoodphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4fondooiphc.Properties)).EndInit();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txth4olfnormalphc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txth4olfanosmiaphc.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabPage xtraTabPage7;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage6;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage5;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage3;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabControl tabHistoriaClinica;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage4;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl6;
        private DevExpress.XtraEditors.GroupControl groupControl20;
        private DevExpress.XtraEditors.MemoEdit txth7tratamientophc;
        private DevExpress.XtraEditors.GroupControl groupControl19;
        private DevExpress.XtraEditors.MemoEdit txth7conductaphc;
        private DevExpress.XtraEditors.GroupControl groupControl18;
        private DevExpress.XtraEditors.MemoEdit txth7diagnosticosphc;
        private DevExpress.XtraEditors.GroupControl groupControl17;
        private DevExpress.XtraEditors.MemoEdit txth7resumenphc;
        private DevExpress.XtraEditors.GroupControl groupControl16;
        private DevExpress.XtraEditors.MemoEdit txth7colvertebralphc;
        private DevExpress.XtraEditors.GroupControl groupControl15;
        private DevExpress.XtraEditors.MemoEdit txth7nerviosperifphc;
        private DevExpress.XtraEditors.GroupControl groupControl14;
        private DevExpress.XtraEditors.TextEdit txth7pulcarotideosphc;
        internal System.Windows.Forms.Label lblh7pulcarotideosphc;
        internal System.Windows.Forms.Label lblh7cuellophc;
        internal System.Windows.Forms.Label lblh7cabezaphc;
        private DevExpress.XtraEditors.TextEdit txth7cuellophc;
        internal System.Windows.Forms.Label lblh7pultemporalesphc;
        private DevExpress.XtraEditors.TextEdit txth7cabezaphc;
        private DevExpress.XtraEditors.TextEdit txth7pultemporalesphc;
        private DevExpress.XtraEditors.GroupControl groupControl13;
        private DevExpress.XtraEditors.TextEdit txth7rigideznucaphc;
        internal System.Windows.Forms.Label lblh7hiperestesiaphc;
        internal System.Windows.Forms.Label lblh7sigbrudzinskiphc;
        private DevExpress.XtraEditors.TextEdit txth7hiperestesiaphc;
        internal System.Windows.Forms.Label lblh7fotofobiaphc;
        private DevExpress.XtraEditors.TextEdit txth7sigbrudzinskiphc;
        internal System.Windows.Forms.Label lblh7sigkerningphc;
        private DevExpress.XtraEditors.TextEdit txth7fotofobiaphc;
        internal System.Windows.Forms.Label lblh7rigideznucaphc;
        private DevExpress.XtraEditors.TextEdit txth7sigkerningphc;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl7;
        private DevExpress.XtraEditors.MemoEdit txth1enfermedadphc;
        private DevExpress.XtraEditors.MemoEdit txth1motivophc;
        internal System.Windows.Forms.Label labelMotivoDeConsulta;
        internal System.Windows.Forms.Label labelEnfermedadActual;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl1;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private System.Windows.Forms.GroupBox groupBox6;
        private DevExpress.XtraEditors.GroupControl groupControl22;
        private DevExpress.XtraEditors.TextEdit txth2paedelusionesphc;
        internal System.Windows.Forms.Label lblh2paeilsuionesphc;
        internal System.Windows.Forms.Label lblh2paedelusionesphc;
        private DevExpress.XtraEditors.TextEdit txth2paeilsuionesphc;
        internal System.Windows.Forms.Label lblh2paealucinacionesphc;
        private DevExpress.XtraEditors.TextEdit txth2paealucinacionesphc;
        private DevExpress.XtraEditors.GroupControl groupControl21;
        private DevExpress.XtraEditors.TextEdit txth2pagconfusionphc;
        internal System.Windows.Forms.Label lblh2pagconfusionphc;
        internal System.Windows.Forms.Label lblh2pagdeliriophc;
        private DevExpress.XtraEditors.TextEdit txth2pagdeliriophc;
        private System.Windows.Forms.GroupBox groupBox5;
        private DevExpress.XtraEditors.TextEdit txth2atgdesatentophc;
        internal System.Windows.Forms.Label lblh2atgdesatentophc;
        private DevExpress.XtraEditors.TextEdit txth2atgnegligentephc;
        internal System.Windows.Forms.Label lblh2atgnegligentephc;
        private DevExpress.XtraEditors.TextEdit txth2atgnegativophc;
        internal System.Windows.Forms.Label lblh2atgnegativophc;
        private DevExpress.XtraEditors.TextEdit txth2atgfluctuantephc;
        internal System.Windows.Forms.Label lblh2atgfluctuantephc;
        private System.Windows.Forms.GroupBox groupBox4;
        private DevExpress.XtraEditors.MemoEdit txth2conocimientosgenerales;
        private System.Windows.Forms.GroupBox groupBox3;
        private DevExpress.XtraEditors.TextEdit txth2razideaphc;
        internal System.Windows.Forms.Label lblh2razideaphc;
        private DevExpress.XtraEditors.TextEdit txth2razjuiciophc;
        internal System.Windows.Forms.Label lblh2razjuiciophc;
        internal System.Windows.Forms.Label lblh2razabstraccionphc;
        private DevExpress.XtraEditors.TextEdit txth2razabstraccionphc;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.TextEdit txth2orilugarphc;
        internal System.Windows.Forms.Label lblh2orilugarphc;
        private DevExpress.XtraEditors.TextEdit txth2oriespaciophc;
        internal System.Windows.Forms.Label lblh2oriespaciophc;
        private DevExpress.XtraEditors.TextEdit txth2oripersonaphc;
        internal System.Windows.Forms.Label lblh2oripersonaphc;
        private System.Windows.Forms.GroupBox groupBox1;
        internal System.Windows.Forms.Label lblh2conconfusionphc;
        private DevExpress.XtraEditors.TextEdit txth2conalertaphc;
        private DevExpress.XtraEditors.TextEdit txth2concomaphc;
        internal System.Windows.Forms.Label lblh2concomaphc;
        private DevExpress.XtraEditors.TextEdit txth2conestuforphc;
        internal System.Windows.Forms.Label lblh2conestuforphc;
        internal System.Windows.Forms.Label lblh2conalertaphc;
        private DevExpress.XtraEditors.TextEdit txth2consomnolenciaphc;
        internal System.Windows.Forms.Label lblh2consomnolenciaphc;
        private DevExpress.XtraEditors.TextEdit txth2conconfusionphc;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.MemoEdit txth2extremidadesphc;
        private DevExpress.XtraEditors.MemoEdit txth2genitourinariophc;
        private DevExpress.XtraEditors.MemoEdit txth2abdomenphc;
        private DevExpress.XtraEditors.MemoEdit txth2toraxphc;
        private DevExpress.XtraEditors.MemoEdit txth2cuellophc;
        private DevExpress.XtraEditors.MemoEdit txth2cabezaphc;
        internal System.Windows.Forms.Label lblh2cabezaphc;
        internal System.Windows.Forms.Label lblh2extremidadesphc;
        internal System.Windows.Forms.Label lblh2cuellophc;
        internal System.Windows.Forms.Label lblh2genitourinariophc;
        internal System.Windows.Forms.Label lblh2toraxphc;
        internal System.Windows.Forms.Label lblh2abdomenphc;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.MemoEdit txth1examenfisicophc;
        private DevExpress.XtraEditors.MemoEdit txth1revisionphc;
        private DevExpress.XtraEditors.MemoEdit txth1otrosphc;
        private DevExpress.XtraEditors.TextEdit txth1Cirugiasphc;
        private DevExpress.XtraEditors.TextEdit txth1pcphc;
        internal System.Windows.Forms.Label labelPC;
        private DevExpress.XtraEditors.TextEdit txth1pesophc;
        internal System.Windows.Forms.Label labelPESO;
        internal System.Windows.Forms.Label labelCirugias;
        private DevExpress.XtraEditors.TextEdit txth1tallaphc;
        internal System.Windows.Forms.Label labelTALLA;
        internal System.Windows.Forms.Label labelInfecciones;
        private DevExpress.XtraEditors.TextEdit txth1tempphc;
        private DevExpress.XtraEditors.TextEdit txth1infeccionesphc;
        internal System.Windows.Forms.Label labelTEMP;
        internal System.Windows.Forms.Label labelTraumatismos;
        private DevExpress.XtraEditors.TextEdit txth1frphc;
        private DevExpress.XtraEditors.TextEdit txth1traumatismosphc;
        internal System.Windows.Forms.Label labelFR;
        internal System.Windows.Forms.Label labelCardiovascular;
        private DevExpress.XtraEditors.TextEdit txth1fcphc;
        private DevExpress.XtraEditors.TextEdit txth1cardiovascularphc;
        internal System.Windows.Forms.Label labelFC;
        internal System.Windows.Forms.Label labelMetabolico;
        private DevExpress.XtraEditors.TextEdit txth1taphc;
        private DevExpress.XtraEditors.TextEdit txth1metabolicophc;
        internal System.Windows.Forms.Label labelTA;
        internal System.Windows.Forms.Label labelToxico;
        private DevExpress.XtraEditors.TextEdit txth1toxicophc;
        internal System.Windows.Forms.Label labelExamenFisicoGeneral;
        internal System.Windows.Forms.Label labelFamiliar;
        private DevExpress.XtraEditors.TextEdit txth1familiarphc;
        internal System.Windows.Forms.Label labelRevisionPorSistemas;
        internal System.Windows.Forms.Label labelGinecoObstetrico;
        private DevExpress.XtraEditors.TextEdit txth1fupphc;
        private DevExpress.XtraEditors.TextEdit txth1ginecoobsphc;
        internal System.Windows.Forms.Label labelFUP;
        internal System.Windows.Forms.Label labelOtros;
        private DevExpress.XtraEditors.TextEdit txth1fumphc;
        internal System.Windows.Forms.Label labelFUM;
        internal System.Windows.Forms.Label labelEmbarazos;
        private DevExpress.XtraEditors.TextEdit txth1mesntruacionesphc;
        private DevExpress.XtraEditors.TextEdit nudh1embarazosphc;
        internal System.Windows.Forms.Label labelMesntruaciones;
        internal System.Windows.Forms.Label labelGesta;
        private DevExpress.XtraEditors.TextEdit nudh1abortosphc;
        private DevExpress.XtraEditors.TextEdit nudh1gestaphc;
        internal System.Windows.Forms.Label labelAbortos;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl5;
        private DevExpress.XtraEditors.GroupControl groupControl12;
        private DevExpress.XtraEditors.LabelControl lblh6dolorprofphc;
        internal System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.MemoEdit txth6localizacionphc;
        private DevExpress.XtraEditors.MemoEdit txth6grafestesiaphc;
        private DevExpress.XtraEditors.MemoEdit txth6estereognosiaphc;
        private DevExpress.XtraEditors.MemoEdit txth6extincionphc;
        private DevExpress.XtraEditors.MemoEdit txth6dospuntosphc;
        private DevExpress.XtraEditors.MemoEdit txth6vibracionphc;
        private DevExpress.XtraEditors.MemoEdit txth6posicionphc;
        private DevExpress.XtraEditors.MemoEdit txth6dolorprofphc;
        private DevExpress.XtraEditors.SimpleButton btnRecargar;
        private DevExpress.XtraEditors.TextEdit txth6sistsensitivophc;
        private System.Windows.Forms.PictureBox picSilueta;
        internal System.Windows.Forms.Label lblh6extincionphc;
        internal System.Windows.Forms.Label lblh6estereognosiaphc;
        internal System.Windows.Forms.Label lblh6dospuntosphc;
        internal System.Windows.Forms.Label lblh6grafestesiaphc;
        internal System.Windows.Forms.Label lblh6vibracionphc;
        internal System.Windows.Forms.Label lblh6localizacionphc;
        internal System.Windows.Forms.Label lblh6posicionphc;
        private DevExpress.XtraEditors.GroupControl groupControl11;
        private System.Windows.Forms.GroupBox groupBox33;
        private DevExpress.XtraEditors.TextEdit txth6otrmaxilarphc;
        internal System.Windows.Forms.Label lblh6otrmaxilarphc;
        private DevExpress.XtraEditors.TextEdit txth6otrglabelarphc;
        internal System.Windows.Forms.Label lblh6otrglabelarphc;
        private System.Windows.Forms.GroupBox groupBox32;
        internal System.Windows.Forms.Label label29;
        internal System.Windows.Forms.Label label30;
        internal System.Windows.Forms.Label label27;
        internal System.Windows.Forms.Label label28;
        internal System.Windows.Forms.Label label25;
        internal System.Windows.Forms.Label label26;
        private DevExpress.XtraEditors.TextEdit txth6supbicderphc;
        internal System.Windows.Forms.Label lblh6supbicderphc;
        private DevExpress.XtraEditors.TextEdit txth6supbicizqphc;
        private DevExpress.XtraEditors.TextEdit txth6suptriderphc;
        internal System.Windows.Forms.Label lblh6suptriderphc;
        private DevExpress.XtraEditors.TextEdit txth6suptriizqphc;
        private DevExpress.XtraEditors.TextEdit txth6supestderphc;
        internal System.Windows.Forms.Label lblh6supestderphc;
        private DevExpress.XtraEditors.TextEdit txth6supestizqphc;
        private System.Windows.Forms.GroupBox groupBox31;
        internal System.Windows.Forms.Label label23;
        internal System.Windows.Forms.Label label24;
        internal System.Windows.Forms.Label label21;
        internal System.Windows.Forms.Label label22;
        internal System.Windows.Forms.Label label19;
        internal System.Windows.Forms.Label label20;
        internal System.Windows.Forms.Label label17;
        internal System.Windows.Forms.Label label18;
        internal System.Windows.Forms.Label label15;
        internal System.Windows.Forms.Label label16;
        private DevExpress.XtraEditors.TextEdit txth6patpladerphc;
        internal System.Windows.Forms.Label lblh6patpladerphc;
        private DevExpress.XtraEditors.TextEdit txth6patplaizqphc;
        private DevExpress.XtraEditors.TextEdit txth6pathofderphc;
        internal System.Windows.Forms.Label lblh6pathofderphc;
        private DevExpress.XtraEditors.TextEdit txth6pathofizqphc;
        private DevExpress.XtraEditors.TextEdit txth6pattroderphc;
        internal System.Windows.Forms.Label lblh6pattroderphc;
        private DevExpress.XtraEditors.TextEdit txth6pattroizqphc;
        private DevExpress.XtraEditors.TextEdit txth6patprederphc;
        internal System.Windows.Forms.Label lblh6patprederphc;
        internal System.Windows.Forms.Label lblh6patpmenderphc;
        private DevExpress.XtraEditors.TextEdit txth6patpmenizqphc;
        private DevExpress.XtraEditors.TextEdit txth6patpreizqphc;
        private DevExpress.XtraEditors.TextEdit txth6patpmenderphc;
        private System.Windows.Forms.GroupBox groupBox30;
        internal System.Windows.Forms.Label label13;
        internal System.Windows.Forms.Label label14;
        internal System.Windows.Forms.Label label11;
        internal System.Windows.Forms.Label label12;
        internal System.Windows.Forms.Label label9;
        internal System.Windows.Forms.Label label10;
        internal System.Windows.Forms.Label label7;
        internal System.Windows.Forms.Label label8;
        internal System.Windows.Forms.Label label6;
        internal System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.TextEdit txth6miotbicderphc;
        private DevExpress.XtraEditors.TextEdit txth6miotbicizqphc;
        private DevExpress.XtraEditors.TextEdit txth6miottriderphc;
        internal System.Windows.Forms.Label lblh6miotbicderphc;
        private DevExpress.XtraEditors.TextEdit txth6miottriizqphc;
        private DevExpress.XtraEditors.TextEdit txth6miotestderphc;
        internal System.Windows.Forms.Label lblh6miottriderphc;
        private DevExpress.XtraEditors.TextEdit txth6miotestizqphc;
        private DevExpress.XtraEditors.TextEdit txth6miotpatderphc;
        internal System.Windows.Forms.Label lblh6miotestderphc;
        private DevExpress.XtraEditors.TextEdit txth6miotpatizqphc;
        private DevExpress.XtraEditors.TextEdit txth6miotaquderphc;
        internal System.Windows.Forms.Label lblh6miotaquderphc;
        internal System.Windows.Forms.Label lblh6miotpatderphc;
        private DevExpress.XtraEditors.TextEdit txth6miotaquizqphc;
        private DevExpress.XtraEditors.TextEdit txth6reflejos01phc;
        private DevExpress.XtraEditors.TextEdit txth6reflejos02phc;
        private DevExpress.XtraEditors.TextEdit txth6reflejos03phc;
        private DevExpress.XtraEditors.TextEdit txth6reflejos04phc;
        private DevExpress.XtraEditors.TextEdit txth6reflejos05phc;
        private DevExpress.XtraEditors.TextEdit txth6reflejos06phc;
        private DevExpress.XtraEditors.TextEdit txth6reflejos07phc;
        private DevExpress.XtraEditors.TextEdit txth6reflejos08phc;
        private DevExpress.XtraEditors.TextEdit txth6reflejos09phc;
        private DevExpress.XtraEditors.TextEdit txth6reflejos10phc;
        private System.Windows.Forms.PictureBox picManiqui;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl4;
        private DevExpress.XtraEditors.GroupControl groupControl10;
        private System.Windows.Forms.GroupBox groupBox29;
        private DevExpress.XtraEditors.MemoEdit txth5movinvoluntariosphc;
        private System.Windows.Forms.GroupBox groupBox28;
        private DevExpress.XtraEditors.MemoEdit txth5fuemuscularphc;
        private System.Windows.Forms.GroupBox groupBox27;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.MemoEdit txth5habilidadespecifphc;
        private DevExpress.XtraEditors.MemoEdit txth5rebotephc;
        private DevExpress.XtraEditors.MemoEdit txth5movrapidphc;
        internal System.Windows.Forms.Label lblh5rebotephc;
        private DevExpress.XtraEditors.TextEdit txth5talrodizqphc;
        internal System.Windows.Forms.Label lblh5talrodizqphc;
        private DevExpress.XtraEditors.TextEdit txth5talrodderphc;
        internal System.Windows.Forms.Label lblh5talrodderphc;
        private DevExpress.XtraEditors.TextEdit txth5deddedizqphc;
        internal System.Windows.Forms.Label lblh5deddedizqphc;
        private DevExpress.XtraEditors.TextEdit txth5deddedderphc;
        internal System.Windows.Forms.Label lblh5deddedderphc;
        private DevExpress.XtraEditors.TextEdit txth5dednarizqphc;
        internal System.Windows.Forms.Label lblh5dednarizqphc;
        private DevExpress.XtraEditors.TextEdit txth5dednarderphc;
        internal System.Windows.Forms.Label lblh5dednarderphc;
        private DevExpress.XtraEditors.TextEdit txth5rombergphc;
        internal System.Windows.Forms.Label lblh5rombergphc;
        private DevExpress.XtraEditors.TextEdit txth5equilibratoriaphc;
        internal System.Windows.Forms.Label lblh5equilibratoriaphc;
        private System.Windows.Forms.GroupBox groupBox26;
        private DevExpress.XtraEditors.TextEdit txth5fasciculacionesphc;
        internal System.Windows.Forms.Label lblh5fasciculacionesphc;
        private DevExpress.XtraEditors.TextEdit txth5volumenphc;
        internal System.Windows.Forms.Label lblh5volumenphc;
        private DevExpress.XtraEditors.TextEdit txth5tonophc;
        internal System.Windows.Forms.Label lblh5tonophc;
        private DevExpress.XtraEditors.TextEdit txth5marchaphc;
        internal System.Windows.Forms.Label lblh5marchaphc;
        private DevExpress.XtraEditors.GroupControl groupControl9;
        private System.Windows.Forms.GroupBox groupBox25;
        internal System.Windows.Forms.Label lblh5desviacionphc;
        private DevExpress.XtraEditors.TextEdit txth5desviacionphc;
        internal System.Windows.Forms.Label lblh5atrofiaphc;
        private DevExpress.XtraEditors.TextEdit txth5atrofiaphc;
        internal System.Windows.Forms.Label lblh5fasciculacionphc;
        private DevExpress.XtraEditors.TextEdit txth5fasciculacionphc;
        internal System.Windows.Forms.Label lblh5fuerzaphc;
        private DevExpress.XtraEditors.TextEdit txth5fuerzaphc;
        private System.Windows.Forms.GroupBox groupBox24;
        private DevExpress.XtraEditors.TextEdit txth5esternocleidomastoideophc;
        private DevExpress.XtraEditors.TextEdit txth5trapeciophc;
        internal System.Windows.Forms.Label lblh5trapeciophc;
        internal System.Windows.Forms.Label lblh5esternocleidomastoideophc;
        private System.Windows.Forms.GroupBox groupBox23;
        private DevExpress.XtraEditors.TextEdit txth5elevpaladarphc;
        internal System.Windows.Forms.Label lblh5elevpaladarphc;
        private DevExpress.XtraEditors.TextEdit txth5refnauceosophc;
        internal System.Windows.Forms.Label lblh5refnauceosophc;
        private DevExpress.XtraEditors.TextEdit txth5uvulaphc;
        internal System.Windows.Forms.Label lblh5uvulaphc;
        private DevExpress.XtraEditors.TextEdit txth5deglucionphc;
        private DevExpress.XtraEditors.TextEdit txth5tonovozphc;
        internal System.Windows.Forms.Label lblh5tonovozphc;
        internal System.Windows.Forms.Label lblh5deglucionphc;
        private System.Windows.Forms.GroupBox groupBox22;
        private DevExpress.XtraEditors.TextEdit txth5otoscopiaphc;
        private DevExpress.XtraEditors.TextEdit txth5aguaudiderphc;
        internal System.Windows.Forms.Label lblh5otoscopiaphc;
        internal System.Windows.Forms.Label lblh5aguaudiderphc;
        private DevExpress.XtraEditors.TextEdit txth5aguaudiizqphc;
        internal System.Windows.Forms.Label lblh5aguaudiizqphc;
        private DevExpress.XtraEditors.TextEdit txth5weberphc;
        internal System.Windows.Forms.Label lblh5weberphc;
        private DevExpress.XtraEditors.TextEdit txth5rinnephc;
        internal System.Windows.Forms.Label lblh5rinnephc;
        private DevExpress.XtraEditors.TextEdit txth5pruebaslabphc;
        internal System.Windows.Forms.Label lblh5pruebaslabphc;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl2;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl8;
        private DevExpress.XtraEditors.GroupControl groupControl7;
        private System.Windows.Forms.GroupBox groupBox15;
        private DevExpress.XtraEditors.TextEdit txth3aprdesatencionphc;
        internal System.Windows.Forms.Label lblh3aprdesatencionphc;
        internal System.Windows.Forms.Label lblh3anosognosiaphc;
        private DevExpress.XtraEditors.TextEdit txth3anosognosiaphc;
        private System.Windows.Forms.GroupBox groupBox14;
        internal System.Windows.Forms.Label lblh3aprideacionalphc;
        private DevExpress.XtraEditors.TextEdit txth3aprideacionalphc;
        internal System.Windows.Forms.Label lblh3aprideomotoraphc;
        private DevExpress.XtraEditors.TextEdit txth3aprideomotoraphc;
        private System.Windows.Forms.GroupBox groupBox13;
        internal System.Windows.Forms.Label lblh3agnauditivaphc;
        private DevExpress.XtraEditors.TextEdit txth3agnauditivaphc;
        internal System.Windows.Forms.Label lblh3agnvisualphc;
        private DevExpress.XtraEditors.TextEdit txth3agnvisualphc;
        internal System.Windows.Forms.Label lblh3agntactilphc;
        private DevExpress.XtraEditors.TextEdit txth3agntactilphc;
        private System.Windows.Forms.GroupBox groupBox12;
        private DevExpress.XtraEditors.TextEdit txth3lpdagrafiaphc;
        internal System.Windows.Forms.Label lblh3lpdacalculiaphc;
        private DevExpress.XtraEditors.TextEdit txth3lpdagnosiaphc;
        internal System.Windows.Forms.Label lblh3lpdagnosiaphc;
        private DevExpress.XtraEditors.TextEdit txth3lpdacalculiaphc;
        internal System.Windows.Forms.Label lblh3lpdagrafiaphc;
        private DevExpress.XtraEditors.TextEdit txth3lpddesorientacionphc;
        internal System.Windows.Forms.Label lblh3lpddesorientacionphc;
        private DevExpress.XtraEditors.GroupControl groupControl6;
        private System.Windows.Forms.GroupBox groupBox11;
        internal System.Windows.Forms.Label lblh3lesagrafiaphc;
        private DevExpress.XtraEditors.TextEdit txth3lesagrafiaphc;
        internal System.Windows.Forms.Label lblh3lesalexiaphc;
        private DevExpress.XtraEditors.TextEdit txth3lesalexiaphc;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.GroupBox groupBox38;
        internal System.Windows.Forms.Label lblh3cgeexpresionphc;
        private DevExpress.XtraEditors.TextEdit txth3cgeexpresionphc;
        internal System.Windows.Forms.Label lblh3cgenominacionphc;
        private DevExpress.XtraEditors.TextEdit txth3cgenominacionphc;
        internal System.Windows.Forms.Label lblh3cgerepeticionphc;
        private DevExpress.XtraEditors.TextEdit txth3cgerepeticionphc;
        internal System.Windows.Forms.Label lblh3cgecomprensionphc;
        private DevExpress.XtraEditors.TextEdit txth3cgecomprensionphc;
        private System.Windows.Forms.GroupBox groupBox37;
        internal System.Windows.Forms.Label lblh3aprmotoraphc;
        private DevExpress.XtraEditors.TextEdit txth3aprmotoraphc;
        internal System.Windows.Forms.Label lblh3aprsensorialphc;
        private DevExpress.XtraEditors.TextEdit txth3aprsensorialphc;
        private System.Windows.Forms.GroupBox groupBox36;
        private DevExpress.XtraEditors.TextEdit txth3atacomprensionphc;
        internal System.Windows.Forms.Label lblh3ataexpresionphc;
        private DevExpress.XtraEditors.TextEdit txth3ataexpresionphc;
        internal System.Windows.Forms.Label lblh3atacomprensionphc;
        internal System.Windows.Forms.Label lblh3atamixtaphc;
        private DevExpress.XtraEditors.TextEdit txth3atamixtaphc;
        private System.Windows.Forms.GroupBox groupBox35;
        private DevExpress.XtraEditors.TextEdit txth3atrmotoraphc;
        internal System.Windows.Forms.Label lblh3atrmotoraphc;
        internal System.Windows.Forms.Label lblh3atrsensitivaphc;
        private DevExpress.XtraEditors.TextEdit txth3atrsensitivaphc;
        internal System.Windows.Forms.Label lblh3atrmixtaphc;
        private DevExpress.XtraEditors.TextEdit txth3atrmixtaphc;
        private System.Windows.Forms.GroupBox groupBox34;
        private DevExpress.XtraEditors.TextEdit txth3apebrocaphc;
        private DevExpress.XtraEditors.TextEdit txth3apeconduccionphc;
        private DevExpress.XtraEditors.TextEdit txth3apewernickephc;
        private DevExpress.XtraEditors.TextEdit txth3apeglobalphc;
        private DevExpress.XtraEditors.TextEdit txth3apeanomiaphc;
        internal System.Windows.Forms.Label lblh3apebrocaphc;
        internal System.Windows.Forms.Label lblh3apeconduccionphc;
        internal System.Windows.Forms.Label lblh3apewernickephc;
        internal System.Windows.Forms.Label lblh3apeglobalphc;
        internal System.Windows.Forms.Label lblh3apeanomiaphc;
        private System.Windows.Forms.GroupBox groupBox9;
        internal System.Windows.Forms.Label lblh3hdoizquierdophc;
        private DevExpress.XtraEditors.TextEdit txth3hdoizquierdophc;
        internal System.Windows.Forms.Label lblh3hdoderechophc;
        private DevExpress.XtraEditors.TextEdit txth3hdoderechophc;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private System.Windows.Forms.GroupBox groupBox8;
        internal System.Windows.Forms.Label lblh3conrecientephc;
        private DevExpress.XtraEditors.TextEdit txth3conrecientephc;
        internal System.Windows.Forms.Label lblh3conremotaphc;
        private DevExpress.XtraEditors.TextEdit txth3conremotaphc;
        private System.Windows.Forms.GroupBox groupBox7;
        private DevExpress.XtraEditors.TextEdit txth3afeplanophc;
        private DevExpress.XtraEditors.TextEdit txth3afeeuforicophc;
        private DevExpress.XtraEditors.TextEdit txth3afeinadecuadophc;
        private DevExpress.XtraEditors.TextEdit txth3afehostilphc;
        private DevExpress.XtraEditors.TextEdit txth3afelabilphc;
        internal System.Windows.Forms.Label lblh3afeplanophc;
        private DevExpress.XtraEditors.TextEdit txth3afedeprimidophc;
        internal System.Windows.Forms.Label lblh3afeeuforicophc;
        internal System.Windows.Forms.Label lblh3afedeprimidophc;
        internal System.Windows.Forms.Label lblh3afelabilphc;
        internal System.Windows.Forms.Label lblh3afehostilphc;
        internal System.Windows.Forms.Label lblh3afeinadecuadophc;
        private DevExpress.XtraEditors.XtraScrollableControl xtraScrollableControl3;
        private DevExpress.XtraEditors.GroupControl groupControl8;
        private System.Windows.Forms.GroupBox groupBox21;
        private DevExpress.XtraEditors.TextEdit txth4facsimetriaphc;
        internal System.Windows.Forms.Label lblh4facsimetriaphc;
        private DevExpress.XtraEditors.TextEdit txth4facmovimientosphc;
        internal System.Windows.Forms.Label lblh4facmovimientosphc;
        private DevExpress.XtraEditors.TextEdit txth4faccentralphc;
        internal System.Windows.Forms.Label lblh4facparalisisphc;
        internal System.Windows.Forms.Label lblh4faccentralphc;
        private DevExpress.XtraEditors.TextEdit txth4facparalisisphc;
        private DevExpress.XtraEditors.TextEdit txth4facperifericaphc;
        internal System.Windows.Forms.Label lblh4facgustophc;
        internal System.Windows.Forms.Label lblh4facperifericaphc;
        private DevExpress.XtraEditors.TextEdit txth4facgustophc;
        private System.Windows.Forms.GroupBox groupBox20;
        private DevExpress.XtraEditors.LabelControl lblh4movmasticatoriosphc;
        private DevExpress.XtraEditors.TextEdit txth4senfacializqphc;
        internal System.Windows.Forms.Label lblh4senfacialderphc;
        private DevExpress.XtraEditors.TextEdit txth4senfacialderphc;
        internal System.Windows.Forms.Label lblh4senfacializqphc;
        internal System.Windows.Forms.Label lblh4refmentonianophc;
        private DevExpress.XtraEditors.MemoEdit txth4movmasticatoriosphc;
        private DevExpress.XtraEditors.TextEdit txth4reflejoizqphc;
        private DevExpress.XtraEditors.TextEdit txth4refmentonianophc;
        internal System.Windows.Forms.Label lblh4reflejoizqphc;
        private DevExpress.XtraEditors.TextEdit txth4reflejoderphc;
        internal System.Windows.Forms.Label lblh4reflejoderphc;
        private DevExpress.XtraEditors.TextEdit txth4aberturabocaphc;
        internal System.Windows.Forms.Label lblh4aberturabocaphc;
        private System.Windows.Forms.GroupBox groupBox18;
        private DevExpress.XtraEditors.TextEdit txth4nistagmuxphc;
        private DevExpress.XtraEditors.TextEdit txth4posicionojosphc;
        private System.Windows.Forms.GroupBox groupBox39;
        private DevExpress.XtraEditors.TextEdit txth4acomodacionodphc;
        private DevExpress.XtraEditors.TextEdit txth4acomodacionoiphc;
        internal System.Windows.Forms.Label lblh4acomodacionoiphc;
        internal System.Windows.Forms.Label lblh4acomodacionodphc;
        private System.Windows.Forms.GroupBox groupBox19;
        private DevExpress.XtraEditors.TextEdit txth4diplopia1phc;
        private DevExpress.XtraEditors.TextEdit txth4diplopia9phc;
        private DevExpress.XtraEditors.TextEdit txth4diplopia8phc;
        private DevExpress.XtraEditors.TextEdit txth4diplopia7phc;
        private DevExpress.XtraEditors.TextEdit txth4diplopia6phc;
        private DevExpress.XtraEditors.TextEdit txth4diplopia5phc;
        private DevExpress.XtraEditors.TextEdit txth4diplopia4phc;
        private DevExpress.XtraEditors.TextEdit txth4diplopia3phc;
        private DevExpress.XtraEditors.TextEdit txth4diplopia2phc;
        internal System.Windows.Forms.Label lblh4posicionojosphc;
        internal System.Windows.Forms.Label lblh4exoftalmiaphc;
        internal System.Windows.Forms.Label lblh4hornerphc;
        private DevExpress.XtraEditors.TextEdit txth4hornerphc;
        private DevExpress.XtraEditors.TextEdit txth4exoftalmiaphc;
        internal System.Windows.Forms.Label lblh4movocularesphc;
        private DevExpress.XtraEditors.TextEdit txth4movocularesphc;
        internal System.Windows.Forms.Label lblh4nistagmuxphc;
        internal System.Windows.Forms.Label lblh4prosisphc;
        private DevExpress.XtraEditors.TextEdit txth4prosisphc;
        internal System.Windows.Forms.Label lblh4convergenciaphc;
        private DevExpress.XtraEditors.TextEdit txth4convergenciaphc;
        internal System.Windows.Forms.Label lblh4pupulasodphc;
        private DevExpress.XtraEditors.TextEdit txth4pupulasodphc;
        internal System.Windows.Forms.Label lblh4pupulasoiphc;
        private DevExpress.XtraEditors.TextEdit txth4pupulasoiphc;
        internal System.Windows.Forms.Label lblh4formaphc;
        private DevExpress.XtraEditors.TextEdit txth4formaphc;
        internal System.Windows.Forms.Label lblh4fotomotorodphc;
        private DevExpress.XtraEditors.TextEdit txth4fotomotorodphc;
        internal System.Windows.Forms.Label lblh4fotomotoroiphc;
        private DevExpress.XtraEditors.TextEdit txth4fotomotoroiphc;
        internal System.Windows.Forms.Label lblh4consensualdaiphc;
        private DevExpress.XtraEditors.TextEdit txth4consensualdaiphc;
        internal System.Windows.Forms.Label lblh4consensualiadphc;
        private DevExpress.XtraEditors.TextEdit txth4consensualiadphc;
        private System.Windows.Forms.GroupBox groupBox17;
        private DevExpress.XtraEditors.TextEdit txth4optccodphc;
        private DevExpress.XtraEditors.TextEdit txth4optccoiphc;
        internal System.Windows.Forms.Label label2;
        internal System.Windows.Forms.Label label3;
        internal System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.MemoEdit txth4campimetriaphc;
        internal System.Windows.Forms.Label lblh4optscodphc;
        private DevExpress.XtraEditors.TextEdit txth4optscodphc;
        internal System.Windows.Forms.Label lblh4optscoiphc;
        private DevExpress.XtraEditors.TextEdit txth4optscoiphc;
        internal System.Windows.Forms.Label lblh4optccodphc;
        internal System.Windows.Forms.Label lblh4optccoiphc;
        internal System.Windows.Forms.Label lblh4fondoodphc;
        private DevExpress.XtraEditors.TextEdit txth4fondoodphc;
        internal System.Windows.Forms.Label lblh4fondooiphc;
        private DevExpress.XtraEditors.TextEdit txth4fondooiphc;
        internal System.Windows.Forms.Label lblh4campimetriaphc;
        private System.Windows.Forms.GroupBox groupBox16;
        internal System.Windows.Forms.Label lblh4olfnormalphc;
        private DevExpress.XtraEditors.TextEdit txth4olfnormalphc;
        internal System.Windows.Forms.Label lblh4olfanosmiaphc;
        private DevExpress.XtraEditors.TextEdit txth4olfanosmiaphc;
        private DevExpress.XtraEditors.SimpleButton btnAuditoría;

    }
}