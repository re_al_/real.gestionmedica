


namespace ReAl.GestionMedica.App.PAC
{
	partial class FPacPacientes
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPacPacientes));
            lblnombresppa = new Label();
            txtnombresppa = new DevExpress.XtraEditors.TextEdit();
            lblappaternoppa = new Label();
            txtappaternoppa = new DevExpress.XtraEditors.TextEdit();
            lblapmaternoppa = new Label();
            txtapmaternoppa = new DevExpress.XtraEditors.TextEdit();
            lblgeneroppa = new Label();
            lblidcts = new Label();
            cmbidcts = new DevExpress.XtraEditors.LookUpEdit();
            lblidcec = new Label();
            cmbidcec = new DevExpress.XtraEditors.LookUpEdit();
            lblocupacionppa = new Label();
            txtocupacionppa = new DevExpress.XtraEditors.TextEdit();
            lblprocedenciappa = new Label();
            txtprocedenciappa = new DevExpress.XtraEditors.TextEdit();
            lbldomicilioppa = new Label();
            txtdomicilioppa = new DevExpress.XtraEditors.TextEdit();
            lbldominanciappa = new Label();
            txtdominanciappa = new DevExpress.XtraEditors.TextEdit();
            lbltelefonosppa = new Label();
            txttelefonosppa = new DevExpress.XtraEditors.TextEdit();
            lblemailppa = new Label();
            txtemailppa = new DevExpress.XtraEditors.TextEdit();
            lblreligionppa = new Label();
            txtreligionppa = new DevExpress.XtraEditors.TextEdit();
            lblempresappa = new Label();
            txtempresappa = new DevExpress.XtraEditors.TextEdit();
            lblseguroppa = new Label();
            txtseguroppa = new DevExpress.XtraEditors.TextEdit();
            lblremiteppa = new Label();
            txtremiteppa = new DevExpress.XtraEditors.TextEdit();
            lblinformanteppa = new Label();
            txtinformanteppa = new DevExpress.XtraEditors.TextEdit();
            btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            cmbGenero = new DevExpress.XtraEditors.LookUpEdit();
            dtpfechanac = new DevExpress.XtraEditors.DateEdit();
            lblfechanac = new Label();
            label1 = new Label();
            txtEdad = new DevExpress.XtraEditors.TextEdit();
            picFoto = new PictureBox();
            lblTitulo = new DevExpress.XtraEditors.LabelControl();
            btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            gcPendientes = new DevExpress.XtraGrid.GridControl();
            gvPendientes = new DevExpress.XtraGrid.Views.Grid.GridView();
            label2 = new Label();
            txtcippa = new DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)txtnombresppa.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtappaternoppa.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtapmaternoppa.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)cmbidcts.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)cmbidcec.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtocupacionppa.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtprocedenciappa.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtdomicilioppa.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtdominanciappa.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txttelefonosppa.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtemailppa.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtreligionppa.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtempresappa.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtseguroppa.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtremiteppa.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtinformanteppa.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)cmbGenero.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dtpfechanac.Properties.CalendarTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dtpfechanac.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtEdad.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picFoto).BeginInit();
            ((System.ComponentModel.ISupportInitialize)xtraTabControl1).BeginInit();
            xtraTabControl1.SuspendLayout();
            xtraTabPage1.SuspendLayout();
            xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)gcPendientes).BeginInit();
            ((System.ComponentModel.ISupportInitialize)gvPendientes).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtcippa.Properties).BeginInit();
            SuspendLayout();
            // 
            // lblnombresppa
            // 
            lblnombresppa.AutoSize = true;
            lblnombresppa.Location = new Point(3, 12);
            lblnombresppa.Name = "lblnombresppa";
            lblnombresppa.Size = new Size(53, 13);
            lblnombresppa.TabIndex = 0;
            lblnombresppa.Text = "Nombres:";
            // 
            // txtnombresppa
            // 
            txtnombresppa.Location = new Point(109, 9);
            txtnombresppa.Name = "txtnombresppa";
            txtnombresppa.Properties.MaxLength = 200;
            txtnombresppa.Size = new Size(204, 20);
            txtnombresppa.TabIndex = 1;
            txtnombresppa.EditValueChanged += txtnombresppa_EditValueChanged;
            // 
            // lblappaternoppa
            // 
            lblappaternoppa.AutoSize = true;
            lblappaternoppa.Location = new Point(3, 37);
            lblappaternoppa.Name = "lblappaternoppa";
            lblappaternoppa.Size = new Size(53, 13);
            lblappaternoppa.TabIndex = 2;
            lblappaternoppa.Text = "Apellidos:";
            // 
            // txtappaternoppa
            // 
            txtappaternoppa.Location = new Point(109, 34);
            txtappaternoppa.Name = "txtappaternoppa";
            txtappaternoppa.Properties.MaxLength = 200;
            txtappaternoppa.Size = new Size(204, 20);
            txtappaternoppa.TabIndex = 3;
            txtappaternoppa.EditValueChanged += txtappaternoppa_EditValueChanged;
            // 
            // lblapmaternoppa
            // 
            lblapmaternoppa.AutoSize = true;
            lblapmaternoppa.Location = new Point(350, 37);
            lblapmaternoppa.Name = "lblapmaternoppa";
            lblapmaternoppa.Size = new Size(71, 13);
            lblapmaternoppa.TabIndex = 4;
            lblapmaternoppa.Text = "Ap. Materno:";
            lblapmaternoppa.Visible = false;
            // 
            // txtapmaternoppa
            // 
            txtapmaternoppa.Location = new Point(424, 34);
            txtapmaternoppa.Name = "txtapmaternoppa";
            txtapmaternoppa.Properties.MaxLength = 200;
            txtapmaternoppa.Size = new Size(204, 20);
            txtapmaternoppa.TabIndex = 5;
            txtapmaternoppa.Visible = false;
            txtapmaternoppa.EditValueChanged += txtapmaternoppa_EditValueChanged;
            // 
            // lblgeneroppa
            // 
            lblgeneroppa.AutoSize = true;
            lblgeneroppa.Location = new Point(3, 63);
            lblgeneroppa.Name = "lblgeneroppa";
            lblgeneroppa.Size = new Size(46, 13);
            lblgeneroppa.TabIndex = 6;
            lblgeneroppa.Text = "Genero:";
            // 
            // lblidcts
            // 
            lblidcts.AutoSize = true;
            lblidcts.Location = new Point(350, 88);
            lblidcts.Name = "lblidcts";
            lblidcts.Size = new Size(68, 13);
            lblidcts.TabIndex = 12;
            lblidcts.Text = "Tipo Sangre:";
            // 
            // cmbidcts
            // 
            cmbidcts.Location = new Point(424, 85);
            cmbidcts.Name = "cmbidcts";
            cmbidcts.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            cmbidcts.Size = new Size(204, 20);
            cmbidcts.TabIndex = 13;
            // 
            // lblidcec
            // 
            lblidcec.AutoSize = true;
            lblidcec.Location = new Point(3, 113);
            lblidcec.Name = "lblidcec";
            lblidcec.Size = new Size(66, 13);
            lblidcec.TabIndex = 14;
            lblidcec.Text = "Estado Civil:";
            // 
            // cmbidcec
            // 
            cmbidcec.Location = new Point(109, 110);
            cmbidcec.Name = "cmbidcec";
            cmbidcec.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            cmbidcec.Size = new Size(204, 20);
            cmbidcec.TabIndex = 15;
            // 
            // lblocupacionppa
            // 
            lblocupacionppa.AutoSize = true;
            lblocupacionppa.Location = new Point(350, 138);
            lblocupacionppa.Name = "lblocupacionppa";
            lblocupacionppa.Size = new Size(61, 13);
            lblocupacionppa.TabIndex = 20;
            lblocupacionppa.Text = "Ocupación:";
            // 
            // txtocupacionppa
            // 
            txtocupacionppa.Location = new Point(424, 135);
            txtocupacionppa.Name = "txtocupacionppa";
            txtocupacionppa.Properties.MaxLength = 300;
            txtocupacionppa.Size = new Size(204, 20);
            txtocupacionppa.TabIndex = 21;
            // 
            // lblprocedenciappa
            // 
            lblprocedenciappa.AutoSize = true;
            lblprocedenciappa.Location = new Point(350, 163);
            lblprocedenciappa.Name = "lblprocedenciappa";
            lblprocedenciappa.Size = new Size(69, 13);
            lblprocedenciappa.TabIndex = 24;
            lblprocedenciappa.Text = "Procedencia:";
            // 
            // txtprocedenciappa
            // 
            txtprocedenciappa.Location = new Point(424, 160);
            txtprocedenciappa.Name = "txtprocedenciappa";
            txtprocedenciappa.Properties.MaxLength = 300;
            txtprocedenciappa.Size = new Size(204, 20);
            txtprocedenciappa.TabIndex = 25;
            // 
            // lbldomicilioppa
            // 
            lbldomicilioppa.AutoSize = true;
            lbldomicilioppa.Location = new Point(3, 163);
            lbldomicilioppa.Name = "lbldomicilioppa";
            lbldomicilioppa.Size = new Size(47, 13);
            lbldomicilioppa.TabIndex = 22;
            lbldomicilioppa.Text = "Domicilio";
            // 
            // txtdomicilioppa
            // 
            txtdomicilioppa.Location = new Point(109, 160);
            txtdomicilioppa.Name = "txtdomicilioppa";
            txtdomicilioppa.Properties.MaxLength = 500;
            txtdomicilioppa.Size = new Size(204, 20);
            txtdomicilioppa.TabIndex = 23;
            // 
            // lbldominanciappa
            // 
            lbldominanciappa.AutoSize = true;
            lbldominanciappa.Location = new Point(350, 188);
            lbldominanciappa.Name = "lbldominanciappa";
            lbldominanciappa.Size = new Size(65, 13);
            lbldominanciappa.TabIndex = 28;
            lbldominanciappa.Text = "Dominancia:";
            // 
            // txtdominanciappa
            // 
            txtdominanciappa.Location = new Point(424, 185);
            txtdominanciappa.Name = "txtdominanciappa";
            txtdominanciappa.Properties.MaxLength = 100;
            txtdominanciappa.Size = new Size(204, 20);
            txtdominanciappa.TabIndex = 29;
            // 
            // lbltelefonosppa
            // 
            lbltelefonosppa.AutoSize = true;
            lbltelefonosppa.Location = new Point(350, 113);
            lbltelefonosppa.Name = "lbltelefonosppa";
            lbltelefonosppa.Size = new Size(58, 13);
            lbltelefonosppa.TabIndex = 16;
            lbltelefonosppa.Text = "Teléfonos:";
            // 
            // txttelefonosppa
            // 
            txttelefonosppa.Location = new Point(424, 110);
            txttelefonosppa.Name = "txttelefonosppa";
            txttelefonosppa.Properties.MaxLength = 200;
            txttelefonosppa.Size = new Size(204, 20);
            txttelefonosppa.TabIndex = 17;
            // 
            // lblemailppa
            // 
            lblemailppa.AutoSize = true;
            lblemailppa.Location = new Point(3, 138);
            lblemailppa.Name = "lblemailppa";
            lblemailppa.Size = new Size(99, 13);
            lblemailppa.TabIndex = 18;
            lblemailppa.Text = "Correo Electrónico:";
            // 
            // txtemailppa
            // 
            txtemailppa.Location = new Point(109, 135);
            txtemailppa.Name = "txtemailppa";
            txtemailppa.Properties.MaxLength = 200;
            txtemailppa.Size = new Size(204, 20);
            txtemailppa.TabIndex = 19;
            // 
            // lblreligionppa
            // 
            lblreligionppa.AutoSize = true;
            lblreligionppa.Location = new Point(3, 188);
            lblreligionppa.Name = "lblreligionppa";
            lblreligionppa.Size = new Size(48, 13);
            lblreligionppa.TabIndex = 26;
            lblreligionppa.Text = "Religión:";
            // 
            // txtreligionppa
            // 
            txtreligionppa.Location = new Point(109, 185);
            txtreligionppa.Name = "txtreligionppa";
            txtreligionppa.Properties.MaxLength = 200;
            txtreligionppa.Size = new Size(204, 20);
            txtreligionppa.TabIndex = 27;
            // 
            // lblempresappa
            // 
            lblempresappa.AutoSize = true;
            lblempresappa.Location = new Point(350, 213);
            lblempresappa.Name = "lblempresappa";
            lblempresappa.Size = new Size(52, 13);
            lblempresappa.TabIndex = 32;
            lblempresappa.Text = "Empresa:";
            // 
            // txtempresappa
            // 
            txtempresappa.Location = new Point(424, 210);
            txtempresappa.Name = "txtempresappa";
            txtempresappa.Properties.MaxLength = 500;
            txtempresappa.Size = new Size(204, 20);
            txtempresappa.TabIndex = 33;
            // 
            // lblseguroppa
            // 
            lblseguroppa.AutoSize = true;
            lblseguroppa.Location = new Point(3, 213);
            lblseguroppa.Name = "lblseguroppa";
            lblseguroppa.Size = new Size(45, 13);
            lblseguroppa.TabIndex = 30;
            lblseguroppa.Text = "Seguro:";
            // 
            // txtseguroppa
            // 
            txtseguroppa.Location = new Point(109, 210);
            txtseguroppa.Name = "txtseguroppa";
            txtseguroppa.Properties.MaxLength = 200;
            txtseguroppa.Size = new Size(204, 20);
            txtseguroppa.TabIndex = 31;
            // 
            // lblremiteppa
            // 
            lblremiteppa.AutoSize = true;
            lblremiteppa.Location = new Point(350, 238);
            lblremiteppa.Name = "lblremiteppa";
            lblremiteppa.Size = new Size(44, 13);
            lblremiteppa.TabIndex = 36;
            lblremiteppa.Text = "Remite:";
            // 
            // txtremiteppa
            // 
            txtremiteppa.Location = new Point(424, 235);
            txtremiteppa.Name = "txtremiteppa";
            txtremiteppa.Properties.MaxLength = 500;
            txtremiteppa.Size = new Size(204, 20);
            txtremiteppa.TabIndex = 37;
            // 
            // lblinformanteppa
            // 
            lblinformanteppa.AutoSize = true;
            lblinformanteppa.Location = new Point(3, 238);
            lblinformanteppa.Name = "lblinformanteppa";
            lblinformanteppa.Size = new Size(65, 13);
            lblinformanteppa.TabIndex = 34;
            lblinformanteppa.Text = "Informante:";
            // 
            // txtinformanteppa
            // 
            txtinformanteppa.Location = new Point(109, 235);
            txtinformanteppa.Name = "txtinformanteppa";
            txtinformanteppa.Properties.MaxLength = 500;
            txtinformanteppa.Size = new Size(204, 20);
            txtinformanteppa.TabIndex = 35;
            // 
            // btnCancelar
            // 
            btnCancelar.DialogResult = DialogResult.Cancel;
            btnCancelar.Location = new Point(438, 488);
            btnCancelar.Name = "btnCancelar";
            btnCancelar.Size = new Size(108, 34);
            btnCancelar.TabIndex = 2;
            btnCancelar.Text = "&Cancelar";
            btnCancelar.Click += btnCancelar_Click;
            // 
            // cmbGenero
            // 
            cmbGenero.Location = new Point(109, 60);
            cmbGenero.Name = "cmbGenero";
            cmbGenero.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            cmbGenero.Properties.Columns.AddRange(new DevExpress.XtraEditors.Controls.LookUpColumnInfo[] { new DevExpress.XtraEditors.Controls.LookUpColumnInfo("M", "M", 31, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default), new DevExpress.XtraEditors.Controls.LookUpColumnInfo("F", "F", 16, DevExpress.Utils.FormatType.None, "", true, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.None, DevExpress.Utils.DefaultBoolean.Default) });
            cmbGenero.Size = new Size(204, 20);
            cmbGenero.TabIndex = 7;
            // 
            // dtpfechanac
            // 
            dtpfechanac.EditValue = new DateTime(2013, 6, 6, 17, 25, 37, 0);
            dtpfechanac.Location = new Point(109, 85);
            dtpfechanac.Name = "dtpfechanac";
            dtpfechanac.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            dtpfechanac.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            dtpfechanac.Size = new Size(204, 20);
            dtpfechanac.TabIndex = 11;
            dtpfechanac.EditValueChanged += dtpfechanac_EditValueChanged;
            // 
            // lblfechanac
            // 
            lblfechanac.AutoSize = true;
            lblfechanac.Location = new Point(3, 88);
            lblfechanac.Name = "lblfechanac";
            lblfechanac.Size = new Size(95, 13);
            lblfechanac.TabIndex = 10;
            lblfechanac.Text = "Fecha Nacimiento:";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(350, 63);
            label1.Name = "label1";
            label1.Size = new Size(35, 13);
            label1.TabIndex = 8;
            label1.Text = "Edad:";
            // 
            // txtEdad
            // 
            txtEdad.Location = new Point(424, 60);
            txtEdad.Name = "txtEdad";
            txtEdad.Properties.Appearance.BackColor = SystemColors.Info;
            txtEdad.Properties.Appearance.Options.UseBackColor = true;
            txtEdad.Properties.MaxLength = 200;
            txtEdad.Properties.ReadOnly = true;
            txtEdad.Size = new Size(204, 20);
            txtEdad.TabIndex = 9;
            txtEdad.TabStop = false;
            // 
            // picFoto
            // 
            picFoto.Location = new Point(532, 43);
            picFoto.Name = "picFoto";
            picFoto.Size = new Size(125, 146);
            picFoto.TabIndex = 44;
            picFoto.TabStop = false;
            // 
            // lblTitulo
            // 
            lblTitulo.Appearance.Font = new Font("Tahoma", 24F);
            lblTitulo.Appearance.Options.UseFont = true;
            lblTitulo.Location = new Point(15, 12);
            lblTitulo.Name = "lblTitulo";
            lblTitulo.Size = new Size(24, 117);
            lblTitulo.TabIndex = 0;
            lblTitulo.Text = "--\r\n--\r\n--";
            // 
            // btnAceptar
            // 
            btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            btnAceptar.Appearance.Options.UseBackColor = true;
            btnAceptar.Location = new Point(552, 488);
            btnAceptar.Name = "btnAceptar";
            btnAceptar.Size = new Size(108, 34);
            btnAceptar.TabIndex = 3;
            btnAceptar.Text = "&Registrar";
            btnAceptar.Click += btnAceptar_Click;
            // 
            // xtraTabControl1
            // 
            xtraTabControl1.Location = new Point(15, 195);
            xtraTabControl1.Name = "xtraTabControl1";
            xtraTabControl1.SelectedTabPage = xtraTabPage1;
            xtraTabControl1.Size = new Size(645, 288);
            xtraTabControl1.TabIndex = 1;
            xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] { xtraTabPage1, xtraTabPage2 });
            // 
            // xtraTabPage1
            // 
            xtraTabPage1.Controls.Add(lblnombresppa);
            xtraTabPage1.Controls.Add(txtinformanteppa);
            xtraTabPage1.Controls.Add(lblinformanteppa);
            xtraTabPage1.Controls.Add(txtremiteppa);
            xtraTabPage1.Controls.Add(label1);
            xtraTabPage1.Controls.Add(lblremiteppa);
            xtraTabPage1.Controls.Add(txtEdad);
            xtraTabPage1.Controls.Add(txtseguroppa);
            xtraTabPage1.Controls.Add(cmbGenero);
            xtraTabPage1.Controls.Add(lblseguroppa);
            xtraTabPage1.Controls.Add(txtempresappa);
            xtraTabPage1.Controls.Add(lblempresappa);
            xtraTabPage1.Controls.Add(txtnombresppa);
            xtraTabPage1.Controls.Add(txtreligionppa);
            xtraTabPage1.Controls.Add(lblappaternoppa);
            xtraTabPage1.Controls.Add(lblreligionppa);
            xtraTabPage1.Controls.Add(txtappaternoppa);
            xtraTabPage1.Controls.Add(txtemailppa);
            xtraTabPage1.Controls.Add(lblapmaternoppa);
            xtraTabPage1.Controls.Add(lblemailppa);
            xtraTabPage1.Controls.Add(txtapmaternoppa);
            xtraTabPage1.Controls.Add(txttelefonosppa);
            xtraTabPage1.Controls.Add(lblgeneroppa);
            xtraTabPage1.Controls.Add(lbltelefonosppa);
            xtraTabPage1.Controls.Add(lblfechanac);
            xtraTabPage1.Controls.Add(txtdominanciappa);
            xtraTabPage1.Controls.Add(dtpfechanac);
            xtraTabPage1.Controls.Add(lbldominanciappa);
            xtraTabPage1.Controls.Add(lblidcts);
            xtraTabPage1.Controls.Add(txtdomicilioppa);
            xtraTabPage1.Controls.Add(cmbidcts);
            xtraTabPage1.Controls.Add(lbldomicilioppa);
            xtraTabPage1.Controls.Add(lblidcec);
            xtraTabPage1.Controls.Add(txtprocedenciappa);
            xtraTabPage1.Controls.Add(cmbidcec);
            xtraTabPage1.Controls.Add(lblprocedenciappa);
            xtraTabPage1.Controls.Add(lblocupacionppa);
            xtraTabPage1.Controls.Add(txtocupacionppa);
            xtraTabPage1.Name = "xtraTabPage1";
            xtraTabPage1.Size = new Size(643, 263);
            xtraTabPage1.Text = "Datos del Paciente";
            // 
            // xtraTabPage2
            // 
            xtraTabPage2.Controls.Add(gcPendientes);
            xtraTabPage2.Name = "xtraTabPage2";
            xtraTabPage2.Size = new Size(643, 287);
            xtraTabPage2.Text = "Historial agenda";
            // 
            // gcPendientes
            // 
            gcPendientes.Dock = DockStyle.Fill;
            gcPendientes.Location = new Point(0, 0);
            gcPendientes.MainView = gvPendientes;
            gcPendientes.Name = "gcPendientes";
            gcPendientes.Size = new Size(643, 287);
            gcPendientes.TabIndex = 0;
            gcPendientes.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] { gvPendientes });
            // 
            // gvPendientes
            // 
            gvPendientes.GridControl = gcPendientes;
            gvPendientes.Name = "gvPendientes";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(12, 143);
            label2.Name = "label2";
            label2.Size = new Size(118, 13);
            label2.TabIndex = 0;
            label2.Text = "CI - Identificador Web:";
            // 
            // txtcippa
            // 
            txtcippa.Location = new Point(12, 159);
            txtcippa.Name = "txtcippa";
            txtcippa.Properties.Appearance.Font = new Font("Tahoma", 14F);
            txtcippa.Properties.Appearance.Options.UseFont = true;
            txtcippa.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            txtcippa.Properties.MaskSettings.Set("MaskManagerSignature", "allowNull=False");
            txtcippa.Properties.MaskSettings.Set("mask", "##########");
            txtcippa.Properties.MaxLength = 10;
            txtcippa.Size = new Size(204, 30);
            txtcippa.TabIndex = 0;
            // 
            // FPacPacientes
            // 
            AcceptButton = btnAceptar;
            AutoScaleDimensions = new SizeF(6F, 13F);
            AutoScaleMode = AutoScaleMode.Font;
            CancelButton = btnCancelar;
            ClientSize = new Size(669, 531);
            Controls.Add(label2);
            Controls.Add(xtraTabControl1);
            Controls.Add(txtcippa);
            Controls.Add(btnAceptar);
            Controls.Add(lblTitulo);
            Controls.Add(picFoto);
            Controls.Add(btnCancelar);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            IconOptions.Icon = (Icon)resources.GetObject("FPacPacientes.IconOptions.Icon");
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FPacPacientes";
            ShowInTaskbar = false;
            StartPosition = FormStartPosition.CenterParent;
            Text = "Administracion de Pacientes";
            Load += fPacPacientes_Load;
            ((System.ComponentModel.ISupportInitialize)txtnombresppa.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtappaternoppa.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtapmaternoppa.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)cmbidcts.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)cmbidcec.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtocupacionppa.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtprocedenciappa.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtdomicilioppa.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtdominanciappa.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txttelefonosppa.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtemailppa.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtreligionppa.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtempresappa.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtseguroppa.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtremiteppa.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtinformanteppa.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)cmbGenero.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)dtpfechanac.Properties.CalendarTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)dtpfechanac.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtEdad.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)picFoto).EndInit();
            ((System.ComponentModel.ISupportInitialize)xtraTabControl1).EndInit();
            xtraTabControl1.ResumeLayout(false);
            xtraTabPage1.ResumeLayout(false);
            xtraTabPage1.PerformLayout();
            xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)gcPendientes).EndInit();
            ((System.ComponentModel.ISupportInitialize)gvPendientes).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtcippa.Properties).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        internal System.Windows.Forms.Label lblnombresppa;
		private DevExpress.XtraEditors.TextEdit txtnombresppa;
		internal System.Windows.Forms.Label lblappaternoppa;
		private DevExpress.XtraEditors.TextEdit txtappaternoppa;
		internal System.Windows.Forms.Label lblapmaternoppa;
		private DevExpress.XtraEditors.TextEdit txtapmaternoppa;
        internal System.Windows.Forms.Label lblgeneroppa;
		internal System.Windows.Forms.Label lblidcts;
		private DevExpress.XtraEditors.LookUpEdit cmbidcts;
		internal System.Windows.Forms.Label lblidcec;
		private DevExpress.XtraEditors.LookUpEdit cmbidcec;
		internal System.Windows.Forms.Label lblocupacionppa;
		private DevExpress.XtraEditors.TextEdit txtocupacionppa;
		internal System.Windows.Forms.Label lblprocedenciappa;
		private DevExpress.XtraEditors.TextEdit txtprocedenciappa;
		internal System.Windows.Forms.Label lbldomicilioppa;
		private DevExpress.XtraEditors.TextEdit txtdomicilioppa;
		internal System.Windows.Forms.Label lbldominanciappa;
		private DevExpress.XtraEditors.TextEdit txtdominanciappa;
		internal System.Windows.Forms.Label lbltelefonosppa;
		private DevExpress.XtraEditors.TextEdit txttelefonosppa;
		internal System.Windows.Forms.Label lblemailppa;
		private DevExpress.XtraEditors.TextEdit txtemailppa;
		internal System.Windows.Forms.Label lblreligionppa;
		private DevExpress.XtraEditors.TextEdit txtreligionppa;
		internal System.Windows.Forms.Label lblempresappa;
		private DevExpress.XtraEditors.TextEdit txtempresappa;
		internal System.Windows.Forms.Label lblseguroppa;
		private DevExpress.XtraEditors.TextEdit txtseguroppa;
		internal System.Windows.Forms.Label lblremiteppa;
		private DevExpress.XtraEditors.TextEdit txtremiteppa;
		internal System.Windows.Forms.Label lblinformanteppa;
        private DevExpress.XtraEditors.TextEdit txtinformanteppa;
		private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.LookUpEdit cmbGenero;
        private DevExpress.XtraEditors.DateEdit dtpfechanac;
        internal System.Windows.Forms.Label lblfechanac;
        internal System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txtEdad;
        private System.Windows.Forms.PictureBox picFoto;
        private DevExpress.XtraEditors.LabelControl lblTitulo;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gcPendientes;
        private DevExpress.XtraGrid.Views.Grid.GridView gvPendientes;
        internal System.Windows.Forms.Label label2;
        private DevExpress.XtraEditors.TextEdit txtcippa;
    }
}

