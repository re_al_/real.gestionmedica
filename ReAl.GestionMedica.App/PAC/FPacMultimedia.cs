using System.IO;
using ReAl.GestionMedica.App.App_Class;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;
using ReAl.Utils;

namespace ReAl.GestionMedica.App.PAC
{
    public partial class FPacMultimedia : DevExpress.XtraEditors.XtraForm
    {
        #region Fields

        private readonly PatMultimedia _myObj = null;
        private string _strArchivo = "";

        #endregion Fields

        #region Constructors

        public FPacMultimedia()
        {
            InitializeComponent();
        }

        public FPacMultimedia(string strLocation)
        {
            InitializeComponent();

            _strArchivo = strLocation;
            txtArchivo.Text = _strArchivo;
        }

        public FPacMultimedia(PatMultimedia obj)
        {
            InitializeComponent();

            _myObj = obj;
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var rn = new PatMultimediaController();
                if (_myObj == null)
                {
                    if (string.IsNullOrEmpty(_strArchivo))
                    {
                        MessageBox.Show("Debe elegir un archivo");
                    }

                    //FileName
                    Guid myuuid = Guid.NewGuid();
                    string myuuidAsString = myuuid.ToString();
                    var strExtension = _strArchivo.Substring(_strArchivo.LastIndexOf(".") + 1,
                        _strArchivo.Length - _strArchivo.LastIndexOf(".") - 1);
                    var strFileName = cParametrosApp.AppPaciente.IdPatients + "-" + myuuidAsString + "." +
                                      strExtension;

                    //Firebase
                    var credential = CFirebaseHelper.FirebaseLogin();
                    Stream stream = File.Open(_strArchivo, FileMode.Open);

                    Task<string> taskFirebase = Task.Run<string>(async () => await CFirebaseHelper.FirebaseUpload(credential, stream, strFileName));
                    var fireBaseUrl = taskFirebase.Result;

                    //Insertamos el registro
                    var obj = new PatMultimedia();
                    obj.IdPatients = cParametrosApp.AppPaciente.IdPatients;
                    obj.UploadDate = dtpfechapmu.DateTime;
                    obj.Filename = strFileName;
                    obj.Observations = txtobservacionespmu.Text;
                    obj.ExternalUrl = fireBaseUrl;
                    obj.Extension = strExtension;
                    rn.Create(obj);
                    bProcede = true;
                }
                else
                {
                    //Actualizamos el registro
                    _myObj.UploadDate = dtpfechapmu.DateTime;
                    _myObj.Observations = txtobservacionespmu.Text;
                    _myObj.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                    rn.Update(_myObj);
                    bProcede = true;
                }
            }
            catch (Exception exp)
            {
                bProcede = false;
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }

            if (bProcede)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnExaminar_Click(object sender, EventArgs e)
        {
            _strArchivo = cFuncionesFicheros.AbrirArchivoImagen();
            txtArchivo.Text = _strArchivo;
        }

        private void CargarDatos()
        {
            if (_myObj != null)
            {
                dtpfechapmu.EditValue = _myObj.UploadDate;
                txtobservacionespmu.Text = _myObj.Observations;

                txtArchivo.Text = "[ NO PUEDE CAMBIAR EL ARCHIVO ]";
                btnExaminar.Visible = false;
            }
        }

        private void fPacMultimedia_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            if (_myObj != null)
            {
                CargarDatos();
            }
        }

        #endregion Methods
    }
}