using System.Collections;
using DevExpress.Utils;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;

namespace ReAl.GestionMedica.App.PAC
{
    public partial class LPacConsultas : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public LPacConsultas()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        public void CargarListado()
        {
            var rn = new PatConsultationsController();
            var dt = rn.GetByPatientId(cParametrosApp.AppPaciente.IdPatients);
            gcPacConsultas.DataSource = dt;
            if (dt is { Count: > 0 })
            {
                gvPacConsultas.PopulateColumns();
                gvPacConsultas.Columns[PatConsultations.Fields.IdConsultations.ToString()].Visible = false;

                gvPacConsultas.Columns[PatConsultations.Fields.ConsultationDate.ToString()].DisplayFormat.FormatType = FormatType.DateTime;
                gvPacConsultas.Columns[PatConsultations.Fields.ConsultationDate.ToString()].DisplayFormat.FormatString = "dd/MM/yyyy";
                gvPacConsultas.Columns[PatConsultations.Fields.ConsultationTime.ToString()].DisplayFormat.FormatType = FormatType.DateTime;
                gvPacConsultas.Columns[PatConsultations.Fields.ConsultationTime.ToString()].DisplayFormat.FormatString = "HH:mm";
            }
            gvPacConsultas.OptionsBehavior.Editable = false;
        }

        public void EliminarRegistro()
        {
            try
            {
                if (gvPacConsultas.RowCount <= 0) return;
                if (MessageBox.Show("¿Desea ELIMINAR el registro seleccionado?", "Eliminar registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    //Eliminamos el registro seleccionado
                    var rn = new PatConsultationsController();

                    //Apropiamos los valores del Grid
                    Int64 intidpco = int.Parse(gvPacConsultas.GetFocusedRowCellValue(PatConsultations.Fields.IdConsultations.ToString()).ToString());

                    //Obtenemos el objeto
                    var obj = rn.GetById(intidpco);

                    if (obj != null)
                    {
                        rn.Delete(obj.IdConsultations);
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void ModificarRegistro()
        {
            try
            {
                if (gvPacConsultas.RowCount <= 0) return;
                var rn = new PatConsultationsController();

                //Apropiamos los valores del Grid
                Int64 intidpco = int.Parse(gvPacConsultas.GetFocusedRowCellValue(PatConsultations.Fields.IdConsultations.ToString()).ToString());

                //Obtenemos el objeto
                var obj = rn.GetById(intidpco);

                if (obj != null)
                {
                    var frm = new FPacConsultas(obj);
                    var resultado = frm.ShowDialog();
                    if (resultado == DialogResult.OK)
                    {
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void btnAuditoría_Click(object sender, EventArgs e)
        {
            if (gvPacConsultas.RowCount <= 0) return;
            var rn = new PatConsultationsController();

            //Apropiamos los valores del Grid
            Int64 intidpco = int.Parse(gvPacConsultas.GetFocusedRowCellValue(PatConsultations.Fields.IdConsultations.ToString()).ToString());

            //Obtenemos el objeto
            var obj = rn.GetById(intidpco);

            if (obj != null)
            {
                //Abrimos la AUDITORIA
                var frm = new FAuditaria(obj.ApiUsucre, obj.ApiFeccre, obj.ApiUsumod, obj.ApiFecmod, obj.ApiStatus);
                frm.ShowDialog();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarRegistro();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            ModificarRegistro();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frm = new FPacConsultas();
            var resultado = frm.ShowDialog();
            if (resultado == DialogResult.OK)
            {
                this.CargarListado();
            }
        }

        private void gcPacConsultas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                EliminarRegistro();
            }
        }

        private void gcPacConsultas_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ModificarRegistro();
        }

        private void gvPacConsultas_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            //gvPacConsultas.SetFocusedRowCellValue(EntPacConsultas.Fields.usucre.ToString(),"postgres");
        }

        private void gvPacConsultas_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            
        }

        private void lPacConsultas_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            //CargarListado();
        }

        #endregion Methods
    }
}