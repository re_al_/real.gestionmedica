


namespace ReAl.GestionMedica.App.PAC
{
	partial class FPacMultimedia
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPacMultimedia));
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblfechapmu = new System.Windows.Forms.Label();
            this.dtpfechapmu = new DevExpress.XtraEditors.DateEdit();
            this.lblobservacionespmu = new System.Windows.Forms.Label();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.txtobservacionespmu = new DevExpress.XtraEditors.MemoEdit();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.txtArchivo = new DevExpress.XtraEditors.TextEdit();
            this.btnExaminar = new DevExpress.XtraEditors.SimpleButton();
            this.btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.dtpfechapmu.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpfechapmu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtobservacionespmu.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArchivo.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblTitulo.Location = new System.Drawing.Point(12, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(415, 53);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Administracion de pacmultimedia";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblfechapmu
            // 
            this.lblfechapmu.AutoSize = true;
            this.lblfechapmu.Location = new System.Drawing.Point(14, 92);
            this.lblfechapmu.Name = "lblfechapmu";
            this.lblfechapmu.Size = new System.Drawing.Size(40, 13);
            this.lblfechapmu.TabIndex = 1;
            this.lblfechapmu.Text = "Fecha:";
            // 
            // dtpfechapmu
            // 
            this.dtpfechapmu.EditValue = new System.DateTime(2013, 6, 5, 14, 24, 10, 0);
            this.dtpfechapmu.Location = new System.Drawing.Point(120, 89);
            this.dtpfechapmu.Name = "dtpfechapmu";
            this.dtpfechapmu.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpfechapmu.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpfechapmu.Size = new System.Drawing.Size(204, 20);
            this.dtpfechapmu.TabIndex = 2;
            // 
            // lblobservacionespmu
            // 
            this.lblobservacionespmu.AutoSize = true;
            this.lblobservacionespmu.Location = new System.Drawing.Point(14, 144);
            this.lblobservacionespmu.Name = "lblobservacionespmu";
            this.lblobservacionespmu.Size = new System.Drawing.Size(82, 13);
            this.lblobservacionespmu.TabIndex = 6;
            this.lblobservacionespmu.Text = "Observaciones:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Location = new System.Drawing.Point(348, 255);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(108, 34);
            this.btnCancelar.TabIndex = 8;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // txtobservacionespmu
            // 
            this.txtobservacionespmu.Location = new System.Drawing.Point(120, 141);
            this.txtobservacionespmu.Name = "txtobservacionespmu";
            this.txtobservacionespmu.Size = new System.Drawing.Size(452, 108);
            this.txtobservacionespmu.TabIndex = 7;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 118);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Archivo:";
            // 
            // txtArchivo
            // 
            this.txtArchivo.Location = new System.Drawing.Point(120, 115);
            this.txtArchivo.Name = "txtArchivo";
            this.txtArchivo.Properties.Appearance.BackColor = System.Drawing.SystemColors.Info;
            this.txtArchivo.Properties.Appearance.ForeColor = System.Drawing.Color.Black;
            this.txtArchivo.Properties.Appearance.Options.UseBackColor = true;
            this.txtArchivo.Properties.Appearance.Options.UseForeColor = true;
            this.txtArchivo.Properties.ReadOnly = true;
            this.txtArchivo.Size = new System.Drawing.Size(409, 20);
            this.txtArchivo.TabIndex = 4;
            // 
            // btnExaminar
            // 
            this.btnExaminar.Location = new System.Drawing.Point(535, 112);
            this.btnExaminar.Name = "btnExaminar";
            this.btnExaminar.Size = new System.Drawing.Size(35, 23);
            this.btnExaminar.TabIndex = 5;
            this.btnExaminar.Text = "...";
            this.btnExaminar.Click += new System.EventHandler(this.btnExaminar_Click);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnAceptar.Appearance.Options.UseBackColor = true;
            this.btnAceptar.Location = new System.Drawing.Point(462, 255);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(108, 34);
            this.btnAceptar.TabIndex = 9;
            this.btnAceptar.Text = "&Registrar";
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // FPacMultimedia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(582, 301);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.btnExaminar);
            this.Controls.Add(this.txtArchivo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtobservacionespmu);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lblfechapmu);
            this.Controls.Add(this.dtpfechapmu);
            this.Controls.Add(this.lblobservacionespmu);
            this.Controls.Add(this.lblTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("FPacMultimedia.IconOptions.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FPacMultimedia";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Administracion de pacmultimedia";
            this.Load += new System.EventHandler(this.fPacMultimedia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtpfechapmu.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpfechapmu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtobservacionespmu.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtArchivo.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		
		#endregion

        internal System.Windows.Forms.Label lblTitulo;
		internal System.Windows.Forms.Label lblfechapmu;
		private DevExpress.XtraEditors.DateEdit dtpfechapmu;
        internal System.Windows.Forms.Label lblobservacionespmu;
		private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.MemoEdit txtobservacionespmu;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        internal System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.TextEdit txtArchivo;
        private DevExpress.XtraEditors.SimpleButton btnExaminar;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
	}
}

