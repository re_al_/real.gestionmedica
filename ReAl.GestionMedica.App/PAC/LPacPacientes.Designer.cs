﻿


namespace ReAl.GestionMedica.App.PAC
{
    partial class LPacPacientes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcPacPaciente = new DevExpress.XtraGrid.GridControl();
            this.gvPacPaciente = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.btnNuevo = new DevExpress.XtraEditors.SimpleButton();
            this.btnSeleccionar = new DevExpress.XtraEditors.SimpleButton();
            this.btnAuditoría = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnEliminar = new DevExpress.XtraEditors.SimpleButton();
            this.btnCita = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcPacPaciente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPacPaciente)).BeginInit();
            this.SuspendLayout();
            // 
            // gcPacPaciente
            // 
            this.gcPacPaciente.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcPacPaciente.Location = new System.Drawing.Point(-1, 0);
            this.gcPacPaciente.MainView = this.gvPacPaciente;
            this.gcPacPaciente.Name = "gcPacPaciente";
            this.gcPacPaciente.Size = new System.Drawing.Size(785, 510);
            this.gcPacPaciente.TabIndex = 0;
            this.gcPacPaciente.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvPacPaciente});
            this.gcPacPaciente.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gcPacPaciente_KeyDown);
            this.gcPacPaciente.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.gcPacPaciente_MouseDoubleClick);
            // 
            // gvPacPaciente
            // 
            this.gvPacPaciente.GridControl = this.gcPacPaciente;
            this.gvPacPaciente.Name = "gvPacPaciente";
            this.gvPacPaciente.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvPacPaciente.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvPacPaciente.OptionsBehavior.Editable = false;
            this.gvPacPaciente.OptionsBehavior.ReadOnly = true;
            this.gvPacPaciente.OptionsFind.AlwaysVisible = true;
            // 
            // btnNuevo
            // 
            this.btnNuevo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNuevo.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnNuevo.Appearance.Options.UseBackColor = true;
            this.btnNuevo.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnNuevo.Location = new System.Drawing.Point(664, 516);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(108, 34);
            this.btnNuevo.TabIndex = 6;
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnSeleccionar
            // 
            this.btnSeleccionar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSeleccionar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Question;
            this.btnSeleccionar.Appearance.Options.UseBackColor = true;
            this.btnSeleccionar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSeleccionar.Location = new System.Drawing.Point(12, 516);
            this.btnSeleccionar.Name = "btnSeleccionar";
            this.btnSeleccionar.Size = new System.Drawing.Size(108, 34);
            this.btnSeleccionar.TabIndex = 1;
            this.btnSeleccionar.Text = "&Seleccionar Paciente";
            this.btnSeleccionar.Click += new System.EventHandler(this.btnSeleccionar_Click);
            // 
            // btnAuditoría
            // 
            this.btnAuditoría.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAuditoría.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAuditoría.Location = new System.Drawing.Point(323, 516);
            this.btnAuditoría.Name = "btnAuditoría";
            this.btnAuditoría.Size = new System.Drawing.Size(108, 34);
            this.btnAuditoría.TabIndex = 3;
            this.btnAuditoría.Text = "&Auditoría";
            this.btnAuditoría.Click += new System.EventHandler(this.btnAuditoría_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Primary;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Location = new System.Drawing.Point(551, 517);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(107, 33);
            this.simpleButton1.TabIndex = 5;
            this.simpleButton1.Text = "&Modificar";
            this.simpleButton1.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEliminar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Danger;
            this.btnEliminar.Appearance.Options.UseBackColor = true;
            this.btnEliminar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnEliminar.Location = new System.Drawing.Point(209, 516);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(108, 34);
            this.btnEliminar.TabIndex = 2;
            this.btnEliminar.Text = "&Eliminar";
            this.btnEliminar.Visible = false;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnCita
            // 
            this.btnCita.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCita.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Warning;
            this.btnCita.Appearance.Options.UseBackColor = true;
            this.btnCita.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnCita.Location = new System.Drawing.Point(437, 516);
            this.btnCita.Name = "btnCita";
            this.btnCita.Size = new System.Drawing.Size(108, 34);
            this.btnCita.TabIndex = 4;
            this.btnCita.Text = "Nueva &Cita";
            this.btnCita.Click += new System.EventHandler(this.btnCita_Click);
            // 
            // LPacPacientes
            // 
            this.AcceptButton = this.btnSeleccionar;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.btnCita);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btnAuditoría);
            this.Controls.Add(this.btnSeleccionar);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.gcPacPaciente);
            this.Name = "LPacPacientes";
            this.Text = "Listado de Pacientes";
            this.Activated += new System.EventHandler(this.lPacPacientes_Activated);
            this.Load += new System.EventHandler(this.lPacPacientes_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lPacPacientes_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.gcPacPaciente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPacPaciente)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gcPacPaciente;
        private DevExpress.XtraGrid.Views.Grid.GridView gvPacPaciente;
        private DevExpress.XtraEditors.SimpleButton btnNuevo;
        private DevExpress.XtraEditors.SimpleButton btnSeleccionar;
        private DevExpress.XtraEditors.SimpleButton btnAuditoría;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btnEliminar;
        private DevExpress.XtraEditors.SimpleButton btnCita;
    }
}