﻿using ReAl.GestionMedica.App.RPT;

namespace ReAl.GestionMedica.App.PAC
{
    public partial class FPacRptAusencias : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public FPacRptAusencias()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var frm = new RptViewerPac("A", dtpFecIni.DateTime, dtpFecFin.DateTime);
            frm.ShowDialog();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void fGasRptTendenciaAtencion_Load(object sender, EventArgs e)
        {
            dtpFecIni.DateTime = DateTime.Now;
            dtpFecFin.DateTime = DateTime.Now;
        }

        #endregion Methods
    }
}