


namespace ReAl.GestionMedica.App.PAC
{
	partial class LPacMultimedia
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.gcPacMultimedia = new DevExpress.XtraGrid.GridControl();
            this.gvPacMultimedia = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.lblTitulo = new System.Windows.Forms.Label();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnAuditoría = new DevExpress.XtraEditors.SimpleButton();
            this.btnNuevo = new DevExpress.XtraEditors.SimpleButton();
            this.btnVer = new DevExpress.XtraEditors.SimpleButton();
            this.btnEliminar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.gcPacMultimedia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPacMultimedia)).BeginInit();
            this.SuspendLayout();
            // 
            // gcPacMultimedia
            // 
            this.gcPacMultimedia.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gcPacMultimedia.Location = new System.Drawing.Point(0, 65);
            this.gcPacMultimedia.MainView = this.gvPacMultimedia;
            this.gcPacMultimedia.Name = "gcPacMultimedia";
            this.gcPacMultimedia.Size = new System.Drawing.Size(784, 445);
            this.gcPacMultimedia.TabIndex = 1;
            this.gcPacMultimedia.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvPacMultimedia});
            this.gcPacMultimedia.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.gcPacMultimedia_MouseDoubleClick);
            // 
            // gvPacMultimedia
            // 
            this.gvPacMultimedia.GridControl = this.gcPacMultimedia;
            this.gvPacMultimedia.Name = "gvPacMultimedia";
            this.gvPacMultimedia.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvPacMultimedia.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gvPacMultimedia.OptionsBehavior.Editable = false;
            this.gvPacMultimedia.OptionsBehavior.ReadOnly = true;
            this.gvPacMultimedia.InitNewRow += new DevExpress.XtraGrid.Views.Grid.InitNewRowEventHandler(this.gvPacMultimedia_InitNewRow);
            this.gvPacMultimedia.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gvPacMultimedia_RowUpdated);
            this.gvPacMultimedia.KeyDown += new System.Windows.Forms.KeyEventHandler(this.gcPacMultimedia_KeyDown);
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblTitulo.Location = new System.Drawing.Point(12, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(617, 53);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Administracion de Multimedia";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Primary;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Location = new System.Drawing.Point(551, 516);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(107, 34);
            this.simpleButton1.TabIndex = 5;
            this.simpleButton1.Text = "&Modificar";
            this.simpleButton1.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnAuditoría
            // 
            this.btnAuditoría.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAuditoría.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnAuditoría.Location = new System.Drawing.Point(323, 516);
            this.btnAuditoría.Name = "btnAuditoría";
            this.btnAuditoría.Size = new System.Drawing.Size(108, 34);
            this.btnAuditoría.TabIndex = 3;
            this.btnAuditoría.Text = "&Auditoría";
            this.btnAuditoría.Click += new System.EventHandler(this.btnAuditoría_Click);
            // 
            // btnNuevo
            // 
            this.btnNuevo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNuevo.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnNuevo.Appearance.Options.UseBackColor = true;
            this.btnNuevo.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnNuevo.Location = new System.Drawing.Point(664, 516);
            this.btnNuevo.Name = "btnNuevo";
            this.btnNuevo.Size = new System.Drawing.Size(108, 34);
            this.btnNuevo.TabIndex = 6;
            this.btnNuevo.Text = "&Nuevo";
            this.btnNuevo.Click += new System.EventHandler(this.btnNuevo_Click);
            // 
            // btnVer
            // 
            this.btnVer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnVer.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Question;
            this.btnVer.Appearance.Options.UseBackColor = true;
            this.btnVer.Location = new System.Drawing.Point(12, 516);
            this.btnVer.Name = "btnVer";
            this.btnVer.Size = new System.Drawing.Size(108, 33);
            this.btnVer.TabIndex = 2;
            this.btnVer.Text = "&Ver";
            this.btnVer.Click += new System.EventHandler(this.btnVer_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEliminar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Danger;
            this.btnEliminar.Appearance.Options.UseBackColor = true;
            this.btnEliminar.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnEliminar.Location = new System.Drawing.Point(437, 516);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(108, 34);
            this.btnEliminar.TabIndex = 4;
            this.btnEliminar.Text = "&Eliminar";
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // LPacMultimedia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnVer);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.btnAuditoría);
            this.Controls.Add(this.btnNuevo);
            this.Controls.Add(this.lblTitulo);
            this.Controls.Add(this.gcPacMultimedia);
            this.Name = "LPacMultimedia";
            this.Text = "Administracion de Contenido Multimedia";
            this.Activated += new System.EventHandler(this.lPacMultimedia_Activated);
            this.Load += new System.EventHandler(this.lPacMultimedia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gcPacMultimedia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvPacMultimedia)).EndInit();
            this.ResumeLayout(false);

		}
		
		#endregion
		
		private DevExpress.XtraGrid.GridControl gcPacMultimedia;
		private DevExpress.XtraGrid.Views.Grid.GridView gvPacMultimedia;
        internal System.Windows.Forms.Label lblTitulo;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SimpleButton btnAuditoría;
        private DevExpress.XtraEditors.SimpleButton btnNuevo;
        private DevExpress.XtraEditors.SimpleButton btnVer;
        private DevExpress.XtraEditors.SimpleButton btnEliminar;
	}
}

