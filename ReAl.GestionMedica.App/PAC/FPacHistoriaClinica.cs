﻿using System.Drawing.Imaging;
using System.Formats.Tar;
using System.IO;
using System.Net;
using ReAl.GestionMedica.App.App_Class;
using ReAl.GestionMedica.App.Properties;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
using ReAl.GestionMedica.Class;
using ReAl.Utils;

namespace ReAl.GestionMedica.App.PAC
{
    public partial class FPacHistoriaClinica : DevExpress.XtraEditors.XtraForm
    {
        #region Fields

        private readonly Color _color = Color.Red;
        private readonly Int64 _intGrosor = 6;
        private bool _draw = false;
        private float _intScala = 1;
        private Image _mainimage;

        #endregion Fields

        #region Constructors

        public FPacHistoriaClinica()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                Task<bool> taskFirebase = Task.Run<bool>(async () => await GuardarHoja3());
                bProcede = this.GuardarHoja1() && this.GuardarHoja2() && taskFirebase.Result;
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog(this);
            }

            if (bProcede)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnAuditoría_Click(object sender, EventArgs e)
        {
            var rn = new PatHistoryneuro1Controller();
            var obj = rn.GetById(cParametrosApp.AppPaciente.IdPatients);

            if (obj != null)
            {
                //Abrimos la AUDITORIA
                var frm = new FAuditaria(obj.ApiUsucre, obj.ApiFeccre, obj.ApiUsumod, obj.ApiFecmod, obj.ApiStatus);
                frm.ShowDialog();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnRecargar_Click(object sender, EventArgs e)
        {
            picSilueta.Image = Resources.Silueta;
            _mainimage = Resources.Silueta;
            picSilueta.Refresh();
        }

        private void CargarDatos()
        {
            var bProcede = false;
            try
            {
                bProcede = this.CargarHoja1() && this.CargarHoja2() && this.CargarHoja3();

                //if (!bProcede)
                    //throw new Exception("ALO: NO SE HAN CARGADO CORRECTAMENTE LOS DAOTS");
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog(this);
            }
        }

        private bool CargarHoja1()
        {
            var rn = new PatHistoryneuro1Controller();
            var obj = rn.GetById(cParametrosApp.AppPaciente.IdPatients);

            if (obj != null)
            {
                txth1motivophc.Text = obj.H1Motivo;
                txth1enfermedadphc.Text = obj.H1Enfermedad;
                txth1Cirugiasphc.Text = obj.H1Cirujias;
                txth1infeccionesphc.Text = obj.H1Infecciones;
                txth1traumatismosphc.Text = obj.H1Traumatismos;
                txth1cardiovascularphc.Text = obj.H1Cardiovascular;
                txth1metabolicophc.Text = obj.H1Metabolico;
                txth1toxicophc.Text = obj.H1Toxico;
                txth1familiarphc.Text = obj.H1Familiar;
                txth1ginecoobsphc.Text = obj.H1Ginecoobs;
                txth1otrosphc.Text = obj.H1Otros;
                nudh1embarazosphc.EditValue = obj.H1Embarazos;
                nudh1gestaphc.EditValue = obj.H1Gesta;
                nudh1abortosphc.EditValue = obj.H1Abortos;
                txth1mesntruacionesphc.Text = obj.H1Mesntruaciones;
                txth1fumphc.Text = obj.H1Fum;
                txth1fupphc.Text = obj.H1Fup;
                txth1revisionphc.Text = obj.H1Revision;
                txth1examenfisicophc.Text = obj.H1Examenfisico;
                txth1taphc.Text = obj.H1Ta;
                txth1fcphc.Text = obj.H1Fc;
                txth1frphc.Text = obj.H1Fr;
                txth1tempphc.Text = obj.H1Temp;
                txth1tallaphc.Text = obj.H1Talla;
                txth1pesophc.Text = obj.H1Peso;
                txth1pcphc.Text = obj.H1Pc;
                txth2cabezaphc.Text = obj.H2Cabeza;
                txth2cuellophc.Text = obj.H2Cuello;
                txth2toraxphc.Text = obj.H2Torax;
                txth2abdomenphc.Text = obj.H2Abdomen;
                txth2genitourinariophc.Text = obj.H2Genitourinario;
                txth2extremidadesphc.Text = obj.H2Extremidades;
                txth2conalertaphc.Text = obj.H2Conalerta;
                txth2conconfusionphc.Text = obj.H2Conconfusion;
                txth2consomnolenciaphc.Text = obj.H2Consomnolencia;
                txth2conestuforphc.Text = obj.H2Conestufor;
                txth2concomaphc.Text = obj.H2Concoma;
                txth2oriespaciophc.Text = obj.H2Oriespacio;
                txth2orilugarphc.Text = obj.H2Orilugar;
                txth2oripersonaphc.Text = obj.H2Oripersona;
                txth2razideaphc.Text = obj.H2Razidea;
                txth2razjuiciophc.Text = obj.H2Razjuicio;
                txth2razabstraccionphc.Text = obj.H2Razabstraccion;
                txth2conocimientosgenerales.Text = obj.H2Conocimientosgenerales;
                txth2atgdesatentophc.Text = obj.H2Atgdesatento;
                txth2atgfluctuantephc.Text = obj.H2Atgfluctuante;
                txth2atgnegativophc.Text = obj.H2Atgnegativo;
                txth2atgnegligentephc.Text = obj.H2Atgnegligente;
                txth2pagconfusionphc.Text = obj.H2Pagconfusion;
                txth2pagdeliriophc.Text = obj.H2Pagdelirio;
                txth2paedelusionesphc.Text = obj.H2Paedelusiones;
                txth2paeilsuionesphc.Text = obj.H2Paeilsuiones;
                txth2paealucinacionesphc.Text = obj.H2Paealucinaciones;
                txth3conrecientephc.Text = obj.H3Conreciente;
                txth3conremotaphc.Text = obj.H3Conremota;
                txth3afeeuforicophc.Text = obj.H3Afeeuforico;
                txth3afeplanophc.Text = obj.H3Afeplano;
                txth3afedeprimidophc.Text = obj.H3Afedeprimido;
                txth3afelabilphc.Text = obj.H3Afelabil;
                txth3afehostilphc.Text = obj.H3Afehostil;
                txth3afeinadecuadophc.Text = obj.H3Afeinadecuado;
                txth3hdoizquierdophc.Text = obj.H3Hdoizquierdo;
                txth3hdoderechophc.Text = obj.H3Hdoderecho;
                txth3cgeexpresionphc.Text = obj.H3Cgeexpresion;
                txth3cgenominacionphc.Text = obj.H3Cgenominacion;
                txth3cgerepeticionphc.Text = obj.H3Cgerepeticion;
                txth3cgecomprensionphc.Text = obj.H3Cgecomprension;
                txth3apebrocaphc.Text = obj.H3Apebroca;
                txth3apeconduccionphc.Text = obj.H3Apeconduccion;
                txth3apewernickephc.Text = obj.H3Apewernicke;
                txth3apeglobalphc.Text = obj.H3Apeglobal;
                txth3apeanomiaphc.Text = obj.H3Apeanomia;
                txth3atrmotoraphc.Text = obj.H3Atrmotora;
                txth3atrsensitivaphc.Text = obj.H3Atrsensitiva;
                txth3atrmixtaphc.Text = obj.H3Atrmixta;
                txth3ataexpresionphc.Text = obj.H3Ataexpresion;
                txth3atacomprensionphc.Text = obj.H3Atacomprension;
                txth3atamixtaphc.Text = obj.H3Atamixta;
                txth3aprmotoraphc.Text = obj.H3Aprmotora;
                txth3aprsensorialphc.Text = obj.H3Aprsensorial;
                txth3lesalexiaphc.Text = obj.H3Lesalexia;
                txth3lesagrafiaphc.Text = obj.H3Lesagrafia;
                txth3lpdacalculiaphc.Text = obj.H3Lpdacalculia;
                txth3lpdagrafiaphc.Text = obj.H3Lpdagrafia;
                txth3lpddesorientacionphc.Text = obj.H3Lpddesorientacion;
                txth3lpdagnosiaphc.Text = obj.H3Lpdagnosia;
                txth3agnauditivaphc.Text = obj.H3Agnauditiva;
                txth3agnvisualphc.Text = obj.H3Agnvisual;
                txth3agntactilphc.Text = obj.H3Agntactil;
                txth3aprideacionalphc.Text = obj.H3Aprideacional;
                txth3aprideomotoraphc.Text = obj.H3Aprideomotora;
                txth3aprdesatencionphc.Text = obj.H3Aprdesatencion;
                txth3anosognosiaphc.Text = obj.H3Anosognosia;

                return true;
            }
            else return false;
        }

        private bool CargarHoja2()
        {
            var rn = new PatHistoryneuro2Controller();
            var obj = rn.GetById(cParametrosApp.AppPaciente.IdPatients);

            if (obj != null)
            {
                txth4olfnormalphc.Text = obj.H4Olfnormal;
                txth4olfanosmiaphc.Text = obj.H4Olfanosmia;
                txth4optscodphc.Text = obj.H4Optscod;
                txth4optscoiphc.Text = obj.H4Optscoi;
                txth4optccodphc.Text = obj.H4Optccod;
                txth4optccoiphc.Text = obj.H4Optccoi;
                txth4fondoodphc.Text = obj.H4Fondood;
                txth4fondooiphc.Text = obj.H4Fondooi;
                txth4campimetriaphc.Text = obj.H4Campimetria;
                txth4prosisphc.Text = obj.H4Prosis;
                txth4posicionojosphc.Text = obj.H4Posicionojos;
                txth4exoftalmiaphc.Text = obj.H4Exoftalmia;
                txth4hornerphc.Text = obj.H4Horner;
                txth4movocularesphc.Text = obj.H4Movoculares;
                txth4nistagmuxphc.Text = obj.H4Nistagmux;
                txth4diplopia1phc.Text = obj.H4diplopia1;
                txth4diplopia2phc.Text = obj.H4diplopia2;
                txth4diplopia3phc.Text = obj.H4diplopia3;
                txth4diplopia4phc.Text = obj.H4diplopia4;
                txth4diplopia5phc.Text = obj.H4diplopia5;
                txth4diplopia6phc.Text = obj.H4diplopia6;
                txth4diplopia7phc.Text = obj.H4diplopia7;
                txth4diplopia8phc.Text = obj.H4diplopia8;
                txth4diplopia9phc.Text = obj.H4diplopia9;
                txth4convergenciaphc.Text = obj.H4Convergencia;
                txth4acomodacionodphc.Text = obj.H4Acomodacionod;
                txth4acomodacionoiphc.Text = obj.H4Acomodacionoi;
                txth4pupulasodphc.Text = obj.H4Pupulasod;
                txth4pupulasoiphc.Text = obj.H4Pupulasoi;
                txth4formaphc.Text = obj.H4Forma;
                txth4fotomotorodphc.Text = obj.H4Fotomotorod;
                txth4fotomotoroiphc.Text = obj.H4Fotomotoroi;
                txth4consensualdaiphc.Text = obj.H4Consensualdai;
                txth4consensualiadphc.Text = obj.H4Consensualiad;
                txth4senfacialderphc.Text = obj.H4Senfacialder;
                txth4senfacializqphc.Text = obj.H4Senfacializq;
                txth4reflejoderphc.Text = obj.H4Reflejoder;
                txth4reflejoizqphc.Text = obj.H4Reflejoizq;
                txth4aberturabocaphc.Text = obj.H4Aberturaboca;
                txth4movmasticatoriosphc.Text = obj.H4Movmasticatorios;
                txth4refmentonianophc.Text = obj.H4Refmentoniano;
                txth4facsimetriaphc.Text = obj.H4Facsimetria;
                txth4facmovimientosphc.Text = obj.H4Facmovimientos;
                txth4facparalisisphc.Text = obj.H4Facparalisis;
                txth4faccentralphc.Text = obj.H4Faccentral;
                txth4facperifericaphc.Text = obj.H4Facperiferica;
                txth4facgustophc.Text = obj.H4Facgusto;
                txth5otoscopiaphc.Text = obj.H5Otoscopia;
                txth5aguaudiderphc.Text = obj.H5Aguaudider;
                txth5aguaudiizqphc.Text = obj.H5Aguaudiizq;
                txth5weberphc.Text = obj.H5Weber;
                txth5rinnephc.Text = obj.H5Rinne;
                txth5pruebaslabphc.Text = obj.H5Pruebaslab;
                txth5elevpaladarphc.Text = obj.H5Elevpaladar;
                txth5uvulaphc.Text = obj.H5Uvula;
                txth5refnauceosophc.Text = obj.H5Refnauceoso;
                txth5deglucionphc.Text = obj.H5deglucion;
                txth5tonovozphc.Text = obj.H5Tonovoz;
                txth5esternocleidomastoideophc.Text = obj.H5Esternocleidomastoideo;
                txth5trapeciophc.Text = obj.H5Trapecio;
                txth5desviacionphc.Text = obj.H5desviacion;
                txth5atrofiaphc.Text = obj.H5Atrofia;
                txth5fasciculacionphc.Text = obj.H5Fasciculacion;
                txth5fuerzaphc.Text = obj.H5Fuerza;
                txth5marchaphc.Text = obj.H5Marcha;
                txth5tonophc.Text = obj.H5Tono;
                txth5volumenphc.Text = obj.H5Volumen;
                txth5fasciculacionesphc.Text = obj.H5Fasciculaciones;
                txth5fuemuscularphc.Text = obj.H5Fuemuscular;
                txth5movinvoluntariosphc.Text = obj.H5Movinvoluntarios;
                txth5equilibratoriaphc.Text = obj.H5Equilibratoria;
                txth5rombergphc.Text = obj.H5Romberg;
                txth5dednarderphc.Text = obj.H5dednarder;
                txth5dednarizqphc.Text = obj.H5dednarizq;
                txth5deddedderphc.Text = obj.H5deddedder;
                txth5deddedizqphc.Text = obj.H5deddedizq;
                txth5talrodderphc.Text = obj.H5Talrodder;
                txth5talrodizqphc.Text = obj.H5Talrodizq;
                txth5movrapidphc.Text = obj.H5Movrapid;
                txth5rebotephc.Text = obj.H5Rebote;
                txth5habilidadespecifphc.Text = obj.H5Habilidadespecif;
                return true;
            }
            else return false;
        }

        private bool CargarHoja3()
        {
            var rn = new PatHistoryneuro3Controller();
            var obj = rn.GetById(cParametrosApp.AppPaciente.IdPatients);

            if (obj != null)
            {
                txth6reflejos01phc.Text = obj.H6Reflejos01;
                txth6reflejos02phc.Text = obj.H6Reflejos02;
                txth6reflejos03phc.Text = obj.H6Reflejos03;
                txth6reflejos04phc.Text = obj.H6Reflejos04;
                txth6reflejos05phc.Text = obj.H6Reflejos05;
                txth6reflejos06phc.Text = obj.H6Reflejos06;
                txth6reflejos07phc.Text = obj.H6Reflejos07;
                txth6reflejos08phc.Text = obj.H6Reflejos08;
                txth6reflejos09phc.Text = obj.H6Reflejos09;
                txth6reflejos10phc.Text = obj.H6Reflejos10;
                txth6miotbicderphc.Text = obj.H6Miotbicder;
                txth6miotbicizqphc.Text = obj.H6Miotbicizq;
                txth6miottriderphc.Text = obj.H6Miottrider;
                txth6miottriizqphc.Text = obj.H6Miottriizq;
                txth6miotestderphc.Text = obj.H6Miotestder;
                txth6miotestizqphc.Text = obj.H6Miotestizq;
                txth6miotpatderphc.Text = obj.H6Miotpatder;
                txth6miotpatizqphc.Text = obj.H6Miotpatizq;
                txth6miotaquderphc.Text = obj.H6Miotaquder;
                txth6miotaquizqphc.Text = obj.H6Miotaquizq;
                txth6patpladerphc.Text = obj.H6Patplader;
                txth6patplaizqphc.Text = obj.H6Patplaizq;
                txth6pathofderphc.Text = obj.H6Pathofder;
                txth6pathofizqphc.Text = obj.H6Pathofizq;
                txth6pattroderphc.Text = obj.H6Pattroder;
                txth6pattroizqphc.Text = obj.H6Pattroizq;
                txth6patprederphc.Text = obj.H6Patpreder;
                txth6patpreizqphc.Text = obj.H6Patpreizq;
                txth6patpmenderphc.Text = obj.H6Patpmender;
                txth6patpmenizqphc.Text = obj.H6Patpmenizq;
                txth6supbicderphc.Text = obj.H6Supbicder;
                txth6supbicizqphc.Text = obj.H6Supbicizq;
                txth6suptriderphc.Text = obj.H6Suptrider;
                txth6suptriizqphc.Text = obj.H6Suptriizq;
                txth6supestderphc.Text = obj.H6Supestder;
                txth6supestizqphc.Text = obj.H6Supestizq;
                txth6otrmaxilarphc.Text = obj.H6Otrmaxilar;
                txth6otrglabelarphc.Text = obj.H6Otrglabelar;
                txth6sistsensitivophc.Text = obj.H6Sistsensitivo;
                txth6dolorprofphc.Text = obj.H6dolorprof;
                txth6posicionphc.Text = obj.H6Posicion;
                txth6vibracionphc.Text = obj.H6Vibracion;
                txth6dospuntosphc.Text = obj.H6dospuntos;
                txth6extincionphc.Text = obj.H6Extincion;
                txth6estereognosiaphc.Text = obj.H6Estereognosia;
                txth6grafestesiaphc.Text = obj.H6Grafestesia;
                txth6localizacionphc.Text = obj.H6Localizacion;

                try
                {
                    var strFile = cParametrosApp.StrTempFolder + obj.IdPatients + ".jpeg";
                    WebClient client = new WebClient();
                    byte[] imageBytes = client.DownloadData(obj.H6Imagenurl);
                    picSilueta.Image = cFuncionesImagenes.GenerarMemoryStreamImagen(imageBytes);
                }
                catch (Exception e)
                {
                }

                txth7rigideznucaphc.Text = obj.H7Rigideznuca;
                txth7sigkerningphc.Text = obj.H7Sigkerning;
                txth7fotofobiaphc.Text = obj.H7Fotofobia;
                txth7sigbrudzinskiphc.Text = obj.H7Sigbrudzinski;
                txth7hiperestesiaphc.Text = obj.H7Hiperestesia;
                txth7pulcarotideosphc.Text = obj.H7Pulcarotideos;
                txth7pultemporalesphc.Text = obj.H7Pultemporales;
                txth7cabezaphc.Text = obj.H7Cabeza;
                txth7cuellophc.Text = obj.H7Cuello;
                txth7nerviosperifphc.Text = obj.H7Nerviosperif;
                txth7colvertebralphc.Text = obj.H7Colvertebral;
                txth7resumenphc.Text = obj.H7Resumen;
                txth7diagnosticosphc.Text = obj.H7diagnosticos;
                txth7conductaphc.Text = obj.H7Conducta;
                txth7tratamientophc.Text = obj.H7Tratamiento;
                return true;
            }
            else return false;
        }

        private void fPacHistoriaClinica_Load(object sender, EventArgs e)
        {
            _mainimage = picSilueta.Image;
            _intScala = 2F;

            CargarDatos();
        }

        private bool GuardarHoja1()
        {
            var bProcede = false;

            try
            {
                //Insertamos el registro
                var rn = new PatHistoryneuro1Controller();
                var obj = new PatHistoryneuro1();
                obj.IdPatients = cParametrosApp.AppPaciente.IdPatients;
                obj.H1Motivo = txth1motivophc.Text;
                obj.H1Enfermedad = txth1enfermedadphc.Text;
                obj.H1Cirujias = txth1Cirugiasphc.Text;
                obj.H1Infecciones = txth1infeccionesphc.Text;
                obj.H1Traumatismos = txth1traumatismosphc.Text;
                obj.H1Cardiovascular = txth1cardiovascularphc.Text;
                obj.H1Metabolico = txth1metabolicophc.Text;
                obj.H1Toxico = txth1toxicophc.Text;
                obj.H1Familiar = txth1familiarphc.Text;
                obj.H1Ginecoobs = txth1ginecoobsphc.Text;
                obj.H1Otros = txth1otrosphc.Text;
                obj.H1Embarazos = int.Parse(nudh1embarazosphc.EditValue.ToString());
                obj.H1Gesta = int.Parse(nudh1gestaphc.EditValue.ToString());
                obj.H1Abortos = int.Parse(nudh1abortosphc.EditValue.ToString());
                obj.H1Mesntruaciones = txth1mesntruacionesphc.Text;
                obj.H1Fum = txth1fumphc.Text;
                obj.H1Fup = txth1fupphc.Text;
                obj.H1Revision = txth1revisionphc.Text;
                obj.H1Examenfisico = txth1examenfisicophc.Text;
                obj.H1Ta = txth1taphc.Text;
                obj.H1Fc = txth1fcphc.Text;
                obj.H1Fr = txth1frphc.Text;
                obj.H1Temp = txth1tempphc.Text;
                obj.H1Talla = txth1tallaphc.Text;
                obj.H1Peso = txth1pesophc.Text;
                obj.H1Pc = txth1pcphc.Text;
                obj.H2Cabeza = txth2cabezaphc.Text;
                obj.H2Cuello = txth2cuellophc.Text;
                obj.H2Torax = txth2toraxphc.Text;
                obj.H2Abdomen = txth2abdomenphc.Text;
                obj.H2Genitourinario = txth2genitourinariophc.Text;
                obj.H2Extremidades = txth2extremidadesphc.Text;
                obj.H2Conalerta = txth2conalertaphc.Text;
                obj.H2Conconfusion = txth2conconfusionphc.Text;
                obj.H2Consomnolencia = txth2consomnolenciaphc.Text;
                obj.H2Conestufor = txth2conestuforphc.Text;
                obj.H2Concoma = txth2concomaphc.Text;
                obj.H2Oriespacio = txth2oriespaciophc.Text;
                obj.H2Orilugar = txth2orilugarphc.Text;
                obj.H2Oripersona = txth2oripersonaphc.Text;
                obj.H2Razidea = txth2razideaphc.Text;
                obj.H2Razjuicio = txth2razjuiciophc.Text;
                obj.H2Razabstraccion = txth2razabstraccionphc.Text;
                obj.H2Conocimientosgenerales = txth2conocimientosgenerales.Text;
                obj.H2Atgdesatento = txth2atgdesatentophc.Text;
                obj.H2Atgfluctuante = txth2atgfluctuantephc.Text;
                obj.H2Atgnegativo = txth2atgnegativophc.Text;
                obj.H2Atgnegligente = txth2atgnegligentephc.Text;
                obj.H2Pagconfusion = txth2pagconfusionphc.Text;
                obj.H2Pagdelirio = txth2pagdeliriophc.Text;
                obj.H2Paedelusiones = txth2paedelusionesphc.Text;
                obj.H2Paeilsuiones = txth2paeilsuionesphc.Text;
                obj.H2Paealucinaciones = txth2paealucinacionesphc.Text;
                obj.H3Conreciente = txth3conrecientephc.Text;
                obj.H3Conremota = txth3conremotaphc.Text;
                obj.H3Afeeuforico = txth3afeeuforicophc.Text;
                obj.H3Afeplano = txth3afeplanophc.Text;
                obj.H3Afedeprimido = txth3afedeprimidophc.Text;
                obj.H3Afelabil = txth3afelabilphc.Text;
                obj.H3Afehostil = txth3afehostilphc.Text;
                obj.H3Afeinadecuado = txth3afeinadecuadophc.Text;
                obj.H3Hdoizquierdo = txth3hdoizquierdophc.Text;
                obj.H3Hdoderecho = txth3hdoderechophc.Text;
                obj.H3Cgeexpresion = txth3cgeexpresionphc.Text;
                obj.H3Cgenominacion = txth3cgenominacionphc.Text;
                obj.H3Cgerepeticion = txth3cgerepeticionphc.Text;
                obj.H3Cgecomprension = txth3cgecomprensionphc.Text;
                obj.H3Apebroca = txth3apebrocaphc.Text;
                obj.H3Apeconduccion = txth3apeconduccionphc.Text;
                obj.H3Apewernicke = txth3apewernickephc.Text;
                obj.H3Apeglobal = txth3apeglobalphc.Text;
                obj.H3Apeanomia = txth3apeanomiaphc.Text;
                obj.H3Atrmotora = txth3atrmotoraphc.Text;
                obj.H3Atrsensitiva = txth3atrsensitivaphc.Text;
                obj.H3Atrmixta = txth3atrmixtaphc.Text;
                obj.H3Ataexpresion = txth3ataexpresionphc.Text;
                obj.H3Atacomprension = txth3atacomprensionphc.Text;
                obj.H3Atamixta = txth3atamixtaphc.Text;
                obj.H3Aprmotora = txth3aprmotoraphc.Text;
                obj.H3Aprsensorial = txth3aprsensorialphc.Text;
                obj.H3Lesalexia = txth3lesalexiaphc.Text;
                obj.H3Lesagrafia = txth3lesagrafiaphc.Text;
                obj.H3Lpdacalculia = txth3lpdacalculiaphc.Text;
                obj.H3Lpdagrafia = txth3lpdagrafiaphc.Text;
                obj.H3Lpddesorientacion = txth3lpddesorientacionphc.Text;
                obj.H3Lpdagnosia = txth3lpdagnosiaphc.Text;
                obj.H3Agnauditiva = txth3agnauditivaphc.Text;
                obj.H3Agnvisual = txth3agnvisualphc.Text;
                obj.H3Agntactil = txth3agntactilphc.Text;
                obj.H3Aprideacional = txth3aprideacionalphc.Text;
                obj.H3Aprideomotora = txth3aprideomotoraphc.Text;
                obj.H3Aprdesatencion = txth3aprdesatencionphc.Text;
                obj.H3Anosognosia = txth3anosognosiaphc.Text;
                obj.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                rn.Create(obj);
                bProcede = true;
            }
            catch (Exception)
            {
                throw;
            }
            return bProcede;
        }

        private bool GuardarHoja2()
        {
            var bProcede = false;
            try
            {
                //Insertamos el registro
                var rn = new PatHistoryneuro2Controller();
                var obj = new PatHistoryneuro2();
                obj.IdPatients = cParametrosApp.AppPaciente.IdPatients;
                obj.H4Olfnormal = txth4olfnormalphc.Text;
                obj.H4Olfanosmia = txth4olfanosmiaphc.Text;
                obj.H4Optscod = txth4optscodphc.Text;
                obj.H4Optscoi = txth4optscoiphc.Text;
                obj.H4Optccod = txth4optccodphc.Text;
                obj.H4Optccoi = txth4optccoiphc.Text;
                obj.H4Fondood = txth4fondoodphc.Text;
                obj.H4Fondooi = txth4fondooiphc.Text;
                obj.H4Campimetria = txth4campimetriaphc.Text;
                obj.H4Prosis = txth4prosisphc.Text;
                obj.H4Posicionojos = txth4posicionojosphc.Text;
                obj.H4Exoftalmia = txth4exoftalmiaphc.Text;
                obj.H4Horner = txth4hornerphc.Text;
                obj.H4Movoculares = txth4movocularesphc.Text;
                obj.H4Nistagmux = txth4nistagmuxphc.Text;
                obj.H4diplopia1 = txth4diplopia1phc.Text;
                obj.H4diplopia2 = txth4diplopia2phc.Text;
                obj.H4diplopia3 = txth4diplopia3phc.Text;
                obj.H4diplopia4 = txth4diplopia4phc.Text;
                obj.H4diplopia5 = txth4diplopia5phc.Text;
                obj.H4diplopia6 = txth4diplopia6phc.Text;
                obj.H4diplopia7 = txth4diplopia7phc.Text;
                obj.H4diplopia8 = txth4diplopia8phc.Text;
                obj.H4diplopia9 = txth4diplopia9phc.Text;
                obj.H4Convergencia = txth4convergenciaphc.Text;
                obj.H4Acomodacionod = txth4acomodacionodphc.Text;
                obj.H4Acomodacionoi = txth4acomodacionoiphc.Text;
                obj.H4Pupulasod = txth4pupulasodphc.Text;
                obj.H4Pupulasoi = txth4pupulasoiphc.Text;
                obj.H4Forma = txth4formaphc.Text;
                obj.H4Fotomotorod = txth4fotomotorodphc.Text;
                obj.H4Fotomotoroi = txth4fotomotoroiphc.Text;
                obj.H4Consensualdai = txth4consensualdaiphc.Text;
                obj.H4Consensualiad = txth4consensualiadphc.Text;
                obj.H4Senfacialder = txth4senfacialderphc.Text;
                obj.H4Senfacializq = txth4senfacializqphc.Text;
                obj.H4Reflejoder = txth4reflejoderphc.Text;
                obj.H4Reflejoizq = txth4reflejoizqphc.Text;
                obj.H4Aberturaboca = txth4aberturabocaphc.Text;
                obj.H4Movmasticatorios = txth4movmasticatoriosphc.Text;
                obj.H4Refmentoniano = txth4refmentonianophc.Text;
                obj.H4Facsimetria = txth4facsimetriaphc.Text;
                obj.H4Facmovimientos = txth4facmovimientosphc.Text;
                obj.H4Facparalisis = txth4facparalisisphc.Text;
                obj.H4Faccentral = txth4faccentralphc.Text;
                obj.H4Facperiferica = txth4facperifericaphc.Text;
                obj.H4Facgusto = txth4facgustophc.Text;
                obj.H5Otoscopia = txth5otoscopiaphc.Text;
                obj.H5Aguaudider = txth5aguaudiderphc.Text;
                obj.H5Aguaudiizq = txth5aguaudiizqphc.Text;
                obj.H5Weber = txth5weberphc.Text;
                obj.H5Rinne = txth5rinnephc.Text;
                obj.H5Pruebaslab = txth5pruebaslabphc.Text;
                obj.H5Elevpaladar = txth5elevpaladarphc.Text;
                obj.H5Uvula = txth5uvulaphc.Text;
                obj.H5Refnauceoso = txth5refnauceosophc.Text;
                obj.H5deglucion = txth5deglucionphc.Text;
                obj.H5Tonovoz = txth5tonovozphc.Text;
                obj.H5Esternocleidomastoideo = txth5esternocleidomastoideophc.Text;
                obj.H5Trapecio = txth5trapeciophc.Text;
                obj.H5desviacion = txth5desviacionphc.Text;
                obj.H5Atrofia = txth5atrofiaphc.Text;
                obj.H5Fasciculacion = txth5fasciculacionphc.Text;
                obj.H5Fuerza = txth5fuerzaphc.Text;
                obj.H5Marcha = txth5marchaphc.Text;
                obj.H5Tono = txth5tonophc.Text;
                obj.H5Volumen = txth5volumenphc.Text;
                obj.H5Fasciculaciones = txth5fasciculacionesphc.Text;
                obj.H5Fuemuscular = txth5fuemuscularphc.Text;
                obj.H5Movinvoluntarios = txth5movinvoluntariosphc.Text;
                obj.H5Equilibratoria = txth5equilibratoriaphc.Text;
                obj.H5Romberg = txth5rombergphc.Text;
                obj.H5dednarder = txth5dednarderphc.Text;
                obj.H5dednarizq = txth5dednarizqphc.Text;
                obj.H5deddedder = txth5deddedderphc.Text;
                obj.H5deddedizq = txth5deddedizqphc.Text;
                obj.H5Talrodder = txth5talrodderphc.Text;
                obj.H5Talrodizq = txth5talrodizqphc.Text;
                obj.H5Movrapid = txth5movrapidphc.Text;
                obj.H5Rebote = txth5rebotephc.Text;
                obj.H5Habilidadespecif = txth5habilidadespecifphc.Text;
                obj.ApiTransaction = CApi.Transaccion.UPDATE.ToString();

                rn.Create(obj);
                bProcede = true;
            }
            catch (Exception)
            {
                throw;
            }
            return bProcede;
        }

        private async Task<bool> GuardarHoja3()
        {
            var bProcede = false;
            try
            {
                var credential = CFirebaseHelper.FirebaseLogin();
                picSilueta.Image.Save("temp.jpeg", ImageFormat.Jpeg);
                Stream stream = File.Open(@"temp.jpeg", FileMode.Open);

                //Guid myuuid = Guid.NewGuid();
                //string myuuidAsString = myuuid.ToString();
                var strExtension = "jpeg";
                //var strFileName = cParametrosApp.AppPaciente.IdPatients + "-" + myuuidAsString + "." +  strExtension;
                var strFileName = cParametrosApp.AppPaciente.IdPatients + "." + strExtension;
                var fireBaseUrl = await CFirebaseHelper.FirebaseUpload(credential, stream, strFileName, "history-images");

                //Insertamos el registro
                var rn = new PatHistoryneuro3Controller();
                var obj = new PatHistoryneuro3();
                obj.IdPatients = cParametrosApp.AppPaciente.IdPatients;
                obj.H6Reflejos01 = txth6reflejos01phc.Text;
                obj.H6Reflejos02 = txth6reflejos02phc.Text;
                obj.H6Reflejos03 = txth6reflejos03phc.Text;
                obj.H6Reflejos04 = txth6reflejos04phc.Text;
                obj.H6Reflejos05 = txth6reflejos05phc.Text;
                obj.H6Reflejos06 = txth6reflejos06phc.Text;
                obj.H6Reflejos07 = txth6reflejos07phc.Text;
                obj.H6Reflejos08 = txth6reflejos08phc.Text;
                obj.H6Reflejos09 = txth6reflejos09phc.Text;
                obj.H6Reflejos10 = txth6reflejos10phc.Text;
                obj.H6Miotbicder = txth6miotbicderphc.Text;
                obj.H6Miotbicizq = txth6miotbicizqphc.Text;
                obj.H6Miottrider = txth6miottriderphc.Text;
                obj.H6Miottriizq = txth6miottriizqphc.Text;
                obj.H6Miotestder = txth6miotestderphc.Text;
                obj.H6Miotestizq = txth6miotestizqphc.Text;
                obj.H6Miotpatder = txth6miotpatderphc.Text;
                obj.H6Miotpatizq = txth6miotpatizqphc.Text;
                obj.H6Miotaquder = txth6miotaquderphc.Text;
                obj.H6Miotaquizq = txth6miotaquizqphc.Text;
                obj.H6Patplader = txth6patpladerphc.Text;
                obj.H6Patplaizq = txth6patplaizqphc.Text;
                obj.H6Pathofder = txth6pathofderphc.Text;
                obj.H6Pathofizq = txth6pathofizqphc.Text;
                obj.H6Pattroder = txth6pattroderphc.Text;
                obj.H6Pattroizq = txth6pattroizqphc.Text;
                obj.H6Patpreder = txth6patprederphc.Text;
                obj.H6Patpreizq = txth6patpreizqphc.Text;
                obj.H6Patpmender = txth6patpmenderphc.Text;
                obj.H6Patpmenizq = txth6patpmenizqphc.Text;
                obj.H6Supbicder = txth6supbicderphc.Text;
                obj.H6Supbicizq = txth6supbicizqphc.Text;
                obj.H6Suptrider = txth6suptriderphc.Text;
                obj.H6Suptriizq = txth6suptriizqphc.Text;
                obj.H6Supestder = txth6supestderphc.Text;
                obj.H6Supestizq = txth6supestizqphc.Text;
                obj.H6Otrmaxilar = txth6otrmaxilarphc.Text;
                obj.H6Otrglabelar = txth6otrglabelarphc.Text;
                obj.H6Sistsensitivo = txth6sistsensitivophc.Text;
                obj.H6dolorprof = txth6dolorprofphc.Text;
                obj.H6Posicion = txth6posicionphc.Text;
                obj.H6Vibracion = txth6vibracionphc.Text;
                obj.H6dospuntos = txth6dospuntosphc.Text;
                obj.H6Extincion = txth6extincionphc.Text;
                obj.H6Estereognosia = txth6estereognosiaphc.Text;
                obj.H6Grafestesia = txth6grafestesiaphc.Text;
                obj.H6Localizacion = txth6localizacionphc.Text;
                //obj.H6Imagen = cFuncionesImagenes.ImageReadBinaryPictureBox(ref picSilueta);
                obj.H6Imagenurl = fireBaseUrl;
                obj.H7Rigideznuca = txth7rigideznucaphc.Text;
                obj.H7Sigkerning = txth7sigkerningphc.Text;
                obj.H7Fotofobia = txth7fotofobiaphc.Text;
                obj.H7Sigbrudzinski = txth7sigbrudzinskiphc.Text;
                obj.H7Hiperestesia = txth7hiperestesiaphc.Text;
                obj.H7Pulcarotideos = txth7pulcarotideosphc.Text;
                obj.H7Pultemporales = txth7pultemporalesphc.Text;
                obj.H7Cabeza = txth7cabezaphc.Text;
                obj.H7Cuello = txth7cuellophc.Text;
                obj.H7Nerviosperif = txth7nerviosperifphc.Text;
                obj.H7Colvertebral = txth7colvertebralphc.Text;
                obj.H7Resumen = txth7resumenphc.Text;
                obj.H7diagnosticos = txth7diagnosticosphc.Text;
                obj.H7Conducta = txth7conductaphc.Text;
                obj.H7Tratamiento = txth7tratamientophc.Text;
                obj.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                rn.Create(obj);
                bProcede = true;
            }
            catch (Exception)
            {
                throw;
            }
            return bProcede;
        }

        private void picSilueta_MouseDown(object sender, MouseEventArgs e)
        {
            _draw = true;
            var g = Graphics.FromImage(_mainimage);
            var pen1 = new Pen(_color, 4);
            g.DrawRectangle(pen1, e.X * _intScala, e.Y * _intScala, 2, 2);
            g.Save();
            picSilueta.Image = _mainimage;
        }

        private void picSilueta_MouseMove(object sender, MouseEventArgs e)
        {
            if (_draw)
            {
                var g = Graphics.FromImage(_mainimage);
                var brush = new SolidBrush(_color);
                g.FillRectangle(brush, e.X * _intScala, e.Y * _intScala, this._intGrosor, this._intGrosor);
                g.Save();
                picSilueta.Image = _mainimage;
            }
        }

        private void picSilueta_MouseUp(object sender, MouseEventArgs e)
        {
            _draw = false;
        }

        #endregion Methods
    }
}