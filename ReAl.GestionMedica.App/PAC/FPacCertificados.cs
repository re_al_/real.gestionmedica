﻿using System.Text;
using DevExpress.XtraRichEdit;
using ReAl.GestionMedica.App.RPT;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;
using ReAl.Utils;

namespace ReAl.GestionMedica.App.PAC
{
    public partial class FPacCertificados : DevExpress.XtraEditors.XtraForm
    {
        #region Fields

        private PatCertifications _myObj = null;

        #endregion Fields

        #region Constructors

        public FPacCertificados()
        {
            InitializeComponent();
        }

        public FPacCertificados(PatCertifications obj)
        {
            InitializeComponent();

            _myObj = obj;
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (this.GuardarRegistro(true))
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (this.GuardarRegistro(false))
            {
                //Mostramos la Plantilla
                var frm = new RptViewerPac(_myObj);
                frm.ShowDialog();
            }
        }

        private void CargarDatos()
        {
            try
            {
                if (_myObj != null)
                {
                    txtmatriculapcm.Text = _myObj.Registration;
                    dtpFechapcm.EditValue = DateTime.Now;

                    var enc = new ASCIIEncoding();
                    //txtcuerpocpl.Rtf = enc.GetString(_myObj.RtfFormat, 0, _myObj.RtfFormat.Length);
                    ReAlRichControl1.richEditControl1.Document.HtmlText = _myObj.HtmlFormat;
                    ReAlRichControl1.richEditControl1.ActiveViewType = RichEditViewType.Simple;

                    btnPreview.Visible = true;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void fPacCertificados_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            dtpFechapcm.DateTime = DateTime.Now;

            if (_myObj != null)
            {
                CargarDatos();
            }
        }

        private bool GuardarRegistro(bool bPregunta)
        {
            var bProcede = false;
            try
            {
                var rn = new PatCertificationsController();

                if (_myObj == null)
                {
                    ReAlRichControl1.richEditControl1.SaveDocument("temp.rtf", DocumentFormat.Rtf);
                    var obj = new PatCertifications();
                    //Insertamos el registro
                    obj.IdPatients = cParametrosApp.AppPaciente.IdPatients;
                    obj.Registration = txtmatriculapcm.Text;
                    obj.CertificationDate = dtpFechapcm.DateTime;
                    obj.TextFormat = ReAlRichControl1.richEditControl1.Document.Text;
                    obj.RtfFormat = Convert.ToBase64String(cFuncionesImagenes.ImageReadBinaryFile("temp.rtf"));
                    obj.HtmlFormat = ReAlRichControl1.richEditControl1.Document.HtmlText;
                    rn.Create(obj);
                    _myObj = obj;
                    bProcede = true;
                }
                else
                {
                    ReAlRichControl1.richEditControl1.SaveDocument("temp.rtf", DocumentFormat.Rtf);
                    //Actualizamos
                    _myObj.Registration = txtmatriculapcm.Text;
                    _myObj.CertificationDate = dtpFechapcm.DateTime;
                    _myObj.TextFormat = ReAlRichControl1.richEditControl1.Document.Text;
                    _myObj.RtfFormat = Convert.ToBase64String(cFuncionesImagenes.ImageReadBinaryFile("temp.rtf"));
                    _myObj.HtmlFormat = ReAlRichControl1.richEditControl1.Document.HtmlText;
                    _myObj.ApiTransaction= CApi.Transaccion.UPDATE.ToString();
                    rn.Update(_myObj);
                    bProcede = true;
                }
            }
            catch (Exception exp)
            {
                bProcede = false;
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }

            if (bPregunta)
            {
                var res = MessageBox.Show(
                    "¿Desea ver una vista previa de su Informe Médico?",
                    "Informe Médico",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1);

                if (res == DialogResult.Yes)
                {
                    var frm = new RptViewerPac(_myObj);
                    frm.ShowDialog();
                }
            }

            return bProcede;
        }

        #endregion Methods
    }
}