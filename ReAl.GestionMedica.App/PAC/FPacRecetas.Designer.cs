﻿


namespace ReAl.GestionMedica.App.PAC
{
    partial class FPacRecetas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPacRecetas));
            btnPreview = new DevExpress.XtraEditors.SimpleButton();
            btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            lblidctp = new Label();
            lblTitulo = new Label();
            dtpFechapre = new DevExpress.XtraEditors.DateEdit();
            label1 = new Label();
            cmbidcpl = new DevExpress.XtraEditors.LookUpEdit();
            ReAlRichControl1 = new CLA.ReAlRichControl();
            ((System.ComponentModel.ISupportInitialize)dtpFechapre.Properties.CalendarTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dtpFechapre.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)cmbidcpl.Properties).BeginInit();
            SuspendLayout();
            // 
            // btnPreview
            // 
            btnPreview.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            btnPreview.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Question;
            btnPreview.Appearance.Options.UseBackColor = true;
            btnPreview.DialogResult = DialogResult.OK;
            btnPreview.Location = new Point(17, 451);
            btnPreview.Name = "btnPreview";
            btnPreview.Size = new Size(108, 34);
            btnPreview.TabIndex = 6;
            btnPreview.Text = "&Vista Previa";
            btnPreview.Visible = false;
            btnPreview.Click += btnPreview_Click;
            // 
            // btnCancelar
            // 
            btnCancelar.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            btnCancelar.DialogResult = DialogResult.OK;
            btnCancelar.Location = new Point(488, 451);
            btnCancelar.Name = "btnCancelar";
            btnCancelar.Size = new Size(108, 34);
            btnCancelar.TabIndex = 7;
            btnCancelar.Text = "&Cancelar";
            btnCancelar.Click += btnCancelar_Click;
            // 
            // btnAceptar
            // 
            btnAceptar.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            btnAceptar.Appearance.Options.UseBackColor = true;
            btnAceptar.DialogResult = DialogResult.OK;
            btnAceptar.Location = new Point(602, 451);
            btnAceptar.Name = "btnAceptar";
            btnAceptar.Size = new Size(108, 34);
            btnAceptar.TabIndex = 8;
            btnAceptar.Text = "&Registrar";
            btnAceptar.Click += btnAceptar_Click;
            // 
            // lblidctp
            // 
            lblidctp.AutoSize = true;
            lblidctp.Location = new Point(12, 68);
            lblidctp.Name = "lblidctp";
            lblidctp.Size = new Size(47, 13);
            lblidctp.TabIndex = 1;
            lblidctp.Text = "Plantilla:";
            // 
            // lblTitulo
            // 
            lblTitulo.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            lblTitulo.Font = new Font("Microsoft Sans Serif", 9.25F, FontStyle.Bold | FontStyle.Underline);
            lblTitulo.Location = new Point(12, 9);
            lblTitulo.Name = "lblTitulo";
            lblTitulo.Size = new Size(491, 53);
            lblTitulo.TabIndex = 0;
            lblTitulo.Text = "Administracion de Recetas";
            lblTitulo.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // dtpFechapre
            // 
            dtpFechapre.EditValue = null;
            dtpFechapre.Location = new Point(369, 65);
            dtpFechapre.Name = "dtpFechapre";
            dtpFechapre.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            dtpFechapre.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            dtpFechapre.Size = new Size(124, 20);
            dtpFechapre.TabIndex = 4;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(323, 68);
            label1.Name = "label1";
            label1.Size = new Size(40, 13);
            label1.TabIndex = 3;
            label1.Text = "Fecha:";
            // 
            // cmbidcpl
            // 
            cmbidcpl.Location = new Point(65, 65);
            cmbidcpl.Name = "cmbidcpl";
            cmbidcpl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            cmbidcpl.Size = new Size(204, 20);
            cmbidcpl.TabIndex = 2;
            cmbidcpl.EditValueChanged += cmbidcpl_EditValueChanged;
            // 
            // ReAlRichControl1
            // 
            ReAlRichControl1.Location = new Point(12, 91);
            ReAlRichControl1.Name = "ReAlRichControl1";
            ReAlRichControl1.Size = new Size(697, 353);
            ReAlRichControl1.TabIndex = 5;
            // 
            // FPacRecetas
            // 
            AutoScaleDimensions = new SizeF(6F, 13F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(721, 500);
            Controls.Add(ReAlRichControl1);
            Controls.Add(cmbidcpl);
            Controls.Add(label1);
            Controls.Add(dtpFechapre);
            Controls.Add(btnPreview);
            Controls.Add(btnCancelar);
            Controls.Add(btnAceptar);
            Controls.Add(lblidctp);
            Controls.Add(lblTitulo);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            IconOptions.Icon = (Icon)resources.GetObject("FPacRecetas.IconOptions.Icon");
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FPacRecetas";
            ShowInTaskbar = false;
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Recetas";
            Load += fPacRecetas_Load;
            ((System.ComponentModel.ISupportInitialize)dtpFechapre.Properties.CalendarTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)dtpFechapre.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)cmbidcpl.Properties).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton btnPreview;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        internal System.Windows.Forms.Label lblidctp;
        internal System.Windows.Forms.Label lblTitulo;
        private DevExpress.XtraEditors.DateEdit dtpFechapre;
        internal System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.LookUpEdit cmbidcpl;
        private CLA.ReAlRichControl ReAlRichControl1;
    }
}