﻿


namespace ReAl.GestionMedica.App.PAC
{
    partial class FPacCirugias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPacCirugias));
            btnPreview = new DevExpress.XtraEditors.SimpleButton();
            btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            lblidctp = new Label();
            cmbidcpl = new DevExpress.XtraEditors.LookUpEdit();
            lblTitulo = new Label();
            dtpFechapre = new DevExpress.XtraEditors.DateEdit();
            label1 = new Label();
            btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            ReAlRichControl1 = new CLA.ReAlRichControl();
            ((System.ComponentModel.ISupportInitialize)cmbidcpl.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dtpFechapre.Properties.CalendarTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dtpFechapre.Properties).BeginInit();
            SuspendLayout();
            // 
            // btnPreview
            // 
            btnPreview.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            btnPreview.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Question;
            btnPreview.Appearance.Options.UseBackColor = true;
            btnPreview.DialogResult = DialogResult.OK;
            btnPreview.Location = new Point(17, 451);
            btnPreview.Name = "btnPreview";
            btnPreview.Size = new Size(108, 34);
            btnPreview.TabIndex = 6;
            btnPreview.Text = "&Vista Previa";
            btnPreview.Visible = false;
            btnPreview.Click += btnPreview_Click;
            // 
            // btnCancelar
            // 
            btnCancelar.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            btnCancelar.DialogResult = DialogResult.OK;
            btnCancelar.Location = new Point(487, 454);
            btnCancelar.Name = "btnCancelar";
            btnCancelar.Size = new Size(108, 34);
            btnCancelar.TabIndex = 7;
            btnCancelar.Text = "&Cancelar";
            btnCancelar.Click += btnCancelar_Click;
            // 
            // lblidctp
            // 
            lblidctp.AutoSize = true;
            lblidctp.Location = new Point(14, 68);
            lblidctp.Name = "lblidctp";
            lblidctp.Size = new Size(47, 13);
            lblidctp.TabIndex = 1;
            lblidctp.Text = "Plantilla:";
            // 
            // cmbidcpl
            // 
            cmbidcpl.Location = new Point(67, 65);
            cmbidcpl.Name = "cmbidcpl";
            cmbidcpl.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            cmbidcpl.Size = new Size(204, 20);
            cmbidcpl.TabIndex = 2;
            cmbidcpl.EditValueChanged += cmbidcpl_EditValueChanged;
            // 
            // lblTitulo
            // 
            lblTitulo.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            lblTitulo.Font = new Font("Microsoft Sans Serif", 9.25F, FontStyle.Bold | FontStyle.Underline);
            lblTitulo.Location = new Point(12, 9);
            lblTitulo.Name = "lblTitulo";
            lblTitulo.Size = new Size(491, 53);
            lblTitulo.TabIndex = 0;
            lblTitulo.Text = "Administracion de Cirujías";
            lblTitulo.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // dtpFechapre
            // 
            dtpFechapre.EditValue = null;
            dtpFechapre.Location = new Point(371, 65);
            dtpFechapre.Name = "dtpFechapre";
            dtpFechapre.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            dtpFechapre.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            dtpFechapre.Size = new Size(124, 20);
            dtpFechapre.TabIndex = 4;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(325, 68);
            label1.Name = "label1";
            label1.Size = new Size(40, 13);
            label1.TabIndex = 3;
            label1.Text = "Fecha:";
            // 
            // btnAceptar
            // 
            btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            btnAceptar.Appearance.Options.UseBackColor = true;
            btnAceptar.Location = new Point(601, 454);
            btnAceptar.Name = "btnAceptar";
            btnAceptar.Size = new Size(108, 34);
            btnAceptar.TabIndex = 8;
            btnAceptar.Text = "&Registrar";
            btnAceptar.Click += btnAceptar_Click;
            // 
            // ReAlRichControl1
            // 
            ReAlRichControl1.Location = new Point(12, 91);
            ReAlRichControl1.Name = "ReAlRichControl1";
            ReAlRichControl1.Size = new Size(697, 353);
            ReAlRichControl1.TabIndex = 5;
            // 
            // FPacCirugias
            // 
            AutoScaleDimensions = new SizeF(6F, 13F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(721, 500);
            Controls.Add(ReAlRichControl1);
            Controls.Add(btnAceptar);
            Controls.Add(label1);
            Controls.Add(dtpFechapre);
            Controls.Add(btnPreview);
            Controls.Add(btnCancelar);
            Controls.Add(lblidctp);
            Controls.Add(cmbidcpl);
            Controls.Add(lblTitulo);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            IconOptions.Icon = (Icon)resources.GetObject("FPacCirugias.IconOptions.Icon");
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FPacCirugias";
            ShowInTaskbar = false;
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Cirugias";
            Load += fPacCirugias_Load;
            ((System.ComponentModel.ISupportInitialize)cmbidcpl.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)dtpFechapre.Properties.CalendarTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)dtpFechapre.Properties).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton btnPreview;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        internal System.Windows.Forms.Label lblidctp;
        private DevExpress.XtraEditors.LookUpEdit cmbidcpl;
        internal System.Windows.Forms.Label lblTitulo;
        private DevExpress.XtraEditors.DateEdit dtpFechapre;
        internal System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        private CLA.ReAlRichControl ReAlRichControl1;
    }
}