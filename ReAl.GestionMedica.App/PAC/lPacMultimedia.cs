using System.Collections;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Security.Policy;
using DevExpress.Utils;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;
using ReAl.Utils;

namespace ReAl.GestionMedica.App.PAC
{
    public partial class LPacMultimedia : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public LPacMultimedia()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        public void CargarListado()
        {
            try
            {
                var rn = new PatMultimediaController();
                var dt = rn.GetByPatientId(cParametrosApp.AppPaciente.IdPatients);
                gcPacMultimedia.DataSource = dt;

                if (dt is { Count: > 0 })
                {
                    foreach (PatMultimedia obj in dt)
                    {
                        if (obj.ExternalUrl != null)
                        {
                            obj.Preview = cFuncionesImagenes.GenerarMemoryStreamFromUrl(obj.ExternalUrl);
                        }
                    }
                    
                    gvPacMultimedia.RowHeight = 120;
                    gvPacMultimedia.Columns[PatMultimedia.Fields.UploadDate.ToString()].DisplayFormat.FormatType = FormatType.DateTime;
                    gvPacMultimedia.Columns[PatMultimedia.Fields.UploadDate.ToString()].DisplayFormat.FormatString = "dd/MM/yyyy";

                    var rep = new RepositoryItemPictureEdit();
                    rep.SizeMode = PictureSizeMode.Squeeze;
                    gvPacMultimedia.Columns[PatMultimedia.Fields.Preview.ToString()].ColumnEdit = rep;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void EliminarRegistro()
        {
            try
            {
                if (gvPacMultimedia.RowCount <= 0) return;
                if (MessageBox.Show("¿Desea ELIMINAR el registro seleccionado?", "Eliminar registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    var rn = new PatMultimediaController();
                    var obj =
                        rn.GetById(
                            int.Parse(
                                gvPacMultimedia.GetFocusedRowCellValue(PatMultimedia.Fields.IdMultimedia.ToString()).
                                    ToString()));

                    if (obj != null)
                    {
                        rn.Delete(obj.IdMultimedia);
                        CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void ModificarRegistro()
        {
            try
            {
                if (gvPacMultimedia.RowCount <= 0) return;
                var rn = new PatMultimediaController();
                var obj =
                    rn.GetById(
                        int.Parse(
                            gvPacMultimedia.GetFocusedRowCellValue(PatMultimedia.Fields.IdMultimedia.ToString()).
                                ToString()));

                if (obj != null)
                {
                    var strFile = cParametrosApp.StrTempFolder + obj.Filename;
                    WebClient client = new WebClient();
                    byte[] imageBytes = client.DownloadData(obj.ExternalUrl);
                    File.WriteAllBytes(strFile, imageBytes);
                    Process.Start("explorer.exe", strFile);
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void btnAuditoría_Click(object sender, EventArgs e)
        {
            if (gvPacMultimedia.RowCount <= 0) return;
            var rn = new PatMultimediaController();
            var obj =
                rn.GetById(
                    int.Parse(
                        gvPacMultimedia.GetFocusedRowCellValue(PatMultimedia.Fields.IdMultimedia.ToString()).
                            ToString()));

            if (obj != null)
            {
                //Abrimos la AUDITORIA
                var frm = new FAuditaria(obj.ApiUsucre, obj.ApiFeccre, obj.ApiUsumod, obj.ApiFecmod, obj.ApiStatus);
                frm.ShowDialog();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarRegistro();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            ModificarRegistro();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frm = new FPacMultimedia();
            var resultado = frm.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                this.CargarListado();
            }
        }

        private void btnVer_Click(object sender, EventArgs e)
        {
            if (gvPacMultimedia.RowCount <= 0) return;
            var rn = new PatMultimediaController();
            var obj =
                rn.GetById(
                    int.Parse(
                        gvPacMultimedia.GetFocusedRowCellValue(PatMultimedia.Fields.IdMultimedia.ToString()).
                            ToString()));

            if (obj != null)
            {
                var strFile = cParametrosApp.StrTempFolder + DateTime.Now.Ticks.ToString() + "." + obj.Extension;
                WebClient client = new WebClient();
                byte[] imageBytes = client.DownloadData(obj.ExternalUrl);
                File.WriteAllBytes(strFile, imageBytes);
                Process.Start(strFile);
            }
        }

        private void gcPacMultimedia_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                EliminarRegistro();
            }
        }

        private void gcPacMultimedia_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ModificarRegistro();
        }

        private void gvPacMultimedia_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            //gvPacMultimedia.SetFocusedRowCellValue(EntPacMultimedia.Fields.usucre.ToString(),"postgres");
        }

        private void gvPacMultimedia_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
        }

        private void lPacMultimedia_Activated(object sender, EventArgs e)
        {
            this.CargarListado();
        }

        private void lPacMultimedia_DragDrop(object sender, DragEventArgs e)
        {
            if (cParametrosApp.AppPaciente.IdPatients > 0)
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    var filePaths = (string[])(e.Data.GetData(DataFormats.FileDrop));
                    foreach (var fileLoc in filePaths)
                    {
                        using (var frm = new FPacMultimedia(fileLoc))
                        {
                            frm.ShowDialog();
                        }
                    }
                }

                this.CargarListado();
            }
        }

        private void lPacMultimedia_DragEnter(object sender, DragEventArgs e)
        {
            if (cParametrosApp.AppPaciente.IdPatients > 0)
            {
                if (e.Data.GetDataPresent(DataFormats.FileDrop))
                {
                    e.Effect = DragDropEffects.Copy;
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                }
            }
        }

        private void lPacMultimedia_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            //Habilitamos el Drag and Drop
            this.AllowDrop = true;
            this.DragEnter += lPacMultimedia_DragEnter;
            this.DragDrop += lPacMultimedia_DragDrop;

            //CargarListado();
        }

        #endregion Methods
    }
}