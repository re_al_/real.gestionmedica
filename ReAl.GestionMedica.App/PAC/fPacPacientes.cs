using System.Collections;
using System.Data;
using DevExpress.Utils;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Controller;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
using ReAl.GestionMedica.BackendConnector.Utils;
using ReAl.GestionMedica.Class;
using ReAl.Utils;

namespace ReAl.GestionMedica.App.PAC
{
    public partial class FPacPacientes : DevExpress.XtraEditors.XtraForm
    {
        #region Fields

        private readonly PatPatients _myPpa = null;

        #endregion Fields

        #region Constructors

        public FPacPacientes()
        {
            InitializeComponent();
        }

        public FPacPacientes(PatPatients obj)
        {
            InitializeComponent();

            _myPpa = obj;
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var service = new PatPatientsController();
                if (_myPpa == null)
                {
                    //Insertamos el registro
                    var obj = new PatPatients();
                    obj.FirstName = txtnombresppa.Text;
                    obj.LastName = txtappaternoppa.Text;
                    obj.Gender = (cmbGenero.EditValue.ToString() == "1");
                    obj.Birthdate = dtpfechanac.DateTime;
                    obj.IdBloodTypes = int.Parse(cmbidcts.EditValue.ToString());
                    obj.IdMarital = int.Parse(cmbidcec.EditValue.ToString());
                    obj.Occupation = txtocupacionppa.Text;
                    obj.Origin = txtprocedenciappa.Text;
                    obj.Address = txtdomicilioppa.Text;
                    obj.Dominance = txtdominanciappa.Text;
                    obj.Phone = txttelefonosppa.Text;
                    obj.Email = txtemailppa.Text;
                    obj.Religion = txtreligionppa.Text;
                    obj.Company = txtempresappa.Text;
                    obj.Insurance = txtseguroppa.Text;
                    obj.Refers = txtremiteppa.Text;
                    obj.Informant = txtinformanteppa.Text;
                    obj.IdentityNumber = txtcippa.Text.Trim();
                    obj.ApiUsucre = cParametrosApp.AppRestUser.Login;
                    bProcede = service.Create(obj);
                }
                else
                {
                    _myPpa.FirstName = txtnombresppa.Text;
                    _myPpa.LastName = txtappaternoppa.Text;
                    _myPpa.Gender = (cmbGenero.EditValue.ToString() == "1");
                    _myPpa.Birthdate = dtpfechanac.DateTime;
                    _myPpa.IdBloodTypes = int.Parse(cmbidcts.EditValue.ToString());
                    _myPpa.IdMarital = int.Parse(cmbidcec.EditValue.ToString());
                    _myPpa.Occupation = txtocupacionppa.Text;
                    _myPpa.Origin = txtprocedenciappa.Text;
                    _myPpa.Address = txtdomicilioppa.Text;
                    _myPpa.Dominance = txtdominanciappa.Text;
                    _myPpa.Phone = txttelefonosppa.Text;
                    _myPpa.Email = txtemailppa.Text;
                    _myPpa.Religion = txtreligionppa.Text;
                    _myPpa.Company = txtempresappa.Text;
                    _myPpa.Insurance = txtseguroppa.Text;
                    _myPpa.Refers = txtremiteppa.Text;
                    _myPpa.Informant = txtinformanteppa.Text;
                    _myPpa.IdentityNumber = txtcippa.Text.Trim();
                    _myPpa.ApiUsumod = cParametrosApp.AppRestUser.Login;
                    _myPpa.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                    bProcede = service.Update(_myPpa);
                }
            }
            catch (Exception exp)
            {
                bProcede = false;
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }

            if (bProcede)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void CargarCmbGenero()
        {
            try
            {
                var dtGender = new DataTable();
                var dcId = new DataColumn("id", typeof(Boolean));
                dtGender.Columns.Add(dcId);
                var dcGender = new DataColumn("gender", typeof(String));
                dtGender.Columns.Add(dcGender);

                var dr = dtGender.NewRow();
                dr["id"] = true;
                dr["gender"] = "Masculino";
                dtGender.Rows.Add(dr);
                dr = dtGender.NewRow();
                dr["id"] = false;
                dr["gender"] = "Femenino";
                dtGender.Rows.Add(dr);

                cmbGenero.Properties.DataSource = dtGender;
                if (dtGender.Rows.Count > 0)
                {
                    cmbGenero.Properties.ValueMember = "id";
                    cmbGenero.Properties.DisplayMember = "gender";
                    cmbGenero.Properties.PopulateColumns();
                    cmbGenero.Properties.ShowHeader = false;
                    cmbGenero.Properties.Columns["id"].Visible = false;

                    cmbGenero.ItemIndex = 0;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void CargarCmbidcec()
        {
            try
            {
                var service = new ClaMaritalController();
                var list = service.GetActive();
                if (list != null)
                {
                    cmbidcec.Properties.DataSource = list;
                    if (list.Count > 0)
                    {
                        cmbidcec.Properties.ValueMember = ClaMarital.Fields.IdMarital.ToString();
                        cmbidcec.Properties.DisplayMember = ClaMarital.Fields.Description.ToString();
                        cmbidcec.Properties.PopulateColumns();
                        cmbidcec.Properties.ShowHeader = false;
                        cmbidcec.Properties.Columns[ClaMarital.Fields.IdMarital.ToString()].Visible = false;
                        cmbidcec.ItemIndex = 0;
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void CargarCmbidcts()
        {
            try
            {
                var service = new ClaBloodtypesController();
                var list = service.GetActive();
                if (list != null)
                {
                    cmbidcts.Properties.DataSource = list;
                    if (list.Count > 0)
                    {
                        cmbidcts.Properties.ValueMember = ClaBloodtypes.Fields.IdBloodTypes.ToString();
                        cmbidcts.Properties.DisplayMember = ClaBloodtypes.Fields.Description.ToString();
                        cmbidcts.Properties.PopulateColumns();
                        cmbidcts.Properties.ShowHeader = false;
                        cmbidcts.Properties.Columns[ClaBloodtypes.Fields.IdBloodTypes.ToString()].Visible = false;

                        cmbidcts.ItemIndex = 0;
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void CargarDatos()
        {
            try
            {
                if (_myPpa != null)
                {
                    txtcippa.Text = _myPpa.IdentityNumber;
                    txtnombresppa.Text = _myPpa.FirstName;
                    txtappaternoppa.Text = _myPpa.LastName;
                    cmbGenero.EditValue = _myPpa.Gender ? 1 : 0;
                    dtpfechanac.EditValue = _myPpa.Birthdate;
                    cmbidcts.EditValue = _myPpa.IdBloodTypes;
                    cmbidcec.EditValue = _myPpa.IdMarital;
                    txtocupacionppa.Text = _myPpa.Occupation;
                    txtprocedenciappa.Text = _myPpa.Origin;
                    txtdomicilioppa.Text = _myPpa.Address;
                    txtdominanciappa.Text = _myPpa.Dominance;
                    txttelefonosppa.Text = _myPpa.Phone;
                    txtemailppa.Text = _myPpa.Email;
                    txtreligionppa.Text = _myPpa.Religion;
                    txtempresappa.Text = _myPpa.Company;
                    txtseguroppa.Text = _myPpa.Insurance;
                    txtremiteppa.Text = _myPpa.Refers;
                    txtinformanteppa.Text = _myPpa.Informant;
                    CargarHistorialAgenda();
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void CargarEdad()
        {
            try
            {
                var fecNac = dtpfechanac.DateTime;
                var fecHoy = DateTime.Today;
                txtEdad.Text = ((int.Parse(fecHoy.ToString("yyyyMMdd")) - int.Parse(fecNac.ToString("yyyyMMdd"))) / 10000).ToString();
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void CargarHistorialAgenda()
        {
            try
            {
                var service = new PatAppointmentsController();
                var list = service.GetAppointmentsByPatientId(_myPpa.IdPatients);
                if (list != null)
                {
                    this.gcPendientes.DataSource = list;
                    if (list.Count > 0)
                    {
                        this.gvPendientes.PopulateColumns();
                        this.gvPendientes.OptionsBehavior.Editable = false;
                        this.gvPendientes.Columns[PatAppointments.Fields.TimeStart.ToString()].DisplayFormat.FormatType = FormatType.DateTime;
                        this.gvPendientes.Columns[PatAppointments.Fields.TimeStart.ToString()].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
                        this.gvPendientes.Columns[PatAppointments.Fields.TimeEnd.ToString()].DisplayFormat.FormatType = FormatType.DateTime;
                        this.gvPendientes.Columns[PatAppointments.Fields.TimeEnd.ToString()].DisplayFormat.FormatString = "dd/MM/yyyy HH:mm";
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void CargarNombreTitulo()
        {
            try
            {
                lblTitulo.Text = cFuncionesStrings.TitleCase(txtnombresppa.Text) + Environment.NewLine
                                 + cFuncionesStrings.TitleCase(txtappaternoppa.Text) + Environment.NewLine
                                 + cFuncionesStrings.TitleCase(txtapmaternoppa.Text);
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void dtpfechanac_EditValueChanged(object sender, EventArgs e)
        {
            CargarEdad();
        }

        private void fPacPacientes_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            CargarCmbGenero();
            CargarCmbidcts();
            CargarCmbidcec();

            cmbGenero.ItemIndex = 0;

            if (_myPpa != null)
            {
                CargarDatos();
                CargarNombreTitulo();
                CargarEdad();
            }
        }

        private void txtapmaternoppa_EditValueChanged(object sender, EventArgs e)
        {
            CargarNombreTitulo();
        }

        private void txtappaternoppa_EditValueChanged(object sender, EventArgs e)
        {
            CargarNombreTitulo();
        }

        private void txtnombresppa_EditValueChanged(object sender, EventArgs e)
        {
            CargarNombreTitulo();
        }

        #endregion Methods
    }
}