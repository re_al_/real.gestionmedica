﻿using System.Text;
using DevExpress.XtraRichEdit;
using ReAl.GestionMedica.App.RPT;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;
using ReAl.Utils;

namespace ReAl.GestionMedica.App.PAC
{
    public partial class FPacInformes : DevExpress.XtraEditors.XtraForm
    {
        #region Fields

        private PatReports _myObj = null;

        #endregion Fields

        #region Constructors

        public FPacInformes()
        {
            InitializeComponent();
        }

        public FPacInformes(PatReports obj)
        {
            InitializeComponent();

            _myObj = obj;
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            if (this.GuardarRegistro(true))
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (this.GuardarRegistro(false))
            {
                //Mostramos la Plantilla
                var frm = new RptViewerPac(_myObj);
                frm.ShowDialog();
            }
        }

        private void CargarCmbidcpl()
        {
            try
            {
                var rn = new ClaTemplatesController();
                var dt = rn.GetByTemplateTypeId(3);
                cmbidcpl.Properties.DataSource = dt;
                if (dt.Count > 0)
                {
                    cmbidcpl.Properties.ValueMember = ClaTemplates.Fields.IdTemplates.ToString();
                    cmbidcpl.Properties.DisplayMember = ClaTemplates.Fields.Name.ToString();
                    cmbidcpl.Properties.PopulateColumns();
                    cmbidcpl.Properties.ShowHeader = false;
                    cmbidcpl.Properties.Columns[ClaTemplates.Fields.IdTemplates.ToString()].Visible = false;
                    cmbidcpl.Properties.Columns[ClaTemplates.Fields.IdTemplatesType.ToString()].Visible = false;
                    cmbidcpl.Properties.Columns[ClaTemplates.Fields.IdWorkplaces.ToString()].Visible = false;
                    cmbidcpl.Properties.Columns[ClaTemplates.Fields.Description.ToString()].Visible = false;
                    cmbidcpl.Properties.Columns[ClaTemplates.Fields.TextFormat.ToString()].Visible = false;
                    cmbidcpl.Properties.Columns[ClaTemplates.Fields.RtfFormat.ToString()].Visible = false;
                    cmbidcpl.Properties.Columns[ClaTemplates.Fields.HtmlFormat.ToString()].Visible = false;
                    cmbidcpl.Properties.Columns[ClaTemplates.Fields.MarginBottom.ToString()].Visible = false;
                    cmbidcpl.Properties.Columns[ClaTemplates.Fields.MarginLeft.ToString()].Visible = false;
                    cmbidcpl.Properties.Columns[ClaTemplates.Fields.MarginRight.ToString()].Visible = false;
                    cmbidcpl.Properties.Columns[ClaTemplates.Fields.MarginTop.ToString()].Visible = false;
                    cmbidcpl.Properties.Columns[ClaTemplates.Fields.Height.ToString()].Visible = false;
                    cmbidcpl.Properties.Columns[ClaTemplates.Fields.Width.ToString()].Visible = false;
                    cmbidcpl.Properties.ForceInitialize();

                    cmbidcpl.ItemIndex = 0;

                    //Obtenemos la plantilla
                    var rnCpl = new ClaTemplatesController();
                    var objCpl = rnCpl.GetById(int.Parse(cmbidcpl.EditValue.ToString()));

                    if (objCpl != null)
                    {
                        if (objCpl.HtmlFormat != null)
                        {
                            ReAlRichControl1.richEditControl1.Document.HtmlText = objCpl.HtmlFormat.Replace("%PACIENTE%", (cParametrosApp.AppPaciente.Gender ? "Sr. " : "Sra. ") + cParametrosApp.AppPaciente.LastName);
                            ReAlRichControl1.richEditControl1.ActiveViewType = RichEditViewType.Simple;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        private void CargarDatos()
        {
            try
            {
                if (_myObj != null)
                {
                    cmbidcpl.EditValue = _myObj.IdTemplates;
                    //dtpFechapre.EditValue = _myObj.fechapin;
                    dtpFechapre.EditValue = DateTime.Now;

                    var enc = new ASCIIEncoding();
                    //txtcuerpocpl.Rtf = enc.GetString(_myObj.RtfFormat, 0, _myObj.RtfFormat.Length);
                    ReAlRichControl1.richEditControl1.Document.HtmlText = _myObj.HtmlFormat;
                    ReAlRichControl1.richEditControl1.ActiveViewType = RichEditViewType.Simple;

                    btnPreview.Visible = true;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void cmbidcpl_EditValueChanged(object sender, EventArgs e)
        {
            try
            {
                //Obtenemos la plantilla
                var rn = new ClaTemplatesController();
                var obj = rn.GetById(int.Parse(cmbidcpl.EditValue.ToString()));

                if (obj != null)
                {
                    if (obj.RtfFormat != null)
                    {
                        if (obj.HtmlFormat != null)
                        {
                            ReAlRichControl1.richEditControl1.Document.HtmlText = obj.HtmlFormat.Replace("%PACIENTE%", (cParametrosApp.AppPaciente.Gender ? "Sr. " : "Sra. ") + cParametrosApp.AppPaciente.LastName);
                            ReAlRichControl1.richEditControl1.ActiveViewType = RichEditViewType.Simple;
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void fPacInformes_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            CargarCmbidcpl();
            dtpFechapre.DateTime = DateTime.Now;

            if (_myObj != null)
            {
                CargarDatos();
            }
        }

        private bool GuardarRegistro(bool bPregunta)
        {
            var bProcede = false;
            try
            {
                var rn = new PatReportsController();

                if (_myObj == null)
                {
                    ReAlRichControl1.richEditControl1.SaveDocument("temp.rtf", DocumentFormat.Rtf);
                    var obj = new PatReports();
                    //Insertamos el registro
                    obj.IdPatients = cParametrosApp.AppPaciente.IdPatients;
                    obj.IdTemplates = int.Parse(cmbidcpl.EditValue.ToString());
                    obj.ReportDate = dtpFechapre.DateTime;
                    obj.TextFormat = ReAlRichControl1.richEditControl1.Document.Text;
                    obj.RtfFormat = Convert.ToBase64String(cFuncionesImagenes.ImageReadBinaryFile("temp.rtf"));
                    obj.HtmlFormat = ReAlRichControl1.richEditControl1.Document.HtmlText;
                    rn.Create(obj);
                    _myObj = obj;
                    bProcede = true;
                }
                else
                {
                    //Actualizamos
                    ReAlRichControl1.richEditControl1.SaveDocument("temp.rtf", DocumentFormat.Rtf);
                    _myObj.IdTemplates = int.Parse(cmbidcpl.EditValue.ToString());
                    _myObj.ReportDate = dtpFechapre.DateTime;
                    _myObj.TextFormat = ReAlRichControl1.richEditControl1.Document.Text;
                    _myObj.RtfFormat = Convert.ToBase64String(cFuncionesImagenes.ImageReadBinaryFile("temp.rtf"));
                    _myObj.HtmlFormat = ReAlRichControl1.richEditControl1.Document.HtmlText;
                    _myObj.ApiTransaction = CApi.Transaccion.UPDATE.ToString();
                    rn.Update(_myObj);
                    bProcede = true;
                }
            }
            catch (Exception exp)
            {
                bProcede = false;
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }

            if (bPregunta)
            {
                var res = MessageBox.Show(
                    "¿Desea ver una vista previa de su Informe Médico?",
                    "Informe Médico",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button1);

                if (res == DialogResult.Yes)
                {
                    var frm = new RptViewerPac(_myObj);
                    frm.ShowDialog();
                }
            }

            return bProcede;
        }

        #endregion Methods

        private void dtpFechapre_EditValueChanged(object sender, EventArgs e)
        {

        }
    }
}