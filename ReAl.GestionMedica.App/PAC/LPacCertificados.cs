using System.Collections;
using System.Data;
using System.Text;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraRichEdit;
using ReAl.GestionMedica.App.RPT;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;

namespace ReAl.GestionMedica.App.PAC
{
    public partial class LPacCertificados : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public LPacCertificados()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        public void CargarListado()
        {
            var rn = new PatCertificationsController();
            var dt = rn.GetByPatientId(cParametrosApp.AppPaciente.IdPatients);
            gcPacConsultas.DataSource = dt;
            if (dt is { Count: > 0 })
            {
                gvPacConsultas.RowHeight = 150;
                gvPacConsultas.PopulateColumns();
                gvPacConsultas.OptionsBehavior.Editable = false;

                var repRichText = new RepositoryItemRichTextEdit() { DocumentFormat = DocumentFormat.Html };
                repRichText.OptionsBehavior.ShowPopupMenu = DocumentCapability.Hidden;
                repRichText.MaxHeight = 700;
                repRichText.OptionsHorizontalScrollbar.Visibility = RichEditScrollbarVisibility.Auto;
                gcPacConsultas.RepositoryItems.Add(repRichText);
                gvPacConsultas.Columns[PatCertifications.Fields.HtmlFormat.ToString()].ColumnEdit = repRichText;

                gvPacConsultas.Columns[PatCertifications.Fields.CertificationDate.ToString()].DisplayFormat.FormatType = FormatType.DateTime;
                gvPacConsultas.Columns[PatCertifications.Fields.CertificationDate.ToString()].DisplayFormat.FormatString = "dd/MM/yyyy";
            }
        }

        public void EliminarRegistro()
        {
            try
            {
                if (gvPacConsultas.RowCount <= 0) return;
                if (MessageBox.Show("¿Desea ELIMINAR el registro seleccionado?", "Eliminar registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    //Eliminamos el registro seleccionado
                    var rn = new PatCertificationsController();

                    //Apropiamos los valores del Grid
                    Int64 intidpco = int.Parse(gvPacConsultas.GetFocusedRowCellValue(PatCertifications.Fields.IdCertifications.ToString()).ToString());

                    //Obtenemos el objeto
                    var obj = rn.GetById(intidpco);

                    if (obj != null)
                    {
                        rn.Delete(obj.IdCertifications);
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void ModificarRegistro()
        {
            try
            {
                if (gvPacConsultas.RowCount <= 0) return;
                var rn = new PatCertificationsController();

                //Apropiamos los valores del Grid
                Int64 intidpco = int.Parse(gvPacConsultas.GetFocusedRowCellValue(PatCertifications.Fields.IdCertifications.ToString()).ToString());

                //Obtenemos el objeto
                var obj = rn.GetById(intidpco);

                if (obj != null)
                {
                    var frm = new FPacCertificados(obj);
                    var resultado = frm.ShowDialog();
                    if (resultado == DialogResult.OK)
                    {
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void btnAuditoría_Click(object sender, EventArgs e)
        {
            if (gvPacConsultas.RowCount <= 0) return;
            var rn = new PatCertificationsController();

            //Apropiamos los valores del Grid
            Int64 intidpco = int.Parse(gvPacConsultas.GetFocusedRowCellValue(PatCertifications.Fields.IdCertifications.ToString()).ToString());

            //Obtenemos el objeto
            var obj = rn.GetById(intidpco);

            if (obj != null)
            {
                //Abrimos la AUDITORIA
                var frm = new FAuditaria(obj.ApiUsucre, obj.ApiFeccre, obj.ApiUsumod, obj.ApiFecmod, obj.ApiStatus);
                frm.ShowDialog();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarRegistro();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            ModificarRegistro();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frm = new FPacCertificados();
            var resultado = frm.ShowDialog();
            if (resultado == DialogResult.OK)
            {
                this.CargarListado();
            }
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (gvPacConsultas.RowCount <= 0) return;
            var rn = new PatCertificationsController();

            //Apropiamos los valores del Grid
            Int64 intidpco = int.Parse(gvPacConsultas.GetFocusedRowCellValue(PatCertifications.Fields.IdCertifications.ToString()).ToString());

            //Obtenemos el objeto
            var obj = rn.GetById(intidpco);

            if (obj != null)
            {
                var frm = new RptViewerPac(obj);
                frm.ShowDialog();
            }
        }

        private void gcPacConsultas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                EliminarRegistro();
            }
        }

        private void gcPacConsultas_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ModificarRegistro();
        }

        private void gvPacConsultas_InitNewRow(object sender, DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs e)
        {
            //gvPacConsultas.SetFocusedRowCellValue(EntPacCertificados.Fields.usucre.ToString(),"postgres");
        }

        private void gvPacConsultas_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
        }

        private void LPacCertificados_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            //CargarListado();
        }

        #endregion Methods
    }
}