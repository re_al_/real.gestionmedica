using System.Collections;
using System.Data;
using System.Text;
using DevExpress.Utils;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraRichEdit;
using ReAl.GestionMedica.App.RPT;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;

namespace ReAl.GestionMedica.App.PAC
{
    public partial class LPacRecetas : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public LPacRecetas()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        public void CargarListado()
        {
            try
            {
                var rn = new PatPrescriptionsController();
                var dt = rn.GetByPatientId(cParametrosApp.AppPaciente.IdPatients);
                gcClaPlantillas.DataSource = dt;
                if (dt is { Count: > 0 })
                {
                    gvClaPlantillas.RowHeight = 120;
                    gvClaPlantillas.PopulateColumns();
                    gvClaPlantillas.OptionsBehavior.Editable = false;

                    var repRichText = new RepositoryItemRichTextEdit() { DocumentFormat = DocumentFormat.Html };
                    repRichText.OptionsBehavior.ShowPopupMenu = DocumentCapability.Hidden;
                    repRichText.MaxHeight = 700;
                    repRichText.OptionsHorizontalScrollbar.Visibility = RichEditScrollbarVisibility.Auto;
                    gcClaPlantillas.RepositoryItems.Add(repRichText);
                    gvClaPlantillas.Columns[PatPrescriptions.Fields.HtmlFormat.ToString()].ColumnEdit = repRichText;
                    //gvClaPlantillas.Columns["Documento"].Visible = false;
                    gvClaPlantillas.Columns[PatPrescriptions.Fields.PrescriptionDate.ToString()].DisplayFormat.FormatType = FormatType.DateTime;
                    gvClaPlantillas.Columns[PatPrescriptions.Fields.PrescriptionDate.ToString()].DisplayFormat.FormatString = "dd/MM/yyyy";
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void EliminarRegistro()
        {
            try
            {
                if (gvClaPlantillas.RowCount <= 0) return;
                if (MessageBox.Show("¿Desea ELIMINAR el registro seleccionado?", "Eliminar registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    //Eliminamos el registro seleccionado
                    var rn = new PatPrescriptionsController();

                    //Apropiamos los valores del Grid
                    Int64 intidcpl = int.Parse(gvClaPlantillas.GetFocusedRowCellValue(PatPrescriptions.Fields.IdPrescriptions.ToString()).ToString());

                    //Obtenemos el objeto
                    var obj = rn.GetById(intidcpl);

                    if (obj != null)
                    {
                        rn.Delete(obj.IdPrescriptions);
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void ModificarRegistro()
        {
            try
            {
                if (gvClaPlantillas.RowCount <= 0) return;
                var rn = new PatPrescriptionsController();

                //Apropiamos los valores del Grid
                Int64 intidcpl = int.Parse(gvClaPlantillas.GetFocusedRowCellValue(PatPrescriptions.Fields.IdPrescriptions.ToString()).ToString());

                //Obtenemos el objeto
                var obj = rn.GetById(intidcpl);

                if (obj != null)
                {
                    var frm = new FPacRecetas(obj);
                    var resultado = frm.ShowDialog();
                    if (resultado == DialogResult.OK)
                    {
                        this.CargarListado();
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void btnAuditoría_Click(object sender, EventArgs e)
        {
            if (gvClaPlantillas.RowCount <= 0) return;
            var rn = new PatPrescriptionsController();

            //Apropiamos los valores del Grid
            Int64 intidcpl = int.Parse(gvClaPlantillas.GetFocusedRowCellValue(PatPrescriptions.Fields.IdPrescriptions.ToString()).ToString());

            //Obtenemos el objeto
            var obj = rn.GetById(intidcpl);

            if (obj != null)
            {
                //Abrimos la AUDITORIA
                var frm = new FAuditaria(obj.ApiUsucre, obj.ApiFeccre, obj.ApiUsumod, obj.ApiFecmod, obj.ApiStatus);
                frm.ShowDialog();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarRegistro();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            ModificarRegistro();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frm = new FPacRecetas();
            var resultado = frm.ShowDialog();
            if (resultado == DialogResult.OK)
            {
                this.CargarListado();
            }
        }

        private void btnPreview_Click(object sender, EventArgs e)
        {
            if (gvClaPlantillas.RowCount <= 0) return;
            var rn = new PatPrescriptionsController();

            //Apropiamos los valores del Grid
            Int64 intidcpl = int.Parse(gvClaPlantillas.GetFocusedRowCellValue(PatPrescriptions.Fields.IdPrescriptions.ToString()).ToString());

            //Obtenemos el objeto
            var obj = rn.GetById(intidcpl);

            if (obj != null)
            {
                var frm = new RptViewerPac(obj);
                frm.ShowDialog();
            }
        }

        private void gcClaPlantillas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)
            {
                EliminarRegistro();
            }
        }

        private void gcClaPlantillas_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ModificarRegistro();
        }

        private void lClaPlantillas_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            //CargarListado();
        }

        #endregion Methods
    }
}