﻿


namespace ReAl.GestionMedica.App.PAC
{
    partial class FPacCertificados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPacCertificados));
            btnPreview = new DevExpress.XtraEditors.SimpleButton();
            btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            lblmatricula = new Label();
            lblTitulo = new Label();
            dtpFechapcm = new DevExpress.XtraEditors.DateEdit();
            label1 = new Label();
            btnAceptar = new DevExpress.XtraEditors.SimpleButton();
            txtmatriculapcm = new DevExpress.XtraEditors.TextEdit();
            ReAlRichControl1 = new CLA.ReAlRichControl();
            ((System.ComponentModel.ISupportInitialize)dtpFechapcm.Properties.CalendarTimeProperties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)dtpFechapcm.Properties).BeginInit();
            ((System.ComponentModel.ISupportInitialize)txtmatriculapcm.Properties).BeginInit();
            SuspendLayout();
            // 
            // btnPreview
            // 
            btnPreview.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            btnPreview.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Question;
            btnPreview.Appearance.Options.UseBackColor = true;
            btnPreview.DialogResult = DialogResult.OK;
            btnPreview.Location = new Point(12, 450);
            btnPreview.Name = "btnPreview";
            btnPreview.Size = new Size(108, 34);
            btnPreview.TabIndex = 6;
            btnPreview.Text = "&Vista Previa";
            btnPreview.Visible = false;
            btnPreview.Click += btnPreview_Click;
            // 
            // btnCancelar
            // 
            btnCancelar.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            btnCancelar.DialogResult = DialogResult.OK;
            btnCancelar.Location = new Point(487, 450);
            btnCancelar.Name = "btnCancelar";
            btnCancelar.Size = new Size(108, 34);
            btnCancelar.TabIndex = 7;
            btnCancelar.Text = "&Cancelar";
            btnCancelar.Click += btnCancelar_Click;
            // 
            // lblmatricula
            // 
            lblmatricula.AutoSize = true;
            lblmatricula.Location = new Point(17, 68);
            lblmatricula.Name = "lblmatricula";
            lblmatricula.Size = new Size(47, 13);
            lblmatricula.TabIndex = 1;
            lblmatricula.Text = "Plantilla:";
            // 
            // lblTitulo
            // 
            lblTitulo.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            lblTitulo.Font = new Font("Microsoft Sans Serif", 9.25F, FontStyle.Bold | FontStyle.Underline);
            lblTitulo.Location = new Point(12, 9);
            lblTitulo.Name = "lblTitulo";
            lblTitulo.Size = new Size(491, 53);
            lblTitulo.TabIndex = 0;
            lblTitulo.Text = "Administracion de Certificados Médicos";
            lblTitulo.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // dtpFechapcm
            // 
            dtpFechapcm.EditValue = null;
            dtpFechapcm.Location = new Point(422, 65);
            dtpFechapcm.Name = "dtpFechapcm";
            dtpFechapcm.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo) });
            dtpFechapcm.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] { new DevExpress.XtraEditors.Controls.EditorButton() });
            dtpFechapcm.Size = new Size(124, 20);
            dtpFechapcm.TabIndex = 4;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(376, 68);
            label1.Name = "label1";
            label1.Size = new Size(40, 13);
            label1.TabIndex = 3;
            label1.Text = "Fecha:";
            // 
            // btnAceptar
            // 
            btnAceptar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            btnAceptar.Appearance.Options.UseBackColor = true;
            btnAceptar.Location = new Point(601, 450);
            btnAceptar.Name = "btnAceptar";
            btnAceptar.Size = new Size(108, 33);
            btnAceptar.TabIndex = 8;
            btnAceptar.Text = "&Registrar";
            btnAceptar.Click += btnAceptar_Click;
            // 
            // txtmatriculapcm
            // 
            txtmatriculapcm.EditValue = "D-125";
            txtmatriculapcm.Location = new Point(70, 65);
            txtmatriculapcm.Name = "txtmatriculapcm";
            txtmatriculapcm.Size = new Size(271, 20);
            txtmatriculapcm.TabIndex = 2;
            // 
            // ReAlRichControl1
            // 
            ReAlRichControl1.Location = new Point(12, 91);
            ReAlRichControl1.Name = "ReAlRichControl1";
            ReAlRichControl1.Size = new Size(698, 353);
            ReAlRichControl1.TabIndex = 5;
            // 
            // FPacCertificados
            // 
            AutoScaleDimensions = new SizeF(6F, 13F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(721, 495);
            Controls.Add(ReAlRichControl1);
            Controls.Add(btnAceptar);
            Controls.Add(label1);
            Controls.Add(dtpFechapcm);
            Controls.Add(btnPreview);
            Controls.Add(btnCancelar);
            Controls.Add(lblmatricula);
            Controls.Add(lblTitulo);
            Controls.Add(txtmatriculapcm);
            FormBorderStyle = FormBorderStyle.FixedDialog;
            IconOptions.Icon = (Icon)resources.GetObject("FPacCertificados.IconOptions.Icon");
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FPacCertificados";
            ShowInTaskbar = false;
            StartPosition = FormStartPosition.CenterScreen;
            Text = "Certificados Médicos";
            Load += fPacCertificados_Load;
            ((System.ComponentModel.ISupportInitialize)dtpFechapcm.Properties.CalendarTimeProperties).EndInit();
            ((System.ComponentModel.ISupportInitialize)dtpFechapcm.Properties).EndInit();
            ((System.ComponentModel.ISupportInitialize)txtmatriculapcm.Properties).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton btnPreview;
        private DevExpress.XtraEditors.SimpleButton btnCancelar;
        internal System.Windows.Forms.Label lblmatricula;
        internal System.Windows.Forms.Label lblTitulo;
        private DevExpress.XtraEditors.DateEdit dtpFechapcm;
        internal System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton btnAceptar;
        private DevExpress.XtraEditors.TextEdit txtmatriculapcm;
        private CLA.ReAlRichControl ReAlRichControl1;
    }
}