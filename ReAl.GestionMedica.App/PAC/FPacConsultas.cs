﻿using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class;

namespace ReAl.GestionMedica.App.PAC
{
    public partial class FPacConsultas : DevExpress.XtraEditors.XtraForm
    {
        #region Fields

        private readonly PatConsultations _myPco = null;

        #endregion Fields

        #region Constructors

        public FPacConsultas()
        {
            InitializeComponent();
        }

        public FPacConsultas(PatConsultations obj)
        {
            InitializeComponent();

            _myPco = obj;
        }

        #endregion Constructors

        #region Methods

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            var bProcede = false;
            var bEsInsercion = false;
            var rn = new PatConsultationsController();

            try
            {
                if (_myPco == null)
                {
                    bEsInsercion = true;
                    //Insertamos el registro
                    var obj = new PatConsultations();
                    obj.IdPatients = cParametrosApp.AppPaciente.IdPatients;
                    obj.ConsultationDate = dtpfechapco.DateTime.Date;
                    obj.ConsultationTime = dtphorapco.DateTime;
                    obj.ClinicalProfile = txtcuadroclinicopco.Text;
                    obj.Diagnosis = txtdiagnosticopco.Text;
                    obj.IdCie = int.Parse(cmbidcie.EditValue.ToString());
                    obj.Behaviour = txtconductapco.Text;
                    obj.Treatment = txttratamientopco.Text;
                    rn.Create(obj);
                    bProcede = true;
                }
                else
                {
                    _myPco.ConsultationDate = dtpfechapco.DateTime.Date;
                    _myPco.ConsultationTime = dtphorapco.DateTime;
                    _myPco.ClinicalProfile = txtcuadroclinicopco.Text;
                    _myPco.Diagnosis = txtdiagnosticopco.Text;
                    _myPco.IdCie = int.Parse(cmbidcie.EditValue.ToString());
                    _myPco.Behaviour = txtconductapco.Text;
                    _myPco.Treatment = txttratamientopco.Text;
                    _myPco.ApiTransaction = CApi.Transaccion.UPDATE.ToString();

                    rn.Update(_myPco);
                    bProcede = true;
                }
            }
            catch (Exception exp)
            {
                bProcede = false;
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }

            if (bProcede)
            {
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void CargarCmbCie()
        {
            var rn = new ClaCieController();
            var dt = rn.GetActive();

            cmbidcie.Properties.DataSource = dt;
            if (dt.Count > 0)
            {
                cmbidcie.Properties.ValueMember = ClaCie.Fields.IdCie.ToString();
                cmbidcie.Properties.DisplayMember = ClaCie.Fields.Description.ToString();
                cmbidcie.Properties.PopulateColumns();
                cmbidcie.Properties.ShowHeader = false;
                //cmbidcie.Properties.Columns[ClaTemplates.Fields.usucre.ToString()].Visible = false;
                //cmbidcie.Properties.Columns[ClaTemplates.Fields.feccre.ToString()].Visible = false;
                //cmbidcie.Properties.Columns[ClaTemplates.Fields.usumod.ToString()].Visible = false;
                //cmbidcie.Properties.Columns[ClaTemplates.Fields.fecmod.ToString()].Visible = false;
                //cmbidcie.Properties.Columns[ClaTemplates.Fields.apiestado.ToString()].Visible = false;
                //cmbidcie.Properties.Columns[ClaTemplates.Fields.apitransaccion.ToString()].Visible = false;
                cmbidcie.Properties.ForceInitialize();

                cmbidcie.EditValue = "-N/A-";
            }
        }

        private void CargarDatos()
        {
            try
            {
                if (_myPco != null)
                {
                    dtpfechapco.EditValue = _myPco.ConsultationDate.Date;
                    dtphorapco.EditValue = _myPco.ConsultationTime;
                    txtcuadroclinicopco.Text = _myPco.ClinicalProfile;
                    txtdiagnosticopco.Text = _myPco.Diagnosis;
                    cmbidcie.EditValue = _myPco.IdCie;
                    txtconductapco.Text = _myPco.Behaviour;
                    txttratamientopco.Text = _myPco.Treatment;
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void cmbidcie_EditValueChanged(object sender, EventArgs e)
        {
            if (cmbidcie.IsDisplayTextValid)
            {
                if (txtdiagnosticopco.Text == "")
                {
                    txtdiagnosticopco.Text = cmbidcie.Text + " -- ";
                }
                else
                {
                    var intReplace = txtdiagnosticopco.Text.IndexOf("--");
                    if (intReplace > 0)
                    {
                        var strCadena = txtdiagnosticopco.Text.Substring(0, intReplace);
                        txtdiagnosticopco.Text = txtdiagnosticopco.Text.Replace(strCadena, cmbidcie.Text + " ");
                    }
                }
            }
        }

        private void fPacConsultas_Load(object sender, EventArgs e)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

            CargarCmbCie();

            dtpfechapco.EditValue = DateTime.Now;
            dtphorapco.EditValue = DateTime.Now;

            if (_myPco != null)
            {
                CargarDatos();
            }
        }

        #endregion Methods
    }
}