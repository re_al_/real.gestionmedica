﻿


namespace ReAl.GestionMedica.App.PAC
{
	partial class FPacConsultas
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FPacConsultas));
            this.lblTitulo = new System.Windows.Forms.Label();
            this.lblfechapco = new System.Windows.Forms.Label();
            this.dtpfechapco = new DevExpress.XtraEditors.DateEdit();
            this.lblhorapco = new System.Windows.Forms.Label();
            this.dtphorapco = new DevExpress.XtraEditors.DateEdit();
            this.lblcuadroclinicopco = new System.Windows.Forms.Label();
            this.lbldiagnosticopco = new System.Windows.Forms.Label();
            this.lblidcie = new System.Windows.Forms.Label();
            this.lblconductapco = new System.Windows.Forms.Label();
            this.lbltratamientopco = new System.Windows.Forms.Label();
            this.btnCancelar = new DevExpress.XtraEditors.SimpleButton();
            this.txtcuadroclinicopco = new DevExpress.XtraEditors.MemoEdit();
            this.txtdiagnosticopco = new DevExpress.XtraEditors.MemoEdit();
            this.txttratamientopco = new DevExpress.XtraEditors.MemoEdit();
            this.txtconductapco = new DevExpress.XtraEditors.MemoEdit();
            this.cmbidcie = new DevExpress.XtraEditors.LookUpEdit();
            this.btnRegistrar = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.dtpfechapco.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpfechapco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtphorapco.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtphorapco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuadroclinicopco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdiagnosticopco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttratamientopco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtconductapco.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbidcie.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitulo
            // 
            this.lblTitulo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTitulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblTitulo.Location = new System.Drawing.Point(12, 9);
            this.lblTitulo.Name = "lblTitulo";
            this.lblTitulo.Size = new System.Drawing.Size(617, 53);
            this.lblTitulo.TabIndex = 0;
            this.lblTitulo.Text = "Registro de Consultas";
            this.lblTitulo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblfechapco
            // 
            this.lblfechapco.AutoSize = true;
            this.lblfechapco.Location = new System.Drawing.Point(14, 87);
            this.lblfechapco.Name = "lblfechapco";
            this.lblfechapco.Size = new System.Drawing.Size(40, 13);
            this.lblfechapco.TabIndex = 1;
            this.lblfechapco.Text = "Fecha:";
            // 
            // dtpfechapco
            // 
            this.dtpfechapco.EditValue = new System.DateTime(2013, 5, 31, 10, 22, 7, 115);
            this.dtpfechapco.Location = new System.Drawing.Point(99, 84);
            this.dtpfechapco.Name = "dtpfechapco";
            this.dtpfechapco.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtpfechapco.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtpfechapco.Size = new System.Drawing.Size(204, 20);
            this.dtpfechapco.TabIndex = 2;
            // 
            // lblhorapco
            // 
            this.lblhorapco.AutoSize = true;
            this.lblhorapco.Location = new System.Drawing.Point(400, 87);
            this.lblhorapco.Name = "lblhorapco";
            this.lblhorapco.Size = new System.Drawing.Size(34, 13);
            this.lblhorapco.TabIndex = 3;
            this.lblhorapco.Text = "Hora:";
            // 
            // dtphorapco
            // 
            this.dtphorapco.EditValue = new System.DateTime(2013, 5, 31, 10, 21, 53, 227);
            this.dtphorapco.Location = new System.Drawing.Point(485, 84);
            this.dtphorapco.Name = "dtphorapco";
            this.dtphorapco.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.dtphorapco.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.dtphorapco.Properties.DisplayFormat.FormatString = "t";
            this.dtphorapco.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtphorapco.Properties.EditFormat.FormatString = "t";
            this.dtphorapco.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.dtphorapco.Properties.Mask.EditMask = "t";
            this.dtphorapco.Size = new System.Drawing.Size(204, 20);
            this.dtphorapco.TabIndex = 4;
            // 
            // lblcuadroclinicopco
            // 
            this.lblcuadroclinicopco.AutoSize = true;
            this.lblcuadroclinicopco.Location = new System.Drawing.Point(14, 116);
            this.lblcuadroclinicopco.Name = "lblcuadroclinicopco";
            this.lblcuadroclinicopco.Size = new System.Drawing.Size(79, 13);
            this.lblcuadroclinicopco.TabIndex = 5;
            this.lblcuadroclinicopco.Text = "Cuadro Clinico:";
            // 
            // lbldiagnosticopco
            // 
            this.lbldiagnosticopco.AutoSize = true;
            this.lbldiagnosticopco.Location = new System.Drawing.Point(400, 142);
            this.lbldiagnosticopco.Name = "lbldiagnosticopco";
            this.lbldiagnosticopco.Size = new System.Drawing.Size(66, 13);
            this.lbldiagnosticopco.TabIndex = 9;
            this.lbldiagnosticopco.Text = "Diagnostico:";
            // 
            // lblidcie
            // 
            this.lblidcie.AutoSize = true;
            this.lblidcie.Location = new System.Drawing.Point(400, 116);
            this.lblidcie.Name = "lblidcie";
            this.lblidcie.Size = new System.Drawing.Size(44, 13);
            this.lblidcie.TabIndex = 7;
            this.lblidcie.Text = "CIE-10:";
            // 
            // lblconductapco
            // 
            this.lblconductapco.AutoSize = true;
            this.lblconductapco.Location = new System.Drawing.Point(14, 204);
            this.lblconductapco.Name = "lblconductapco";
            this.lblconductapco.Size = new System.Drawing.Size(57, 13);
            this.lblconductapco.TabIndex = 11;
            this.lblconductapco.Text = "Conducta:";
            // 
            // lbltratamientopco
            // 
            this.lbltratamientopco.AutoSize = true;
            this.lbltratamientopco.Location = new System.Drawing.Point(398, 204);
            this.lbltratamientopco.Name = "lbltratamientopco";
            this.lbltratamientopco.Size = new System.Drawing.Size(69, 13);
            this.lbltratamientopco.TabIndex = 13;
            this.lbltratamientopco.Text = "Tratamiento:";
            // 
            // btnCancelar
            // 
            this.btnCancelar.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelar.Location = new System.Drawing.Point(534, 293);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(108, 34);
            this.btnCancelar.TabIndex = 15;
            this.btnCancelar.Text = "&Cancelar";
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // txtcuadroclinicopco
            // 
            this.txtcuadroclinicopco.Location = new System.Drawing.Point(99, 113);
            this.txtcuadroclinicopco.Name = "txtcuadroclinicopco";
            this.txtcuadroclinicopco.Size = new System.Drawing.Size(271, 82);
            this.txtcuadroclinicopco.TabIndex = 6;
            // 
            // txtdiagnosticopco
            // 
            this.txtdiagnosticopco.Location = new System.Drawing.Point(485, 139);
            this.txtdiagnosticopco.Name = "txtdiagnosticopco";
            this.txtdiagnosticopco.Size = new System.Drawing.Size(271, 56);
            this.txtdiagnosticopco.TabIndex = 10;
            // 
            // txttratamientopco
            // 
            this.txttratamientopco.Location = new System.Drawing.Point(485, 201);
            this.txttratamientopco.Name = "txttratamientopco";
            this.txttratamientopco.Size = new System.Drawing.Size(271, 82);
            this.txttratamientopco.TabIndex = 14;
            // 
            // txtconductapco
            // 
            this.txtconductapco.Location = new System.Drawing.Point(99, 201);
            this.txtconductapco.Name = "txtconductapco";
            this.txtconductapco.Size = new System.Drawing.Size(271, 82);
            this.txtconductapco.TabIndex = 12;
            // 
            // cmbidcie
            // 
            this.cmbidcie.Location = new System.Drawing.Point(485, 113);
            this.cmbidcie.Name = "cmbidcie";
            this.cmbidcie.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbidcie.Properties.PopupWidth = 500;
            this.cmbidcie.Size = new System.Drawing.Size(271, 20);
            this.cmbidcie.TabIndex = 8;
            this.cmbidcie.EditValueChanged += new System.EventHandler(this.cmbidcie_EditValueChanged);
            // 
            // btnRegistrar
            // 
            this.btnRegistrar.Appearance.BackColor = DevExpress.LookAndFeel.DXSkinColors.FillColors.Success;
            this.btnRegistrar.Appearance.Options.UseBackColor = true;
            this.btnRegistrar.Location = new System.Drawing.Point(648, 293);
            this.btnRegistrar.Name = "btnRegistrar";
            this.btnRegistrar.Size = new System.Drawing.Size(108, 34);
            this.btnRegistrar.TabIndex = 16;
            this.btnRegistrar.Text = "&Registrar";
            this.btnRegistrar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // FPacConsultas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancelar;
            this.ClientSize = new System.Drawing.Size(784, 339);
            this.Controls.Add(this.btnRegistrar);
            this.Controls.Add(this.cmbidcie);
            this.Controls.Add(this.txtconductapco);
            this.Controls.Add(this.txtdiagnosticopco);
            this.Controls.Add(this.txttratamientopco);
            this.Controls.Add(this.txtcuadroclinicopco);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lblfechapco);
            this.Controls.Add(this.dtpfechapco);
            this.Controls.Add(this.lblhorapco);
            this.Controls.Add(this.dtphorapco);
            this.Controls.Add(this.lblcuadroclinicopco);
            this.Controls.Add(this.lbldiagnosticopco);
            this.Controls.Add(this.lblidcie);
            this.Controls.Add(this.lblconductapco);
            this.Controls.Add(this.lbltratamientopco);
            this.Controls.Add(this.lblTitulo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.IconOptions.Icon = ((System.Drawing.Icon)(resources.GetObject("FPacConsultas.IconOptions.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FPacConsultas";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consultas";
            this.Load += new System.EventHandler(this.fPacConsultas_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtpfechapco.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtpfechapco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtphorapco.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtphorapco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtcuadroclinicopco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtdiagnosticopco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txttratamientopco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtconductapco.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbidcie.Properties)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		
		#endregion

        internal System.Windows.Forms.Label lblTitulo;
		internal System.Windows.Forms.Label lblfechapco;
		private DevExpress.XtraEditors.DateEdit dtpfechapco;
		internal System.Windows.Forms.Label lblhorapco;
		private DevExpress.XtraEditors.DateEdit dtphorapco;
        internal System.Windows.Forms.Label lblcuadroclinicopco;
        internal System.Windows.Forms.Label lbldiagnosticopco;
        internal System.Windows.Forms.Label lblidcie;
        internal System.Windows.Forms.Label lblconductapco;
        internal System.Windows.Forms.Label lbltratamientopco;
		private DevExpress.XtraEditors.SimpleButton btnCancelar;
        private DevExpress.XtraEditors.MemoEdit txtcuadroclinicopco;
        private DevExpress.XtraEditors.MemoEdit txtdiagnosticopco;
        private DevExpress.XtraEditors.MemoEdit txttratamientopco;
        private DevExpress.XtraEditors.MemoEdit txtconductapco;
        private DevExpress.XtraEditors.LookUpEdit cmbidcie;
        private DevExpress.XtraEditors.SimpleButton btnRegistrar;
	}
}

