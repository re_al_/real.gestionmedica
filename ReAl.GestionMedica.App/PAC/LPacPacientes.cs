﻿using DevExpress.Utils;
using Newtonsoft.Json;
using ReAl.GestionMedica.App.CAL;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
using ReAl.GestionMedica.Class;

namespace ReAl.GestionMedica.App.PAC
{
    public partial class LPacPacientes : DevExpress.XtraEditors.XtraForm
    {
        #region Constructors

        public LPacPacientes()
        {
            InitializeComponent();
        }

        #endregion Constructors

        #region Methods

        public void CargarListado()
        {
            try
            {
                var list = cParametrosApp.AppPatientsList;
                gcPacPaciente.DataSource = list;
                if (list is { Count: > 0 })
                {
                    gvPacPaciente.PopulateColumns();
                    gvPacPaciente.Columns[PatPatients.Fields.IdPatients.ToString()].Visible = false;
                    gvPacPaciente.Columns[PatPatients.Fields.Birthdate.ToString()].DisplayFormat.FormatType = FormatType.DateTime;
                    gvPacPaciente.Columns[PatPatients.Fields.Birthdate.ToString()].DisplayFormat.FormatString = "{0:dd/MM/yyyy}";

                    //gvPacPaciente.Columns["UltimaVisita"].DisplayFormat.FormatType = FormatType.DateTime;
                    //gvPacPaciente.Columns["UltimaVisita"].DisplayFormat.FormatString = "{0:dd/MM/yyyy HH:mm}";
                }

                gvPacPaciente.ShowFindPanel();                
                gvPacPaciente.OptionsBehavior.Editable = false;
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog(this.Parent);
            }
        }

        public void EliminarRegistro()
        {
            try
            {
                if (gvPacPaciente.RowCount <= 0) return;
                if (MessageBox.Show("¿Desea DAR DE BAJA el registro seleccionado?", "Eliminar registro", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {
                    var service = new PatPatientsController();
                    var obj = service.GetById(long.Parse(gvPacPaciente.GetFocusedRowCellValue(PatPatients.Fields.IdPatients.ToString()).ToString()));
                    if (obj != null)
                    {
                        //Abrimos la AUDITORIA
                        obj.ApiTransaction = CApi.Transaccion.DELETE.ToString();
                        if (service.Update(obj))
                        {
                            //Init Task to GetPatients
                            Task<bool> taskPatients = Task.Run(() =>
                            {
                                var service = new PatPatientsController();
                                cParametrosApp.AppPatientsList = service.GetActive();
                                return true;
                            });
                            taskPatients.Wait();
                            this.CargarListado();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void ModificarRegistro()
        {
            try
            {
                if (gvPacPaciente.RowCount <= 0) return;
                var service = new PatPatientsController();
                var obj= service.GetById(long.Parse(gvPacPaciente.GetFocusedRowCellValue(PatPatients.Fields.IdPatients.ToString()).ToString()));
                if (obj != null)
                {
                    var frm = new FPacPacientes(obj);
                    var resultado = frm.ShowDialog();
                    if (resultado == DialogResult.OK)
                    {
                        //Init Task to GetPatients
                        Task<bool> taskPatients = Task.Run(() =>
                        {
                            var service = new PatPatientsController();
                            cParametrosApp.AppPatientsList = service.GetActive();
                            return true;
                        });
                        taskPatients.Wait();
                        this.CargarListado();
                        if (obj.IdPatients == cParametrosApp.AppPaciente.IdPatients)
                        {
                            obj = service.GetById(long.Parse(gvPacPaciente.GetFocusedRowCellValue(PatPatients.Fields.IdPatients.ToString()).ToString()));
                            if (obj != null)
                            {
                                cParametrosApp.AppPaciente = obj;
                            }
                        }
                    }
                }
                
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        public void SeleccionarRegistro()
        {
            try
            {
                if (gvPacPaciente.RowCount <= 0) return;
                cParametrosApp.AppPaciente = new PatPatients();
                cParametrosApp.AppPaciente.IdPatients = 0;

                var service = new PatPatientsController();
                var obj = service.GetById(long.Parse(gvPacPaciente.GetFocusedRowCellValue(PatPatients.Fields.IdPatients.ToString()).ToString()));
                cParametrosApp.AppPaciente = obj;

                //Tratamos de cerrar las pantallas
                var miParent = this.MdiParent;
                try
                {
                    var frm = (FPrincipal)miParent;

                    frm.CargarDatosPaciente();
                    frm.CargarDatosCita();

                    foreach (var mdiChild in frm.MdiChildren)
                    {
                        if (mdiChild.GetType() == typeof(FPacHistoriaClinica)) mdiChild.Close();

                        if (mdiChild.GetType() == typeof(LPacConsultas)) mdiChild.Close();

                        if (mdiChild.GetType() == typeof(LPacRecetas)) mdiChild.Close();

                        if (mdiChild.GetType() == typeof(LPacCirugias)) mdiChild.Close();

                        if (mdiChild.GetType() == typeof(LPacInformes)) mdiChild.Close();

                        if (mdiChild.GetType() == typeof(LPacMultimedia)) mdiChild.Close();
                    }
                    frm.ribbon.SelectedPage = frm.rpPacientes;
                }
                catch (Exception)
                {
                }
            
            }
            catch (Exception exp)
            {
                var frm = new FErrores(exp, FErrores.tipoError.error, "Error");
                frm.ShowDialog();
            }
        }

        private void btnAuditoría_Click(object sender, EventArgs e)
        {
            if (gvPacPaciente.RowCount <= 0) return;
            var service = new PatPatientsController();
            var obj = service.GetById(long.Parse(gvPacPaciente.GetFocusedRowCellValue(PatPatients.Fields.IdPatients.ToString()).ToString()));
            if (obj != null)
            {
                //Abrimos la AUDITORIA
                var frm = new FAuditaria(obj.ApiUsucre, obj.ApiFeccre, obj.ApiUsumod, obj.ApiFecmod, obj.ApiStatus);
                frm.ShowDialog();
            }
        }

        private void btnCita_Click(object sender, EventArgs e)
        {
            if (gvPacPaciente.RowCount <= 0) return;
            var service = new PatPatientsController();
            var obj = service.GetById(long.Parse(gvPacPaciente.GetFocusedRowCellValue(PatPatients.Fields.IdPatients.ToString()).ToString()));
            if (obj != null)
            {
                //Creamos el Form
                var frm = new FCalAgenda(obj);

                //Necesario para los skins
                frm.LookAndFeel.ParentLookAndFeel = this.LookAndFeel;
                frm.ShowDialog();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarRegistro();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            ModificarRegistro();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            var frm = new FPacPacientes();
            var resultado = frm.ShowDialog();

            if (resultado == DialogResult.OK)
            {
                //Init Task to GetPatients
                Task<bool> taskPatients = Task.Run(() =>
                {
                    var service = new PatPatientsController();
                    cParametrosApp.AppPatientsList = service.GetActive();
                    return true;
                });
                taskPatients.Wait();
                this.CargarListado();
            }
        }

        private void btnSeleccionar_Click(object sender, EventArgs e)
        {
            SeleccionarRegistro();
        }

        private void gcPacPaciente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.B && e.Modifiers == Keys.Control)
            {
                gvPacPaciente.ShowFindPanel();
            }
            if (e.KeyCode == Keys.F && e.Modifiers == Keys.Control)
            {
                gvPacPaciente.ShowFindPanel();
            }
            if (e.KeyCode == Keys.Delete)
            {
                EliminarRegistro();
            }
        }

        private void gcPacPaciente_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            SeleccionarRegistro();
        }

        private void lPacPacientes_Activated(object sender, EventArgs e)
        {
            //this.cargarListado();
        }

        private void lPacPacientes_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.B && e.Modifiers == Keys.Control)
            {
                gvPacPaciente.ShowFindPanel();
            }
            if (e.KeyCode == Keys.F && e.Modifiers == Keys.Control)
            {
                gvPacPaciente.ShowFindPanel();
            }
        }

        private void lPacPacientes_Load(object sender, EventArgs e)
        {
            //CargarListado();
        }

        #endregion Methods
    }
}