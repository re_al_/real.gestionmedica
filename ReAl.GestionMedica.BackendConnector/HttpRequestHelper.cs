﻿using RestSharp;
using System;
using Microsoft.Extensions.Logging;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ReAl.GestionMedica.BackendConnector.Services;
using Newtonsoft.Json.Serialization;

namespace ReAl.GestionMedica.BackendConnector
{
    internal class HttpRequestHelper
    {
        private readonly RestClient _client;
        private readonly ILogger _logger;

        public HttpRequestHelper(ILogger logger)
        {
            _client = new RestClient(BackendParameters.ApiDomain);
            _logger = logger;
        }

        public HttpRequestHelper()
        {
            _client = new RestClient(BackendParameters.ApiDomain);
            
        }

        internal dynamic SimpleGet(out bool success, string path, bool includeToken = true)
        {
            success = false;
            dynamic dataExecution = null;
            RestResponse response = null;
            try
            {
                var request = new RestRequest(path);
                if (includeToken)
                    request.AddHeader("Authorization", "Bearer " + BackendParameters.JwtToken);
                _logger?.LogInformation("GET " + request.Resource);
                response = _client.ExecuteGet(request);
                _logger?.LogInformation("Response {code} for GET {resource}{content}", response.StatusCode, request.Resource,
                    response.Content != null ? "\r\n" + response.Content : "");
                if (response.Content != null)
                    dataExecution = Newtonsoft.Json.Linq.JObject.Parse(response.Content);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    if (dataExecution != null)
                        success = dataExecution.responseCode != null && dataExecution.responseCode == "OK";
                }
                else if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new UnauthorizedAccessException();
                }
                else if (response.StatusCode == HttpStatusCode.BadGateway)
                {
                    success = false;
                    dataExecution = "HTTP " + response.StatusCode;
                }
                else
                {
                    if (response.Content != null)
                        dataExecution = Newtonsoft.Json.Linq.JObject.Parse(response.Content);
                    else
                        dataExecution = "HTTP " + response.StatusCode +
                                        (response.Content != null ? ": " + response.Content : "");
                }
                return dataExecution;
            }
            catch (UnauthorizedAccessException)
            {
                var refreshToken = new AuthServices();
                if (refreshToken.RefreshToken()) return SimpleGet(out success, path);
                return "Usuario no Autenticado";
            }
            catch (Exception)
            {
                if (response == null) return null;
                if (response.ErrorMessage != null)
                    dataExecution = response.ErrorMessage;
                else if (response.Content != null)
                    dataExecution = Newtonsoft.Json.Linq.JObject.Parse(response.Content);
                return dataExecution;
            }
        }

        internal dynamic SimpleDelete(out bool success, string path)
        {
            success = false;
            dynamic dataExecution = null;
            RestResponse response = null;
            try
            {
                var request = new RestRequest(path, Method.Delete);
                request.AddHeader("Authorization", "Bearer " + BackendParameters.JwtToken);
                _logger?.LogInformation("DELETE " + request.Resource);
                response = _client.Delete(request);
                _logger?.LogInformation("Response {code} for DELETE {resource}{content}", response.StatusCode, request.Resource,
                    response.Content != null ? "\r\n" + response.Content : "");
                if (response.Content != null)
                    dataExecution = Newtonsoft.Json.Linq.JObject.Parse(response.Content);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    if (dataExecution != null)
                        success = dataExecution.responseCode != null && dataExecution.responseCode == "OK";
                }
                else if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new UnauthorizedAccessException();
                }
                else if (response.StatusCode == HttpStatusCode.BadGateway)
                {
                    success = false;
                    dataExecution = "HTTP " + response.StatusCode;
                }
                else
                {
                    if (response.Content != null)
                        dataExecution = Newtonsoft.Json.Linq.JObject.Parse(response.Content);
                    else
                        dataExecution = "HTTP " + response.StatusCode +
                                        (response.Content != null ? ": " + response.Content : "");
                }
                return dataExecution;
            }
            catch (UnauthorizedAccessException)
            {
                var refreshToken = new AuthServices();
                if (refreshToken.RefreshToken()) return SimpleDelete(out success, path);
                return "Usuario no Autenticado";
            }
            catch (Exception)
            {
                if (response == null) return null;
                if (response.ErrorMessage != null)
                    dataExecution = response.ErrorMessage;
                else if (response.Content != null)
                    dataExecution = Newtonsoft.Json.Linq.JObject.Parse(response.Content);
                return dataExecution;
            }
        }

        internal dynamic SimplePost(out bool success, string path, string payload, bool includeToken = true)
        {
            success = false;
            dynamic dataExecution = null;
            RestResponse response = null;
            try
            {
                if (!IsValidJson(payload))
                {
                    return "Ha especificado un valor inválido en el formulario. Corrija el valor e intente de nuevo";
                }
                var request = string.IsNullOrEmpty(payload) ? new RestRequest(path, Method.Post) : new RestRequest(path, Method.Post).AddJsonBody(payload);
                if (includeToken)
                    request.AddHeader("Authorization", "Bearer " + BackendParameters.JwtToken);
                _logger?.LogInformation("POST " + request.Resource + "\r\n" + payload);
                response = _client.ExecutePost(request);
                _logger?.LogInformation("Response {code} for POST {resource}{content}", response.StatusCode, request.Resource,
                    response.Content != null ? "\r\n" + response.Content : "");

                if (response.Content != null)
                    dataExecution = Newtonsoft.Json.Linq.JObject.Parse(response.Content);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    if (dataExecution != null)
                        success = dataExecution.responseCode != null && dataExecution.responseCode == "OK";
                }
                else if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new UnauthorizedAccessException();
                }
                else if (response.StatusCode == HttpStatusCode.BadGateway)
                {
                    success = false;
                    dataExecution = "HTTP " + response.StatusCode;
                }
                else
                {
                    if (response.Content != null)
                        dataExecution = Newtonsoft.Json.Linq.JObject.Parse(response.Content);
                    else
                        dataExecution = "HTTP " + response.StatusCode +
                                        (response.Content != null ? ": " + response.Content : "");
                }
                return dataExecution;
            }
            catch (UnauthorizedAccessException)
            {
                var refreshToken = new AuthServices();
                if (refreshToken.RefreshToken()) return SimplePost(out success, path, payload);
                return "Usuario no Autenticado";
            }
            catch (Exception)
            {
                if (response == null) return null;
                if (response.ErrorMessage != null)
                    dataExecution = response.ErrorMessage;
                else if (response.Content != null)
                    dataExecution = Newtonsoft.Json.Linq.JObject.Parse(response.Content);
                return dataExecution;
            }
        }

        internal dynamic SimplePut(out bool success, string path, string payload = "")
        {
            success = false;
            dynamic dataExecution = null;
            RestResponse response = null;
            try
            {
                if (payload != "" && !IsValidJson(payload))
                {
                    return "Ha especificado un valor inválido en el formulario. Corrija el valor e intente de nuevo";
                }

                var request = string.IsNullOrEmpty(payload) ? new RestRequest(path, Method.Put) : new RestRequest(path, Method.Put).AddJsonBody(payload);
                request.AddHeader("Authorization", "Bearer " + BackendParameters.JwtToken);
                _logger?.LogInformation("PUT " + request.Resource + "\r\n" + payload);
                response = _client.ExecutePut(request);
                _logger?.LogInformation("Response {code} for PUT {resource}{content}", response.StatusCode, request.Resource,
                    response.Content != null ? "\r\n" + response.Content : "");

                if (response.Content != null)
                    dataExecution = Newtonsoft.Json.Linq.JObject.Parse(response.Content);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    if (dataExecution != null)
                        success = dataExecution.responseCode != null && dataExecution.responseCode == "OK";
                }
                else if (response.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new UnauthorizedAccessException();
                }
                else if (response.StatusCode == HttpStatusCode.BadGateway)
                {
                    success = false;
                    dataExecution = "HTTP " + response.StatusCode;
                }
                else
                {
                    if (response.Content != null)
                        dataExecution = Newtonsoft.Json.Linq.JObject.Parse(response.Content);
                    else
                        dataExecution = "HTTP " + response.StatusCode +
                                        (response.Content != null ? ": " + response.Content : "");
                }
                return dataExecution;
            }
            catch (UnauthorizedAccessException)
            {
                var refreshToken = new AuthServices();
                if (refreshToken.RefreshToken()) return SimplePut(out success, path, payload);
                return "Usuario no Autenticado";
            }
            catch (Exception)
            {
                if (response == null) return null;
                if (response.ErrorMessage != null)
                    dataExecution = response.ErrorMessage;
                else if (response.Content != null)
                    dataExecution = Newtonsoft.Json.Linq.JObject.Parse(response.Content);
                return dataExecution;
            }
        }

        private bool IsValidJson(string strInput)
        {
            if (string.IsNullOrWhiteSpace(strInput)) { return false; }
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    //Exception in parsing json
                    Console.WriteLine(jex.Message);
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    Console.WriteLine(ex.ToString());
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }

    internal class UnderscorePropertyNamesContractResolver : DefaultContractResolver
    {
        public UnderscorePropertyNamesContractResolver()
        {
            NamingStrategy = new SnakeCaseNamingStrategy();
        }
    }
}
