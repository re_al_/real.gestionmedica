﻿namespace ReAl.GestionMedica.BackendConnector
{
    public static class BackendParameters
    {
        public static string JwtToken = string.Empty;
        internal static string JwtRefreshToken = string.Empty;
        //internal static string ApiDomain = "https://marcoa136.sg-host.com/gestionmed";
        internal static string ApiDomain = "https://neurodiagnostico.com.bo/gestionmed";
        internal static string SecretKey = string.Empty;
    }
}
