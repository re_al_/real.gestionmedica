﻿using System.Collections.Generic;
using System.Text.Json;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ReAl.GestionMedica.BackendConnector.Entities;

namespace ReAl.GestionMedica.BackendConnector.Services
{
    internal class PatPatientsServices
    {
        internal const string Path = "patients";
        private readonly HttpRequestHelper _httpRequestHelper;

        public PatPatientsServices(ILogger logger)
        {
            _httpRequestHelper = new HttpRequestHelper(logger);
        }

        public PatPatientsServices()
        {
            _httpRequestHelper = new HttpRequestHelper();
        }

        public dynamic GetAll(out bool success, int limit = 10000, int lastIndex = 0, string sortColumn = "id_patients", string searchCriteria = "")
        {
            dynamic payload = new JObject();
            payload.limit = limit;
            payload.last_index = lastIndex;
            payload.sort_column = sortColumn;
            payload.search_criteria = searchCriteria;

            var endpoint = Path + "/all";
            var dataExecution = _httpRequestHelper.SimplePost(out success, endpoint, payload.ToString());
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetActive(out bool success, int limit = 10000, int lastIndex = 0, string sortColumn = "id_patients", string searchCriteria = "")
        {
            dynamic payload = new JObject();
            payload.limit = limit;
            payload.last_index = lastIndex;
            payload.sort_column = sortColumn;
            payload.search_criteria = searchCriteria;


            var endpoint = Path + "/active";
            var dataExecution = _httpRequestHelper.SimplePost(out success, endpoint, payload.ToString());
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetById(out bool success, long id)
        {
            var endpoint = Path + "/" + id;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Create(out bool success, PatPatients obj)
        {
            var endpoint = Path;
            string payload = JsonConvert.SerializeObject(obj);
            var dataExecution = _httpRequestHelper.SimplePost(out success, endpoint, payload.ToString());
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Update(out bool success, PatPatients obj)
        {
            var endpoint = Path + "/" + obj.IdPatients;
            string payload = JsonConvert.SerializeObject(obj);
            var dataExecution = _httpRequestHelper.SimplePut(out success, endpoint, payload.ToString());
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Delete(out bool success, long id)
        {
            var endpoint = Path + "/" + id;
            var dataExecution = _httpRequestHelper.SimpleDelete(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }
    }
}