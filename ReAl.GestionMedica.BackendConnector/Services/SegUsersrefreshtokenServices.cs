#region 
/***********************************************************************************************************
	NOMBRE:       SegUsersrefreshtoken
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla seg_users_refresh_token

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        06/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System.Text.Json;
using Microsoft.Extensions.Logging;
using ReAl.GestionMedica.BackendConnector.Entities;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Services
{
	internal class SegUsersrefreshtokenServices
	{
		
        internal const string Path = "xxxxxxxx/";
        private readonly HttpRequestHelper _httpRequestHelper;

        public SegUsersrefreshtokenServices(ILogger logger)
        {
            _httpRequestHelper = new HttpRequestHelper(logger);
        }

        public SegUsersrefreshtokenServices()
        {
	        _httpRequestHelper = new HttpRequestHelper();
        }

        public dynamic GetAll(out bool success)
        {
	        var endpoint = Path + "all";
	        var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

        public dynamic GetActive(out bool success)
        {
	        var endpoint = Path + "active";
	        var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

        public dynamic GetById(out bool success, long id)
        {
	        var endpoint = Path + "/" + id;
	        var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

        public dynamic Create(out bool success, SegUsersrefreshtoken obj)
        {
	        var endpoint = Path + "/";
	        string payload = JsonConvert.SerializeObject<SegUsersrefreshtoken>(obj);
	        var dataExecution = _httpRequestHelper.SimplePost(out success, endpoint, payload);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

        public dynamic Update(out bool success, SegUsersrefreshtoken obj)
        {
	        var endpoint = Path + "/" + obj.id_;

	        string payload = JsonConvert.SerializeObject<SegUsersrefreshtoken>(obj);
	        var dataExecution = _httpRequestHelper.SimplePut(out success, endpoint, payload);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

        public dynamic Delete(out bool success, long id)
        {
	        var endpoint = Path + "/" + id;
	        var dataExecution = _httpRequestHelper.SimpleDelete(out success, endpoint);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

	}
}

