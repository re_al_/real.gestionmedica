#region 
/***********************************************************************************************************
	NOMBRE:       PatHistoryneuro1
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla pat_history_neuro1

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        06/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System.Text.Json;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Entities;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Services
{
	internal class PatHistoryneuro1Services
	{
		
        internal const string Path = "historyneuro1";
        private readonly HttpRequestHelper _httpRequestHelper;

        public PatHistoryneuro1Services(ILogger logger)
        {
            _httpRequestHelper = new HttpRequestHelper(logger);
        }

        public PatHistoryneuro1Services()
        {
	        _httpRequestHelper = new HttpRequestHelper();
        }

        public dynamic GetAll(out bool success)
        {
	        var endpoint = Path + "/all";
	        var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

        public dynamic GetActive(out bool success)
        {
	        var endpoint = Path + "/active";
	        var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

        public dynamic GetById(out bool success, long id)
        {
	        var endpoint = Path + "/" + id;
	        var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

        public dynamic Create(out bool success, PatHistoryneuro1 obj)
        {
	        var endpoint = Path;
	        string payload = JsonConvert.SerializeObject(obj);
	        var dataExecution = _httpRequestHelper.SimplePost(out success, endpoint, payload);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }
    }
}

