﻿using System.Text.Json;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ReAl.GestionMedica.BackendConnector.Entities;

namespace ReAl.GestionMedica.BackendConnector.Services
{
    internal class PatAppointmentsServices
    {
        internal const string Path = "appointments";
        private readonly HttpRequestHelper _httpRequestHelper;

        public PatAppointmentsServices(ILogger logger)
        {
            _httpRequestHelper = new HttpRequestHelper(logger);
        }

        public PatAppointmentsServices()
        {
            _httpRequestHelper = new HttpRequestHelper();
        }

        public dynamic GetAll(out bool success)
        {
            var endpoint = Path + "/all";
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetActive(out bool success)
        {
            var endpoint = Path + "/active";
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetPending(out bool success)
        {
            var endpoint = Path + "/pending";
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetTodayPendings(out bool success)
        {
            var endpoint = Path + "/todayPendings";
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetDayPendings(out bool success, string dateFilter)
        {
            var endpoint = Path + "/dayPendings/" + dateFilter;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetWeekPendings(out bool success, string dateFilter)
        {
            var endpoint = Path + "/weekPendings/" + dateFilter;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetMonthPendings(out bool success, string dateFilter)
        {
            var endpoint = Path + "/monthPendings/" + dateFilter;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetByPatientId(out bool success, long idPatients)
        {
            var endpoint = Path + "/byPatientId/" + idPatients;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetAppointmentsByPatientId(out bool success, long idPatients)
        {
            var endpoint = Path + "/appointmentsByPatientId/" + idPatients;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetById(out bool success, long id)
        {
            var endpoint = Path + "/" + id;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Create(out bool success, PatAppointments obj)
        {
            var endpoint = Path;
            string payload = JsonConvert.SerializeObject(obj);
            var dataExecution = _httpRequestHelper.SimplePost(out success, endpoint, payload);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Update(out bool success, PatAppointments obj)
        {
            var endpoint = Path + "/" + obj.IdAppointments;

            string payload = JsonConvert.SerializeObject(obj);
            var dataExecution = _httpRequestHelper.SimplePut(out success, endpoint, payload);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Delete(out bool success, long id)
        {
            var endpoint = Path + "/" + id;
            var dataExecution = _httpRequestHelper.SimpleDelete(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }
    }
}