﻿using System;


using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace ReAl.GestionMedica.BackendConnector.Services
{
    public class AuthServices
    {
        internal const string Path = "auth";
        private readonly HttpRequestHelper _httpRequestHelper;

        public AuthServices(ILogger logger)
        {
            _httpRequestHelper = new HttpRequestHelper(logger);
        }

        public AuthServices()
        {
            _httpRequestHelper = new HttpRequestHelper();
        }

        public dynamic Login(out bool success, string userName, string password)
        {
            //var encryptedPassword = EncryptUtils.EncryptStringAes(password, BackendParameters.SecretKey);

            dynamic payload = new JObject();
            payload.login = userName;
            payload.password = password;

            var endpoint = Path + "/login";
            var dataExecution = _httpRequestHelper.SimplePost(out success, endpoint, payload.ToString(), false);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            BackendParameters.JwtToken = dataExecution.data.token;
            BackendParameters.JwtRefreshToken = dataExecution.data.refreshToken;
            return dataExecution;
        }

        internal bool RefreshToken()
        {
            try
            {
                if (string.IsNullOrEmpty(BackendParameters.JwtRefreshToken)) return false;
                dynamic payload = new JObject();
                payload.refresh_token = BackendParameters.JwtRefreshToken;
                string strApiBody = payload.ToString();
                var request = new RestRequest(Path + "refresh").AddJsonBody(strApiBody);
                var client = new RestClient(BackendParameters.ApiDomain);
                var response = client.ExecutePost(request);
                dynamic dataExecution = Newtonsoft.Json.Linq.JObject.Parse(response.Content);
                if (dataExecution == null) return false;
                if (dataExecution is string) return false;
                var success = dataExecution.responseCode != null && dataExecution.responseCode == "OK";
                if (!success) return false;
                BackendParameters.JwtToken = dataExecution.data.accessToken;
                BackendParameters.JwtRefreshToken = dataExecution.data.refreshToken;
                return true;
            }
            catch (UnauthorizedAccessException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}