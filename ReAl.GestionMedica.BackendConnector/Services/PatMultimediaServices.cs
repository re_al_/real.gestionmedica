﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using ReAl.GestionMedica.BackendConnector.Entities;

namespace ReAl.GestionMedica.BackendConnector.Services
{
    public class PatMultimediaServices
    {
        internal const string Path = "multimedia";
        private readonly HttpRequestHelper _httpRequestHelper;

        public PatMultimediaServices(ILogger logger)
        {
            _httpRequestHelper = new HttpRequestHelper(logger);
        }

        public PatMultimediaServices()
        {
            _httpRequestHelper = new HttpRequestHelper();
        }

        public dynamic GetAll(out bool success)
        {
            var endpoint = Path + "/all";
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetActive(out bool success)
        {
            var endpoint = Path + "/active";
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetByPatientId(out bool success, long idPatients)
        {
            var endpoint = Path + "/byPatientId/" + idPatients;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetById(out bool success, long id)
        {
            var endpoint = Path + "/" + id;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Create(out bool success, PatMultimedia obj)
        {
            var endpoint = Path;

            dynamic payload = new JObject();
            payload.extension = obj.Extension;
            payload.external_url = obj.ExternalUrl;
            payload.filename = obj.Filename;
            payload.id_multimedia = obj.IdMultimedia;
            payload.id_patients = obj.IdPatients;
            payload.observations = obj.Observations;
            payload.upload_date = obj.UploadDate;
            payload.api_transaction = "CREATE";
            var dataExecution = _httpRequestHelper.SimplePost(out success, endpoint, payload.ToString());
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Update(out bool success, PatMultimedia obj)
        {
            var endpoint = Path + "/" + obj.IdMultimedia;

            dynamic payload = new JObject();
            payload.extension = obj.Extension;
            payload.external_url = obj.ExternalUrl;
            payload.filename = obj.Filename;
            payload.id_multimedia = obj.IdMultimedia;
            payload.id_patients = obj.IdPatients;
            payload.observations = obj.Observations;
            payload.upload_date = obj.UploadDate;
            payload.api_transaction = obj.ApiTransaction;
            var dataExecution = _httpRequestHelper.SimplePut(out success, endpoint, payload.ToString());
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Delete(out bool success, long id)
        {
            var endpoint = Path + "/" + id;
            var dataExecution = _httpRequestHelper.SimpleDelete(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }
    }
}