﻿using System.Text.Json;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ReAl.GestionMedica.BackendConnector.Entities;

namespace ReAl.GestionMedica.BackendConnector.Services
{
    public class ClaBloodtypesServices
    {
        internal const string Path = "bloodtypes";
        private readonly HttpRequestHelper _httpRequestHelper;

        public ClaBloodtypesServices(ILogger logger)
        {
            _httpRequestHelper = new HttpRequestHelper(logger);
        }

        public ClaBloodtypesServices()
        {
            _httpRequestHelper = new HttpRequestHelper();
        }

        public dynamic GetAll(out bool success)
        {
            var endpoint = Path + "/all";
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetActive(out bool success)
        {
            var endpoint = Path + "/active";
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetById(out bool success, long id)
        {
            var endpoint = Path + "/" + id;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Create(out bool success, ClaBloodtypes obj)
        {
            var endpoint = Path;
            string payload = JsonConvert.SerializeObject(obj);
            var dataExecution = _httpRequestHelper.SimplePost(out success, endpoint, payload);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Update(out bool success, ClaBloodtypes obj)
        {
            var endpoint = Path + "/" + obj.IdBloodTypes;

            string payload = JsonConvert.SerializeObject(obj);
            var dataExecution = _httpRequestHelper.SimplePut(out success, endpoint, payload);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Delete(out bool success, long id)
        {
            var endpoint = Path + "/" + id;
            var dataExecution = _httpRequestHelper.SimpleDelete(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }
    }
}