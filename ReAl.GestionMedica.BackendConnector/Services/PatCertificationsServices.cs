#region 
/***********************************************************************************************************
	NOMBRE:       PatCertifications
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla pat_certifications

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        06/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System.Text.Json;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Entities;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Services
{
	internal class PatCertificationsServices
	{
		
        internal const string Path = "certifications";
        private readonly HttpRequestHelper _httpRequestHelper;

        public PatCertificationsServices(ILogger logger)
        {
            _httpRequestHelper = new HttpRequestHelper(logger);
        }

        public PatCertificationsServices()
        {
	        _httpRequestHelper = new HttpRequestHelper();
        }

        public dynamic GetAll(out bool success)
        {
	        var endpoint = Path + "/all";
	        var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

        public dynamic GetActive(out bool success)
        {
	        var endpoint = Path + "/active";
	        var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

        public dynamic GetByPatientId(out bool success, long idPatients)
        {
            var endpoint = Path + "/byPatientId/" + idPatients;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetById(out bool success, long id)
        {
	        var endpoint = Path + "/" + id;
	        var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

        public dynamic Create(out bool success, PatCertifications obj)
        {
	        var endpoint = Path;
	        string payload = JsonConvert.SerializeObject(obj);
	        var dataExecution = _httpRequestHelper.SimplePost(out success, endpoint, payload);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

        public dynamic Update(out bool success, PatCertifications obj)
        {
	        var endpoint = Path + "/" + obj.IdCertifications;

	        string payload = JsonConvert.SerializeObject(obj);
	        var dataExecution = _httpRequestHelper.SimplePut(out success, endpoint, payload);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

        public dynamic Delete(out bool success, long id)
        {
	        var endpoint = Path + "/" + id;
	        var dataExecution = _httpRequestHelper.SimpleDelete(out success, endpoint);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

	}
}

