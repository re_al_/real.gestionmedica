﻿

using System.Text.Json;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Entities;

namespace ReAl.GestionMedica.BackendConnector.Services
{
    internal class SegUsersServices
    {
        internal const string Path = "users";
        private readonly HttpRequestHelper _httpRequestHelper;

        public SegUsersServices(ILogger logger)
        {
            _httpRequestHelper = new HttpRequestHelper(logger);
        }

        public SegUsersServices()
        {
            _httpRequestHelper = new HttpRequestHelper();
        }

        public dynamic GetActive(out bool success)
        {
            var endpoint = Path + "/active";
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetDoctors(out bool success)
        {
            var endpoint = Path + "/doctors";
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Current(out bool success)
        {
            //var encryptedPassword = EncryptUtils.EncryptStringAes(password, BackendParameters.SecretKey);

            var endpoint = Path + "/current";
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetById(out bool success, long id)
        {
            var endpoint = Path + "/" + id;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetByLogin(out bool success, string login)
        {
            var endpoint = Path + "/byLogin/" + login;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Create(out bool success, SegUsers obj)
        {
            var endpoint = Path;
            string payload = JsonConvert.SerializeObject(obj);
            var dataExecution = _httpRequestHelper.SimplePost(out success, endpoint, payload);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Update(out bool success, SegUsers obj)
        {
            var endpoint = Path + "/" + obj.IdUsers;

            string payload = JsonConvert.SerializeObject(obj);
            var dataExecution = _httpRequestHelper.SimplePut(out success, endpoint, payload);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic UpdatePassword(out bool success, FilterChangePassword obj)
        {
            var endpoint = Path + "/" + obj.IdUsers;

            string payload = JsonConvert.SerializeObject(obj);
            var dataExecution = _httpRequestHelper.SimplePut(out success, endpoint, payload);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }
    }
}