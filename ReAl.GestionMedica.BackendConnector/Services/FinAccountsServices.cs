﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ReAl.GestionMedica.BackendConnector.Entities;

namespace ReAl.GestionMedica.BackendConnector.Services
{
    internal class FinAccountsServices
    {
        internal const string Path = "accounts";
        private readonly HttpRequestHelper _httpRequestHelper;

        public FinAccountsServices(ILogger logger)
        {
            _httpRequestHelper = new HttpRequestHelper(logger);
        }

        public FinAccountsServices()
        {
            _httpRequestHelper = new HttpRequestHelper();
        }

        public dynamic GetAll(out bool success)
        {
            var endpoint = Path + "/all";
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetActive(out bool success)
        {
            var endpoint = Path + "/active";
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetCobros(out bool success)
        {
            var endpoint = Path + "/cobros";
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }
        public dynamic GetById(out bool success, long id)
        {
            var endpoint = Path + "/" + id;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetbyAppointmentId(out bool success, long idAppointments)
        {
            var endpoint = Path + "/byAppointmentId/" + idAppointments;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetByDates(out bool success, FilterDates dates)
        {
            var endpoint = Path + "/inDates";
            string payload = JsonConvert.SerializeObject(dates);
            var dataExecution = _httpRequestHelper.SimplePost(out success, endpoint, payload);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Create(out bool success, FinAccounts obj)
        {
            var endpoint = Path;
            string payload = JsonConvert.SerializeObject(obj);
            var dataExecution = _httpRequestHelper.SimplePost(out success, endpoint, payload);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic CreateCharge(out bool success, FinAccounts obj)
        {
            var endpoint = Path + "/charge";
            string payload = JsonConvert.SerializeObject(obj);
            var dataExecution = _httpRequestHelper.SimplePost(out success, endpoint, payload);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Update(out bool success, FinAccounts obj)
        {
            var endpoint = Path + "/" + obj.IdAppointments;

            string payload = JsonConvert.SerializeObject(obj);
            var dataExecution = _httpRequestHelper.SimplePut(out success, endpoint, payload);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Payment(out bool success, FinAccounts obj)
        {
            var endpoint = Path + "/payment/" + obj.IdAccounts;
            var dataExecution = _httpRequestHelper.SimplePut(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic Delete(out bool success, long id)
        {
            var endpoint = Path + "/" + id;
            var dataExecution = _httpRequestHelper.SimpleDelete(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }
    }
}