#region 
/***********************************************************************************************************
	NOMBRE:       ClaCie
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla cla_cie

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        06/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System.Text.Json;
using Microsoft.Extensions.Logging;
using ReAl.GestionMedica.BackendConnector.Entities;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Services
{
	internal class DashboardServices
    {
		
        internal const string Path = "dashboard";
        private readonly HttpRequestHelper _httpRequestHelper;

        public DashboardServices(ILogger logger)
        {
            _httpRequestHelper = new HttpRequestHelper(logger);
        }

        public DashboardServices()
        {
	        _httpRequestHelper = new HttpRequestHelper();
        }

        public dynamic GetAbsence(out bool success, string strFrom, string strTo)
        {
	        var endpoint = Path + "/absence/" + strFrom + "/" + strTo;
	        var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
	        if (dataExecution == null)
		        return null;
	        if (dataExecution is string)
	        {
		        success = false;
		        return dataExecution;
	        }

	        success = dataExecution.status != null && dataExecution.status == "200";
	        if (!success) return dataExecution;
	        return dataExecution;
        }

        public dynamic GetFrequency(out bool success, string strFrom, string strTo)
        {
            var endpoint = Path + "/frequency/" + strFrom + "/" + strTo;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetCategory(out bool success, string strFrom, string strTo, string strType)
        {
            var endpoint = Path + "/categoryType/" + strFrom + "/" + strTo + "/" + strType;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetMayor(out bool success, string strFrom, string strTo)
        {
            var endpoint = Path + "/mayor/" + strFrom + "/" + strTo;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetTrend(out bool success, string strFrom, string strTo)
        {
            var endpoint = Path + "/trend/" + strFrom + "/" + strTo;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }

        public dynamic GetAppointments(out bool success, string strFrom, string strTo)
        {
            var endpoint = Path + "/appointments/" + strFrom + "/" + strTo;
            var dataExecution = _httpRequestHelper.SimpleGet(out success, endpoint);
            if (dataExecution == null)
                return null;
            if (dataExecution is string)
            {
                success = false;
                return dataExecution;
            }

            success = dataExecution.status != null && dataExecution.status == "200";
            if (!success) return dataExecution;
            return dataExecution;
        }
    }
}

