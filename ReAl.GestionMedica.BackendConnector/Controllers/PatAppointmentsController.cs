#region 
/***********************************************************************************************************
	NOMBRE:       PatAppointments
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla pat_appointments

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        06/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Controllers
{
	public class PatAppointmentsController
	{
        private readonly JsonSerializerSettings _settings;
        public PatAppointmentsController()
        {
            _settings = new JsonSerializerSettings()
            {
                //ContractResolver = new UnderscorePropertyNamesContractResolver()
            };
        }

        public List<PatAppointments> GetAll()
		{
			var service = new PatAppointmentsServices();
			var resultData = service.GetAll(out bool success);
			var list = new List<PatAppointments>();
			if (success)
			{
				string response = resultData.data.ToString();
				list = JsonConvert.DeserializeObject<List<PatAppointments>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public List<PatAppointments> GetActive()
		{
			var service = new PatAppointmentsServices();
			var resultData = service.GetActive(out bool success);
			var list = new List<PatAppointments>();
			if (success)
			{
				string response = resultData.data.ToString();
				list = JsonConvert.DeserializeObject<List<PatAppointments>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

        public List<PatAppointments> GetPending()
        {
            var service = new PatAppointmentsServices();
            var resultData = service.GetPending(out bool success);
            var list = new List<PatAppointments>();
            if (success)
            {
                string response = resultData.data.ToString();
                list = JsonConvert.DeserializeObject<List<PatAppointments>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public List<PatAppointments> GetTodayPendings()
        {
            var service = new PatAppointmentsServices();
            var resultData = service.GetTodayPendings(out bool success);
            var list = new List<PatAppointments>();
            if (success)
            {
                string response = resultData.data.ToString();
                list = JsonConvert.DeserializeObject<List<PatAppointments>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public List<PatAppointments> GetDayPendings(string strDateFilter)
        {
            var service = new PatAppointmentsServices();
            var resultData = service.GetDayPendings(out bool success, strDateFilter);
            var list = new List<PatAppointments>();
            if (success)
            {
                string response = resultData.data.ToString();
                list = JsonConvert.DeserializeObject<List<PatAppointments>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public List<PatAppointments> GetWeekPendings(string strDateFilter)
        {
            var service = new PatAppointmentsServices();
            var resultData = service.GetWeekPendings(out bool success, strDateFilter);
            var list = new List<PatAppointments>();
            if (success)
            {
                string response = resultData.data.ToString();
                list = JsonConvert.DeserializeObject<List<PatAppointments>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public List<PatAppointments> GetMonthPendings(string strDateFilter)
        {
            var service = new PatAppointmentsServices();
            var resultData = service.GetMonthPendings(out bool success, strDateFilter);
            var list = new List<PatAppointments>();
            if (success)
            {
                string response = resultData.data.ToString();
                list = JsonConvert.DeserializeObject<List<PatAppointments>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public List<PatAppointments> GetAppointmentsByPatientId(long idPatients)
        {
            var service = new PatAppointmentsServices();
            var resultData = service.GetAppointmentsByPatientId(out bool success, idPatients);
            var list = new List<PatAppointments>();
            if (success)
            {
                string response = resultData.data.ToString();
                list = JsonConvert.DeserializeObject<List<PatAppointments>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public PatAppointments GetById(long id)
		{
			var service = new PatAppointmentsServices();
			var resultData = service.GetById(out bool success, id);
			var obj = new PatAppointments();
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<PatAppointments>(response, _settings);
				return obj;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public bool Create(PatAppointments obj)
		{
			var service = new PatAppointmentsServices();
			var resultData = service.Create(out bool success, obj);
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<PatAppointments>(response, _settings);
				return obj != null;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public bool Update(PatAppointments obj)
		{
			var service = new PatAppointmentsServices();
			var resultData = service.Update(out bool success, obj);
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<PatAppointments>(response, _settings);
				return obj != null;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public void Delete(Int64 id)
		{
			var service = new PatAppointmentsServices();
			var resultData = service.Delete(out bool success, id);
			if (!success)
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

	}
}

