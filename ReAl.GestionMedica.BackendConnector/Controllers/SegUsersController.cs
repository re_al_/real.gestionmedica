#region 
/***********************************************************************************************************
	NOMBRE:       SegUsers
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla seg_users

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        06/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
#pragma warning disable S112
#endregion

namespace ReAl.GestionMedica.BackendConnector.Controllers
{
	public class SegUsersController
	{
        private readonly JsonSerializerSettings _settings;
        public SegUsersController()
        {
            _settings = new JsonSerializerSettings()
            {
                //ContractResolver = new UnderscorePropertyNamesContractResolver()
            };
        }

        public List<SegUsers> GetDoctors()
		{
			var service = new SegUsersServices();
			var resultData = service.GetDoctors(out bool success);
            if (success)
			{
				string response = resultData.data.ToString();
				var list = JsonConvert.DeserializeObject<List<SegUsers>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public List<SegUsers> GetActive()
		{
			var service = new SegUsersServices();
			var resultData = service.GetActive(out bool success);
            if (success)
			{
				string response = resultData.data.ToString();
				var list = JsonConvert.DeserializeObject<List<SegUsers>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

        public SegUsers Current()
        {
            var service = new SegUsersServices();
            var resultData = service.Current(out bool success);
            if (success)
            {
                string response = resultData.data.ToString();
                var list = JsonConvert.DeserializeObject<SegUsers>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public SegUsers GetById(long id)
        {
            var service = new SegUsersServices();
            var resultData = service.GetById(out bool success, id);
            if (success)
            {
                string response = resultData.data.ToString();
                var obj = JsonConvert.DeserializeObject<SegUsers>(response, _settings);
                return obj;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public SegUsers GetByLogin(string login)
        {
            var service = new SegUsersServices();
            var resultData = service.GetByLogin(out bool success, login);
            if (success)
            {
                string response = resultData.data.ToString();
                var obj = JsonConvert.DeserializeObject<SegUsers>(response, _settings);
                return obj;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }
        public bool Create(SegUsers obj)
        {
            var service = new SegUsersServices();
            var resultData = service.Create(out bool success, obj);
            if (success)
            {
                string response = resultData.data.ToString();
                obj = JsonConvert.DeserializeObject<SegUsers>(response, _settings);
                return obj != null;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public bool Update(SegUsers obj)
        {
            var service = new SegUsersServices();
            var resultData = service.Update(out bool success, obj);
            if (success)
            {
                string response = resultData.data.ToString();
                obj = JsonConvert.DeserializeObject<SegUsers>(response, _settings);
                return obj != null;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public bool UpdatePassword(FilterChangePassword obj)
        {
            var service = new SegUsersServices();
            var resultData = service.UpdatePassword(out bool success, obj);
            if (success)
            {
                string response = resultData.data.ToString();
                var objResp = JsonConvert.DeserializeObject<SegUsers>(response, _settings);
                return objResp != null;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }
    }

    
}

