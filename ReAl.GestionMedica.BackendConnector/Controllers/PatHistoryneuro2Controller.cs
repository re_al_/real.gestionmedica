#region 
/***********************************************************************************************************
	NOMBRE:       PatHistoryneuro2
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla pat_history_neuro2

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        06/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Controllers
{
	public class PatHistoryneuro2Controller
	{
        private readonly JsonSerializerSettings _settings;
        public PatHistoryneuro2Controller()
        {
            _settings = new JsonSerializerSettings()
            {
                //ContractResolver = new UnderscorePropertyNamesContractResolver()
            };
        }

        public List<PatHistoryneuro2> GetAll()
		{
			var service = new PatHistoryneuro2Services();
			var resultData = service.GetAll(out bool success);
			var list = new List<PatHistoryneuro2>();
			if (success)
			{
				string response = resultData.data.ToString();
				list = JsonConvert.DeserializeObject<List<PatHistoryneuro2>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public List<PatHistoryneuro2> GetActive()
		{
			var service = new PatHistoryneuro2Services();
			var resultData = service.GetActive(out bool success);
			var list = new List<PatHistoryneuro2>();
			if (success)
			{
				string response = resultData.data.ToString();
				list = JsonConvert.DeserializeObject<List<PatHistoryneuro2>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public PatHistoryneuro2 GetById(long id)
		{
			var service = new PatHistoryneuro2Services();
			var resultData = service.GetById(out bool success, id);
			var obj = new PatHistoryneuro2();
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<PatHistoryneuro2>(response, _settings);
				return obj;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public bool Create(PatHistoryneuro2 obj)
		{
			var service = new PatHistoryneuro2Services();
			var resultData = service.Create(out bool success, obj);
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<PatHistoryneuro2>(response, _settings);
				return obj != null;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}
    }
}

