﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;


namespace ReAl.GestionMedica.BackendConnector.Controller
{
    public class ClaMaritalController
    {
        private readonly JsonSerializerSettings _settings;
        public ClaMaritalController()
        {
            _settings = new JsonSerializerSettings()
            {
                //ContractResolver = new UnderscorePropertyNamesContractResolver()
            };
        }

        public List<ClaMarital> GetAll()
        {
            var service = new ClaMaritalServices();
            var resultData = service.GetAll(out bool success);
            var list = new List<ClaMarital>();
            if (success)
            {
                string response = resultData.data.ToString();
                list = JsonConvert.DeserializeObject<List<ClaMarital>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public List<ClaMarital> GetActive()
        {
            var service = new ClaMaritalServices();
            var resultData = service.GetActive(out bool success);
            var list = new List<ClaMarital>();
            if (success)
            {
                string response = resultData.data.ToString();
                list = JsonConvert.DeserializeObject<List<ClaMarital>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public ClaMarital GetById(long id)
        {
            var service = new ClaMaritalServices();
            var resultData = service.GetById(out bool success, id);
            var obj = new ClaMarital();
            if (success)
            {
                string response = resultData.data.ToString();
                obj = JsonConvert.DeserializeObject<ClaMarital>(response, _settings);
                return obj;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public bool Create(ClaMarital obj)
        {
            var service = new ClaMaritalServices();
            var resultData = service.Create(out bool success, obj);
            if (success)
            {
                string response = resultData.data.ToString();
                obj = JsonConvert.DeserializeObject<ClaMarital>(response, _settings);
                return obj != null;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public bool Update(ClaMarital obj)
        {
            var service = new ClaMaritalServices();
            var resultData = service.Update(out bool success, obj);
            if (success)
            {
                string response = resultData.data.ToString();
                obj = JsonConvert.DeserializeObject<ClaMarital>(response, _settings);
                return obj != null;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public void Delete(Int64 id)
        {
            var service = new ClaMaritalServices();
            var resultData = service.Delete(out bool success, id);
            if (success)
            {
                return;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }
    }
}
