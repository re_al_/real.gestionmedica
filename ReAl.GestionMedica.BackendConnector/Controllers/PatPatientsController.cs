#region 
/***********************************************************************************************************
	NOMBRE:       PatPatients
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla pat_patients

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        06/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Controllers
{
	public class PatPatientsController
	{
        private readonly JsonSerializerSettings _settings;
        public PatPatientsController()
        {
            _settings = new JsonSerializerSettings()
            {
                //ContractResolver = new UnderscorePropertyNamesContractResolver()
            };
        }

        public List<PatPatients> GetAll()
		{
			var service = new PatPatientsServices();
			var resultData = service.GetAll(out bool success);
			var list = new List<PatPatients>();
			if (success)
			{
				string response = resultData.data.elements.ToString();
				list = JsonConvert.DeserializeObject<List<PatPatients>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public List<PatPatients> GetActive()
		{
			var service = new PatPatientsServices();
			var resultData = service.GetActive(out bool success);
			var list = new List<PatPatients>();
			if (success)
			{
				string response = resultData.data.elements.ToString();
				list = JsonConvert.DeserializeObject<List<PatPatients>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public PatPatients GetById(long id)
		{
			var service = new PatPatientsServices();
			var resultData = service.GetById(out bool success, id);
			var obj = new PatPatients();
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<PatPatients>(response, _settings);
				return obj;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public bool Create(PatPatients obj)
		{
			var service = new PatPatientsServices();
			var resultData = service.Create(out bool success, obj);
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<PatPatients>(response, _settings);
				return obj != null;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public bool Update(PatPatients obj)
		{
			var service = new PatPatientsServices();
			var resultData = service.Update(out bool success, obj);
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<PatPatients>(response, _settings);
				return obj != null;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public void Delete(Int64 id)
		{
			var service = new PatPatientsServices();
			var resultData = service.Delete(out bool success, id);
			if (!success)
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

	}
}

