#region 
/***********************************************************************************************************
	NOMBRE:       PatPrescriptions
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla pat_prescriptions

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        06/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Controllers
{
	public class PatPrescriptionsController
	{
        private readonly JsonSerializerSettings _settings;
        public PatPrescriptionsController()
        {
            _settings = new JsonSerializerSettings()
            {
                //ContractResolver = new UnderscorePropertyNamesContractResolver()
            };
        }

        public List<PatPrescriptions> GetAll()
		{
			var service = new PatPrescriptionsServices();
			var resultData = service.GetAll(out bool success);
			var list = new List<PatPrescriptions>();
			if (success)
			{
				string response = resultData.data.ToString();
				list = JsonConvert.DeserializeObject<List<PatPrescriptions>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public List<PatPrescriptions> GetActive()
		{
			var service = new PatPrescriptionsServices();
			var resultData = service.GetActive(out bool success);
			var list = new List<PatPrescriptions>();
			if (success)
			{
				string response = resultData.data.ToString();
				list = JsonConvert.DeserializeObject<List<PatPrescriptions>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

        public List<PatPrescriptions> GetByPatientId(long idPatients)
        {
            var service = new PatPrescriptionsServices();
            var resultData = service.GetByPatientId(out bool success, idPatients);
            var list = new List<PatPrescriptions>();
            if (success)
            {
                string response = resultData.data.ToString();
                list = JsonConvert.DeserializeObject<List<PatPrescriptions>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public PatPrescriptions GetById(long id)
		{
			var service = new PatPrescriptionsServices();
			var resultData = service.GetById(out bool success, id);
			var obj = new PatPrescriptions();
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<PatPrescriptions>(response, _settings);
				return obj;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public bool Create(PatPrescriptions obj)
		{
			var service = new PatPrescriptionsServices();
			var resultData = service.Create(out bool success, obj);
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<PatPrescriptions>(response, _settings);
				return obj != null;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public bool Update(PatPrescriptions obj)
		{
			var service = new PatPrescriptionsServices();
			var resultData = service.Update(out bool success, obj);
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<PatPrescriptions>(response, _settings);
				return obj != null;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public void Delete(Int64 id)
		{
			var service = new PatPrescriptionsServices();
			var resultData = service.Delete(out bool success, id);
			if (!success)
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

	}
}

