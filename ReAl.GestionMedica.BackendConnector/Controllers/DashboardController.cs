#region 
/***********************************************************************************************************
	NOMBRE:       ClaCie
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla cla_cie

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        06/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
using ReAl.GestionMedica.BackendConnector.Utils;

#endregion

namespace ReAl.GestionMedica.BackendConnector.Controllers
{
	public class DashboardController
	{
        private readonly JsonSerializerSettings _settings;
        public DashboardController()
        {
            _settings = new JsonSerializerSettings()
            {
                //ContractResolver = new UnderscorePropertyNamesContractResolver()
            };
        }

        public List<DashboardAbsence> GetAbsence(string strFrom, string strTo)
		{
			var service = new DashboardServices();
			var resultData = service.GetAbsence(out bool success, strFrom, strTo);
            var list = new List<DashboardAbsence>();
            if (success)
			{
				string response = resultData.data.ToString();
                list = JsonConvert.DeserializeObject<List<DashboardAbsence>>(response, _settings);
                return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

        public List<DashboardFrequency> GetFrequency(string strFrom, string strTo)
        {
            var service = new DashboardServices();
            var resultData = service.GetFrequency(out bool success, strFrom, strTo);

            if (success)
            {
                string response = resultData.data.ToString();
                var list = JsonConvert.DeserializeObject<List<DashboardFrequency>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public List<DashboardCategory> GetCategory(string strFrom, string strTo, string strType)
        {
            var service = new DashboardServices();
            var resultData = service.GetCategory(out bool success, strFrom, strTo, strType);

            if (success)
            {
                string response = resultData.data.ToString();
                var list = JsonConvert.DeserializeObject<List<DashboardCategory>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }
        public List<DashboardMayor> GetMayor(string strFrom, string strTo)
        {
            var service = new DashboardServices();
            var resultData = service.GetMayor(out bool success, strFrom, strTo);

            if (success)
            {
                string response = resultData.data.ToString();
                var list = JsonConvert.DeserializeObject<List<DashboardMayor>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public List<DashboardTrend> GetTrend(string strFrom, string strTo)
        {
            var service = new DashboardServices();
            var resultData = service.GetTrend(out bool success, strFrom, strTo);

            if (success)
            {
                string response = resultData.data.ToString();
                var list = JsonConvert.DeserializeObject<List<DashboardTrend>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public List<DashboardAppointments> GetAppointments(string strFrom, string strTo)
        {
            var service = new DashboardServices();
            var resultData = service.GetAppointments(out bool success, strFrom, strTo);
            var list = new List<DashboardAppointments>();
            if (success)
            {
                string response = resultData.data.ToString();
                list = JsonConvert.DeserializeObject<List<DashboardAppointments>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }
    }
}

