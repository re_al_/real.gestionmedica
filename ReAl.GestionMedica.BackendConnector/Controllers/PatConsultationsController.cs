#region 
/***********************************************************************************************************
	NOMBRE:       PatConsultations
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla pat_consultations

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        06/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Controllers
{
	public class PatConsultationsController
	{
        private readonly JsonSerializerSettings _settings;
        public PatConsultationsController()
        {
            _settings = new JsonSerializerSettings()
            {
                ////ContractResolver = new UnderscorePropertyNamesContractResolver()
            };
        }

        public List<PatConsultations> GetAll()
		{
			var service = new PatConsultationsServices();
			var resultData = service.GetAll(out bool success);
			var list = new List<PatConsultations>();
			if (success)
			{
				string response = resultData.data.ToString();
				list = JsonConvert.DeserializeObject<List<PatConsultations>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public List<PatConsultations> GetActive()
		{
			var service = new PatConsultationsServices();
			var resultData = service.GetActive(out bool success);
			var list = new List<PatConsultations>();
			if (success)
			{
				string response = resultData.data.ToString();
				list = JsonConvert.DeserializeObject<List<PatConsultations>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}
        public List<PatConsultations> GetByPatientId(long idPatients)
        {
            var service = new PatConsultationsServices();
            var resultData = service.GetByPatientId(out bool success, idPatients);
            var list = new List<PatConsultations>();
            if (success)
            {
                string response = resultData.data.ToString();
                list = JsonConvert.DeserializeObject<List<PatConsultations>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public PatConsultations GetById(long id)
		{
			var service = new PatConsultationsServices();
			var resultData = service.GetById(out bool success, id);
			var obj = new PatConsultations();
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<PatConsultations>(response, _settings);
				return obj;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public bool Create(PatConsultations obj)
		{
			var service = new PatConsultationsServices();
			var resultData = service.Create(out bool success, obj);
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<PatConsultations>(response, _settings);
				return obj != null;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public bool Update(PatConsultations obj)
		{
			var service = new PatConsultationsServices();
			var resultData = service.Update(out bool success, obj);
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<PatConsultations>(response, _settings);
				return obj != null;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public void Delete(Int64 id)
		{
			var service = new PatConsultationsServices();
			var resultData = service.Delete(out bool success, id);
			if (!success)
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

	}
}

