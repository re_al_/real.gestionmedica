﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Windows.Forms;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
using ReAl.GestionMedica.BackendConnector.Utils;

namespace ReAl.GestionMedica.BackendConnector.Controller
{
    public class ClaBloodtypesController
    {
        private readonly JsonSerializerSettings _settings;
        public ClaBloodtypesController()
        {
            _settings = new JsonSerializerSettings()
            {
                //ContractResolver = new UnderscorePropertyNamesContractResolver()
            };
        }

        public List<ClaBloodtypes> GetAll()
        {
            var service = new ClaBloodtypesServices();
            var resultData = service.GetAll(out bool success);
            var list = new List<ClaBloodtypes>();
            if (success)
            {
                string response = resultData.data.ToString();
                list = JsonConvert.DeserializeObject<List<ClaBloodtypes>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public List<ClaBloodtypes> GetActive()
        {
            var service = new ClaBloodtypesServices();
            var resultData = service.GetActive(out bool success);
            var list = new List<ClaBloodtypes>();
            if (success)
            {
                string response = resultData.data.ToString();
                list = JsonConvert.DeserializeObject<List<ClaBloodtypes>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public ClaBloodtypes GetById(long id)
        {
            var service = new ClaBloodtypesServices();
            var resultData = service.GetById(out bool success, id);
            var obj = new ClaBloodtypes();
            if (success)
            {
                string response = resultData.data.ToString();
                obj = JsonConvert.DeserializeObject<ClaBloodtypes>(response, _settings);
                return obj;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public bool Create(ClaBloodtypes obj)
        {
            var service = new ClaBloodtypesServices();
            var resultData = service.Create(out bool success, obj);
            if (success)
            {
                string response = resultData.data.ToString();
                obj = JsonConvert.DeserializeObject<ClaBloodtypes>(response, _settings);
                return obj != null;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public bool Update(ClaBloodtypes obj)
        {
            var service = new ClaBloodtypesServices();
            var resultData = service.Update(out bool success, obj);
            if (success)
            {
                string response = resultData.data.ToString();
                obj = JsonConvert.DeserializeObject<ClaBloodtypes>(response, _settings);
                return obj != null;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public void Delete(Int64 id)
        {
            var service = new ClaBloodtypesServices();
            var resultData = service.Delete(out bool success, id);
            if (!success)
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }
    }
}
