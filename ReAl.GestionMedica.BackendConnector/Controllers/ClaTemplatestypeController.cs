#region 
/***********************************************************************************************************
	NOMBRE:       ClaTemplatestype
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla cla_templates_type

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        06/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Controllers
{
	public class ClaTemplatestypeController
	{
        private readonly JsonSerializerSettings _settings;
        public ClaTemplatestypeController()
        {
            _settings = new JsonSerializerSettings()
            {
                //ContractResolver = new UnderscorePropertyNamesContractResolver()
            };
        }

        public List<ClaTemplatestype> GetAll()
		{
			var service = new ClaTemplatestypeServices();
			var resultData = service.GetAll(out bool success);
			var list = new List<ClaTemplatestype>();
			if (success)
			{
				string response = resultData.data.ToString();
				list = JsonConvert.DeserializeObject<List<ClaTemplatestype>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public List<ClaTemplatestype> GetActive()
		{
			var service = new ClaTemplatestypeServices();
			var resultData = service.GetActive(out bool success);
			var list = new List<ClaTemplatestype>();
			if (success)
			{
				string response = resultData.data.ToString();
				list = JsonConvert.DeserializeObject<List<ClaTemplatestype>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public ClaTemplatestype GetById(long id)
		{
			var service = new ClaTemplatestypeServices();
			var resultData = service.GetById(out bool success, id);
			var obj = new ClaTemplatestype();
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<ClaTemplatestype>(response, _settings);
				return obj;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public bool Create(ClaTemplatestype obj)
		{
			var service = new ClaTemplatestypeServices();
			var resultData = service.Create(out bool success, obj);
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<ClaTemplatestype>(response, _settings);
				return obj != null;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public bool Update(ClaTemplatestype obj)
		{
			var service = new ClaTemplatestypeServices();
			var resultData = service.Update(out bool success, obj);
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<ClaTemplatestype>(response, _settings);
				return obj != null;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public void Delete(Int64 id)
		{
			var service = new ClaTemplatestypeServices();
			var resultData = service.Delete(out bool success, id);
			if (!success)
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

	}
}

