#region 
/***********************************************************************************************************
	NOMBRE:       ClaTemplates
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla cla_templates

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        06/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Controllers
{
	public class ClaTemplatesController
	{
        private readonly JsonSerializerSettings _settings;
        public ClaTemplatesController()
        {
            _settings = new JsonSerializerSettings()
            {
                ////ContractResolver = new UnderscorePropertyNamesContractResolver()
            };
        }

        public List<ClaTemplates> GetAll()
		{
			var service = new ClaTemplatesServices();
			var resultData = service.GetAll(out bool success);
			var list = new List<ClaTemplates>();
			if (success)
			{
				string response = resultData.data.ToString();
				list = JsonConvert.DeserializeObject<List<ClaTemplates>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public List<ClaTemplates> GetActive()
		{
			var service = new ClaTemplatesServices();
			var resultData = service.GetActive(out bool success);
			var list = new List<ClaTemplates>();
			if (success)
			{
				string response = resultData.data.ToString();
				list = JsonConvert.DeserializeObject<List<ClaTemplates>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

        public List<ClaTemplates> GetByTemplateTypeId(long id)
        {
            var service = new ClaTemplatesServices();
            var resultData = service.GetByTemplateTypeId(out bool success, id);
            var list = new List<ClaTemplates>();
            if (success)
            {
                string response = resultData.data.ToString();
                list = JsonConvert.DeserializeObject<List<ClaTemplates>>(response, _settings);
                return list;
            }
            else
            {
                string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
                throw new ApplicationException(messageText);
            }
        }

        public ClaTemplates GetById(long id)
		{
			var service = new ClaTemplatesServices();
			var resultData = service.GetById(out bool success, id);
			var obj = new ClaTemplates();
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<ClaTemplates>(response, _settings);
				return obj;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

        public bool Create(ClaTemplates obj)
		{
			var service = new ClaTemplatesServices();
			var resultData = service.Create(out bool success, obj);
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<ClaTemplates>(response, _settings);
				return obj != null;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public bool Update(ClaTemplates obj)
		{
			var service = new ClaTemplatesServices();
			var resultData = service.Update(out bool success, obj);
			if (success)
			{
				string response = resultData.data.ToString();
				obj = JsonConvert.DeserializeObject<ClaTemplates>(response, _settings);
				return obj != null;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public void Delete(Int64 id)
		{
			var service = new ClaTemplatesServices();
			var resultData = service.Delete(out bool success, id);
			if (!success)
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

	}
}

