#region 
/***********************************************************************************************************
	NOMBRE:       SegUsersrefreshtoken
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla seg_users_refresh_token

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        06/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.BackendConnector.Services;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Controllers
{
	public class SegUsersrefreshtokenController
	{
		
		public List<SegUsersrefreshtoken> getAll()
		{
			var service = new SegUsersrefreshtokenServices();
			var resultData = service.GetAll(out bool success);
			var list = new List<SegUsersrefreshtoken>();
			if (success)
			{
				string response = resultData.data.elements.ToString();
				list = JsonConvert.DeserializeObject<List<SegUsersrefreshtoken>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public List<SegUsersrefreshtoken> getActive()
		{
			var service = new SegUsersrefreshtokenServices();
			var resultData = service.GetActive(out bool success);
			var list = new List<SegUsersrefreshtoken>();
			if (success)
			{
				string response = resultData.data.elements.ToString();
				list = JsonConvert.DeserializeObject<List<SegUsersrefreshtoken>>(response, _settings);
				return list;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public SegUsersrefreshtoken GetById(long id)
		{
			var service = new SegUsersrefreshtokenServices();
			var resultData = service.GetById(out bool success, id);
			var obj = new SegUsersrefreshtoken();
			if (success)
			{
				string response = resultData.data.elements.ToString();
				obj = JsonConvert.DeserializeObject<SegUsersrefreshtoken>(response, _settings);
				return obj;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public bool Create(SegUsersrefreshtoken obj)
		{
			var service = new SegUsersrefreshtokenServices();
			var resultData = service.Create(out bool success, obj);
			if (success)
			{
				string response = resultData.data.elements.ToString();
				obj = JsonConvert.DeserializeObject<SegUsersrefreshtoken>(response, _settings);
				return obj != null;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public bool Update(SegUsersrefreshtoken obj)
		{
			var service = new SegUsersrefreshtokenServices();
			var resultData = service.Update(out bool success, obj);
			if (success)
			{
				string response = resultData.data.elements.ToString();
				obj = JsonConvert.DeserializeObject<SegUsersrefreshtoken>(response, _settings);
				return obj != null;
			}
			else
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

		public void Delete(Int64 id)
		{
			var service = new SegUsersrefreshtokenServices();
			var resultData = service.Delete(out bool success, id);
			if (!success)
			{
				string messageText = Utils.ErrorHandler.GetMessageFromResponse(resultData);
				throw new ApplicationException(messageText);
			}
		}

	}
}

