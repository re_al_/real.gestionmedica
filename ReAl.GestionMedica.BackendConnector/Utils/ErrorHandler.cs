﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Extensions.Logging;

namespace ReAl.GestionMedica.BackendConnector.Utils
{
    public static class ErrorHandler
    {
        public static string GetMessageFromResponse(dynamic response, string defaultMessage = "", bool debugMode = true)
        {
            var messageText = String.Empty;
            var strMessage = new StringBuilder();
            if (response != null)
            {
                if (response is string)
                {
                    if (response.ToString().Equals("HTTP 0"))
                    {
                        strMessage.AppendLine("No se puede acceder al host");
                    }
                    else
                    {
                        strMessage.AppendLine(response
                            .Replace("No such host is known", "No se puede acceder al host"));
                    }
                }
                else
                {
                    if (response.originalMessage != null)
                    {
                        if (response.originalMessage != "OK")
                        {
                            strMessage.AppendLine(response.originalMessage.ToString());
                            if (response.exceptionId != null)
                                strMessage.AppendLine("Código: " + response.exceptionId.ToString());
                        }
                    }
                    else if (response.responseMessage != null)
                    {
                        if (response.responseCode != "OK")
                        {
                            strMessage.AppendLine(response.responseMessage.ToString());
                            if (response.exceptionId != null)
                                strMessage.AppendLine("Código: " + response.exceptionId.ToString());
                        }
                    }
                    else if (response.message != null)
                    {
                        if (response.message.ToString().Contains("upstream server"))
                        {
                            strMessage.AppendLine("Ha ocurrido un error inesperado al comunicarse con el servidor. Por favor, vuelva a intentar la operación");
                        }
                        else if (response.message.ToString().Contains("failure to get a peer from the ring-balancer"))
                        {
                            strMessage.AppendLine("Ha ocurrido un error inesperado al comunicarse con el servidor (UNAVAILABLE). Por favor, vuelva a intentar la operación");
                        }
                        else
                        {
                            strMessage.AppendLine(response.message.ToString());
                        }
                    }
                    else
                    {
                        if (defaultMessage == "")
                            strMessage.AppendLine("Ha ocurrido un error inesperado");
                    }
                }
            }

            if (debugMode)
                if (response is not string)
                {
                    if (response != null && response.developerMessage != null)
                    {
                        strMessage.AppendLine("");
                        strMessage.AppendLine("Debug: " + response.developerMessage.ToString());
                    }

                    if (response != null && response.httpStatus != null && response.httpStatus == "500" &&
                        response.stackTrace != null)
                        strMessage.AppendLine("StackTrace: " + response.stackTrace.ToString());
                }

            messageText = strMessage.ToString() == "" ? defaultMessage : strMessage.ToString();
            return messageText;
        }
    }
}
