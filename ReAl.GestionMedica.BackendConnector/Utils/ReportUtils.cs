﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReAl.GestionMedica.BackendConnector.Utils
{
    internal class ReportUtils
    {
        public static DataTable DeserializeToDataTable(string jsource)
        {
            var jRootObject = JObject.Parse(jsource);
            var children = jRootObject.SelectTokens("$..data.*").ToArray();
            var records = children.OfType<JObject>().ToArray();
            var dicList = new List<Dictionary<string, object>>();
            foreach (var rec in records)
            {
                dicList.Add(DeserializeToDictionary(rec));
            }
            var fieldnames = dicList.SelectMany(d => d.Keys).Distinct().ToArray();
            var dt = new DataTable();
            foreach (var fieldname in fieldnames)
            {
                dt.Columns.Add(fieldname, typeof(object));
            }
            DataRow row;
            foreach (var dic in dicList)
            {
                row = dt.NewRow();
                foreach (var kvp in dic)
                {
                    row.SetField(kvp.Key, kvp.Value);
                }
                dt.Rows.Add(row);
            }
            return dt;
        }

        private static Dictionary<string, object> DeserializeToDictionary(JObject jsonObject)
        {
            var dic = new Dictionary<string, object>();
            foreach (var field in jsonObject.Properties())
            {
                switch (field.Value.Type)
                {
                    case JTokenType.Array:
                        var subobject = new JObject();
                        var item = 0;
                        foreach (var token in field.Value)
                        {
                            subobject["item" + item] = token;
                            item += 1;
                        }
                        var subdic = DeserializeToDictionary(subobject);
                        foreach (var kvp in subdic)
                        {
                            dic[kvp.Key] = kvp.Value;
                        }
                        break;
                    case JTokenType.Boolean:
                        dic[field.Name] = field.Value.ToObject<bool>();
                        break;
                    case JTokenType.Bytes:
                        dic[field.Name] = field.Value.ToObject<byte[]>();
                        break;
                    case JTokenType.Date:
                        dic[field.Name] = field.Value.ToObject<DateTime>();
                        break;
                    case JTokenType.Float:
                        dic[field.Name] = field.Value.ToObject<double>();
                        break;
                    case JTokenType.Guid:
                        dic[field.Name] = field.Value.ToObject<Guid>();
                        break;
                    case JTokenType.Integer:
                        dic[field.Name] = field.Value.ToObject<int>();
                        break;
                    case JTokenType.Object:
                        var subdic2 = DeserializeToDictionary((JObject)field.Value);
                        foreach (var kvp in subdic2)
                        {
                            dic[kvp.Key] = kvp.Value;
                        }
                        break;
                    case JTokenType.String:
                        try
                        {
                            dic[field.Name] = field.Value.ToObject<string>();
                        }
                        catch (Exception ex)
                        {
                            dic[field.Name] = field.Value.ToObject<object>();
                        }
                        break;
                    case JTokenType.TimeSpan:
                        dic[field.Name] = field.Value.ToObject<TimeSpan>();
                        break;
                    default:
                        dic[field.Name] = field.Value.ToString();
                        break;
                }
            }
            return dic;
        }
    }
}
