﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace ReAl.GestionMedica.BackendConnector.Utils
{
    internal static class EncryptUtils
    {
        internal static string EncryptStringAes(string plainText, string secret)
        {
            byte[] key = Encoding.UTF8.GetBytes(secret);
            var sha1 = SHA1.Create();
            key = sha1.ComputeHash(key);

            sbyte[] signedKey = new sbyte[key.Length];
            //Convert to unsigned:
            for (int i = 0; i < key.Length; i++)
                if (key[i] >= 128)
                    signedKey[i] = (sbyte)(key[i] - 256);
                else
                    signedKey[i] = (sbyte)key[i];
            key = key.Take(16).ToArray();

            byte[] iv = new byte[16];

            using Aes aesAlg = Aes.Create();
            aesAlg.Key = key;
            aesAlg.IV = iv;
            aesAlg.Mode = CipherMode.CBC;
            aesAlg.Padding = PaddingMode.PKCS7;

#pragma warning disable S3329 // Cipher Block Chaining IVs should be unpredictable
            var encryptor = aesAlg.CreateEncryptor(key, aesAlg.IV);
#pragma warning restore S3329 // Cipher Block Chaining IVs should be unpredictable

            using var msEncrypt = new MemoryStream();
            using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write, true))
            {
                // just write to stream directly
                csEncrypt.Write(Encoding.UTF8.GetBytes(plainText),0, Encoding.UTF8.GetBytes(plainText).Length);
            }

            // read here, so that we are sure writing is finished
            var encrypted = msEncrypt.ToArray();

            StringBuilder sb = new StringBuilder();
            //sb.Append(Encoding.UTF8.GetString(IV));

            string encodedString = Convert.ToBase64String(encrypted);

            sb.Append(encodedString);
            //Console.WriteLine(sb.ToString());
            return sb.ToString();
        }
    }
}