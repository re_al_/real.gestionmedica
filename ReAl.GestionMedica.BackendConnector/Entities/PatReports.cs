#region 
/***********************************************************************************************************
	NOMBRE:       PatReports
	DESCRIPCION:
		Clase que define un objeto para la Tabla pat_reports

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        05/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Utils;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Entities
{
	[Table("pat_reports")]
	public class PatReports
	{
		public const string StrNombreTabla = "Pat_reports";
		public const string StrAliasTabla = "pat_reports";
		public const string StrDescripcionTabla = "pat_reports";
		public const string StrNombreLisForm = "Listado de pat_reports";
		public const string StrNombreDocForm = "Detalle de pat_reports";
		public const string StrNombreForm = "Registro de pat_reports";
		public enum Fields
		{
			IdReports
			,IdWorkplaces
			,IdPatients
			,IdTemplates
			,ReportDate
			,TextFormat
			,RtfFormat
			,HtmlFormat
			,ApiStatus
			,ApiTransaction
			,ApiUsucre
			,ApiFeccre
			,ApiUsumod
			,ApiFecmod
			,ApiUsudel
			,ApiFecdel

		}
		
		#region Constructoress
		
		public PatReports()
		{
			//Inicializacion de Variables
			TextFormat = null;
			RtfFormat = null;
			HtmlFormat = null;
			ApiStatus = null;
			ApiTransaction = null;
			ApiUsucre = null;
			ApiUsumod = null;
			ApiFecmod = null;
			ApiUsudel = null;
			ApiFecdel = null;
		}
		
		public PatReports(PatReports obj)
		{
			IdReports = obj.IdReports;
			IdWorkplaces = obj.IdWorkplaces;
			IdPatients = obj.IdPatients;
			IdTemplates = obj.IdTemplates;
			ReportDate = obj.ReportDate;
			TextFormat = obj.TextFormat;
			RtfFormat = obj.RtfFormat;
			HtmlFormat = obj.HtmlFormat;
			ApiStatus = obj.ApiStatus;
			ApiTransaction = obj.ApiTransaction;
			ApiUsucre = obj.ApiUsucre;
			ApiFeccre = obj.ApiFeccre;
			ApiUsumod = obj.ApiUsumod;
			ApiFecmod = obj.ApiFecmod;
			ApiUsudel = obj.ApiUsudel;
			ApiFecdel = obj.ApiFecdel;
		}
		
		#endregion
		
		#region Metodos Estaticos
		#endregion
		
		#region Metodos Privados
		
		/// <summary>
		/// Obtiene el Hash a partir de un array de Bytes
		/// </summary>
		/// <param name="objectAsBytes"></param>
		/// <returns>string</returns>
		private string ComputeHash(byte[] objectAsBytes)
		{
			MD5 md5 = new MD5CryptoServiceProvider();
			try
			{
				byte[] result = md5.ComputeHash(objectAsBytes);
				
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < result.Length; i++)
				{
					sb.Append(result[i].ToString("X2"));
				}
				
				return sb.ToString();
			}
			catch (ArgumentNullException ane)
			{
				return null;
			}
		}
		
		/// <summary>
		///     Obtienen el Hash basado en algun algoritmo de Encriptación
		/// </summary>
		/// <typeparam name="T">
		///     Algoritmo de encriptación
		/// </typeparam>
		/// <param name="cryptoServiceProvider">
		///     Provvedor de Servicios de Criptografía
		/// </param>
		/// <returns>
		///     String que representa el Hash calculado
		        /// </returns>
		private string ComputeHash<T>( T cryptoServiceProvider) where T : HashAlgorithm, new()
		{
			DataContractSerializer serializer = new DataContractSerializer(this.GetType());
			using (MemoryStream memoryStream = new MemoryStream())
			{
				serializer.WriteObject(memoryStream, this);
				cryptoServiceProvider.ComputeHash(memoryStream.ToArray());
				return Convert.ToBase64String(cryptoServiceProvider.Hash);
			}
		}
		
		#endregion
		
		#region Overrides
		
		/// <summary>
		/// Devuelve un String que representa al Objeto
		/// </summary>
		/// <returns>string</returns>
		public override string ToString()
		{
			string hashString;
			
			//Evitar parametros NULL
			if (this == null)
				throw new ArgumentNullException("Parametro NULL no es valido");
			
			//Se verifica si el objeto es serializable.
			try
			{
				MemoryStream memStream = new MemoryStream();
				XmlSerializer serializer = new XmlSerializer(typeof(PatReports));
				serializer.Serialize(memStream, this);
				
				//Ahora se obtiene el Hash del Objeto.
				hashString = ComputeHash(memStream.ToArray());
				
				return hashString;
			}
			catch (AmbiguousMatchException ame)
			{
				throw new ApplicationException("El Objeto no es Serializable. Message:" + ame.Message);
			}
		}
		
		/// <summary>
		/// Verifica que dos objetos sean identicos
		/// </summary>
		public static bool operator ==(PatReports first, PatReports second)
		{
			// Verifica si el puntero en memoria es el mismo
			if (Object.ReferenceEquals(first, second))
				return true;
			
			// Verifica valores nulos
			if ((object) first == null || (object) second == null)
				return false;
			
			return first.GetHashCode() == second.GetHashCode();
		}
		
		/// <summary>
		/// Verifica que dos objetos sean distintos
		/// </summary>
		public static bool operator !=(PatReports first, PatReports second)
		{
			return !(first == second);
		}
		
		/// <summary>
		/// Compara este objeto con otro
		/// </summary>
		/// <param name="obj">El objeto a comparar</param>
		/// <returns>Devuelve Verdadero si ambos objetos son iguales</returns>
		public override bool Equals(object obj)
		{
			if (obj == null)
				return false;
			
			if (obj.GetType() == this.GetType())
				return obj.GetHashCode() == this.GetHashCode();
			
			return false;
		}
		
		#endregion
		
		#region Encriptacion
		
		/// <summary>
		/// Un valor unico que identifica cada instancia de este objeto
		/// </summary>
		/// <returns>Unico valor entero</returns>
		public override int GetHashCode()
		{
			return (this.GetSha512Hash().GetHashCode());
		}
		
		/// <summary>
		///     Obtiene el Hash de la Instancia Actual
		/// </summary>
		/// <typeparam name="T">
		///     Tipo de Proveedor de servicios criptográficos
		/// </typeparam>
		/// <returns>
		///     String en Base64 que representa el Hash
		/// </returns>
		public string GetHash<T>() where T : HashAlgorithm, new()
		{
			T cryptoServiceProvider = new T();
			return ComputeHash(cryptoServiceProvider);
		}
		
		/// <summary>
		///     Obtiene el codigo Hash en Base al Algoritmo SHA1
		/// </summary>
		/// <returns>
		///     String en Base64 que representa el Hash en SHA1
		/// </returns>
		public string GetSha1Hash()
		{
			return GetHash<SHA1CryptoServiceProvider>();
		}
		
		/// <summary>
		///     Obtiene el codigo Hash en Base al Algoritmo SHA512
		/// </summary>
		/// <returns>
		///     String en Base64 que representa el Hash en SHA512
		/// </returns>
		public string GetSha512Hash()
		{
			return GetHash<SHA512CryptoServiceProvider>();
		}
		
		/// <summary>
		///     Obtiene el codigo Hash en Base al Algoritmo MD5
		/// </summary>
		/// <returns>
		///     String en Base64 que representa el Hash en MD5
		/// </returns>
		public string GetMd5Hash()
		{
			return GetHash<MD5CryptoServiceProvider>();
		}
		
		
		#endregion
		
		#region DataAnnotations
		
		/// <summary>
		/// Obtiene los errores basado en los DataAnnotations 
		/// </summary>
		/// <returns>Devuelve un IList del tipo ValidationResult con los errores obtenidos</returns>
		public IList<ValidationResult> ValidationErrors()
		{
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			if (!Validator.TryValidateObject(this, context, errors, true))
				return errors;
			
			return new List<ValidationResult>(0);
		}
		
		/// <summary>
		/// Obtiene los errores basado en los DataAnnotations 
		/// </summary>
		/// <returns>Devuelve un String con los errores obtenidos</returns>
		public string ValidationErrorsString()
		{
			string strErrors = "";
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			if (!Validator.TryValidateObject(this, context, errors, true))
			{
				foreach (ValidationResult result in errors)
					strErrors = strErrors + result.ErrorMessage + Environment.NewLine;
			}
			return strErrors;
		}
		
		/// <summary>
		/// Funcion que determina si un objeto es valido o no
		/// </summary>
		/// <returns>Resultado de la validacion</returns>
		public bool IsValid()
		{
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			return Validator.TryValidateObject(this, context, errors, true);
		}
		
		#endregion
		
		/// <summary>
		/// Entity identifier
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_reports", 
            Description = "Entity identifier",
            Order = -1)]
        [JsonProperty(PropertyName = "id_reports")]
        [Required(ErrorMessage = "Se necesita un valor para -id_reports- porque es un campo requerido.")]
		[Key]
		public Int64 IdReports { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna id_workplaces de la Tabla pat_reports
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_workplaces", 
            Description = " Propiedad publica de tipo Int64 que representa a la columna id_workplaces de la Tabla pat_reports",
            Order = -1)]
        [JsonProperty(PropertyName = "id_workplaces")]
        [Required(ErrorMessage = "Se necesita un valor para -id_workplaces- porque es un campo requerido.")]
		public Int64 IdWorkplaces { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna id_patients de la Tabla pat_reports
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_patients", 
            Description = " Propiedad publica de tipo Int64 que representa a la columna id_patients de la Tabla pat_reports",
            Order = -1)]
        [JsonProperty(PropertyName = "id_patients")]
        [Required(ErrorMessage = "Se necesita un valor para -id_patients- porque es un campo requerido.")]
		public Int64 IdPatients { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna id_templates de la Tabla pat_reports
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_templates", 
            Description = " Propiedad publica de tipo Int64 que representa a la columna id_templates de la Tabla pat_reports",
            Order = -1)]
        [JsonProperty(PropertyName = "id_templates")]
        [Required(ErrorMessage = "Se necesita un valor para -id_templates- porque es un campo requerido.")]
		public Int64 IdTemplates { get; set; } 

		/// <summary>
		/// Report date
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "Fecha", 
            Description = "Report date",
            Order = 2)]
        [JsonProperty(PropertyName = "report_date")]
        [Required(ErrorMessage = "Se necesita un valor para -report_date- porque es un campo requerido.")]
        //[JsonConverter(typeof(UnixMillisecondsConverter))]
        public DateTime ReportDate { get; set; } 

		/// <summary>
		/// Report content in text format
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(50000, MinimumLength=0)]
		[Display(Name = "Reporte", 
            Description = "Report content in text format",
            Order = -1)]
        [JsonProperty(PropertyName = "text_format")]
        public string TextFormat { get; set; } 

		/// <summary>
		/// Report content in RTF format
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "rtf_format", 
            Description = "Report content in RTF format",
            Order = -1)]
        [JsonProperty(PropertyName = "rtf_format")]
        public string RtfFormat { get; set; } 

		/// <summary>
		/// Report content in HTML format
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100000, MinimumLength=0)]
		[Display(Name = "Informe", 
            Description = "Report content in HTML format",
            Order = 2)]
        [JsonProperty(PropertyName = "html_format")]
        [Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -html_format- porque es un campo requerido.")]
		public string HtmlFormat { get; set; } 

		/// <summary>
		/// Current status
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_status", 
            Description = "Current status",
            Order = -1)]
        [JsonProperty(PropertyName = "api_status")]
        [Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -api_status- porque es un campo requerido.")]
		public string ApiStatus { get; set; } 

		/// <summary>
		/// Last transaction used
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(40, MinimumLength=0)]
		[Display(Name = "api_transaction", 
            Description = "Last transaction used",
            Order = -1)]
        [JsonProperty(PropertyName = "api_transaction")]
        [Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -api_transaction- porque es un campo requerido.")]
		public string ApiTransaction { get; set; } 

		/// <summary>
		/// User creation
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_usucre", 
            Description = "User creation",
            Order = -1)]
        [JsonProperty(PropertyName = "api_usucre")]
        public string ApiUsucre { get; set; } 

		/// <summary>
		/// Timestamp creation
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "api_feccre", 
            Description = "Timestamp creation",
            Order = -1)]
        [JsonProperty(PropertyName = "api_feccre")]
        //[JsonConverter(typeof(UnixMillisecondsConverter))]
        public DateTime ApiFeccre { get; set; } 

		/// <summary>
		/// User update
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_usumod", 
            Description = "User update",
            Order = -1)]
        [JsonProperty(PropertyName = "api_usumod")]
        public string ApiUsumod { get; set; } 

		/// <summary>
		/// Timestamp update
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "api_fecmod", 
            Description = "Timestamp update",
            Order = -1)]
        [JsonProperty(PropertyName = "api_fecmod")]
        //[JsonConverter(typeof(UnixMillisecondsConverter))]
        public DateTime? ApiFecmod { get; set; } 

		/// <summary>
		/// User delete
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_usudel", 
            Description = "User delete",
            Order = -1)]
        [JsonProperty(PropertyName = "api_usudel")]
        public string ApiUsudel { get; set; } 

		/// <summary>
		/// Timestamp delete
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "api_fecdel", 
            Description = "Timestamp delete", 
            Order = -1)] 
        [JsonProperty(PropertyName = "api_fecdel")]
        //[JsonConverter(typeof(UnixMillisecondsConverter))]
        public DateTime? ApiFecdel { get; set; } 

	}
}

