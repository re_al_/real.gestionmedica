﻿


using Newtonsoft.Json;

namespace ReAl.GestionMedica.BackendConnector.Entities
{
    public class Login
    {
        [JsonProperty(PropertyName = "token")]
        public string Token { get; set; }

        [JsonProperty(PropertyName = "refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty(PropertyName = "token_type")]
        public string TokenType { get; set; }
    }
}
