#region 
/***********************************************************************************************************
	NOMBRE:       PatPatients
	DESCRIPCION:
		Clase que define un objeto para la Tabla pat_patients

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        05/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Utils;

#endregion

namespace ReAl.GestionMedica.BackendConnector.Entities
{
	[Table("pat_patients")]
	public class PatPatients
	{
		public const string StrNombreTabla = "Pat_patients";
		public const string StrAliasTabla = "pat_patients";
		public const string StrDescripcionTabla = "pat_patients";
		public const string StrNombreLisForm = "Listado de pat_patients";
		public const string StrNombreDocForm = "Detalle de pat_patients";
		public const string StrNombreForm = "Registro de pat_patients";
		public enum Fields
		{
			IdPatients
			,IdentityNumber
			,FirstName
			,LastName
			,FullName
			,Gender
			,Birthdate
			,IdBloodTypes
			,IdMarital
			,Occupation
			,Origin
			,Address
			,Dominance
			,Phone
			,Email
			,Religion
			,Company
			,Insurance
			,Refers
			,Informant
			,ApiStatus
			,ApiTransaction
			,ApiUsucre
			,ApiFeccre
			,ApiUsumod
			,ApiFecmod
			,ApiUsudel
			,ApiFecdel

		}
		
		#region Constructoress
		
		public PatPatients()
		{
			//Inicializacion de Variables
			IdentityNumber = null;
			FirstName = null;
			LastName = null;
			//gender = false;
            Occupation = null;
			Origin = null;
			Address = null;
			Dominance = null;
			Phone = null;
			Email = null;
			Religion = null;
			Company = null;
			Insurance = null;
			Refers = null;
			Informant = null;
			ApiStatus = null;
			ApiTransaction = null;
			ApiUsucre = null;
			ApiUsumod = null;
			ApiFecmod = null;
			ApiUsudel = null;
			ApiFecdel = null;
		}
		
		public PatPatients(PatPatients obj)
		{
			IdPatients = obj.IdPatients;
			IdentityNumber = obj.IdentityNumber;
			FirstName = obj.FirstName;
			LastName = obj.LastName;
			Gender = obj.Gender;
			Birthdate = obj.Birthdate;
			IdBloodTypes = obj.IdBloodTypes;
			IdMarital = obj.IdMarital;
			Occupation = obj.Occupation;
			Origin = obj.Origin;
			Address = obj.Address;
			Dominance = obj.Dominance;
			Phone = obj.Phone;
			Email = obj.Email;
			Religion = obj.Religion;
			Company = obj.Company;
			Insurance = obj.Insurance;
			Refers = obj.Refers;
			Informant = obj.Informant;
			ApiStatus = obj.ApiStatus;
			ApiTransaction = obj.ApiTransaction;
			ApiUsucre = obj.ApiUsucre;
			ApiFeccre = obj.ApiFeccre;
			ApiUsumod = obj.ApiUsumod;
			ApiFecmod = obj.ApiFecmod;
			ApiUsudel = obj.ApiUsudel;
			ApiFecdel = obj.ApiFecdel;
		}
		
		#endregion
		
		#region Metodos Estaticos
		#endregion
		
		#region Metodos Privados
		
		/// <summary>
		/// Obtiene el Hash a partir de un array de Bytes
		/// </summary>
		/// <param name="objectAsBytes"></param>
		/// <returns>string</returns>
		private string ComputeHash(byte[] objectAsBytes)
		{
			MD5 md5 = new MD5CryptoServiceProvider();
			try
			{
				byte[] result = md5.ComputeHash(objectAsBytes);
				
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < result.Length; i++)
				{
					sb.Append(result[i].ToString("X2"));
				}
				
				return sb.ToString();
			}
			catch (ArgumentNullException ane)
			{
				return null;
			}
		}
		
		/// <summary>
		///     Obtienen el Hash basado en algun algoritmo de Encriptación
		/// </summary>
		/// <typeparam name="T">
		///     Algoritmo de encriptación
		/// </typeparam>
		/// <param name="cryptoServiceProvider">
		///     Provvedor de Servicios de Criptografía
		/// </param>
		/// <returns>
		///     String que representa el Hash calculado
		        /// </returns>
		private string ComputeHash<T>( T cryptoServiceProvider) where T : HashAlgorithm, new()
		{
			DataContractSerializer serializer = new DataContractSerializer(this.GetType());
			using (MemoryStream memoryStream = new MemoryStream())
			{
				serializer.WriteObject(memoryStream, this);
				cryptoServiceProvider.ComputeHash(memoryStream.ToArray());
				return Convert.ToBase64String(cryptoServiceProvider.Hash);
			}
		}
		
		#endregion
		
		#region Overrides
		
		/// <summary>
		/// Devuelve un String que representa al Objeto
		/// </summary>
		/// <returns>string</returns>
		public override string ToString()
		{
			string hashString;
			
			//Evitar parametros NULL
			if (this == null)
				throw new ArgumentNullException("Parametro NULL no es valido");
			
			//Se verifica si el objeto es serializable.
			try
			{
				MemoryStream memStream = new MemoryStream();
				XmlSerializer serializer = new XmlSerializer(typeof(PatPatients));
				serializer.Serialize(memStream, this);
				
				//Ahora se obtiene el Hash del Objeto.
				hashString = ComputeHash(memStream.ToArray());
				
				return hashString;
			}
			catch (AmbiguousMatchException ame)
			{
				throw new ApplicationException("El Objeto no es Serializable. Message:" + ame.Message);
			}
		}
		
		/// <summary>
		/// Verifica que dos objetos sean identicos
		/// </summary>
		public static bool operator ==(PatPatients first, PatPatients second)
		{
			// Verifica si el puntero en memoria es el mismo
			if (Object.ReferenceEquals(first, second))
				return true;
			
			// Verifica valores nulos
			if ((object) first == null || (object) second == null)
				return false;
			
			return first.GetHashCode() == second.GetHashCode();
		}
		
		/// <summary>
		/// Verifica que dos objetos sean distintos
		/// </summary>
		public static bool operator !=(PatPatients first, PatPatients second)
		{
			return !(first == second);
		}
		
		/// <summary>
		/// Compara este objeto con otro
		/// </summary>
		/// <param name="obj">El objeto a comparar</param>
		/// <returns>Devuelve Verdadero si ambos objetos son iguales</returns>
		public override bool Equals(object obj)
		{
			if (obj == null)
				return false;
			
			if (obj.GetType() == this.GetType())
				return obj.GetHashCode() == this.GetHashCode();
			
			return false;
		}
		
		#endregion
		
		#region Encriptacion
		
		/// <summary>
		/// Un valor unico que identifica cada instancia de este objeto
		/// </summary>
		/// <returns>Unico valor entero</returns>
		public override int GetHashCode()
		{
			return (this.GetSha512Hash().GetHashCode());
		}
		
		/// <summary>
		///     Obtiene el Hash de la Instancia Actual
		/// </summary>
		/// <typeparam name="T">
		///     Tipo de Proveedor de servicios criptográficos
		/// </typeparam>
		/// <returns>
		///     String en Base64 que representa el Hash
		/// </returns>
		public string GetHash<T>() where T : HashAlgorithm, new()
		{
			T cryptoServiceProvider = new T();
			return ComputeHash(cryptoServiceProvider);
		}
		
		/// <summary>
		///     Obtiene el codigo Hash en Base al Algoritmo SHA1
		/// </summary>
		/// <returns>
		///     String en Base64 que representa el Hash en SHA1
		/// </returns>
		public string GetSha1Hash()
		{
			return GetHash<SHA1CryptoServiceProvider>();
		}
		
		/// <summary>
		///     Obtiene el codigo Hash en Base al Algoritmo SHA512
		/// </summary>
		/// <returns>
		///     String en Base64 que representa el Hash en SHA512
		/// </returns>
		public string GetSha512Hash()
		{
			return GetHash<SHA512CryptoServiceProvider>();
		}
		
		/// <summary>
		///     Obtiene el codigo Hash en Base al Algoritmo MD5
		/// </summary>
		/// <returns>
		///     String en Base64 que representa el Hash en MD5
		/// </returns>
		public string GetMd5Hash()
		{
			return GetHash<MD5CryptoServiceProvider>();
		}
		
		
		#endregion
		
		#region DataAnnotations
		
		/// <summary>
		/// Obtiene los errores basado en los DataAnnotations 
		/// </summary>
		/// <returns>Devuelve un IList del tipo ValidationResult con los errores obtenidos</returns>
		public IList<ValidationResult> ValidationErrors()
		{
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			if (!Validator.TryValidateObject(this, context, errors, true))
				return errors;
			
			return new List<ValidationResult>(0);
		}
		
		/// <summary>
		/// Obtiene los errores basado en los DataAnnotations 
		/// </summary>
		/// <returns>Devuelve un String con los errores obtenidos</returns>
		public string ValidationErrorsString()
		{
			string strErrors = "";
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			if (!Validator.TryValidateObject(this, context, errors, true))
			{
				foreach (ValidationResult result in errors)
					strErrors = strErrors + result.ErrorMessage + Environment.NewLine;
			}
			return strErrors;
		}
		
		/// <summary>
		/// Funcion que determina si un objeto es valido o no
		/// </summary>
		/// <returns>Resultado de la validacion</returns>
		public bool IsValid()
		{
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			return Validator.TryValidateObject(this, context, errors, true);
		}
		
		#endregion
		
		/// <summary>
		/// Entity identifier
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "ID", 
            Description = "Entity identifier", 
            Order = -1)]
        [JsonProperty(PropertyName = "id_patients")]
        [Required(ErrorMessage = "Se necesita un valor para -id_patients- porque es un campo requerido.")]
		[Key]
		public Int64 IdPatients { get; set; } 

		/// <summary>
		/// Patient ID number (also used to log in to the website)
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "CI",
            Description = "Patient ID number (also used to log in to the website)",
            Order = 1)]
        [JsonProperty(PropertyName = "identity_number")]
        public string IdentityNumber { get; set; } 

		/// <summary>
		/// Patient first name
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "Nombres", 
            Description = "Patient first name",
            Order = 2)]
        [JsonProperty(PropertyName = "first_name")]
        [Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -first_name- porque es un campo requerido.")]
		public string FirstName { get; set; } 

		/// <summary>
		/// Patient last name
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "Apellidos", 
            Description = "Patient last name",
            Order = 3)]
        [JsonProperty(PropertyName = "last_name")]
        public string LastName { get; set; }

        [Display(Name = "Nombre Completo",
            Order = 4)]
        [JsonIgnore]
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        /// <summary>
        /// Patient gender
        /// Permite Null: No
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [Display(Name = "Género", 
            Description = "Patient gender",
            Order = 5)]
        [JsonProperty(PropertyName = "gender")]
        [Required(ErrorMessage = "Se necesita un valor para -gender- porque es un campo requerido.")]
		public bool Gender { get; set; } 

		/// <summary>
		/// Birth date
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "Fec Nac", 
            Description = "Birth date",
            Order = 6)]
        [JsonProperty(PropertyName = "birthdate")]
        [Required(ErrorMessage = "Se necesita un valor para -birthdate- porque es un campo requerido.")]
        //[JsonConverter(typeof(UnixMillisecondsConverter))]
        public DateTime Birthdate { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna id_blood_types de la Tabla pat_patients
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "Tipo Sangre", 
            Description = " Propiedad publica de tipo Int64 que representa a la columna id_blood_types de la Tabla pat_patients",
            Order = -1)]
        [JsonProperty(PropertyName = "id_blood_types")]
        [Required(ErrorMessage = "Se necesita un valor para -id_blood_types- porque es un campo requerido.")]
		public Int64 IdBloodTypes { get; set; }

        [Display(Order = -1)]
        [JsonProperty("blood_types")]
        public ClaBloodtypes BloodTypes { get; set; }

        [Display(Name = "Tipo Sangre",
            Order = 7)]
        [JsonIgnore]
        public string BloodtypesDescription
        {
            get
            {
                return BloodTypes == null ? null : BloodTypes.Description;
            }
        }

        /// <summary>
        /// Propiedad publica de tipo Int64 que representa a la columna id_marital de la Tabla pat_patients
        /// Permite Null: No
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: Yes
        /// </summary>
        [Display(Name = "Est Civil", 
            Description = " Propiedad publica de tipo Int64 que representa a la columna id_marital de la Tabla pat_patients",
            Order = -1)]
        [JsonProperty(PropertyName = "id_marital")]
        [Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -id_marital- porque es un campo requerido.")]
		public Int64 IdMarital { get; set; }

        [Display(Order = -1)]
        [JsonProperty("marital")]
        public ClaMarital Marital { get; set; }

        [Display(Name = "Est Civil",
            Order = 8)]
        [JsonIgnore]
        public string MaritalDescription
        {
            get
            {
                return Marital == null ? null : Marital.Description;
            }
        }

        /// <summary>
        /// Patient occupation
        /// Permite Null: Yes
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [StringLength(300, MinimumLength=0)]
		[Display(Name = "Ocupación", 
            Description = "Patient occupation",
            Order = 9)]
        [JsonProperty(PropertyName = "occupation")]
        public string Occupation { get; set; } 

		/// <summary>
		/// Patient origin
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(300, MinimumLength=0)]
		[Display(Name = "Origen", 
            Description = "Patient origin",
            Order = -1)]
        [JsonProperty(PropertyName = "origin")]
        public string Origin { get; set; } 

		/// <summary>
		/// Patient address
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "Dirección", 
            Description = "Patient address",
            Order = -1)]
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; } 

		/// <summary>
		/// Dominant hand
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "Mano dominante", 
            Description = "Dominant hand",
            Order = -1)]
        [JsonProperty(PropertyName = "dominance")]
        public string Dominance { get; set; } 

		/// <summary>
		/// Patient phone
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "Teléfonos", 
            Description = "Patient phone",
            Order = 10)]
        [JsonProperty(PropertyName = "phone")]
        public string Phone { get; set; } 

		/// <summary>
		/// Patient email
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "Email", 
            Description = "Patient email",
            Order = -1)]
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; } 

		/// <summary>
		/// Patient religion
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "Religión", 
            Description = "Patient religion",
            Order = -1)]
        [JsonProperty(PropertyName = "religion")]
        public string Religion { get; set; } 

		/// <summary>
		/// Patient company
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "Empresa", 
            Description = "Patient company",
            Order = 11)]
        [JsonProperty(PropertyName = "company")]
        public string Company { get; set; } 

		/// <summary>
		/// Patient health insurance
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "Seguro", 
            Description = "Patient health insurance",
            Order = 11)]
        [JsonProperty(PropertyName = "insurance")]
        public string Insurance { get; set; } 

		/// <summary>
		/// Patient referral
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "refers", 
            Description = "Patient referral",
            Order = -1)]
        [JsonProperty(PropertyName = "refers")]
        public string Refers { get; set; } 

		/// <summary>
		/// Patient informant
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "informant", 
            Description = "Patient informant",
            Order = -1)]
        [JsonProperty(PropertyName = "informant")]
        public string Informant { get; set; } 

		/// <summary>
		/// Current status
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_status", 
            Description = "Current status",
            Order = -1)]
        [JsonProperty(PropertyName = "api_status")]
        [Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -api_status- porque es un campo requerido.")]
		public string ApiStatus { get; set; } 

		/// <summary>
		/// Last transaction used
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(40, MinimumLength=0)]
		[Display(Name = "api_transaction", 
            Description = "Last transaction used",
            Order = -1)]
        [JsonProperty(PropertyName = "api_transaction")]
        [Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -api_transaction- porque es un campo requerido.")]
		public string ApiTransaction { get; set; } 

		/// <summary>
		/// User creation
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_usucre", 
            Description = "User creation",
            Order = -1)]
        [JsonProperty(PropertyName = "api_usucre")]
        public string ApiUsucre { get; set; } 

		/// <summary>
		/// Timestamp creation
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "api_feccre", 
            Description = "Timestamp creation",
            Order = -1)]
        [JsonProperty(PropertyName = "api_feccre")]
        //[JsonConverter(typeof(UnixMillisecondsConverter))]
        public DateTime ApiFeccre { get; set; } 

		/// <summary>
		/// User update
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_usumod", 
            Description = "User update",
            Order = -1)]
        [JsonProperty(PropertyName = "api_usumod")]
        public string ApiUsumod { get; set; } 

		/// <summary>
		/// Timestamp update
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "api_fecmod", 
            Description = "Timestamp update",
            Order = -1)]
        [JsonProperty(PropertyName = "api_fecmod")]
        //[JsonConverter(typeof(UnixMillisecondsConverter))]
        public DateTime? ApiFecmod { get; set; } 

		/// <summary>
		/// User delete
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_usudel", 
            Description = "User delete",
            Order = -1)]
        [JsonProperty(PropertyName = "api_usudel")]
        public string ApiUsudel { get; set; } 

		/// <summary>
		/// Timestamp delete
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "api_fecdel", 
            Description = "Timestamp delete",
            Order = -1)]
        [JsonProperty(PropertyName = "api_fecdel")]
        //[JsonConverter(typeof(UnixMillisecondsConverter))]
        public DateTime? ApiFecdel { get; set; }
    }
}

