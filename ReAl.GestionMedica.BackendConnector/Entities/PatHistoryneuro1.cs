#region 
/***********************************************************************************************************
	NOMBRE:       PatHistoryneuro1
	DESCRIPCION:
		Clase que define un objeto para la Tabla pat_history_neuro1

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        15/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using Newtonsoft.Json;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Entities
{
	[Table("pat_history_neuro1")]
	public class PatHistoryneuro1
	{
		public const string StrNombreTabla = "Pat_history_neuro1";
		public const string StrAliasTabla = "pat_history_neuro1";
		public const string StrDescripcionTabla = "pat_history_neuro1";
		public const string StrNombreLisForm = "Listado de pat_history_neuro1";
		public const string StrNombreDocForm = "Detalle de pat_history_neuro1";
		public const string StrNombreForm = "Registro de pat_history_neuro1";
		public enum Fields
		{
			IdHistoryNeuro1
			,IdPatients
			,H1Motivo
			,H1Enfermedad
			,H1Cirujias
			,H1Infecciones
			,H1Traumatismos
			,H1Cardiovascular
			,H1Metabolico
			,H1Toxico
			,H1Familiar
			,H1Ginecoobs
			,H1Otros
			,H1Embarazos
			,H1Gesta
			,H1Abortos
			,H1Mesntruaciones
			,H1Fum
			,H1Fup
			,H1Revision
			,H1Examenfisico
			,H1Ta
			,H1Fc
			,H1Fr
			,H1Temp
			,H1Talla
			,H1Peso
			,H1Pc
			,H2Cabeza
			,H2Cuello
			,H2Torax
			,H2Abdomen
			,H2Genitourinario
			,H2Extremidades
			,H2Conalerta
			,H2Conconfusion
			,H2Consomnolencia
			,H2Conestufor
			,H2Concoma
			,H2Oriespacio
			,H2Orilugar
			,H2Oripersona
			,H2Razidea
			,H2Razjuicio
			,H2Razabstraccion
			,H2Conocimientosgenerales
			,H2Atgdesatento
			,H2Atgfluctuante
			,H2Atgnegativo
			,H2Atgnegligente
			,H2Pagconfusion
			,H2Pagdelirio
			,H2Paedelusiones
			,H2Paeilsuiones
			,H2Paealucinaciones
			,H3Conreciente
			,H3Conremota
			,H3Afeeuforico
			,H3Afeplano
			,H3Afedeprimido
			,H3Afelabil
			,H3Afehostil
			,H3Afeinadecuado
			,H3Hdoizquierdo
			,H3Hdoderecho
			,H3Cgeexpresion
			,H3Cgenominacion
			,H3Cgerepeticion
			,H3Cgecomprension
			,H3Apebroca
			,H3Apeconduccion
			,H3Apewernicke
			,H3Apeglobal
			,H3Apeanomia
			,H3Atrmotora
			,H3Atrsensitiva
			,H3Atrmixta
			,H3Ataexpresion
			,H3Atacomprension
			,H3Atamixta
			,H3Aprmotora
			,H3Aprsensorial
			,H3Lesalexia
			,H3Lesagrafia
			,H3Lpdacalculia
			,H3Lpdagrafia
			,H3Lpddesorientacion
			,H3Lpdagnosia
			,H3Agnauditiva
			,H3Agnvisual
			,H3Agntactil
			,H3Aprideacional
			,H3Aprideomotora
			,H3Aprdesatencion
			,H3Anosognosia
			,ApiStatus
			,ApiTransaction
			,ApiUsucre
			,ApiFeccre
			,ApiUsumod
			,ApiFecmod
			,ApiUsudel
			,ApiFecdel

		}
		
		#region Constructoress
		
		public PatHistoryneuro1()
		{
			//Inicializacion de Variables
			H1Motivo = null;
			H1Enfermedad = null;
			H1Cirujias = null;
			H1Infecciones = null;
			H1Traumatismos = null;
			H1Cardiovascular = null;
			H1Metabolico = null;
			H1Toxico = null;
			H1Familiar = null;
			H1Ginecoobs = null;
			H1Otros = null;
			H1Embarazos = null;
			H1Gesta = null;
			H1Abortos = null;
			H1Mesntruaciones = null;
			H1Fum = null;
			H1Fup = null;
			H1Revision = null;
			H1Examenfisico = null;
			H1Ta = null;
			H1Fc = null;
			H1Fr = null;
			H1Temp = null;
			H1Talla = null;
			H1Peso = null;
			H1Pc = null;
			H2Cabeza = null;
			H2Cuello = null;
			H2Torax = null;
			H2Abdomen = null;
			H2Genitourinario = null;
			H2Extremidades = null;
			H2Conalerta = null;
			H2Conconfusion = null;
			H2Consomnolencia = null;
			H2Conestufor = null;
			H2Concoma = null;
			H2Oriespacio = null;
			H2Orilugar = null;
			H2Oripersona = null;
			H2Razidea = null;
			H2Razjuicio = null;
			H2Razabstraccion = null;
			H2Conocimientosgenerales = null;
			H2Atgdesatento = null;
			H2Atgfluctuante = null;
			H2Atgnegativo = null;
			H2Atgnegligente = null;
			H2Pagconfusion = null;
			H2Pagdelirio = null;
			H2Paedelusiones = null;
			H2Paeilsuiones = null;
			H2Paealucinaciones = null;
			H3Conreciente = null;
			H3Conremota = null;
			H3Afeeuforico = null;
			H3Afeplano = null;
			H3Afedeprimido = null;
			H3Afelabil = null;
			H3Afehostil = null;
			H3Afeinadecuado = null;
			H3Hdoizquierdo = null;
			H3Hdoderecho = null;
			H3Cgeexpresion = null;
			H3Cgenominacion = null;
			H3Cgerepeticion = null;
			H3Cgecomprension = null;
			H3Apebroca = null;
			H3Apeconduccion = null;
			H3Apewernicke = null;
			H3Apeglobal = null;
			H3Apeanomia = null;
			H3Atrmotora = null;
			H3Atrsensitiva = null;
			H3Atrmixta = null;
			H3Ataexpresion = null;
			H3Atacomprension = null;
			H3Atamixta = null;
			H3Aprmotora = null;
			H3Aprsensorial = null;
			H3Lesalexia = null;
			H3Lesagrafia = null;
			H3Lpdacalculia = null;
			H3Lpdagrafia = null;
			H3Lpddesorientacion = null;
			H3Lpdagnosia = null;
			H3Agnauditiva = null;
			H3Agnvisual = null;
			H3Agntactil = null;
			H3Aprideacional = null;
			H3Aprideomotora = null;
			H3Aprdesatencion = null;
			H3Anosognosia = null;
			ApiStatus = null;
			ApiTransaction = null;
			ApiUsucre = null;
			ApiUsumod = null;
			ApiFecmod = null;
			ApiUsudel = null;
			ApiFecdel = null;
		}
		
		public PatHistoryneuro1(PatHistoryneuro1 obj)
		{
			IdHistoryNeuro1 = obj.IdHistoryNeuro1;
			IdPatients = obj.IdPatients;
			H1Motivo = obj.H1Motivo;
			H1Enfermedad = obj.H1Enfermedad;
			H1Cirujias = obj.H1Cirujias;
			H1Infecciones = obj.H1Infecciones;
			H1Traumatismos = obj.H1Traumatismos;
			H1Cardiovascular = obj.H1Cardiovascular;
			H1Metabolico = obj.H1Metabolico;
			H1Toxico = obj.H1Toxico;
			H1Familiar = obj.H1Familiar;
			H1Ginecoobs = obj.H1Ginecoobs;
			H1Otros = obj.H1Otros;
			H1Embarazos = obj.H1Embarazos;
			H1Gesta = obj.H1Gesta;
			H1Abortos = obj.H1Abortos;
			H1Mesntruaciones = obj.H1Mesntruaciones;
			H1Fum = obj.H1Fum;
			H1Fup = obj.H1Fup;
			H1Revision = obj.H1Revision;
			H1Examenfisico = obj.H1Examenfisico;
			H1Ta = obj.H1Ta;
			H1Fc = obj.H1Fc;
			H1Fr = obj.H1Fr;
			H1Temp = obj.H1Temp;
			H1Talla = obj.H1Talla;
			H1Peso = obj.H1Peso;
			H1Pc = obj.H1Pc;
			H2Cabeza = obj.H2Cabeza;
			H2Cuello = obj.H2Cuello;
			H2Torax = obj.H2Torax;
			H2Abdomen = obj.H2Abdomen;
			H2Genitourinario = obj.H2Genitourinario;
			H2Extremidades = obj.H2Extremidades;
			H2Conalerta = obj.H2Conalerta;
			H2Conconfusion = obj.H2Conconfusion;
			H2Consomnolencia = obj.H2Consomnolencia;
			H2Conestufor = obj.H2Conestufor;
			H2Concoma = obj.H2Concoma;
			H2Oriespacio = obj.H2Oriespacio;
			H2Orilugar = obj.H2Orilugar;
			H2Oripersona = obj.H2Oripersona;
			H2Razidea = obj.H2Razidea;
			H2Razjuicio = obj.H2Razjuicio;
			H2Razabstraccion = obj.H2Razabstraccion;
			H2Conocimientosgenerales = obj.H2Conocimientosgenerales;
			H2Atgdesatento = obj.H2Atgdesatento;
			H2Atgfluctuante = obj.H2Atgfluctuante;
			H2Atgnegativo = obj.H2Atgnegativo;
			H2Atgnegligente = obj.H2Atgnegligente;
			H2Pagconfusion = obj.H2Pagconfusion;
			H2Pagdelirio = obj.H2Pagdelirio;
			H2Paedelusiones = obj.H2Paedelusiones;
			H2Paeilsuiones = obj.H2Paeilsuiones;
			H2Paealucinaciones = obj.H2Paealucinaciones;
			H3Conreciente = obj.H3Conreciente;
			H3Conremota = obj.H3Conremota;
			H3Afeeuforico = obj.H3Afeeuforico;
			H3Afeplano = obj.H3Afeplano;
			H3Afedeprimido = obj.H3Afedeprimido;
			H3Afelabil = obj.H3Afelabil;
			H3Afehostil = obj.H3Afehostil;
			H3Afeinadecuado = obj.H3Afeinadecuado;
			H3Hdoizquierdo = obj.H3Hdoizquierdo;
			H3Hdoderecho = obj.H3Hdoderecho;
			H3Cgeexpresion = obj.H3Cgeexpresion;
			H3Cgenominacion = obj.H3Cgenominacion;
			H3Cgerepeticion = obj.H3Cgerepeticion;
			H3Cgecomprension = obj.H3Cgecomprension;
			H3Apebroca = obj.H3Apebroca;
			H3Apeconduccion = obj.H3Apeconduccion;
			H3Apewernicke = obj.H3Apewernicke;
			H3Apeglobal = obj.H3Apeglobal;
			H3Apeanomia = obj.H3Apeanomia;
			H3Atrmotora = obj.H3Atrmotora;
			H3Atrsensitiva = obj.H3Atrsensitiva;
			H3Atrmixta = obj.H3Atrmixta;
			H3Ataexpresion = obj.H3Ataexpresion;
			H3Atacomprension = obj.H3Atacomprension;
			H3Atamixta = obj.H3Atamixta;
			H3Aprmotora = obj.H3Aprmotora;
			H3Aprsensorial = obj.H3Aprsensorial;
			H3Lesalexia = obj.H3Lesalexia;
			H3Lesagrafia = obj.H3Lesagrafia;
			H3Lpdacalculia = obj.H3Lpdacalculia;
			H3Lpdagrafia = obj.H3Lpdagrafia;
			H3Lpddesorientacion = obj.H3Lpddesorientacion;
			H3Lpdagnosia = obj.H3Lpdagnosia;
			H3Agnauditiva = obj.H3Agnauditiva;
			H3Agnvisual = obj.H3Agnvisual;
			H3Agntactil = obj.H3Agntactil;
			H3Aprideacional = obj.H3Aprideacional;
			H3Aprideomotora = obj.H3Aprideomotora;
			H3Aprdesatencion = obj.H3Aprdesatencion;
			H3Anosognosia = obj.H3Anosognosia;
			ApiStatus = obj.ApiStatus;
			ApiTransaction = obj.ApiTransaction;
			ApiUsucre = obj.ApiUsucre;
			ApiFeccre = obj.ApiFeccre;
			ApiUsumod = obj.ApiUsumod;
			ApiFecmod = obj.ApiFecmod;
			ApiUsudel = obj.ApiUsudel;
			ApiFecdel = obj.ApiFecdel;
		}
		
		#endregion
		
		#region Metodos Estaticos
		#endregion
		
		#region Metodos Privados
		
		/// <summary>
		/// Obtiene el Hash a partir de un array de Bytes
		/// </summary>
		/// <param name="objectAsBytes"></param>
		/// <returns>string</returns>
		private string ComputeHash(byte[] objectAsBytes)
		{
			MD5 md5 = new MD5CryptoServiceProvider();
			try
			{
				byte[] result = md5.ComputeHash(objectAsBytes);
				
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < result.Length; i++)
				{
					sb.Append(result[i].ToString("X2"));
				}
				
				return sb.ToString();
			}
			catch (ArgumentNullException ane)
			{
				return null;
			}
		}
		
		/// <summary>
		///     Obtienen el Hash basado en algun algoritmo de Encriptación
		/// </summary>
		/// <typeparam name="T">
		///     Algoritmo de encriptación
		/// </typeparam>
		/// <param name="cryptoServiceProvider">
		///     Provvedor de Servicios de Criptografía
		/// </param>
		/// <returns>
		///     String que representa el Hash calculado
		        /// </returns>
		private string ComputeHash<T>( T cryptoServiceProvider) where T : HashAlgorithm, new()
		{
			DataContractSerializer serializer = new DataContractSerializer(this.GetType());
			using (MemoryStream memoryStream = new MemoryStream())
			{
				serializer.WriteObject(memoryStream, this);
				cryptoServiceProvider.ComputeHash(memoryStream.ToArray());
				return Convert.ToBase64String(cryptoServiceProvider.Hash);
			}
		}
		
		#endregion
		
		#region Overrides
		
		/// <summary>
		/// Devuelve un String que representa al Objeto
		/// </summary>
		/// <returns>string</returns>
		public override string ToString()
		{
			string hashString;
			
			//Evitar parametros NULL
			if (this == null)
				throw new ArgumentNullException("Parametro NULL no es valido");
			
			//Se verifica si el objeto es serializable.
			try
			{
				MemoryStream memStream = new MemoryStream();
				XmlSerializer serializer = new XmlSerializer(typeof(PatHistoryneuro1));
				serializer.Serialize(memStream, this);
				
				//Ahora se obtiene el Hash del Objeto.
				hashString = ComputeHash(memStream.ToArray());
				
				return hashString;
			}
			catch (AmbiguousMatchException ame)
			{
				throw new ApplicationException("El Objeto no es Serializable. Message:" + ame.Message);
			}
		}
		
		/// <summary>
		/// Verifica que dos objetos sean identicos
		/// </summary>
		public static bool operator ==(PatHistoryneuro1 first, PatHistoryneuro1 second)
		{
			// Verifica si el puntero en memoria es el mismo
			if (Object.ReferenceEquals(first, second))
				return true;
			
			// Verifica valores nulos
			if ((object) first == null || (object) second == null)
				return false;
			
			return first.GetHashCode() == second.GetHashCode();
		}
		
		/// <summary>
		/// Verifica que dos objetos sean distintos
		/// </summary>
		public static bool operator !=(PatHistoryneuro1 first, PatHistoryneuro1 second)
		{
			return !(first == second);
		}
		
		/// <summary>
		/// Compara este objeto con otro
		/// </summary>
		/// <param name="obj">El objeto a comparar</param>
		/// <returns>Devuelve Verdadero si ambos objetos son iguales</returns>
		public override bool Equals(object obj)
		{
			if (obj == null)
				return false;
			
			if (obj.GetType() == this.GetType())
				return obj.GetHashCode() == this.GetHashCode();
			
			return false;
		}
		
		#endregion
		
		#region Encriptacion
		
		/// <summary>
		/// Un valor unico que identifica cada instancia de este objeto
		/// </summary>
		/// <returns>Unico valor entero</returns>
		public override int GetHashCode()
		{
			return (this.GetSha512Hash().GetHashCode());
		}
		
		/// <summary>
		///     Obtiene el Hash de la Instancia Actual
		/// </summary>
		/// <typeparam name="T">
		///     Tipo de Proveedor de servicios criptográficos
		/// </typeparam>
		/// <returns>
		///     String en Base64 que representa el Hash
		/// </returns>
		public string GetHash<T>() where T : HashAlgorithm, new()
		{
			T cryptoServiceProvider = new T();
			return ComputeHash(cryptoServiceProvider);
		}
		
		/// <summary>
		///     Obtiene el codigo Hash en Base al Algoritmo SHA1
		/// </summary>
		/// <returns>
		///     String en Base64 que representa el Hash en SHA1
		/// </returns>
		public string GetSha1Hash()
		{
			return GetHash<SHA1CryptoServiceProvider>();
		}
		
		/// <summary>
		///     Obtiene el codigo Hash en Base al Algoritmo SHA512
		/// </summary>
		/// <returns>
		///     String en Base64 que representa el Hash en SHA512
		/// </returns>
		public string GetSha512Hash()
		{
			return GetHash<SHA512CryptoServiceProvider>();
		}
		
		/// <summary>
		///     Obtiene el codigo Hash en Base al Algoritmo MD5
		/// </summary>
		/// <returns>
		///     String en Base64 que representa el Hash en MD5
		/// </returns>
		public string GetMd5Hash()
		{
			return GetHash<MD5CryptoServiceProvider>();
		}
		
		
		#endregion
		
		#region DataAnnotations
		
		/// <summary>
		/// Obtiene los errores basado en los DataAnnotations 
		/// </summary>
		/// <returns>Devuelve un IList del tipo ValidationResult con los errores obtenidos</returns>
		public IList<ValidationResult> ValidationErrors()
		{
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			if (!Validator.TryValidateObject(this, context, errors, true))
				return errors;
			
			return new List<ValidationResult>(0);
		}
		
		/// <summary>
		/// Obtiene los errores basado en los DataAnnotations 
		/// </summary>
		/// <returns>Devuelve un String con los errores obtenidos</returns>
		public string ValidationErrorsString()
		{
			string strErrors = "";
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			if (!Validator.TryValidateObject(this, context, errors, true))
			{
				foreach (ValidationResult result in errors)
					strErrors = strErrors + result.ErrorMessage + Environment.NewLine;
			}
			return strErrors;
		}
		
		/// <summary>
		/// Funcion que determina si un objeto es valido o no
		/// </summary>
		/// <returns>Resultado de la validacion</returns>
		public bool IsValid()
		{
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			return Validator.TryValidateObject(this, context, errors, true);
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna id_history_neuro1 de la Tabla pat_history_neuro1
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_history_neuro1", Description = " Propiedad publica de tipo Int64 que representa a la columna id_history_neuro1 de la Tabla pat_history_neuro1", Order = 1)]
		[JsonProperty(PropertyName = "id_history_neuro1")]
		[Required(ErrorMessage = "Se necesita un valor para -id_history_neuro1- porque es un campo requerido.")]
		[Key]
		public Int64 IdHistoryNeuro1 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna id_patients de la Tabla pat_history_neuro1
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_patients", Description = " Propiedad publica de tipo Int64 que representa a la columna id_patients de la Tabla pat_history_neuro1", Order = 2)]
		[JsonProperty(PropertyName = "id_patients")]
		[Required(ErrorMessage = "Se necesita un valor para -id_patients- porque es un campo requerido.")]
		public Int64 IdPatients { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1motivo de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h1motivo", Description = " Propiedad publica de tipo string que representa a la columna h1motivo de la Tabla pat_history_neuro1", Order = 3)]
		[JsonProperty(PropertyName = "h1motivo")]
		public string H1Motivo { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1enfermedad de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h1enfermedad", Description = " Propiedad publica de tipo string que representa a la columna h1enfermedad de la Tabla pat_history_neuro1", Order = 4)]
		[JsonProperty(PropertyName = "h1enfermedad")]
		public string H1Enfermedad { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1cirujias de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1cirujias", Description = " Propiedad publica de tipo string que representa a la columna h1cirujias de la Tabla pat_history_neuro1", Order = 5)]
		[JsonProperty(PropertyName = "h1cirujias")]
		public string H1Cirujias { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1infecciones de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1infecciones", Description = " Propiedad publica de tipo string que representa a la columna h1infecciones de la Tabla pat_history_neuro1", Order = 6)]
		[JsonProperty(PropertyName = "h1infecciones")]
		public string H1Infecciones { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1traumatismos de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1traumatismos", Description = " Propiedad publica de tipo string que representa a la columna h1traumatismos de la Tabla pat_history_neuro1", Order = 7)]
		[JsonProperty(PropertyName = "h1traumatismos")]
		public string H1Traumatismos { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1cardiovascular de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1cardiovascular", Description = " Propiedad publica de tipo string que representa a la columna h1cardiovascular de la Tabla pat_history_neuro1", Order = 8)]
		[JsonProperty(PropertyName = "h1cardiovascular")]
		public string H1Cardiovascular { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1metabolico de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1metabolico", Description = " Propiedad publica de tipo string que representa a la columna h1metabolico de la Tabla pat_history_neuro1", Order = 9)]
		[JsonProperty(PropertyName = "h1metabolico")]
		public string H1Metabolico { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1toxico de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1toxico", Description = " Propiedad publica de tipo string que representa a la columna h1toxico de la Tabla pat_history_neuro1", Order = 10)]
		[JsonProperty(PropertyName = "h1toxico")]
		public string H1Toxico { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1familiar de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1familiar", Description = " Propiedad publica de tipo string que representa a la columna h1familiar de la Tabla pat_history_neuro1", Order = 11)]
		[JsonProperty(PropertyName = "h1familiar")]
		public string H1Familiar { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1ginecoobs de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1ginecoobs", Description = " Propiedad publica de tipo string que representa a la columna h1ginecoobs de la Tabla pat_history_neuro1", Order = 12)]
		[JsonProperty(PropertyName = "h1ginecoobs")]
		public string H1Ginecoobs { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1otros de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1otros", Description = " Propiedad publica de tipo string que representa a la columna h1otros de la Tabla pat_history_neuro1", Order = 13)]
		[JsonProperty(PropertyName = "h1otros")]
		public string H1Otros { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna h1embarazos de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "h1embarazos", Description = " Propiedad publica de tipo int que representa a la columna h1embarazos de la Tabla pat_history_neuro1", Order = 14)]
		[JsonProperty(PropertyName = "h1embarazos")]
		public int? H1Embarazos { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna h1gesta de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "h1gesta", Description = " Propiedad publica de tipo int que representa a la columna h1gesta de la Tabla pat_history_neuro1", Order = 15)]
		[JsonProperty(PropertyName = "h1gesta")]
		public int? H1Gesta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna h1abortos de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "h1abortos", Description = " Propiedad publica de tipo int que representa a la columna h1abortos de la Tabla pat_history_neuro1", Order = 16)]
		[JsonProperty(PropertyName = "h1abortos")]
		public int? H1Abortos { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1mesntruaciones de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1mesntruaciones", Description = " Propiedad publica de tipo string que representa a la columna h1mesntruaciones de la Tabla pat_history_neuro1", Order = 17)]
		[JsonProperty(PropertyName = "h1mesntruaciones")]
		public string H1Mesntruaciones { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1fum de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1fum", Description = " Propiedad publica de tipo string que representa a la columna h1fum de la Tabla pat_history_neuro1", Order = 18)]
		[JsonProperty(PropertyName = "h1fum")]
		public string H1Fum { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1fup de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1fup", Description = " Propiedad publica de tipo string que representa a la columna h1fup de la Tabla pat_history_neuro1", Order = 19)]
		[JsonProperty(PropertyName = "h1fup")]
		public string H1Fup { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1revision de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h1revision", Description = " Propiedad publica de tipo string que representa a la columna h1revision de la Tabla pat_history_neuro1", Order = 20)]
		[JsonProperty(PropertyName = "h1revision")]
		public string H1Revision { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1examenfisico de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h1examenfisico", Description = " Propiedad publica de tipo string que representa a la columna h1examenfisico de la Tabla pat_history_neuro1", Order = 21)]
		[JsonProperty(PropertyName = "h1examenfisico")]
		public string H1Examenfisico { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1ta de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h1ta", Description = " Propiedad publica de tipo string que representa a la columna h1ta de la Tabla pat_history_neuro1", Order = 22)]
		[JsonProperty(PropertyName = "h1ta")]
		public string H1Ta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1fc de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h1fc", Description = " Propiedad publica de tipo string que representa a la columna h1fc de la Tabla pat_history_neuro1", Order = 23)]
		[JsonProperty(PropertyName = "h1fc")]
		public string H1Fc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1fr de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h1fr", Description = " Propiedad publica de tipo string que representa a la columna h1fr de la Tabla pat_history_neuro1", Order = 24)]
		[JsonProperty(PropertyName = "h1fr")]
		public string H1Fr { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1temp de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h1temp", Description = " Propiedad publica de tipo string que representa a la columna h1temp de la Tabla pat_history_neuro1", Order = 25)]
		[JsonProperty(PropertyName = "h1temp")]
		public string H1Temp { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1talla de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h1talla", Description = " Propiedad publica de tipo string que representa a la columna h1talla de la Tabla pat_history_neuro1", Order = 26)]
		[JsonProperty(PropertyName = "h1talla")]
		public string H1Talla { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1peso de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h1peso", Description = " Propiedad publica de tipo string que representa a la columna h1peso de la Tabla pat_history_neuro1", Order = 27)]
		[JsonProperty(PropertyName = "h1peso")]
		public string H1Peso { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1pc de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h1pc", Description = " Propiedad publica de tipo string que representa a la columna h1pc de la Tabla pat_history_neuro1", Order = 28)]
		[JsonProperty(PropertyName = "h1pc")]
		public string H1Pc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2cabeza de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h2cabeza", Description = " Propiedad publica de tipo string que representa a la columna h2cabeza de la Tabla pat_history_neuro1", Order = 29)]
		[JsonProperty(PropertyName = "h2cabeza")]
		public string H2Cabeza { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2cuello de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h2cuello", Description = " Propiedad publica de tipo string que representa a la columna h2cuello de la Tabla pat_history_neuro1", Order = 30)]
		[JsonProperty(PropertyName = "h2cuello")]
		public string H2Cuello { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2torax de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h2torax", Description = " Propiedad publica de tipo string que representa a la columna h2torax de la Tabla pat_history_neuro1", Order = 31)]
		[JsonProperty(PropertyName = "h2torax")]
		public string H2Torax { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2abdomen de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h2abdomen", Description = " Propiedad publica de tipo string que representa a la columna h2abdomen de la Tabla pat_history_neuro1", Order = 32)]
		[JsonProperty(PropertyName = "h2abdomen")]
		public string H2Abdomen { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2genitourinario de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h2genitourinario", Description = " Propiedad publica de tipo string que representa a la columna h2genitourinario de la Tabla pat_history_neuro1", Order = 33)]
		[JsonProperty(PropertyName = "h2genitourinario")]
		public string H2Genitourinario { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2extremidades de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h2extremidades", Description = " Propiedad publica de tipo string que representa a la columna h2extremidades de la Tabla pat_history_neuro1", Order = 34)]
		[JsonProperty(PropertyName = "h2extremidades")]
		public string H2Extremidades { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2conalerta de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2conalerta", Description = " Propiedad publica de tipo string que representa a la columna h2conalerta de la Tabla pat_history_neuro1", Order = 35)]
		[JsonProperty(PropertyName = "h2conalerta")]
		public string H2Conalerta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2conconfusion de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2conconfusion", Description = " Propiedad publica de tipo string que representa a la columna h2conconfusion de la Tabla pat_history_neuro1", Order = 36)]
		[JsonProperty(PropertyName = "h2conconfusion")]
		public string H2Conconfusion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2consomnolencia de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2consomnolencia", Description = " Propiedad publica de tipo string que representa a la columna h2consomnolencia de la Tabla pat_history_neuro1", Order = 37)]
		[JsonProperty(PropertyName = "h2consomnolencia")]
		public string H2Consomnolencia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2conestufor de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2conestufor", Description = " Propiedad publica de tipo string que representa a la columna h2conestufor de la Tabla pat_history_neuro1", Order = 38)]
		[JsonProperty(PropertyName = "h2conestufor")]
		public string H2Conestufor { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2concoma de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2concoma", Description = " Propiedad publica de tipo string que representa a la columna h2concoma de la Tabla pat_history_neuro1", Order = 39)]
		[JsonProperty(PropertyName = "h2concoma")]
		public string H2Concoma { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2oriespacio de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2oriespacio", Description = " Propiedad publica de tipo string que representa a la columna h2oriespacio de la Tabla pat_history_neuro1", Order = 40)]
		[JsonProperty(PropertyName = "h2oriespacio")]
		public string H2Oriespacio { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2orilugar de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2orilugar", Description = " Propiedad publica de tipo string que representa a la columna h2orilugar de la Tabla pat_history_neuro1", Order = 41)]
		[JsonProperty(PropertyName = "h2orilugar")]
		public string H2Orilugar { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2oripersona de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2oripersona", Description = " Propiedad publica de tipo string que representa a la columna h2oripersona de la Tabla pat_history_neuro1", Order = 42)]
		[JsonProperty(PropertyName = "h2oripersona")]
		public string H2Oripersona { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2razidea de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2razidea", Description = " Propiedad publica de tipo string que representa a la columna h2razidea de la Tabla pat_history_neuro1", Order = 43)]
		[JsonProperty(PropertyName = "h2razidea")]
		public string H2Razidea { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2razjuicio de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2razjuicio", Description = " Propiedad publica de tipo string que representa a la columna h2razjuicio de la Tabla pat_history_neuro1", Order = 44)]
		[JsonProperty(PropertyName = "h2razjuicio")]
		public string H2Razjuicio { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2razabstraccion de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2razabstraccion", Description = " Propiedad publica de tipo string que representa a la columna h2razabstraccion de la Tabla pat_history_neuro1", Order = 45)]
		[JsonProperty(PropertyName = "h2razabstraccion")]
		public string H2Razabstraccion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2conocimientosgenerales de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h2conocimientosgenerales", Description = " Propiedad publica de tipo string que representa a la columna h2conocimientosgenerales de la Tabla pat_history_neuro1", Order = 46)]
		[JsonProperty(PropertyName = "h2conocimientosgenerales")]
		public string H2Conocimientosgenerales { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2atgdesatento de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2atgdesatento", Description = " Propiedad publica de tipo string que representa a la columna h2atgdesatento de la Tabla pat_history_neuro1", Order = 47)]
		[JsonProperty(PropertyName = "h2atgdesatento")]
		public string H2Atgdesatento { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2atgfluctuante de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2atgfluctuante", Description = " Propiedad publica de tipo string que representa a la columna h2atgfluctuante de la Tabla pat_history_neuro1", Order = 48)]
		[JsonProperty(PropertyName = "h2atgfluctuante")]
		public string H2Atgfluctuante { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2atgnegativo de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2atgnegativo", Description = " Propiedad publica de tipo string que representa a la columna h2atgnegativo de la Tabla pat_history_neuro1", Order = 49)]
		[JsonProperty(PropertyName = "h2atgnegativo")]
		public string H2Atgnegativo { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2atgnegligente de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2atgnegligente", Description = " Propiedad publica de tipo string que representa a la columna h2atgnegligente de la Tabla pat_history_neuro1", Order = 50)]
		[JsonProperty(PropertyName = "h2atgnegligente")]
		public string H2Atgnegligente { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2pagconfusion de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2pagconfusion", Description = " Propiedad publica de tipo string que representa a la columna h2pagconfusion de la Tabla pat_history_neuro1", Order = 51)]
		[JsonProperty(PropertyName = "h2pagconfusion")]
		public string H2Pagconfusion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2pagdelirio de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2pagdelirio", Description = " Propiedad publica de tipo string que representa a la columna h2pagdelirio de la Tabla pat_history_neuro1", Order = 52)]
		[JsonProperty(PropertyName = "h2pagdelirio")]
		public string H2Pagdelirio { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2paedelusiones de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2paedelusiones", Description = " Propiedad publica de tipo string que representa a la columna h2paedelusiones de la Tabla pat_history_neuro1", Order = 53)]
		[JsonProperty(PropertyName = "h2paedelusiones")]
		public string H2Paedelusiones { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2paeilsuiones de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2paeilsuiones", Description = " Propiedad publica de tipo string que representa a la columna h2paeilsuiones de la Tabla pat_history_neuro1", Order = 54)]
		[JsonProperty(PropertyName = "h2paeilsuiones")]
		public string H2Paeilsuiones { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2paealucinaciones de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2paealucinaciones", Description = " Propiedad publica de tipo string que representa a la columna h2paealucinaciones de la Tabla pat_history_neuro1", Order = 55)]
		[JsonProperty(PropertyName = "h2paealucinaciones")]
		public string H2Paealucinaciones { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3conreciente de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3conreciente", Description = " Propiedad publica de tipo string que representa a la columna h3conreciente de la Tabla pat_history_neuro1", Order = 56)]
		[JsonProperty(PropertyName = "h3conreciente")]
		public string H3Conreciente { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3conremota de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3conremota", Description = " Propiedad publica de tipo string que representa a la columna h3conremota de la Tabla pat_history_neuro1", Order = 57)]
		[JsonProperty(PropertyName = "h3conremota")]
		public string H3Conremota { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3afeeuforico de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3afeeuforico", Description = " Propiedad publica de tipo string que representa a la columna h3afeeuforico de la Tabla pat_history_neuro1", Order = 58)]
		[JsonProperty(PropertyName = "h3afeeuforico")]
		public string H3Afeeuforico { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3afeplano de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3afeplano", Description = " Propiedad publica de tipo string que representa a la columna h3afeplano de la Tabla pat_history_neuro1", Order = 59)]
		[JsonProperty(PropertyName = "h3afeplano")]
		public string H3Afeplano { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3afedeprimido de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3afedeprimido", Description = " Propiedad publica de tipo string que representa a la columna h3afedeprimido de la Tabla pat_history_neuro1", Order = 60)]
		[JsonProperty(PropertyName = "h3afedeprimido")]
		public string H3Afedeprimido { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3afelabil de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3afelabil", Description = " Propiedad publica de tipo string que representa a la columna h3afelabil de la Tabla pat_history_neuro1", Order = 61)]
		[JsonProperty(PropertyName = "h3afelabil")]
		public string H3Afelabil { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3afehostil de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3afehostil", Description = " Propiedad publica de tipo string que representa a la columna h3afehostil de la Tabla pat_history_neuro1", Order = 62)]
		[JsonProperty(PropertyName = "h3afehostil")]
		public string H3Afehostil { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3afeinadecuado de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3afeinadecuado", Description = " Propiedad publica de tipo string que representa a la columna h3afeinadecuado de la Tabla pat_history_neuro1", Order = 63)]
		[JsonProperty(PropertyName = "h3afeinadecuado")]
		public string H3Afeinadecuado { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3hdoizquierdo de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3hdoizquierdo", Description = " Propiedad publica de tipo string que representa a la columna h3hdoizquierdo de la Tabla pat_history_neuro1", Order = 64)]
		[JsonProperty(PropertyName = "h3hdoizquierdo")]
		public string H3Hdoizquierdo { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3hdoderecho de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3hdoderecho", Description = " Propiedad publica de tipo string que representa a la columna h3hdoderecho de la Tabla pat_history_neuro1", Order = 65)]
		[JsonProperty(PropertyName = "h3hdoderecho")]
		public string H3Hdoderecho { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3cgeexpresion de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3cgeexpresion", Description = " Propiedad publica de tipo string que representa a la columna h3cgeexpresion de la Tabla pat_history_neuro1", Order = 66)]
		[JsonProperty(PropertyName = "h3cgeexpresion")]
		public string H3Cgeexpresion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3cgenominacion de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3cgenominacion", Description = " Propiedad publica de tipo string que representa a la columna h3cgenominacion de la Tabla pat_history_neuro1", Order = 67)]
		[JsonProperty(PropertyName = "h3cgenominacion")]
		public string H3Cgenominacion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3cgerepeticion de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3cgerepeticion", Description = " Propiedad publica de tipo string que representa a la columna h3cgerepeticion de la Tabla pat_history_neuro1", Order = 68)]
		[JsonProperty(PropertyName = "h3cgerepeticion")]
		public string H3Cgerepeticion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3cgecomprension de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3cgecomprension", Description = " Propiedad publica de tipo string que representa a la columna h3cgecomprension de la Tabla pat_history_neuro1", Order = 69)]
		[JsonProperty(PropertyName = "h3cgecomprension")]
		public string H3Cgecomprension { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3apebroca de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3apebroca", Description = " Propiedad publica de tipo string que representa a la columna h3apebroca de la Tabla pat_history_neuro1", Order = 70)]
		[JsonProperty(PropertyName = "h3apebroca")]
		public string H3Apebroca { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3apeconduccion de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3apeconduccion", Description = " Propiedad publica de tipo string que representa a la columna h3apeconduccion de la Tabla pat_history_neuro1", Order = 71)]
		[JsonProperty(PropertyName = "h3apeconduccion")]
		public string H3Apeconduccion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3apewernicke de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3apewernicke", Description = " Propiedad publica de tipo string que representa a la columna h3apewernicke de la Tabla pat_history_neuro1", Order = 72)]
		[JsonProperty(PropertyName = "h3apewernicke")]
		public string H3Apewernicke { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3apeglobal de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3apeglobal", Description = " Propiedad publica de tipo string que representa a la columna h3apeglobal de la Tabla pat_history_neuro1", Order = 73)]
		[JsonProperty(PropertyName = "h3apeglobal")]
		public string H3Apeglobal { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3apeanomia de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3apeanomia", Description = " Propiedad publica de tipo string que representa a la columna h3apeanomia de la Tabla pat_history_neuro1", Order = 74)]
		[JsonProperty(PropertyName = "h3apeanomia")]
		public string H3Apeanomia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3atrmotora de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3atrmotora", Description = " Propiedad publica de tipo string que representa a la columna h3atrmotora de la Tabla pat_history_neuro1", Order = 75)]
		[JsonProperty(PropertyName = "h3atrmotora")]
		public string H3Atrmotora { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3atrsensitiva de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3atrsensitiva", Description = " Propiedad publica de tipo string que representa a la columna h3atrsensitiva de la Tabla pat_history_neuro1", Order = 76)]
		[JsonProperty(PropertyName = "h3atrsensitiva")]
		public string H3Atrsensitiva { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3atrmixta de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3atrmixta", Description = " Propiedad publica de tipo string que representa a la columna h3atrmixta de la Tabla pat_history_neuro1", Order = 77)]
		[JsonProperty(PropertyName = "h3atrmixta")]
		public string H3Atrmixta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3ataexpresion de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3ataexpresion", Description = " Propiedad publica de tipo string que representa a la columna h3ataexpresion de la Tabla pat_history_neuro1", Order = 78)]
		[JsonProperty(PropertyName = "h3ataexpresion")]
		public string H3Ataexpresion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3atacomprension de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3atacomprension", Description = " Propiedad publica de tipo string que representa a la columna h3atacomprension de la Tabla pat_history_neuro1", Order = 79)]
		[JsonProperty(PropertyName = "h3atacomprension")]
		public string H3Atacomprension { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3atamixta de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3atamixta", Description = " Propiedad publica de tipo string que representa a la columna h3atamixta de la Tabla pat_history_neuro1", Order = 80)]
		[JsonProperty(PropertyName = "h3atamixta")]
		public string H3Atamixta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3aprmotora de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3aprmotora", Description = " Propiedad publica de tipo string que representa a la columna h3aprmotora de la Tabla pat_history_neuro1", Order = 81)]
		[JsonProperty(PropertyName = "h3aprmotora")]
		public string H3Aprmotora { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3aprsensorial de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3aprsensorial", Description = " Propiedad publica de tipo string que representa a la columna h3aprsensorial de la Tabla pat_history_neuro1", Order = 82)]
		[JsonProperty(PropertyName = "h3aprsensorial")]
		public string H3Aprsensorial { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3lesalexia de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3lesalexia", Description = " Propiedad publica de tipo string que representa a la columna h3lesalexia de la Tabla pat_history_neuro1", Order = 83)]
		[JsonProperty(PropertyName = "h3lesalexia")]
		public string H3Lesalexia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3lesagrafia de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3lesagrafia", Description = " Propiedad publica de tipo string que representa a la columna h3lesagrafia de la Tabla pat_history_neuro1", Order = 84)]
		[JsonProperty(PropertyName = "h3lesagrafia")]
		public string H3Lesagrafia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3lpdacalculia de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3lpdacalculia", Description = " Propiedad publica de tipo string que representa a la columna h3lpdacalculia de la Tabla pat_history_neuro1", Order = 85)]
		[JsonProperty(PropertyName = "h3lpdacalculia")]
		public string H3Lpdacalculia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3lpdagrafia de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3lpdagrafia", Description = " Propiedad publica de tipo string que representa a la columna h3lpdagrafia de la Tabla pat_history_neuro1", Order = 86)]
		[JsonProperty(PropertyName = "h3lpdagrafia")]
		public string H3Lpdagrafia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3lpddesorientacion de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3lpddesorientacion", Description = " Propiedad publica de tipo string que representa a la columna h3lpddesorientacion de la Tabla pat_history_neuro1", Order = 87)]
		[JsonProperty(PropertyName = "h3lpddesorientacion")]
		public string H3Lpddesorientacion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3lpdagnosia de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3lpdagnosia", Description = " Propiedad publica de tipo string que representa a la columna h3lpdagnosia de la Tabla pat_history_neuro1", Order = 88)]
		[JsonProperty(PropertyName = "h3lpdagnosia")]
		public string H3Lpdagnosia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3agnauditiva de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3agnauditiva", Description = " Propiedad publica de tipo string que representa a la columna h3agnauditiva de la Tabla pat_history_neuro1", Order = 89)]
		[JsonProperty(PropertyName = "h3agnauditiva")]
		public string H3Agnauditiva { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3agnvisual de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3agnvisual", Description = " Propiedad publica de tipo string que representa a la columna h3agnvisual de la Tabla pat_history_neuro1", Order = 90)]
		[JsonProperty(PropertyName = "h3agnvisual")]
		public string H3Agnvisual { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3agntactil de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3agntactil", Description = " Propiedad publica de tipo string que representa a la columna h3agntactil de la Tabla pat_history_neuro1", Order = 91)]
		[JsonProperty(PropertyName = "h3agntactil")]
		public string H3Agntactil { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3aprideacional de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3aprideacional", Description = " Propiedad publica de tipo string que representa a la columna h3aprideacional de la Tabla pat_history_neuro1", Order = 92)]
		[JsonProperty(PropertyName = "h3aprideacional")]
		public string H3Aprideacional { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3aprideomotora de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3aprideomotora", Description = " Propiedad publica de tipo string que representa a la columna h3aprideomotora de la Tabla pat_history_neuro1", Order = 93)]
		[JsonProperty(PropertyName = "h3aprideomotora")]
		public string H3Aprideomotora { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3aprdesatencion de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3aprdesatencion", Description = " Propiedad publica de tipo string que representa a la columna h3aprdesatencion de la Tabla pat_history_neuro1", Order = 94)]
		[JsonProperty(PropertyName = "h3aprdesatencion")]
		public string H3Aprdesatencion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3anosognosia de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3anosognosia", Description = " Propiedad publica de tipo string que representa a la columna h3anosognosia de la Tabla pat_history_neuro1", Order = 95)]
		[JsonProperty(PropertyName = "h3anosognosia")]
		public string H3Anosognosia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna api_status de la Tabla pat_history_neuro1
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_status", Description = " Propiedad publica de tipo string que representa a la columna api_status de la Tabla pat_history_neuro1", Order = 96)]
		[JsonProperty(PropertyName = "api_status")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -api_status- porque es un campo requerido.")]
		public string ApiStatus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna api_transaction de la Tabla pat_history_neuro1
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(40, MinimumLength=0)]
		[Display(Name = "api_transaction", Description = " Propiedad publica de tipo string que representa a la columna api_transaction de la Tabla pat_history_neuro1", Order = 97)]
		[JsonProperty(PropertyName = "api_transaction")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -api_transaction- porque es un campo requerido.")]
		public string ApiTransaction { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna api_usucre de la Tabla pat_history_neuro1
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_usucre", Description = " Propiedad publica de tipo string que representa a la columna api_usucre de la Tabla pat_history_neuro1", Order = 98)]
		[JsonProperty(PropertyName = "api_usucre")]
		public string ApiUsucre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna api_feccre de la Tabla pat_history_neuro1
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "api_feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna api_feccre de la Tabla pat_history_neuro1", Order = 99)]
		[JsonProperty(PropertyName = "api_feccre")]
		public DateTime ApiFeccre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna api_usumod de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_usumod", Description = " Propiedad publica de tipo string que representa a la columna api_usumod de la Tabla pat_history_neuro1", Order = 100)]
		[JsonProperty(PropertyName = "api_usumod")]
		public string ApiUsumod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna api_fecmod de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "api_fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna api_fecmod de la Tabla pat_history_neuro1", Order = 101)]
		[JsonProperty(PropertyName = "api_fecmod")]
		public DateTime? ApiFecmod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna api_usudel de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_usudel", Description = " Propiedad publica de tipo string que representa a la columna api_usudel de la Tabla pat_history_neuro1", Order = 102)]
		[JsonProperty(PropertyName = "api_usudel")]
		public string ApiUsudel { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna api_fecdel de la Tabla pat_history_neuro1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "api_fecdel", Description = " Propiedad publica de tipo DateTime que representa a la columna api_fecdel de la Tabla pat_history_neuro1", Order = 103)]
		[JsonProperty(PropertyName = "api_fecdel")]
		public DateTime? ApiFecdel { get; set; } 

	}
}

