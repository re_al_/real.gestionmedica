#region 
/***********************************************************************************************************
	NOMBRE:       PatHistoryneuro3
	DESCRIPCION:
		Clase que define un objeto para la Tabla pat_history_neuro3

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        15/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using Newtonsoft.Json;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Entities
{
	[Table("pat_history_neuro3")]
	public class PatHistoryneuro3
	{
		public const string StrNombreTabla = "Pat_history_neuro3";
		public const string StrAliasTabla = "pat_history_neuro3";
		public const string StrDescripcionTabla = "pat_history_neuro3";
		public const string StrNombreLisForm = "Listado de pat_history_neuro3";
		public const string StrNombreDocForm = "Detalle de pat_history_neuro3";
		public const string StrNombreForm = "Registro de pat_history_neuro3";
		public enum Fields
		{
			IdHistoryNeuro3
			,IdPatients
			,H6Reflejos01
			,H6Reflejos02
			,H6Reflejos03
			,H6Reflejos04
			,H6Reflejos05
			,H6Reflejos06
			,H6Reflejos07
			,H6Reflejos08
			,H6Reflejos09
			,H6Reflejos10
			,H6Miotbicder
			,H6Miotbicizq
			,H6Miottrider
			,H6Miottriizq
			,H6Miotestder
			,H6Miotestizq
			,H6Miotpatder
			,H6Miotpatizq
			,H6Miotaquder
			,H6Miotaquizq
			,H6Patplader
			,H6Patplaizq
			,H6Pathofder
			,H6Pathofizq
			,H6Pattroder
			,H6Pattroizq
			,H6Patpreder
			,H6Patpreizq
			,H6Patpmender
			,H6Patpmenizq
			,H6Supbicder
			,H6Supbicizq
			,H6Suptrider
			,H6Suptriizq
			,H6Supestder
			,H6Supestizq
			,H6Otrmaxilar
			,H6Otrglabelar
			,H6Sistsensitivo
			,H6dolorprof
			,H6Posicion
			,H6Vibracion
			,H6dospuntos
			,H6Extincion
			,H6Estereognosia
			,H6Grafestesia
			,H6Localizacion
			,H6Imagen
			,H7Rigideznuca
			,H7Sigkerning
			,H7Fotofobia
			,H7Sigbrudzinski
			,H7Hiperestesia
			,H7Pulcarotideos
			,H7Pultemporales
			,H7Cabeza
			,H7Cuello
			,H7Nerviosperif
			,H7Colvertebral
			,H7Resumen
			,H7diagnosticos
			,H7Conducta
			,H7Tratamiento
			,ApiStatus
			,ApiTransaction
			,ApiUsucre
			,ApiFeccre
			,ApiUsumod
			,ApiFecmod
			,ApiUsudel
			,ApiFecdel
			,H6Imagenurl

		}
		
		#region Constructoress
		
		public PatHistoryneuro3()
		{
			//Inicializacion de Variables
			H6Reflejos01 = null;
			H6Reflejos02 = null;
			H6Reflejos03 = null;
			H6Reflejos04 = null;
			H6Reflejos05 = null;
			H6Reflejos06 = null;
			H6Reflejos07 = null;
			H6Reflejos08 = null;
			H6Reflejos09 = null;
			H6Reflejos10 = null;
			H6Miotbicder = null;
			H6Miotbicizq = null;
			H6Miottrider = null;
			H6Miottriizq = null;
			H6Miotestder = null;
			H6Miotestizq = null;
			H6Miotpatder = null;
			H6Miotpatizq = null;
			H6Miotaquder = null;
			H6Miotaquizq = null;
			H6Patplader = null;
			H6Patplaizq = null;
			H6Pathofder = null;
			H6Pathofizq = null;
			H6Pattroder = null;
			H6Pattroizq = null;
			H6Patpreder = null;
			H6Patpreizq = null;
			H6Patpmender = null;
			H6Patpmenizq = null;
			H6Supbicder = null;
			H6Supbicizq = null;
			H6Suptrider = null;
			H6Suptriizq = null;
			H6Supestder = null;
			H6Supestizq = null;
			H6Otrmaxilar = null;
			H6Otrglabelar = null;
			H6Sistsensitivo = null;
			H6dolorprof = null;
			H6Posicion = null;
			H6Vibracion = null;
			H6dospuntos = null;
			H6Extincion = null;
			H6Estereognosia = null;
			H6Grafestesia = null;
			H6Localizacion = null;
			//H6Imagen = null;
			H7Rigideznuca = null;
			H7Sigkerning = null;
			H7Fotofobia = null;
			H7Sigbrudzinski = null;
			H7Hiperestesia = null;
			H7Pulcarotideos = null;
			H7Pultemporales = null;
			H7Cabeza = null;
			H7Cuello = null;
			H7Nerviosperif = null;
			H7Colvertebral = null;
			H7Resumen = null;
			H7diagnosticos = null;
			H7Conducta = null;
			H7Tratamiento = null;
			ApiStatus = null;
			ApiTransaction = null;
			ApiUsucre = null;
			ApiUsumod = null;
			ApiFecmod = null;
			ApiUsudel = null;
			ApiFecdel = null;
			H6Imagenurl = null;
		}
		
		public PatHistoryneuro3(PatHistoryneuro3 obj)
		{
			IdHistoryNeuro3 = obj.IdHistoryNeuro3;
			IdPatients = obj.IdPatients;
			H6Reflejos01 = obj.H6Reflejos01;
			H6Reflejos02 = obj.H6Reflejos02;
			H6Reflejos03 = obj.H6Reflejos03;
			H6Reflejos04 = obj.H6Reflejos04;
			H6Reflejos05 = obj.H6Reflejos05;
			H6Reflejos06 = obj.H6Reflejos06;
			H6Reflejos07 = obj.H6Reflejos07;
			H6Reflejos08 = obj.H6Reflejos08;
			H6Reflejos09 = obj.H6Reflejos09;
			H6Reflejos10 = obj.H6Reflejos10;
			H6Miotbicder = obj.H6Miotbicder;
			H6Miotbicizq = obj.H6Miotbicizq;
			H6Miottrider = obj.H6Miottrider;
			H6Miottriizq = obj.H6Miottriizq;
			H6Miotestder = obj.H6Miotestder;
			H6Miotestizq = obj.H6Miotestizq;
			H6Miotpatder = obj.H6Miotpatder;
			H6Miotpatizq = obj.H6Miotpatizq;
			H6Miotaquder = obj.H6Miotaquder;
			H6Miotaquizq = obj.H6Miotaquizq;
			H6Patplader = obj.H6Patplader;
			H6Patplaizq = obj.H6Patplaizq;
			H6Pathofder = obj.H6Pathofder;
			H6Pathofizq = obj.H6Pathofizq;
			H6Pattroder = obj.H6Pattroder;
			H6Pattroizq = obj.H6Pattroizq;
			H6Patpreder = obj.H6Patpreder;
			H6Patpreizq = obj.H6Patpreizq;
			H6Patpmender = obj.H6Patpmender;
			H6Patpmenizq = obj.H6Patpmenizq;
			H6Supbicder = obj.H6Supbicder;
			H6Supbicizq = obj.H6Supbicizq;
			H6Suptrider = obj.H6Suptrider;
			H6Suptriizq = obj.H6Suptriizq;
			H6Supestder = obj.H6Supestder;
			H6Supestizq = obj.H6Supestizq;
			H6Otrmaxilar = obj.H6Otrmaxilar;
			H6Otrglabelar = obj.H6Otrglabelar;
			H6Sistsensitivo = obj.H6Sistsensitivo;
			H6dolorprof = obj.H6dolorprof;
			H6Posicion = obj.H6Posicion;
			H6Vibracion = obj.H6Vibracion;
			H6dospuntos = obj.H6dospuntos;
			H6Extincion = obj.H6Extincion;
			H6Estereognosia = obj.H6Estereognosia;
			H6Grafestesia = obj.H6Grafestesia;
			H6Localizacion = obj.H6Localizacion;
			//H6Imagen = obj.H6Imagen;
			H7Rigideznuca = obj.H7Rigideznuca;
			H7Sigkerning = obj.H7Sigkerning;
			H7Fotofobia = obj.H7Fotofobia;
			H7Sigbrudzinski = obj.H7Sigbrudzinski;
			H7Hiperestesia = obj.H7Hiperestesia;
			H7Pulcarotideos = obj.H7Pulcarotideos;
			H7Pultemporales = obj.H7Pultemporales;
			H7Cabeza = obj.H7Cabeza;
			H7Cuello = obj.H7Cuello;
			H7Nerviosperif = obj.H7Nerviosperif;
			H7Colvertebral = obj.H7Colvertebral;
			H7Resumen = obj.H7Resumen;
			H7diagnosticos = obj.H7diagnosticos;
			H7Conducta = obj.H7Conducta;
			H7Tratamiento = obj.H7Tratamiento;
			ApiStatus = obj.ApiStatus;
			ApiTransaction = obj.ApiTransaction;
			ApiUsucre = obj.ApiUsucre;
			ApiFeccre = obj.ApiFeccre;
			ApiUsumod = obj.ApiUsumod;
			ApiFecmod = obj.ApiFecmod;
			ApiUsudel = obj.ApiUsudel;
			ApiFecdel = obj.ApiFecdel;
			H6Imagenurl = obj.H6Imagenurl;
		}
		
		#endregion
		
		#region Metodos Estaticos
		#endregion
		
		#region Metodos Privados
		
		/// <summary>
		/// Obtiene el Hash a partir de un array de Bytes
		/// </summary>
		/// <param name="objectAsBytes"></param>
		/// <returns>string</returns>
		private string ComputeHash(byte[] objectAsBytes)
		{
			MD5 md5 = new MD5CryptoServiceProvider();
			try
			{
				byte[] result = md5.ComputeHash(objectAsBytes);
				
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < result.Length; i++)
				{
					sb.Append(result[i].ToString("X2"));
				}
				
				return sb.ToString();
			}
			catch (ArgumentNullException ane)
			{
				return null;
			}
		}
		
		/// <summary>
		///     Obtienen el Hash basado en algun algoritmo de Encriptación
		/// </summary>
		/// <typeparam name="T">
		///     Algoritmo de encriptación
		/// </typeparam>
		/// <param name="cryptoServiceProvider">
		///     Provvedor de Servicios de Criptografía
		/// </param>
		/// <returns>
		///     String que representa el Hash calculado
		        /// </returns>
		private string ComputeHash<T>( T cryptoServiceProvider) where T : HashAlgorithm, new()
		{
			DataContractSerializer serializer = new DataContractSerializer(this.GetType());
			using (MemoryStream memoryStream = new MemoryStream())
			{
				serializer.WriteObject(memoryStream, this);
				cryptoServiceProvider.ComputeHash(memoryStream.ToArray());
				return Convert.ToBase64String(cryptoServiceProvider.Hash);
			}
		}
		
		#endregion
		
		#region Overrides
		
		/// <summary>
		/// Devuelve un String que representa al Objeto
		/// </summary>
		/// <returns>string</returns>
		public override string ToString()
		{
			string hashString;
			
			//Evitar parametros NULL
			if (this == null)
				throw new ArgumentNullException("Parametro NULL no es valido");
			
			//Se verifica si el objeto es serializable.
			try
			{
				MemoryStream memStream = new MemoryStream();
				XmlSerializer serializer = new XmlSerializer(typeof(PatHistoryneuro3));
				serializer.Serialize(memStream, this);
				
				//Ahora se obtiene el Hash del Objeto.
				hashString = ComputeHash(memStream.ToArray());
				
				return hashString;
			}
			catch (AmbiguousMatchException ame)
			{
				throw new ApplicationException("El Objeto no es Serializable. Message:" + ame.Message);
			}
		}
		
		/// <summary>
		/// Verifica que dos objetos sean identicos
		/// </summary>
		public static bool operator ==(PatHistoryneuro3 first, PatHistoryneuro3 second)
		{
			// Verifica si el puntero en memoria es el mismo
			if (Object.ReferenceEquals(first, second))
				return true;
			
			// Verifica valores nulos
			if ((object) first == null || (object) second == null)
				return false;
			
			return first.GetHashCode() == second.GetHashCode();
		}
		
		/// <summary>
		/// Verifica que dos objetos sean distintos
		/// </summary>
		public static bool operator !=(PatHistoryneuro3 first, PatHistoryneuro3 second)
		{
			return !(first == second);
		}
		
		/// <summary>
		/// Compara este objeto con otro
		/// </summary>
		/// <param name="obj">El objeto a comparar</param>
		/// <returns>Devuelve Verdadero si ambos objetos son iguales</returns>
		public override bool Equals(object obj)
		{
			if (obj == null)
				return false;
			
			if (obj.GetType() == this.GetType())
				return obj.GetHashCode() == this.GetHashCode();
			
			return false;
		}
		
		#endregion
		
		#region Encriptacion
		
		/// <summary>
		/// Un valor unico que identifica cada instancia de este objeto
		/// </summary>
		/// <returns>Unico valor entero</returns>
		public override int GetHashCode()
		{
			return (this.GetSha512Hash().GetHashCode());
		}
		
		/// <summary>
		///     Obtiene el Hash de la Instancia Actual
		/// </summary>
		/// <typeparam name="T">
		///     Tipo de Proveedor de servicios criptográficos
		/// </typeparam>
		/// <returns>
		///     String en Base64 que representa el Hash
		/// </returns>
		public string GetHash<T>() where T : HashAlgorithm, new()
		{
			T cryptoServiceProvider = new T();
			return ComputeHash(cryptoServiceProvider);
		}
		
		/// <summary>
		///     Obtiene el codigo Hash en Base al Algoritmo SHA1
		/// </summary>
		/// <returns>
		///     String en Base64 que representa el Hash en SHA1
		/// </returns>
		public string GetSha1Hash()
		{
			return GetHash<SHA1CryptoServiceProvider>();
		}
		
		/// <summary>
		///     Obtiene el codigo Hash en Base al Algoritmo SHA512
		/// </summary>
		/// <returns>
		///     String en Base64 que representa el Hash en SHA512
		/// </returns>
		public string GetSha512Hash()
		{
			return GetHash<SHA512CryptoServiceProvider>();
		}
		
		/// <summary>
		///     Obtiene el codigo Hash en Base al Algoritmo MD5
		/// </summary>
		/// <returns>
		///     String en Base64 que representa el Hash en MD5
		/// </returns>
		public string GetMd5Hash()
		{
			return GetHash<MD5CryptoServiceProvider>();
		}
		
		
		#endregion
		
		#region DataAnnotations
		
		/// <summary>
		/// Obtiene los errores basado en los DataAnnotations 
		/// </summary>
		/// <returns>Devuelve un IList del tipo ValidationResult con los errores obtenidos</returns>
		public IList<ValidationResult> ValidationErrors()
		{
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			if (!Validator.TryValidateObject(this, context, errors, true))
				return errors;
			
			return new List<ValidationResult>(0);
		}
		
		/// <summary>
		/// Obtiene los errores basado en los DataAnnotations 
		/// </summary>
		/// <returns>Devuelve un String con los errores obtenidos</returns>
		public string ValidationErrorsString()
		{
			string strErrors = "";
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			if (!Validator.TryValidateObject(this, context, errors, true))
			{
				foreach (ValidationResult result in errors)
					strErrors = strErrors + result.ErrorMessage + Environment.NewLine;
			}
			return strErrors;
		}
		
		/// <summary>
		/// Funcion que determina si un objeto es valido o no
		/// </summary>
		/// <returns>Resultado de la validacion</returns>
		public bool IsValid()
		{
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			return Validator.TryValidateObject(this, context, errors, true);
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna id_history_neuro3 de la Tabla pat_history_neuro3
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_history_neuro3", Description = " Propiedad publica de tipo Int64 que representa a la columna id_history_neuro3 de la Tabla pat_history_neuro3", Order = 1)]
		[JsonProperty(PropertyName = "id_history_neuro3")]
		[Required(ErrorMessage = "Se necesita un valor para -id_history_neuro3- porque es un campo requerido.")]
		[Key]
		public Int64 IdHistoryNeuro3 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna id_patients de la Tabla pat_history_neuro3
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_patients", Description = " Propiedad publica de tipo Int64 que representa a la columna id_patients de la Tabla pat_history_neuro3", Order = 2)]
		[JsonProperty(PropertyName = "id_patients")]
		[Required(ErrorMessage = "Se necesita un valor para -id_patients- porque es un campo requerido.")]
		public Int64 IdPatients { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos01 de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos01", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos01 de la Tabla pat_history_neuro3", Order = 3)]
		[JsonProperty(PropertyName = "h6reflejos01")]
		public string H6Reflejos01 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos02 de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos02", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos02 de la Tabla pat_history_neuro3", Order = 4)]
		[JsonProperty(PropertyName = "h6reflejos02")]
		public string H6Reflejos02 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos03 de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos03", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos03 de la Tabla pat_history_neuro3", Order = 5)]
		[JsonProperty(PropertyName = "h6reflejos03")]
		public string H6Reflejos03 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos04 de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos04", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos04 de la Tabla pat_history_neuro3", Order = 6)]
		[JsonProperty(PropertyName = "h6reflejos04")]
		public string H6Reflejos04 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos05 de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos05", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos05 de la Tabla pat_history_neuro3", Order = 7)]
		[JsonProperty(PropertyName = "h6reflejos05")]
		public string H6Reflejos05 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos06 de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos06", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos06 de la Tabla pat_history_neuro3", Order = 8)]
		[JsonProperty(PropertyName = "h6reflejos06")]
		public string H6Reflejos06 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos07 de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos07", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos07 de la Tabla pat_history_neuro3", Order = 9)]
		[JsonProperty(PropertyName = "h6reflejos07")]
		public string H6Reflejos07 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos08 de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos08", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos08 de la Tabla pat_history_neuro3", Order = 10)]
		[JsonProperty(PropertyName = "h6reflejos08")]
		public string H6Reflejos08 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos09 de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos09", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos09 de la Tabla pat_history_neuro3", Order = 11)]
		[JsonProperty(PropertyName = "h6reflejos09")]
		public string H6Reflejos09 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos10 de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos10", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos10 de la Tabla pat_history_neuro3", Order = 12)]
		[JsonProperty(PropertyName = "h6reflejos10")]
		public string H6Reflejos10 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotbicder de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotbicder", Description = " Propiedad publica de tipo string que representa a la columna h6miotbicder de la Tabla pat_history_neuro3", Order = 13)]
		[JsonProperty(PropertyName = "h6miotbicder")]
		public string H6Miotbicder { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotbicizq de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotbicizq", Description = " Propiedad publica de tipo string que representa a la columna h6miotbicizq de la Tabla pat_history_neuro3", Order = 14)]
		[JsonProperty(PropertyName = "h6miotbicizq")]
		public string H6Miotbicizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miottrider de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miottrider", Description = " Propiedad publica de tipo string que representa a la columna h6miottrider de la Tabla pat_history_neuro3", Order = 15)]
		[JsonProperty(PropertyName = "h6miottrider")]
		public string H6Miottrider { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miottriizq de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miottriizq", Description = " Propiedad publica de tipo string que representa a la columna h6miottriizq de la Tabla pat_history_neuro3", Order = 16)]
		[JsonProperty(PropertyName = "h6miottriizq")]
		public string H6Miottriizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotestder de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotestder", Description = " Propiedad publica de tipo string que representa a la columna h6miotestder de la Tabla pat_history_neuro3", Order = 17)]
		[JsonProperty(PropertyName = "h6miotestder")]
		public string H6Miotestder { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotestizq de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotestizq", Description = " Propiedad publica de tipo string que representa a la columna h6miotestizq de la Tabla pat_history_neuro3", Order = 18)]
		[JsonProperty(PropertyName = "h6miotestizq")]
		public string H6Miotestizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotpatder de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotpatder", Description = " Propiedad publica de tipo string que representa a la columna h6miotpatder de la Tabla pat_history_neuro3", Order = 19)]
		[JsonProperty(PropertyName = "h6miotpatder")]
		public string H6Miotpatder { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotpatizq de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotpatizq", Description = " Propiedad publica de tipo string que representa a la columna h6miotpatizq de la Tabla pat_history_neuro3", Order = 20)]
		[JsonProperty(PropertyName = "h6miotpatizq")]
		public string H6Miotpatizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotaquder de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotaquder", Description = " Propiedad publica de tipo string que representa a la columna h6miotaquder de la Tabla pat_history_neuro3", Order = 21)]
		[JsonProperty(PropertyName = "h6miotaquder")]
		public string H6Miotaquder { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotaquizq de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotaquizq", Description = " Propiedad publica de tipo string que representa a la columna h6miotaquizq de la Tabla pat_history_neuro3", Order = 22)]
		[JsonProperty(PropertyName = "h6miotaquizq")]
		public string H6Miotaquizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6patplader de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6patplader", Description = " Propiedad publica de tipo string que representa a la columna h6patplader de la Tabla pat_history_neuro3", Order = 23)]
		[JsonProperty(PropertyName = "h6patplader")]
		public string H6Patplader { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6patplaizq de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6patplaizq", Description = " Propiedad publica de tipo string que representa a la columna h6patplaizq de la Tabla pat_history_neuro3", Order = 24)]
		[JsonProperty(PropertyName = "h6patplaizq")]
		public string H6Patplaizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6pathofder de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6pathofder", Description = " Propiedad publica de tipo string que representa a la columna h6pathofder de la Tabla pat_history_neuro3", Order = 25)]
		[JsonProperty(PropertyName = "h6pathofder")]
		public string H6Pathofder { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6pathofizq de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6pathofizq", Description = " Propiedad publica de tipo string que representa a la columna h6pathofizq de la Tabla pat_history_neuro3", Order = 26)]
		[JsonProperty(PropertyName = "h6pathofizq")]
		public string H6Pathofizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6pattroder de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6pattroder", Description = " Propiedad publica de tipo string que representa a la columna h6pattroder de la Tabla pat_history_neuro3", Order = 27)]
		[JsonProperty(PropertyName = "h6pattroder")]
		public string H6Pattroder { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6pattroizq de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6pattroizq", Description = " Propiedad publica de tipo string que representa a la columna h6pattroizq de la Tabla pat_history_neuro3", Order = 28)]
		[JsonProperty(PropertyName = "h6pattroizq")]
		public string H6Pattroizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6patpreder de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6patpreder", Description = " Propiedad publica de tipo string que representa a la columna h6patpreder de la Tabla pat_history_neuro3", Order = 29)]
		[JsonProperty(PropertyName = "h6patpreder")]
		public string H6Patpreder { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6patpreizq de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6patpreizq", Description = " Propiedad publica de tipo string que representa a la columna h6patpreizq de la Tabla pat_history_neuro3", Order = 30)]
		[JsonProperty(PropertyName = "h6patpreizq")]
		public string H6Patpreizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6patpmender de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6patpmender", Description = " Propiedad publica de tipo string que representa a la columna h6patpmender de la Tabla pat_history_neuro3", Order = 31)]
		[JsonProperty(PropertyName = "h6patpmender")]
		public string H6Patpmender { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6patpmenizq de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6patpmenizq", Description = " Propiedad publica de tipo string que representa a la columna h6patpmenizq de la Tabla pat_history_neuro3", Order = 32)]
		[JsonProperty(PropertyName = "h6patpmenizq")]
		public string H6Patpmenizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6supbicder de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6supbicder", Description = " Propiedad publica de tipo string que representa a la columna h6supbicder de la Tabla pat_history_neuro3", Order = 33)]
		[JsonProperty(PropertyName = "h6supbicder")]
		public string H6Supbicder { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6supbicizq de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6supbicizq", Description = " Propiedad publica de tipo string que representa a la columna h6supbicizq de la Tabla pat_history_neuro3", Order = 34)]
		[JsonProperty(PropertyName = "h6supbicizq")]
		public string H6Supbicizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6suptrider de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6suptrider", Description = " Propiedad publica de tipo string que representa a la columna h6suptrider de la Tabla pat_history_neuro3", Order = 35)]
		[JsonProperty(PropertyName = "h6suptrider")]
		public string H6Suptrider { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6suptriizq de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6suptriizq", Description = " Propiedad publica de tipo string que representa a la columna h6suptriizq de la Tabla pat_history_neuro3", Order = 36)]
		[JsonProperty(PropertyName = "h6suptriizq")]
		public string H6Suptriizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6supestder de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6supestder", Description = " Propiedad publica de tipo string que representa a la columna h6supestder de la Tabla pat_history_neuro3", Order = 37)]
		[JsonProperty(PropertyName = "h6supestder")]
		public string H6Supestder { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6supestizq de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6supestizq", Description = " Propiedad publica de tipo string que representa a la columna h6supestizq de la Tabla pat_history_neuro3", Order = 38)]
		[JsonProperty(PropertyName = "h6supestizq")]
		public string H6Supestizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6otrmaxilar de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h6otrmaxilar", Description = " Propiedad publica de tipo string que representa a la columna h6otrmaxilar de la Tabla pat_history_neuro3", Order = 39)]
		[JsonProperty(PropertyName = "h6otrmaxilar")]
		public string H6Otrmaxilar { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6otrglabelar de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h6otrglabelar", Description = " Propiedad publica de tipo string que representa a la columna h6otrglabelar de la Tabla pat_history_neuro3", Order = 40)]
		[JsonProperty(PropertyName = "h6otrglabelar")]
		public string H6Otrglabelar { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6sistsensitivo de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6sistsensitivo", Description = " Propiedad publica de tipo string que representa a la columna h6sistsensitivo de la Tabla pat_history_neuro3", Order = 41)]
		[JsonProperty(PropertyName = "h6sistsensitivo")]
		public string H6Sistsensitivo { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6dolorprof de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6dolorprof", Description = " Propiedad publica de tipo string que representa a la columna h6dolorprof de la Tabla pat_history_neuro3", Order = 42)]
		[JsonProperty(PropertyName = "h6dolorprof")]
		public string H6dolorprof { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6posicion de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6posicion", Description = " Propiedad publica de tipo string que representa a la columna h6posicion de la Tabla pat_history_neuro3", Order = 43)]
		[JsonProperty(PropertyName = "h6posicion")]
		public string H6Posicion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6vibracion de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6vibracion", Description = " Propiedad publica de tipo string que representa a la columna h6vibracion de la Tabla pat_history_neuro3", Order = 44)]
		[JsonProperty(PropertyName = "h6vibracion")]
		public string H6Vibracion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6dospuntos de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6dospuntos", Description = " Propiedad publica de tipo string que representa a la columna h6dospuntos de la Tabla pat_history_neuro3", Order = 45)]
		[JsonProperty(PropertyName = "h6dospuntos")]
		public string H6dospuntos { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6extincion de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6extincion", Description = " Propiedad publica de tipo string que representa a la columna h6extincion de la Tabla pat_history_neuro3", Order = 46)]
		[JsonProperty(PropertyName = "h6extincion")]
		public string H6Extincion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6estereognosia de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6estereognosia", Description = " Propiedad publica de tipo string que representa a la columna h6estereognosia de la Tabla pat_history_neuro3", Order = 47)]
		[JsonProperty(PropertyName = "h6estereognosia")]
		public string H6Estereognosia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6grafestesia de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6grafestesia", Description = " Propiedad publica de tipo string que representa a la columna h6grafestesia de la Tabla pat_history_neuro3", Order = 48)]
		[JsonProperty(PropertyName = "h6grafestesia")]
		public string H6Grafestesia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6localizacion de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6localizacion", Description = " Propiedad publica de tipo string que representa a la columna h6localizacion de la Tabla pat_history_neuro3", Order = 49)]
		[JsonProperty(PropertyName = "h6localizacion")]
		public string H6Localizacion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Byte[] que representa a la columna h6imagen de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		//[Display(Name = "h6imagen", Description = " Propiedad publica de tipo Byte[] que representa a la columna h6imagen de la Tabla pat_history_neuro3", Order = -1)]
		//[JsonProperty(PropertyName = "h6imagen")]
		//public Byte[] H6Imagen { get; set; }

       
        /// <summary>
        /// Propiedad publica de tipo string que representa a la columna h7rigideznuca de la Tabla pat_history_neuro3
        /// Permite Null: Yes
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7rigideznuca", Description = " Propiedad publica de tipo string que representa a la columna h7rigideznuca de la Tabla pat_history_neuro3", Order = 51)]
		[JsonProperty(PropertyName = "h7rigideznuca")]
		public string H7Rigideznuca { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7sigkerning de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7sigkerning", Description = " Propiedad publica de tipo string que representa a la columna h7sigkerning de la Tabla pat_history_neuro3", Order = 52)]
		[JsonProperty(PropertyName = "h7sigkerning")]
		public string H7Sigkerning { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7fotofobia de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7fotofobia", Description = " Propiedad publica de tipo string que representa a la columna h7fotofobia de la Tabla pat_history_neuro3", Order = 53)]
		[JsonProperty(PropertyName = "h7fotofobia")]
		public string H7Fotofobia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7sigbrudzinski de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7sigbrudzinski", Description = " Propiedad publica de tipo string que representa a la columna h7sigbrudzinski de la Tabla pat_history_neuro3", Order = 54)]
		[JsonProperty(PropertyName = "h7sigbrudzinski")]
		public string H7Sigbrudzinski { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7hiperestesia de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7hiperestesia", Description = " Propiedad publica de tipo string que representa a la columna h7hiperestesia de la Tabla pat_history_neuro3", Order = 55)]
		[JsonProperty(PropertyName = "h7hiperestesia")]
		public string H7Hiperestesia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7pulcarotideos de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7pulcarotideos", Description = " Propiedad publica de tipo string que representa a la columna h7pulcarotideos de la Tabla pat_history_neuro3", Order = 56)]
		[JsonProperty(PropertyName = "h7pulcarotideos")]
		public string H7Pulcarotideos { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7pultemporales de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7pultemporales", Description = " Propiedad publica de tipo string que representa a la columna h7pultemporales de la Tabla pat_history_neuro3", Order = 57)]
		[JsonProperty(PropertyName = "h7pultemporales")]
		public string H7Pultemporales { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7cabeza de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7cabeza", Description = " Propiedad publica de tipo string que representa a la columna h7cabeza de la Tabla pat_history_neuro3", Order = 58)]
		[JsonProperty(PropertyName = "h7cabeza")]
		public string H7Cabeza { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7cuello de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7cuello", Description = " Propiedad publica de tipo string que representa a la columna h7cuello de la Tabla pat_history_neuro3", Order = 59)]
		[JsonProperty(PropertyName = "h7cuello")]
		public string H7Cuello { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7nerviosperif de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h7nerviosperif", Description = " Propiedad publica de tipo string que representa a la columna h7nerviosperif de la Tabla pat_history_neuro3", Order = 60)]
		[JsonProperty(PropertyName = "h7nerviosperif")]
		public string H7Nerviosperif { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7colvertebral de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h7colvertebral", Description = " Propiedad publica de tipo string que representa a la columna h7colvertebral de la Tabla pat_history_neuro3", Order = 61)]
		[JsonProperty(PropertyName = "h7colvertebral")]
		public string H7Colvertebral { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7resumen de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h7resumen", Description = " Propiedad publica de tipo string que representa a la columna h7resumen de la Tabla pat_history_neuro3", Order = 62)]
		[JsonProperty(PropertyName = "h7resumen")]
		public string H7Resumen { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7diagnosticos de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h7diagnosticos", Description = " Propiedad publica de tipo string que representa a la columna h7diagnosticos de la Tabla pat_history_neuro3", Order = 63)]
		[JsonProperty(PropertyName = "h7diagnosticos")]
		public string H7diagnosticos { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7conducta de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h7conducta", Description = " Propiedad publica de tipo string que representa a la columna h7conducta de la Tabla pat_history_neuro3", Order = 64)]
		[JsonProperty(PropertyName = "h7conducta")]
		public string H7Conducta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7tratamiento de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h7tratamiento", Description = " Propiedad publica de tipo string que representa a la columna h7tratamiento de la Tabla pat_history_neuro3", Order = 65)]
		[JsonProperty(PropertyName = "h7tratamiento")]
		public string H7Tratamiento { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna api_status de la Tabla pat_history_neuro3
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_status", Description = " Propiedad publica de tipo string que representa a la columna api_status de la Tabla pat_history_neuro3", Order = 66)]
		[JsonProperty(PropertyName = "api_status")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -api_status- porque es un campo requerido.")]
		public string ApiStatus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna api_transaction de la Tabla pat_history_neuro3
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(40, MinimumLength=0)]
		[Display(Name = "api_transaction", Description = " Propiedad publica de tipo string que representa a la columna api_transaction de la Tabla pat_history_neuro3", Order = 67)]
		[JsonProperty(PropertyName = "api_transaction")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -api_transaction- porque es un campo requerido.")]
		public string ApiTransaction { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna api_usucre de la Tabla pat_history_neuro3
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_usucre", Description = " Propiedad publica de tipo string que representa a la columna api_usucre de la Tabla pat_history_neuro3", Order = 68)]
		[JsonProperty(PropertyName = "api_usucre")]
		public string ApiUsucre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna api_feccre de la Tabla pat_history_neuro3
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "api_feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna api_feccre de la Tabla pat_history_neuro3", Order = 69)]
		[JsonProperty(PropertyName = "api_feccre")]
		public DateTime ApiFeccre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna api_usumod de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_usumod", Description = " Propiedad publica de tipo string que representa a la columna api_usumod de la Tabla pat_history_neuro3", Order = 70)]
		[JsonProperty(PropertyName = "api_usumod")]
		public string ApiUsumod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna api_fecmod de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "api_fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna api_fecmod de la Tabla pat_history_neuro3", Order = 71)]
		[JsonProperty(PropertyName = "api_fecmod")]
		public DateTime? ApiFecmod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna api_usudel de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_usudel", Description = " Propiedad publica de tipo string que representa a la columna api_usudel de la Tabla pat_history_neuro3", Order = 72)]
		[JsonProperty(PropertyName = "api_usudel")]
		public string ApiUsudel { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna api_fecdel de la Tabla pat_history_neuro3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "api_fecdel", Description = " Propiedad publica de tipo DateTime que representa a la columna api_fecdel de la Tabla pat_history_neuro3", Order = 73)]
		[JsonProperty(PropertyName = "api_fecdel")]
		public DateTime? ApiFecdel { get; set; } 

		/// <summary>
		/// External URL for image
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(512, MinimumLength=0)]
		[Display(Name = "h6imagenurl", Description = "External URL for image", Order = 74)]
		[JsonProperty(PropertyName = "h6imagenurl")]
		public string H6Imagenurl { get; set; } 

	}
}

