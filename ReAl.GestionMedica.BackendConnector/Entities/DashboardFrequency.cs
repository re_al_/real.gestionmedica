#region 
/***********************************************************************************************************
	NOMBRE:       ClaBloodtypes
	DESCRIPCION:
		Clase que define un objeto para la Tabla cla_blood_types

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        05/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Utils;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Entities
{
	
	public class DashboardFrequency
    {
        [JsonProperty("Nombres")]
        public string Nombres { get; set; }

        [JsonProperty("Apellidos")]
        public string Apellidos { get; set; }

        [JsonProperty("Consultas")]
        public int Consultas { get; set; }
        
        [JsonProperty("Ingreso")]
        public Decimal Ingreso { get; set; }
        
        [JsonProperty("Visita")]
        public DateTime Visita { get; set; }


        //public string consultation_date { get; set; }
    }
}

