﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReAl.GestionMedica.BackendConnector.Entities
{
    public class FilterChangePassword
    {
        [JsonIgnore]
        public Int64 IdUsers { get; set; }

        [JsonProperty("old_password")]
        public string OldPassword { get; set; }

        [JsonProperty("new_password")]
        public string NewPassword { get; set; }
    }
}
