#region 
/***********************************************************************************************************
	NOMBRE:       ClaBloodtypes
	DESCRIPCION:
		Clase que define un objeto para la Tabla cla_blood_types

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        05/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using ReAl.GestionMedica.BackendConnector.Utils;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Entities
{
	
	public class DashboardAbsence
    {
        
        [JsonProperty(PropertyName = "Nombres")]
        public string Nombres { get; set; }
        
        [JsonProperty(PropertyName = "Apellidos")]
        public string Apellidos { get; set; }

        [JsonProperty(PropertyName = "Fecha")]
        public DateTime Fecha { get; set; }

        [JsonProperty(PropertyName = "Motivo")]
        public string Motivo { get; set; }
        //public string time_start { get; set; }
    }
}

