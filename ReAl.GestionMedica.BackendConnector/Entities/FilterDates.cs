﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReAl.GestionMedica.BackendConnector.Entities
{
    public class FilterDates
    {
        [JsonProperty("dates")]
        public List<string> Dates { get; set; }
    }
}
