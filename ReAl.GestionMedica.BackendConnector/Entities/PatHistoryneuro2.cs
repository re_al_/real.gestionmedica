#region 
/***********************************************************************************************************
	NOMBRE:       PatHistoryneuro2
	DESCRIPCION:
		Clase que define un objeto para la Tabla pat_history_neuro2

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        15/01/2024  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
using Newtonsoft.Json;
#endregion

namespace ReAl.GestionMedica.BackendConnector.Entities
{
	[Table("pat_history_neuro2")]
	public class PatHistoryneuro2
	{
		public const string StrNombreTabla = "Pat_history_neuro2";
		public const string StrAliasTabla = "pat_history_neuro2";
		public const string StrDescripcionTabla = "pat_history_neuro2";
		public const string StrNombreLisForm = "Listado de pat_history_neuro2";
		public const string StrNombreDocForm = "Detalle de pat_history_neuro2";
		public const string StrNombreForm = "Registro de pat_history_neuro2";
		public enum Fields
		{
			IdHistoryNeuro2
			,IdPatients
			,H4Olfnormal
			,H4Olfanosmia
			,H4Optscod
			,H4Optscoi
			,H4Optccod
			,H4Optccoi
			,H4Fondood
			,H4Fondooi
			,H4Campimetria
			,H4Prosis
			,H4Posicionojos
			,H4Exoftalmia
			,H4Horner
			,H4Movoculares
			,H4Nistagmux
			,H4diplopia1
			,H4diplopia2
			,H4diplopia3
			,H4diplopia4
			,H4diplopia5
			,H4diplopia6
			,H4diplopia7
			,H4diplopia8
			,H4diplopia9
			,H4Convergencia
			,H4Acomodacionod
			,H4Acomodacionoi
			,H4Pupulasod
			,H4Pupulasoi
			,H4Forma
			,H4Fotomotorod
			,H4Fotomotoroi
			,H4Consensualdai
			,H4Consensualiad
			,H4Senfacialder
			,H4Senfacializq
			,H4Reflejoder
			,H4Reflejoizq
			,H4Aberturaboca
			,H4Movmasticatorios
			,H4Refmentoniano
			,H4Facsimetria
			,H4Facmovimientos
			,H4Facparalisis
			,H4Faccentral
			,H4Facperiferica
			,H4Facgusto
			,H5Otoscopia
			,H5Aguaudider
			,H5Aguaudiizq
			,H5Weber
			,H5Rinne
			,H5Pruebaslab
			,H5Elevpaladar
			,H5Uvula
			,H5Refnauceoso
			,H5deglucion
			,H5Tonovoz
			,H5Esternocleidomastoideo
			,H5Trapecio
			,H5desviacion
			,H5Atrofia
			,H5Fasciculacion
			,H5Fuerza
			,H5Marcha
			,H5Tono
			,H5Volumen
			,H5Fasciculaciones
			,H5Fuemuscular
			,H5Movinvoluntarios
			,H5Equilibratoria
			,H5Romberg
			,H5dednarder
			,H5dednarizq
			,H5deddedder
			,H5deddedizq
			,H5Talrodder
			,H5Talrodizq
			,H5Movrapid
			,H5Rebote
			,H5Habilidadespecif
			,ApiStatus
			,ApiTransaction
			,ApiUsucre
			,ApiFeccre
			,ApiUsumod
			,ApiFecmod
			,ApiUsudel
			,ApiFecdel

		}
		
		#region Constructoress
		
		public PatHistoryneuro2()
		{
			//Inicializacion de Variables
			H4Olfnormal = null;
			H4Olfanosmia = null;
			H4Optscod = null;
			H4Optscoi = null;
			H4Optccod = null;
			H4Optccoi = null;
			H4Fondood = null;
			H4Fondooi = null;
			H4Campimetria = null;
			H4Prosis = null;
			H4Posicionojos = null;
			H4Exoftalmia = null;
			H4Horner = null;
			H4Movoculares = null;
			H4Nistagmux = null;
			H4diplopia1 = null;
			H4diplopia2 = null;
			H4diplopia3 = null;
			H4diplopia4 = null;
			H4diplopia5 = null;
			H4diplopia6 = null;
			H4diplopia7 = null;
			H4diplopia8 = null;
			H4diplopia9 = null;
			H4Convergencia = null;
			H4Acomodacionod = null;
			H4Acomodacionoi = null;
			H4Pupulasod = null;
			H4Pupulasoi = null;
			H4Forma = null;
			H4Fotomotorod = null;
			H4Fotomotoroi = null;
			H4Consensualdai = null;
			H4Consensualiad = null;
			H4Senfacialder = null;
			H4Senfacializq = null;
			H4Reflejoder = null;
			H4Reflejoizq = null;
			H4Aberturaboca = null;
			H4Movmasticatorios = null;
			H4Refmentoniano = null;
			H4Facsimetria = null;
			H4Facmovimientos = null;
			H4Facparalisis = null;
			H4Faccentral = null;
			H4Facperiferica = null;
			H4Facgusto = null;
			H5Otoscopia = null;
			H5Aguaudider = null;
			H5Aguaudiizq = null;
			H5Weber = null;
			H5Rinne = null;
			H5Pruebaslab = null;
			H5Elevpaladar = null;
			H5Uvula = null;
			H5Refnauceoso = null;
			H5deglucion = null;
			H5Tonovoz = null;
			H5Esternocleidomastoideo = null;
			H5Trapecio = null;
			H5desviacion = null;
			H5Atrofia = null;
			H5Fasciculacion = null;
			H5Fuerza = null;
			H5Marcha = null;
			H5Tono = null;
			H5Volumen = null;
			H5Fasciculaciones = null;
			H5Fuemuscular = null;
			H5Movinvoluntarios = null;
			H5Equilibratoria = null;
			H5Romberg = null;
			H5dednarder = null;
			H5dednarizq = null;
			H5deddedder = null;
			H5deddedizq = null;
			H5Talrodder = null;
			H5Talrodizq = null;
			H5Movrapid = null;
			H5Rebote = null;
			H5Habilidadespecif = null;
			ApiStatus = null;
			ApiTransaction = null;
			ApiUsucre = null;
			ApiUsumod = null;
			ApiFecmod = null;
			ApiUsudel = null;
			ApiFecdel = null;
		}
		
		public PatHistoryneuro2(PatHistoryneuro2 obj)
		{
			IdHistoryNeuro2 = obj.IdHistoryNeuro2;
			IdPatients = obj.IdPatients;
			H4Olfnormal = obj.H4Olfnormal;
			H4Olfanosmia = obj.H4Olfanosmia;
			H4Optscod = obj.H4Optscod;
			H4Optscoi = obj.H4Optscoi;
			H4Optccod = obj.H4Optccod;
			H4Optccoi = obj.H4Optccoi;
			H4Fondood = obj.H4Fondood;
			H4Fondooi = obj.H4Fondooi;
			H4Campimetria = obj.H4Campimetria;
			H4Prosis = obj.H4Prosis;
			H4Posicionojos = obj.H4Posicionojos;
			H4Exoftalmia = obj.H4Exoftalmia;
			H4Horner = obj.H4Horner;
			H4Movoculares = obj.H4Movoculares;
			H4Nistagmux = obj.H4Nistagmux;
			H4diplopia1 = obj.H4diplopia1;
			H4diplopia2 = obj.H4diplopia2;
			H4diplopia3 = obj.H4diplopia3;
			H4diplopia4 = obj.H4diplopia4;
			H4diplopia5 = obj.H4diplopia5;
			H4diplopia6 = obj.H4diplopia6;
			H4diplopia7 = obj.H4diplopia7;
			H4diplopia8 = obj.H4diplopia8;
			H4diplopia9 = obj.H4diplopia9;
			H4Convergencia = obj.H4Convergencia;
			H4Acomodacionod = obj.H4Acomodacionod;
			H4Acomodacionoi = obj.H4Acomodacionoi;
			H4Pupulasod = obj.H4Pupulasod;
			H4Pupulasoi = obj.H4Pupulasoi;
			H4Forma = obj.H4Forma;
			H4Fotomotorod = obj.H4Fotomotorod;
			H4Fotomotoroi = obj.H4Fotomotoroi;
			H4Consensualdai = obj.H4Consensualdai;
			H4Consensualiad = obj.H4Consensualiad;
			H4Senfacialder = obj.H4Senfacialder;
			H4Senfacializq = obj.H4Senfacializq;
			H4Reflejoder = obj.H4Reflejoder;
			H4Reflejoizq = obj.H4Reflejoizq;
			H4Aberturaboca = obj.H4Aberturaboca;
			H4Movmasticatorios = obj.H4Movmasticatorios;
			H4Refmentoniano = obj.H4Refmentoniano;
			H4Facsimetria = obj.H4Facsimetria;
			H4Facmovimientos = obj.H4Facmovimientos;
			H4Facparalisis = obj.H4Facparalisis;
			H4Faccentral = obj.H4Faccentral;
			H4Facperiferica = obj.H4Facperiferica;
			H4Facgusto = obj.H4Facgusto;
			H5Otoscopia = obj.H5Otoscopia;
			H5Aguaudider = obj.H5Aguaudider;
			H5Aguaudiizq = obj.H5Aguaudiizq;
			H5Weber = obj.H5Weber;
			H5Rinne = obj.H5Rinne;
			H5Pruebaslab = obj.H5Pruebaslab;
			H5Elevpaladar = obj.H5Elevpaladar;
			H5Uvula = obj.H5Uvula;
			H5Refnauceoso = obj.H5Refnauceoso;
			H5deglucion = obj.H5deglucion;
			H5Tonovoz = obj.H5Tonovoz;
			H5Esternocleidomastoideo = obj.H5Esternocleidomastoideo;
			H5Trapecio = obj.H5Trapecio;
			H5desviacion = obj.H5desviacion;
			H5Atrofia = obj.H5Atrofia;
			H5Fasciculacion = obj.H5Fasciculacion;
			H5Fuerza = obj.H5Fuerza;
			H5Marcha = obj.H5Marcha;
			H5Tono = obj.H5Tono;
			H5Volumen = obj.H5Volumen;
			H5Fasciculaciones = obj.H5Fasciculaciones;
			H5Fuemuscular = obj.H5Fuemuscular;
			H5Movinvoluntarios = obj.H5Movinvoluntarios;
			H5Equilibratoria = obj.H5Equilibratoria;
			H5Romberg = obj.H5Romberg;
			H5dednarder = obj.H5dednarder;
			H5dednarizq = obj.H5dednarizq;
			H5deddedder = obj.H5deddedder;
			H5deddedizq = obj.H5deddedizq;
			H5Talrodder = obj.H5Talrodder;
			H5Talrodizq = obj.H5Talrodizq;
			H5Movrapid = obj.H5Movrapid;
			H5Rebote = obj.H5Rebote;
			H5Habilidadespecif = obj.H5Habilidadespecif;
			ApiStatus = obj.ApiStatus;
			ApiTransaction = obj.ApiTransaction;
			ApiUsucre = obj.ApiUsucre;
			ApiFeccre = obj.ApiFeccre;
			ApiUsumod = obj.ApiUsumod;
			ApiFecmod = obj.ApiFecmod;
			ApiUsudel = obj.ApiUsudel;
			ApiFecdel = obj.ApiFecdel;
		}
		
		#endregion
		
		#region Metodos Estaticos
		#endregion
		
		#region Metodos Privados
		
		/// <summary>
		/// Obtiene el Hash a partir de un array de Bytes
		/// </summary>
		/// <param name="objectAsBytes"></param>
		/// <returns>string</returns>
		private string ComputeHash(byte[] objectAsBytes)
		{
			MD5 md5 = new MD5CryptoServiceProvider();
			try
			{
				byte[] result = md5.ComputeHash(objectAsBytes);
				
				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < result.Length; i++)
				{
					sb.Append(result[i].ToString("X2"));
				}
				
				return sb.ToString();
			}
			catch (ArgumentNullException ane)
			{
				return null;
			}
		}
		
		/// <summary>
		///     Obtienen el Hash basado en algun algoritmo de Encriptación
		/// </summary>
		/// <typeparam name="T">
		///     Algoritmo de encriptación
		/// </typeparam>
		/// <param name="cryptoServiceProvider">
		///     Provvedor de Servicios de Criptografía
		/// </param>
		/// <returns>
		///     String que representa el Hash calculado
		        /// </returns>
		private string ComputeHash<T>( T cryptoServiceProvider) where T : HashAlgorithm, new()
		{
			DataContractSerializer serializer = new DataContractSerializer(this.GetType());
			using (MemoryStream memoryStream = new MemoryStream())
			{
				serializer.WriteObject(memoryStream, this);
				cryptoServiceProvider.ComputeHash(memoryStream.ToArray());
				return Convert.ToBase64String(cryptoServiceProvider.Hash);
			}
		}
		
		#endregion
		
		#region Overrides
		
		/// <summary>
		/// Devuelve un String que representa al Objeto
		/// </summary>
		/// <returns>string</returns>
		public override string ToString()
		{
			string hashString;
			
			//Evitar parametros NULL
			if (this == null)
				throw new ArgumentNullException("Parametro NULL no es valido");
			
			//Se verifica si el objeto es serializable.
			try
			{
				MemoryStream memStream = new MemoryStream();
				XmlSerializer serializer = new XmlSerializer(typeof(PatHistoryneuro2));
				serializer.Serialize(memStream, this);
				
				//Ahora se obtiene el Hash del Objeto.
				hashString = ComputeHash(memStream.ToArray());
				
				return hashString;
			}
			catch (AmbiguousMatchException ame)
			{
				throw new ApplicationException("El Objeto no es Serializable. Message:" + ame.Message);
			}
		}
		
		/// <summary>
		/// Verifica que dos objetos sean identicos
		/// </summary>
		public static bool operator ==(PatHistoryneuro2 first, PatHistoryneuro2 second)
		{
			// Verifica si el puntero en memoria es el mismo
			if (Object.ReferenceEquals(first, second))
				return true;
			
			// Verifica valores nulos
			if ((object) first == null || (object) second == null)
				return false;
			
			return first.GetHashCode() == second.GetHashCode();
		}
		
		/// <summary>
		/// Verifica que dos objetos sean distintos
		/// </summary>
		public static bool operator !=(PatHistoryneuro2 first, PatHistoryneuro2 second)
		{
			return !(first == second);
		}
		
		/// <summary>
		/// Compara este objeto con otro
		/// </summary>
		/// <param name="obj">El objeto a comparar</param>
		/// <returns>Devuelve Verdadero si ambos objetos son iguales</returns>
		public override bool Equals(object obj)
		{
			if (obj == null)
				return false;
			
			if (obj.GetType() == this.GetType())
				return obj.GetHashCode() == this.GetHashCode();
			
			return false;
		}
		
		#endregion
		
		#region Encriptacion
		
		/// <summary>
		/// Un valor unico que identifica cada instancia de este objeto
		/// </summary>
		/// <returns>Unico valor entero</returns>
		public override int GetHashCode()
		{
			return (this.GetSha512Hash().GetHashCode());
		}
		
		/// <summary>
		///     Obtiene el Hash de la Instancia Actual
		/// </summary>
		/// <typeparam name="T">
		///     Tipo de Proveedor de servicios criptográficos
		/// </typeparam>
		/// <returns>
		///     String en Base64 que representa el Hash
		/// </returns>
		public string GetHash<T>() where T : HashAlgorithm, new()
		{
			T cryptoServiceProvider = new T();
			return ComputeHash(cryptoServiceProvider);
		}
		
		/// <summary>
		///     Obtiene el codigo Hash en Base al Algoritmo SHA1
		/// </summary>
		/// <returns>
		///     String en Base64 que representa el Hash en SHA1
		/// </returns>
		public string GetSha1Hash()
		{
			return GetHash<SHA1CryptoServiceProvider>();
		}
		
		/// <summary>
		///     Obtiene el codigo Hash en Base al Algoritmo SHA512
		/// </summary>
		/// <returns>
		///     String en Base64 que representa el Hash en SHA512
		/// </returns>
		public string GetSha512Hash()
		{
			return GetHash<SHA512CryptoServiceProvider>();
		}
		
		/// <summary>
		///     Obtiene el codigo Hash en Base al Algoritmo MD5
		/// </summary>
		/// <returns>
		///     String en Base64 que representa el Hash en MD5
		/// </returns>
		public string GetMd5Hash()
		{
			return GetHash<MD5CryptoServiceProvider>();
		}
		
		
		#endregion
		
		#region DataAnnotations
		
		/// <summary>
		/// Obtiene los errores basado en los DataAnnotations 
		/// </summary>
		/// <returns>Devuelve un IList del tipo ValidationResult con los errores obtenidos</returns>
		public IList<ValidationResult> ValidationErrors()
		{
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			if (!Validator.TryValidateObject(this, context, errors, true))
				return errors;
			
			return new List<ValidationResult>(0);
		}
		
		/// <summary>
		/// Obtiene los errores basado en los DataAnnotations 
		/// </summary>
		/// <returns>Devuelve un String con los errores obtenidos</returns>
		public string ValidationErrorsString()
		{
			string strErrors = "";
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			if (!Validator.TryValidateObject(this, context, errors, true))
			{
				foreach (ValidationResult result in errors)
					strErrors = strErrors + result.ErrorMessage + Environment.NewLine;
			}
			return strErrors;
		}
		
		/// <summary>
		/// Funcion que determina si un objeto es valido o no
		/// </summary>
		/// <returns>Resultado de la validacion</returns>
		public bool IsValid()
		{
			ValidationContext context = new ValidationContext(this, null, null);
			IList<ValidationResult> errors = new List<ValidationResult>();
			
			return Validator.TryValidateObject(this, context, errors, true);
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna id_history_neuro2 de la Tabla pat_history_neuro2
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "id_history_neuro2", Description = " Propiedad publica de tipo Int64 que representa a la columna id_history_neuro2 de la Tabla pat_history_neuro2", Order = 1)]
		[JsonProperty(PropertyName = "id_history_neuro2")]
		[Required(ErrorMessage = "Se necesita un valor para -id_history_neuro2- porque es un campo requerido.")]
		[Key]
		public Int64 IdHistoryNeuro2 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna id_patients de la Tabla pat_history_neuro2
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "id_patients", Description = " Propiedad publica de tipo Int64 que representa a la columna id_patients de la Tabla pat_history_neuro2", Order = 2)]
		[JsonProperty(PropertyName = "id_patients")]
		[Required(ErrorMessage = "Se necesita un valor para -id_patients- porque es un campo requerido.")]
		public Int64 IdPatients { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4olfnormal de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4olfnormal", Description = " Propiedad publica de tipo string que representa a la columna h4olfnormal de la Tabla pat_history_neuro2", Order = 3)]
		[JsonProperty(PropertyName = "h4olfnormal")]
		public string H4Olfnormal { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4olfanosmia de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4olfanosmia", Description = " Propiedad publica de tipo string que representa a la columna h4olfanosmia de la Tabla pat_history_neuro2", Order = 4)]
		[JsonProperty(PropertyName = "h4olfanosmia")]
		public string H4Olfanosmia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4optscod de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4optscod", Description = " Propiedad publica de tipo string que representa a la columna h4optscod de la Tabla pat_history_neuro2", Order = 5)]
		[JsonProperty(PropertyName = "h4optscod")]
		public string H4Optscod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4optscoi de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4optscoi", Description = " Propiedad publica de tipo string que representa a la columna h4optscoi de la Tabla pat_history_neuro2", Order = 6)]
		[JsonProperty(PropertyName = "h4optscoi")]
		public string H4Optscoi { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4optccod de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4optccod", Description = " Propiedad publica de tipo string que representa a la columna h4optccod de la Tabla pat_history_neuro2", Order = 7)]
		[JsonProperty(PropertyName = "h4optccod")]
		public string H4Optccod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4optccoi de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4optccoi", Description = " Propiedad publica de tipo string que representa a la columna h4optccoi de la Tabla pat_history_neuro2", Order = 8)]
		[JsonProperty(PropertyName = "h4optccoi")]
		public string H4Optccoi { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4fondood de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4fondood", Description = " Propiedad publica de tipo string que representa a la columna h4fondood de la Tabla pat_history_neuro2", Order = 9)]
		[JsonProperty(PropertyName = "h4fondood")]
		public string H4Fondood { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4fondooi de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4fondooi", Description = " Propiedad publica de tipo string que representa a la columna h4fondooi de la Tabla pat_history_neuro2", Order = 10)]
		[JsonProperty(PropertyName = "h4fondooi")]
		public string H4Fondooi { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4campimetria de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h4campimetria", Description = " Propiedad publica de tipo string que representa a la columna h4campimetria de la Tabla pat_history_neuro2", Order = 11)]
		[JsonProperty(PropertyName = "h4campimetria")]
		public string H4Campimetria { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4prosis de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4prosis", Description = " Propiedad publica de tipo string que representa a la columna h4prosis de la Tabla pat_history_neuro2", Order = 12)]
		[JsonProperty(PropertyName = "h4prosis")]
		public string H4Prosis { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4posicionojos de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4posicionojos", Description = " Propiedad publica de tipo string que representa a la columna h4posicionojos de la Tabla pat_history_neuro2", Order = 13)]
		[JsonProperty(PropertyName = "h4posicionojos")]
		public string H4Posicionojos { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4exoftalmia de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4exoftalmia", Description = " Propiedad publica de tipo string que representa a la columna h4exoftalmia de la Tabla pat_history_neuro2", Order = 14)]
		[JsonProperty(PropertyName = "h4exoftalmia")]
		public string H4Exoftalmia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4horner de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4horner", Description = " Propiedad publica de tipo string que representa a la columna h4horner de la Tabla pat_history_neuro2", Order = 15)]
		[JsonProperty(PropertyName = "h4horner")]
		public string H4Horner { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4movoculares de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4movoculares", Description = " Propiedad publica de tipo string que representa a la columna h4movoculares de la Tabla pat_history_neuro2", Order = 16)]
		[JsonProperty(PropertyName = "h4movoculares")]
		public string H4Movoculares { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4nistagmux de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4nistagmux", Description = " Propiedad publica de tipo string que representa a la columna h4nistagmux de la Tabla pat_history_neuro2", Order = 17)]
		[JsonProperty(PropertyName = "h4nistagmux")]
		public string H4Nistagmux { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia1 de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia1", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia1 de la Tabla pat_history_neuro2", Order = 18)]
		[JsonProperty(PropertyName = "h4diplopia1")]
		public string H4diplopia1 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia2 de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia2", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia2 de la Tabla pat_history_neuro2", Order = 19)]
		[JsonProperty(PropertyName = "h4diplopia2")]
		public string H4diplopia2 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia3 de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia3", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia3 de la Tabla pat_history_neuro2", Order = 20)]
		[JsonProperty(PropertyName = "h4diplopia3")]
		public string H4diplopia3 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia4 de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia4", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia4 de la Tabla pat_history_neuro2", Order = 21)]
		[JsonProperty(PropertyName = "h4diplopia4")]
		public string H4diplopia4 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia5 de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia5", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia5 de la Tabla pat_history_neuro2", Order = 22)]
		[JsonProperty(PropertyName = "h4diplopia5")]
		public string H4diplopia5 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia6 de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia6", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia6 de la Tabla pat_history_neuro2", Order = 23)]
		[JsonProperty(PropertyName = "h4diplopia6")]
		public string H4diplopia6 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia7 de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia7", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia7 de la Tabla pat_history_neuro2", Order = 24)]
		[JsonProperty(PropertyName = "h4diplopia7")]
		public string H4diplopia7 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia8 de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia8", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia8 de la Tabla pat_history_neuro2", Order = 25)]
		[JsonProperty(PropertyName = "h4diplopia8")]
		public string H4diplopia8 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia9 de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia9", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia9 de la Tabla pat_history_neuro2", Order = 26)]
		[JsonProperty(PropertyName = "h4diplopia9")]
		public string H4diplopia9 { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4convergencia de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4convergencia", Description = " Propiedad publica de tipo string que representa a la columna h4convergencia de la Tabla pat_history_neuro2", Order = 27)]
		[JsonProperty(PropertyName = "h4convergencia")]
		public string H4Convergencia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4acomodacionod de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4acomodacionod", Description = " Propiedad publica de tipo string que representa a la columna h4acomodacionod de la Tabla pat_history_neuro2", Order = 28)]
		[JsonProperty(PropertyName = "h4acomodacionod")]
		public string H4Acomodacionod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4acomodacionoi de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4acomodacionoi", Description = " Propiedad publica de tipo string que representa a la columna h4acomodacionoi de la Tabla pat_history_neuro2", Order = 29)]
		[JsonProperty(PropertyName = "h4acomodacionoi")]
		public string H4Acomodacionoi { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4pupulasod de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4pupulasod", Description = " Propiedad publica de tipo string que representa a la columna h4pupulasod de la Tabla pat_history_neuro2", Order = 30)]
		[JsonProperty(PropertyName = "h4pupulasod")]
		public string H4Pupulasod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4pupulasoi de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4pupulasoi", Description = " Propiedad publica de tipo string que representa a la columna h4pupulasoi de la Tabla pat_history_neuro2", Order = 31)]
		[JsonProperty(PropertyName = "h4pupulasoi")]
		public string H4Pupulasoi { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4forma de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4forma", Description = " Propiedad publica de tipo string que representa a la columna h4forma de la Tabla pat_history_neuro2", Order = 32)]
		[JsonProperty(PropertyName = "h4forma")]
		public string H4Forma { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4fotomotorod de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4fotomotorod", Description = " Propiedad publica de tipo string que representa a la columna h4fotomotorod de la Tabla pat_history_neuro2", Order = 33)]
		[JsonProperty(PropertyName = "h4fotomotorod")]
		public string H4Fotomotorod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4fotomotoroi de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4fotomotoroi", Description = " Propiedad publica de tipo string que representa a la columna h4fotomotoroi de la Tabla pat_history_neuro2", Order = 34)]
		[JsonProperty(PropertyName = "h4fotomotoroi")]
		public string H4Fotomotoroi { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4consensualdai de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4consensualdai", Description = " Propiedad publica de tipo string que representa a la columna h4consensualdai de la Tabla pat_history_neuro2", Order = 35)]
		[JsonProperty(PropertyName = "h4consensualdai")]
		public string H4Consensualdai { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4consensualiad de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4consensualiad", Description = " Propiedad publica de tipo string que representa a la columna h4consensualiad de la Tabla pat_history_neuro2", Order = 36)]
		[JsonProperty(PropertyName = "h4consensualiad")]
		public string H4Consensualiad { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4senfacialder de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4senfacialder", Description = " Propiedad publica de tipo string que representa a la columna h4senfacialder de la Tabla pat_history_neuro2", Order = 37)]
		[JsonProperty(PropertyName = "h4senfacialder")]
		public string H4Senfacialder { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4senfacializq de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4senfacializq", Description = " Propiedad publica de tipo string que representa a la columna h4senfacializq de la Tabla pat_history_neuro2", Order = 38)]
		[JsonProperty(PropertyName = "h4senfacializq")]
		public string H4Senfacializq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4reflejoder de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4reflejoder", Description = " Propiedad publica de tipo string que representa a la columna h4reflejoder de la Tabla pat_history_neuro2", Order = 39)]
		[JsonProperty(PropertyName = "h4reflejoder")]
		public string H4Reflejoder { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4reflejoizq de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4reflejoizq", Description = " Propiedad publica de tipo string que representa a la columna h4reflejoizq de la Tabla pat_history_neuro2", Order = 40)]
		[JsonProperty(PropertyName = "h4reflejoizq")]
		public string H4Reflejoizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4aberturaboca de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h4aberturaboca", Description = " Propiedad publica de tipo string que representa a la columna h4aberturaboca de la Tabla pat_history_neuro2", Order = 41)]
		[JsonProperty(PropertyName = "h4aberturaboca")]
		public string H4Aberturaboca { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4movmasticatorios de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h4movmasticatorios", Description = " Propiedad publica de tipo string que representa a la columna h4movmasticatorios de la Tabla pat_history_neuro2", Order = 42)]
		[JsonProperty(PropertyName = "h4movmasticatorios")]
		public string H4Movmasticatorios { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4refmentoniano de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h4refmentoniano", Description = " Propiedad publica de tipo string que representa a la columna h4refmentoniano de la Tabla pat_history_neuro2", Order = 43)]
		[JsonProperty(PropertyName = "h4refmentoniano")]
		public string H4Refmentoniano { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4facsimetria de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4facsimetria", Description = " Propiedad publica de tipo string que representa a la columna h4facsimetria de la Tabla pat_history_neuro2", Order = 44)]
		[JsonProperty(PropertyName = "h4facsimetria")]
		public string H4Facsimetria { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4facmovimientos de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h4facmovimientos", Description = " Propiedad publica de tipo string que representa a la columna h4facmovimientos de la Tabla pat_history_neuro2", Order = 45)]
		[JsonProperty(PropertyName = "h4facmovimientos")]
		public string H4Facmovimientos { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4facparalisis de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4facparalisis", Description = " Propiedad publica de tipo string que representa a la columna h4facparalisis de la Tabla pat_history_neuro2", Order = 46)]
		[JsonProperty(PropertyName = "h4facparalisis")]
		public string H4Facparalisis { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4faccentral de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4faccentral", Description = " Propiedad publica de tipo string que representa a la columna h4faccentral de la Tabla pat_history_neuro2", Order = 47)]
		[JsonProperty(PropertyName = "h4faccentral")]
		public string H4Faccentral { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4facperiferica de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4facperiferica", Description = " Propiedad publica de tipo string que representa a la columna h4facperiferica de la Tabla pat_history_neuro2", Order = 48)]
		[JsonProperty(PropertyName = "h4facperiferica")]
		public string H4Facperiferica { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4facgusto de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h4facgusto", Description = " Propiedad publica de tipo string que representa a la columna h4facgusto de la Tabla pat_history_neuro2", Order = 49)]
		[JsonProperty(PropertyName = "h4facgusto")]
		public string H4Facgusto { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5otoscopia de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h5otoscopia", Description = " Propiedad publica de tipo string que representa a la columna h5otoscopia de la Tabla pat_history_neuro2", Order = 50)]
		[JsonProperty(PropertyName = "h5otoscopia")]
		public string H5Otoscopia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5aguaudider de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5aguaudider", Description = " Propiedad publica de tipo string que representa a la columna h5aguaudider de la Tabla pat_history_neuro2", Order = 51)]
		[JsonProperty(PropertyName = "h5aguaudider")]
		public string H5Aguaudider { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5aguaudiizq de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5aguaudiizq", Description = " Propiedad publica de tipo string que representa a la columna h5aguaudiizq de la Tabla pat_history_neuro2", Order = 52)]
		[JsonProperty(PropertyName = "h5aguaudiizq")]
		public string H5Aguaudiizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5weber de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5weber", Description = " Propiedad publica de tipo string que representa a la columna h5weber de la Tabla pat_history_neuro2", Order = 53)]
		[JsonProperty(PropertyName = "h5weber")]
		public string H5Weber { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5rinne de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5rinne", Description = " Propiedad publica de tipo string que representa a la columna h5rinne de la Tabla pat_history_neuro2", Order = 54)]
		[JsonProperty(PropertyName = "h5rinne")]
		public string H5Rinne { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5pruebaslab de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5pruebaslab", Description = " Propiedad publica de tipo string que representa a la columna h5pruebaslab de la Tabla pat_history_neuro2", Order = 55)]
		[JsonProperty(PropertyName = "h5pruebaslab")]
		public string H5Pruebaslab { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5elevpaladar de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5elevpaladar", Description = " Propiedad publica de tipo string que representa a la columna h5elevpaladar de la Tabla pat_history_neuro2", Order = 56)]
		[JsonProperty(PropertyName = "h5elevpaladar")]
		public string H5Elevpaladar { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5uvula de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5uvula", Description = " Propiedad publica de tipo string que representa a la columna h5uvula de la Tabla pat_history_neuro2", Order = 57)]
		[JsonProperty(PropertyName = "h5uvula")]
		public string H5Uvula { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5refnauceoso de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5refnauceoso", Description = " Propiedad publica de tipo string que representa a la columna h5refnauceoso de la Tabla pat_history_neuro2", Order = 58)]
		[JsonProperty(PropertyName = "h5refnauceoso")]
		public string H5Refnauceoso { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5deglucion de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5deglucion", Description = " Propiedad publica de tipo string que representa a la columna h5deglucion de la Tabla pat_history_neuro2", Order = 59)]
		[JsonProperty(PropertyName = "h5deglucion")]
		public string H5deglucion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5tonovoz de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5tonovoz", Description = " Propiedad publica de tipo string que representa a la columna h5tonovoz de la Tabla pat_history_neuro2", Order = 60)]
		[JsonProperty(PropertyName = "h5tonovoz")]
		public string H5Tonovoz { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5esternocleidomastoideo de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5esternocleidomastoideo", Description = " Propiedad publica de tipo string que representa a la columna h5esternocleidomastoideo de la Tabla pat_history_neuro2", Order = 61)]
		[JsonProperty(PropertyName = "h5esternocleidomastoideo")]
		public string H5Esternocleidomastoideo { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5trapecio de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5trapecio", Description = " Propiedad publica de tipo string que representa a la columna h5trapecio de la Tabla pat_history_neuro2", Order = 62)]
		[JsonProperty(PropertyName = "h5trapecio")]
		public string H5Trapecio { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5desviacion de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5desviacion", Description = " Propiedad publica de tipo string que representa a la columna h5desviacion de la Tabla pat_history_neuro2", Order = 63)]
		[JsonProperty(PropertyName = "h5desviacion")]
		public string H5desviacion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5atrofia de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5atrofia", Description = " Propiedad publica de tipo string que representa a la columna h5atrofia de la Tabla pat_history_neuro2", Order = 64)]
		[JsonProperty(PropertyName = "h5atrofia")]
		public string H5Atrofia { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5fasciculacion de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5fasciculacion", Description = " Propiedad publica de tipo string que representa a la columna h5fasciculacion de la Tabla pat_history_neuro2", Order = 65)]
		[JsonProperty(PropertyName = "h5fasciculacion")]
		public string H5Fasciculacion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5fuerza de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5fuerza", Description = " Propiedad publica de tipo string que representa a la columna h5fuerza de la Tabla pat_history_neuro2", Order = 66)]
		[JsonProperty(PropertyName = "h5fuerza")]
		public string H5Fuerza { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5marcha de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h5marcha", Description = " Propiedad publica de tipo string que representa a la columna h5marcha de la Tabla pat_history_neuro2", Order = 67)]
		[JsonProperty(PropertyName = "h5marcha")]
		public string H5Marcha { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5tono de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5tono", Description = " Propiedad publica de tipo string que representa a la columna h5tono de la Tabla pat_history_neuro2", Order = 68)]
		[JsonProperty(PropertyName = "h5tono")]
		public string H5Tono { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5volumen de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5volumen", Description = " Propiedad publica de tipo string que representa a la columna h5volumen de la Tabla pat_history_neuro2", Order = 69)]
		[JsonProperty(PropertyName = "h5volumen")]
		public string H5Volumen { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5fasciculaciones de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5fasciculaciones", Description = " Propiedad publica de tipo string que representa a la columna h5fasciculaciones de la Tabla pat_history_neuro2", Order = 70)]
		[JsonProperty(PropertyName = "h5fasciculaciones")]
		public string H5Fasciculaciones { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5fuemuscular de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h5fuemuscular", Description = " Propiedad publica de tipo string que representa a la columna h5fuemuscular de la Tabla pat_history_neuro2", Order = 71)]
		[JsonProperty(PropertyName = "h5fuemuscular")]
		public string H5Fuemuscular { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5movinvoluntarios de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h5movinvoluntarios", Description = " Propiedad publica de tipo string que representa a la columna h5movinvoluntarios de la Tabla pat_history_neuro2", Order = 72)]
		[JsonProperty(PropertyName = "h5movinvoluntarios")]
		public string H5Movinvoluntarios { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5equilibratoria de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5equilibratoria", Description = " Propiedad publica de tipo string que representa a la columna h5equilibratoria de la Tabla pat_history_neuro2", Order = 73)]
		[JsonProperty(PropertyName = "h5equilibratoria")]
		public string H5Equilibratoria { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5romberg de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5romberg", Description = " Propiedad publica de tipo string que representa a la columna h5romberg de la Tabla pat_history_neuro2", Order = 74)]
		[JsonProperty(PropertyName = "h5romberg")]
		public string H5Romberg { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5dednarder de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h5dednarder", Description = " Propiedad publica de tipo string que representa a la columna h5dednarder de la Tabla pat_history_neuro2", Order = 75)]
		[JsonProperty(PropertyName = "h5dednarder")]
		public string H5dednarder { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5dednarizq de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h5dednarizq", Description = " Propiedad publica de tipo string que representa a la columna h5dednarizq de la Tabla pat_history_neuro2", Order = 76)]
		[JsonProperty(PropertyName = "h5dednarizq")]
		public string H5dednarizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5deddedder de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5deddedder", Description = " Propiedad publica de tipo string que representa a la columna h5deddedder de la Tabla pat_history_neuro2", Order = 77)]
		[JsonProperty(PropertyName = "h5deddedder")]
		public string H5deddedder { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5deddedizq de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5deddedizq", Description = " Propiedad publica de tipo string que representa a la columna h5deddedizq de la Tabla pat_history_neuro2", Order = 78)]
		[JsonProperty(PropertyName = "h5deddedizq")]
		public string H5deddedizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5talrodder de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5talrodder", Description = " Propiedad publica de tipo string que representa a la columna h5talrodder de la Tabla pat_history_neuro2", Order = 79)]
		[JsonProperty(PropertyName = "h5talrodder")]
		public string H5Talrodder { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5talrodizq de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5talrodizq", Description = " Propiedad publica de tipo string que representa a la columna h5talrodizq de la Tabla pat_history_neuro2", Order = 80)]
		[JsonProperty(PropertyName = "h5talrodizq")]
		public string H5Talrodizq { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5movrapid de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h5movrapid", Description = " Propiedad publica de tipo string que representa a la columna h5movrapid de la Tabla pat_history_neuro2", Order = 81)]
		[JsonProperty(PropertyName = "h5movrapid")]
		public string H5Movrapid { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5rebote de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h5rebote", Description = " Propiedad publica de tipo string que representa a la columna h5rebote de la Tabla pat_history_neuro2", Order = 82)]
		[JsonProperty(PropertyName = "h5rebote")]
		public string H5Rebote { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5habilidadespecif de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h5habilidadespecif", Description = " Propiedad publica de tipo string que representa a la columna h5habilidadespecif de la Tabla pat_history_neuro2", Order = 83)]
		[JsonProperty(PropertyName = "h5habilidadespecif")]
		public string H5Habilidadespecif { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna api_status de la Tabla pat_history_neuro2
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_status", Description = " Propiedad publica de tipo string que representa a la columna api_status de la Tabla pat_history_neuro2", Order = 84)]
		[JsonProperty(PropertyName = "api_status")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -api_status- porque es un campo requerido.")]
		public string ApiStatus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna api_transaction de la Tabla pat_history_neuro2
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(40, MinimumLength=0)]
		[Display(Name = "api_transaction", Description = " Propiedad publica de tipo string que representa a la columna api_transaction de la Tabla pat_history_neuro2", Order = 85)]
		[JsonProperty(PropertyName = "api_transaction")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -api_transaction- porque es un campo requerido.")]
		public string ApiTransaction { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna api_usucre de la Tabla pat_history_neuro2
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_usucre", Description = " Propiedad publica de tipo string que representa a la columna api_usucre de la Tabla pat_history_neuro2", Order = 86)]
		[JsonProperty(PropertyName = "api_usucre")]
		public string ApiUsucre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna api_feccre de la Tabla pat_history_neuro2
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "api_feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna api_feccre de la Tabla pat_history_neuro2", Order = 87)]
		[JsonProperty(PropertyName = "api_feccre")]
		public DateTime ApiFeccre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna api_usumod de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_usumod", Description = " Propiedad publica de tipo string que representa a la columna api_usumod de la Tabla pat_history_neuro2", Order = 88)]
		[JsonProperty(PropertyName = "api_usumod")]
		public string ApiUsumod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna api_fecmod de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "api_fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna api_fecmod de la Tabla pat_history_neuro2", Order = 89)]
		[JsonProperty(PropertyName = "api_fecmod")]
		public DateTime? ApiFecmod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna api_usudel de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "api_usudel", Description = " Propiedad publica de tipo string que representa a la columna api_usudel de la Tabla pat_history_neuro2", Order = 90)]
		[JsonProperty(PropertyName = "api_usudel")]
		public string ApiUsudel { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna api_fecdel de la Tabla pat_history_neuro2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "api_fecdel", Description = " Propiedad publica de tipo DateTime que representa a la columna api_fecdel de la Tabla pat_history_neuro2", Order = 91)]
		[JsonProperty(PropertyName = "api_fecdel")]
		public DateTime? ApiFecdel { get; set; } 

	}
}

