namespace ReAl.GestionMedica.ConnFB
{
    static public class cParametrosFb
    {
         //PRODUCCION SIAPS
        public static string User = "SYSDBA";
        public static string Pass = "masterkey";
        public static string Bd = "realmed";
        
        //Otros parametros
        public static string ParFormatoFechaHora = "dd/MM/yyyy HH:mm:ss.ffffff";
        public static string ParFormatoFecha = "dd/MM/yyyy";
    }
}
