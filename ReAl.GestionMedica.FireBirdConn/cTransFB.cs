#region

using System;
using System.Data;
using FirebirdSql.Data.FirebirdClient;

#endregion


namespace ReAl.GestionMedica.ConnFB
{
    

    public class cTransFb
    {
        public FbTransaction MyTrans;
        public FbConnection MyConn;

        /// <summary>
        ///     Constructor, que además abre la conexion y la transaccion
        /// </summary>
        public cTransFb()
        {
            cConnFb tempConnWebService = new cConnFb();
            this.MyConn = tempConnWebService.ConexionBd;
            this.MyConn.Open();
            this.MyTrans = this.MyConn.BeginTransaction();
        }

        /// <summary>
        ///     Commit transaccion y cerrar conexion
        /// </summary>
        public void ConfirmarTransaccion()
        {
            try
            {
                this.MyTrans.Commit();
            }
            catch (Exception exp)
            {
                this.MyTrans.Rollback();
                if (this.MyConn.State == ConnectionState.Open)
                {
                    this.MyConn.Close();
                }
                throw;
            }
        }

        /// <summary>
        ///     RollBack transaccion y cerrar conexion
        /// </summary>
        public void AnularTransaccion()
        {
            try
            {
                this.MyTrans.Rollback();
                this.MyConn.Close();
            }
            catch (Exception exp)
            {
                this.MyTrans.Rollback();
                if (this.MyConn.State == ConnectionState.Open)
                {
                    this.MyConn.Close();
                }
                throw;
            }
        }
    }
}