namespace ReAl.GestionMedica.PgConn
{
    public static class CParametros
    {
        #region Fields

        //Parametros de conexion
        public static bool BChangeUserOnLogon;

        public static bool BConfigConectionWithFile;

        public static string Bd;

        public static bool BUseIntegratedSecurity;

        public static string ParFormatoFecha = "dd/MM/yyyy";

        //Otros parametros
        public static string ParFormatoFechaHora = "dd/MM/yyyy HH:mm:ss";

        public static string Pass;

        public static string PassDefault;

        public static string Puerto;

        public static string Schema;

        public static string Server;

        public static string User;

        public static string UserDefault;

        private const MiEntornoEnum MiEntorno = MiEntornoEnum.Vultr;

        #endregion Fields

        #region Constructors

        //Constructor
        static CParametros()
        {
            switch (MiEntorno)
            {
                case MiEntornoEnum.Local:
                    BChangeUserOnLogon = false;
                    BUseIntegratedSecurity = false;
                    BConfigConectionWithFile = false;
                    Server = "127.0.0.1";
                    Puerto = "5432";
                    User = "postgres";
                    Pass = "Desa2019";
                    Schema = "";
                    Bd = "db_gestionmedica";
                    UserDefault = "postgres";
                    PassDefault = "Desa2019";

                    ParFormatoFechaHora = "dd/MM/yyyy HH:mm:ss.ffffff";
                    ParFormatoFecha = "dd/MM/yyyy";
                    break;

                case MiEntornoEnum.Vultr:
                    BChangeUserOnLogon = false;
                    BUseIntegratedSecurity = false;
                    BConfigConectionWithFile = false;
                    Server = "104.238.136.15";
                    Puerto = "5432";
                    User = "postgres";
                    Pass = "Desa2019";
                    Schema = "";
                    Bd = "db_gestionmedica_off";
                    UserDefault = "postgres";
                    PassDefault = "Desa2019";

                    ParFormatoFechaHora = "dd/MM/yyyy HH:mm:ss.ffffff";
                    ParFormatoFecha = "dd/MM/yyyy";
                    break;

                case MiEntornoEnum.Demo:
                    BChangeUserOnLogon = false;
                    BUseIntegratedSecurity = false;
                    BConfigConectionWithFile = false;
                    Server = "104.238.136.15";
                    Puerto = "5432";
                    User = "postgres";
                    Pass = "Desa2019";
                    Schema = "";
                    Bd = "db_demo_med";
                    UserDefault = "postgres";
                    PassDefault = "Desa2019";

                    ParFormatoFechaHora = "dd/MM/yyyy HH:mm:ss.ffffff";
                    ParFormatoFecha = "dd/MM/yyyy";
                    break;
            }
        }

        #endregion Constructors

        #region Enums

        private enum MiEntornoEnum
        {
            Local,
            Vultr,
            Demo
        }

        #endregion Enums
    }
}