#region

using System;
using System.Data;
using Npgsql;

#endregion

namespace ReAl.GestionMedica.PgConn
{
    public class CTrans
    {
        public NpgsqlTransaction MyTrans;
        public NpgsqlConnection MyConn;

        /// <summary>
        ///     Constructor, que además abre la conexion y la transaccion
        /// </summary>
        public CTrans()
        {
            var tempConnWebService = new CConn();
            this.MyConn = tempConnWebService.ConexionBd;
            this.MyConn.Open();
            this.MyTrans = this.MyConn.BeginTransaction();
        }

        /// <summary>
        ///     Commit transaccion y cerrar conexion
        /// </summary>
        public void ConfirmarTransaccion()
        {
            try
            {
                this.MyTrans.Commit();
            }
            catch (Exception exp)
            {
                this.MyTrans.Rollback();
                if (this.MyConn.State == ConnectionState.Open)
                {
                    this.MyConn.Close();
                }
                throw;
            }
        }

        /// <summary>
        ///     RollBack transaccion y cerrar conexion
        /// </summary>
        public void AnularTransaccion()
        {
            try
            {
                this.MyTrans.Rollback();
                this.MyConn.Close();
            }
            catch (Exception exp)
            {
                this.MyTrans.Rollback();
                if (this.MyConn.State == ConnectionState.Open)
                {
                    this.MyConn.Close();
                }
                throw;
            }
        }
    }
}