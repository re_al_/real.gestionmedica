﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

public partial class PatConsultation
{
    /// <summary>
    /// Entity identifier
    /// </summary>
    [Key]
    public long id_consultations { get; set; }

    public long? id_patients { get; set; }

    /// <summary>
    /// Consultation date
    /// </summary>
    public DateOnly consultation_date { get; set; }

    /// <summary>
    /// Consultation hour
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime consultation_time { get; set; }

    public long id_cie { get; set; }

    /// <summary>
    /// Clinical profile in consultation
    /// </summary>
    [StringLength(2000)]
    public string? clinical_profile { get; set; }

    /// <summary>
    /// Diagnostics in consultation
    /// </summary>
    [StringLength(2000)]
    public string? diagnosis { get; set; }

    /// <summary>
    /// Behaviour in consultation
    /// </summary>
    [StringLength(2000)]
    public string? behaviour { get; set; }

    /// <summary>
    /// Treatment for consultation
    /// </summary>
    [StringLength(2000)]
    public string? treatment { get; set; }

    /// <summary>
    /// Current status
    /// </summary>
    [StringLength(15)]
    public string api_status { get; set; } = null!;

    /// <summary>
    /// Last transaction used
    /// </summary>
    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    /// <summary>
    /// User creation
    /// </summary>
    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    /// <summary>
    /// Timestamp creation
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    /// <summary>
    /// User update
    /// </summary>
    [StringLength(15)]
    public string? api_usumod { get; set; }

    /// <summary>
    /// Timestamp update
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    /// <summary>
    /// User delete
    /// </summary>
    [StringLength(15)]
    public string? api_usudel { get; set; }

    /// <summary>
    /// Timestamp delete
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [ForeignKey("id_cie")]
    [InverseProperty("pat_consultations")]
    public virtual ClaCie id_cieNavigation { get; set; } = null!;

    [ForeignKey("id_patients")]
    [InverseProperty("pat_consultations")]
    public virtual PatPatient? id_patientsNavigation { get; set; }
}
