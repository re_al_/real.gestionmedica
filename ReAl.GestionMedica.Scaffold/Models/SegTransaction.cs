﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

/// <summary>
/// Almacena las TRANSACCIONES que pueden ser realizadas en una TABLA particular
/// </summary>
public partial class SegTransaction
{
    /// <summary>
    /// Entity identifier
    /// </summary>
    [Key]
    public long id_transactions { get; set; }

    public long id_tables { get; set; }

    /// <summary>
    /// Transaction code
    /// </summary>
    [StringLength(40)]
    public string code { get; set; } = null!;

    /// <summary>
    /// Transaction description
    /// </summary>
    [StringLength(60)]
    public string description { get; set; } = null!;

    /// <summary>
    /// SQL sentence for transaction
    /// </summary>
    [StringLength(7)]
    public string sentence { get; set; } = null!;

    /// <summary>
    /// Current status
    /// </summary>
    [StringLength(15)]
    public string api_status { get; set; } = null!;

    /// <summary>
    /// Last transaction used
    /// </summary>
    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    /// <summary>
    /// User creation
    /// </summary>
    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    /// <summary>
    /// Timestamp creation
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    /// <summary>
    /// User update
    /// </summary>
    [StringLength(15)]
    public string? api_usumod { get; set; }

    /// <summary>
    /// Timestamp update
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    /// <summary>
    /// User delete
    /// </summary>
    [StringLength(15)]
    public string? api_usudel { get; set; }

    /// <summary>
    /// Timestamp delete
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [ForeignKey("id_tables")]
    [InverseProperty("seg_transactions")]
    public virtual SegTable id_tablesNavigation { get; set; } = null!;

    [InverseProperty("id_transactionsNavigation")]
    public virtual ICollection<SegTransition> seg_transitions { get; set; } = new List<SegTransition>();
}
