﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

public partial class PatMultimedia
{
    /// <summary>
    /// Entity identifier
    /// </summary>
    [Key]
    public long id_multimedia { get; set; }

    public long id_patients { get; set; }

    /// <summary>
    /// Upload_date
    /// </summary>
    public DateOnly upload_date { get; set; }

    /// <summary>
    /// Media observations
    /// </summary>
    [StringLength(5000)]
    public string observations { get; set; } = null!;

    /// <summary>
    /// External URL
    /// </summary>
    [StringLength(500)]
    public string external_url { get; set; } = null!;

    /// <summary>
    /// Media extension
    /// </summary>
    [StringLength(4)]
    public string extension { get; set; } = null!;

    /// <summary>
    /// Media filename
    /// </summary>
    [StringLength(256)]
    public string filename { get; set; } = null!;

    /// <summary>
    /// Current status
    /// </summary>
    [StringLength(15)]
    public string api_status { get; set; } = null!;

    /// <summary>
    /// Last transaction used
    /// </summary>
    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    /// <summary>
    /// User creation
    /// </summary>
    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    /// <summary>
    /// Timestamp creation
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    /// <summary>
    /// User update
    /// </summary>
    [StringLength(15)]
    public string? api_usumod { get; set; }

    /// <summary>
    /// Timestamp update
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    /// <summary>
    /// User delete
    /// </summary>
    [StringLength(15)]
    public string? api_usudel { get; set; }

    /// <summary>
    /// Timestamp delete
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [ForeignKey("id_patients")]
    [InverseProperty("pat_multimedia")]
    public virtual PatPatient id_patientsNavigation { get; set; } = null!;
}
