﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

/// <summary>
/// Almacena todos los posibles estados que puedan poseer los registro de cada una de las tablas, debera existir siempre el estado estado ELABORADO, que es el estado en el cual se encontraran todos los registros una vez que hayan sido creados
/// </summary>
public partial class SegState
{
    /// <summary>
    /// Entity identifier
    /// </summary>
    [Key]
    public long id_states { get; set; }

    public long id_tables { get; set; }

    /// <summary>
    /// State code
    /// </summary>
    [StringLength(40)]
    public string code { get; set; } = null!;

    /// <summary>
    /// State description
    /// </summary>
    [StringLength(60)]
    public string description { get; set; } = null!;

    /// <summary>
    /// Current status
    /// </summary>
    [StringLength(15)]
    public string api_status { get; set; } = null!;

    /// <summary>
    /// Last transaction used
    /// </summary>
    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    /// <summary>
    /// User creation
    /// </summary>
    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    /// <summary>
    /// Timestamp creation
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    /// <summary>
    /// User update
    /// </summary>
    [StringLength(15)]
    public string? api_usumod { get; set; }

    /// <summary>
    /// Timestamp update
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    /// <summary>
    /// User delete
    /// </summary>
    [StringLength(15)]
    public string? api_usudel { get; set; }

    /// <summary>
    /// Timestamp delete
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [ForeignKey("id_tables")]
    [InverseProperty("seg_states")]
    public virtual SegTable id_tablesNavigation { get; set; } = null!;

    [InverseProperty("id_states_endNavigation")]
    public virtual ICollection<SegTransition> seg_transitionid_states_endNavigations { get; set; } = new List<SegTransition>();

    [InverseProperty("id_states_iniNavigation")]
    public virtual ICollection<SegTransition> seg_transitionid_states_iniNavigations { get; set; } = new List<SegTransition>();
}
