﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

public partial class PatAppointment
{
    /// <summary>
    /// Entity identifier
    /// </summary>
    [Key]
    public long id_appointments { get; set; }

    public long? id_patients { get; set; }

    public long? id_users { get; set; }

    /// <summary>
    /// Appointment subject
    /// </summary>
    [StringLength(500)]
    public string subject { get; set; } = null!;

    /// <summary>
    /// Appointment details
    /// </summary>
    [StringLength(500)]
    public string? description { get; set; }

    /// <summary>
    /// Appointment date and time for init
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime time_start { get; set; }

    /// <summary>
    /// Appointment date and time for end
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime time_end { get; set; }

    /// <summary>
    /// Appointment location
    /// </summary>
    [StringLength(200)]
    public string? location { get; set; }

    /// <summary>
    /// Appointment labels
    /// </summary>
    public int labels { get; set; }

    /// <summary>
    /// Reasons for non-attendance
    /// </summary>
    [StringLength(500)]
    public string? leaving_reason { get; set; }

    /// <summary>
    /// Current status
    /// </summary>
    [StringLength(15)]
    public string api_status { get; set; } = null!;

    /// <summary>
    /// Last transaction used
    /// </summary>
    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    /// <summary>
    /// User creation
    /// </summary>
    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    /// <summary>
    /// Timestamp creation
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    /// <summary>
    /// User update
    /// </summary>
    [StringLength(15)]
    public string? api_usumod { get; set; }

    /// <summary>
    /// Timestamp update
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    /// <summary>
    /// User delete
    /// </summary>
    [StringLength(15)]
    public string? api_usudel { get; set; }

    /// <summary>
    /// Timestamp delete
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [InverseProperty("id_appointmentsNavigation")]
    public virtual ICollection<FinAccount> fin_accounts { get; set; } = new List<FinAccount>();

    [ForeignKey("id_patients")]
    [InverseProperty("pat_appointments")]
    public virtual PatPatient? id_patientsNavigation { get; set; }

    [ForeignKey("id_users")]
    [InverseProperty("pat_appointments")]
    public virtual seg_user? id_usersNavigation { get; set; }
}
