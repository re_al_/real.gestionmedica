﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

public partial class SegTable
{
    /// <summary>
    /// Entity identifier
    /// </summary>
    [Key]
    public long id_tables { get; set; }

    public long? id_modules { get; set; }

    /// <summary>
    /// Table name
    /// </summary>
    [StringLength(40)]
    public string name { get; set; } = null!;

    /// <summary>
    /// Table alias for abbreviation
    /// </summary>
    [StringLength(3)]
    public string alias { get; set; } = null!;

    /// <summary>
    /// Table description
    /// </summary>
    [StringLength(240)]
    public string description { get; set; } = null!;

    /// <summary>
    /// Current status
    /// </summary>
    [StringLength(15)]
    public string api_status { get; set; } = null!;

    /// <summary>
    /// Last transaction used
    /// </summary>
    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    /// <summary>
    /// User creation
    /// </summary>
    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    /// <summary>
    /// Timestamp creation
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    /// <summary>
    /// User update
    /// </summary>
    [StringLength(15)]
    public string? api_usumod { get; set; }

    /// <summary>
    /// Timestamp update
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    /// <summary>
    /// User delete
    /// </summary>
    [StringLength(15)]
    public string? api_usudel { get; set; }

    /// <summary>
    /// Timestamp delete
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [ForeignKey("id_modules")]
    [InverseProperty("seg_tables")]
    public virtual SegModule? id_modulesNavigation { get; set; }

    [InverseProperty("id_tablesNavigation")]
    public virtual ICollection<SegState> seg_states { get; set; } = new List<SegState>();

    [InverseProperty("id_tablesNavigation")]
    public virtual ICollection<SegTransaction> seg_transactions { get; set; } = new List<SegTransaction>();
}
