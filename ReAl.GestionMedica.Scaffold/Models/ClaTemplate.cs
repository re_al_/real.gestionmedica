﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

public partial class ClaTemplate
{
    /// <summary>
    /// Entity identifier
    /// </summary>
    [Key]
    public long id_templates { get; set; }

    public long id_workplaces { get; set; }

    public long id_templates_type { get; set; }

    /// <summary>
    /// Template name
    /// </summary>
    [Column(TypeName = "character varying")]
    public string name { get; set; } = null!;

    /// <summary>
    /// Template description
    /// </summary>
    [StringLength(500)]
    public string description { get; set; } = null!;

    /// <summary>
    /// Document height
    /// </summary>
    [Precision(10, 2)]
    public decimal height { get; set; }

    /// <summary>
    /// Document width
    /// </summary>
    [Precision(10, 2)]
    public decimal width { get; set; }

    /// <summary>
    /// Documen margin top
    /// </summary>
    [Precision(10, 2)]
    public decimal margin_top { get; set; }

    /// <summary>
    /// Documen margin bottom
    /// </summary>
    [Precision(10, 2)]
    public decimal margin_bottom { get; set; }

    /// <summary>
    /// Documen margin right
    /// </summary>
    [Precision(10, 2)]
    public decimal margin_right { get; set; }

    /// <summary>
    /// Documen margin left
    /// </summary>
    [Precision(10, 2)]
    public decimal margin_left { get; set; }

    /// <summary>
    /// Template content in text format
    /// </summary>
    [StringLength(50000)]
    public string? text_format { get; set; }

    /// <summary>
    /// Template content in RTF format
    /// </summary>
    public byte[]? rtf_format { get; set; }

    /// <summary>
    /// Template content in HTML format
    /// </summary>
    [StringLength(100000)]
    public string html_format { get; set; } = null!;

    /// <summary>
    /// Current status
    /// </summary>
    [StringLength(15)]
    public string api_status { get; set; } = null!;

    /// <summary>
    /// Last transaction used
    /// </summary>
    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    /// <summary>
    /// User creation
    /// </summary>
    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    /// <summary>
    /// Timestamp creation
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    /// <summary>
    /// User update
    /// </summary>
    [StringLength(15)]
    public string? api_usumod { get; set; }

    /// <summary>
    /// Timestamp update
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    /// <summary>
    /// User delete
    /// </summary>
    [StringLength(15)]
    public string? api_usudel { get; set; }

    /// <summary>
    /// Timestamp delete
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [ForeignKey("id_templates_type")]
    [InverseProperty("cla_templates")]
    public virtual cla_templates_type id_templates_typeNavigation { get; set; } = null!;

    [ForeignKey("id_workplaces")]
    [InverseProperty("cla_templates")]
    public virtual seg_workplace id_workplacesNavigation { get; set; } = null!;

    [InverseProperty("id_templatesNavigation")]
    public virtual ICollection<PatCertification> pat_certifications { get; set; } = new List<PatCertification>();

    [InverseProperty("id_templatesNavigation")]
    public virtual ICollection<PatPrescription> pat_prescriptions { get; set; } = new List<PatPrescription>();

    [InverseProperty("id_templatesNavigation")]
    public virtual ICollection<PatReport> pat_reports { get; set; } = new List<PatReport>();

    [InverseProperty("id_templatesNavigation")]
    public virtual ICollection<PatSurgery> pat_surgeries { get; set; } = new List<PatSurgery>();
}
