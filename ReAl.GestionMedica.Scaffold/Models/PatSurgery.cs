﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

public partial class PatSurgery
{
    /// <summary>
    /// Entity identifier
    /// </summary>
    [Key]
    public long id_surgeries { get; set; }

    public long id_workplaces { get; set; }

    public long id_patients { get; set; }

    public long id_templates { get; set; }

    /// <summary>
    /// Surgery date
    /// </summary>
    public DateOnly surgery_date { get; set; }

    /// <summary>
    /// Surgery content in text format
    /// </summary>
    [StringLength(50000)]
    public string? text_format { get; set; }

    /// <summary>
    /// Surgery content in RTF format
    /// </summary>
    public byte[]? rtf_format { get; set; }

    /// <summary>
    /// Surgery content in HTML format
    /// </summary>
    [StringLength(100000)]
    public string html_format { get; set; } = null!;

    /// <summary>
    /// Current status
    /// </summary>
    [StringLength(15)]
    public string api_status { get; set; } = null!;

    /// <summary>
    /// Last transaction used
    /// </summary>
    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    /// <summary>
    /// User creation
    /// </summary>
    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    /// <summary>
    /// Timestamp creation
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    /// <summary>
    /// User update
    /// </summary>
    [StringLength(15)]
    public string? api_usumod { get; set; }

    /// <summary>
    /// Timestamp update
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    /// <summary>
    /// User delete
    /// </summary>
    [StringLength(15)]
    public string? api_usudel { get; set; }

    /// <summary>
    /// Timestamp delete
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [ForeignKey("id_patients")]
    [InverseProperty("pat_surgeries")]
    public virtual PatPatient id_patientsNavigation { get; set; } = null!;

    [ForeignKey("id_templates")]
    [InverseProperty("pat_surgeries")]
    public virtual ClaTemplate id_templatesNavigation { get; set; } = null!;

    [ForeignKey("id_workplaces")]
    [InverseProperty("pat_surgeries")]
    public virtual seg_workplace id_workplacesNavigation { get; set; } = null!;
}
