﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

public partial class FinAccount
{
    /// <summary>
    /// Entity identifier
    /// </summary>
    [Key]
    public long id_accounts { get; set; }

    public long id_categories { get; set; }

    public long? id_appointments { get; set; }

    /// <summary>
    /// Account type: Incoming, Outcoming
    /// </summary>
    [StringLength(1)]
    public string type { get; set; } = null!;

    /// <summary>
    /// Name for account
    /// </summary>
    [StringLength(150)]
    public string name { get; set; } = null!;

    /// <summary>
    /// Account description
    /// </summary>
    [StringLength(500)]
    public string? description { get; set; }

    /// <summary>
    /// Accoun date
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime account_date { get; set; }

    /// <summary>
    /// Amount
    /// </summary>
    [Precision(18, 2)]
    public decimal amount { get; set; }

    /// <summary>
    /// Current status
    /// </summary>
    [StringLength(15)]
    public string api_status { get; set; } = null!;

    /// <summary>
    /// Last transaction used
    /// </summary>
    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    /// <summary>
    /// User creation
    /// </summary>
    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    /// <summary>
    /// Timestamp creation
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    /// <summary>
    /// User update
    /// </summary>
    [StringLength(15)]
    public string? api_usumod { get; set; }

    /// <summary>
    /// Timestamp update
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    /// <summary>
    /// User delete
    /// </summary>
    [StringLength(15)]
    public string? api_usudel { get; set; }

    /// <summary>
    /// Timestamp delete
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [ForeignKey("id_appointments")]
    [InverseProperty("fin_accounts")]
    public virtual pat_appointment? id_appointmentsNavigation { get; set; }

    [ForeignKey("id_categories")]
    [InverseProperty("fin_accounts")]
    public virtual fin_category id_categoriesNavigation { get; set; } = null!;
}
