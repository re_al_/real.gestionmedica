﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

[Table("pat_history_neuro3")]
public partial class PatHistoryNeuro3
{
    [Key]
    public long id_history_neuro3 { get; set; }

    public long id_patients { get; set; }

    [StringLength(10)]
    public string? h6reflejos01 { get; set; }

    [StringLength(10)]
    public string? h6reflejos02 { get; set; }

    [StringLength(10)]
    public string? h6reflejos03 { get; set; }

    [StringLength(10)]
    public string? h6reflejos04 { get; set; }

    [StringLength(10)]
    public string? h6reflejos05 { get; set; }

    [StringLength(10)]
    public string? h6reflejos06 { get; set; }

    [StringLength(10)]
    public string? h6reflejos07 { get; set; }

    [StringLength(10)]
    public string? h6reflejos08 { get; set; }

    [StringLength(10)]
    public string? h6reflejos09 { get; set; }

    [StringLength(10)]
    public string? h6reflejos10 { get; set; }

    [StringLength(10)]
    public string? h6miotbicder { get; set; }

    [StringLength(10)]
    public string? h6miotbicizq { get; set; }

    [StringLength(10)]
    public string? h6miottrider { get; set; }

    [StringLength(10)]
    public string? h6miottriizq { get; set; }

    [StringLength(10)]
    public string? h6miotestder { get; set; }

    [StringLength(10)]
    public string? h6miotestizq { get; set; }

    [StringLength(10)]
    public string? h6miotpatder { get; set; }

    [StringLength(10)]
    public string? h6miotpatizq { get; set; }

    [StringLength(10)]
    public string? h6miotaquder { get; set; }

    [StringLength(10)]
    public string? h6miotaquizq { get; set; }

    [StringLength(10)]
    public string? h6patplader { get; set; }

    [StringLength(10)]
    public string? h6patplaizq { get; set; }

    [StringLength(10)]
    public string? h6pathofder { get; set; }

    [StringLength(10)]
    public string? h6pathofizq { get; set; }

    [StringLength(10)]
    public string? h6pattroder { get; set; }

    [StringLength(10)]
    public string? h6pattroizq { get; set; }

    [StringLength(10)]
    public string? h6patpreder { get; set; }

    [StringLength(10)]
    public string? h6patpreizq { get; set; }

    [StringLength(10)]
    public string? h6patpmender { get; set; }

    [StringLength(10)]
    public string? h6patpmenizq { get; set; }

    [StringLength(10)]
    public string? h6supbicder { get; set; }

    [StringLength(10)]
    public string? h6supbicizq { get; set; }

    [StringLength(10)]
    public string? h6suptrider { get; set; }

    [StringLength(10)]
    public string? h6suptriizq { get; set; }

    [StringLength(10)]
    public string? h6supestder { get; set; }

    [StringLength(10)]
    public string? h6supestizq { get; set; }

    [StringLength(100)]
    public string? h6otrmaxilar { get; set; }

    [StringLength(100)]
    public string? h6otrglabelar { get; set; }

    [StringLength(2000)]
    public string? h6sistsensitivo { get; set; }

    [StringLength(2000)]
    public string? h6dolorprof { get; set; }

    [StringLength(2000)]
    public string? h6posicion { get; set; }

    [StringLength(2000)]
    public string? h6vibracion { get; set; }

    [StringLength(2000)]
    public string? h6dospuntos { get; set; }

    [StringLength(2000)]
    public string? h6extincion { get; set; }

    [StringLength(2000)]
    public string? h6estereognosia { get; set; }

    [StringLength(2000)]
    public string? h6grafestesia { get; set; }

    [StringLength(2000)]
    public string? h6localizacion { get; set; }

    public byte[]? h6imagen { get; set; }

    [StringLength(1000)]
    public string? h7rigideznuca { get; set; }

    [StringLength(1000)]
    public string? h7sigkerning { get; set; }

    [StringLength(1000)]
    public string? h7fotofobia { get; set; }

    [StringLength(1000)]
    public string? h7sigbrudzinski { get; set; }

    [StringLength(1000)]
    public string? h7hiperestesia { get; set; }

    [StringLength(1000)]
    public string? h7pulcarotideos { get; set; }

    [StringLength(1000)]
    public string? h7pultemporales { get; set; }

    [StringLength(1000)]
    public string? h7cabeza { get; set; }

    [StringLength(1000)]
    public string? h7cuello { get; set; }

    [StringLength(5000)]
    public string? h7nerviosperif { get; set; }

    [StringLength(5000)]
    public string? h7colvertebral { get; set; }

    [StringLength(5000)]
    public string? h7resumen { get; set; }

    [StringLength(5000)]
    public string? h7diagnosticos { get; set; }

    [StringLength(5000)]
    public string? h7conducta { get; set; }

    [StringLength(5000)]
    public string? h7tratamiento { get; set; }

    [StringLength(15)]
    public string api_status { get; set; } = null!;

    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    [StringLength(15)]
    public string? api_usumod { get; set; }

    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    [StringLength(15)]
    public string? api_usudel { get; set; }

    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [ForeignKey("id_patients")]
    [InverseProperty("pat_history_neuro3s")]
    public virtual PatPatient id_patientsNavigation { get; set; } = null!;
}
