﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

[Table("pat_history_neuro2")]
public partial class PatHistoryNeuro2
{
    [Key]
    public long id_history_neuro2 { get; set; }

    public long id_patients { get; set; }

    [StringLength(200)]
    public string? h4olfnormal { get; set; }

    [StringLength(200)]
    public string? h4olfanosmia { get; set; }

    [StringLength(200)]
    public string? h4optscod { get; set; }

    [StringLength(200)]
    public string? h4optscoi { get; set; }

    [StringLength(200)]
    public string? h4optccod { get; set; }

    [StringLength(200)]
    public string? h4optccoi { get; set; }

    [StringLength(200)]
    public string? h4fondood { get; set; }

    [StringLength(200)]
    public string? h4fondooi { get; set; }

    [StringLength(5000)]
    public string? h4campimetria { get; set; }

    [StringLength(200)]
    public string? h4prosis { get; set; }

    [StringLength(200)]
    public string? h4posicionojos { get; set; }

    [StringLength(200)]
    public string? h4exoftalmia { get; set; }

    [StringLength(200)]
    public string? h4horner { get; set; }

    [StringLength(200)]
    public string? h4movoculares { get; set; }

    [StringLength(200)]
    public string? h4nistagmux { get; set; }

    [StringLength(10)]
    public string? h4diplopia1 { get; set; }

    [StringLength(10)]
    public string? h4diplopia2 { get; set; }

    [StringLength(10)]
    public string? h4diplopia3 { get; set; }

    [StringLength(10)]
    public string? h4diplopia4 { get; set; }

    [StringLength(10)]
    public string? h4diplopia5 { get; set; }

    [StringLength(10)]
    public string? h4diplopia6 { get; set; }

    [StringLength(10)]
    public string? h4diplopia7 { get; set; }

    [StringLength(10)]
    public string? h4diplopia8 { get; set; }

    [StringLength(10)]
    public string? h4diplopia9 { get; set; }

    [StringLength(200)]
    public string? h4convergencia { get; set; }

    [StringLength(200)]
    public string? h4acomodacionod { get; set; }

    [StringLength(200)]
    public string? h4acomodacionoi { get; set; }

    [StringLength(200)]
    public string? h4pupulasod { get; set; }

    [StringLength(200)]
    public string? h4pupulasoi { get; set; }

    [StringLength(200)]
    public string? h4forma { get; set; }

    [StringLength(200)]
    public string? h4fotomotorod { get; set; }

    [StringLength(200)]
    public string? h4fotomotoroi { get; set; }

    [StringLength(200)]
    public string? h4consensualdai { get; set; }

    [StringLength(200)]
    public string? h4consensualiad { get; set; }

    [StringLength(200)]
    public string? h4senfacialder { get; set; }

    [StringLength(200)]
    public string? h4senfacializq { get; set; }

    [StringLength(200)]
    public string? h4reflejoder { get; set; }

    [StringLength(200)]
    public string? h4reflejoizq { get; set; }

    [StringLength(500)]
    public string? h4aberturaboca { get; set; }

    [StringLength(500)]
    public string? h4movmasticatorios { get; set; }

    [StringLength(500)]
    public string? h4refmentoniano { get; set; }

    [StringLength(200)]
    public string? h4facsimetria { get; set; }

    [StringLength(500)]
    public string? h4facmovimientos { get; set; }

    [StringLength(200)]
    public string? h4facparalisis { get; set; }

    [StringLength(200)]
    public string? h4faccentral { get; set; }

    [StringLength(200)]
    public string? h4facperiferica { get; set; }

    [StringLength(500)]
    public string? h4facgusto { get; set; }

    [StringLength(1000)]
    public string? h5otoscopia { get; set; }

    [StringLength(200)]
    public string? h5aguaudider { get; set; }

    [StringLength(200)]
    public string? h5aguaudiizq { get; set; }

    [StringLength(200)]
    public string? h5weber { get; set; }

    [StringLength(200)]
    public string? h5rinne { get; set; }

    [StringLength(200)]
    public string? h5pruebaslab { get; set; }

    [StringLength(200)]
    public string? h5elevpaladar { get; set; }

    [StringLength(200)]
    public string? h5uvula { get; set; }

    [StringLength(200)]
    public string? h5refnauceoso { get; set; }

    [StringLength(200)]
    public string? h5deglucion { get; set; }

    [StringLength(200)]
    public string? h5tonovoz { get; set; }

    [StringLength(200)]
    public string? h5esternocleidomastoideo { get; set; }

    [StringLength(200)]
    public string? h5trapecio { get; set; }

    [StringLength(200)]
    public string? h5desviacion { get; set; }

    [StringLength(200)]
    public string? h5atrofia { get; set; }

    [StringLength(200)]
    public string? h5fasciculacion { get; set; }

    [StringLength(200)]
    public string? h5fuerza { get; set; }

    [StringLength(1000)]
    public string? h5marcha { get; set; }

    [StringLength(200)]
    public string? h5tono { get; set; }

    [StringLength(200)]
    public string? h5volumen { get; set; }

    [StringLength(200)]
    public string? h5fasciculaciones { get; set; }

    [StringLength(5000)]
    public string? h5fuemuscular { get; set; }

    [StringLength(5000)]
    public string? h5movinvoluntarios { get; set; }

    [StringLength(200)]
    public string? h5equilibratoria { get; set; }

    [StringLength(200)]
    public string? h5romberg { get; set; }

    [StringLength(100)]
    public string? h5dednarder { get; set; }

    [StringLength(100)]
    public string? h5dednarizq { get; set; }

    [StringLength(200)]
    public string? h5deddedder { get; set; }

    [StringLength(200)]
    public string? h5deddedizq { get; set; }

    [StringLength(200)]
    public string? h5talrodder { get; set; }

    [StringLength(200)]
    public string? h5talrodizq { get; set; }

    [StringLength(500)]
    public string? h5movrapid { get; set; }

    [StringLength(500)]
    public string? h5rebote { get; set; }

    [StringLength(500)]
    public string? h5habilidadespecif { get; set; }

    [StringLength(15)]
    public string api_status { get; set; } = null!;

    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    [StringLength(15)]
    public string? api_usumod { get; set; }

    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    [StringLength(15)]
    public string? api_usudel { get; set; }

    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [ForeignKey("id_patients")]
    [InverseProperty("pat_history_neuro2s")]
    public virtual PatPatient id_patientsNavigation { get; set; } = null!;
}
