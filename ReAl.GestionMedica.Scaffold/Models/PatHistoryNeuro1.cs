﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

[Table("pat_history_neuro1")]
public partial class PatHistoryNeuro1
{
    [Key]
    public long id_history_neuro1 { get; set; }

    public long id_patients { get; set; }

    [StringLength(5000)]
    public string? h1motivo { get; set; }

    [StringLength(5000)]
    public string? h1enfermedad { get; set; }

    [StringLength(1000)]
    public string? h1cirujias { get; set; }

    [StringLength(1000)]
    public string? h1infecciones { get; set; }

    [StringLength(1000)]
    public string? h1traumatismos { get; set; }

    [StringLength(1000)]
    public string? h1cardiovascular { get; set; }

    [StringLength(1000)]
    public string? h1metabolico { get; set; }

    [StringLength(1000)]
    public string? h1toxico { get; set; }

    [StringLength(1000)]
    public string? h1familiar { get; set; }

    [StringLength(1000)]
    public string? h1ginecoobs { get; set; }

    [StringLength(1000)]
    public string? h1otros { get; set; }

    public int? h1embarazos { get; set; }

    public int? h1gesta { get; set; }

    public int? h1abortos { get; set; }

    [StringLength(1000)]
    public string? h1mesntruaciones { get; set; }

    [StringLength(1000)]
    public string? h1fum { get; set; }

    [StringLength(1000)]
    public string? h1fup { get; set; }

    [StringLength(5000)]
    public string? h1revision { get; set; }

    [StringLength(5000)]
    public string? h1examenfisico { get; set; }

    [StringLength(100)]
    public string? h1ta { get; set; }

    [StringLength(100)]
    public string? h1fc { get; set; }

    [StringLength(100)]
    public string? h1fr { get; set; }

    [StringLength(100)]
    public string? h1temp { get; set; }

    [StringLength(100)]
    public string? h1talla { get; set; }

    [StringLength(100)]
    public string? h1peso { get; set; }

    [StringLength(100)]
    public string? h1pc { get; set; }

    [StringLength(1000)]
    public string? h2cabeza { get; set; }

    [StringLength(1000)]
    public string? h2cuello { get; set; }

    [StringLength(1000)]
    public string? h2torax { get; set; }

    [StringLength(1000)]
    public string? h2abdomen { get; set; }

    [StringLength(1000)]
    public string? h2genitourinario { get; set; }

    [StringLength(1000)]
    public string? h2extremidades { get; set; }

    [StringLength(100)]
    public string? h2conalerta { get; set; }

    [StringLength(100)]
    public string? h2conconfusion { get; set; }

    [StringLength(100)]
    public string? h2consomnolencia { get; set; }

    [StringLength(100)]
    public string? h2conestufor { get; set; }

    [StringLength(100)]
    public string? h2concoma { get; set; }

    [StringLength(100)]
    public string? h2oriespacio { get; set; }

    [StringLength(100)]
    public string? h2orilugar { get; set; }

    [StringLength(100)]
    public string? h2oripersona { get; set; }

    [StringLength(100)]
    public string? h2razidea { get; set; }

    [StringLength(100)]
    public string? h2razjuicio { get; set; }

    [StringLength(100)]
    public string? h2razabstraccion { get; set; }

    [StringLength(5000)]
    public string? h2conocimientosgenerales { get; set; }

    [StringLength(100)]
    public string? h2atgdesatento { get; set; }

    [StringLength(100)]
    public string? h2atgfluctuante { get; set; }

    [StringLength(100)]
    public string? h2atgnegativo { get; set; }

    [StringLength(100)]
    public string? h2atgnegligente { get; set; }

    [StringLength(100)]
    public string? h2pagconfusion { get; set; }

    [StringLength(100)]
    public string? h2pagdelirio { get; set; }

    [StringLength(100)]
    public string? h2paedelusiones { get; set; }

    [StringLength(100)]
    public string? h2paeilsuiones { get; set; }

    [StringLength(100)]
    public string? h2paealucinaciones { get; set; }

    [StringLength(100)]
    public string? h3conreciente { get; set; }

    [StringLength(100)]
    public string? h3conremota { get; set; }

    [StringLength(100)]
    public string? h3afeeuforico { get; set; }

    [StringLength(100)]
    public string? h3afeplano { get; set; }

    [StringLength(100)]
    public string? h3afedeprimido { get; set; }

    [StringLength(100)]
    public string? h3afelabil { get; set; }

    [StringLength(100)]
    public string? h3afehostil { get; set; }

    [StringLength(100)]
    public string? h3afeinadecuado { get; set; }

    [StringLength(100)]
    public string? h3hdoizquierdo { get; set; }

    [StringLength(100)]
    public string? h3hdoderecho { get; set; }

    [StringLength(100)]
    public string? h3cgeexpresion { get; set; }

    [StringLength(100)]
    public string? h3cgenominacion { get; set; }

    [StringLength(100)]
    public string? h3cgerepeticion { get; set; }

    [StringLength(100)]
    public string? h3cgecomprension { get; set; }

    [StringLength(100)]
    public string? h3apebroca { get; set; }

    [StringLength(100)]
    public string? h3apeconduccion { get; set; }

    [StringLength(100)]
    public string? h3apewernicke { get; set; }

    [StringLength(100)]
    public string? h3apeglobal { get; set; }

    [StringLength(100)]
    public string? h3apeanomia { get; set; }

    [StringLength(100)]
    public string? h3atrmotora { get; set; }

    [StringLength(100)]
    public string? h3atrsensitiva { get; set; }

    [StringLength(100)]
    public string? h3atrmixta { get; set; }

    [StringLength(100)]
    public string? h3ataexpresion { get; set; }

    [StringLength(100)]
    public string? h3atacomprension { get; set; }

    [StringLength(100)]
    public string? h3atamixta { get; set; }

    [StringLength(200)]
    public string? h3aprmotora { get; set; }

    [StringLength(200)]
    public string? h3aprsensorial { get; set; }

    [StringLength(200)]
    public string? h3lesalexia { get; set; }

    [StringLength(200)]
    public string? h3lesagrafia { get; set; }

    [StringLength(100)]
    public string? h3lpdacalculia { get; set; }

    [StringLength(100)]
    public string? h3lpdagrafia { get; set; }

    [StringLength(100)]
    public string? h3lpddesorientacion { get; set; }

    [StringLength(100)]
    public string? h3lpdagnosia { get; set; }

    [StringLength(100)]
    public string? h3agnauditiva { get; set; }

    [StringLength(100)]
    public string? h3agnvisual { get; set; }

    [StringLength(100)]
    public string? h3agntactil { get; set; }

    [StringLength(200)]
    public string? h3aprideacional { get; set; }

    [StringLength(200)]
    public string? h3aprideomotora { get; set; }

    [StringLength(200)]
    public string? h3aprdesatencion { get; set; }

    [StringLength(200)]
    public string? h3anosognosia { get; set; }

    [StringLength(15)]
    public string api_status { get; set; } = null!;

    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    [StringLength(15)]
    public string? api_usumod { get; set; }

    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    [StringLength(15)]
    public string? api_usudel { get; set; }

    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [ForeignKey("id_patients")]
    [InverseProperty("pat_history_neuro1s")]
    public virtual PatPatient id_patientsNavigation { get; set; } = null!;
}
