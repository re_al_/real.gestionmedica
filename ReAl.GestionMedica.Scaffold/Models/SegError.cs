﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

/// <summary>
/// Almacena los mensajes de error que se originan en la operacion del sistema, se clasifica los mismos por el codigo de aplicacion
/// </summary>
public partial class SegError
{
    /// <summary>
    /// Entity identifier
    /// </summary>
    [Key]
    public long id_errors { get; set; }

    public long? id_modules { get; set; }

    /// <summary>
    /// Error code
    /// </summary>
    [StringLength(10)]
    public string code { get; set; } = null!;

    /// <summary>
    /// Error description
    /// </summary>
    [StringLength(180)]
    public string description { get; set; } = null!;

    /// <summary>
    /// Error cause
    /// </summary>
    [StringLength(700)]
    public string causes { get; set; } = null!;

    /// <summary>
    /// Error actions
    /// </summary>
    [StringLength(700)]
    public string actions { get; set; } = null!;

    /// <summary>
    /// Error comments
    /// </summary>
    [StringLength(180)]
    public string? comments { get; set; }

    /// <summary>
    /// Origin for error
    /// </summary>
    [StringLength(100)]
    public string? origin { get; set; }

    /// <summary>
    /// Current status
    /// </summary>
    [StringLength(15)]
    public string api_status { get; set; } = null!;

    /// <summary>
    /// Last transaction used
    /// </summary>
    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    /// <summary>
    /// User creation
    /// </summary>
    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    /// <summary>
    /// Timestamp creation
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    /// <summary>
    /// User update
    /// </summary>
    [StringLength(15)]
    public string? api_usumod { get; set; }

    /// <summary>
    /// Timestamp update
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    /// <summary>
    /// User delete
    /// </summary>
    [StringLength(15)]
    public string? api_usudel { get; set; }

    /// <summary>
    /// Timestamp delete
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [ForeignKey("id_modules")]
    [InverseProperty("seg_errors")]
    public virtual SegModule? id_modulesNavigation { get; set; }
}
