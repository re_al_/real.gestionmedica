﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

/// <summary>
/// Contiene los nombres fisico de las todas las paginas del sistema, clasificadas por aplicacion, se valida que la extencion del nombre de pagina termine con los caracteres .aspx
/// </summary>
public partial class SegPage
{
    /// <summary>
    /// Entity identifier
    /// </summary>
    [Key]
    public long id_pages { get; set; }

    public long? id_modules { get; set; }

    /// <summary>
    /// Paren id for recursive relationship
    /// </summary>
    public long? id_pages_parent { get; set; }

    /// <summary>
    /// Text in menu
    /// </summary>
    [StringLength(60)]
    public string menu { get; set; } = null!;

    /// <summary>
    /// Text for tooltip
    /// </summary>
    [StringLength(100)]
    public string tooltip { get; set; } = null!;

    /// <summary>
    /// Priority order
    /// </summary>
    public int priority { get; set; }

    /// <summary>
    /// Icon for menu in website
    /// </summary>
    [StringLength(60)]
    public string iconospg { get; set; } = null!;

    /// <summary>
    /// Current status
    /// </summary>
    [StringLength(15)]
    public string api_status { get; set; } = null!;

    /// <summary>
    /// Last transaction used
    /// </summary>
    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    /// <summary>
    /// User creation
    /// </summary>
    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    /// <summary>
    /// Timestamp creation
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    /// <summary>
    /// User update
    /// </summary>
    [StringLength(15)]
    public string? api_usumod { get; set; }

    /// <summary>
    /// Timestamp update
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    /// <summary>
    /// User delete
    /// </summary>
    [StringLength(15)]
    public string? api_usudel { get; set; }

    /// <summary>
    /// Timestamp delete
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [InverseProperty("id_pages_parentNavigation")]
    public virtual ICollection<SegPage> Inverseid_pages_parentNavigation { get; set; } = new List<SegPage>();

    [ForeignKey("id_modules")]
    [InverseProperty("seg_pages")]
    public virtual SegModule? id_modulesNavigation { get; set; }

    [ForeignKey("id_pages_parent")]
    [InverseProperty("Inverseid_pages_parentNavigation")]
    public virtual SegPage? id_pages_parentNavigation { get; set; }

    [InverseProperty("id_pagesNavigation")]
    public virtual ICollection<seg_roles_page> seg_roles_pages { get; set; } = new List<seg_roles_page>();
}
