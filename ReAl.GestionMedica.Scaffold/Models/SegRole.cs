﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

/// <summary>
/// Definicion de ROLES de operacion en el sistema que se asignan a los distintos usuarios
/// </summary>
public partial class SegRole
{
    /// <summary>
    /// Entity identifier
    /// </summary>
    [Key]
    public long id_roles { get; set; }

    /// <summary>
    /// Role code
    /// </summary>
    [StringLength(30)]
    public string code { get; set; } = null!;

    /// <summary>
    /// Role description
    /// </summary>
    [StringLength(60)]
    public string description { get; set; } = null!;

    /// <summary>
    /// Current status
    /// </summary>
    [StringLength(15)]
    public string api_status { get; set; } = null!;

    /// <summary>
    /// Last transaction used
    /// </summary>
    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    /// <summary>
    /// User creation
    /// </summary>
    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    /// <summary>
    /// Timestamp creation
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    /// <summary>
    /// User update
    /// </summary>
    [StringLength(15)]
    public string? api_usumod { get; set; }

    /// <summary>
    /// Timestamp update
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    /// <summary>
    /// User delete
    /// </summary>
    [StringLength(15)]
    public string? api_usudel { get; set; }

    /// <summary>
    /// Timestamp delete
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [InverseProperty("id_rolesNavigation")]
    public virtual ICollection<seg_roles_page> seg_roles_pages { get; set; } = new List<seg_roles_page>();

    [InverseProperty("id_rolesNavigation")]
    public virtual ICollection<seg_users_restriction> seg_users_restrictions { get; set; } = new List<seg_users_restriction>();
}
