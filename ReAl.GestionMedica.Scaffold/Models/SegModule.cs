﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

/// <summary>
/// Codigo y descripcion de cada uno de los sub sistemas del sistema SGIBS, el codigo de la aplicacion es una variable de tipo caracter que se utiliza como identificador de todos los objetos que pertenecen a este sub sistema
/// </summary>
public partial class SegModule
{
    /// <summary>
    /// Entity identifier
    /// </summary>
    [Key]
    public long id_modules { get; set; }

    /// <summary>
    /// Module code
    /// </summary>
    [StringLength(20)]
    public string code { get; set; } = null!;

    /// <summary>
    /// Module description
    /// </summary>
    [StringLength(60)]
    public string description { get; set; } = null!;

    /// <summary>
    /// Current status
    /// </summary>
    [StringLength(15)]
    public string api_status { get; set; } = null!;

    /// <summary>
    /// Last transaction used
    /// </summary>
    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    /// <summary>
    /// User creation
    /// </summary>
    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    /// <summary>
    /// Timestamp creation
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    /// <summary>
    /// User update
    /// </summary>
    [StringLength(15)]
    public string? api_usumod { get; set; }

    /// <summary>
    /// Timestamp update
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    /// <summary>
    /// User delete
    /// </summary>
    [StringLength(15)]
    public string? api_usudel { get; set; }

    /// <summary>
    /// Timestamp delete
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [InverseProperty("id_modulesNavigation")]
    public virtual ICollection<SegError> seg_errors { get; set; } = new List<SegError>();

    [InverseProperty("id_modulesNavigation")]
    public virtual ICollection<SegPage> seg_pages { get; set; } = new List<SegPage>();

    [InverseProperty("id_modulesNavigation")]
    public virtual ICollection<SegTable> seg_tables { get; set; } = new List<SegTable>();
}
