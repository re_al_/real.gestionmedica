﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

/// <summary>
/// Indica las TRANSICIONES entre ESTADOS que se definen para una determinada TABLA
/// </summary>
public partial class SegTransition
{
    /// <summary>
    /// Entity identifier
    /// </summary>
    [Key]
    public long id_transitions { get; set; }

    public long id_states_ini { get; set; }

    public long id_transactions { get; set; }

    public long id_states_end { get; set; }

    /// <summary>
    /// Current status
    /// </summary>
    [StringLength(15)]
    public string api_status { get; set; } = null!;

    /// <summary>
    /// Last transaction used
    /// </summary>
    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    /// <summary>
    /// User creation
    /// </summary>
    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    /// <summary>
    /// Timestamp creation
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    /// <summary>
    /// User update
    /// </summary>
    [StringLength(15)]
    public string? api_usumod { get; set; }

    /// <summary>
    /// Timestamp update
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    /// <summary>
    /// User delete
    /// </summary>
    [StringLength(15)]
    public string? api_usudel { get; set; }

    /// <summary>
    /// Timestamp delete
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [ForeignKey("id_states_end")]
    [InverseProperty("seg_transitionid_states_endNavigations")]
    public virtual SegState id_states_endNavigation { get; set; } = null!;

    [ForeignKey("id_states_ini")]
    [InverseProperty("seg_transitionid_states_iniNavigations")]
    public virtual SegState id_states_iniNavigation { get; set; } = null!;

    [ForeignKey("id_transactions")]
    [InverseProperty("seg_transitions")]
    public virtual SegTransaction id_transactionsNavigation { get; set; } = null!;
}
