﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace ReAl.GestionMedica.Scaffold.Models;

/// <summary>
/// Registro de Pacientes
/// </summary>
public partial class PatPatient
{
    /// <summary>
    /// Entity identifier
    /// </summary>
    [Key]
    public long id_patients { get; set; }

    /// <summary>
    /// Patient ID number (also used to log in to the website)
    /// </summary>
    [StringLength(10)]
    public string? identity_number { get; set; }

    /// <summary>
    /// Patient first name
    /// </summary>
    [StringLength(200)]
    public string first_name { get; set; } = null!;

    /// <summary>
    /// Patient last name
    /// </summary>
    [StringLength(200)]
    public string? last_name { get; set; }

    /// <summary>
    /// Patient gender
    /// </summary>
    public bool gender { get; set; }

    /// <summary>
    /// Birth date
    /// </summary>
    public DateOnly birthdate { get; set; }

    public long id_blood_types { get; set; }

    [MaxLength(1)]
    public char id_marital { get; set; }

    /// <summary>
    /// Patient occupation
    /// </summary>
    [StringLength(300)]
    public string? occupation { get; set; }

    /// <summary>
    /// Patient origin
    /// </summary>
    [StringLength(300)]
    public string? origin { get; set; }

    /// <summary>
    /// Patient address
    /// </summary>
    [StringLength(500)]
    public string? address { get; set; }

    /// <summary>
    /// Dominant hand
    /// </summary>
    [StringLength(100)]
    public string? dominance { get; set; }

    /// <summary>
    /// Patient phone
    /// </summary>
    [StringLength(200)]
    public string? phone { get; set; }

    /// <summary>
    /// Patient email
    /// </summary>
    [StringLength(200)]
    public string? email { get; set; }

    /// <summary>
    /// Patient religion
    /// </summary>
    [StringLength(200)]
    public string? religion { get; set; }

    /// <summary>
    /// Patient company
    /// </summary>
    [StringLength(500)]
    public string? company { get; set; }

    /// <summary>
    /// Patient health insurance
    /// </summary>
    [StringLength(200)]
    public string? insurance { get; set; }

    /// <summary>
    /// Patient referral
    /// </summary>
    [StringLength(500)]
    public string? refers { get; set; }

    /// <summary>
    /// Patient informant
    /// </summary>
    [StringLength(500)]
    public string? informant { get; set; }

    /// <summary>
    /// Current status
    /// </summary>
    [StringLength(15)]
    public string api_status { get; set; } = null!;

    /// <summary>
    /// Last transaction used
    /// </summary>
    [StringLength(40)]
    public string api_transaction { get; set; } = null!;

    /// <summary>
    /// User creation
    /// </summary>
    [StringLength(15)]
    public string api_usucre { get; set; } = null!;

    /// <summary>
    /// Timestamp creation
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime api_feccre { get; set; }

    /// <summary>
    /// User update
    /// </summary>
    [StringLength(15)]
    public string? api_usumod { get; set; }

    /// <summary>
    /// Timestamp update
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecmod { get; set; }

    /// <summary>
    /// User delete
    /// </summary>
    [StringLength(15)]
    public string? api_usudel { get; set; }

    /// <summary>
    /// Timestamp delete
    /// </summary>
    [Column(TypeName = "timestamp without time zone")]
    public DateTime? api_fecdel { get; set; }

    [ForeignKey("id_blood_types")]
    [InverseProperty("pat_patients")]
    public virtual ClaBloodType id_blood_typesNavigation { get; set; } = null!;

    [ForeignKey("id_marital")]
    [InverseProperty("pat_patients")]
    public virtual ClaMarital id_maritalNavigation { get; set; } = null!;

    [InverseProperty("id_patientsNavigation")]
    public virtual ICollection<pat_appointment> pat_appointments { get; set; } = new List<pat_appointment>();

    [InverseProperty("id_patientsNavigation")]
    public virtual ICollection<PatCertification> pat_certifications { get; set; } = new List<PatCertification>();

    [InverseProperty("id_patientsNavigation")]
    public virtual ICollection<PatConsultation> pat_consultations { get; set; } = new List<PatConsultation>();

    [InverseProperty("id_patientsNavigation")]
    public virtual ICollection<PatHistoryNeuro1> pat_history_neuro1s { get; set; } = new List<PatHistoryNeuro1>();

    [InverseProperty("id_patientsNavigation")]
    public virtual ICollection<PatHistoryNeuro2> pat_history_neuro2s { get; set; } = new List<PatHistoryNeuro2>();

    [InverseProperty("id_patientsNavigation")]
    public virtual ICollection<pat_history_neuro3> pat_history_neuro3s { get; set; } = new List<pat_history_neuro3>();

    [InverseProperty("id_patientsNavigation")]
    public virtual ICollection<PatMultimedia> pat_multimedia { get; set; } = new List<PatMultimedia>();

    [InverseProperty("id_patientsNavigation")]
    public virtual ICollection<PatPrescription> pat_prescriptions { get; set; } = new List<PatPrescription>();

    [InverseProperty("id_patientsNavigation")]
    public virtual ICollection<PatReport> pat_reports { get; set; } = new List<PatReport>();

    [InverseProperty("id_patientsNavigation")]
    public virtual ICollection<PatSurgery> pat_surgeries { get; set; } = new List<PatSurgery>();
}
