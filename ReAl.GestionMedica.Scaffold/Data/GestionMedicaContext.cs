﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using ReAl.GestionMedica.Scaffold.Models;

namespace ReAl.GestionMedica.Scaffold.Data;

public partial class GestionMedicaContext : DbContext
{
    public GestionMedicaContext()
    {
    }

    public GestionMedicaContext(DbContextOptions<GestionMedicaContext> options)
        : base(options)
    {
    }

    public virtual DbSet<ClaBloodType> cla_blood_types { get; set; }

    public virtual DbSet<ClaCie> cla_cies { get; set; }

    public virtual DbSet<ClaMarital> cla_maritals { get; set; }

    public virtual DbSet<ClaTemplate> cla_templates { get; set; }

    public virtual DbSet<cla_templates_type> cla_templates_types { get; set; }

    public virtual DbSet<FinAccount> fin_accounts { get; set; }

    public virtual DbSet<fin_category> fin_categories { get; set; }

    public virtual DbSet<pat_appointment> pat_appointments { get; set; }

    public virtual DbSet<PatCertification> pat_certifications { get; set; }

    public virtual DbSet<PatConsultation> pat_consultations { get; set; }

    public virtual DbSet<PatHistoryNeuro1> pat_history_neuro1s { get; set; }

    public virtual DbSet<PatHistoryNeuro2> pat_history_neuro2s { get; set; }

    public virtual DbSet<pat_history_neuro3> pat_history_neuro3s { get; set; }

    public virtual DbSet<PatMultimedia> pat_multimedia { get; set; }

    public virtual DbSet<PatPatient> pat_patients { get; set; }

    public virtual DbSet<PatPrescription> pat_prescriptions { get; set; }

    public virtual DbSet<PatReport> pat_reports { get; set; }

    public virtual DbSet<PatSurgery> pat_surgeries { get; set; }

    public virtual DbSet<SegError> seg_errors { get; set; }

    public virtual DbSet<SegModule> seg_modules { get; set; }

    public virtual DbSet<SegPage> seg_pages { get; set; }

    public virtual DbSet<SegRole> seg_roles { get; set; }

    public virtual DbSet<seg_roles_page> seg_roles_pages { get; set; }

    public virtual DbSet<SegState> seg_states { get; set; }

    public virtual DbSet<SegTable> seg_tables { get; set; }

    public virtual DbSet<SegTransaction> seg_transactions { get; set; }

    public virtual DbSet<SegTransition> seg_transitions { get; set; }

    public virtual DbSet<seg_user> seg_users { get; set; }

    public virtual DbSet<seg_users_refresh_token> seg_users_refresh_tokens { get; set; }

    public virtual DbSet<seg_users_restriction> seg_users_restrictions { get; set; }

    public virtual DbSet<seg_workplace> seg_workplaces { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseNpgsql("Host=34.174.164.220;Database=dbqh8gsmgtglsi;Username=ufm2i9xourat6;Password=Desa2023");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ClaBloodType>(entity =>
        {
            entity.HasKey(e => e.id_blood_types).HasName("cla_blood_types_pk");

            entity.Property(e => e.id_blood_types).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.description).HasComment("Blod type name");
        });

        modelBuilder.Entity<ClaCie>(entity =>
        {
            entity.HasKey(e => e.id_cie).HasName("cla_cie_pk");

            entity.Property(e => e.id_cie).HasComment("Entity description");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.code).HasComment("CIE codification");
            entity.Property(e => e.description).HasComment("CIE description");
        });

        modelBuilder.Entity<ClaMarital>(entity =>
        {
            entity.HasKey(e => e.id_marital).HasName("cla_marital_pk");

            entity.Property(e => e.id_marital).ValueGeneratedNever();
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
        });

        modelBuilder.Entity<ClaTemplate>(entity =>
        {
            entity.HasKey(e => e.id_templates).HasName("cla_templates_pk");

            entity.Property(e => e.id_templates).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.description).HasComment("Template description");
            entity.Property(e => e.height)
                .HasDefaultValueSql("2")
                .HasComment("Document height");
            entity.Property(e => e.html_format).HasComment("Template content in HTML format");
            entity.Property(e => e.margin_bottom)
                .HasDefaultValueSql("2")
                .HasComment("Documen margin bottom");
            entity.Property(e => e.margin_left)
                .HasDefaultValueSql("2")
                .HasComment("Documen margin left");
            entity.Property(e => e.margin_right)
                .HasDefaultValueSql("2")
                .HasComment("Documen margin right");
            entity.Property(e => e.margin_top)
                .HasDefaultValueSql("2")
                .HasComment("Documen margin top");
            entity.Property(e => e.name).HasComment("Template name");
            entity.Property(e => e.rtf_format).HasComment("Template content in RTF format");
            entity.Property(e => e.text_format).HasComment("Template content in text format");
            entity.Property(e => e.width)
                .HasDefaultValueSql("2")
                .HasComment("Document width");

            entity.HasOne(d => d.id_templates_typeNavigation).WithMany(p => p.cla_templates)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("cla_templates_type_cla_templates");

            entity.HasOne(d => d.id_workplacesNavigation).WithMany(p => p.cla_templates)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("seg_workplaces_cla_templates");
        });

        modelBuilder.Entity<cla_templates_type>(entity =>
        {
            entity.HasKey(e => e.id_templates_type).HasName("cla_templates_type_pk");

            entity.Property(e => e.id_templates_type).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.description).HasComment("Template type description");
            entity.Property(e => e.name).HasComment("Template type name");
        });

        modelBuilder.Entity<FinAccount>(entity =>
        {
            entity.HasKey(e => e.id_accounts).HasName("fin_accounts_pk");

            entity.Property(e => e.id_accounts).HasComment("Entity identifier");
            entity.Property(e => e.account_date).HasComment("Accoun date");
            entity.Property(e => e.amount)
                .HasDefaultValueSql("0.00")
                .HasComment("Amount");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.description).HasComment("Account description");
            entity.Property(e => e.name).HasComment("Name for account");
            entity.Property(e => e.type).HasComment("Account type: Incoming, Outcoming");

            entity.HasOne(d => d.id_appointmentsNavigation).WithMany(p => p.fin_accounts).HasConstraintName("pat_appointments_fin_accounts");

            entity.HasOne(d => d.id_categoriesNavigation).WithMany(p => p.fin_accounts)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fin_categories_fin_accounts");
        });

        modelBuilder.Entity<fin_category>(entity =>
        {
            entity.HasKey(e => e.id_categories).HasName("fin_categories_pk");

            entity.Property(e => e.id_categories).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.description).HasComment("Financial category description");
        });

        modelBuilder.Entity<pat_appointment>(entity =>
        {
            entity.HasKey(e => e.id_appointments).HasName("pat_appointments_pk");

            entity.Property(e => e.id_appointments).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.description).HasComment("Appointment details");
            entity.Property(e => e.labels).HasComment("Appointment labels");
            entity.Property(e => e.leaving_reason).HasComment("Reasons for non-attendance");
            entity.Property(e => e.location).HasComment("Appointment location");
            entity.Property(e => e.subject).HasComment("Appointment subject");
            entity.Property(e => e.time_end).HasComment("Appointment date and time for end");
            entity.Property(e => e.time_start).HasComment("Appointment date and time for init");

            entity.HasOne(d => d.id_patientsNavigation).WithMany(p => p.pat_appointments).HasConstraintName("pat_patients_pat_appointments");

            entity.HasOne(d => d.id_usersNavigation).WithMany(p => p.pat_appointments).HasConstraintName("seg_users_pat_appointments");
        });

        modelBuilder.Entity<PatCertification>(entity =>
        {
            entity.HasKey(e => e.id_certifications).HasName("pat_certifications_pk");

            entity.Property(e => e.id_certifications).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.certification_date).HasComment("Certification date");
            entity.Property(e => e.html_format).HasComment("Certification content in HTML format");
            entity.Property(e => e.registration).HasComment("Restistration number (matricula)");
            entity.Property(e => e.rtf_format).HasComment("Certification content in RTF format");
            entity.Property(e => e.text_format).HasComment("Certification content in text format");

            entity.HasOne(d => d.id_patientsNavigation).WithMany(p => p.pat_certifications)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("pat_patients_pat_certifications");

            entity.HasOne(d => d.id_templatesNavigation).WithMany(p => p.pat_certifications)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("cla_templates_pat_certifications");

            entity.HasOne(d => d.id_workplacesNavigation).WithMany(p => p.pat_certifications)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("seg_workplaces_pat_certifications");
        });

        modelBuilder.Entity<PatConsultation>(entity =>
        {
            entity.HasKey(e => e.id_consultations).HasName("pat_consultations_pk");

            entity.Property(e => e.id_consultations).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.behaviour).HasComment("Behaviour in consultation");
            entity.Property(e => e.clinical_profile).HasComment("Clinical profile in consultation");
            entity.Property(e => e.consultation_date).HasComment("Consultation date");
            entity.Property(e => e.consultation_time).HasComment("Consultation hour");
            entity.Property(e => e.diagnosis).HasComment("Diagnostics in consultation");
            entity.Property(e => e.treatment).HasComment("Treatment for consultation");

            entity.HasOne(d => d.id_cieNavigation).WithMany(p => p.pat_consultations)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("cla_cie_pat_consultations");

            entity.HasOne(d => d.id_patientsNavigation).WithMany(p => p.pat_consultations).HasConstraintName("pat_patients_pat_consultations");
        });

        modelBuilder.Entity<PatHistoryNeuro1>(entity =>
        {
            entity.HasKey(e => e.id_history_neuro1).HasName("pat_history_neuro1_pk");

            entity.Property(e => e.api_feccre).HasDefaultValueSql("CURRENT_TIMESTAMP");
            entity.Property(e => e.api_status).HasDefaultValueSql("'ACTIVE'::character varying");
            entity.Property(e => e.api_transaction).HasDefaultValueSql("'INITIAL'::character varying");

            entity.HasOne(d => d.id_patientsNavigation).WithMany(p => p.pat_history_neuro1s)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("pat_patients_pat_history_neuro1");
        });

        modelBuilder.Entity<PatHistoryNeuro2>(entity =>
        {
            entity.HasKey(e => e.id_history_neuro2).HasName("pat_history_neuro2_pk");

            entity.Property(e => e.api_feccre).HasDefaultValueSql("CURRENT_TIMESTAMP");
            entity.Property(e => e.api_status).HasDefaultValueSql("'ACTIVE'::character varying");
            entity.Property(e => e.api_transaction).HasDefaultValueSql("'INITIAL'::character varying");

            entity.HasOne(d => d.id_patientsNavigation).WithMany(p => p.pat_history_neuro2s)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("pat_patients_pat_history_neuro2");
        });

        modelBuilder.Entity<pat_history_neuro3>(entity =>
        {
            entity.HasKey(e => e.id_history_neuro3).HasName("pat_history_neuro3_pk");

            entity.Property(e => e.api_feccre).HasDefaultValueSql("CURRENT_TIMESTAMP");
            entity.Property(e => e.api_status).HasDefaultValueSql("'ACTIVE'::character varying");
            entity.Property(e => e.api_transaction).HasDefaultValueSql("'INITIAL'::character varying");

            entity.HasOne(d => d.id_patientsNavigation).WithMany(p => p.pat_history_neuro3s)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("pat_patients_pat_history_neuro3");
        });

        modelBuilder.Entity<PatMultimedia>(entity =>
        {
            entity.HasKey(e => e.id_multimedia).HasName("pat_multimedia_pk");

            entity.Property(e => e.id_multimedia).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.extension)
                .IsFixedLength()
                .HasComment("Media extension");
            entity.Property(e => e.external_url).HasComment("External URL");
            entity.Property(e => e.filename)
                .HasDefaultValueSql("''::character varying")
                .HasComment("Media filename");
            entity.Property(e => e.observations).HasComment("Media observations");
            entity.Property(e => e.upload_date).HasComment("Upload_date");

            entity.HasOne(d => d.id_patientsNavigation).WithMany(p => p.pat_multimedia)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("pat_patients_pat_multimedia");
        });

        modelBuilder.Entity<PatPatient>(entity =>
        {
            entity.HasKey(e => e.id_patients).HasName("pat_patients_pk");

            entity.ToTable(tb => tb.HasComment("Registro de Pacientes"));

            entity.Property(e => e.id_patients).HasComment("Entity identifier");
            entity.Property(e => e.address).HasComment("Patient address");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.birthdate).HasComment("Birth date");
            entity.Property(e => e.company).HasComment("Patient company");
            entity.Property(e => e.dominance).HasComment("Dominant hand");
            entity.Property(e => e.email).HasComment("Patient email");
            entity.Property(e => e.first_name).HasComment("Patient first name");
            entity.Property(e => e.gender).HasComment("Patient gender");
            entity.Property(e => e.identity_number).HasComment("Patient ID number (also used to log in to the website)");
            entity.Property(e => e.informant).HasComment("Patient informant");
            entity.Property(e => e.insurance).HasComment("Patient health insurance");
            entity.Property(e => e.last_name).HasComment("Patient last name");
            entity.Property(e => e.occupation).HasComment("Patient occupation");
            entity.Property(e => e.origin).HasComment("Patient origin");
            entity.Property(e => e.phone).HasComment("Patient phone");
            entity.Property(e => e.refers).HasComment("Patient referral");
            entity.Property(e => e.religion).HasComment("Patient religion");

            entity.HasOne(d => d.id_blood_typesNavigation).WithMany(p => p.pat_patients)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("cla_blood_types_pat_patients");

            entity.HasOne(d => d.id_maritalNavigation).WithMany(p => p.pat_patients)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("cla_marital_pat_patients");
        });

        modelBuilder.Entity<PatPrescription>(entity =>
        {
            entity.HasKey(e => e.id_prescriptions).HasName("pat_prescriptions_pk");

            entity.Property(e => e.id_prescriptions).HasComment("Entity description");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.html_format).HasComment("Prescription content in HTML format");
            entity.Property(e => e.prescription_date).HasComment("Presciption date");
            entity.Property(e => e.rtf_format).HasComment("Prescription content in RTF format");
            entity.Property(e => e.text_format).HasComment("Prescription content in text format");

            entity.HasOne(d => d.id_patientsNavigation).WithMany(p => p.pat_prescriptions)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("pat_patients_pat_prescriptions");

            entity.HasOne(d => d.id_templatesNavigation).WithMany(p => p.pat_prescriptions)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("cla_templates_pat_prescriptions");

            entity.HasOne(d => d.id_workplacesNavigation).WithMany(p => p.pat_prescriptions)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("seg_workplaces_pat_certifications");
        });

        modelBuilder.Entity<PatReport>(entity =>
        {
            entity.HasKey(e => e.id_reports).HasName("pat_reports_pk");

            entity.Property(e => e.id_reports).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.html_format).HasComment("Report content in HTML format");
            entity.Property(e => e.report_date).HasComment("Report date");
            entity.Property(e => e.rtf_format).HasComment("Report content in RTF format");
            entity.Property(e => e.text_format).HasComment("Report content in text format");

            entity.HasOne(d => d.id_patientsNavigation).WithMany(p => p.pat_reports)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("pat_patients_pat_reports");

            entity.HasOne(d => d.id_templatesNavigation).WithMany(p => p.pat_reports)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("cla_templates_pat_reports");

            entity.HasOne(d => d.id_workplacesNavigation).WithMany(p => p.pat_reports)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("seg_workplaces_pat_certifications");
        });

        modelBuilder.Entity<PatSurgery>(entity =>
        {
            entity.HasKey(e => e.id_surgeries).HasName("pat_surgeries_pk");

            entity.Property(e => e.id_surgeries).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.html_format).HasComment("Surgery content in HTML format");
            entity.Property(e => e.rtf_format).HasComment("Surgery content in RTF format");
            entity.Property(e => e.surgery_date).HasComment("Surgery date");
            entity.Property(e => e.text_format).HasComment("Surgery content in text format");

            entity.HasOne(d => d.id_patientsNavigation).WithMany(p => p.pat_surgeries)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("pat_patients_pat_surgeries");

            entity.HasOne(d => d.id_templatesNavigation).WithMany(p => p.pat_surgeries)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("cla_templates_pat_surgeries");

            entity.HasOne(d => d.id_workplacesNavigation).WithMany(p => p.pat_surgeries)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("seg_workplaces_pat_certifications");
        });

        modelBuilder.Entity<SegError>(entity =>
        {
            entity.HasKey(e => e.id_errors).HasName("seg_errors_pk");

            entity.ToTable(tb => tb.HasComment("Almacena los mensajes de error que se originan en la operacion del sistema, se clasifica los mismos por el codigo de aplicacion"));

            entity.Property(e => e.id_errors).HasComment("Entity identifier");
            entity.Property(e => e.actions).HasComment("Error actions");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.causes).HasComment("Error cause");
            entity.Property(e => e.code).HasComment("Error code");
            entity.Property(e => e.comments).HasComment("Error comments");
            entity.Property(e => e.description).HasComment("Error description");
            entity.Property(e => e.origin).HasComment("Origin for error");

            entity.HasOne(d => d.id_modulesNavigation).WithMany(p => p.seg_errors).HasConstraintName("seg_modules_seg_errors");
        });

        modelBuilder.Entity<SegModule>(entity =>
        {
            entity.HasKey(e => e.id_modules).HasName("seg_modules_pk");

            entity.ToTable(tb => tb.HasComment("Codigo y descripcion de cada uno de los sub sistemas del sistema SGIBS, el codigo de la aplicacion es una variable de tipo caracter que se utiliza como identificador de todos los objetos que pertenecen a este sub sistema"));

            entity.Property(e => e.id_modules).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.code).HasComment("Module code");
            entity.Property(e => e.description)
                .IsFixedLength()
                .HasComment("Module description");
        });

        modelBuilder.Entity<SegPage>(entity =>
        {
            entity.HasKey(e => e.id_pages).HasName("seg_pages_pk");

            entity.ToTable(tb => tb.HasComment("Contiene los nombres fisico de las todas las paginas del sistema, clasificadas por aplicacion, se valida que la extencion del nombre de pagina termine con los caracteres .aspx"));

            entity.Property(e => e.id_pages).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.iconospg).HasComment("Icon for menu in website");
            entity.Property(e => e.id_pages_parent).HasComment("Paren id for recursive relationship");
            entity.Property(e => e.menu).HasComment("Text in menu");
            entity.Property(e => e.priority)
                .HasDefaultValue(1)
                .HasComment("Priority order");
            entity.Property(e => e.tooltip).HasComment("Text for tooltip");

            entity.HasOne(d => d.id_modulesNavigation).WithMany(p => p.seg_pages).HasConstraintName("seg_modules_seg_pages");

            entity.HasOne(d => d.id_pages_parentNavigation).WithMany(p => p.Inverseid_pages_parentNavigation).HasConstraintName("seg_pages_seg_pages");
        });

        modelBuilder.Entity<SegRole>(entity =>
        {
            entity.HasKey(e => e.id_roles).HasName("seg_roles_pk");

            entity.ToTable(tb => tb.HasComment("Definicion de ROLES de operacion en el sistema que se asignan a los distintos usuarios"));

            entity.Property(e => e.id_roles).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.code).HasComment("Role code");
            entity.Property(e => e.description).HasComment("Role description");
        });

        modelBuilder.Entity<seg_roles_page>(entity =>
        {
            entity.HasKey(e => e.id_roles_pages).HasName("seg_roles_pages_pk");

            entity.ToTable(tb => tb.HasComment("Registra las diferentes paginas que son accesibles por un determinado rol"));

            entity.Property(e => e.id_roles_pages).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");

            entity.HasOne(d => d.id_pagesNavigation).WithMany(p => p.seg_roles_pages).HasConstraintName("seg_pages_seg_roles_pages");

            entity.HasOne(d => d.id_rolesNavigation).WithMany(p => p.seg_roles_pages).HasConstraintName("seg_roles_seg_roles_pages");
        });

        modelBuilder.Entity<SegState>(entity =>
        {
            entity.HasKey(e => e.id_states).HasName("seg_states_pk");

            entity.ToTable(tb => tb.HasComment("Almacena todos los posibles estados que puedan poseer los registro de cada una de las tablas, debera existir siempre el estado estado ELABORADO, que es el estado en el cual se encontraran todos los registros una vez que hayan sido creados"));

            entity.Property(e => e.id_states).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.code).HasComment("State code");
            entity.Property(e => e.description).HasComment("State description");

            entity.HasOne(d => d.id_tablesNavigation).WithMany(p => p.seg_states)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("seg_tables_seg_states");
        });

        modelBuilder.Entity<SegTable>(entity =>
        {
            entity.HasKey(e => e.id_tables).HasName("seg_tables_pk");

            entity.Property(e => e.id_tables).HasComment("Entity identifier");
            entity.Property(e => e.alias).HasComment("Table alias for abbreviation");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.description).HasComment("Table description");
            entity.Property(e => e.name).HasComment("Table name");

            entity.HasOne(d => d.id_modulesNavigation).WithMany(p => p.seg_tables).HasConstraintName("seg_modules_seg_tables");
        });

        modelBuilder.Entity<SegTransaction>(entity =>
        {
            entity.HasKey(e => e.id_transactions).HasName("seg_transactions_pk");

            entity.ToTable(tb => tb.HasComment("Almacena las TRANSACCIONES que pueden ser realizadas en una TABLA particular"));

            entity.Property(e => e.id_transactions).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.code).HasComment("Transaction code");
            entity.Property(e => e.description).HasComment("Transaction description");
            entity.Property(e => e.sentence).HasComment("SQL sentence for transaction");

            entity.HasOne(d => d.id_tablesNavigation).WithMany(p => p.seg_transactions)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("seg_tables_seg_transactions");
        });

        modelBuilder.Entity<SegTransition>(entity =>
        {
            entity.HasKey(e => e.id_transitions).HasName("seg_transitions_pk");

            entity.ToTable(tb => tb.HasComment("Indica las TRANSICIONES entre ESTADOS que se definen para una determinada TABLA"));

            entity.Property(e => e.id_transitions).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");

            entity.HasOne(d => d.id_states_endNavigation).WithMany(p => p.seg_transitionid_states_endNavigations)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("seg_states_seg_transitions_end_end");

            entity.HasOne(d => d.id_states_iniNavigation).WithMany(p => p.seg_transitionid_states_iniNavigations)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("seg_states_seg_transitions_ini_ini");

            entity.HasOne(d => d.id_transactionsNavigation).WithMany(p => p.seg_transitions)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("seg_transactions_seg_transitions");
        });

        modelBuilder.Entity<seg_user>(entity =>
        {
            entity.HasKey(e => e.id_users).HasName("seg_users_pk");

            entity.ToTable(tb => tb.HasComment("Almacena la informacion de usuarios del sistema SGIBS que poseen conexion a sus distintos modulos"));

            entity.Property(e => e.id_users).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.first_name).HasComment("User first name");
            entity.Property(e => e.last_name).HasComment("User last name");
            entity.Property(e => e.login).HasComment("User login name");
            entity.Property(e => e.password).HasComment("User password");
        });

        modelBuilder.Entity<seg_users_refresh_token>(entity =>
        {
            entity.HasKey(e => e.id_users_refresh_token).HasName("pk_seg_users_refresh_token");

            entity.Property(e => e.id_users_refresh_token).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.expiration).HasComment("Expiration token");
            entity.Property(e => e.refresh_token).HasComment("User refresh token");

            entity.HasOne(d => d.id_usersNavigation).WithMany(p => p.seg_users_refresh_tokens)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("seg_users_seg_users_refresh_token");
        });

        modelBuilder.Entity<seg_users_restriction>(entity =>
        {
            entity.HasKey(e => e.id_users_restriction).HasName("seg_users_restriction_pk");

            entity.ToTable("seg_users_restriction", tb => tb.HasComment("Registro de usuarios del sistema, se definen aqui el o los distintos roles de operacion que posee un usuario, tambien el nivel de restriccion para cada uno de los roles definidos, los niveles de restriccion se dan a nivel de institucion, gerencia administrativa y unidad ejecutora"));

            entity.Property(e => e.id_users_restriction).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.role_active).HasComment("Is role active?");

            entity.HasOne(d => d.id_rolesNavigation).WithMany(p => p.seg_users_restrictions).HasConstraintName("seg_roles_seg_users_restriction");

            entity.HasOne(d => d.id_usersNavigation).WithMany(p => p.seg_users_restrictions).HasConstraintName("seg_users_seg_users_restriction");

            entity.HasOne(d => d.id_workplacesNavigation).WithMany(p => p.seg_users_restrictions).HasConstraintName("seg_workplaces_seg_users_restriction");
        });

        modelBuilder.Entity<seg_workplace>(entity =>
        {
            entity.HasKey(e => e.id_workplaces).HasName("seg_workplaces_pk");

            entity.ToTable(tb => tb.HasComment("Definicion de CONSULTORIOS en el sistema a los que pertenecen los distintos usuarios"));

            entity.Property(e => e.id_workplaces).HasComment("Entity identifier");
            entity.Property(e => e.api_feccre)
                .HasDefaultValueSql("CURRENT_TIMESTAMP")
                .HasComment("Timestamp creation");
            entity.Property(e => e.api_fecdel).HasComment("Timestamp delete");
            entity.Property(e => e.api_fecmod).HasComment("Timestamp update");
            entity.Property(e => e.api_status)
                .HasDefaultValueSql("'ACTIVE'::character varying")
                .HasComment("Current status");
            entity.Property(e => e.api_transaction)
                .HasDefaultValueSql("'CREATE'::character varying")
                .HasComment("Last transaction used");
            entity.Property(e => e.api_usucre).HasComment("User creation");
            entity.Property(e => e.api_usudel).HasComment("User delete");
            entity.Property(e => e.api_usumod).HasComment("User update");
            entity.Property(e => e.description).HasComment("Workplace description");
            entity.Property(e => e.name).HasComment("Workplace name");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
