#region 
/***********************************************************************************************************
	NOMBRE:       EntSegRoles
	DESCRIPCION:
		Clase que define un objeto para la Tabla segroles

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntSegRoles : CBaseClass
	{
		public const string StrNombreTabla = "SegRoles";
		public const string StrAliasTabla = "Sro";
		public enum Fields
		{
			rolsro
			,siglasro
			,descripcionsro
			,apiestadosro
			,apitransaccionsro
			,usucresro
			,feccresro
			,usumodsro
			,fecmodsro

		}
		
		#region Constructoress
		
		public EntSegRoles()
		{
			//Inicializacion de Variables
			siglasro = null;
			descripcionsro = null;
			apiestadosro = null;
			apitransaccionsro = null;
			usucresro = null;
			usumodsro = null;
			fecmodsro = null;
		}
		
		public EntSegRoles(EntSegRoles obj)
		{
			rolsro = obj.rolsro;
			siglasro = obj.siglasro;
			descripcionsro = obj.descripcionsro;
			apiestadosro = obj.apiestadosro;
			apitransaccionsro = obj.apitransaccionsro;
			usucresro = obj.usucresro;
			feccresro = obj.feccresro;
			usumodsro = obj.usumodsro;
			fecmodsro = obj.fecmodsro;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntSegRoles.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna rolsro de la Tabla segroles
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "rolsro", Description = " Propiedad publica de tipo int que representa a la columna rolsro de la Tabla segroles")]
		[Required(ErrorMessage = "Se necesita un valor para -rolsro- porque es un campo requerido.")]
		[Key]
		public int rolsro { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna siglasro de la Tabla segroles
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(30, MinimumLength=0)]
		[Display(Name = "siglasro", Description = " Propiedad publica de tipo string que representa a la columna siglasro de la Tabla segroles")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -siglasro- porque es un campo requerido.")]
		public string siglasro { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna descripcionsro de la Tabla segroles
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "descripcionsro", Description = " Propiedad publica de tipo string que representa a la columna descripcionsro de la Tabla segroles")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -descripcionsro- porque es un campo requerido.")]
		public string descripcionsro { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestadosro de la Tabla segroles
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestadosro", Description = " Propiedad publica de tipo string que representa a la columna apiestadosro de la Tabla segroles")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestadosro { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccionsro de la Tabla segroles
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apitransaccionsro", Description = " Propiedad publica de tipo string que representa a la columna apitransaccionsro de la Tabla segroles")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccionsro { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucresro de la Tabla segroles
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucresro", Description = " Propiedad publica de tipo string que representa a la columna usucresro de la Tabla segroles")]
		public string usucresro { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccresro de la Tabla segroles
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccresro", Description = " Propiedad publica de tipo DateTime que representa a la columna feccresro de la Tabla segroles")]
		public DateTime feccresro { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumodsro de la Tabla segroles
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumodsro", Description = " Propiedad publica de tipo string que representa a la columna usumodsro de la Tabla segroles")]
		public string usumodsro { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmodsro de la Tabla segroles
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmodsro", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmodsro de la Tabla segroles")]
		public DateTime? fecmodsro { get; set; } 

	}
}

