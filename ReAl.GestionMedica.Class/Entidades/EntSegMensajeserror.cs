#region 
/***********************************************************************************************************
	NOMBRE:       EntSegMensajeserror
	DESCRIPCION:
		Clase que define un objeto para la Tabla segmensajeserror

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntSegMensajeserror : CBaseClass
	{
		public const string StrNombreTabla = "SegMensajeserror";
		public const string StrAliasTabla = "Sme";
		public enum Fields
		{
			errorsme
			,aplicacionsap
			,aplicacionerrorsme
			,descripcionsme
			,causasme
			,accionsme
			,comentariosme
			,origensme
			,apiestadosme
			,apitransaccionsme
			,usucresme
			,feccresme
			,usumodsme
			,fecmodsme

		}
		
		#region Constructoress
		
		public EntSegMensajeserror()
		{
			//Inicializacion de Variables
			aplicacionsap = null;
			aplicacionerrorsme = null;
			descripcionsme = null;
			causasme = null;
			accionsme = null;
			comentariosme = null;
			origensme = null;
			apiestadosme = null;
			apitransaccionsme = null;
			usucresme = null;
			usumodsme = null;
			fecmodsme = null;
		}
		
		public EntSegMensajeserror(EntSegMensajeserror obj)
		{
			errorsme = obj.errorsme;
			aplicacionsap = obj.aplicacionsap;
			aplicacionerrorsme = obj.aplicacionerrorsme;
			descripcionsme = obj.descripcionsme;
			causasme = obj.causasme;
			accionsme = obj.accionsme;
			comentariosme = obj.comentariosme;
			origensme = obj.origensme;
			apiestadosme = obj.apiestadosme;
			apitransaccionsme = obj.apitransaccionsme;
			usucresme = obj.usucresme;
			feccresme = obj.feccresme;
			usumodsme = obj.usumodsme;
			fecmodsme = obj.fecmodsme;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntSegMensajeserror.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna errorsme de la Tabla segmensajeserror
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "errorsme", Description = " Propiedad publica de tipo int que representa a la columna errorsme de la Tabla segmensajeserror")]
		[Required(ErrorMessage = "Se necesita un valor para -errorsme- porque es un campo requerido.")]
		[Key]
		public int errorsme { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna aplicacionsap de la Tabla segmensajeserror
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: Yes
		/// </summary>
		[StringLength(3, MinimumLength=0)]
		[Display(Name = "aplicacionsap", Description = " Propiedad publica de tipo string que representa a la columna aplicacionsap de la Tabla segmensajeserror")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -aplicacionsap- porque es un campo requerido.")]
		[Key]
		public string aplicacionsap { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna aplicacionerrorsme de la Tabla segmensajeserror
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(9, MinimumLength=0)]
		[Display(Name = "aplicacionerrorsme", Description = " Propiedad publica de tipo string que representa a la columna aplicacionerrorsme de la Tabla segmensajeserror")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -aplicacionerrorsme- porque es un campo requerido.")]
		public string aplicacionerrorsme { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna descripcionsme de la Tabla segmensajeserror
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(180, MinimumLength=0)]
		[Display(Name = "descripcionsme", Description = " Propiedad publica de tipo string que representa a la columna descripcionsme de la Tabla segmensajeserror")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -descripcionsme- porque es un campo requerido.")]
		public string descripcionsme { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna causasme de la Tabla segmensajeserror
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(700, MinimumLength=0)]
		[Display(Name = "causasme", Description = " Propiedad publica de tipo string que representa a la columna causasme de la Tabla segmensajeserror")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -causasme- porque es un campo requerido.")]
		public string causasme { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna accionsme de la Tabla segmensajeserror
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(700, MinimumLength=0)]
		[Display(Name = "accionsme", Description = " Propiedad publica de tipo string que representa a la columna accionsme de la Tabla segmensajeserror")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -accionsme- porque es un campo requerido.")]
		public string accionsme { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna comentariosme de la Tabla segmensajeserror
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(180, MinimumLength=0)]
		[Display(Name = "comentariosme", Description = " Propiedad publica de tipo string que representa a la columna comentariosme de la Tabla segmensajeserror")]
		public string comentariosme { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna origensme de la Tabla segmensajeserror
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "origensme", Description = " Propiedad publica de tipo string que representa a la columna origensme de la Tabla segmensajeserror")]
		public string origensme { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestadosme de la Tabla segmensajeserror
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestadosme", Description = " Propiedad publica de tipo string que representa a la columna apiestadosme de la Tabla segmensajeserror")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestadosme { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccionsme de la Tabla segmensajeserror
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apitransaccionsme", Description = " Propiedad publica de tipo string que representa a la columna apitransaccionsme de la Tabla segmensajeserror")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccionsme { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucresme de la Tabla segmensajeserror
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucresme", Description = " Propiedad publica de tipo string que representa a la columna usucresme de la Tabla segmensajeserror")]
		public string usucresme { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccresme de la Tabla segmensajeserror
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccresme", Description = " Propiedad publica de tipo DateTime que representa a la columna feccresme de la Tabla segmensajeserror")]
		public DateTime feccresme { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumodsme de la Tabla segmensajeserror
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumodsme", Description = " Propiedad publica de tipo string que representa a la columna usumodsme de la Tabla segmensajeserror")]
		public string usumodsme { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmodsme de la Tabla segmensajeserror
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmodsme", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmodsme de la Tabla segmensajeserror")]
		public DateTime? fecmodsme { get; set; } 

	}
}

