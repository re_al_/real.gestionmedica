#region 
/***********************************************************************************************************
	NOMBRE:       EntSegUsuariosrestriccion
	DESCRIPCION:
		Clase que define un objeto para la Tabla segusuariosrestriccion

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntSegUsuariosrestriccion : CBaseClass
	{
		public const string StrNombreTabla = "SegUsuariosrestriccion";
		public const string StrAliasTabla = "Sur";
		public enum Fields
		{
			idsur
			,idsus
			,rolsro
			,rolactivosur
			,fechavigentesur
			,apiestadosur
			,apitransaccionsur
			,usucresur
			,feccresur
			,usumodsur
			,fecmodsur

		}
		
		#region Constructoress
		
		public EntSegUsuariosrestriccion()
		{
			//Inicializacion de Variables
			fechavigentesur = null;
			apiestadosur = null;
			apitransaccionsur = null;
			usucresur = null;
			usumodsur = null;
			fecmodsur = null;
		}
		
		public EntSegUsuariosrestriccion(EntSegUsuariosrestriccion obj)
		{
			idsur = obj.idsur;
			idsus = obj.idsus;
			rolsro = obj.rolsro;
			rolactivosur = obj.rolactivosur;
			fechavigentesur = obj.fechavigentesur;
			apiestadosur = obj.apiestadosur;
			apitransaccionsur = obj.apitransaccionsur;
			usucresur = obj.usucresur;
			feccresur = obj.feccresur;
			usumodsur = obj.usumodsur;
			fecmodsur = obj.fecmodsur;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntSegUsuariosrestriccion.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Identificador unico de la tabla
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "idsur", Description = "Identificador unico de la tabla")]
		[Required(ErrorMessage = "Se necesita un valor para -idsur- porque es un campo requerido.")]
		[Key]
		public int idsur { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna idsus de la Tabla segusuariosrestriccion
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Range(0, long.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "idsus", Description = " Propiedad publica de tipo int que representa a la columna idsus de la Tabla segusuariosrestriccion")]
		[Required(ErrorMessage = "Se necesita un valor para -idsus- porque es un campo requerido.")]
		public long idsus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna rolsro de la Tabla segusuariosrestriccion
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "rolsro", Description = " Propiedad publica de tipo int que representa a la columna rolsro de la Tabla segusuariosrestriccion")]
		[Required(ErrorMessage = "Se necesita un valor para -rolsro- porque es un campo requerido.")]
		public int rolsro { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna rolactivosur de la Tabla segusuariosrestriccion
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "rolactivosur", Description = " Propiedad publica de tipo int que representa a la columna rolactivosur de la Tabla segusuariosrestriccion")]
		[Required(ErrorMessage = "Se necesita un valor para -rolactivosur- porque es un campo requerido.")]
		public int rolactivosur { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fechavigentesur de la Tabla segusuariosrestriccion
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fechavigentesur", Description = " Propiedad publica de tipo DateTime que representa a la columna fechavigentesur de la Tabla segusuariosrestriccion")]
		public DateTime? fechavigentesur { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestadosur de la Tabla segusuariosrestriccion
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestadosur", Description = " Propiedad publica de tipo string que representa a la columna apiestadosur de la Tabla segusuariosrestriccion")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestadosur { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccionsur de la Tabla segusuariosrestriccion
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apitransaccionsur", Description = " Propiedad publica de tipo string que representa a la columna apitransaccionsur de la Tabla segusuariosrestriccion")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccionsur { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucresur de la Tabla segusuariosrestriccion
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucresur", Description = " Propiedad publica de tipo string que representa a la columna usucresur de la Tabla segusuariosrestriccion")]
		public string usucresur { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccresur de la Tabla segusuariosrestriccion
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccresur", Description = " Propiedad publica de tipo DateTime que representa a la columna feccresur de la Tabla segusuariosrestriccion")]
		public DateTime feccresur { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumodsur de la Tabla segusuariosrestriccion
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumodsur", Description = " Propiedad publica de tipo string que representa a la columna usumodsur de la Tabla segusuariosrestriccion")]
		public string usumodsur { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmodsur de la Tabla segusuariosrestriccion
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmodsur", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmodsur de la Tabla segusuariosrestriccion")]
		public DateTime? fecmodsur { get; set; } 

	}
}

