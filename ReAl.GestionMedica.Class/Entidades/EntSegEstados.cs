#region 
/***********************************************************************************************************
	NOMBRE:       EntSegEstados
	DESCRIPCION:
		Clase que define un objeto para la Tabla segestados

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntSegEstados : CBaseClass
	{
		public const string StrNombreTabla = "SegEstados";
		public const string StrAliasTabla = "Ses";
		public enum Fields
		{
			tablasta
			,estadoses
			,descripcionses
			,apiestadoses
			,apitransaccionses
			,usucreses
			,feccreses
			,usumodses
			,fecmodses

		}
		
		#region Constructoress
		
		public EntSegEstados()
		{
			//Inicializacion de Variables
			tablasta = null;
			estadoses = null;
			descripcionses = null;
			apiestadoses = null;
			apitransaccionses = null;
			usucreses = null;
			usumodses = null;
			fecmodses = null;
		}
		
		public EntSegEstados(EntSegEstados obj)
		{
			tablasta = obj.tablasta;
			estadoses = obj.estadoses;
			descripcionses = obj.descripcionses;
			apiestadoses = obj.apiestadoses;
			apitransaccionses = obj.apitransaccionses;
			usucreses = obj.usucreses;
			feccreses = obj.feccreses;
			usumodses = obj.usumodses;
			fecmodses = obj.fecmodses;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntSegEstados.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna tablasta de la Tabla segestados
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: Yes
		/// </summary>
		[StringLength(30, MinimumLength=0)]
		[Display(Name = "tablasta", Description = " Propiedad publica de tipo string que representa a la columna tablasta de la Tabla segestados")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -tablasta- porque es un campo requerido.")]
		[Key]
		public string tablasta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna estadoses de la Tabla segestados
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: Yes
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "estadoses", Description = " Propiedad publica de tipo string que representa a la columna estadoses de la Tabla segestados")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -estadoses- porque es un campo requerido.")]
		[Key]
		public string estadoses { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna descripcionses de la Tabla segestados
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "descripcionses", Description = " Propiedad publica de tipo string que representa a la columna descripcionses de la Tabla segestados")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -descripcionses- porque es un campo requerido.")]
		public string descripcionses { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestadoses de la Tabla segestados
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestadoses", Description = " Propiedad publica de tipo string que representa a la columna apiestadoses de la Tabla segestados")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestadoses { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccionses de la Tabla segestados
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apitransaccionses", Description = " Propiedad publica de tipo string que representa a la columna apitransaccionses de la Tabla segestados")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccionses { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucreses de la Tabla segestados
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucreses", Description = " Propiedad publica de tipo string que representa a la columna usucreses de la Tabla segestados")]
		public string usucreses { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccreses de la Tabla segestados
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccreses", Description = " Propiedad publica de tipo DateTime que representa a la columna feccreses de la Tabla segestados")]
		public DateTime feccreses { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumodses de la Tabla segestados
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumodses", Description = " Propiedad publica de tipo string que representa a la columna usumodses de la Tabla segestados")]
		public string usumodses { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmodses de la Tabla segestados
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmodses", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmodses de la Tabla segestados")]
		public DateTime? fecmodses { get; set; } 

	}
}

