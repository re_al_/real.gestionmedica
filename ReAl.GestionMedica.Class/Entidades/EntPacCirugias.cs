#region 
/***********************************************************************************************************
	NOMBRE:       EntPacCirugias
	DESCRIPCION:
		Clase que define un objeto para la Tabla paccirugias

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        16/07/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntPacCirugias : CBaseClass
	{
		public const string StrNombreTabla = "PacCirugias";
		public const string StrAliasTabla = "Pci";
		public enum Fields
		{
			idpci
			,idppa
			,idcpl
			,fechapci
			,rtfpci
			,textopci
			,apiestado
			,apitransaccion
			,usucre
			,feccre
			,usumod
			,fecmod
			,htmlpci

		}
		
		#region Constructoress
		
		public EntPacCirugias()
		{
			//Inicializacion de Variables
			idppa = null;
			idcpl = null;
			fechapci = null;
			rtfpci = null;
			textopci = null;
			apiestado = null;
			apitransaccion = null;
			usucre = null;
			usumod = null;
			fecmod = null;
			htmlpci = null;
		}
		
		public EntPacCirugias(EntPacCirugias obj)
		{
			idpci = obj.idpci;
			idppa = obj.idppa;
			idcpl = obj.idcpl;
			fechapci = obj.fechapci;
			rtfpci = obj.rtfpci;
			textopci = obj.textopci;
			apiestado = obj.apiestado;
			apitransaccion = obj.apitransaccion;
			usucre = obj.usucre;
			feccre = obj.feccre;
			usumod = obj.usumod;
			fecmod = obj.fecmod;
			htmlpci = obj.htmlpci;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntPacCirugias.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna idpci de la Tabla paccirugias
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "idpci", Description = " Propiedad publica de tipo Int64 que representa a la columna idpci de la Tabla paccirugias")]
		[Required(ErrorMessage = "Se necesita un valor para -idpci- porque es un campo requerido.")]
		[Key]
		public Int64 idpci { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla paccirugias
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "idppa", Description = " Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla paccirugias")]
		public Int64? idppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna idcpl de la Tabla paccirugias
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "idcpl", Description = " Propiedad publica de tipo int que representa a la columna idcpl de la Tabla paccirugias")]
		public int? idcpl { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fechapci de la Tabla paccirugias
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fechapci", Description = " Propiedad publica de tipo DateTime que representa a la columna fechapci de la Tabla paccirugias")]
		public DateTime? fechapci { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Byte[] que representa a la columna rtfpci de la Tabla paccirugias
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "rtfpci", Description = " Propiedad publica de tipo Byte[] que representa a la columna rtfpci de la Tabla paccirugias")]
		public Byte[] rtfpci { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna textopci de la Tabla paccirugias
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(50000, MinimumLength=0)]
		[Display(Name = "textopci", Description = " Propiedad publica de tipo string que representa a la columna textopci de la Tabla paccirugias")]
		public string textopci { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestado de la Tabla paccirugias
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo string que representa a la columna apiestado de la Tabla paccirugias")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestado { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla paccirugias
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apitransaccion", Description = " Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla paccirugias")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucre de la Tabla paccirugias
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo string que representa a la columna usucre de la Tabla paccirugias")]
		public string usucre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla paccirugias
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla paccirugias")]
		public DateTime feccre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumod de la Tabla paccirugias
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo string que representa a la columna usumod de la Tabla paccirugias")]
		public string usumod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla paccirugias
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla paccirugias")]
		public DateTime? fecmod { get; set; } 

		/// <summary>
		/// Contiene el documento en formato HTML
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100000, MinimumLength=0)]
		[Display(Name = "htmlpci", Description = "Contiene el documento en formato HTML")]
		public string htmlpci { get; set; } 

	}
}

