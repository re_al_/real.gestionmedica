#region 
/***********************************************************************************************************
	NOMBRE:       EntPacHistoriaclinica2
	DESCRIPCION:
		Clase que define un objeto para la Tabla pachistoriaclinica2

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntPacHistoriaclinica2 : CBaseClass
	{
		public const string StrNombreTabla = "PacHistoriaclinica2";
		public const string StrAliasTabla = "Ph2";
		public enum Fields
		{
			idppa
			,h4olfnormalphc
			,h4olfanosmiaphc
			,h4optscodphc
			,h4optscoiphc
			,h4optccodphc
			,h4optccoiphc
			,h4fondoodphc
			,h4fondooiphc
			,h4campimetriaphc
			,h4prosisphc
			,h4posicionojosphc
			,h4exoftalmiaphc
			,h4hornerphc
			,h4movocularesphc
			,h4nistagmuxphc
			,h4diplopia1phc
			,h4diplopia2phc
			,h4diplopia3phc
			,h4diplopia4phc
			,h4diplopia5phc
			,h4diplopia6phc
			,h4diplopia7phc
			,h4diplopia8phc
			,h4diplopia9phc
			,h4convergenciaphc
			,h4acomodacionodphc
			,h4acomodacionoiphc
			,h4pupulasodphc
			,h4pupulasoiphc
			,h4formaphc
			,h4fotomotorodphc
			,h4fotomotoroiphc
			,h4consensualdaiphc
			,h4consensualiadphc
			,h4senfacialderphc
			,h4senfacializqphc
			,h4reflejoderphc
			,h4reflejoizqphc
			,h4aberturabocaphc
			,h4movmasticatoriosphc
			,h4refmentonianophc
			,h4facsimetriaphc
			,h4facmovimientosphc
			,h4facparalisisphc
			,h4faccentralphc
			,h4facperifericaphc
			,h4facgustophc
			,h5otoscopiaphc
			,h5aguaudiderphc
			,h5aguaudiizqphc
			,h5weberphc
			,h5rinnephc
			,h5pruebaslabphc
			,h5elevpaladarphc
			,h5uvulaphc
			,h5refnauceosophc
			,h5deglucionphc
			,h5tonovozphc
			,h5esternocleidomastoideophc
			,h5trapeciophc
			,h5desviacionphc
			,h5atrofiaphc
			,h5fasciculacionphc
			,h5fuerzaphc
			,h5marchaphc
			,h5tonophc
			,h5volumenphc
			,h5fasciculacionesphc
			,h5fuemuscularphc
			,h5movinvoluntariosphc
			,h5equilibratoriaphc
			,h5rombergphc
			,h5dednarderphc
			,h5dednarizqphc
			,h5deddedderphc
			,h5deddedizqphc
			,h5talrodderphc
			,h5talrodizqphc
			,h5movrapidphc
			,h5rebotephc
			,h5habilidadespecifphc
			,apiestado
			,apitransaccion
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public EntPacHistoriaclinica2()
		{
			//Inicializacion de Variables
			h4olfnormalphc = null;
			h4olfanosmiaphc = null;
			h4optscodphc = null;
			h4optscoiphc = null;
			h4optccodphc = null;
			h4optccoiphc = null;
			h4fondoodphc = null;
			h4fondooiphc = null;
			h4campimetriaphc = null;
			h4prosisphc = null;
			h4posicionojosphc = null;
			h4exoftalmiaphc = null;
			h4hornerphc = null;
			h4movocularesphc = null;
			h4nistagmuxphc = null;
			h4diplopia1phc = null;
			h4diplopia2phc = null;
			h4diplopia3phc = null;
			h4diplopia4phc = null;
			h4diplopia5phc = null;
			h4diplopia6phc = null;
			h4diplopia7phc = null;
			h4diplopia8phc = null;
			h4diplopia9phc = null;
			h4convergenciaphc = null;
			h4acomodacionodphc = null;
			h4acomodacionoiphc = null;
			h4pupulasodphc = null;
			h4pupulasoiphc = null;
			h4formaphc = null;
			h4fotomotorodphc = null;
			h4fotomotoroiphc = null;
			h4consensualdaiphc = null;
			h4consensualiadphc = null;
			h4senfacialderphc = null;
			h4senfacializqphc = null;
			h4reflejoderphc = null;
			h4reflejoizqphc = null;
			h4aberturabocaphc = null;
			h4movmasticatoriosphc = null;
			h4refmentonianophc = null;
			h4facsimetriaphc = null;
			h4facmovimientosphc = null;
			h4facparalisisphc = null;
			h4faccentralphc = null;
			h4facperifericaphc = null;
			h4facgustophc = null;
			h5otoscopiaphc = null;
			h5aguaudiderphc = null;
			h5aguaudiizqphc = null;
			h5weberphc = null;
			h5rinnephc = null;
			h5pruebaslabphc = null;
			h5elevpaladarphc = null;
			h5uvulaphc = null;
			h5refnauceosophc = null;
			h5deglucionphc = null;
			h5tonovozphc = null;
			h5esternocleidomastoideophc = null;
			h5trapeciophc = null;
			h5desviacionphc = null;
			h5atrofiaphc = null;
			h5fasciculacionphc = null;
			h5fuerzaphc = null;
			h5marchaphc = null;
			h5tonophc = null;
			h5volumenphc = null;
			h5fasciculacionesphc = null;
			h5fuemuscularphc = null;
			h5movinvoluntariosphc = null;
			h5equilibratoriaphc = null;
			h5rombergphc = null;
			h5dednarderphc = null;
			h5dednarizqphc = null;
			h5deddedderphc = null;
			h5deddedizqphc = null;
			h5talrodderphc = null;
			h5talrodizqphc = null;
			h5movrapidphc = null;
			h5rebotephc = null;
			h5habilidadespecifphc = null;
			apiestado = null;
			apitransaccion = null;
			usucre = null;
			usumod = null;
			fecmod = null;
		}
		
		public EntPacHistoriaclinica2(EntPacHistoriaclinica2 obj)
		{
			idppa = obj.idppa;
			h4olfnormalphc = obj.h4olfnormalphc;
			h4olfanosmiaphc = obj.h4olfanosmiaphc;
			h4optscodphc = obj.h4optscodphc;
			h4optscoiphc = obj.h4optscoiphc;
			h4optccodphc = obj.h4optccodphc;
			h4optccoiphc = obj.h4optccoiphc;
			h4fondoodphc = obj.h4fondoodphc;
			h4fondooiphc = obj.h4fondooiphc;
			h4campimetriaphc = obj.h4campimetriaphc;
			h4prosisphc = obj.h4prosisphc;
			h4posicionojosphc = obj.h4posicionojosphc;
			h4exoftalmiaphc = obj.h4exoftalmiaphc;
			h4hornerphc = obj.h4hornerphc;
			h4movocularesphc = obj.h4movocularesphc;
			h4nistagmuxphc = obj.h4nistagmuxphc;
			h4diplopia1phc = obj.h4diplopia1phc;
			h4diplopia2phc = obj.h4diplopia2phc;
			h4diplopia3phc = obj.h4diplopia3phc;
			h4diplopia4phc = obj.h4diplopia4phc;
			h4diplopia5phc = obj.h4diplopia5phc;
			h4diplopia6phc = obj.h4diplopia6phc;
			h4diplopia7phc = obj.h4diplopia7phc;
			h4diplopia8phc = obj.h4diplopia8phc;
			h4diplopia9phc = obj.h4diplopia9phc;
			h4convergenciaphc = obj.h4convergenciaphc;
			h4acomodacionodphc = obj.h4acomodacionodphc;
			h4acomodacionoiphc = obj.h4acomodacionoiphc;
			h4pupulasodphc = obj.h4pupulasodphc;
			h4pupulasoiphc = obj.h4pupulasoiphc;
			h4formaphc = obj.h4formaphc;
			h4fotomotorodphc = obj.h4fotomotorodphc;
			h4fotomotoroiphc = obj.h4fotomotoroiphc;
			h4consensualdaiphc = obj.h4consensualdaiphc;
			h4consensualiadphc = obj.h4consensualiadphc;
			h4senfacialderphc = obj.h4senfacialderphc;
			h4senfacializqphc = obj.h4senfacializqphc;
			h4reflejoderphc = obj.h4reflejoderphc;
			h4reflejoizqphc = obj.h4reflejoizqphc;
			h4aberturabocaphc = obj.h4aberturabocaphc;
			h4movmasticatoriosphc = obj.h4movmasticatoriosphc;
			h4refmentonianophc = obj.h4refmentonianophc;
			h4facsimetriaphc = obj.h4facsimetriaphc;
			h4facmovimientosphc = obj.h4facmovimientosphc;
			h4facparalisisphc = obj.h4facparalisisphc;
			h4faccentralphc = obj.h4faccentralphc;
			h4facperifericaphc = obj.h4facperifericaphc;
			h4facgustophc = obj.h4facgustophc;
			h5otoscopiaphc = obj.h5otoscopiaphc;
			h5aguaudiderphc = obj.h5aguaudiderphc;
			h5aguaudiizqphc = obj.h5aguaudiizqphc;
			h5weberphc = obj.h5weberphc;
			h5rinnephc = obj.h5rinnephc;
			h5pruebaslabphc = obj.h5pruebaslabphc;
			h5elevpaladarphc = obj.h5elevpaladarphc;
			h5uvulaphc = obj.h5uvulaphc;
			h5refnauceosophc = obj.h5refnauceosophc;
			h5deglucionphc = obj.h5deglucionphc;
			h5tonovozphc = obj.h5tonovozphc;
			h5esternocleidomastoideophc = obj.h5esternocleidomastoideophc;
			h5trapeciophc = obj.h5trapeciophc;
			h5desviacionphc = obj.h5desviacionphc;
			h5atrofiaphc = obj.h5atrofiaphc;
			h5fasciculacionphc = obj.h5fasciculacionphc;
			h5fuerzaphc = obj.h5fuerzaphc;
			h5marchaphc = obj.h5marchaphc;
			h5tonophc = obj.h5tonophc;
			h5volumenphc = obj.h5volumenphc;
			h5fasciculacionesphc = obj.h5fasciculacionesphc;
			h5fuemuscularphc = obj.h5fuemuscularphc;
			h5movinvoluntariosphc = obj.h5movinvoluntariosphc;
			h5equilibratoriaphc = obj.h5equilibratoriaphc;
			h5rombergphc = obj.h5rombergphc;
			h5dednarderphc = obj.h5dednarderphc;
			h5dednarizqphc = obj.h5dednarizqphc;
			h5deddedderphc = obj.h5deddedderphc;
			h5deddedizqphc = obj.h5deddedizqphc;
			h5talrodderphc = obj.h5talrodderphc;
			h5talrodizqphc = obj.h5talrodizqphc;
			h5movrapidphc = obj.h5movrapidphc;
			h5rebotephc = obj.h5rebotephc;
			h5habilidadespecifphc = obj.h5habilidadespecifphc;
			apiestado = obj.apiestado;
			apitransaccion = obj.apitransaccion;
			usucre = obj.usucre;
			feccre = obj.feccre;
			usumod = obj.usumod;
			fecmod = obj.fecmod;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntPacHistoriaclinica2.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla pachistoriaclinica2
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "idppa", Description = " Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla pachistoriaclinica2")]
		[Required(ErrorMessage = "Se necesita un valor para -idppa- porque es un campo requerido.")]
		[Key]
		public Int64 idppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4olfnormalphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4olfnormalphc", Description = " Propiedad publica de tipo string que representa a la columna h4olfnormalphc de la Tabla pachistoriaclinica2")]
		public string h4olfnormalphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4olfanosmiaphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4olfanosmiaphc", Description = " Propiedad publica de tipo string que representa a la columna h4olfanosmiaphc de la Tabla pachistoriaclinica2")]
		public string h4olfanosmiaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4optscodphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4optscodphc", Description = " Propiedad publica de tipo string que representa a la columna h4optscodphc de la Tabla pachistoriaclinica2")]
		public string h4optscodphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4optscoiphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4optscoiphc", Description = " Propiedad publica de tipo string que representa a la columna h4optscoiphc de la Tabla pachistoriaclinica2")]
		public string h4optscoiphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4optccodphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4optccodphc", Description = " Propiedad publica de tipo string que representa a la columna h4optccodphc de la Tabla pachistoriaclinica2")]
		public string h4optccodphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4optccoiphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4optccoiphc", Description = " Propiedad publica de tipo string que representa a la columna h4optccoiphc de la Tabla pachistoriaclinica2")]
		public string h4optccoiphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4fondoodphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4fondoodphc", Description = " Propiedad publica de tipo string que representa a la columna h4fondoodphc de la Tabla pachistoriaclinica2")]
		public string h4fondoodphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4fondooiphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4fondooiphc", Description = " Propiedad publica de tipo string que representa a la columna h4fondooiphc de la Tabla pachistoriaclinica2")]
		public string h4fondooiphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4campimetriaphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h4campimetriaphc", Description = " Propiedad publica de tipo string que representa a la columna h4campimetriaphc de la Tabla pachistoriaclinica2")]
		public string h4campimetriaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4prosisphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4prosisphc", Description = " Propiedad publica de tipo string que representa a la columna h4prosisphc de la Tabla pachistoriaclinica2")]
		public string h4prosisphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4posicionojosphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4posicionojosphc", Description = " Propiedad publica de tipo string que representa a la columna h4posicionojosphc de la Tabla pachistoriaclinica2")]
		public string h4posicionojosphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4exoftalmiaphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4exoftalmiaphc", Description = " Propiedad publica de tipo string que representa a la columna h4exoftalmiaphc de la Tabla pachistoriaclinica2")]
		public string h4exoftalmiaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4hornerphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4hornerphc", Description = " Propiedad publica de tipo string que representa a la columna h4hornerphc de la Tabla pachistoriaclinica2")]
		public string h4hornerphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4movocularesphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4movocularesphc", Description = " Propiedad publica de tipo string que representa a la columna h4movocularesphc de la Tabla pachistoriaclinica2")]
		public string h4movocularesphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4nistagmuxphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4nistagmuxphc", Description = " Propiedad publica de tipo string que representa a la columna h4nistagmuxphc de la Tabla pachistoriaclinica2")]
		public string h4nistagmuxphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia1phc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia1phc", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia1phc de la Tabla pachistoriaclinica2")]
		public string h4diplopia1phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia2phc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia2phc", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia2phc de la Tabla pachistoriaclinica2")]
		public string h4diplopia2phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia3phc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia3phc", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia3phc de la Tabla pachistoriaclinica2")]
		public string h4diplopia3phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia4phc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia4phc", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia4phc de la Tabla pachistoriaclinica2")]
		public string h4diplopia4phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia5phc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia5phc", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia5phc de la Tabla pachistoriaclinica2")]
		public string h4diplopia5phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia6phc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia6phc", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia6phc de la Tabla pachistoriaclinica2")]
		public string h4diplopia6phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia7phc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia7phc", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia7phc de la Tabla pachistoriaclinica2")]
		public string h4diplopia7phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia8phc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia8phc", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia8phc de la Tabla pachistoriaclinica2")]
		public string h4diplopia8phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4diplopia9phc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h4diplopia9phc", Description = " Propiedad publica de tipo string que representa a la columna h4diplopia9phc de la Tabla pachistoriaclinica2")]
		public string h4diplopia9phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4convergenciaphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4convergenciaphc", Description = " Propiedad publica de tipo string que representa a la columna h4convergenciaphc de la Tabla pachistoriaclinica2")]
		public string h4convergenciaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4acomodacionodphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4acomodacionodphc", Description = " Propiedad publica de tipo string que representa a la columna h4acomodacionodphc de la Tabla pachistoriaclinica2")]
		public string h4acomodacionodphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4acomodacionoiphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4acomodacionoiphc", Description = " Propiedad publica de tipo string que representa a la columna h4acomodacionoiphc de la Tabla pachistoriaclinica2")]
		public string h4acomodacionoiphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4pupulasodphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4pupulasodphc", Description = " Propiedad publica de tipo string que representa a la columna h4pupulasodphc de la Tabla pachistoriaclinica2")]
		public string h4pupulasodphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4pupulasoiphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4pupulasoiphc", Description = " Propiedad publica de tipo string que representa a la columna h4pupulasoiphc de la Tabla pachistoriaclinica2")]
		public string h4pupulasoiphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4formaphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4formaphc", Description = " Propiedad publica de tipo string que representa a la columna h4formaphc de la Tabla pachistoriaclinica2")]
		public string h4formaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4fotomotorodphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4fotomotorodphc", Description = " Propiedad publica de tipo string que representa a la columna h4fotomotorodphc de la Tabla pachistoriaclinica2")]
		public string h4fotomotorodphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4fotomotoroiphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4fotomotoroiphc", Description = " Propiedad publica de tipo string que representa a la columna h4fotomotoroiphc de la Tabla pachistoriaclinica2")]
		public string h4fotomotoroiphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4consensualdaiphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4consensualdaiphc", Description = " Propiedad publica de tipo string que representa a la columna h4consensualdaiphc de la Tabla pachistoriaclinica2")]
		public string h4consensualdaiphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4consensualiadphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4consensualiadphc", Description = " Propiedad publica de tipo string que representa a la columna h4consensualiadphc de la Tabla pachistoriaclinica2")]
		public string h4consensualiadphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4senfacialderphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4senfacialderphc", Description = " Propiedad publica de tipo string que representa a la columna h4senfacialderphc de la Tabla pachistoriaclinica2")]
		public string h4senfacialderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4senfacializqphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4senfacializqphc", Description = " Propiedad publica de tipo string que representa a la columna h4senfacializqphc de la Tabla pachistoriaclinica2")]
		public string h4senfacializqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4reflejoderphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4reflejoderphc", Description = " Propiedad publica de tipo string que representa a la columna h4reflejoderphc de la Tabla pachistoriaclinica2")]
		public string h4reflejoderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4reflejoizqphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4reflejoizqphc", Description = " Propiedad publica de tipo string que representa a la columna h4reflejoizqphc de la Tabla pachistoriaclinica2")]
		public string h4reflejoizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4aberturabocaphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h4aberturabocaphc", Description = " Propiedad publica de tipo string que representa a la columna h4aberturabocaphc de la Tabla pachistoriaclinica2")]
		public string h4aberturabocaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4movmasticatoriosphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h4movmasticatoriosphc", Description = " Propiedad publica de tipo string que representa a la columna h4movmasticatoriosphc de la Tabla pachistoriaclinica2")]
		public string h4movmasticatoriosphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4refmentonianophc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h4refmentonianophc", Description = " Propiedad publica de tipo string que representa a la columna h4refmentonianophc de la Tabla pachistoriaclinica2")]
		public string h4refmentonianophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4facsimetriaphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4facsimetriaphc", Description = " Propiedad publica de tipo string que representa a la columna h4facsimetriaphc de la Tabla pachistoriaclinica2")]
		public string h4facsimetriaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4facmovimientosphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h4facmovimientosphc", Description = " Propiedad publica de tipo string que representa a la columna h4facmovimientosphc de la Tabla pachistoriaclinica2")]
		public string h4facmovimientosphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4facparalisisphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4facparalisisphc", Description = " Propiedad publica de tipo string que representa a la columna h4facparalisisphc de la Tabla pachistoriaclinica2")]
		public string h4facparalisisphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4faccentralphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4faccentralphc", Description = " Propiedad publica de tipo string que representa a la columna h4faccentralphc de la Tabla pachistoriaclinica2")]
		public string h4faccentralphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4facperifericaphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h4facperifericaphc", Description = " Propiedad publica de tipo string que representa a la columna h4facperifericaphc de la Tabla pachistoriaclinica2")]
		public string h4facperifericaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h4facgustophc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h4facgustophc", Description = " Propiedad publica de tipo string que representa a la columna h4facgustophc de la Tabla pachistoriaclinica2")]
		public string h4facgustophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5otoscopiaphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h5otoscopiaphc", Description = " Propiedad publica de tipo string que representa a la columna h5otoscopiaphc de la Tabla pachistoriaclinica2")]
		public string h5otoscopiaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5aguaudiderphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5aguaudiderphc", Description = " Propiedad publica de tipo string que representa a la columna h5aguaudiderphc de la Tabla pachistoriaclinica2")]
		public string h5aguaudiderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5aguaudiizqphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5aguaudiizqphc", Description = " Propiedad publica de tipo string que representa a la columna h5aguaudiizqphc de la Tabla pachistoriaclinica2")]
		public string h5aguaudiizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5weberphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5weberphc", Description = " Propiedad publica de tipo string que representa a la columna h5weberphc de la Tabla pachistoriaclinica2")]
		public string h5weberphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5rinnephc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5rinnephc", Description = " Propiedad publica de tipo string que representa a la columna h5rinnephc de la Tabla pachistoriaclinica2")]
		public string h5rinnephc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5pruebaslabphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5pruebaslabphc", Description = " Propiedad publica de tipo string que representa a la columna h5pruebaslabphc de la Tabla pachistoriaclinica2")]
		public string h5pruebaslabphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5elevpaladarphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5elevpaladarphc", Description = " Propiedad publica de tipo string que representa a la columna h5elevpaladarphc de la Tabla pachistoriaclinica2")]
		public string h5elevpaladarphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5uvulaphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5uvulaphc", Description = " Propiedad publica de tipo string que representa a la columna h5uvulaphc de la Tabla pachistoriaclinica2")]
		public string h5uvulaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5refnauceosophc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5refnauceosophc", Description = " Propiedad publica de tipo string que representa a la columna h5refnauceosophc de la Tabla pachistoriaclinica2")]
		public string h5refnauceosophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5deglucionphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5deglucionphc", Description = " Propiedad publica de tipo string que representa a la columna h5deglucionphc de la Tabla pachistoriaclinica2")]
		public string h5deglucionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5tonovozphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5tonovozphc", Description = " Propiedad publica de tipo string que representa a la columna h5tonovozphc de la Tabla pachistoriaclinica2")]
		public string h5tonovozphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5esternocleidomastoideophc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5esternocleidomastoideophc", Description = " Propiedad publica de tipo string que representa a la columna h5esternocleidomastoideophc de la Tabla pachistoriaclinica2")]
		public string h5esternocleidomastoideophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5trapeciophc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5trapeciophc", Description = " Propiedad publica de tipo string que representa a la columna h5trapeciophc de la Tabla pachistoriaclinica2")]
		public string h5trapeciophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5desviacionphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5desviacionphc", Description = " Propiedad publica de tipo string que representa a la columna h5desviacionphc de la Tabla pachistoriaclinica2")]
		public string h5desviacionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5atrofiaphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5atrofiaphc", Description = " Propiedad publica de tipo string que representa a la columna h5atrofiaphc de la Tabla pachistoriaclinica2")]
		public string h5atrofiaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5fasciculacionphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5fasciculacionphc", Description = " Propiedad publica de tipo string que representa a la columna h5fasciculacionphc de la Tabla pachistoriaclinica2")]
		public string h5fasciculacionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5fuerzaphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5fuerzaphc", Description = " Propiedad publica de tipo string que representa a la columna h5fuerzaphc de la Tabla pachistoriaclinica2")]
		public string h5fuerzaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5marchaphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h5marchaphc", Description = " Propiedad publica de tipo string que representa a la columna h5marchaphc de la Tabla pachistoriaclinica2")]
		public string h5marchaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5tonophc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5tonophc", Description = " Propiedad publica de tipo string que representa a la columna h5tonophc de la Tabla pachistoriaclinica2")]
		public string h5tonophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5volumenphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5volumenphc", Description = " Propiedad publica de tipo string que representa a la columna h5volumenphc de la Tabla pachistoriaclinica2")]
		public string h5volumenphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5fasciculacionesphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5fasciculacionesphc", Description = " Propiedad publica de tipo string que representa a la columna h5fasciculacionesphc de la Tabla pachistoriaclinica2")]
		public string h5fasciculacionesphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5fuemuscularphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h5fuemuscularphc", Description = " Propiedad publica de tipo string que representa a la columna h5fuemuscularphc de la Tabla pachistoriaclinica2")]
		public string h5fuemuscularphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5movinvoluntariosphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h5movinvoluntariosphc", Description = " Propiedad publica de tipo string que representa a la columna h5movinvoluntariosphc de la Tabla pachistoriaclinica2")]
		public string h5movinvoluntariosphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5equilibratoriaphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5equilibratoriaphc", Description = " Propiedad publica de tipo string que representa a la columna h5equilibratoriaphc de la Tabla pachistoriaclinica2")]
		public string h5equilibratoriaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5rombergphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5rombergphc", Description = " Propiedad publica de tipo string que representa a la columna h5rombergphc de la Tabla pachistoriaclinica2")]
		public string h5rombergphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5dednarderphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h5dednarderphc", Description = " Propiedad publica de tipo string que representa a la columna h5dednarderphc de la Tabla pachistoriaclinica2")]
		public string h5dednarderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5dednarizqphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h5dednarizqphc", Description = " Propiedad publica de tipo string que representa a la columna h5dednarizqphc de la Tabla pachistoriaclinica2")]
		public string h5dednarizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5deddedderphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5deddedderphc", Description = " Propiedad publica de tipo string que representa a la columna h5deddedderphc de la Tabla pachistoriaclinica2")]
		public string h5deddedderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5deddedizqphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5deddedizqphc", Description = " Propiedad publica de tipo string que representa a la columna h5deddedizqphc de la Tabla pachistoriaclinica2")]
		public string h5deddedizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5talrodderphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5talrodderphc", Description = " Propiedad publica de tipo string que representa a la columna h5talrodderphc de la Tabla pachistoriaclinica2")]
		public string h5talrodderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5talrodizqphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h5talrodizqphc", Description = " Propiedad publica de tipo string que representa a la columna h5talrodizqphc de la Tabla pachistoriaclinica2")]
		public string h5talrodizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5movrapidphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h5movrapidphc", Description = " Propiedad publica de tipo string que representa a la columna h5movrapidphc de la Tabla pachistoriaclinica2")]
		public string h5movrapidphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5rebotephc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h5rebotephc", Description = " Propiedad publica de tipo string que representa a la columna h5rebotephc de la Tabla pachistoriaclinica2")]
		public string h5rebotephc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h5habilidadespecifphc de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "h5habilidadespecifphc", Description = " Propiedad publica de tipo string que representa a la columna h5habilidadespecifphc de la Tabla pachistoriaclinica2")]
		public string h5habilidadespecifphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestado de la Tabla pachistoriaclinica2
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo string que representa a la columna apiestado de la Tabla pachistoriaclinica2")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestado { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla pachistoriaclinica2
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apitransaccion", Description = " Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla pachistoriaclinica2")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucre de la Tabla pachistoriaclinica2
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo string que representa a la columna usucre de la Tabla pachistoriaclinica2")]
		public string usucre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla pachistoriaclinica2
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla pachistoriaclinica2")]
		public DateTime feccre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumod de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo string que representa a la columna usumod de la Tabla pachistoriaclinica2")]
		public string usumod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla pachistoriaclinica2
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla pachistoriaclinica2")]
		public DateTime? fecmod { get; set; } 

	}
}

