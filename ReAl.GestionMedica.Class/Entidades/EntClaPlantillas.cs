#region 
/***********************************************************************************************************
	NOMBRE:       EntClaPlantillas
	DESCRIPCION:
		Clase que define un objeto para la Tabla claplantillas

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        16/07/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntClaPlantillas : CBaseClass
	{
		public const string StrNombreTabla = "ClaPlantillas";
		public const string StrAliasTabla = "Cpl";
		public enum Fields
		{
			idcpl
			,idctp
			,nombrecpl
			,descripcioncpl
			,altocpl
			,anchocpl
			,margensupcpl
			,margeninfcpl
			,margendercpl
			,margenizqcpl
			,textocpl
			,rtfcpl
			,apiestado
			,apitransaccion
			,usucre
			,feccre
			,usumod
			,fecmod
			,htmlcpl

		}
		
		#region Constructoress
		
		public EntClaPlantillas()
		{
			//Inicializacion de Variables
			idctp = null;
			nombrecpl = null;
			descripcioncpl = null;
			textocpl = null;
			rtfcpl = null;
			apiestado = null;
			apitransaccion = null;
			usucre = null;
			usumod = null;
			fecmod = null;
			htmlcpl = null;
		}
		
		public EntClaPlantillas(EntClaPlantillas obj)
		{
			idcpl = obj.idcpl;
			idctp = obj.idctp;
			nombrecpl = obj.nombrecpl;
			descripcioncpl = obj.descripcioncpl;
			altocpl = obj.altocpl;
			anchocpl = obj.anchocpl;
			margensupcpl = obj.margensupcpl;
			margeninfcpl = obj.margeninfcpl;
			margendercpl = obj.margendercpl;
			margenizqcpl = obj.margenizqcpl;
			textocpl = obj.textocpl;
			rtfcpl = obj.rtfcpl;
			apiestado = obj.apiestado;
			apitransaccion = obj.apitransaccion;
			usucre = obj.usucre;
			feccre = obj.feccre;
			usumod = obj.usumod;
			fecmod = obj.fecmod;
			htmlcpl = obj.htmlcpl;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntClaPlantillas.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna idcpl de la Tabla claplantillas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "idcpl", Description = " Propiedad publica de tipo int que representa a la columna idcpl de la Tabla claplantillas")]
		[Required(ErrorMessage = "Se necesita un valor para -idcpl- porque es un campo requerido.")]
		[Key]
		public int idcpl { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna idctp de la Tabla claplantillas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "idctp", Description = " Propiedad publica de tipo int que representa a la columna idctp de la Tabla claplantillas")]
		public int? idctp { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna nombrecpl de la Tabla claplantillas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "nombrecpl", Description = " Propiedad publica de tipo string que representa a la columna nombrecpl de la Tabla claplantillas")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -nombrecpl- porque es un campo requerido.")]
		public string nombrecpl { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna descripcioncpl de la Tabla claplantillas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "descripcioncpl", Description = " Propiedad publica de tipo string que representa a la columna descripcioncpl de la Tabla claplantillas")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -descripcioncpl- porque es un campo requerido.")]
		public string descripcioncpl { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Decimal que representa a la columna altocpl de la Tabla claplantillas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "altocpl", Description = " Propiedad publica de tipo Decimal que representa a la columna altocpl de la Tabla claplantillas")]
		[Required(ErrorMessage = "Se necesita un valor para -altocpl- porque es un campo requerido.")]
		public Decimal altocpl { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Decimal que representa a la columna anchocpl de la Tabla claplantillas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "anchocpl", Description = " Propiedad publica de tipo Decimal que representa a la columna anchocpl de la Tabla claplantillas")]
		[Required(ErrorMessage = "Se necesita un valor para -anchocpl- porque es un campo requerido.")]
		public Decimal anchocpl { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Decimal que representa a la columna margensupcpl de la Tabla claplantillas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "margensupcpl", Description = " Propiedad publica de tipo Decimal que representa a la columna margensupcpl de la Tabla claplantillas")]
		[Required(ErrorMessage = "Se necesita un valor para -margensupcpl- porque es un campo requerido.")]
		public Decimal margensupcpl { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Decimal que representa a la columna margeninfcpl de la Tabla claplantillas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "margeninfcpl", Description = " Propiedad publica de tipo Decimal que representa a la columna margeninfcpl de la Tabla claplantillas")]
		[Required(ErrorMessage = "Se necesita un valor para -margeninfcpl- porque es un campo requerido.")]
		public Decimal margeninfcpl { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Decimal que representa a la columna margendercpl de la Tabla claplantillas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "margendercpl", Description = " Propiedad publica de tipo Decimal que representa a la columna margendercpl de la Tabla claplantillas")]
		[Required(ErrorMessage = "Se necesita un valor para -margendercpl- porque es un campo requerido.")]
		public Decimal margendercpl { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Decimal que representa a la columna margenizqcpl de la Tabla claplantillas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "margenizqcpl", Description = " Propiedad publica de tipo Decimal que representa a la columna margenizqcpl de la Tabla claplantillas")]
		[Required(ErrorMessage = "Se necesita un valor para -margenizqcpl- porque es un campo requerido.")]
		public Decimal margenizqcpl { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna textocpl de la Tabla claplantillas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(50000, MinimumLength=0)]
		[Display(Name = "textocpl", Description = " Propiedad publica de tipo string que representa a la columna textocpl de la Tabla claplantillas")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -textocpl- porque es un campo requerido.")]
		public string textocpl { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Byte[] que representa a la columna rtfcpl de la Tabla claplantillas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "rtfcpl", Description = " Propiedad publica de tipo Byte[] que representa a la columna rtfcpl de la Tabla claplantillas")]
		public Byte[] rtfcpl { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestado de la Tabla claplantillas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo string que representa a la columna apiestado de la Tabla claplantillas")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestado { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla claplantillas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apitransaccion", Description = " Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla claplantillas")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucre de la Tabla claplantillas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo string que representa a la columna usucre de la Tabla claplantillas")]
		public string usucre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla claplantillas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla claplantillas")]
		public DateTime feccre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumod de la Tabla claplantillas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo string que representa a la columna usumod de la Tabla claplantillas")]
		public string usumod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla claplantillas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla claplantillas")]
		public DateTime? fecmod { get; set; } 

		/// <summary>
		/// Contiene el documento en formato HTML
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100000, MinimumLength=0)]
		[Display(Name = "htmlcpl", Description = "Contiene el documento en formato HTML")]
		public string htmlcpl { get; set; } 

	}
}

