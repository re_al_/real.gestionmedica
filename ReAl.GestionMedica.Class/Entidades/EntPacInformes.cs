#region 
/***********************************************************************************************************
	NOMBRE:       EntPacInformes
	DESCRIPCION:
		Clase que define un objeto para la Tabla pacinformes

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        16/07/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntPacInformes : CBaseClass
	{
		public const string StrNombreTabla = "PacInformes";
		public const string StrAliasTabla = "Pin";
		public enum Fields
		{
			idpin
			,idppa
			,idcpl
			,fechapin
			,rtfpin
			,textopin
			,apiestado
			,apitransaccion
			,usucre
			,feccre
			,usumod
			,fecmod
			,htmlpin

		}
		
		#region Constructoress
		
		public EntPacInformes()
		{
			//Inicializacion de Variables
			idppa = null;
			idcpl = null;
			fechapin = null;
			rtfpin = null;
			textopin = null;
			apiestado = null;
			apitransaccion = null;
			usucre = null;
			usumod = null;
			fecmod = null;
			htmlpin = null;
		}
		
		public EntPacInformes(EntPacInformes obj)
		{
			idpin = obj.idpin;
			idppa = obj.idppa;
			idcpl = obj.idcpl;
			fechapin = obj.fechapin;
			rtfpin = obj.rtfpin;
			textopin = obj.textopin;
			apiestado = obj.apiestado;
			apitransaccion = obj.apitransaccion;
			usucre = obj.usucre;
			feccre = obj.feccre;
			usumod = obj.usumod;
			fecmod = obj.fecmod;
			htmlpin = obj.htmlpin;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntPacInformes.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna idpin de la Tabla pacinformes
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "idpin", Description = " Propiedad publica de tipo Int64 que representa a la columna idpin de la Tabla pacinformes")]
		[Required(ErrorMessage = "Se necesita un valor para -idpin- porque es un campo requerido.")]
		[Key]
		public Int64 idpin { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla pacinformes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "idppa", Description = " Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla pacinformes")]
		public Int64? idppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna idcpl de la Tabla pacinformes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "idcpl", Description = " Propiedad publica de tipo int que representa a la columna idcpl de la Tabla pacinformes")]
		public int? idcpl { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fechapin de la Tabla pacinformes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fechapin", Description = " Propiedad publica de tipo DateTime que representa a la columna fechapin de la Tabla pacinformes")]
		public DateTime? fechapin { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Byte[] que representa a la columna rtfpin de la Tabla pacinformes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "rtfpin", Description = " Propiedad publica de tipo Byte[] que representa a la columna rtfpin de la Tabla pacinformes")]
		public Byte[] rtfpin { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna textopin de la Tabla pacinformes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(50000, MinimumLength=0)]
		[Display(Name = "textopin", Description = " Propiedad publica de tipo string que representa a la columna textopin de la Tabla pacinformes")]
		public string textopin { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestado de la Tabla pacinformes
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo string que representa a la columna apiestado de la Tabla pacinformes")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestado { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla pacinformes
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apitransaccion", Description = " Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla pacinformes")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucre de la Tabla pacinformes
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo string que representa a la columna usucre de la Tabla pacinformes")]
		public string usucre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla pacinformes
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla pacinformes")]
		public DateTime feccre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumod de la Tabla pacinformes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo string que representa a la columna usumod de la Tabla pacinformes")]
		public string usumod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla pacinformes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla pacinformes")]
		public DateTime? fecmod { get; set; } 

		/// <summary>
		/// Contiene el documento en formato HTML
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100000, MinimumLength=0)]
		[Display(Name = "htmlpin", Description = "Contiene el documento en formato HTML")]
		public string htmlpin { get; set; } 

	}
}

