#region
/***********************************************************************************************************
	NOMBRE:       EntPacCertificados
	DESCRIPCION:
		Clase que define un objeto para la Tabla paccertificados

	REVISIONES:
		Ver        FECHA       Autor            Descripcion
		---------  ----------  ---------------  ------------------------------------
		1.0        16/05/2021  R Alonzo Vera A  Creacion

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;

#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
    public class EntPacCertificados : CBaseClass
    {
        public const string StrNombreTabla = "PacCertificados";
        public const string StrAliasTabla = "paccertificados";

        public enum Fields
        {
            idpcm
            , idppa
            , fechapcm
            , matriculapcm
            , textopcm
            , rtfpcm
            , htmlpcm
            , apiestado
            , apitransaccion
            , usucre
            , feccre
            , usumod
            , fecmod
        }

        #region Constructoress

        public EntPacCertificados()
        {
            //Inicializacion de Variables
            idppa = null;
            fechapcm = null;
            matriculapcm = null;
            textopcm = null;
            rtfpcm = null;
            htmlpcm = null;
            apiestado = null;
            apitransaccion = null;
            usucre = null;
            usumod = null;
            fecmod = null;
        }

        public EntPacCertificados(EntPacCertificados obj)
        {
            idpcm = obj.idpcm;
            idppa = obj.idppa;
            fechapcm = obj.fechapcm;
            matriculapcm = obj.matriculapcm;
            textopcm = obj.textopcm;
            rtfpcm = obj.rtfpcm;
            htmlpcm = obj.htmlpcm;
            apiestado = obj.apiestado;
            apitransaccion = obj.apitransaccion;
            usucre = obj.usucre;
            feccre = obj.feccre;
            usumod = obj.usumod;
            fecmod = obj.fecmod;
        }

        #endregion

        #region Metodos Estaticos

        /// <summary>
        /// Funcion para obtener un objeto para el API y sus datos
        /// </summary>
        /// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
        public CApiObject CreateApiObject()
        {
            var objApi = new CApiObject();
            objApi.Nombre = EntPacCertificados.StrNombreTabla;
            objApi.Datos = this;
            return objApi;
        }

        #endregion

        /// <summary>
        /// Propiedad publica de tipo Int64 que representa a la columna idpcm de la Tabla paccertificados
        /// Permite Null: No
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: Yes
        /// Es ForeignKey: No
        /// </summary>
        [Display(Name = "idpcm", Description = " Propiedad publica de tipo Int64 que representa a la columna idpcm de la Tabla paccertificados")]
        [Required(ErrorMessage = "Se necesita un valor para -idpcm- porque es un campo requerido.")]
        [Key]
        public Int64 idpcm { get; set; }

        /// <summary>
        /// Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla paccertificados
        /// Permite Null: Yes
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: Yes
        /// </summary>
        [Display(Name = "idppa", Description = " Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla paccertificados")]
        public Int64? idppa { get; set; }

        /// <summary>
        /// Propiedad publica de tipo DateTime que representa a la columna fechapcm de la Tabla paccertificados
        /// Permite Null: Yes
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
        [DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
        [Display(Name = "fechapcm", Description = " Propiedad publica de tipo DateTime que representa a la columna fechapcm de la Tabla paccertificados")]
        public DateTime? fechapcm { get; set; }

        /// <summary>
        /// Propiedad publica de tipo string que representa a la columna matriculapcm de la Tabla paccertificados
        /// Permite Null: Yes
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [StringLength(150, MinimumLength = 0)]
        [Display(Name = "matriculapcm", Description = " Propiedad publica de tipo string que representa a la columna matriculapcm de la Tabla paccertificados")]
        public string matriculapcm { get; set; }

        /// <summary>
        /// Propiedad publica de tipo string que representa a la columna textopcm de la Tabla paccertificados
        /// Permite Null: Yes
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [StringLength(16000, MinimumLength = 0)]
        [Display(Name = "textopcm", Description = " Propiedad publica de tipo string que representa a la columna textopcm de la Tabla paccertificados")]
        public string textopcm { get; set; }

        /// <summary>
        /// Propiedad publica de tipo Byte[] que representa a la columna rtfpcm de la Tabla paccertificados
        /// Permite Null: Yes
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [Display(Name = "rtfpcm", Description = " Propiedad publica de tipo Byte[] que representa a la columna rtfpcm de la Tabla paccertificados")]
        public Byte[] rtfpcm { get; set; }

        /// <summary>
        /// Propiedad publica de tipo string que representa a la columna htmlpcm de la Tabla paccertificados
        /// Permite Null: Yes
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [StringLength(32000, MinimumLength = 0)]
        [Display(Name = "htmlpcm", Description = " Propiedad publica de tipo string que representa a la columna htmlpcm de la Tabla paccertificados")]
        public string htmlpcm { get; set; }

        /// <summary>
        /// Propiedad publica de tipo string que representa a la columna apiestado de la Tabla paccertificados
        /// Permite Null: No
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [StringLength(60, MinimumLength = 0)]
        [Display(Name = "apiestado", Description = " Propiedad publica de tipo string que representa a la columna apiestado de la Tabla paccertificados")]
        [EnumDataType(typeof(CApi.Estado))]
        public string apiestado { get; set; }

        /// <summary>
        /// Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla paccertificados
        /// Permite Null: No
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [StringLength(60, MinimumLength = 0)]
        [Display(Name = "apitransaccion", Description = " Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla paccertificados")]
        [EnumDataType(typeof(CApi.Transaccion))]
        public string apitransaccion { get; set; }

        /// <summary>
        /// Propiedad publica de tipo string que representa a la columna usucre de la Tabla paccertificados
        /// Permite Null: No
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [StringLength(60, MinimumLength = 0)]
        [Display(Name = "usucre", Description = " Propiedad publica de tipo string que representa a la columna usucre de la Tabla paccertificados")]
        public string usucre { get; set; }

        /// <summary>
        /// Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla paccertificados
        /// Permite Null: No
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
        [DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
        [Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla paccertificados")]
        public DateTime feccre { get; set; }

        /// <summary>
        /// Propiedad publica de tipo string que representa a la columna usumod de la Tabla paccertificados
        /// Permite Null: Yes
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [StringLength(60, MinimumLength = 0)]
        [Display(Name = "usumod", Description = " Propiedad publica de tipo string que representa a la columna usumod de la Tabla paccertificados")]
        public string usumod { get; set; }

        /// <summary>
        /// Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla paccertificados
        /// Permite Null: Yes
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
        [DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
        [Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla paccertificados")]
        public DateTime? fecmod { get; set; }
    }
}