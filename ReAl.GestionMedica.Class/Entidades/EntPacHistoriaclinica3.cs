#region 
/***********************************************************************************************************
	NOMBRE:       EntPacHistoriaclinica3
	DESCRIPCION:
		Clase que define un objeto para la Tabla pachistoriaclinica3

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntPacHistoriaclinica3 : CBaseClass
	{
		public const string StrNombreTabla = "PacHistoriaclinica3";
		public const string StrAliasTabla = "Ph3";
		public enum Fields
		{
			idppa
			,h6reflejos01phc
			,h6reflejos02phc
			,h6reflejos03phc
			,h6reflejos04phc
			,h6reflejos05phc
			,h6reflejos06phc
			,h6reflejos07phc
			,h6reflejos08phc
			,h6reflejos09phc
			,h6reflejos10phc
			,h6miotbicderphc
			,h6miotbicizqphc
			,h6miottriderphc
			,h6miottriizqphc
			,h6miotestderphc
			,h6miotestizqphc
			,h6miotpatderphc
			,h6miotpatizqphc
			,h6miotaquderphc
			,h6miotaquizqphc
			,h6patpladerphc
			,h6patplaizqphc
			,h6pathofderphc
			,h6pathofizqphc
			,h6pattroderphc
			,h6pattroizqphc
			,h6patprederphc
			,h6patpreizqphc
			,h6patpmenderphc
			,h6patpmenizqphc
			,h6supbicderphc
			,h6supbicizqphc
			,h6suptriderphc
			,h6suptriizqphc
			,h6supestderphc
			,h6supestizqphc
			,h6otrmaxilarphc
			,h6otrglabelarphc
			,h6sistsensitivophc
			,h6dolorprofphc
			,h6posicionphc
			,h6vibracionphc
			,h6dospuntosphc
			,h6extincionphc
			,h6estereognosiaphc
			,h6grafestesiaphc
			,h6localizacionphc
			,h6imagen
			,h7rigideznucaphc
			,h7sigkerningphc
			,h7fotofobiaphc
			,h7sigbrudzinskiphc
			,h7hiperestesiaphc
			,h7pulcarotideosphc
			,h7pultemporalesphc
			,h7cabezaphc
			,h7cuellophc
			,h7nerviosperifphc
			,h7colvertebralphc
			,h7resumenphc
			,h7diagnosticosphc
			,h7conductaphc
			,h7tratamientophc
			,apiestado
			,apitransaccion
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public EntPacHistoriaclinica3()
		{
			//Inicializacion de Variables
			h6reflejos01phc = null;
			h6reflejos02phc = null;
			h6reflejos03phc = null;
			h6reflejos04phc = null;
			h6reflejos05phc = null;
			h6reflejos06phc = null;
			h6reflejos07phc = null;
			h6reflejos08phc = null;
			h6reflejos09phc = null;
			h6reflejos10phc = null;
			h6miotbicderphc = null;
			h6miotbicizqphc = null;
			h6miottriderphc = null;
			h6miottriizqphc = null;
			h6miotestderphc = null;
			h6miotestizqphc = null;
			h6miotpatderphc = null;
			h6miotpatizqphc = null;
			h6miotaquderphc = null;
			h6miotaquizqphc = null;
			h6patpladerphc = null;
			h6patplaizqphc = null;
			h6pathofderphc = null;
			h6pathofizqphc = null;
			h6pattroderphc = null;
			h6pattroizqphc = null;
			h6patprederphc = null;
			h6patpreizqphc = null;
			h6patpmenderphc = null;
			h6patpmenizqphc = null;
			h6supbicderphc = null;
			h6supbicizqphc = null;
			h6suptriderphc = null;
			h6suptriizqphc = null;
			h6supestderphc = null;
			h6supestizqphc = null;
			h6otrmaxilarphc = null;
			h6otrglabelarphc = null;
			h6sistsensitivophc = null;
			h6dolorprofphc = null;
			h6posicionphc = null;
			h6vibracionphc = null;
			h6dospuntosphc = null;
			h6extincionphc = null;
			h6estereognosiaphc = null;
			h6grafestesiaphc = null;
			h6localizacionphc = null;
			h6imagen = null;
			h7rigideznucaphc = null;
			h7sigkerningphc = null;
			h7fotofobiaphc = null;
			h7sigbrudzinskiphc = null;
			h7hiperestesiaphc = null;
			h7pulcarotideosphc = null;
			h7pultemporalesphc = null;
			h7cabezaphc = null;
			h7cuellophc = null;
			h7nerviosperifphc = null;
			h7colvertebralphc = null;
			h7resumenphc = null;
			h7diagnosticosphc = null;
			h7conductaphc = null;
			h7tratamientophc = null;
			apiestado = null;
			apitransaccion = null;
			usucre = null;
			usumod = null;
			fecmod = null;
		}
		
		public EntPacHistoriaclinica3(EntPacHistoriaclinica3 obj)
		{
			idppa = obj.idppa;
			h6reflejos01phc = obj.h6reflejos01phc;
			h6reflejos02phc = obj.h6reflejos02phc;
			h6reflejos03phc = obj.h6reflejos03phc;
			h6reflejos04phc = obj.h6reflejos04phc;
			h6reflejos05phc = obj.h6reflejos05phc;
			h6reflejos06phc = obj.h6reflejos06phc;
			h6reflejos07phc = obj.h6reflejos07phc;
			h6reflejos08phc = obj.h6reflejos08phc;
			h6reflejos09phc = obj.h6reflejos09phc;
			h6reflejos10phc = obj.h6reflejos10phc;
			h6miotbicderphc = obj.h6miotbicderphc;
			h6miotbicizqphc = obj.h6miotbicizqphc;
			h6miottriderphc = obj.h6miottriderphc;
			h6miottriizqphc = obj.h6miottriizqphc;
			h6miotestderphc = obj.h6miotestderphc;
			h6miotestizqphc = obj.h6miotestizqphc;
			h6miotpatderphc = obj.h6miotpatderphc;
			h6miotpatizqphc = obj.h6miotpatizqphc;
			h6miotaquderphc = obj.h6miotaquderphc;
			h6miotaquizqphc = obj.h6miotaquizqphc;
			h6patpladerphc = obj.h6patpladerphc;
			h6patplaizqphc = obj.h6patplaizqphc;
			h6pathofderphc = obj.h6pathofderphc;
			h6pathofizqphc = obj.h6pathofizqphc;
			h6pattroderphc = obj.h6pattroderphc;
			h6pattroizqphc = obj.h6pattroizqphc;
			h6patprederphc = obj.h6patprederphc;
			h6patpreizqphc = obj.h6patpreizqphc;
			h6patpmenderphc = obj.h6patpmenderphc;
			h6patpmenizqphc = obj.h6patpmenizqphc;
			h6supbicderphc = obj.h6supbicderphc;
			h6supbicizqphc = obj.h6supbicizqphc;
			h6suptriderphc = obj.h6suptriderphc;
			h6suptriizqphc = obj.h6suptriizqphc;
			h6supestderphc = obj.h6supestderphc;
			h6supestizqphc = obj.h6supestizqphc;
			h6otrmaxilarphc = obj.h6otrmaxilarphc;
			h6otrglabelarphc = obj.h6otrglabelarphc;
			h6sistsensitivophc = obj.h6sistsensitivophc;
			h6dolorprofphc = obj.h6dolorprofphc;
			h6posicionphc = obj.h6posicionphc;
			h6vibracionphc = obj.h6vibracionphc;
			h6dospuntosphc = obj.h6dospuntosphc;
			h6extincionphc = obj.h6extincionphc;
			h6estereognosiaphc = obj.h6estereognosiaphc;
			h6grafestesiaphc = obj.h6grafestesiaphc;
			h6localizacionphc = obj.h6localizacionphc;
			h6imagen = obj.h6imagen;
			h7rigideznucaphc = obj.h7rigideznucaphc;
			h7sigkerningphc = obj.h7sigkerningphc;
			h7fotofobiaphc = obj.h7fotofobiaphc;
			h7sigbrudzinskiphc = obj.h7sigbrudzinskiphc;
			h7hiperestesiaphc = obj.h7hiperestesiaphc;
			h7pulcarotideosphc = obj.h7pulcarotideosphc;
			h7pultemporalesphc = obj.h7pultemporalesphc;
			h7cabezaphc = obj.h7cabezaphc;
			h7cuellophc = obj.h7cuellophc;
			h7nerviosperifphc = obj.h7nerviosperifphc;
			h7colvertebralphc = obj.h7colvertebralphc;
			h7resumenphc = obj.h7resumenphc;
			h7diagnosticosphc = obj.h7diagnosticosphc;
			h7conductaphc = obj.h7conductaphc;
			h7tratamientophc = obj.h7tratamientophc;
			apiestado = obj.apiestado;
			apitransaccion = obj.apitransaccion;
			usucre = obj.usucre;
			feccre = obj.feccre;
			usumod = obj.usumod;
			fecmod = obj.fecmod;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntPacHistoriaclinica3.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla pachistoriaclinica3
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "idppa", Description = " Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla pachistoriaclinica3")]
		[Required(ErrorMessage = "Se necesita un valor para -idppa- porque es un campo requerido.")]
		[Key]
		public Int64 idppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos01phc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos01phc", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos01phc de la Tabla pachistoriaclinica3")]
		public string h6reflejos01phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos02phc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos02phc", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos02phc de la Tabla pachistoriaclinica3")]
		public string h6reflejos02phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos03phc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos03phc", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos03phc de la Tabla pachistoriaclinica3")]
		public string h6reflejos03phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos04phc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos04phc", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos04phc de la Tabla pachistoriaclinica3")]
		public string h6reflejos04phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos05phc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos05phc", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos05phc de la Tabla pachistoriaclinica3")]
		public string h6reflejos05phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos06phc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos06phc", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos06phc de la Tabla pachistoriaclinica3")]
		public string h6reflejos06phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos07phc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos07phc", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos07phc de la Tabla pachistoriaclinica3")]
		public string h6reflejos07phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos08phc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos08phc", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos08phc de la Tabla pachistoriaclinica3")]
		public string h6reflejos08phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos09phc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos09phc", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos09phc de la Tabla pachistoriaclinica3")]
		public string h6reflejos09phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6reflejos10phc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6reflejos10phc", Description = " Propiedad publica de tipo string que representa a la columna h6reflejos10phc de la Tabla pachistoriaclinica3")]
		public string h6reflejos10phc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotbicderphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotbicderphc", Description = " Propiedad publica de tipo string que representa a la columna h6miotbicderphc de la Tabla pachistoriaclinica3")]
		public string h6miotbicderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotbicizqphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotbicizqphc", Description = " Propiedad publica de tipo string que representa a la columna h6miotbicizqphc de la Tabla pachistoriaclinica3")]
		public string h6miotbicizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miottriderphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miottriderphc", Description = " Propiedad publica de tipo string que representa a la columna h6miottriderphc de la Tabla pachistoriaclinica3")]
		public string h6miottriderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miottriizqphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miottriizqphc", Description = " Propiedad publica de tipo string que representa a la columna h6miottriizqphc de la Tabla pachistoriaclinica3")]
		public string h6miottriizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotestderphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotestderphc", Description = " Propiedad publica de tipo string que representa a la columna h6miotestderphc de la Tabla pachistoriaclinica3")]
		public string h6miotestderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotestizqphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotestizqphc", Description = " Propiedad publica de tipo string que representa a la columna h6miotestizqphc de la Tabla pachistoriaclinica3")]
		public string h6miotestizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotpatderphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotpatderphc", Description = " Propiedad publica de tipo string que representa a la columna h6miotpatderphc de la Tabla pachistoriaclinica3")]
		public string h6miotpatderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotpatizqphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotpatizqphc", Description = " Propiedad publica de tipo string que representa a la columna h6miotpatizqphc de la Tabla pachistoriaclinica3")]
		public string h6miotpatizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotaquderphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotaquderphc", Description = " Propiedad publica de tipo string que representa a la columna h6miotaquderphc de la Tabla pachistoriaclinica3")]
		public string h6miotaquderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6miotaquizqphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6miotaquizqphc", Description = " Propiedad publica de tipo string que representa a la columna h6miotaquizqphc de la Tabla pachistoriaclinica3")]
		public string h6miotaquizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6patpladerphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6patpladerphc", Description = " Propiedad publica de tipo string que representa a la columna h6patpladerphc de la Tabla pachistoriaclinica3")]
		public string h6patpladerphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6patplaizqphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6patplaizqphc", Description = " Propiedad publica de tipo string que representa a la columna h6patplaizqphc de la Tabla pachistoriaclinica3")]
		public string h6patplaizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6pathofderphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6pathofderphc", Description = " Propiedad publica de tipo string que representa a la columna h6pathofderphc de la Tabla pachistoriaclinica3")]
		public string h6pathofderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6pathofizqphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6pathofizqphc", Description = " Propiedad publica de tipo string que representa a la columna h6pathofizqphc de la Tabla pachistoriaclinica3")]
		public string h6pathofizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6pattroderphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6pattroderphc", Description = " Propiedad publica de tipo string que representa a la columna h6pattroderphc de la Tabla pachistoriaclinica3")]
		public string h6pattroderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6pattroizqphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6pattroizqphc", Description = " Propiedad publica de tipo string que representa a la columna h6pattroizqphc de la Tabla pachistoriaclinica3")]
		public string h6pattroizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6patprederphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6patprederphc", Description = " Propiedad publica de tipo string que representa a la columna h6patprederphc de la Tabla pachistoriaclinica3")]
		public string h6patprederphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6patpreizqphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6patpreizqphc", Description = " Propiedad publica de tipo string que representa a la columna h6patpreizqphc de la Tabla pachistoriaclinica3")]
		public string h6patpreizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6patpmenderphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6patpmenderphc", Description = " Propiedad publica de tipo string que representa a la columna h6patpmenderphc de la Tabla pachistoriaclinica3")]
		public string h6patpmenderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6patpmenizqphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6patpmenizqphc", Description = " Propiedad publica de tipo string que representa a la columna h6patpmenizqphc de la Tabla pachistoriaclinica3")]
		public string h6patpmenizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6supbicderphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6supbicderphc", Description = " Propiedad publica de tipo string que representa a la columna h6supbicderphc de la Tabla pachistoriaclinica3")]
		public string h6supbicderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6supbicizqphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6supbicizqphc", Description = " Propiedad publica de tipo string que representa a la columna h6supbicizqphc de la Tabla pachistoriaclinica3")]
		public string h6supbicizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6suptriderphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6suptriderphc", Description = " Propiedad publica de tipo string que representa a la columna h6suptriderphc de la Tabla pachistoriaclinica3")]
		public string h6suptriderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6suptriizqphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6suptriizqphc", Description = " Propiedad publica de tipo string que representa a la columna h6suptriizqphc de la Tabla pachistoriaclinica3")]
		public string h6suptriizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6supestderphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6supestderphc", Description = " Propiedad publica de tipo string que representa a la columna h6supestderphc de la Tabla pachistoriaclinica3")]
		public string h6supestderphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6supestizqphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(10, MinimumLength=0)]
		[Display(Name = "h6supestizqphc", Description = " Propiedad publica de tipo string que representa a la columna h6supestizqphc de la Tabla pachistoriaclinica3")]
		public string h6supestizqphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6otrmaxilarphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h6otrmaxilarphc", Description = " Propiedad publica de tipo string que representa a la columna h6otrmaxilarphc de la Tabla pachistoriaclinica3")]
		public string h6otrmaxilarphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6otrglabelarphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h6otrglabelarphc", Description = " Propiedad publica de tipo string que representa a la columna h6otrglabelarphc de la Tabla pachistoriaclinica3")]
		public string h6otrglabelarphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6sistsensitivophc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6sistsensitivophc", Description = " Propiedad publica de tipo string que representa a la columna h6sistsensitivophc de la Tabla pachistoriaclinica3")]
		public string h6sistsensitivophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6dolorprofphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6dolorprofphc", Description = " Propiedad publica de tipo string que representa a la columna h6dolorprofphc de la Tabla pachistoriaclinica3")]
		public string h6dolorprofphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6posicionphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6posicionphc", Description = " Propiedad publica de tipo string que representa a la columna h6posicionphc de la Tabla pachistoriaclinica3")]
		public string h6posicionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6vibracionphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6vibracionphc", Description = " Propiedad publica de tipo string que representa a la columna h6vibracionphc de la Tabla pachistoriaclinica3")]
		public string h6vibracionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6dospuntosphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6dospuntosphc", Description = " Propiedad publica de tipo string que representa a la columna h6dospuntosphc de la Tabla pachistoriaclinica3")]
		public string h6dospuntosphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6extincionphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6extincionphc", Description = " Propiedad publica de tipo string que representa a la columna h6extincionphc de la Tabla pachistoriaclinica3")]
		public string h6extincionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6estereognosiaphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6estereognosiaphc", Description = " Propiedad publica de tipo string que representa a la columna h6estereognosiaphc de la Tabla pachistoriaclinica3")]
		public string h6estereognosiaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6grafestesiaphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6grafestesiaphc", Description = " Propiedad publica de tipo string que representa a la columna h6grafestesiaphc de la Tabla pachistoriaclinica3")]
		public string h6grafestesiaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h6localizacionphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "h6localizacionphc", Description = " Propiedad publica de tipo string que representa a la columna h6localizacionphc de la Tabla pachistoriaclinica3")]
		public string h6localizacionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Byte[] que representa a la columna h6imagen de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "h6imagen", Description = " Propiedad publica de tipo Byte[] que representa a la columna h6imagen de la Tabla pachistoriaclinica3")]
		public Byte[] h6imagen { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7rigideznucaphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7rigideznucaphc", Description = " Propiedad publica de tipo string que representa a la columna h7rigideznucaphc de la Tabla pachistoriaclinica3")]
		public string h7rigideznucaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7sigkerningphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7sigkerningphc", Description = " Propiedad publica de tipo string que representa a la columna h7sigkerningphc de la Tabla pachistoriaclinica3")]
		public string h7sigkerningphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7fotofobiaphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7fotofobiaphc", Description = " Propiedad publica de tipo string que representa a la columna h7fotofobiaphc de la Tabla pachistoriaclinica3")]
		public string h7fotofobiaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7sigbrudzinskiphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7sigbrudzinskiphc", Description = " Propiedad publica de tipo string que representa a la columna h7sigbrudzinskiphc de la Tabla pachistoriaclinica3")]
		public string h7sigbrudzinskiphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7hiperestesiaphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7hiperestesiaphc", Description = " Propiedad publica de tipo string que representa a la columna h7hiperestesiaphc de la Tabla pachistoriaclinica3")]
		public string h7hiperestesiaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7pulcarotideosphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7pulcarotideosphc", Description = " Propiedad publica de tipo string que representa a la columna h7pulcarotideosphc de la Tabla pachistoriaclinica3")]
		public string h7pulcarotideosphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7pultemporalesphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7pultemporalesphc", Description = " Propiedad publica de tipo string que representa a la columna h7pultemporalesphc de la Tabla pachistoriaclinica3")]
		public string h7pultemporalesphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7cabezaphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7cabezaphc", Description = " Propiedad publica de tipo string que representa a la columna h7cabezaphc de la Tabla pachistoriaclinica3")]
		public string h7cabezaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7cuellophc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h7cuellophc", Description = " Propiedad publica de tipo string que representa a la columna h7cuellophc de la Tabla pachistoriaclinica3")]
		public string h7cuellophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7nerviosperifphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h7nerviosperifphc", Description = " Propiedad publica de tipo string que representa a la columna h7nerviosperifphc de la Tabla pachistoriaclinica3")]
		public string h7nerviosperifphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7colvertebralphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h7colvertebralphc", Description = " Propiedad publica de tipo string que representa a la columna h7colvertebralphc de la Tabla pachistoriaclinica3")]
		public string h7colvertebralphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7resumenphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h7resumenphc", Description = " Propiedad publica de tipo string que representa a la columna h7resumenphc de la Tabla pachistoriaclinica3")]
		public string h7resumenphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7diagnosticosphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h7diagnosticosphc", Description = " Propiedad publica de tipo string que representa a la columna h7diagnosticosphc de la Tabla pachistoriaclinica3")]
		public string h7diagnosticosphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7conductaphc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h7conductaphc", Description = " Propiedad publica de tipo string que representa a la columna h7conductaphc de la Tabla pachistoriaclinica3")]
		public string h7conductaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h7tratamientophc de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h7tratamientophc", Description = " Propiedad publica de tipo string que representa a la columna h7tratamientophc de la Tabla pachistoriaclinica3")]
		public string h7tratamientophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestado de la Tabla pachistoriaclinica3
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo string que representa a la columna apiestado de la Tabla pachistoriaclinica3")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestado { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla pachistoriaclinica3
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apitransaccion", Description = " Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla pachistoriaclinica3")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucre de la Tabla pachistoriaclinica3
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo string que representa a la columna usucre de la Tabla pachistoriaclinica3")]
		public string usucre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla pachistoriaclinica3
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla pachistoriaclinica3")]
		public DateTime feccre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumod de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo string que representa a la columna usumod de la Tabla pachistoriaclinica3")]
		public string usumod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla pachistoriaclinica3
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla pachistoriaclinica3")]
		public DateTime? fecmod { get; set; } 

	}
}

