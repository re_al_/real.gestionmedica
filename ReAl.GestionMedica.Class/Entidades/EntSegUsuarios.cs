#region 
/***********************************************************************************************************
	NOMBRE:       EntSegUsuarios
	DESCRIPCION:
		Clase que define un objeto para la Tabla segusuarios

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntSegUsuarios : CBaseClass
	{
		public const string StrNombreTabla = "SegUsuarios";
		public const string StrAliasTabla = "Sus";
		public enum Fields
		{
			idsus
			,loginsus
			,passsus
			,nombresus
			,apellidosus
			,vigentesus
			,fechavigentesus
			,fechapasssus
			,apiestadosus
			,apitransaccionsus
			,usucresus
			,feccresus
			,usumodsus
			,fecmodsus

		}
		
		#region Constructoress
		
		public EntSegUsuarios()
		{
			//Inicializacion de Variables
			loginsus = null;
			passsus = null;
			nombresus = null;
			apellidosus = null;
			fechapasssus = null;
			apiestadosus = null;
			apitransaccionsus = null;
			usucresus = null;
			usumodsus = null;
			fecmodsus = null;
		}
		
		public EntSegUsuarios(EntSegUsuarios obj)
		{
			idsus = obj.idsus;
			loginsus = obj.loginsus;
			passsus = obj.passsus;
			nombresus = obj.nombresus;
			apellidosus = obj.apellidosus;
			vigentesus = obj.vigentesus;
			fechavigentesus = obj.fechavigentesus;
			fechapasssus = obj.fechapasssus;
			apiestadosus = obj.apiestadosus;
			apitransaccionsus = obj.apitransaccionsus;
			usucresus = obj.usucresus;
			feccresus = obj.feccresus;
			usumodsus = obj.usumodsus;
			fecmodsus = obj.fecmodsus;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntSegUsuarios.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna idsus de la Tabla segusuarios
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, long.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "idsus", Description = " Propiedad publica de tipo int que representa a la columna idsus de la Tabla segusuarios")]
		[Required(ErrorMessage = "Se necesita un valor para -idsus- porque es un campo requerido.")]
		[Key]
		public long idsus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna loginsus de la Tabla segusuarios
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "loginsus", Description = " Propiedad publica de tipo string que representa a la columna loginsus de la Tabla segusuarios")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -loginsus- porque es un campo requerido.")]
		public string loginsus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna passsus de la Tabla segusuarios
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(256, MinimumLength=0)]
		[Display(Name = "passsus", Description = " Propiedad publica de tipo string que representa a la columna passsus de la Tabla segusuarios")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -passsus- porque es un campo requerido.")]
		public string passsus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna nombresus de la Tabla segusuarios
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(256, MinimumLength=0)]
		[Display(Name = "nombresus", Description = " Propiedad publica de tipo string que representa a la columna nombresus de la Tabla segusuarios")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -nombresus- porque es un campo requerido.")]
		public string nombresus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apellidosus de la Tabla segusuarios
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(256, MinimumLength=0)]
		[Display(Name = "apellidosus", Description = " Propiedad publica de tipo string que representa a la columna apellidosus de la Tabla segusuarios")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -apellidosus- porque es un campo requerido.")]
		public string apellidosus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna vigentesus de la Tabla segusuarios
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "vigentesus", Description = " Propiedad publica de tipo int que representa a la columna vigentesus de la Tabla segusuarios")]
		[Required(ErrorMessage = "Se necesita un valor para -vigentesus- porque es un campo requerido.")]
		public int vigentesus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fechavigentesus de la Tabla segusuarios
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fechavigentesus", Description = " Propiedad publica de tipo DateTime que representa a la columna fechavigentesus de la Tabla segusuarios")]
		[Required(ErrorMessage = "Se necesita un valor para -fechavigentesus- porque es un campo requerido.")]
		public DateTime fechavigentesus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fechapasssus de la Tabla segusuarios
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fechapasssus", Description = " Propiedad publica de tipo DateTime que representa a la columna fechapasssus de la Tabla segusuarios")]
		public DateTime? fechapasssus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestadosus de la Tabla segusuarios
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestadosus", Description = " Propiedad publica de tipo string que representa a la columna apiestadosus de la Tabla segusuarios")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestadosus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccionsus de la Tabla segusuarios
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apitransaccionsus", Description = " Propiedad publica de tipo string que representa a la columna apitransaccionsus de la Tabla segusuarios")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccionsus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucresus de la Tabla segusuarios
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucresus", Description = " Propiedad publica de tipo string que representa a la columna usucresus de la Tabla segusuarios")]
		public string usucresus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccresus de la Tabla segusuarios
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccresus", Description = " Propiedad publica de tipo DateTime que representa a la columna feccresus de la Tabla segusuarios")]
		public DateTime feccresus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumodsus de la Tabla segusuarios
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumodsus", Description = " Propiedad publica de tipo string que representa a la columna usumodsus de la Tabla segusuarios")]
		public string usumodsus { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmodsus de la Tabla segusuarios
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmodsus", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmodsus de la Tabla segusuarios")]
		public DateTime? fecmodsus { get; set; } 

	}
}

