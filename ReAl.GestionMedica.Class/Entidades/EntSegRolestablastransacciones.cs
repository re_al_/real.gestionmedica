#region 
/***********************************************************************************************************
	NOMBRE:       EntSegRolestablastransacciones
	DESCRIPCION:
		Clase que define un objeto para la Tabla segrolestablastransacciones

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntSegRolestablastransacciones : CBaseClass
	{
		public const string StrNombreTabla = "SegRolestablastransacciones";
		public const string StrAliasTabla = "Srt";
		public enum Fields
		{
			rolsro
			,tablasta
			,transaccionstr
			,apiestadosrt
			,apitransaccionsrt
			,usucresrt
			,feccresrt
			,usumodsrt
			,fecmodsrt

		}
		
		#region Constructoress
		
		public EntSegRolestablastransacciones()
		{
			//Inicializacion de Variables
			tablasta = null;
			transaccionstr = null;
			apiestadosrt = null;
			apitransaccionsrt = null;
			usucresrt = null;
			usumodsrt = null;
			fecmodsrt = null;
		}
		
		public EntSegRolestablastransacciones(EntSegRolestablastransacciones obj)
		{
			rolsro = obj.rolsro;
			tablasta = obj.tablasta;
			transaccionstr = obj.transaccionstr;
			apiestadosrt = obj.apiestadosrt;
			apitransaccionsrt = obj.apitransaccionsrt;
			usucresrt = obj.usucresrt;
			feccresrt = obj.feccresrt;
			usumodsrt = obj.usumodsrt;
			fecmodsrt = obj.fecmodsrt;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntSegRolestablastransacciones.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna rolsro de la Tabla segrolestablastransacciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: Yes
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "rolsro", Description = " Propiedad publica de tipo int que representa a la columna rolsro de la Tabla segrolestablastransacciones")]
		[Required(ErrorMessage = "Se necesita un valor para -rolsro- porque es un campo requerido.")]
		[Key]
		public int rolsro { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna tablasta de la Tabla segrolestablastransacciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: Yes
		/// </summary>
		[StringLength(30, MinimumLength=0)]
		[Display(Name = "tablasta", Description = " Propiedad publica de tipo string que representa a la columna tablasta de la Tabla segrolestablastransacciones")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -tablasta- porque es un campo requerido.")]
		[Key]
		public string tablasta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna transaccionstr de la Tabla segrolestablastransacciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "transaccionstr", Description = " Propiedad publica de tipo string que representa a la columna transaccionstr de la Tabla segrolestablastransacciones")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -transaccionstr- porque es un campo requerido.")]
		[Key]
		public string transaccionstr { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestadosrt de la Tabla segrolestablastransacciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestadosrt", Description = " Propiedad publica de tipo string que representa a la columna apiestadosrt de la Tabla segrolestablastransacciones")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestadosrt { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccionsrt de la Tabla segrolestablastransacciones
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apitransaccionsrt", Description = " Propiedad publica de tipo string que representa a la columna apitransaccionsrt de la Tabla segrolestablastransacciones")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccionsrt { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucresrt de la Tabla segrolestablastransacciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucresrt", Description = " Propiedad publica de tipo string que representa a la columna usucresrt de la Tabla segrolestablastransacciones")]
		public string usucresrt { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccresrt de la Tabla segrolestablastransacciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccresrt", Description = " Propiedad publica de tipo DateTime que representa a la columna feccresrt de la Tabla segrolestablastransacciones")]
		public DateTime feccresrt { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumodsrt de la Tabla segrolestablastransacciones
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumodsrt", Description = " Propiedad publica de tipo string que representa a la columna usumodsrt de la Tabla segrolestablastransacciones")]
		public string usumodsrt { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmodsrt de la Tabla segrolestablastransacciones
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmodsrt", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmodsrt de la Tabla segrolestablastransacciones")]
		public DateTime? fecmodsrt { get; set; } 

	}
}

