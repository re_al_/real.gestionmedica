#region 
/***********************************************************************************************************
	NOMBRE:       EntSegAplicaciones
	DESCRIPCION:
		Clase que define un objeto para la Tabla segaplicaciones

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntSegAplicaciones : CBaseClass
	{
		public const string StrNombreTabla = "SegAplicaciones";
		public const string StrAliasTabla = "Sap";
		public enum Fields
		{
			aplicacionsap
			,descripcionsap
			,apiestadosap
			,apitransaccionsap
			,usucresap
			,feccresap
			,usumodsap
			,fecmodsap
			,nombresap

		}
		
		#region Constructoress
		
		public EntSegAplicaciones()
		{
			//Inicializacion de Variables
			aplicacionsap = null;
			descripcionsap = null;
			apiestadosap = null;
			apitransaccionsap = null;
			usucresap = null;
			usumodsap = null;
			fecmodsap = null;
			nombresap = null;
		}
		
		public EntSegAplicaciones(EntSegAplicaciones obj)
		{
			aplicacionsap = obj.aplicacionsap;
			descripcionsap = obj.descripcionsap;
			apiestadosap = obj.apiestadosap;
			apitransaccionsap = obj.apitransaccionsap;
			usucresap = obj.usucresap;
			feccresap = obj.feccresap;
			usumodsap = obj.usumodsap;
			fecmodsap = obj.fecmodsap;
			nombresap = obj.nombresap;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntSegAplicaciones.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna aplicacionsap de la Tabla segaplicaciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(3, MinimumLength=0)]
		[Display(Name = "aplicacionsap", Description = " Propiedad publica de tipo string que representa a la columna aplicacionsap de la Tabla segaplicaciones")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -aplicacionsap- porque es un campo requerido.")]
		[Key]
		public string aplicacionsap { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna descripcionsap de la Tabla segaplicaciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "descripcionsap", Description = " Propiedad publica de tipo string que representa a la columna descripcionsap de la Tabla segaplicaciones")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -descripcionsap- porque es un campo requerido.")]
		public string descripcionsap { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestadosap de la Tabla segaplicaciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestadosap", Description = " Propiedad publica de tipo string que representa a la columna apiestadosap de la Tabla segaplicaciones")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestadosap { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccionsap de la Tabla segaplicaciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apitransaccionsap", Description = " Propiedad publica de tipo string que representa a la columna apitransaccionsap de la Tabla segaplicaciones")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccionsap { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucresap de la Tabla segaplicaciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucresap", Description = " Propiedad publica de tipo string que representa a la columna usucresap de la Tabla segaplicaciones")]
		public string usucresap { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccresap de la Tabla segaplicaciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccresap", Description = " Propiedad publica de tipo DateTime que representa a la columna feccresap de la Tabla segaplicaciones")]
		public DateTime feccresap { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumodsap de la Tabla segaplicaciones
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumodsap", Description = " Propiedad publica de tipo string que representa a la columna usumodsap de la Tabla segaplicaciones")]
		public string usumodsap { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmodsap de la Tabla segaplicaciones
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmodsap", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmodsap de la Tabla segaplicaciones")]
		public DateTime? fecmodsap { get; set; } 

		/// <summary>
		/// Nombre que se despliega en el Menu
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(30, MinimumLength=0)]
		[Display(Name = "nombresap", Description = "Nombre que se despliega en el Menu")]
		public string nombresap { get; set; } 

	}
}

