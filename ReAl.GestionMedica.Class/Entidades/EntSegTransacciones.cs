#region 
/***********************************************************************************************************
	NOMBRE:       EntSegTransacciones
	DESCRIPCION:
		Clase que define un objeto para la Tabla segtransacciones

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntSegTransacciones : CBaseClass
	{
		public const string StrNombreTabla = "SegTransacciones";
		public const string StrAliasTabla = "Str";
		public enum Fields
		{
			tablasta
			,transaccionstr
			,descripcionstr
			,sentenciastr
			,apiestadostr
			,apitransaccionstr
			,usucrestr
			,feccrestr
			,usumodstr
			,fecmodstr

		}
		
		#region Constructoress
		
		public EntSegTransacciones()
		{
			//Inicializacion de Variables
			tablasta = null;
			transaccionstr = null;
			descripcionstr = null;
			sentenciastr = null;
			apiestadostr = null;
			apitransaccionstr = null;
			usucrestr = null;
			usumodstr = null;
			fecmodstr = null;
		}
		
		public EntSegTransacciones(EntSegTransacciones obj)
		{
			tablasta = obj.tablasta;
			transaccionstr = obj.transaccionstr;
			descripcionstr = obj.descripcionstr;
			sentenciastr = obj.sentenciastr;
			apiestadostr = obj.apiestadostr;
			apitransaccionstr = obj.apitransaccionstr;
			usucrestr = obj.usucrestr;
			feccrestr = obj.feccrestr;
			usumodstr = obj.usumodstr;
			fecmodstr = obj.fecmodstr;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntSegTransacciones.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna tablasta de la Tabla segtransacciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: Yes
		/// </summary>
		[StringLength(30, MinimumLength=0)]
		[Display(Name = "tablasta", Description = " Propiedad publica de tipo string que representa a la columna tablasta de la Tabla segtransacciones")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -tablasta- porque es un campo requerido.")]
		[Key]
		public string tablasta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna transaccionstr de la Tabla segtransacciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "transaccionstr", Description = " Propiedad publica de tipo string que representa a la columna transaccionstr de la Tabla segtransacciones")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -transaccionstr- porque es un campo requerido.")]
		[Key]
		public string transaccionstr { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna descripcionstr de la Tabla segtransacciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "descripcionstr", Description = " Propiedad publica de tipo string que representa a la columna descripcionstr de la Tabla segtransacciones")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -descripcionstr- porque es un campo requerido.")]
		public string descripcionstr { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna sentenciastr de la Tabla segtransacciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(7, MinimumLength=0)]
		[Display(Name = "sentenciastr", Description = " Propiedad publica de tipo string que representa a la columna sentenciastr de la Tabla segtransacciones")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -sentenciastr- porque es un campo requerido.")]
		public string sentenciastr { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestadostr de la Tabla segtransacciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestadostr", Description = " Propiedad publica de tipo string que representa a la columna apiestadostr de la Tabla segtransacciones")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestadostr { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccionstr de la Tabla segtransacciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apitransaccionstr", Description = " Propiedad publica de tipo string que representa a la columna apitransaccionstr de la Tabla segtransacciones")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccionstr { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucrestr de la Tabla segtransacciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucrestr", Description = " Propiedad publica de tipo string que representa a la columna usucrestr de la Tabla segtransacciones")]
		public string usucrestr { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccrestr de la Tabla segtransacciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccrestr", Description = " Propiedad publica de tipo DateTime que representa a la columna feccrestr de la Tabla segtransacciones")]
		public DateTime feccrestr { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumodstr de la Tabla segtransacciones
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumodstr", Description = " Propiedad publica de tipo string que representa a la columna usumodstr de la Tabla segtransacciones")]
		public string usumodstr { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmodstr de la Tabla segtransacciones
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmodstr", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmodstr de la Tabla segtransacciones")]
		public DateTime? fecmodstr { get; set; } 

	}
}

