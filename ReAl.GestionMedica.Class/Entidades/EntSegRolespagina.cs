#region
/***********************************************************************************************************
	NOMBRE:       EntSegRolespagina
	DESCRIPCION:
		Clase que define un objeto para la Tabla segrolespagina

	REVISIONES:
		Ver        FECHA       Autor            Descripcion
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion

*************************************************************************************************************/
#endregion

#region

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;

#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
    public class EntSegRolespagina : CBaseClass
    {
        public const string StrNombreTabla = "SegRolespagina";
        public const string StrAliasTabla = "Srp";

        public enum Fields
        {
            rolsro
            , paginaspg
            , apiestadosrp
            , apitransaccionsrp
            , usucresrp
            , feccresrp
            , usumodsrp
            , fecmodsrp
        }

        #region Constructoress

        public EntSegRolespagina()
        {
            //Inicializacion de Variables
            apiestadosrp = null;
            apitransaccionsrp = null;
            usucresrp = null;
            usumodsrp = null;
            fecmodsrp = null;
        }

        public EntSegRolespagina(EntSegRolespagina obj)
        {
            rolsro = obj.rolsro;
            paginaspg = obj.paginaspg;
            apiestadosrp = obj.apiestadosrp;
            apitransaccionsrp = obj.apitransaccionsrp;
            usucresrp = obj.usucresrp;
            feccresrp = obj.feccresrp;
            usumodsrp = obj.usumodsrp;
            fecmodsrp = obj.fecmodsrp;
        }

        #endregion

        #region Metodos Estaticos

        /// <summary>
        /// Funcion para obtener un objeto para el API y sus datos
        /// </summary>
        /// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
        public CApiObject CreateApiObject()
        {
            var objApi = new CApiObject();
            objApi.Nombre = EntSegRolespagina.StrNombreTabla;
            objApi.Datos = this;
            return objApi;
        }

        #endregion

        /// <summary>
        /// Propiedad publica de tipo int que representa a la columna rolsro de la Tabla segrolespagina
        /// Permite Null: No
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: Yes
        /// Es ForeignKey: Yes
        /// </summary>
        [Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
        [Display(Name = "rolsro", Description = " Propiedad publica de tipo int que representa a la columna rolsro de la Tabla segrolespagina")]
        [Required(ErrorMessage = "Se necesita un valor para -rolsro- porque es un campo requerido.")]
        [Key]
        public int rolsro { get; set; }

        /// <summary>
        /// Propiedad publica de tipo int que representa a la columna paginaspg de la Tabla segrolespagina
        /// Permite Null: No
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: Yes
        /// Es ForeignKey: Yes
        /// </summary>
        [Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
        [Display(Name = "paginaspg", Description = " Propiedad publica de tipo int que representa a la columna paginaspg de la Tabla segrolespagina")]
        [Required(ErrorMessage = "Se necesita un valor para -paginaspg- porque es un campo requerido.")]
        [Key]
        public int paginaspg { get; set; }

        /// <summary>
        /// Propiedad publica de tipo string que representa a la columna apiestadosrp de la Tabla segrolespagina
        /// Permite Null: No
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [StringLength(15, MinimumLength = 0)]
        [Display(Name = "apiestadosrp", Description = " Propiedad publica de tipo string que representa a la columna apiestadosrp de la Tabla segrolespagina")]
        [EnumDataType(typeof(CApi.Estado))]
        public string apiestadosrp { get; set; }

        /// <summary>
        /// Propiedad publica de tipo string que representa a la columna apitransaccionsrp de la Tabla segrolespagina
        /// Permite Null: No
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [StringLength(15, MinimumLength = 0)]
        [Display(Name = "apitransaccionsrp", Description = " Propiedad publica de tipo string que representa a la columna apitransaccionsrp de la Tabla segrolespagina")]
        [EnumDataType(typeof(CApi.Transaccion))]
        public string apitransaccionsrp { get; set; }

        /// <summary>
        /// Propiedad publica de tipo string que representa a la columna usucresrp de la Tabla segrolespagina
        /// Permite Null: No
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [StringLength(15, MinimumLength = 0)]
        [Display(Name = "usucresrp", Description = " Propiedad publica de tipo string que representa a la columna usucresrp de la Tabla segrolespagina")]
        public string usucresrp { get; set; }

        /// <summary>
        /// Propiedad publica de tipo DateTime que representa a la columna feccresrp de la Tabla segrolespagina
        /// Permite Null: No
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
        [DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
        [Display(Name = "feccresrp", Description = " Propiedad publica de tipo DateTime que representa a la columna feccresrp de la Tabla segrolespagina")]
        public DateTime feccresrp { get; set; }

        /// <summary>
        /// Propiedad publica de tipo string que representa a la columna usumodsrp de la Tabla segrolespagina
        /// Permite Null: Yes
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [StringLength(15, MinimumLength = 0)]
        [Display(Name = "usumodsrp", Description = " Propiedad publica de tipo string que representa a la columna usumodsrp de la Tabla segrolespagina")]
        public string usumodsrp { get; set; }

        /// <summary>
        /// Propiedad publica de tipo DateTime que representa a la columna fecmodsrp de la Tabla segrolespagina
        /// Permite Null: Yes
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
        [DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
        [Display(Name = "fecmodsrp", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmodsrp de la Tabla segrolespagina")]
        public DateTime? fecmodsrp { get; set; }
    }
}