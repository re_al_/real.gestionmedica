#region 
/***********************************************************************************************************
	NOMBRE:       EntSegParametros
	DESCRIPCION:
		Clase que define un objeto para la Tabla segparametros

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntSegParametros : CBaseClass
	{
		public const string StrNombreTabla = "SegParametros";
		public const string StrAliasTabla = "Spa";
		public enum Fields
		{
			siglaspa
			,descripcionspa
			,apiestadospa
			,apitransaccionspa
			,usucrespa
			,feccrespa
			,usumodspa
			,fecmodspa

		}
		
		#region Constructoress
		
		public EntSegParametros()
		{
			//Inicializacion de Variables
			siglaspa = null;
			descripcionspa = null;
			apiestadospa = null;
			apitransaccionspa = null;
			usucrespa = null;
			usumodspa = null;
			fecmodspa = null;
		}
		
		public EntSegParametros(EntSegParametros obj)
		{
			siglaspa = obj.siglaspa;
			descripcionspa = obj.descripcionspa;
			apiestadospa = obj.apiestadospa;
			apitransaccionspa = obj.apitransaccionspa;
			usucrespa = obj.usucrespa;
			feccrespa = obj.feccrespa;
			usumodspa = obj.usumodspa;
			fecmodspa = obj.fecmodspa;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntSegParametros.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna siglaspa de la Tabla segparametros
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(3, MinimumLength=0)]
		[Display(Name = "siglaspa", Description = " Propiedad publica de tipo string que representa a la columna siglaspa de la Tabla segparametros")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -siglaspa- porque es un campo requerido.")]
		[Key]
		public string siglaspa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna descripcionspa de la Tabla segparametros
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(180, MinimumLength=0)]
		[Display(Name = "descripcionspa", Description = " Propiedad publica de tipo string que representa a la columna descripcionspa de la Tabla segparametros")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -descripcionspa- porque es un campo requerido.")]
		public string descripcionspa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestadospa de la Tabla segparametros
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestadospa", Description = " Propiedad publica de tipo string que representa a la columna apiestadospa de la Tabla segparametros")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestadospa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccionspa de la Tabla segparametros
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apitransaccionspa", Description = " Propiedad publica de tipo string que representa a la columna apitransaccionspa de la Tabla segparametros")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccionspa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucrespa de la Tabla segparametros
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucrespa", Description = " Propiedad publica de tipo string que representa a la columna usucrespa de la Tabla segparametros")]
		public string usucrespa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccrespa de la Tabla segparametros
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccrespa", Description = " Propiedad publica de tipo DateTime que representa a la columna feccrespa de la Tabla segparametros")]
		public DateTime feccrespa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumodspa de la Tabla segparametros
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumodspa", Description = " Propiedad publica de tipo string que representa a la columna usumodspa de la Tabla segparametros")]
		public string usumodspa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmodspa de la Tabla segparametros
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmodspa", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmodspa de la Tabla segparametros")]
		public DateTime? fecmodspa { get; set; } 

	}
}

