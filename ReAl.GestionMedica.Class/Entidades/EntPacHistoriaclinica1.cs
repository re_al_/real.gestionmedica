#region 
/***********************************************************************************************************
	NOMBRE:       EntPacHistoriaclinica1
	DESCRIPCION:
		Clase que define un objeto para la Tabla pachistoriaclinica1

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntPacHistoriaclinica1 : CBaseClass
	{
		public const string StrNombreTabla = "PacHistoriaclinica1";
		public const string StrAliasTabla = "Ph1";
		public enum Fields
		{
			idppa
			,h1motivophc
			,h1enfermedadphc
			,h1cirujiasphc
			,h1infeccionesphc
			,h1traumatismosphc
			,h1cardiovascularphc
			,h1metabolicophc
			,h1toxicophc
			,h1familiarphc
			,h1ginecoobsphc
			,h1otrosphc
			,h1embarazosphc
			,h1gestaphc
			,h1abortosphc
			,h1mesntruacionesphc
			,h1fumphc
			,h1fupphc
			,h1revisionphc
			,h1examenfisicophc
			,h1taphc
			,h1fcphc
			,h1frphc
			,h1tempphc
			,h1tallaphc
			,h1pesophc
			,h1pcphc
			,h2cabezaphc
			,h2cuellophc
			,h2toraxphc
			,h2abdomenphc
			,h2genitourinariophc
			,h2extremidadesphc
			,h2conalertaphc
			,h2conconfusionphc
			,h2consomnolenciaphc
			,h2conestuforphc
			,h2concomaphc
			,h2oriespaciophc
			,h2orilugarphc
			,h2oripersonaphc
			,h2razideaphc
			,h2razjuiciophc
			,h2razabstraccionphc
			,h2conocimientosgenerales
			,h2atgdesatentophc
			,h2atgfluctuantephc
			,h2atgnegativophc
			,h2atgnegligentephc
			,h2pagconfusionphc
			,h2pagdeliriophc
			,h2paedelusionesphc
			,h2paeilsuionesphc
			,h2paealucinacionesphc
			,h3conrecientephc
			,h3conremotaphc
			,h3afeeuforicophc
			,h3afeplanophc
			,h3afedeprimidophc
			,h3afelabilphc
			,h3afehostilphc
			,h3afeinadecuadophc
			,h3hdoizquierdophc
			,h3hdoderechophc
			,h3cgeexpresionphc
			,h3cgenominacionphc
			,h3cgerepeticionphc
			,h3cgecomprensionphc
			,h3apebrocaphc
			,h3apeconduccionphc
			,h3apewernickephc
			,h3apeglobalphc
			,h3apeanomiaphc
			,h3atrmotoraphc
			,h3atrsensitivaphc
			,h3atrmixtaphc
			,h3ataexpresionphc
			,h3atacomprensionphc
			,h3atamixtaphc
			,h3aprmotoraphc
			,h3aprsensorialphc
			,h3lesalexiaphc
			,h3lesagrafiaphc
			,h3lpdacalculiaphc
			,h3lpdagrafiaphc
			,h3lpddesorientacionphc
			,h3lpdagnosiaphc
			,h3agnauditivaphc
			,h3agnvisualphc
			,h3agntactilphc
			,h3aprideacionalphc
			,h3aprideomotoraphc
			,h3aprdesatencionphc
			,h3anosognosiaphc
			,apiestado
			,apitransaccion
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public EntPacHistoriaclinica1()
		{
			//Inicializacion de Variables
			h1motivophc = null;
			h1enfermedadphc = null;
			h1cirujiasphc = null;
			h1infeccionesphc = null;
			h1traumatismosphc = null;
			h1cardiovascularphc = null;
			h1metabolicophc = null;
			h1toxicophc = null;
			h1familiarphc = null;
			h1ginecoobsphc = null;
			h1otrosphc = null;
			h1embarazosphc = null;
			h1gestaphc = null;
			h1abortosphc = null;
			h1mesntruacionesphc = null;
			h1fumphc = null;
			h1fupphc = null;
			h1revisionphc = null;
			h1examenfisicophc = null;
			h1taphc = null;
			h1fcphc = null;
			h1frphc = null;
			h1tempphc = null;
			h1tallaphc = null;
			h1pesophc = null;
			h1pcphc = null;
			h2cabezaphc = null;
			h2cuellophc = null;
			h2toraxphc = null;
			h2abdomenphc = null;
			h2genitourinariophc = null;
			h2extremidadesphc = null;
			h2conalertaphc = null;
			h2conconfusionphc = null;
			h2consomnolenciaphc = null;
			h2conestuforphc = null;
			h2concomaphc = null;
			h2oriespaciophc = null;
			h2orilugarphc = null;
			h2oripersonaphc = null;
			h2razideaphc = null;
			h2razjuiciophc = null;
			h2razabstraccionphc = null;
			h2conocimientosgenerales = null;
			h2atgdesatentophc = null;
			h2atgfluctuantephc = null;
			h2atgnegativophc = null;
			h2atgnegligentephc = null;
			h2pagconfusionphc = null;
			h2pagdeliriophc = null;
			h2paedelusionesphc = null;
			h2paeilsuionesphc = null;
			h2paealucinacionesphc = null;
			h3conrecientephc = null;
			h3conremotaphc = null;
			h3afeeuforicophc = null;
			h3afeplanophc = null;
			h3afedeprimidophc = null;
			h3afelabilphc = null;
			h3afehostilphc = null;
			h3afeinadecuadophc = null;
			h3hdoizquierdophc = null;
			h3hdoderechophc = null;
			h3cgeexpresionphc = null;
			h3cgenominacionphc = null;
			h3cgerepeticionphc = null;
			h3cgecomprensionphc = null;
			h3apebrocaphc = null;
			h3apeconduccionphc = null;
			h3apewernickephc = null;
			h3apeglobalphc = null;
			h3apeanomiaphc = null;
			h3atrmotoraphc = null;
			h3atrsensitivaphc = null;
			h3atrmixtaphc = null;
			h3ataexpresionphc = null;
			h3atacomprensionphc = null;
			h3atamixtaphc = null;
			h3aprmotoraphc = null;
			h3aprsensorialphc = null;
			h3lesalexiaphc = null;
			h3lesagrafiaphc = null;
			h3lpdacalculiaphc = null;
			h3lpdagrafiaphc = null;
			h3lpddesorientacionphc = null;
			h3lpdagnosiaphc = null;
			h3agnauditivaphc = null;
			h3agnvisualphc = null;
			h3agntactilphc = null;
			h3aprideacionalphc = null;
			h3aprideomotoraphc = null;
			h3aprdesatencionphc = null;
			h3anosognosiaphc = null;
			apiestado = null;
			apitransaccion = null;
			usucre = null;
			usumod = null;
			fecmod = null;
		}
		
		public EntPacHistoriaclinica1(EntPacHistoriaclinica1 obj)
		{
			idppa = obj.idppa;
			h1motivophc = obj.h1motivophc;
			h1enfermedadphc = obj.h1enfermedadphc;
			h1cirujiasphc = obj.h1cirujiasphc;
			h1infeccionesphc = obj.h1infeccionesphc;
			h1traumatismosphc = obj.h1traumatismosphc;
			h1cardiovascularphc = obj.h1cardiovascularphc;
			h1metabolicophc = obj.h1metabolicophc;
			h1toxicophc = obj.h1toxicophc;
			h1familiarphc = obj.h1familiarphc;
			h1ginecoobsphc = obj.h1ginecoobsphc;
			h1otrosphc = obj.h1otrosphc;
			h1embarazosphc = obj.h1embarazosphc;
			h1gestaphc = obj.h1gestaphc;
			h1abortosphc = obj.h1abortosphc;
			h1mesntruacionesphc = obj.h1mesntruacionesphc;
			h1fumphc = obj.h1fumphc;
			h1fupphc = obj.h1fupphc;
			h1revisionphc = obj.h1revisionphc;
			h1examenfisicophc = obj.h1examenfisicophc;
			h1taphc = obj.h1taphc;
			h1fcphc = obj.h1fcphc;
			h1frphc = obj.h1frphc;
			h1tempphc = obj.h1tempphc;
			h1tallaphc = obj.h1tallaphc;
			h1pesophc = obj.h1pesophc;
			h1pcphc = obj.h1pcphc;
			h2cabezaphc = obj.h2cabezaphc;
			h2cuellophc = obj.h2cuellophc;
			h2toraxphc = obj.h2toraxphc;
			h2abdomenphc = obj.h2abdomenphc;
			h2genitourinariophc = obj.h2genitourinariophc;
			h2extremidadesphc = obj.h2extremidadesphc;
			h2conalertaphc = obj.h2conalertaphc;
			h2conconfusionphc = obj.h2conconfusionphc;
			h2consomnolenciaphc = obj.h2consomnolenciaphc;
			h2conestuforphc = obj.h2conestuforphc;
			h2concomaphc = obj.h2concomaphc;
			h2oriespaciophc = obj.h2oriespaciophc;
			h2orilugarphc = obj.h2orilugarphc;
			h2oripersonaphc = obj.h2oripersonaphc;
			h2razideaphc = obj.h2razideaphc;
			h2razjuiciophc = obj.h2razjuiciophc;
			h2razabstraccionphc = obj.h2razabstraccionphc;
			h2conocimientosgenerales = obj.h2conocimientosgenerales;
			h2atgdesatentophc = obj.h2atgdesatentophc;
			h2atgfluctuantephc = obj.h2atgfluctuantephc;
			h2atgnegativophc = obj.h2atgnegativophc;
			h2atgnegligentephc = obj.h2atgnegligentephc;
			h2pagconfusionphc = obj.h2pagconfusionphc;
			h2pagdeliriophc = obj.h2pagdeliriophc;
			h2paedelusionesphc = obj.h2paedelusionesphc;
			h2paeilsuionesphc = obj.h2paeilsuionesphc;
			h2paealucinacionesphc = obj.h2paealucinacionesphc;
			h3conrecientephc = obj.h3conrecientephc;
			h3conremotaphc = obj.h3conremotaphc;
			h3afeeuforicophc = obj.h3afeeuforicophc;
			h3afeplanophc = obj.h3afeplanophc;
			h3afedeprimidophc = obj.h3afedeprimidophc;
			h3afelabilphc = obj.h3afelabilphc;
			h3afehostilphc = obj.h3afehostilphc;
			h3afeinadecuadophc = obj.h3afeinadecuadophc;
			h3hdoizquierdophc = obj.h3hdoizquierdophc;
			h3hdoderechophc = obj.h3hdoderechophc;
			h3cgeexpresionphc = obj.h3cgeexpresionphc;
			h3cgenominacionphc = obj.h3cgenominacionphc;
			h3cgerepeticionphc = obj.h3cgerepeticionphc;
			h3cgecomprensionphc = obj.h3cgecomprensionphc;
			h3apebrocaphc = obj.h3apebrocaphc;
			h3apeconduccionphc = obj.h3apeconduccionphc;
			h3apewernickephc = obj.h3apewernickephc;
			h3apeglobalphc = obj.h3apeglobalphc;
			h3apeanomiaphc = obj.h3apeanomiaphc;
			h3atrmotoraphc = obj.h3atrmotoraphc;
			h3atrsensitivaphc = obj.h3atrsensitivaphc;
			h3atrmixtaphc = obj.h3atrmixtaphc;
			h3ataexpresionphc = obj.h3ataexpresionphc;
			h3atacomprensionphc = obj.h3atacomprensionphc;
			h3atamixtaphc = obj.h3atamixtaphc;
			h3aprmotoraphc = obj.h3aprmotoraphc;
			h3aprsensorialphc = obj.h3aprsensorialphc;
			h3lesalexiaphc = obj.h3lesalexiaphc;
			h3lesagrafiaphc = obj.h3lesagrafiaphc;
			h3lpdacalculiaphc = obj.h3lpdacalculiaphc;
			h3lpdagrafiaphc = obj.h3lpdagrafiaphc;
			h3lpddesorientacionphc = obj.h3lpddesorientacionphc;
			h3lpdagnosiaphc = obj.h3lpdagnosiaphc;
			h3agnauditivaphc = obj.h3agnauditivaphc;
			h3agnvisualphc = obj.h3agnvisualphc;
			h3agntactilphc = obj.h3agntactilphc;
			h3aprideacionalphc = obj.h3aprideacionalphc;
			h3aprideomotoraphc = obj.h3aprideomotoraphc;
			h3aprdesatencionphc = obj.h3aprdesatencionphc;
			h3anosognosiaphc = obj.h3anosognosiaphc;
			apiestado = obj.apiestado;
			apitransaccion = obj.apitransaccion;
			usucre = obj.usucre;
			feccre = obj.feccre;
			usumod = obj.usumod;
			fecmod = obj.fecmod;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntPacHistoriaclinica1.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla pachistoriaclinica1
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "idppa", Description = " Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla pachistoriaclinica1")]
		[Required(ErrorMessage = "Se necesita un valor para -idppa- porque es un campo requerido.")]
		[Key]
		public Int64 idppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1motivophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h1motivophc", Description = " Propiedad publica de tipo string que representa a la columna h1motivophc de la Tabla pachistoriaclinica1")]
		public string h1motivophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1enfermedadphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h1enfermedadphc", Description = " Propiedad publica de tipo string que representa a la columna h1enfermedadphc de la Tabla pachistoriaclinica1")]
		public string h1enfermedadphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1cirujiasphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1cirujiasphc", Description = " Propiedad publica de tipo string que representa a la columna h1cirujiasphc de la Tabla pachistoriaclinica1")]
		public string h1cirujiasphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1infeccionesphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1infeccionesphc", Description = " Propiedad publica de tipo string que representa a la columna h1infeccionesphc de la Tabla pachistoriaclinica1")]
		public string h1infeccionesphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1traumatismosphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1traumatismosphc", Description = " Propiedad publica de tipo string que representa a la columna h1traumatismosphc de la Tabla pachistoriaclinica1")]
		public string h1traumatismosphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1cardiovascularphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1cardiovascularphc", Description = " Propiedad publica de tipo string que representa a la columna h1cardiovascularphc de la Tabla pachistoriaclinica1")]
		public string h1cardiovascularphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1metabolicophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1metabolicophc", Description = " Propiedad publica de tipo string que representa a la columna h1metabolicophc de la Tabla pachistoriaclinica1")]
		public string h1metabolicophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1toxicophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1toxicophc", Description = " Propiedad publica de tipo string que representa a la columna h1toxicophc de la Tabla pachistoriaclinica1")]
		public string h1toxicophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1familiarphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1familiarphc", Description = " Propiedad publica de tipo string que representa a la columna h1familiarphc de la Tabla pachistoriaclinica1")]
		public string h1familiarphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1ginecoobsphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1ginecoobsphc", Description = " Propiedad publica de tipo string que representa a la columna h1ginecoobsphc de la Tabla pachistoriaclinica1")]
		public string h1ginecoobsphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1otrosphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1otrosphc", Description = " Propiedad publica de tipo string que representa a la columna h1otrosphc de la Tabla pachistoriaclinica1")]
		public string h1otrosphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna h1embarazosphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "h1embarazosphc", Description = " Propiedad publica de tipo int que representa a la columna h1embarazosphc de la Tabla pachistoriaclinica1")]
		public int? h1embarazosphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna h1gestaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "h1gestaphc", Description = " Propiedad publica de tipo int que representa a la columna h1gestaphc de la Tabla pachistoriaclinica1")]
		public int? h1gestaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna h1abortosphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "h1abortosphc", Description = " Propiedad publica de tipo int que representa a la columna h1abortosphc de la Tabla pachistoriaclinica1")]
		public int? h1abortosphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1mesntruacionesphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1mesntruacionesphc", Description = " Propiedad publica de tipo string que representa a la columna h1mesntruacionesphc de la Tabla pachistoriaclinica1")]
		public string h1mesntruacionesphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1fumphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1fumphc", Description = " Propiedad publica de tipo string que representa a la columna h1fumphc de la Tabla pachistoriaclinica1")]
		public string h1fumphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1fupphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h1fupphc", Description = " Propiedad publica de tipo string que representa a la columna h1fupphc de la Tabla pachistoriaclinica1")]
		public string h1fupphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1revisionphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h1revisionphc", Description = " Propiedad publica de tipo string que representa a la columna h1revisionphc de la Tabla pachistoriaclinica1")]
		public string h1revisionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1examenfisicophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h1examenfisicophc", Description = " Propiedad publica de tipo string que representa a la columna h1examenfisicophc de la Tabla pachistoriaclinica1")]
		public string h1examenfisicophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1taphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h1taphc", Description = " Propiedad publica de tipo string que representa a la columna h1taphc de la Tabla pachistoriaclinica1")]
		public string h1taphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1fcphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h1fcphc", Description = " Propiedad publica de tipo string que representa a la columna h1fcphc de la Tabla pachistoriaclinica1")]
		public string h1fcphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1frphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h1frphc", Description = " Propiedad publica de tipo string que representa a la columna h1frphc de la Tabla pachistoriaclinica1")]
		public string h1frphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1tempphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h1tempphc", Description = " Propiedad publica de tipo string que representa a la columna h1tempphc de la Tabla pachistoriaclinica1")]
		public string h1tempphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1tallaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h1tallaphc", Description = " Propiedad publica de tipo string que representa a la columna h1tallaphc de la Tabla pachistoriaclinica1")]
		public string h1tallaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1pesophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h1pesophc", Description = " Propiedad publica de tipo string que representa a la columna h1pesophc de la Tabla pachistoriaclinica1")]
		public string h1pesophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h1pcphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h1pcphc", Description = " Propiedad publica de tipo string que representa a la columna h1pcphc de la Tabla pachistoriaclinica1")]
		public string h1pcphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2cabezaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h2cabezaphc", Description = " Propiedad publica de tipo string que representa a la columna h2cabezaphc de la Tabla pachistoriaclinica1")]
		public string h2cabezaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2cuellophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h2cuellophc", Description = " Propiedad publica de tipo string que representa a la columna h2cuellophc de la Tabla pachistoriaclinica1")]
		public string h2cuellophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2toraxphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h2toraxphc", Description = " Propiedad publica de tipo string que representa a la columna h2toraxphc de la Tabla pachistoriaclinica1")]
		public string h2toraxphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2abdomenphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h2abdomenphc", Description = " Propiedad publica de tipo string que representa a la columna h2abdomenphc de la Tabla pachistoriaclinica1")]
		public string h2abdomenphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2genitourinariophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h2genitourinariophc", Description = " Propiedad publica de tipo string que representa a la columna h2genitourinariophc de la Tabla pachistoriaclinica1")]
		public string h2genitourinariophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2extremidadesphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1000, MinimumLength=0)]
		[Display(Name = "h2extremidadesphc", Description = " Propiedad publica de tipo string que representa a la columna h2extremidadesphc de la Tabla pachistoriaclinica1")]
		public string h2extremidadesphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2conalertaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2conalertaphc", Description = " Propiedad publica de tipo string que representa a la columna h2conalertaphc de la Tabla pachistoriaclinica1")]
		public string h2conalertaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2conconfusionphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2conconfusionphc", Description = " Propiedad publica de tipo string que representa a la columna h2conconfusionphc de la Tabla pachistoriaclinica1")]
		public string h2conconfusionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2consomnolenciaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2consomnolenciaphc", Description = " Propiedad publica de tipo string que representa a la columna h2consomnolenciaphc de la Tabla pachistoriaclinica1")]
		public string h2consomnolenciaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2conestuforphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2conestuforphc", Description = " Propiedad publica de tipo string que representa a la columna h2conestuforphc de la Tabla pachistoriaclinica1")]
		public string h2conestuforphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2concomaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2concomaphc", Description = " Propiedad publica de tipo string que representa a la columna h2concomaphc de la Tabla pachistoriaclinica1")]
		public string h2concomaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2oriespaciophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2oriespaciophc", Description = " Propiedad publica de tipo string que representa a la columna h2oriespaciophc de la Tabla pachistoriaclinica1")]
		public string h2oriespaciophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2orilugarphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2orilugarphc", Description = " Propiedad publica de tipo string que representa a la columna h2orilugarphc de la Tabla pachistoriaclinica1")]
		public string h2orilugarphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2oripersonaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2oripersonaphc", Description = " Propiedad publica de tipo string que representa a la columna h2oripersonaphc de la Tabla pachistoriaclinica1")]
		public string h2oripersonaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2razideaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2razideaphc", Description = " Propiedad publica de tipo string que representa a la columna h2razideaphc de la Tabla pachistoriaclinica1")]
		public string h2razideaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2razjuiciophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2razjuiciophc", Description = " Propiedad publica de tipo string que representa a la columna h2razjuiciophc de la Tabla pachistoriaclinica1")]
		public string h2razjuiciophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2razabstraccionphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2razabstraccionphc", Description = " Propiedad publica de tipo string que representa a la columna h2razabstraccionphc de la Tabla pachistoriaclinica1")]
		public string h2razabstraccionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2conocimientosgenerales de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "h2conocimientosgenerales", Description = " Propiedad publica de tipo string que representa a la columna h2conocimientosgenerales de la Tabla pachistoriaclinica1")]
		public string h2conocimientosgenerales { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2atgdesatentophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2atgdesatentophc", Description = " Propiedad publica de tipo string que representa a la columna h2atgdesatentophc de la Tabla pachistoriaclinica1")]
		public string h2atgdesatentophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2atgfluctuantephc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2atgfluctuantephc", Description = " Propiedad publica de tipo string que representa a la columna h2atgfluctuantephc de la Tabla pachistoriaclinica1")]
		public string h2atgfluctuantephc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2atgnegativophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2atgnegativophc", Description = " Propiedad publica de tipo string que representa a la columna h2atgnegativophc de la Tabla pachistoriaclinica1")]
		public string h2atgnegativophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2atgnegligentephc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2atgnegligentephc", Description = " Propiedad publica de tipo string que representa a la columna h2atgnegligentephc de la Tabla pachistoriaclinica1")]
		public string h2atgnegligentephc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2pagconfusionphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2pagconfusionphc", Description = " Propiedad publica de tipo string que representa a la columna h2pagconfusionphc de la Tabla pachistoriaclinica1")]
		public string h2pagconfusionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2pagdeliriophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2pagdeliriophc", Description = " Propiedad publica de tipo string que representa a la columna h2pagdeliriophc de la Tabla pachistoriaclinica1")]
		public string h2pagdeliriophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2paedelusionesphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2paedelusionesphc", Description = " Propiedad publica de tipo string que representa a la columna h2paedelusionesphc de la Tabla pachistoriaclinica1")]
		public string h2paedelusionesphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2paeilsuionesphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2paeilsuionesphc", Description = " Propiedad publica de tipo string que representa a la columna h2paeilsuionesphc de la Tabla pachistoriaclinica1")]
		public string h2paeilsuionesphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h2paealucinacionesphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h2paealucinacionesphc", Description = " Propiedad publica de tipo string que representa a la columna h2paealucinacionesphc de la Tabla pachistoriaclinica1")]
		public string h2paealucinacionesphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3conrecientephc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3conrecientephc", Description = " Propiedad publica de tipo string que representa a la columna h3conrecientephc de la Tabla pachistoriaclinica1")]
		public string h3conrecientephc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3conremotaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3conremotaphc", Description = " Propiedad publica de tipo string que representa a la columna h3conremotaphc de la Tabla pachistoriaclinica1")]
		public string h3conremotaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3afeeuforicophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3afeeuforicophc", Description = " Propiedad publica de tipo string que representa a la columna h3afeeuforicophc de la Tabla pachistoriaclinica1")]
		public string h3afeeuforicophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3afeplanophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3afeplanophc", Description = " Propiedad publica de tipo string que representa a la columna h3afeplanophc de la Tabla pachistoriaclinica1")]
		public string h3afeplanophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3afedeprimidophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3afedeprimidophc", Description = " Propiedad publica de tipo string que representa a la columna h3afedeprimidophc de la Tabla pachistoriaclinica1")]
		public string h3afedeprimidophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3afelabilphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3afelabilphc", Description = " Propiedad publica de tipo string que representa a la columna h3afelabilphc de la Tabla pachistoriaclinica1")]
		public string h3afelabilphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3afehostilphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3afehostilphc", Description = " Propiedad publica de tipo string que representa a la columna h3afehostilphc de la Tabla pachistoriaclinica1")]
		public string h3afehostilphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3afeinadecuadophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3afeinadecuadophc", Description = " Propiedad publica de tipo string que representa a la columna h3afeinadecuadophc de la Tabla pachistoriaclinica1")]
		public string h3afeinadecuadophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3hdoizquierdophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3hdoizquierdophc", Description = " Propiedad publica de tipo string que representa a la columna h3hdoizquierdophc de la Tabla pachistoriaclinica1")]
		public string h3hdoizquierdophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3hdoderechophc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3hdoderechophc", Description = " Propiedad publica de tipo string que representa a la columna h3hdoderechophc de la Tabla pachistoriaclinica1")]
		public string h3hdoderechophc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3cgeexpresionphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3cgeexpresionphc", Description = " Propiedad publica de tipo string que representa a la columna h3cgeexpresionphc de la Tabla pachistoriaclinica1")]
		public string h3cgeexpresionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3cgenominacionphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3cgenominacionphc", Description = " Propiedad publica de tipo string que representa a la columna h3cgenominacionphc de la Tabla pachistoriaclinica1")]
		public string h3cgenominacionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3cgerepeticionphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3cgerepeticionphc", Description = " Propiedad publica de tipo string que representa a la columna h3cgerepeticionphc de la Tabla pachistoriaclinica1")]
		public string h3cgerepeticionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3cgecomprensionphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3cgecomprensionphc", Description = " Propiedad publica de tipo string que representa a la columna h3cgecomprensionphc de la Tabla pachistoriaclinica1")]
		public string h3cgecomprensionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3apebrocaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3apebrocaphc", Description = " Propiedad publica de tipo string que representa a la columna h3apebrocaphc de la Tabla pachistoriaclinica1")]
		public string h3apebrocaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3apeconduccionphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3apeconduccionphc", Description = " Propiedad publica de tipo string que representa a la columna h3apeconduccionphc de la Tabla pachistoriaclinica1")]
		public string h3apeconduccionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3apewernickephc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3apewernickephc", Description = " Propiedad publica de tipo string que representa a la columna h3apewernickephc de la Tabla pachistoriaclinica1")]
		public string h3apewernickephc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3apeglobalphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3apeglobalphc", Description = " Propiedad publica de tipo string que representa a la columna h3apeglobalphc de la Tabla pachistoriaclinica1")]
		public string h3apeglobalphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3apeanomiaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3apeanomiaphc", Description = " Propiedad publica de tipo string que representa a la columna h3apeanomiaphc de la Tabla pachistoriaclinica1")]
		public string h3apeanomiaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3atrmotoraphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3atrmotoraphc", Description = " Propiedad publica de tipo string que representa a la columna h3atrmotoraphc de la Tabla pachistoriaclinica1")]
		public string h3atrmotoraphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3atrsensitivaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3atrsensitivaphc", Description = " Propiedad publica de tipo string que representa a la columna h3atrsensitivaphc de la Tabla pachistoriaclinica1")]
		public string h3atrsensitivaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3atrmixtaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3atrmixtaphc", Description = " Propiedad publica de tipo string que representa a la columna h3atrmixtaphc de la Tabla pachistoriaclinica1")]
		public string h3atrmixtaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3ataexpresionphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3ataexpresionphc", Description = " Propiedad publica de tipo string que representa a la columna h3ataexpresionphc de la Tabla pachistoriaclinica1")]
		public string h3ataexpresionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3atacomprensionphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3atacomprensionphc", Description = " Propiedad publica de tipo string que representa a la columna h3atacomprensionphc de la Tabla pachistoriaclinica1")]
		public string h3atacomprensionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3atamixtaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3atamixtaphc", Description = " Propiedad publica de tipo string que representa a la columna h3atamixtaphc de la Tabla pachistoriaclinica1")]
		public string h3atamixtaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3aprmotoraphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3aprmotoraphc", Description = " Propiedad publica de tipo string que representa a la columna h3aprmotoraphc de la Tabla pachistoriaclinica1")]
		public string h3aprmotoraphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3aprsensorialphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3aprsensorialphc", Description = " Propiedad publica de tipo string que representa a la columna h3aprsensorialphc de la Tabla pachistoriaclinica1")]
		public string h3aprsensorialphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3lesalexiaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3lesalexiaphc", Description = " Propiedad publica de tipo string que representa a la columna h3lesalexiaphc de la Tabla pachistoriaclinica1")]
		public string h3lesalexiaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3lesagrafiaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3lesagrafiaphc", Description = " Propiedad publica de tipo string que representa a la columna h3lesagrafiaphc de la Tabla pachistoriaclinica1")]
		public string h3lesagrafiaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3lpdacalculiaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3lpdacalculiaphc", Description = " Propiedad publica de tipo string que representa a la columna h3lpdacalculiaphc de la Tabla pachistoriaclinica1")]
		public string h3lpdacalculiaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3lpdagrafiaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3lpdagrafiaphc", Description = " Propiedad publica de tipo string que representa a la columna h3lpdagrafiaphc de la Tabla pachistoriaclinica1")]
		public string h3lpdagrafiaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3lpddesorientacionphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3lpddesorientacionphc", Description = " Propiedad publica de tipo string que representa a la columna h3lpddesorientacionphc de la Tabla pachistoriaclinica1")]
		public string h3lpddesorientacionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3lpdagnosiaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3lpdagnosiaphc", Description = " Propiedad publica de tipo string que representa a la columna h3lpdagnosiaphc de la Tabla pachistoriaclinica1")]
		public string h3lpdagnosiaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3agnauditivaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3agnauditivaphc", Description = " Propiedad publica de tipo string que representa a la columna h3agnauditivaphc de la Tabla pachistoriaclinica1")]
		public string h3agnauditivaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3agnvisualphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3agnvisualphc", Description = " Propiedad publica de tipo string que representa a la columna h3agnvisualphc de la Tabla pachistoriaclinica1")]
		public string h3agnvisualphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3agntactilphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "h3agntactilphc", Description = " Propiedad publica de tipo string que representa a la columna h3agntactilphc de la Tabla pachistoriaclinica1")]
		public string h3agntactilphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3aprideacionalphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3aprideacionalphc", Description = " Propiedad publica de tipo string que representa a la columna h3aprideacionalphc de la Tabla pachistoriaclinica1")]
		public string h3aprideacionalphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3aprideomotoraphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3aprideomotoraphc", Description = " Propiedad publica de tipo string que representa a la columna h3aprideomotoraphc de la Tabla pachistoriaclinica1")]
		public string h3aprideomotoraphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3aprdesatencionphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3aprdesatencionphc", Description = " Propiedad publica de tipo string que representa a la columna h3aprdesatencionphc de la Tabla pachistoriaclinica1")]
		public string h3aprdesatencionphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna h3anosognosiaphc de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "h3anosognosiaphc", Description = " Propiedad publica de tipo string que representa a la columna h3anosognosiaphc de la Tabla pachistoriaclinica1")]
		public string h3anosognosiaphc { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestado de la Tabla pachistoriaclinica1
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo string que representa a la columna apiestado de la Tabla pachistoriaclinica1")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestado { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla pachistoriaclinica1
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apitransaccion", Description = " Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla pachistoriaclinica1")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucre de la Tabla pachistoriaclinica1
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo string que representa a la columna usucre de la Tabla pachistoriaclinica1")]
		public string usucre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla pachistoriaclinica1
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla pachistoriaclinica1")]
		public DateTime feccre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumod de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo string que representa a la columna usumod de la Tabla pachistoriaclinica1")]
		public string usumod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla pachistoriaclinica1
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla pachistoriaclinica1")]
		public DateTime? fecmod { get; set; } 

	}
}

