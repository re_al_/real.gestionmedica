#region 
/***********************************************************************************************************
	NOMBRE:       EntClaCie
	DESCRIPCION:
		Clase que define un objeto para la Tabla clacie

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntClaCie : CBaseClass
	{
		public const string StrNombreTabla = "ClaCie";
		public const string StrAliasTabla = "clacie";
		public enum Fields
		{
			codcie
			,desccie

		}
		
		#region Constructoress
		
		public EntClaCie()
		{
			//Inicializacion de Variables
			codcie = null;
			desccie = null;
		}
		
		public EntClaCie(EntClaCie obj)
		{
			codcie = obj.codcie;
			desccie = obj.desccie;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntClaCie.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna codcie de la Tabla clacie
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5, MinimumLength=0)]
		[Display(Name = "codcie", Description = " Propiedad publica de tipo string que representa a la columna codcie de la Tabla clacie")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -codcie- porque es un campo requerido.")]
		[Key]
		public string codcie { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna desccie de la Tabla clacie
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "desccie", Description = " Propiedad publica de tipo string que representa a la columna desccie de la Tabla clacie")]
		public string desccie { get; set; } 

	}
}

