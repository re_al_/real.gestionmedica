#region 
/***********************************************************************************************************
	NOMBRE:       EntGasLibro
	DESCRIPCION:
		Clase que define un objeto para la Tabla gaslibro

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntGasLibro : CBaseClass
	{
		public const string StrNombreTabla = "GasLibro";
		public const string StrAliasTabla = "Gli";
		public enum Fields
		{
			idgli
			,nombregli
			,idgca
			,descripciongli
			,fechagli
			,tipogli
			,monto
			,idcci
			,apiestado
			,apitransaccion
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public EntGasLibro()
		{
			//Inicializacion de Variables
			nombregli = null;
			descripciongli = null;
			fechagli = null;
			tipogli = null;
			idcci = null;
			apiestado = null;
			apitransaccion = null;
			usucre = null;
			usumod = null;
			fecmod = null;
		}
		
		public EntGasLibro(EntGasLibro obj)
		{
			idgli = obj.idgli;
			nombregli = obj.nombregli;
			idgca = obj.idgca;
			descripciongli = obj.descripciongli;
			fechagli = obj.fechagli;
			tipogli = obj.tipogli;
			monto = obj.monto;
			idcci = obj.idcci;
			apiestado = obj.apiestado;
			apitransaccion = obj.apitransaccion;
			usucre = obj.usucre;
			feccre = obj.feccre;
			usumod = obj.usumod;
			fecmod = obj.fecmod;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntGasLibro.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna idgli de la Tabla gaslibro
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "idgli", Description = " Propiedad publica de tipo Int64 que representa a la columna idgli de la Tabla gaslibro")]
		[Required(ErrorMessage = "Se necesita un valor para -idgli- porque es un campo requerido.")]
		[Key]
		public Int64 idgli { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna nombregli de la Tabla gaslibro
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(150, MinimumLength=0)]
		[Display(Name = "nombregli", Description = " Propiedad publica de tipo string que representa a la columna nombregli de la Tabla gaslibro")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -nombregli- porque es un campo requerido.")]
		public string nombregli { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna idgca de la Tabla gaslibro
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "idgca", Description = " Propiedad publica de tipo int que representa a la columna idgca de la Tabla gaslibro")]
		[Required(ErrorMessage = "Se necesita un valor para -idgca- porque es un campo requerido.")]
		public int idgca { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna descripciongli de la Tabla gaslibro
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "descripciongli", Description = " Propiedad publica de tipo string que representa a la columna descripciongli de la Tabla gaslibro")]
		public string descripciongli { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fechagli de la Tabla gaslibro
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fechagli", Description = " Propiedad publica de tipo DateTime que representa a la columna fechagli de la Tabla gaslibro")]
		public DateTime? fechagli { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna tipogli de la Tabla gaslibro
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(1, MinimumLength=0)]
		[Display(Name = "tipogli", Description = " Propiedad publica de tipo string que representa a la columna tipogli de la Tabla gaslibro")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -tipogli- porque es un campo requerido.")]
		public string tipogli { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Decimal que representa a la columna monto de la Tabla gaslibro
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "monto", Description = " Propiedad publica de tipo Decimal que representa a la columna monto de la Tabla gaslibro")]
		[Required(ErrorMessage = "Se necesita un valor para -monto- porque es un campo requerido.")]
		public Decimal monto { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Decimal que representa a la columna idcci de la Tabla gaslibro
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "idcci", Description = " Propiedad publica de tipo Decimal que representa a la columna idcci de la Tabla gaslibro")]
		public Decimal? idcci { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestado de la Tabla gaslibro
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo string que representa a la columna apiestado de la Tabla gaslibro")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestado { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla gaslibro
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apitransaccion", Description = " Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla gaslibro")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucre de la Tabla gaslibro
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo string que representa a la columna usucre de la Tabla gaslibro")]
		public string usucre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla gaslibro
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla gaslibro")]
		public DateTime feccre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumod de la Tabla gaslibro
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo string que representa a la columna usumod de la Tabla gaslibro")]
		public string usumod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla gaslibro
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla gaslibro")]
		public DateTime? fecmod { get; set; } 

	}
}

