#region 
/***********************************************************************************************************
	NOMBRE:       EntCalCitas
	DESCRIPCION:
		Clase que define un objeto para la Tabla calcitas

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        11/04/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntCalCitas : CBaseClass
	{
		public const string StrNombreTabla = "CalCitas";
		public const string StrAliasTabla = "Cci";
		public enum Fields
		{
			idcci
			,idppa
			,asuntocci
			,descripcioncci
			,iniciocci
			,finalcci
			,lugarcci
			,statuscci
			,labelcci
			,motivofaltacci
			,apiestado
			,apitransaccion
			,usucre
			,feccre
			,usumod
			,fecmod
			,usuasignacion

		}
		
		#region Constructoress
		
		public EntCalCitas()
		{
			//Inicializacion de Variables
			idppa = null;
			asuntocci = null;
			descripcioncci = null;
			lugarcci = null;
			motivofaltacci = null;
			apiestado = null;
			apitransaccion = null;
			usucre = null;
			usumod = null;
			fecmod = null;
			usuasignacion = null;
		}
		
		public EntCalCitas(EntCalCitas obj)
		{
			idcci = obj.idcci;
			idppa = obj.idppa;
			asuntocci = obj.asuntocci;
			descripcioncci = obj.descripcioncci;
			iniciocci = obj.iniciocci;
			finalcci = obj.finalcci;
			lugarcci = obj.lugarcci;
			statuscci = obj.statuscci;
			labelcci = obj.labelcci;
			motivofaltacci = obj.motivofaltacci;
			apiestado = obj.apiestado;
			apitransaccion = obj.apitransaccion;
			usucre = obj.usucre;
			feccre = obj.feccre;
			usumod = obj.usumod;
			fecmod = obj.fecmod;
			usuasignacion = obj.usuasignacion;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntCalCitas.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna idcci de la Tabla calcitas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "idcci", Description = " Propiedad publica de tipo Int64 que representa a la columna idcci de la Tabla calcitas")]
		[Required(ErrorMessage = "Se necesita un valor para -idcci- porque es un campo requerido.")]
		[Key]
		public Int64 idcci { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla calcitas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "idppa", Description = " Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla calcitas")]
		public Int64? idppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna asuntocci de la Tabla calcitas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "asuntocci", Description = " Propiedad publica de tipo string que representa a la columna asuntocci de la Tabla calcitas")]
		public string asuntocci { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna descripcioncci de la Tabla calcitas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "descripcioncci", Description = " Propiedad publica de tipo string que representa a la columna descripcioncci de la Tabla calcitas")]
		public string descripcioncci { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna iniciocci de la Tabla calcitas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "iniciocci", Description = " Propiedad publica de tipo DateTime que representa a la columna iniciocci de la Tabla calcitas")]
		[Required(ErrorMessage = "Se necesita un valor para -iniciocci- porque es un campo requerido.")]
		public DateTime iniciocci { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna finalcci de la Tabla calcitas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "finalcci", Description = " Propiedad publica de tipo DateTime que representa a la columna finalcci de la Tabla calcitas")]
		[Required(ErrorMessage = "Se necesita un valor para -finalcci- porque es un campo requerido.")]
		public DateTime finalcci { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna lugarcci de la Tabla calcitas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "lugarcci", Description = " Propiedad publica de tipo string que representa a la columna lugarcci de la Tabla calcitas")]
		public string lugarcci { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna statuscci de la Tabla calcitas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "statuscci", Description = " Propiedad publica de tipo int que representa a la columna statuscci de la Tabla calcitas")]
		[Required(ErrorMessage = "Se necesita un valor para -statuscci- porque es un campo requerido.")]
		public int statuscci { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna labelcci de la Tabla calcitas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "labelcci", Description = " Propiedad publica de tipo int que representa a la columna labelcci de la Tabla calcitas")]
		[Required(ErrorMessage = "Se necesita un valor para -labelcci- porque es un campo requerido.")]
		public int labelcci { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna motivofaltacci de la Tabla calcitas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "motivofaltacci", Description = " Propiedad publica de tipo string que representa a la columna motivofaltacci de la Tabla calcitas")]
		public string motivofaltacci { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestado de la Tabla calcitas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo string que representa a la columna apiestado de la Tabla calcitas")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestado { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla calcitas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apitransaccion", Description = " Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla calcitas")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucre de la Tabla calcitas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo string que representa a la columna usucre de la Tabla calcitas")]
		public string usucre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla calcitas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla calcitas")]
		public DateTime feccre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumod de la Tabla calcitas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo string que representa a la columna usumod de la Tabla calcitas")]
		public string usumod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla calcitas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla calcitas")]
		public DateTime? fecmod { get; set; } 

		/// <summary>
		/// Usuario al que se le asignó la cita
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usuasignacion", Description = "Usuario al que se le asignó la cita")]
		public string usuasignacion { get; set; } 

	}
}

