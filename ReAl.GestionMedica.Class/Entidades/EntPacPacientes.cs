#region 
/***********************************************************************************************************
	NOMBRE:       EntPacPacientes
	DESCRIPCION:
		Clase que define un objeto para la Tabla pacpacientes

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        05/04/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntPacPacientes : CBaseClass
	{
		public const string StrNombreTabla = "PacPacientes";
		public const string StrAliasTabla = "Ppa";
		public enum Fields
		{
			idppa
			,nombresppa
			,appaternoppa
			,apmaternoppa
			,generoppa
			,fechanac
			,idcts
			,idcec
			,ocupacionppa
			,procedenciappa
			,domicilioppa
			,dominanciappa
			,telefonosppa
			,emailppa
			,religionppa
			,empresappa
			,seguroppa
			,remiteppa
			,informanteppa
			,apiestado
			,apitransaccion
			,usucre
			,feccre
			,usumod
			,fecmod
			, cippa

        }
		
		#region Constructoress
		
		public EntPacPacientes()
		{
			//Inicializacion de Variables
			nombresppa = null;
			appaternoppa = null;
			apmaternoppa = null;
			//generoppa = false;
			idcts = null;
			idcec = null;
			ocupacionppa = null;
			procedenciappa = null;
			domicilioppa = null;
			dominanciappa = null;
			telefonosppa = null;
			emailppa = null;
			religionppa = null;
			empresappa = null;
			seguroppa = null;
			remiteppa = null;
			informanteppa = null;
			apiestado = null;
			apitransaccion = null;
			usucre = null;
			usumod = null;
			fecmod = null;
		}
		
		public EntPacPacientes(EntPacPacientes obj)
		{
			idppa = obj.idppa;
			nombresppa = obj.nombresppa;
			appaternoppa = obj.appaternoppa;
			apmaternoppa = obj.apmaternoppa;
			generoppa = obj.generoppa;
			fechanac = obj.fechanac;
			idcts = obj.idcts;
			idcec = obj.idcec;
			ocupacionppa = obj.ocupacionppa;
			procedenciappa = obj.procedenciappa;
			domicilioppa = obj.domicilioppa;
			dominanciappa = obj.dominanciappa;
			telefonosppa = obj.telefonosppa;
			emailppa = obj.emailppa;
			religionppa = obj.religionppa;
			empresappa = obj.empresappa;
			seguroppa = obj.seguroppa;
			remiteppa = obj.remiteppa;
			informanteppa = obj.informanteppa;
			apiestado = obj.apiestado;
			apitransaccion = obj.apitransaccion;
			usucre = obj.usucre;
			feccre = obj.feccre;
			usumod = obj.usumod;
			fecmod = obj.fecmod;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntPacPacientes.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla pacpacientes
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "idppa", Description = " Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla pacpacientes")]
		[Required(ErrorMessage = "Se necesita un valor para -idppa- porque es un campo requerido.")]
		[Key]
		public Int64 idppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna nombresppa de la Tabla pacpacientes
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "nombresppa", Description = " Propiedad publica de tipo string que representa a la columna nombresppa de la Tabla pacpacientes")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -nombresppa- porque es un campo requerido.")]
		public string nombresppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna appaternoppa de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "appaternoppa", Description = " Propiedad publica de tipo string que representa a la columna appaternoppa de la Tabla pacpacientes")]
		public string appaternoppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apmaternoppa de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "apmaternoppa", Description = " Propiedad publica de tipo string que representa a la columna apmaternoppa de la Tabla pacpacientes")]
		public string apmaternoppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo bool que representa a la columna generoppa de la Tabla pacpacientes
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "generoppa", Description = " Propiedad publica de tipo bool que representa a la columna generoppa de la Tabla pacpacientes")]
		[Required(ErrorMessage = "Se necesita un valor para -generoppa- porque es un campo requerido.")]
		public bool generoppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fechanac de la Tabla pacpacientes
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fechanac", Description = " Propiedad publica de tipo DateTime que representa a la columna fechanac de la Tabla pacpacientes")]
		[Required(ErrorMessage = "Se necesita un valor para -fechanac- porque es un campo requerido.")]
		public DateTime fechanac { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna idcts de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "idcts", Description = " Propiedad publica de tipo int que representa a la columna idcts de la Tabla pacpacientes")]
		public int? idcts { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna idcec de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "idcec", Description = " Propiedad publica de tipo int que representa a la columna idcec de la Tabla pacpacientes")]
		public int? idcec { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna ocupacionppa de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(300, MinimumLength=0)]
		[Display(Name = "ocupacionppa", Description = " Propiedad publica de tipo string que representa a la columna ocupacionppa de la Tabla pacpacientes")]
		public string ocupacionppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna procedenciappa de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(300, MinimumLength=0)]
		[Display(Name = "procedenciappa", Description = " Propiedad publica de tipo string que representa a la columna procedenciappa de la Tabla pacpacientes")]
		public string procedenciappa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna domicilioppa de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "domicilioppa", Description = " Propiedad publica de tipo string que representa a la columna domicilioppa de la Tabla pacpacientes")]
		public string domicilioppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna dominanciappa de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(100, MinimumLength=0)]
		[Display(Name = "dominanciappa", Description = " Propiedad publica de tipo string que representa a la columna dominanciappa de la Tabla pacpacientes")]
		public string dominanciappa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna telefonosppa de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "telefonosppa", Description = " Propiedad publica de tipo string que representa a la columna telefonosppa de la Tabla pacpacientes")]
		public string telefonosppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna emailppa de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "emailppa", Description = " Propiedad publica de tipo string que representa a la columna emailppa de la Tabla pacpacientes")]
		public string emailppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna religionppa de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "religionppa", Description = " Propiedad publica de tipo string que representa a la columna religionppa de la Tabla pacpacientes")]
		public string religionppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna empresappa de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "empresappa", Description = " Propiedad publica de tipo string que representa a la columna empresappa de la Tabla pacpacientes")]
		public string empresappa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna seguroppa de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(200, MinimumLength=0)]
		[Display(Name = "seguroppa", Description = " Propiedad publica de tipo string que representa a la columna seguroppa de la Tabla pacpacientes")]
		public string seguroppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna remiteppa de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "remiteppa", Description = " Propiedad publica de tipo string que representa a la columna remiteppa de la Tabla pacpacientes")]
		public string remiteppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna informanteppa de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(500, MinimumLength=0)]
		[Display(Name = "informanteppa", Description = " Propiedad publica de tipo string que representa a la columna informanteppa de la Tabla pacpacientes")]
		public string informanteppa { get; set; }

        /// <summary>
        /// Propiedad publica de tipo string que representa a la columna cippa de la Tabla pacpacientes
        /// Permite Null: Yes
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [StringLength(10, MinimumLength = 0)]
        [Display(Name = "cippa", Description = " Propiedad publica de tipo string que representa a la columna cippa de la Tabla pacpacientes")]
        public string cippa { get; set; }

        /// <summary>
        /// Propiedad publica de tipo string que representa a la columna apiestado de la Tabla pacpacientes
        /// Permite Null: No
        /// Es Calculada: No
        /// Es RowGui: No
        /// Es PrimaryKey: No
        /// Es ForeignKey: No
        /// </summary>
        [StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo string que representa a la columna apiestado de la Tabla pacpacientes")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestado { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla pacpacientes
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apitransaccion", Description = " Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla pacpacientes")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucre de la Tabla pacpacientes
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo string que representa a la columna usucre de la Tabla pacpacientes")]
		public string usucre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla pacpacientes
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla pacpacientes")]
		public DateTime feccre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumod de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo string que representa a la columna usumod de la Tabla pacpacientes")]
		public string usumod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla pacpacientes
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla pacpacientes")]
		public DateTime? fecmod { get; set; } 

	}
}

