#region 
/***********************************************************************************************************
	NOMBRE:       EntSegTablas
	DESCRIPCION:
		Clase que define un objeto para la Tabla segtablas

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntSegTablas : CBaseClass
	{
		public const string StrNombreTabla = "SegTablas";
		public const string StrAliasTabla = "Sta";
		public enum Fields
		{
			tablasta
			,aplicacionsap
			,aliassta
			,descripcionsta
			,apiestadosta
			,apitransaccionsta
			,usucresta
			,feccresta
			,usumodsta
			,fecmodsta

		}
		
		#region Constructoress
		
		public EntSegTablas()
		{
			//Inicializacion de Variables
			tablasta = null;
			aplicacionsap = null;
			aliassta = null;
			descripcionsta = null;
			apiestadosta = null;
			apitransaccionsta = null;
			usucresta = null;
			usumodsta = null;
			fecmodsta = null;
		}
		
		public EntSegTablas(EntSegTablas obj)
		{
			tablasta = obj.tablasta;
			aplicacionsap = obj.aplicacionsap;
			aliassta = obj.aliassta;
			descripcionsta = obj.descripcionsta;
			apiestadosta = obj.apiestadosta;
			apitransaccionsta = obj.apitransaccionsta;
			usucresta = obj.usucresta;
			feccresta = obj.feccresta;
			usumodsta = obj.usumodsta;
			fecmodsta = obj.fecmodsta;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntSegTablas.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna tablasta de la Tabla segtablas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(30, MinimumLength=0)]
		[Display(Name = "tablasta", Description = " Propiedad publica de tipo string que representa a la columna tablasta de la Tabla segtablas")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -tablasta- porque es un campo requerido.")]
		[Key]
		public string tablasta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna aplicacionsap de la Tabla segtablas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[StringLength(3, MinimumLength=0)]
		[Display(Name = "aplicacionsap", Description = " Propiedad publica de tipo string que representa a la columna aplicacionsap de la Tabla segtablas")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -aplicacionsap- porque es un campo requerido.")]
		public string aplicacionsap { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna aliassta de la Tabla segtablas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(3, MinimumLength=0)]
		[Display(Name = "aliassta", Description = " Propiedad publica de tipo string que representa a la columna aliassta de la Tabla segtablas")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -aliassta- porque es un campo requerido.")]
		public string aliassta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna descripcionsta de la Tabla segtablas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(240, MinimumLength=0)]
		[Display(Name = "descripcionsta", Description = " Propiedad publica de tipo string que representa a la columna descripcionsta de la Tabla segtablas")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -descripcionsta- porque es un campo requerido.")]
		public string descripcionsta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestadosta de la Tabla segtablas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestadosta", Description = " Propiedad publica de tipo string que representa a la columna apiestadosta de la Tabla segtablas")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestadosta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccionsta de la Tabla segtablas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apitransaccionsta", Description = " Propiedad publica de tipo string que representa a la columna apitransaccionsta de la Tabla segtablas")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccionsta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucresta de la Tabla segtablas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucresta", Description = " Propiedad publica de tipo string que representa a la columna usucresta de la Tabla segtablas")]
		public string usucresta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccresta de la Tabla segtablas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccresta", Description = " Propiedad publica de tipo DateTime que representa a la columna feccresta de la Tabla segtablas")]
		public DateTime feccresta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumodsta de la Tabla segtablas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumodsta", Description = " Propiedad publica de tipo string que representa a la columna usumodsta de la Tabla segtablas")]
		public string usumodsta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmodsta de la Tabla segtablas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmodsta", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmodsta de la Tabla segtablas")]
		public DateTime? fecmodsta { get; set; } 

	}
}

