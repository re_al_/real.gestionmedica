#region 
/***********************************************************************************************************
	NOMBRE:       EntPacMultimedia
	DESCRIPCION:
		Clase que define un objeto para la Tabla pacmultimedia

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        11/04/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntPacMultimedia : CBaseClass
	{
		public const string StrNombreTabla = "PacMultimedia";
		public const string StrAliasTabla = "Pmu";
		public enum Fields
		{
			idpmu
			,idppa
			,fechapmu
			,observacionespmu
			,imagenpmu
			,extensionpmu
			,apiestado
			,apitransaccion
			,usucre
			,feccre
			,usumod
			,fecmod
			,pathpmu

		}
		
		#region Constructoress
		
		public EntPacMultimedia()
		{
			//Inicializacion de Variables
			idppa = null;
			fechapmu = null;
			observacionespmu = null;
			imagenpmu = null;
			extensionpmu = null;
			apiestado = null;
			apitransaccion = null;
			usucre = null;
			usumod = null;
			fecmod = null;
			pathpmu = null;
		}
		
		public EntPacMultimedia(EntPacMultimedia obj)
		{
			idpmu = obj.idpmu;
			idppa = obj.idppa;
			fechapmu = obj.fechapmu;
			observacionespmu = obj.observacionespmu;
			imagenpmu = obj.imagenpmu;
			extensionpmu = obj.extensionpmu;
			apiestado = obj.apiestado;
			apitransaccion = obj.apitransaccion;
			usucre = obj.usucre;
			feccre = obj.feccre;
			usumod = obj.usumod;
			fecmod = obj.fecmod;
			pathpmu = obj.pathpmu;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntPacMultimedia.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna idpmu de la Tabla pacmultimedia
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "idpmu", Description = " Propiedad publica de tipo Int64 que representa a la columna idpmu de la Tabla pacmultimedia")]
		[Required(ErrorMessage = "Se necesita un valor para -idpmu- porque es un campo requerido.")]
		[Key]
		public Int64 idpmu { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla pacmultimedia
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "idppa", Description = " Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla pacmultimedia")]
		public Int64? idppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fechapmu de la Tabla pacmultimedia
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fechapmu", Description = " Propiedad publica de tipo DateTime que representa a la columna fechapmu de la Tabla pacmultimedia")]
		public DateTime? fechapmu { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna observacionespmu de la Tabla pacmultimedia
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5000, MinimumLength=0)]
		[Display(Name = "observacionespmu", Description = " Propiedad publica de tipo string que representa a la columna observacionespmu de la Tabla pacmultimedia")]
		public string observacionespmu { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Byte[] que representa a la columna imagenpmu de la Tabla pacmultimedia
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "imagenpmu", Description = " Propiedad publica de tipo Byte[] que representa a la columna imagenpmu de la Tabla pacmultimedia")]
		public Byte[] imagenpmu { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna extensionpmu de la Tabla pacmultimedia
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(3, MinimumLength=0)]
		[Display(Name = "extensionpmu", Description = " Propiedad publica de tipo string que representa a la columna extensionpmu de la Tabla pacmultimedia")]
		public string extensionpmu { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestado de la Tabla pacmultimedia
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo string que representa a la columna apiestado de la Tabla pacmultimedia")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestado { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla pacmultimedia
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apitransaccion", Description = " Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla pacmultimedia")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucre de la Tabla pacmultimedia
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo string que representa a la columna usucre de la Tabla pacmultimedia")]
		public string usucre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla pacmultimedia
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla pacmultimedia")]
		public DateTime feccre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumod de la Tabla pacmultimedia
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo string que representa a la columna usumod de la Tabla pacmultimedia")]
		public string usumod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla pacmultimedia
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla pacmultimedia")]
		public DateTime? fecmod { get; set; } 

		/// <summary>
		/// Ruta donde se encuentra el documento
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(256, MinimumLength=0)]
		[Display(Name = "pathpmu", Description = "Ruta donde se encuentra el documento")]
		public string pathpmu { get; set; } 

	}
}

