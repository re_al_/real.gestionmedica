#region 
/***********************************************************************************************************
	NOMBRE:       EntSegTransiciones
	DESCRIPCION:
		Clase que define un objeto para la Tabla segtransiciones

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntSegTransiciones : CBaseClass
	{
		public const string StrNombreTabla = "SegTransiciones";
		public const string StrAliasTabla = "Sts";
		public enum Fields
		{
			tablasta
			,estadoinicialsts
			,transaccionstr
			,estadofinalsts
			,apiestadosts
			,apitransaccionsts
			,usucrests
			,feccrests
			,usumodsts
			,fecmodsts

		}
		
		#region Constructoress
		
		public EntSegTransiciones()
		{
			//Inicializacion de Variables
			tablasta = null;
			estadoinicialsts = null;
			transaccionstr = null;
			estadofinalsts = null;
			apiestadosts = null;
			apitransaccionsts = null;
			usucrests = null;
			usumodsts = null;
			fecmodsts = null;
		}
		
		public EntSegTransiciones(EntSegTransiciones obj)
		{
			tablasta = obj.tablasta;
			estadoinicialsts = obj.estadoinicialsts;
			transaccionstr = obj.transaccionstr;
			estadofinalsts = obj.estadofinalsts;
			apiestadosts = obj.apiestadosts;
			apitransaccionsts = obj.apitransaccionsts;
			usucrests = obj.usucrests;
			feccrests = obj.feccrests;
			usumodsts = obj.usumodsts;
			fecmodsts = obj.fecmodsts;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntSegTransiciones.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna tablasta de la Tabla segtransiciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: Yes
		/// </summary>
		[StringLength(30, MinimumLength=0)]
		[Display(Name = "tablasta", Description = " Propiedad publica de tipo string que representa a la columna tablasta de la Tabla segtransiciones")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -tablasta- porque es un campo requerido.")]
		[Key]
		public string tablasta { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna estadoinicialsts de la Tabla segtransiciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "estadoinicialsts", Description = " Propiedad publica de tipo string que representa a la columna estadoinicialsts de la Tabla segtransiciones")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -estadoinicialsts- porque es un campo requerido.")]
		[Key]
		public string estadoinicialsts { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna transaccionstr de la Tabla segtransiciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: Yes
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "transaccionstr", Description = " Propiedad publica de tipo string que representa a la columna transaccionstr de la Tabla segtransiciones")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -transaccionstr- porque es un campo requerido.")]
		[Key]
		public string transaccionstr { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna estadofinalsts de la Tabla segtransiciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "estadofinalsts", Description = " Propiedad publica de tipo string que representa a la columna estadofinalsts de la Tabla segtransiciones")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -estadofinalsts- porque es un campo requerido.")]
		[Key]
		public string estadofinalsts { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestadosts de la Tabla segtransiciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestadosts", Description = " Propiedad publica de tipo string que representa a la columna apiestadosts de la Tabla segtransiciones")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestadosts { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccionsts de la Tabla segtransiciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apitransaccionsts", Description = " Propiedad publica de tipo string que representa a la columna apitransaccionsts de la Tabla segtransiciones")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccionsts { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucrests de la Tabla segtransiciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucrests", Description = " Propiedad publica de tipo string que representa a la columna usucrests de la Tabla segtransiciones")]
		public string usucrests { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccrests de la Tabla segtransiciones
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccrests", Description = " Propiedad publica de tipo DateTime que representa a la columna feccrests de la Tabla segtransiciones")]
		public DateTime feccrests { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumodsts de la Tabla segtransiciones
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumodsts", Description = " Propiedad publica de tipo string que representa a la columna usumodsts de la Tabla segtransiciones")]
		public string usumodsts { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmodsts de la Tabla segtransiciones
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmodsts", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmodsts de la Tabla segtransiciones")]
		public DateTime? fecmodsts { get; set; } 

	}
}

