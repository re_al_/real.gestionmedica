#region 
/***********************************************************************************************************
	NOMBRE:       EntSegParametrosdet
	DESCRIPCION:
		Clase que define un objeto para la Tabla segparametrosdet

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntSegParametrosdet : CBaseClass
	{
		public const string StrNombreTabla = "SegParametrosdet";
		public const string StrAliasTabla = "Spd";
		public enum Fields
		{
			siglaspa
			,codigospd
			,parint1spd
			,parint2spd
			,parchar1spd
			,parchar2spd
			,parnum1spd
			,parnum2spd
			,parbit1spd
			,parbit2spd
			,apiestadospd
			,apitransaccionspd
			,usucrespd
			,feccrespd
			,usumodspd
			,fecmodspd

		}
		
		#region Constructoress
		
		public EntSegParametrosdet()
		{
			//Inicializacion de Variables
			siglaspa = null;
			parint1spd = null;
			parint2spd = null;
			parchar1spd = null;
			parchar2spd = null;
			parnum1spd = null;
			parnum2spd = null;
			parbit1spd = null;
			parbit2spd = null;
			apiestadospd = null;
			apitransaccionspd = null;
			usucrespd = null;
			usumodspd = null;
			fecmodspd = null;
		}
		
		public EntSegParametrosdet(EntSegParametrosdet obj)
		{
			siglaspa = obj.siglaspa;
			codigospd = obj.codigospd;
			parint1spd = obj.parint1spd;
			parint2spd = obj.parint2spd;
			parchar1spd = obj.parchar1spd;
			parchar2spd = obj.parchar2spd;
			parnum1spd = obj.parnum1spd;
			parnum2spd = obj.parnum2spd;
			parbit1spd = obj.parbit1spd;
			parbit2spd = obj.parbit2spd;
			apiestadospd = obj.apiestadospd;
			apitransaccionspd = obj.apitransaccionspd;
			usucrespd = obj.usucrespd;
			feccrespd = obj.feccrespd;
			usumodspd = obj.usumodspd;
			fecmodspd = obj.fecmodspd;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntSegParametrosdet.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna siglaspa de la Tabla segparametrosdet
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: Yes
		/// </summary>
		[StringLength(3, MinimumLength=0)]
		[Display(Name = "siglaspa", Description = " Propiedad publica de tipo string que representa a la columna siglaspa de la Tabla segparametrosdet")]
		[Required(AllowEmptyStrings = true, ErrorMessage = "Se necesita un valor para -siglaspa- porque es un campo requerido.")]
		[Key]
		public string siglaspa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo int que representa a la columna codigospd de la Tabla segparametrosdet
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Range(0, int.MaxValue, ErrorMessage = "Por favor ingresa un valor numérico válido")]
		[Display(Name = "codigospd", Description = " Propiedad publica de tipo int que representa a la columna codigospd de la Tabla segparametrosdet")]
		[Required(ErrorMessage = "Se necesita un valor para -codigospd- porque es un campo requerido.")]
		[Key]
		public int codigospd { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Decimal que representa a la columna parint1spd de la Tabla segparametrosdet
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "parint1spd", Description = " Propiedad publica de tipo Decimal que representa a la columna parint1spd de la Tabla segparametrosdet")]
		public Decimal? parint1spd { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Decimal que representa a la columna parint2spd de la Tabla segparametrosdet
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "parint2spd", Description = " Propiedad publica de tipo Decimal que representa a la columna parint2spd de la Tabla segparametrosdet")]
		public Decimal? parint2spd { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna parchar1spd de la Tabla segparametrosdet
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "parchar1spd", Description = " Propiedad publica de tipo string que representa a la columna parchar1spd de la Tabla segparametrosdet")]
		public string parchar1spd { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna parchar2spd de la Tabla segparametrosdet
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(180, MinimumLength=0)]
		[Display(Name = "parchar2spd", Description = " Propiedad publica de tipo string que representa a la columna parchar2spd de la Tabla segparametrosdet")]
		public string parchar2spd { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Decimal que representa a la columna parnum1spd de la Tabla segparametrosdet
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "parnum1spd", Description = " Propiedad publica de tipo Decimal que representa a la columna parnum1spd de la Tabla segparametrosdet")]
		public Decimal? parnum1spd { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Decimal que representa a la columna parnum2spd de la Tabla segparametrosdet
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "parnum2spd", Description = " Propiedad publica de tipo Decimal que representa a la columna parnum2spd de la Tabla segparametrosdet")]
		public Decimal? parnum2spd { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Decimal que representa a la columna parbit1spd de la Tabla segparametrosdet
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "parbit1spd", Description = " Propiedad publica de tipo Decimal que representa a la columna parbit1spd de la Tabla segparametrosdet")]
		public Decimal? parbit1spd { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Decimal que representa a la columna parbit2spd de la Tabla segparametrosdet
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "parbit2spd", Description = " Propiedad publica de tipo Decimal que representa a la columna parbit2spd de la Tabla segparametrosdet")]
		public Decimal? parbit2spd { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestadospd de la Tabla segparametrosdet
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apiestadospd", Description = " Propiedad publica de tipo string que representa a la columna apiestadospd de la Tabla segparametrosdet")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestadospd { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccionspd de la Tabla segparametrosdet
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "apitransaccionspd", Description = " Propiedad publica de tipo string que representa a la columna apitransaccionspd de la Tabla segparametrosdet")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccionspd { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucrespd de la Tabla segparametrosdet
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usucrespd", Description = " Propiedad publica de tipo string que representa a la columna usucrespd de la Tabla segparametrosdet")]
		public string usucrespd { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccrespd de la Tabla segparametrosdet
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccrespd", Description = " Propiedad publica de tipo DateTime que representa a la columna feccrespd de la Tabla segparametrosdet")]
		public DateTime feccrespd { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumodspd de la Tabla segparametrosdet
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(15, MinimumLength=0)]
		[Display(Name = "usumodspd", Description = " Propiedad publica de tipo string que representa a la columna usumodspd de la Tabla segparametrosdet")]
		public string usumodspd { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmodspd de la Tabla segparametrosdet
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmodspd", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmodspd de la Tabla segparametrosdet")]
		public DateTime? fecmodspd { get; set; } 

	}
}

