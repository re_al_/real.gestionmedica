#region 
/***********************************************************************************************************
	NOMBRE:       EntPacConsultas
	DESCRIPCION:
		Clase que define un objeto para la Tabla pacconsultas

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion


#region
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;
using System.Runtime.Serialization;
#endregion

namespace ReAl.GestionMedica.Class.Entidades
{
	public class EntPacConsultas : CBaseClass
	{
		public const string StrNombreTabla = "PacConsultas";
		public const string StrAliasTabla = "Pco";
		public enum Fields
		{
			idpco
			,idppa
			,fechapco
			,horapco
			,cuadroclinicopco
			,diagnosticopco
			,idcie
			,conductapco
			,tratamientopco
			,apiestado
			,apitransaccion
			,usucre
			,feccre
			,usumod
			,fecmod

		}
		
		#region Constructoress
		
		public EntPacConsultas()
		{
			//Inicializacion de Variables
			idppa = null;
			fechapco = null;
			horapco = null;
			cuadroclinicopco = null;
			diagnosticopco = null;
			idcie = null;
			conductapco = null;
			tratamientopco = null;
			apiestado = null;
			apitransaccion = null;
			usucre = null;
			usumod = null;
			fecmod = null;
		}
		
		public EntPacConsultas(EntPacConsultas obj)
		{
			idpco = obj.idpco;
			idppa = obj.idppa;
			fechapco = obj.fechapco;
			horapco = obj.horapco;
			cuadroclinicopco = obj.cuadroclinicopco;
			diagnosticopco = obj.diagnosticopco;
			idcie = obj.idcie;
			conductapco = obj.conductapco;
			tratamientopco = obj.tratamientopco;
			apiestado = obj.apiestado;
			apitransaccion = obj.apitransaccion;
			usucre = obj.usucre;
			feccre = obj.feccre;
			usumod = obj.usumod;
			fecmod = obj.fecmod;
		}
		
		#endregion
		
		#region Metodos Estaticos
		/// <summary>
		/// Funcion para obtener un objeto para el API y sus datos
		/// </summary>
		/// <returns>Objeto de tipo CApiObject con los datos para enviar al API</returns>
		public CApiObject CreateApiObject()
		{
			var objApi = new CApiObject();
			objApi.Nombre = EntPacConsultas.StrNombreTabla;
			objApi.Datos = this;
			return objApi;
		}
		
		#endregion
		
		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna idpco de la Tabla pacconsultas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: Yes
		/// Es ForeignKey: No
		/// </summary>
		[Display(Name = "idpco", Description = " Propiedad publica de tipo Int64 que representa a la columna idpco de la Tabla pacconsultas")]
		[Required(ErrorMessage = "Se necesita un valor para -idpco- porque es un campo requerido.")]
		[Key]
		public Int64 idpco { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla pacconsultas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: Yes
		/// </summary>
		[Display(Name = "idppa", Description = " Propiedad publica de tipo Int64 que representa a la columna idppa de la Tabla pacconsultas")]
		public Int64? idppa { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fechapco de la Tabla pacconsultas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fechapco", Description = " Propiedad publica de tipo DateTime que representa a la columna fechapco de la Tabla pacconsultas")]
		public DateTime? fechapco { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna horapco de la Tabla pacconsultas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "horapco", Description = " Propiedad publica de tipo DateTime que representa a la columna horapco de la Tabla pacconsultas")]
		public DateTime? horapco { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna cuadroclinicopco de la Tabla pacconsultas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "cuadroclinicopco", Description = " Propiedad publica de tipo string que representa a la columna cuadroclinicopco de la Tabla pacconsultas")]
		public string cuadroclinicopco { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna diagnosticopco de la Tabla pacconsultas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "diagnosticopco", Description = " Propiedad publica de tipo string que representa a la columna diagnosticopco de la Tabla pacconsultas")]
		public string diagnosticopco { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna idcie de la Tabla pacconsultas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(5, MinimumLength=0)]
		[Display(Name = "idcie", Description = " Propiedad publica de tipo string que representa a la columna idcie de la Tabla pacconsultas")]
		public string idcie { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna conductapco de la Tabla pacconsultas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "conductapco", Description = " Propiedad publica de tipo string que representa a la columna conductapco de la Tabla pacconsultas")]
		public string conductapco { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna tratamientopco de la Tabla pacconsultas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(2000, MinimumLength=0)]
		[Display(Name = "tratamientopco", Description = " Propiedad publica de tipo string que representa a la columna tratamientopco de la Tabla pacconsultas")]
		public string tratamientopco { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apiestado de la Tabla pacconsultas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apiestado", Description = " Propiedad publica de tipo string que representa a la columna apiestado de la Tabla pacconsultas")]
		[EnumDataType(typeof(CApi.Estado))]
		public string apiestado { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla pacconsultas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "apitransaccion", Description = " Propiedad publica de tipo string que representa a la columna apitransaccion de la Tabla pacconsultas")]
		[EnumDataType(typeof(CApi.Transaccion))]
		public string apitransaccion { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usucre de la Tabla pacconsultas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usucre", Description = " Propiedad publica de tipo string que representa a la columna usucre de la Tabla pacconsultas")]
		public string usucre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla pacconsultas
		/// Permite Null: No
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "feccre", Description = " Propiedad publica de tipo DateTime que representa a la columna feccre de la Tabla pacconsultas")]
		public DateTime feccre { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo string que representa a la columna usumod de la Tabla pacconsultas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[StringLength(60, MinimumLength=0)]
		[Display(Name = "usumod", Description = " Propiedad publica de tipo string que representa a la columna usumod de la Tabla pacconsultas")]
		public string usumod { get; set; } 

		/// <summary>
		/// Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla pacconsultas
		/// Permite Null: Yes
		/// Es Calculada: No
		/// Es RowGui: No
		/// Es PrimaryKey: No
		/// Es ForeignKey: No
		/// </summary>
		[DataType(DataType.DateTime, ErrorMessage = "Fecha invalida")]
		[DisplayFormat(DataFormatString = "{ 0:dd/MM/yyyy HH:mm:ss.ffffff}", ApplyFormatInEditMode = true)]
		[Display(Name = "fecmod", Description = " Propiedad publica de tipo DateTime que representa a la columna fecmod de la Tabla pacconsultas")]
		public DateTime? fecmod { get; set; } 

	}
}

