#region
/***********************************************************************************************************
	NOMBRE:       RnSegParametros
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla segparametros

	REVISIONES:
		Ver        FECHA       Autor            Descripcion
		---------  ----------  ---------------  ------------------------------------
		1.0        22/04/2021  R Alonzo Vera A  Creacion

*************************************************************************************************************/
#endregion

#region

using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Interface;
using System.Windows.Forms;
using System.Web.UI.WebControls;

#endregion

namespace ReAl.GestionMedica.Class.Modelo
{
    public class RnSegParametros : ISegParametros
    {
        //Debe implementar la Interface (Alt + Shift + F10)

        #region ISegParametros Members

        #region Reflection

        /// <summary>
        /// Metodo que devuelve el Script SQL de la Tabla
        /// </summary>
        /// <returns>Script SQL</returns>
        public string GetTableScript()
        {
            TableClass tabla = new TableClass(typeof(EntSegParametros));
            return tabla.CreateTableScript();
        }

        /// <summary>
        /// Metodo para castear Dinamicamente un Tipo
        /// </summary>
        /// <param name="valor">Tipo a ser casteado</param>
        /// <param name="myField">Enum de la columna</param>
        /// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
        public dynamic GetColumnType(object valor, EntSegParametros.Fields myField)
        {
            if (DBNull.Value.Equals(valor))
                return null;
            Type destino = typeof(EntSegParametros).GetProperty(myField.ToString()).PropertyType;
            var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;

            try
            {
                TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
                return tc.ConvertFrom(valor);
            }
            catch (Exception)
            {
                return Convert.ChangeType(valor, miTipo);
            }
        }

        /// <summary>
        /// Metodo para castear Dinamicamente un Tipo
        /// </summary>
        /// <param name="valor">Tipo a ser casteado</param>
        /// <param name="strField">Nombre de la columna</param>
        /// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
        public dynamic GetColumnType(object valor, string strField)
        {
            if (DBNull.Value.Equals(valor))
                return null;
            Type destino = typeof(EntSegParametros).GetProperty(strField).PropertyType;
            var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;

            try
            {
                TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
                return tc.ConvertFrom(valor);
            }
            catch (Exception)
            {
                return Convert.ChangeType(valor, miTipo);
            }
        }

        /// <summary>
        /// Inserta una valor a una propiedad de un objeto instanciado
        /// </summary>
        /// <param name="obj">Objeto instanciado</param>
        /// <param name="strPropiedad">Es el nombre de la propiedad</param>
        /// <param name="dynValor">Es el valor que se insertara a la propiedad</param>
        public void SetDato(ref EntSegParametros obj, string strPropiedad, dynamic dynValor)
        {
            if (obj == null) throw new ArgumentNullException();
            obj.GetType().GetProperty(strPropiedad).SetValue(obj, GetColumnType(dynValor, strPropiedad), null);
        }

        /// <summary>
        /// Obtiene el valor de una propiedad de un objeto instanciado
        /// </summary>
        /// <param name="obj">Objeto instanciado</param>
        /// <param name="strPropiedad">El nombre de la propiedad de la que se obtendra el valor</param>
        /// <returns>Devuelve el valor del a propiedad seleccionada</returns>
        public dynamic GetDato(ref EntSegParametros obj, string strPropiedad)
        {
            if (obj == null) return null;
            var propertyInfo = obj.GetType().GetProperty(strPropiedad);
            return GetColumnType(propertyInfo.GetValue(obj, null), strPropiedad);
        }

        /// <summary>
        /// 	 Funcion que obtiene la llave primaria unica de la tabla segparametros a partir de una cadena
        /// </summary>
        /// <param name="args" type="string[]">
        ///     <para>
        /// 		 Cadena desde la que se construye el identificador unico de la tabla segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Identificador unico de la tabla segparametros
        /// </returns>
        public string CreatePk(string[] args)
        {
            return args[0];
        }

        #endregion

        #region ObtenerObjeto

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametros a partir de la llave primaria
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametros ObtenerObjeto(string stringsiglaspa)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(EntSegParametros.Fields.siglaspa.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'" + stringsiglaspa + "'");

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametros a partir del usuario que inserta
        /// </summary>
        /// <param name="strUsuCre">Login o nombre de usuario</param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametros ObtenerObjetoInsertado(string strUsuCre)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(EntSegParametros.Fields.usucrespa.ToString());
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'" + strUsuCre + "'");

            int iIdInsertado = FuncionesMax(EntSegParametros.Fields.siglaspa, arrColumnasWhere, arrValoresWhere);

            return ObtenerObjeto(iIdInsertado.ToString());
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametros ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametros ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count == 1)
                {
                    EntSegParametros obj = new EntSegParametros();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametros ObtenerObjeto(Hashtable htbFiltro)
        {
            return ObtenerObjeto(htbFiltro, "");
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametros ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count == 1)
                {
                    EntSegParametros obj = new EntSegParametros();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametros ObtenerObjeto(EntSegParametros.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametros ObtenerObjeto(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un Business Object del Tipo EntSegParametros a partir de su llave promaria
        /// </summary>
        /// <returns>
        /// 	Objeto del Tipo EntSegParametros
        /// </returns>
        public EntSegParametros ObtenerObjeto(string stringsiglaspa, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(EntSegParametros.Fields.siglaspa.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'" + stringsiglaspa + "'");
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametros ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametros ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans)
        {
            return ObtenerObjeto(htbFiltro, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametros ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametros ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count == 1)
                {
                    EntSegParametros obj = new EntSegParametros();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametros ObtenerObjeto(EntSegParametros.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametros ObtenerObjeto(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        #endregion

        #region ObtenerLista

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerLista()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerLista(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntSegParametros>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerLista(EntSegParametros.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerLista(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntSegParametros>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerLista(EntSegParametros.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerLista(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerLista(Hashtable htbFiltro)
        {
            return ObtenerLista(htbFiltro, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntSegParametros>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans)
        {
            return ObtenerLista(htbFiltro, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntSegParametros>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerListaDesdeVista(String strVista)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntSegParametros>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
        {
            return ObtenerListaDesdeVista(strVista, htbFiltro, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntSegParametros>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerListaDesdeVista(String strVista, EntSegParametros.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerListaDesdeVista(String strVista, EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntSegParametros>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans)
        {
            return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntSegParametros>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerListaDesdeVista(String strVista, EntSegParametros.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametros> ObtenerListaDesdeVista(String strVista, EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        #endregion

        #region ObtenerCola y Obtener Pila

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametros> ObtenerCola()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerCola(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametros> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametros> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearCola(table);
                }
                else
                    return new Queue<EntSegParametros>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametros> ObtenerCola(EntSegParametros.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametros> ObtenerCola(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametros> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametros> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearCola(table);
                }
                else
                    return new Queue<EntSegParametros>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametros> ObtenerCola(EntSegParametros.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametros> ObtenerCola(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametros> ObtenerPila()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerPila(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametros> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametros> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearPila(table);
                }
                else
                    return new Stack<EntSegParametros>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametros> ObtenerPila(EntSegParametros.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametros> ObtenerPila(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametros> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametros> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearPila(table);
                }
                else
                    return new Stack<EntSegParametros>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametros> ObtenerPila(EntSegParametros.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametros> ObtenerPila(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        #endregion

        #region ObtenerDataTable

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla segparametros
        /// </summary>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable NuevoDataTable()
        {
            try
            {
                DataTable table = new DataTable();
                DataColumn dc;
                dc = new DataColumn(EntSegParametros.Fields.siglaspa.ToString(), typeof(EntSegParametros).GetProperty(EntSegParametros.Fields.siglaspa.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametros.Fields.descripcionspa.ToString(), typeof(EntSegParametros).GetProperty(EntSegParametros.Fields.descripcionspa.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametros.Fields.apiestadospa.ToString(), typeof(EntSegParametros).GetProperty(EntSegParametros.Fields.apiestadospa.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametros.Fields.apitransaccionspa.ToString(), typeof(EntSegParametros).GetProperty(EntSegParametros.Fields.apitransaccionspa.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametros.Fields.usucrespa.ToString(), typeof(EntSegParametros).GetProperty(EntSegParametros.Fields.usucrespa.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametros.Fields.feccrespa.ToString(), typeof(EntSegParametros).GetProperty(EntSegParametros.Fields.feccrespa.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametros.Fields.usumodspa.ToString(), typeof(EntSegParametros).GetProperty(EntSegParametros.Fields.usumodspa.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametros.Fields.fecmodspa.ToString(), typeof(EntSegParametros).GetProperty(EntSegParametros.Fields.fecmodspa.ToString()).PropertyType);
                table.Columns.Add(dc);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que genera un DataTable con determinadas columnas de una segparametros
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable NuevoDataTable(ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'2'");

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla segparametros
        /// </summary>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable ObtenerDataTable()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE segparametros
        /// </summary>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable ObtenerDataTable(String strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametros
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametros
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametros
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametros
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
        {
            try
            {
                return ObtenerDataTable(arrColumnas, htbFiltro, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametros
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametros
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable ObtenerDataTable(Hashtable htbFiltro)
        {
            try
            {
                return ObtenerDataTable(htbFiltro, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametros
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametros
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametros
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametros
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametros
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());
                return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametros
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametros
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametros
        /// </returns>
        public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableOr(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(EntSegParametros.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(EntSegParametros.Fields searchField, object searchValue, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegParametros.Fields searchField, object searchValue, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegParametros.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region ObtenerDiccionario

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametros> ObtenerDiccionario()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametros> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametros> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, EntSegParametros>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametros> ObtenerDiccionario(EntSegParametros.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametros> ObtenerDiccionario(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametros> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametros> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, EntSegParametros>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametros> ObtenerDiccionario(EntSegParametros.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametros a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametros> ObtenerDiccionario(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        public Dictionary<String, EntSegParametros> ObtenerDiccionarioKey(EntSegParametros.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
        }

        public Dictionary<String, EntSegParametros> ObtenerDiccionarioKey(String strParamAdic, EntSegParametros.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdic, dicKey);
        }

        public Dictionary<String, EntSegParametros> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, EntSegParametros.Fields dicKey)
        {
            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, "", dicKey);
        }

        public Dictionary<String, EntSegParametros> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, EntSegParametros.Fields dicKey)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table, dicKey);
                }
                else
                    return new Dictionary<string, EntSegParametros>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public Dictionary<String, EntSegParametros> ObtenerDiccionarioKey(EntSegParametros.Fields searchField, object searchValue, EntSegParametros.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
        }

        public Dictionary<String, EntSegParametros> ObtenerDiccionarioKey(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales, EntSegParametros.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey);
        }

        #endregion

        #region ObjetoASp

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="strNombreSp" type="System.string">
        ///     <para>
        /// 		 Nombre del Procedimiento a ejecutar sobre el SP
        ///     </para>
        /// </param>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se va a ejecutar el SP de la tabla segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor de registros afectados en el Procedimiento de la tabla segparametros
        /// </returns>
        public int EjecutarSpDesdeObjeto(string strNombreSp, EntSegParametros obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrNombreParam = new ArrayList();
                arrNombreParam.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrNombreParam.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrNombreParam.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrNombreParam.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrNombreParam.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrNombreParam.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrNombreParam.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrNombreParam.Add(EntSegParametros.Fields.fecmodspa.ToString());

                ArrayList arrValoresParam = new ArrayList();
                arrValoresParam.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");
                arrValoresParam.Add(obj.descripcionspa == null ? null : "'" + obj.descripcionspa + "'");
                arrValoresParam.Add(obj.apiestadospa == null ? null : "'" + obj.apiestadospa + "'");
                arrValoresParam.Add(obj.apitransaccionspa == null ? null : "'" + obj.apitransaccionspa + "'");
                arrValoresParam.Add(obj.usucrespa == null ? null : "'" + obj.usucrespa + "'");
                arrValoresParam.Add(obj.feccrespa == null ? null : "'" + Convert.ToDateTime(obj.feccrespa).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValoresParam.Add(obj.usumodspa == null ? null : "'" + obj.usumodspa + "'");
                arrValoresParam.Add(obj.fecmodspa == null ? null : "'" + Convert.ToDateTime(obj.fecmodspa).ToString(CParametros.ParFormatoFechaHora) + "'");

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="strNombreSp" type="System.string">
        ///     <para>
        /// 		 Nombre del Procedimiento a ejecutar sobre el SP
        ///     </para>
        /// </param>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se va a ejecutar el SP de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor de registros afectados en el Procedimiento de la tabla segparametros
        /// </returns>
        public int EjecutarSpDesdeObjeto(string strNombreSp, EntSegParametros obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrNombreParam = new ArrayList();
                arrNombreParam.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrNombreParam.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrNombreParam.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrNombreParam.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrNombreParam.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrNombreParam.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrNombreParam.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrNombreParam.Add(EntSegParametros.Fields.fecmodspa.ToString());

                ArrayList arrValoresParam = new ArrayList();
                arrValoresParam.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");
                arrValoresParam.Add(obj.descripcionspa == null ? null : "'" + obj.descripcionspa + "'");
                arrValoresParam.Add(obj.apiestadospa == null ? null : "'" + obj.apiestadospa + "'");
                arrValoresParam.Add(obj.apitransaccionspa == null ? null : "'" + obj.apitransaccionspa + "'");
                arrValoresParam.Add(obj.usucrespa == null ? null : "'" + obj.usucrespa + "'");
                arrValoresParam.Add(obj.feccrespa == null ? null : "'" + Convert.ToDateTime(obj.feccrespa).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValoresParam.Add(obj.usumodspa == null ? null : "'" + obj.usumodspa + "'");
                arrValoresParam.Add(obj.fecmodspa == null ? null : "'" + Convert.ToDateTime(obj.fecmodspa).ToString(CParametros.ParFormatoFechaHora) + "'");

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region FuncionesAgregadas

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesCount(EntSegParametros.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesCount(EntSegParametros.Fields refField, EntSegParametros.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesCount(EntSegParametros.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("count(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMin(EntSegParametros.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMin(EntSegParametros.Fields refField, EntSegParametros.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMin(EntSegParametros.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("min(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMax(EntSegParametros.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMax(EntSegParametros.Fields refField, EntSegParametros.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMax(EntSegParametros.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("max(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesSum(EntSegParametros.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesSum(EntSegParametros.Fields refField, EntSegParametros.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesSum(EntSegParametros.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("sum(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesAvg(EntSegParametros.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesAvg(EntSegParametros.Fields refField, EntSegParametros.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametros que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesAvg(EntSegParametros.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("avg(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region ABMs SP

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametros
        /// </returns>
        public bool Insert(EntSegParametros obj, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add("siglaspa");
                arrValoresParam.Add(null);
                arrNombreParam.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrValoresParam.Add(obj.descripcionspa);

                arrNombreParam.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrValoresParam.Add(obj.usucrespa);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegParametros.SpSpaIns.ToString();
                return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam) > 0);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametros
        /// </returns>
        public bool Insert(EntSegParametros obj, ref CTrans localTrans, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add("siglaspa");
                arrValoresParam.Add("");
                arrNombreParam.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrValoresParam.Add(obj.descripcionspa);

                arrNombreParam.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrValoresParam.Add(obj.usucrespa);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegParametros.SpSpaIns.ToString();
                return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans) > 0);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor que indica la cantidad de registros actualizados en segparametros
        /// </returns>
        public int Update(EntSegParametros obj, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrValoresParam.Add(obj.siglaspa);

                arrNombreParam.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrValoresParam.Add(obj.descripcionspa);

                arrNombreParam.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrValoresParam.Add(obj.apitransaccionspa);

                arrNombreParam.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrValoresParam.Add(obj.usumodspa);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegParametros.SpSpaUpd.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametros
        /// </returns>
        public int Update(EntSegParametros obj, ref CTrans localTrans, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrValoresParam.Add(obj.siglaspa);

                arrNombreParam.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrValoresParam.Add(obj.descripcionspa);

                arrNombreParam.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrValoresParam.Add(obj.apitransaccionspa);

                arrNombreParam.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrValoresParam.Add(obj.usumodspa);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegParametros.SpSpaUpd.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametros
        /// </returns>
        public int Delete(EntSegParametros obj, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrValoresParam.Add(obj.siglaspa);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegParametros.SpSpaDel.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametros
        /// </returns>
        public int Delete(EntSegParametros obj, ref CTrans localTrans, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrValoresParam.Add(obj.siglaspa);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegParametros.SpSpaDel.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametros
        /// </returns>
        public int InsertUpdate(EntSegParametros obj)
        {
            try
            {
                bool esInsertar = true;

                esInsertar = (esInsertar && (obj.siglaspa == null));

                if (esInsertar)
                    return Insert(obj) ? 1 : 0;
                else
                    return Update(obj);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametros
        /// </returns>
        public int InsertUpdate(EntSegParametros obj, ref CTrans localTrans)
        {
            try
            {
                bool esInsertar = false;

                esInsertar = (esInsertar && (obj.siglaspa == null));

                if (esInsertar)
                    return Insert(obj, ref localTrans) ? 1 : 0;
                else
                    return Update(obj, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region ABMs Query

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametros
        /// </returns>
        public bool InsertQuery(EntSegParametros obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                //arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");
                arrValores.Add(obj.descripcionspa == null ? null : "'" + obj.descripcionspa + "'");
                arrValores.Add(obj.apiestadospa == null ? null : "'" + obj.apiestadospa + "'");
                arrValores.Add(obj.usucrespa == null ? null : "'" + obj.usucrespa + "'");

                CConn local = new CConn();
                return local.InsertBd(EntSegParametros.StrNombreTabla, arrColumnas, arrValores);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametros
        /// </returns>
        public bool InsertQuery(EntSegParametros obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                //arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");
                arrValores.Add(obj.descripcionspa == null ? null : "'" + obj.descripcionspa + "'");
                arrValores.Add(obj.apiestadospa == null ? null : "'" + obj.apiestadospa + "'");
                arrValores.Add(obj.usucrespa == null ? null : "'" + obj.usucrespa + "'");

                CConn local = new CConn();
                return local.InsertBd(EntSegParametros.StrNombreTabla, arrColumnas, arrValores, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametros
        /// </returns>
        public bool InsertQueryIdentity(EntSegParametros obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametros
        /// </returns>
        public bool InsertQueryIdentity(EntSegParametros obj, ref CTrans localTrans)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionsegparametros
        /// </returns>
        public int UpdateQueryAll(EntSegParametros obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.descripcionspa == null ? null : "'" + obj.descripcionspa + "'");
                arrValores.Add(obj.apiestadospa == null ? null : "'" + obj.apiestadospa + "'");
                arrValores.Add(obj.apitransaccionspa == null ? null : "'" + obj.apitransaccionspa + "'");
                arrValores.Add(obj.usumodspa == null ? null : "'" + obj.usumodspa + "'");

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegParametros.Fields.siglaspa.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntSegParametros.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla segparametros a partir de una clase del tipo esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacion
        /// </returns>
        public int UpdateQueryAll(EntSegParametros obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.descripcionspa == null ? null : "'" + obj.descripcionspa + "'");
                arrValores.Add(obj.apiestadospa == null ? null : "'" + obj.apiestadospa + "'");
                arrValores.Add(obj.apitransaccionspa == null ? null : "'" + obj.apitransaccionspa + "'");
                arrValores.Add(obj.usumodspa == null ? null : "'" + obj.usumodspa + "'");

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegParametros.Fields.siglaspa.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntSegParametros.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla segparametros a partir de una clase del tipo Esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionsegparametros
        /// </returns>
        public int UpdateQuery(EntSegParametros obj)
        {
            try
            {
                //Obtenemos el Objeto original
                EntSegParametros objOriginal = this.ObtenerObjeto(obj.siglaspa);

                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                ArrayList arrValores = new ArrayList();

                if (obj.descripcionspa != objOriginal.descripcionspa)
                {
                    arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                    arrValores.Add(obj.descripcionspa == null ? null : "'" + obj.descripcionspa + "'");
                }
                if (obj.apiestadospa != objOriginal.apiestadospa)
                {
                    arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                    arrValores.Add(obj.apiestadospa == null ? null : "'" + obj.apiestadospa + "'");
                }
                if (obj.apitransaccionspa != objOriginal.apitransaccionspa)
                {
                    arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                    arrValores.Add(obj.apitransaccionspa == null ? null : "'" + obj.apitransaccionspa + "'");
                }
                if (obj.usumodspa != objOriginal.usumodspa)
                {
                    arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                    arrValores.Add(obj.usumodspa == null ? null : "'" + obj.usumodspa + "'");
                }

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegParametros.Fields.siglaspa.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntSegParametros.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla segparametros a partir de una clase del tipo esegparametros
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacion
        /// </returns>
        public int UpdateQuery(EntSegParametros obj, ref CTrans localTrans)
        {
            try
            {
                //Obtenemos el Objeto original
                EntSegParametros objOriginal = this.ObtenerObjeto(obj.siglaspa, ref localTrans);

                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                ArrayList arrValores = new ArrayList();

                if (obj.descripcionspa != objOriginal.descripcionspa)
                {
                    arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                    arrValores.Add(obj.descripcionspa == null ? null : "'" + obj.descripcionspa + "'");
                }
                if (obj.apiestadospa != objOriginal.apiestadospa)
                {
                    arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                    arrValores.Add(obj.apiestadospa == null ? null : "'" + obj.apiestadospa + "'");
                }
                if (obj.apitransaccionspa != objOriginal.apitransaccionspa)
                {
                    arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                    arrValores.Add(obj.apitransaccionspa == null ? null : "'" + obj.apitransaccionspa + "'");
                }
                if (obj.usumodspa != objOriginal.usumodspa)
                {
                    arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                    arrValores.Add(obj.usumodspa == null ? null : "'" + obj.usumodspa + "'");
                }

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegParametros.Fields.siglaspa.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntSegParametros.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla segparametros a partir de una clase del tipo EntSegParametros y su respectiva PK
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametros">
        ///     <para>
        /// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionsegparametros
        /// </returns>
        public int DeleteQuery(EntSegParametros obj)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegParametros.Fields.siglaspa.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");

                CConn local = new CConn();
                return local.DeleteBd(EntSegParametros.StrNombreTabla, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla segparametros a partir de una clase del tipo EntSegParametros y su PK
        /// </summary>
        /// <param name="obj" type="Entidades.esegparametros">
        ///     <para>
        /// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacionsegparametros
        /// </returns>
        public int DeleteQuery(EntSegParametros obj, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegParametros.Fields.siglaspa.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");

                CConn local = new CConn();
                return local.DeleteBd(EntSegParametros.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla segparametros a partir de una clase del tipo esegparametros
        /// </summary>
        /// <param name="arrColumnasWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Valores para cada una de las columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionsegparametros
        /// </returns>
        public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CConn local = new CConn();
                return local.DeleteBd("segparametros", arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla segparametros a partir de una clase del tipo esegparametros
        /// </summary>
        /// <param name="arrColumnasWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Valores para cada una de las columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametros
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacionsegparametros
        /// </returns>
        public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            try
            {
                CConn local = new CConn();
                return local.DeleteBd(EntSegParametros.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region Llenado de elementos

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField)
        {
            try
            {
                CargarDropDownList(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, String textField)
        {
            try
            {
                CargarDropDownList(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, String textField, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, EntSegParametros.Fields searchField, object searchValue)
        {
            try
            {
                CargarDropDownList(ref cmb, valueField, textField.ToString(), searchField, searchValue);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, String textField, EntSegParametros.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, String textField, EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Columns.Count > 0)
                {
                    cmb.DataValueField = table.Columns[0].ColumnName;
                    cmb.DataTextField = table.Columns[1].ColumnName;
                    cmb.DataSource = table;
                    cmb.DataBind();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                dtg.DataSource = dsReader;
                dtg.DataBind();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, EntSegParametros.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, EntSegParametros.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegParametros.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegParametros.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarGridViewOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
                dtg.DataBind();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, String textField)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, String textField, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, EntSegParametros.Fields searchField, object searchValue)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, String textField, EntSegParametros.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, String textField, EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametros
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametros
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                    cmb.DataSource = table;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, EntSegParametros.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, EntSegParametros.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametros.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.descripcionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apiestadospa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.apitransaccionspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usucrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.feccrespa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.usumodspa.ToString());
                arrColumnas.Add(EntSegParametros.Fields.fecmodspa.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegParametros.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametros.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegParametros.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametros
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntSegParametros.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #endregion

        #region Funciones Internas

        /// <summary>
        /// 	 Funcion que devuelve un objeto a partir de un DataRow
        /// </summary>
        /// <param name="row" type="System.Data.DataRow">
        ///     <para>
        /// 		 DataRow con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Objeto segparametros
        /// </returns>
        internal EntSegParametros crearObjeto(DataRow row)
        {
            var obj = new EntSegParametros();
            obj.siglaspa = GetColumnType(row[EntSegParametros.Fields.siglaspa.ToString()], EntSegParametros.Fields.siglaspa);
            obj.descripcionspa = GetColumnType(row[EntSegParametros.Fields.descripcionspa.ToString()], EntSegParametros.Fields.descripcionspa);
            obj.apiestadospa = GetColumnType(row[EntSegParametros.Fields.apiestadospa.ToString()], EntSegParametros.Fields.apiestadospa);
            obj.apitransaccionspa = GetColumnType(row[EntSegParametros.Fields.apitransaccionspa.ToString()], EntSegParametros.Fields.apitransaccionspa);
            obj.usucrespa = GetColumnType(row[EntSegParametros.Fields.usucrespa.ToString()], EntSegParametros.Fields.usucrespa);
            obj.feccrespa = GetColumnType(row[EntSegParametros.Fields.feccrespa.ToString()], EntSegParametros.Fields.feccrespa);
            obj.usumodspa = GetColumnType(row[EntSegParametros.Fields.usumodspa.ToString()], EntSegParametros.Fields.usumodspa);
            obj.fecmodspa = GetColumnType(row[EntSegParametros.Fields.fecmodspa.ToString()], EntSegParametros.Fields.fecmodspa);
            return obj;
        }

        /// <summary>
        /// 	 Funcion que devuelve un objeto a partir de un DataRow
        /// </summary>
        /// <param name="row" type="System.Data.DataRow">
        ///     <para>
        /// 		 DataRow con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Objeto segparametros
        /// </returns>
        internal EntSegParametros crearObjetoRevisado(DataRow row)
        {
            var obj = new EntSegParametros();
            if (row.Table.Columns.Contains(EntSegParametros.Fields.siglaspa.ToString()))
                obj.siglaspa = GetColumnType(row[EntSegParametros.Fields.siglaspa.ToString()], EntSegParametros.Fields.siglaspa);
            if (row.Table.Columns.Contains(EntSegParametros.Fields.descripcionspa.ToString()))
                obj.descripcionspa = GetColumnType(row[EntSegParametros.Fields.descripcionspa.ToString()], EntSegParametros.Fields.descripcionspa);
            if (row.Table.Columns.Contains(EntSegParametros.Fields.apiestadospa.ToString()))
                obj.apiestadospa = GetColumnType(row[EntSegParametros.Fields.apiestadospa.ToString()], EntSegParametros.Fields.apiestadospa);
            if (row.Table.Columns.Contains(EntSegParametros.Fields.apitransaccionspa.ToString()))
                obj.apitransaccionspa = GetColumnType(row[EntSegParametros.Fields.apitransaccionspa.ToString()], EntSegParametros.Fields.apitransaccionspa);
            if (row.Table.Columns.Contains(EntSegParametros.Fields.usucrespa.ToString()))
                obj.usucrespa = GetColumnType(row[EntSegParametros.Fields.usucrespa.ToString()], EntSegParametros.Fields.usucrespa);
            if (row.Table.Columns.Contains(EntSegParametros.Fields.feccrespa.ToString()))
                obj.feccrespa = GetColumnType(row[EntSegParametros.Fields.feccrespa.ToString()], EntSegParametros.Fields.feccrespa);
            if (row.Table.Columns.Contains(EntSegParametros.Fields.usumodspa.ToString()))
                obj.usumodspa = GetColumnType(row[EntSegParametros.Fields.usumodspa.ToString()], EntSegParametros.Fields.usumodspa);
            if (row.Table.Columns.Contains(EntSegParametros.Fields.fecmodspa.ToString()))
                obj.fecmodspa = GetColumnType(row[EntSegParametros.Fields.fecmodspa.ToString()], EntSegParametros.Fields.fecmodspa);
            return obj;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtsegparametros" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Lista de Objetos segparametros
        /// </returns>
        internal List<EntSegParametros> crearLista(DataTable dtsegparametros)
        {
            var list = new List<EntSegParametros>();

            foreach (DataRow row in dtsegparametros.Rows)
            {
                var obj = crearObjeto(row);
                list.Add(obj);
            }
            return list;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
        /// </summary>
        /// <param name="dtsegparametros" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Lista de Objetos segparametros
        /// </returns>
        internal List<EntSegParametros> crearListaRevisada(DataTable dtsegparametros)
        {
            List<EntSegParametros> list = new List<EntSegParametros>();

            foreach (DataRow row in dtsegparametros.Rows)
            {
                var obj = crearObjetoRevisado(row);
                list.Add(obj);
            }
            return list;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtsegparametros" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cola de Objetos segparametros
        /// </returns>
        internal Queue<EntSegParametros> crearCola(DataTable dtsegparametros)
        {
            Queue<EntSegParametros> cola = new Queue<EntSegParametros>();

            EntSegParametros obj = new EntSegParametros();
            foreach (DataRow row in dtsegparametros.Rows)
            {
                obj = crearObjeto(row);
                cola.Enqueue(obj);
            }
            return cola;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtsegparametros" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Pila de Objetos segparametros
        /// </returns>
        internal Stack<EntSegParametros> crearPila(DataTable dtsegparametros)
        {
            Stack<EntSegParametros> pila = new Stack<EntSegParametros>();

            foreach (DataRow row in dtsegparametros.Rows)
            {
                var obj = crearObjeto(row);
                pila.Push(obj);
            }
            return pila;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable
        /// </summary>
        /// <param name="dtsegparametros" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Diccionario de Objetos segparametros
        /// </returns>
        internal Dictionary<String, EntSegParametros> crearDiccionario(DataTable dtsegparametros)
        {
            Dictionary<String, EntSegParametros> miDic = new Dictionary<String, EntSegParametros>();

            foreach (DataRow row in dtsegparametros.Rows)
            {
                var obj = crearObjeto(row);
                miDic.Add(obj.siglaspa.ToString(), obj);
            }
            return miDic;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable
        /// </summary>
        /// <param name="dtsegparametros" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 HashTable de Objetos segparametros
        /// </returns>
        internal Hashtable crearHashTable(DataTable dtsegparametros)
        {
            Hashtable miTabla = new Hashtable();

            foreach (DataRow row in dtsegparametros.Rows)
            {
                var obj = crearObjeto(row);
                miTabla.Add(obj.siglaspa.ToString(), obj);
            }
            return miTabla;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
        /// </summary>
        /// <param name="dtsegparametros" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Diccionario de Objetos segparametros
        /// </returns>
        internal Dictionary<String, EntSegParametros> crearDiccionarioRevisado(DataTable dtsegparametros)
        {
            Dictionary<String, EntSegParametros> miDic = new Dictionary<String, EntSegParametros>();

            foreach (DataRow row in dtsegparametros.Rows)
            {
                var obj = crearObjetoRevisado(row);
                miDic.Add(obj.siglaspa.ToString(), obj);
            }
            return miDic;
        }

        internal Dictionary<String, EntSegParametros> crearDiccionario(DataTable dtsegparametros, EntSegParametros.Fields dicKey)
        {
            Dictionary<String, EntSegParametros> miDic = new Dictionary<String, EntSegParametros>();

            foreach (DataRow row in dtsegparametros.Rows)
            {
                var obj = crearObjeto(row);

                var nameOfProperty = dicKey.ToString();
                var propertyInfo = obj.GetType().GetProperty(nameOfProperty);
                var value = propertyInfo.GetValue(obj, null);

                miDic.Add(value.ToString(), obj);
            }
            return miDic;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        protected void Finalize()
        {
            Dispose();
        }

        #endregion
    }
}