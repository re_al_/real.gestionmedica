#region
/***********************************************************************************************************
	NOMBRE:       RnSegTransacciones
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla segtransacciones

	REVISIONES:
		Ver        FECHA       Autor            Descripcion
		---------  ----------  ---------------  ------------------------------------
		1.0        22/04/2021  R Alonzo Vera A  Creacion

*************************************************************************************************************/
#endregion

#region

using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Interface;
using System.Windows.Forms;
using System.Web.UI.WebControls;

#endregion

namespace ReAl.GestionMedica.Class.Modelo
{
    public class RnSegTransacciones : ISegTransacciones
    {
        //Debe implementar la Interface (Alt + Shift + F10)

        #region ISegTransacciones Members

        #region Reflection

        /// <summary>
        /// Metodo que devuelve el Script SQL de la Tabla
        /// </summary>
        /// <returns>Script SQL</returns>
        public string GetTableScript()
        {
            TableClass tabla = new TableClass(typeof(EntSegTransacciones));
            return tabla.CreateTableScript();
        }

        /// <summary>
        /// Metodo para castear Dinamicamente un Tipo
        /// </summary>
        /// <param name="valor">Tipo a ser casteado</param>
        /// <param name="myField">Enum de la columna</param>
        /// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
        public dynamic GetColumnType(object valor, EntSegTransacciones.Fields myField)
        {
            if (DBNull.Value.Equals(valor))
                return null;
            Type destino = typeof(EntSegTransacciones).GetProperty(myField.ToString()).PropertyType;
            var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;

            try
            {
                TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
                return tc.ConvertFrom(valor);
            }
            catch (Exception)
            {
                return Convert.ChangeType(valor, miTipo);
            }
        }

        /// <summary>
        /// Metodo para castear Dinamicamente un Tipo
        /// </summary>
        /// <param name="valor">Tipo a ser casteado</param>
        /// <param name="strField">Nombre de la columna</param>
        /// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
        public dynamic GetColumnType(object valor, string strField)
        {
            if (DBNull.Value.Equals(valor))
                return null;
            Type destino = typeof(EntSegTransacciones).GetProperty(strField).PropertyType;
            var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;

            try
            {
                TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
                return tc.ConvertFrom(valor);
            }
            catch (Exception)
            {
                return Convert.ChangeType(valor, miTipo);
            }
        }

        /// <summary>
        /// Inserta una valor a una propiedad de un objeto instanciado
        /// </summary>
        /// <param name="obj">Objeto instanciado</param>
        /// <param name="strPropiedad">Es el nombre de la propiedad</param>
        /// <param name="dynValor">Es el valor que se insertara a la propiedad</param>
        public void SetDato(ref EntSegTransacciones obj, string strPropiedad, dynamic dynValor)
        {
            if (obj == null) throw new ArgumentNullException();
            obj.GetType().GetProperty(strPropiedad).SetValue(obj, GetColumnType(dynValor, strPropiedad), null);
        }

        /// <summary>
        /// Obtiene el valor de una propiedad de un objeto instanciado
        /// </summary>
        /// <param name="obj">Objeto instanciado</param>
        /// <param name="strPropiedad">El nombre de la propiedad de la que se obtendra el valor</param>
        /// <returns>Devuelve el valor del a propiedad seleccionada</returns>
        public dynamic GetDato(ref EntSegTransacciones obj, string strPropiedad)
        {
            if (obj == null) return null;
            var propertyInfo = obj.GetType().GetProperty(strPropiedad);
            return GetColumnType(propertyInfo.GetValue(obj, null), strPropiedad);
        }

        /// <summary>
        /// 	 Funcion que obtiene la llave primaria unica de la tabla segtransacciones a partir de una cadena
        /// </summary>
        /// <param name="args" type="string[]">
        ///     <para>
        /// 		 Cadena desde la que se construye el identificador unico de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Identificador unico de la tabla segtransacciones
        /// </returns>
        public string CreatePk(string[] args)
        {
            return args[0];
        }

        #endregion

        #region ObtenerObjeto

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegTransacciones a partir de la llave primaria
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public EntSegTransacciones ObtenerObjeto(string stringtablasta, string stringtransaccionstr)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(EntSegTransacciones.Fields.tablasta.ToString());
            arrColumnasWhere.Add(EntSegTransacciones.Fields.transaccionstr.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'" + stringtablasta + "'");
            arrValoresWhere.Add("'" + stringtransaccionstr + "'");

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegTransacciones a partir del usuario que inserta
        /// </summary>
        /// <param name="strUsuCre">Login o nombre de usuario</param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public EntSegTransacciones ObtenerObjetoInsertado(string strUsuCre)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public EntSegTransacciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public EntSegTransacciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count == 1)
                {
                    EntSegTransacciones obj = new EntSegTransacciones();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public EntSegTransacciones ObtenerObjeto(Hashtable htbFiltro)
        {
            return ObtenerObjeto(htbFiltro, "");
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public EntSegTransacciones ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count == 1)
                {
                    EntSegTransacciones obj = new EntSegTransacciones();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public EntSegTransacciones ObtenerObjeto(EntSegTransacciones.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public EntSegTransacciones ObtenerObjeto(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un Business Object del Tipo EntSegTransacciones a partir de su llave promaria
        /// </summary>
        /// <returns>
        /// 	Objeto del Tipo EntSegTransacciones
        /// </returns>
        public EntSegTransacciones ObtenerObjeto(string stringtablasta, string stringtransaccionstr, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(EntSegTransacciones.Fields.tablasta.ToString());
            arrColumnasWhere.Add(EntSegTransacciones.Fields.transaccionstr.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'" + stringtablasta + "'");
            arrValoresWhere.Add("'" + stringtransaccionstr + "'");
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public EntSegTransacciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public EntSegTransacciones ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans)
        {
            return ObtenerObjeto(htbFiltro, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public EntSegTransacciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public EntSegTransacciones ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count == 1)
                {
                    EntSegTransacciones obj = new EntSegTransacciones();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public EntSegTransacciones ObtenerObjeto(EntSegTransacciones.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public EntSegTransacciones ObtenerObjeto(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        #endregion

        #region ObtenerLista

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerLista()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerLista(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntSegTransacciones>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerLista(EntSegTransacciones.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerLista(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntSegTransacciones>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerLista(EntSegTransacciones.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerLista(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerLista(Hashtable htbFiltro)
        {
            return ObtenerLista(htbFiltro, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntSegTransacciones>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans)
        {
            return ObtenerLista(htbFiltro, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntSegTransacciones>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntSegTransacciones>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
        {
            return ObtenerListaDesdeVista(strVista, htbFiltro, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntSegTransacciones>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, EntSegTransacciones.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntSegTransacciones>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans)
        {
            return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntSegTransacciones>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, EntSegTransacciones.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        #endregion

        #region ObtenerCola y Obtener Pila

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegTransacciones> ObtenerCola()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerCola(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegTransacciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegTransacciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearCola(table);
                }
                else
                    return new Queue<EntSegTransacciones>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegTransacciones> ObtenerCola(EntSegTransacciones.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegTransacciones> ObtenerCola(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegTransacciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegTransacciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearCola(table);
                }
                else
                    return new Queue<EntSegTransacciones>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegTransacciones> ObtenerCola(EntSegTransacciones.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegTransacciones> ObtenerCola(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegTransacciones> ObtenerPila()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerPila(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegTransacciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegTransacciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearPila(table);
                }
                else
                    return new Stack<EntSegTransacciones>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegTransacciones> ObtenerPila(EntSegTransacciones.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegTransacciones> ObtenerPila(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegTransacciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegTransacciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearPila(table);
                }
                else
                    return new Stack<EntSegTransacciones>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegTransacciones> ObtenerPila(EntSegTransacciones.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegTransacciones> ObtenerPila(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        #endregion

        #region ObtenerDataTable

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla segtransacciones
        /// </summary>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable NuevoDataTable()
        {
            try
            {
                DataTable table = new DataTable();
                DataColumn dc;
                dc = new DataColumn(EntSegTransacciones.Fields.tablasta.ToString(), typeof(EntSegTransacciones).GetProperty(EntSegTransacciones.Fields.tablasta.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegTransacciones.Fields.transaccionstr.ToString(), typeof(EntSegTransacciones).GetProperty(EntSegTransacciones.Fields.transaccionstr.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegTransacciones.Fields.descripcionstr.ToString(), typeof(EntSegTransacciones).GetProperty(EntSegTransacciones.Fields.descripcionstr.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegTransacciones.Fields.sentenciastr.ToString(), typeof(EntSegTransacciones).GetProperty(EntSegTransacciones.Fields.sentenciastr.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegTransacciones.Fields.apiestadostr.ToString(), typeof(EntSegTransacciones).GetProperty(EntSegTransacciones.Fields.apiestadostr.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegTransacciones.Fields.apitransaccionstr.ToString(), typeof(EntSegTransacciones).GetProperty(EntSegTransacciones.Fields.apitransaccionstr.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegTransacciones.Fields.usucrestr.ToString(), typeof(EntSegTransacciones).GetProperty(EntSegTransacciones.Fields.usucrestr.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegTransacciones.Fields.feccrestr.ToString(), typeof(EntSegTransacciones).GetProperty(EntSegTransacciones.Fields.feccrestr.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegTransacciones.Fields.usumodstr.ToString(), typeof(EntSegTransacciones).GetProperty(EntSegTransacciones.Fields.usumodstr.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegTransacciones.Fields.fecmodstr.ToString(), typeof(EntSegTransacciones).GetProperty(EntSegTransacciones.Fields.fecmodstr.ToString()).PropertyType);
                table.Columns.Add(dc);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que genera un DataTable con determinadas columnas de una segtransacciones
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable NuevoDataTable(ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'2'");

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla segtransacciones
        /// </summary>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable ObtenerDataTable()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE segtransacciones
        /// </summary>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable ObtenerDataTable(String strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
        {
            try
            {
                return ObtenerDataTable(arrColumnas, htbFiltro, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable ObtenerDataTable(Hashtable htbFiltro)
        {
            try
            {
                return ObtenerDataTable(htbFiltro, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());
                return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segtransacciones
        /// </returns>
        public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableOr(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(EntSegTransacciones.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(EntSegTransacciones.Fields searchField, object searchValue, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegTransacciones.Fields searchField, object searchValue, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegTransacciones.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region ObtenerDiccionario

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegTransacciones> ObtenerDiccionario()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegTransacciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegTransacciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, EntSegTransacciones>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegTransacciones> ObtenerDiccionario(EntSegTransacciones.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegTransacciones> ObtenerDiccionario(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegTransacciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegTransacciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, EntSegTransacciones>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegTransacciones> ObtenerDiccionario(EntSegTransacciones.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegTransacciones a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegTransacciones> ObtenerDiccionario(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        public Dictionary<String, EntSegTransacciones> ObtenerDiccionarioKey(EntSegTransacciones.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
        }

        public Dictionary<String, EntSegTransacciones> ObtenerDiccionarioKey(String strParamAdic, EntSegTransacciones.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdic, dicKey);
        }

        public Dictionary<String, EntSegTransacciones> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, EntSegTransacciones.Fields dicKey)
        {
            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, "", dicKey);
        }

        public Dictionary<String, EntSegTransacciones> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, EntSegTransacciones.Fields dicKey)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table, dicKey);
                }
                else
                    return new Dictionary<string, EntSegTransacciones>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public Dictionary<String, EntSegTransacciones> ObtenerDiccionarioKey(EntSegTransacciones.Fields searchField, object searchValue, EntSegTransacciones.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
        }

        public Dictionary<String, EntSegTransacciones> ObtenerDiccionarioKey(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales, EntSegTransacciones.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey);
        }

        #endregion

        #region ObjetoASp

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="strNombreSp" type="System.string">
        ///     <para>
        /// 		 Nombre del Procedimiento a ejecutar sobre el SP
        ///     </para>
        /// </param>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se va a ejecutar el SP de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor de registros afectados en el Procedimiento de la tabla segtransacciones
        /// </returns>
        public int EjecutarSpDesdeObjeto(string strNombreSp, EntSegTransacciones obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrNombreParam = new ArrayList();
                arrNombreParam.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                ArrayList arrValoresParam = new ArrayList();
                arrValoresParam.Add(obj.tablasta == null ? null : "'" + obj.tablasta + "'");
                arrValoresParam.Add(obj.transaccionstr == null ? null : "'" + obj.transaccionstr + "'");
                arrValoresParam.Add(obj.descripcionstr == null ? null : "'" + obj.descripcionstr + "'");
                arrValoresParam.Add(obj.sentenciastr == null ? null : "'" + obj.sentenciastr + "'");
                arrValoresParam.Add(obj.apiestadostr == null ? null : "'" + obj.apiestadostr + "'");
                arrValoresParam.Add(obj.apitransaccionstr == null ? null : "'" + obj.apitransaccionstr + "'");
                arrValoresParam.Add(obj.usucrestr == null ? null : "'" + obj.usucrestr + "'");
                arrValoresParam.Add(obj.feccrestr == null ? null : "'" + Convert.ToDateTime(obj.feccrestr).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValoresParam.Add(obj.usumodstr == null ? null : "'" + obj.usumodstr + "'");
                arrValoresParam.Add(obj.fecmodstr == null ? null : "'" + Convert.ToDateTime(obj.fecmodstr).ToString(CParametros.ParFormatoFechaHora) + "'");

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="strNombreSp" type="System.string">
        ///     <para>
        /// 		 Nombre del Procedimiento a ejecutar sobre el SP
        ///     </para>
        /// </param>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se va a ejecutar el SP de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor de registros afectados en el Procedimiento de la tabla segtransacciones
        /// </returns>
        public int EjecutarSpDesdeObjeto(string strNombreSp, EntSegTransacciones obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrNombreParam = new ArrayList();
                arrNombreParam.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrNombreParam.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                ArrayList arrValoresParam = new ArrayList();
                arrValoresParam.Add(obj.tablasta == null ? null : "'" + obj.tablasta + "'");
                arrValoresParam.Add(obj.transaccionstr == null ? null : "'" + obj.transaccionstr + "'");
                arrValoresParam.Add(obj.descripcionstr == null ? null : "'" + obj.descripcionstr + "'");
                arrValoresParam.Add(obj.sentenciastr == null ? null : "'" + obj.sentenciastr + "'");
                arrValoresParam.Add(obj.apiestadostr == null ? null : "'" + obj.apiestadostr + "'");
                arrValoresParam.Add(obj.apitransaccionstr == null ? null : "'" + obj.apitransaccionstr + "'");
                arrValoresParam.Add(obj.usucrestr == null ? null : "'" + obj.usucrestr + "'");
                arrValoresParam.Add(obj.feccrestr == null ? null : "'" + Convert.ToDateTime(obj.feccrestr).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValoresParam.Add(obj.usumodstr == null ? null : "'" + obj.usumodstr + "'");
                arrValoresParam.Add(obj.fecmodstr == null ? null : "'" + Convert.ToDateTime(obj.fecmodstr).ToString(CParametros.ParFormatoFechaHora) + "'");

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region FuncionesAgregadas

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesCount(EntSegTransacciones.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesCount(EntSegTransacciones.Fields refField, EntSegTransacciones.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesCount(EntSegTransacciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("count(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMin(EntSegTransacciones.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMin(EntSegTransacciones.Fields refField, EntSegTransacciones.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMin(EntSegTransacciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("min(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMax(EntSegTransacciones.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMax(EntSegTransacciones.Fields refField, EntSegTransacciones.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMax(EntSegTransacciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("max(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesSum(EntSegTransacciones.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesSum(EntSegTransacciones.Fields refField, EntSegTransacciones.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesSum(EntSegTransacciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("sum(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesAvg(EntSegTransacciones.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesAvg(EntSegTransacciones.Fields refField, EntSegTransacciones.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegTransacciones que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesAvg(EntSegTransacciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("avg(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region ABMs SP

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegtransacciones
        /// </returns>
        public bool Insert(EntSegTransacciones obj, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add("tablasta");
                arrValoresParam.Add(null);
                arrNombreParam.Add("transaccionstr");
                arrValoresParam.Add(null);
                arrNombreParam.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrValoresParam.Add(obj.descripcionstr);

                arrNombreParam.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrValoresParam.Add(obj.sentenciastr);

                arrNombreParam.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrValoresParam.Add(obj.usucrestr);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegTransacciones.SpStrIns.ToString();
                return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam) > 0);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegtransacciones
        /// </returns>
        public bool Insert(EntSegTransacciones obj, ref CTrans localTrans, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add("tablasta");
                arrValoresParam.Add("");
                arrNombreParam.Add("transaccionstr");
                arrValoresParam.Add("");
                arrNombreParam.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrValoresParam.Add(obj.descripcionstr);

                arrNombreParam.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrValoresParam.Add(obj.sentenciastr);

                arrNombreParam.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrValoresParam.Add(obj.usucrestr);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegTransacciones.SpStrIns.ToString();
                return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans) > 0);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor que indica la cantidad de registros actualizados en segtransacciones
        /// </returns>
        public int Update(EntSegTransacciones obj, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrValoresParam.Add(obj.tablasta);

                arrNombreParam.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrValoresParam.Add(obj.transaccionstr);

                arrNombreParam.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrValoresParam.Add(obj.descripcionstr);

                arrNombreParam.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrValoresParam.Add(obj.sentenciastr);

                arrNombreParam.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrValoresParam.Add(obj.apitransaccionstr);

                arrNombreParam.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrValoresParam.Add(obj.usumodstr);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegTransacciones.SpStrUpd.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegtransacciones
        /// </returns>
        public int Update(EntSegTransacciones obj, ref CTrans localTrans, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrValoresParam.Add(obj.tablasta);

                arrNombreParam.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrValoresParam.Add(obj.transaccionstr);

                arrNombreParam.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrValoresParam.Add(obj.descripcionstr);

                arrNombreParam.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrValoresParam.Add(obj.sentenciastr);

                arrNombreParam.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrValoresParam.Add(obj.apitransaccionstr);

                arrNombreParam.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrValoresParam.Add(obj.usumodstr);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegTransacciones.SpStrUpd.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegtransacciones
        /// </returns>
        public int Delete(EntSegTransacciones obj, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrValoresParam.Add(obj.tablasta);

                arrNombreParam.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrValoresParam.Add(obj.transaccionstr);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegTransacciones.SpStrDel.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegtransacciones
        /// </returns>
        public int Delete(EntSegTransacciones obj, ref CTrans localTrans, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrValoresParam.Add(obj.tablasta);

                arrNombreParam.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrValoresParam.Add(obj.transaccionstr);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegTransacciones.SpStrDel.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegtransacciones
        /// </returns>
        public int InsertUpdate(EntSegTransacciones obj)
        {
            try
            {
                bool esInsertar = true;

                esInsertar = (esInsertar && (obj.tablasta == null));
                esInsertar = (esInsertar && (obj.transaccionstr == null));

                if (esInsertar)
                    return Insert(obj) ? 1 : 0;
                else
                    return Update(obj);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegtransacciones
        /// </returns>
        public int InsertUpdate(EntSegTransacciones obj, ref CTrans localTrans)
        {
            try
            {
                bool esInsertar = false;

                esInsertar = (esInsertar && (obj.tablasta == null));
                esInsertar = (esInsertar && (obj.transaccionstr == null));

                if (esInsertar)
                    return Insert(obj, ref localTrans) ? 1 : 0;
                else
                    return Update(obj, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region ABMs Query

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegtransacciones
        /// </returns>
        public bool InsertQuery(EntSegTransacciones obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                //arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                //arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.tablasta == null ? null : "'" + obj.tablasta + "'");
                arrValores.Add(obj.transaccionstr == null ? null : "'" + obj.transaccionstr + "'");
                arrValores.Add(obj.descripcionstr == null ? null : "'" + obj.descripcionstr + "'");
                arrValores.Add(obj.sentenciastr == null ? null : "'" + obj.sentenciastr + "'");
                arrValores.Add(obj.apiestadostr == null ? null : "'" + obj.apiestadostr + "'");
                arrValores.Add(obj.usucrestr == null ? null : "'" + obj.usucrestr + "'");

                CConn local = new CConn();
                return local.InsertBd(EntSegTransacciones.StrNombreTabla, arrColumnas, arrValores);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegtransacciones
        /// </returns>
        public bool InsertQuery(EntSegTransacciones obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                //arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                //arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.tablasta == null ? null : "'" + obj.tablasta + "'");
                arrValores.Add(obj.transaccionstr == null ? null : "'" + obj.transaccionstr + "'");
                arrValores.Add(obj.descripcionstr == null ? null : "'" + obj.descripcionstr + "'");
                arrValores.Add(obj.sentenciastr == null ? null : "'" + obj.sentenciastr + "'");
                arrValores.Add(obj.apiestadostr == null ? null : "'" + obj.apiestadostr + "'");
                arrValores.Add(obj.usucrestr == null ? null : "'" + obj.usucrestr + "'");

                CConn local = new CConn();
                return local.InsertBd(EntSegTransacciones.StrNombreTabla, arrColumnas, arrValores, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegtransacciones
        /// </returns>
        public bool InsertQueryIdentity(EntSegTransacciones obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegtransacciones
        /// </returns>
        public bool InsertQueryIdentity(EntSegTransacciones obj, ref CTrans localTrans)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionsegtransacciones
        /// </returns>
        public int UpdateQueryAll(EntSegTransacciones obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.descripcionstr == null ? null : "'" + obj.descripcionstr + "'");
                arrValores.Add(obj.sentenciastr == null ? null : "'" + obj.sentenciastr + "'");
                arrValores.Add(obj.apiestadostr == null ? null : "'" + obj.apiestadostr + "'");
                arrValores.Add(obj.apitransaccionstr == null ? null : "'" + obj.apitransaccionstr + "'");
                arrValores.Add(obj.usumodstr == null ? null : "'" + obj.usumodstr + "'");

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnasWhere.Add(EntSegTransacciones.Fields.transaccionstr.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.tablasta == null ? null : "'" + obj.tablasta + "'");
                arrValoresWhere.Add(obj.transaccionstr == null ? null : "'" + obj.transaccionstr + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntSegTransacciones.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla segtransacciones a partir de una clase del tipo esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacion
        /// </returns>
        public int UpdateQueryAll(EntSegTransacciones obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.descripcionstr == null ? null : "'" + obj.descripcionstr + "'");
                arrValores.Add(obj.sentenciastr == null ? null : "'" + obj.sentenciastr + "'");
                arrValores.Add(obj.apiestadostr == null ? null : "'" + obj.apiestadostr + "'");
                arrValores.Add(obj.apitransaccionstr == null ? null : "'" + obj.apitransaccionstr + "'");
                arrValores.Add(obj.usumodstr == null ? null : "'" + obj.usumodstr + "'");

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnasWhere.Add(EntSegTransacciones.Fields.transaccionstr.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.tablasta == null ? null : "'" + obj.tablasta + "'");
                arrValoresWhere.Add(obj.transaccionstr == null ? null : "'" + obj.transaccionstr + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntSegTransacciones.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla segtransacciones a partir de una clase del tipo Esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionsegtransacciones
        /// </returns>
        public int UpdateQuery(EntSegTransacciones obj)
        {
            try
            {
                //Obtenemos el Objeto original
                EntSegTransacciones objOriginal = this.ObtenerObjeto(obj.tablasta, obj.transaccionstr);

                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                ArrayList arrValores = new ArrayList();

                if (obj.descripcionstr != objOriginal.descripcionstr)
                {
                    arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                    arrValores.Add(obj.descripcionstr == null ? null : "'" + obj.descripcionstr + "'");
                }
                if (obj.sentenciastr != objOriginal.sentenciastr)
                {
                    arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                    arrValores.Add(obj.sentenciastr == null ? null : "'" + obj.sentenciastr + "'");
                }
                if (obj.apiestadostr != objOriginal.apiestadostr)
                {
                    arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                    arrValores.Add(obj.apiestadostr == null ? null : "'" + obj.apiestadostr + "'");
                }
                if (obj.apitransaccionstr != objOriginal.apitransaccionstr)
                {
                    arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                    arrValores.Add(obj.apitransaccionstr == null ? null : "'" + obj.apitransaccionstr + "'");
                }
                if (obj.usumodstr != objOriginal.usumodstr)
                {
                    arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                    arrValores.Add(obj.usumodstr == null ? null : "'" + obj.usumodstr + "'");
                }

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnasWhere.Add(EntSegTransacciones.Fields.transaccionstr.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.tablasta == null ? null : "'" + obj.tablasta + "'");
                arrValoresWhere.Add(obj.transaccionstr == null ? null : "'" + obj.transaccionstr + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntSegTransacciones.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla segtransacciones a partir de una clase del tipo esegtransacciones
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacion
        /// </returns>
        public int UpdateQuery(EntSegTransacciones obj, ref CTrans localTrans)
        {
            try
            {
                //Obtenemos el Objeto original
                EntSegTransacciones objOriginal = this.ObtenerObjeto(obj.tablasta, obj.transaccionstr, ref localTrans);

                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                ArrayList arrValores = new ArrayList();

                if (obj.descripcionstr != objOriginal.descripcionstr)
                {
                    arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                    arrValores.Add(obj.descripcionstr == null ? null : "'" + obj.descripcionstr + "'");
                }
                if (obj.sentenciastr != objOriginal.sentenciastr)
                {
                    arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                    arrValores.Add(obj.sentenciastr == null ? null : "'" + obj.sentenciastr + "'");
                }
                if (obj.apiestadostr != objOriginal.apiestadostr)
                {
                    arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                    arrValores.Add(obj.apiestadostr == null ? null : "'" + obj.apiestadostr + "'");
                }
                if (obj.apitransaccionstr != objOriginal.apitransaccionstr)
                {
                    arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                    arrValores.Add(obj.apitransaccionstr == null ? null : "'" + obj.apitransaccionstr + "'");
                }
                if (obj.usumodstr != objOriginal.usumodstr)
                {
                    arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                    arrValores.Add(obj.usumodstr == null ? null : "'" + obj.usumodstr + "'");
                }

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnasWhere.Add(EntSegTransacciones.Fields.transaccionstr.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.tablasta == null ? null : "'" + obj.tablasta + "'");
                arrValoresWhere.Add(obj.transaccionstr == null ? null : "'" + obj.transaccionstr + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntSegTransacciones.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla segtransacciones a partir de una clase del tipo EntSegTransacciones y su respectiva PK
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegTransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionsegtransacciones
        /// </returns>
        public int DeleteQuery(EntSegTransacciones obj)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnasWhere.Add(EntSegTransacciones.Fields.transaccionstr.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.tablasta == null ? null : "'" + obj.tablasta + "'");
                arrValoresWhere.Add(obj.transaccionstr == null ? null : "'" + obj.transaccionstr + "'");

                CConn local = new CConn();
                return local.DeleteBd(EntSegTransacciones.StrNombreTabla, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla segtransacciones a partir de una clase del tipo EntSegTransacciones y su PK
        /// </summary>
        /// <param name="obj" type="Entidades.esegtransacciones">
        ///     <para>
        /// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacionsegtransacciones
        /// </returns>
        public int DeleteQuery(EntSegTransacciones obj, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnasWhere.Add(EntSegTransacciones.Fields.transaccionstr.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.tablasta == null ? null : "'" + obj.tablasta + "'");
                arrValoresWhere.Add(obj.transaccionstr == null ? null : "'" + obj.transaccionstr + "'");

                CConn local = new CConn();
                return local.DeleteBd(EntSegTransacciones.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla segtransacciones a partir de una clase del tipo esegtransacciones
        /// </summary>
        /// <param name="arrColumnasWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Valores para cada una de las columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionsegtransacciones
        /// </returns>
        public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CConn local = new CConn();
                return local.DeleteBd("segtransacciones", arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla segtransacciones a partir de una clase del tipo esegtransacciones
        /// </summary>
        /// <param name="arrColumnasWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Valores para cada una de las columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segtransacciones
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacionsegtransacciones
        /// </returns>
        public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            try
            {
                CConn local = new CConn();
                return local.DeleteBd(EntSegTransacciones.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region Llenado de elementos

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField)
        {
            try
            {
                CargarDropDownList(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, String textField)
        {
            try
            {
                CargarDropDownList(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, String textField, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, EntSegTransacciones.Fields searchField, object searchValue)
        {
            try
            {
                CargarDropDownList(ref cmb, valueField, textField.ToString(), searchField, searchValue);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, String textField, EntSegTransacciones.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, String textField, EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Columns.Count > 0)
                {
                    cmb.DataValueField = table.Columns[0].ColumnName;
                    cmb.DataTextField = table.Columns[1].ColumnName;
                    cmb.DataSource = table;
                    cmb.DataBind();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                dtg.DataSource = dsReader;
                dtg.DataBind();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, EntSegTransacciones.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, EntSegTransacciones.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegTransacciones.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegTransacciones.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarGridViewOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
                dtg.DataBind();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, String textField)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, String textField, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, EntSegTransacciones.Fields searchField, object searchValue)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, String textField, EntSegTransacciones.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, String textField, EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segtransacciones
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segtransacciones
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                    cmb.DataSource = table;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, EntSegTransacciones.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, EntSegTransacciones.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegTransacciones.Fields.tablasta.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.transaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.descripcionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.sentenciastr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apiestadostr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.apitransaccionstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usucrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.feccrestr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.usumodstr.ToString());
                arrColumnas.Add(EntSegTransacciones.Fields.fecmodstr.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegTransacciones.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegTransacciones.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegTransacciones.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segtransacciones
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntSegTransacciones.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #endregion

        #region Funciones Internas

        /// <summary>
        /// 	 Funcion que devuelve un objeto a partir de un DataRow
        /// </summary>
        /// <param name="row" type="System.Data.DataRow">
        ///     <para>
        /// 		 DataRow con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Objeto segtransacciones
        /// </returns>
        internal EntSegTransacciones crearObjeto(DataRow row)
        {
            var obj = new EntSegTransacciones();
            obj.tablasta = GetColumnType(row[EntSegTransacciones.Fields.tablasta.ToString()], EntSegTransacciones.Fields.tablasta);
            obj.transaccionstr = GetColumnType(row[EntSegTransacciones.Fields.transaccionstr.ToString()], EntSegTransacciones.Fields.transaccionstr);
            obj.descripcionstr = GetColumnType(row[EntSegTransacciones.Fields.descripcionstr.ToString()], EntSegTransacciones.Fields.descripcionstr);
            obj.sentenciastr = GetColumnType(row[EntSegTransacciones.Fields.sentenciastr.ToString()], EntSegTransacciones.Fields.sentenciastr);
            obj.apiestadostr = GetColumnType(row[EntSegTransacciones.Fields.apiestadostr.ToString()], EntSegTransacciones.Fields.apiestadostr);
            obj.apitransaccionstr = GetColumnType(row[EntSegTransacciones.Fields.apitransaccionstr.ToString()], EntSegTransacciones.Fields.apitransaccionstr);
            obj.usucrestr = GetColumnType(row[EntSegTransacciones.Fields.usucrestr.ToString()], EntSegTransacciones.Fields.usucrestr);
            obj.feccrestr = GetColumnType(row[EntSegTransacciones.Fields.feccrestr.ToString()], EntSegTransacciones.Fields.feccrestr);
            obj.usumodstr = GetColumnType(row[EntSegTransacciones.Fields.usumodstr.ToString()], EntSegTransacciones.Fields.usumodstr);
            obj.fecmodstr = GetColumnType(row[EntSegTransacciones.Fields.fecmodstr.ToString()], EntSegTransacciones.Fields.fecmodstr);
            return obj;
        }

        /// <summary>
        /// 	 Funcion que devuelve un objeto a partir de un DataRow
        /// </summary>
        /// <param name="row" type="System.Data.DataRow">
        ///     <para>
        /// 		 DataRow con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Objeto segtransacciones
        /// </returns>
        internal EntSegTransacciones crearObjetoRevisado(DataRow row)
        {
            var obj = new EntSegTransacciones();
            if (row.Table.Columns.Contains(EntSegTransacciones.Fields.tablasta.ToString()))
                obj.tablasta = GetColumnType(row[EntSegTransacciones.Fields.tablasta.ToString()], EntSegTransacciones.Fields.tablasta);
            if (row.Table.Columns.Contains(EntSegTransacciones.Fields.transaccionstr.ToString()))
                obj.transaccionstr = GetColumnType(row[EntSegTransacciones.Fields.transaccionstr.ToString()], EntSegTransacciones.Fields.transaccionstr);
            if (row.Table.Columns.Contains(EntSegTransacciones.Fields.descripcionstr.ToString()))
                obj.descripcionstr = GetColumnType(row[EntSegTransacciones.Fields.descripcionstr.ToString()], EntSegTransacciones.Fields.descripcionstr);
            if (row.Table.Columns.Contains(EntSegTransacciones.Fields.sentenciastr.ToString()))
                obj.sentenciastr = GetColumnType(row[EntSegTransacciones.Fields.sentenciastr.ToString()], EntSegTransacciones.Fields.sentenciastr);
            if (row.Table.Columns.Contains(EntSegTransacciones.Fields.apiestadostr.ToString()))
                obj.apiestadostr = GetColumnType(row[EntSegTransacciones.Fields.apiestadostr.ToString()], EntSegTransacciones.Fields.apiestadostr);
            if (row.Table.Columns.Contains(EntSegTransacciones.Fields.apitransaccionstr.ToString()))
                obj.apitransaccionstr = GetColumnType(row[EntSegTransacciones.Fields.apitransaccionstr.ToString()], EntSegTransacciones.Fields.apitransaccionstr);
            if (row.Table.Columns.Contains(EntSegTransacciones.Fields.usucrestr.ToString()))
                obj.usucrestr = GetColumnType(row[EntSegTransacciones.Fields.usucrestr.ToString()], EntSegTransacciones.Fields.usucrestr);
            if (row.Table.Columns.Contains(EntSegTransacciones.Fields.feccrestr.ToString()))
                obj.feccrestr = GetColumnType(row[EntSegTransacciones.Fields.feccrestr.ToString()], EntSegTransacciones.Fields.feccrestr);
            if (row.Table.Columns.Contains(EntSegTransacciones.Fields.usumodstr.ToString()))
                obj.usumodstr = GetColumnType(row[EntSegTransacciones.Fields.usumodstr.ToString()], EntSegTransacciones.Fields.usumodstr);
            if (row.Table.Columns.Contains(EntSegTransacciones.Fields.fecmodstr.ToString()))
                obj.fecmodstr = GetColumnType(row[EntSegTransacciones.Fields.fecmodstr.ToString()], EntSegTransacciones.Fields.fecmodstr);
            return obj;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtsegtransacciones" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Lista de Objetos segtransacciones
        /// </returns>
        internal List<EntSegTransacciones> crearLista(DataTable dtsegtransacciones)
        {
            var list = new List<EntSegTransacciones>();

            foreach (DataRow row in dtsegtransacciones.Rows)
            {
                var obj = crearObjeto(row);
                list.Add(obj);
            }
            return list;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
        /// </summary>
        /// <param name="dtsegtransacciones" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Lista de Objetos segtransacciones
        /// </returns>
        internal List<EntSegTransacciones> crearListaRevisada(DataTable dtsegtransacciones)
        {
            List<EntSegTransacciones> list = new List<EntSegTransacciones>();

            foreach (DataRow row in dtsegtransacciones.Rows)
            {
                var obj = crearObjetoRevisado(row);
                list.Add(obj);
            }
            return list;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtsegtransacciones" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cola de Objetos segtransacciones
        /// </returns>
        internal Queue<EntSegTransacciones> crearCola(DataTable dtsegtransacciones)
        {
            Queue<EntSegTransacciones> cola = new Queue<EntSegTransacciones>();

            EntSegTransacciones obj = new EntSegTransacciones();
            foreach (DataRow row in dtsegtransacciones.Rows)
            {
                obj = crearObjeto(row);
                cola.Enqueue(obj);
            }
            return cola;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtsegtransacciones" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Pila de Objetos segtransacciones
        /// </returns>
        internal Stack<EntSegTransacciones> crearPila(DataTable dtsegtransacciones)
        {
            Stack<EntSegTransacciones> pila = new Stack<EntSegTransacciones>();

            foreach (DataRow row in dtsegtransacciones.Rows)
            {
                var obj = crearObjeto(row);
                pila.Push(obj);
            }
            return pila;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable
        /// </summary>
        /// <param name="dtsegtransacciones" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Diccionario de Objetos segtransacciones
        /// </returns>
        internal Dictionary<String, EntSegTransacciones> crearDiccionario(DataTable dtsegtransacciones)
        {
            Dictionary<String, EntSegTransacciones> miDic = new Dictionary<String, EntSegTransacciones>();

            foreach (DataRow row in dtsegtransacciones.Rows)
            {
                var obj = crearObjeto(row);
                miDic.Add(obj.GetHashCode().ToString(), obj);
            }
            return miDic;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable
        /// </summary>
        /// <param name="dtsegtransacciones" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 HashTable de Objetos segtransacciones
        /// </returns>
        internal Hashtable crearHashTable(DataTable dtsegtransacciones)
        {
            Hashtable miTabla = new Hashtable();

            foreach (DataRow row in dtsegtransacciones.Rows)
            {
                var obj = crearObjeto(row);
                miTabla.Add(obj.GetHashCode().ToString(), obj);
            }
            return miTabla;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
        /// </summary>
        /// <param name="dtsegtransacciones" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Diccionario de Objetos segtransacciones
        /// </returns>
        internal Dictionary<String, EntSegTransacciones> crearDiccionarioRevisado(DataTable dtsegtransacciones)
        {
            Dictionary<String, EntSegTransacciones> miDic = new Dictionary<String, EntSegTransacciones>();

            foreach (DataRow row in dtsegtransacciones.Rows)
            {
                var obj = crearObjetoRevisado(row);
                miDic.Add(obj.GetHashCode().ToString(), obj);
            }
            return miDic;
        }

        internal Dictionary<String, EntSegTransacciones> crearDiccionario(DataTable dtsegtransacciones, EntSegTransacciones.Fields dicKey)
        {
            Dictionary<String, EntSegTransacciones> miDic = new Dictionary<String, EntSegTransacciones>();

            foreach (DataRow row in dtsegtransacciones.Rows)
            {
                var obj = crearObjeto(row);

                var nameOfProperty = dicKey.ToString();
                var propertyInfo = obj.GetType().GetProperty(nameOfProperty);
                var value = propertyInfo.GetValue(obj, null);

                miDic.Add(value.ToString(), obj);
            }
            return miDic;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        protected void Finalize()
        {
            Dispose();
        }

        #endregion
    }
}