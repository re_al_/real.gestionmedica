#region
/***********************************************************************************************************
	NOMBRE:       RnPacCertificados
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla paccertificados

	REVISIONES:
		Ver        FECHA       Autor            Descripcion
		---------  ----------  ---------------  ------------------------------------
		1.0        16/05/2021  R Alonzo Vera A  Creacion

*************************************************************************************************************/
#endregion

#region

using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Interface;
using System.Windows.Forms;
using System.Web.UI.WebControls;

#endregion

namespace ReAl.GestionMedica.Class.Modelo
{
    public class RnPacCertificados : IPacCertificados
    {
        //Debe implementar la Interface (Alt + Shift + F10)

        #region IPacCertificados Members

        #region Reflection

        /// <summary>
        /// Metodo que devuelve el Script SQL de la Tabla
        /// </summary>
        /// <returns>Script SQL</returns>
        public string GetTableScript()
        {
            TableClass tabla = new TableClass(typeof(EntPacCertificados));
            return tabla.CreateTableScript();
        }

        /// <summary>
        /// Metodo para castear Dinamicamente un Tipo
        /// </summary>
        /// <param name="valor">Tipo a ser casteado</param>
        /// <param name="myField">Enum de la columna</param>
        /// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
        public dynamic GetColumnType(object valor, EntPacCertificados.Fields myField)
        {
            if (DBNull.Value.Equals(valor))
                return null;
            Type destino = typeof(EntPacCertificados).GetProperty(myField.ToString()).PropertyType;
            var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;

            try
            {
                TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
                return tc.ConvertFrom(valor);
            }
            catch (Exception)
            {
                return Convert.ChangeType(valor, miTipo);
            }
        }

        /// <summary>
        /// Metodo para castear Dinamicamente un Tipo
        /// </summary>
        /// <param name="valor">Tipo a ser casteado</param>
        /// <param name="strField">Nombre de la columna</param>
        /// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
        public dynamic GetColumnType(object valor, string strField)
        {
            if (DBNull.Value.Equals(valor))
                return null;
            Type destino = typeof(EntPacCertificados).GetProperty(strField).PropertyType;
            var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;

            try
            {
                TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
                return tc.ConvertFrom(valor);
            }
            catch (Exception)
            {
                return Convert.ChangeType(valor, miTipo);
            }
        }

        /// <summary>
        /// Inserta una valor a una propiedad de un objeto instanciado
        /// </summary>
        /// <param name="obj">Objeto instanciado</param>
        /// <param name="strPropiedad">Es el nombre de la propiedad</param>
        /// <param name="dynValor">Es el valor que se insertara a la propiedad</param>
        public void SetDato(ref EntPacCertificados obj, string strPropiedad, dynamic dynValor)
        {
            if (obj == null) throw new ArgumentNullException();
            obj.GetType().GetProperty(strPropiedad).SetValue(obj, GetColumnType(dynValor, strPropiedad), null);
        }

        /// <summary>
        /// Obtiene el valor de una propiedad de un objeto instanciado
        /// </summary>
        /// <param name="obj">Objeto instanciado</param>
        /// <param name="strPropiedad">El nombre de la propiedad de la que se obtendra el valor</param>
        /// <returns>Devuelve el valor del a propiedad seleccionada</returns>
        public dynamic GetDato(ref EntPacCertificados obj, string strPropiedad)
        {
            if (obj == null) return null;
            var propertyInfo = obj.GetType().GetProperty(strPropiedad);
            return GetColumnType(propertyInfo.GetValue(obj, null), strPropiedad);
        }

        /// <summary>
        /// 	 Funcion que obtiene la llave primaria unica de la tabla paccertificados a partir de una cadena
        /// </summary>
        /// <param name="args" type="string[]">
        ///     <para>
        /// 		 Cadena desde la que se construye el identificador unico de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Identificador unico de la tabla paccertificados
        /// </returns>
        public string CreatePk(string[] args)
        {
            return args[0];
        }

        #endregion

        #region ObtenerObjeto

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntPacCertificados a partir de la llave primaria
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public EntPacCertificados ObtenerObjeto(Int64 Int64idpcm)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(EntPacCertificados.Fields.idpcm.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'" + Int64idpcm + "'");

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntPacCertificados a partir del usuario que inserta
        /// </summary>
        /// <param name="strUsuCre">Login o nombre de usuario</param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public EntPacCertificados ObtenerObjetoInsertado(string strUsuCre)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(EntPacCertificados.Fields.usucre.ToString());
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'" + strUsuCre + "'");

            int iIdInsertado = FuncionesMax(EntPacCertificados.Fields.idpcm, arrColumnasWhere, arrValoresWhere);

            return ObtenerObjeto(iIdInsertado);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public EntPacCertificados ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public EntPacCertificados ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count == 1)
                {
                    EntPacCertificados obj = new EntPacCertificados();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public EntPacCertificados ObtenerObjeto(Hashtable htbFiltro)
        {
            return ObtenerObjeto(htbFiltro, "");
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public EntPacCertificados ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count == 1)
                {
                    EntPacCertificados obj = new EntPacCertificados();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public EntPacCertificados ObtenerObjeto(EntPacCertificados.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public EntPacCertificados ObtenerObjeto(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un Business Object del Tipo EntPacCertificados a partir de su llave promaria
        /// </summary>
        /// <returns>
        /// 	Objeto del Tipo EntPacCertificados
        /// </returns>
        public EntPacCertificados ObtenerObjeto(Int64 Int64idpcm, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(EntPacCertificados.Fields.idpcm.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'" + Int64idpcm + "'");
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public EntPacCertificados ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public EntPacCertificados ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans)
        {
            return ObtenerObjeto(htbFiltro, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public EntPacCertificados ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public EntPacCertificados ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count == 1)
                {
                    EntPacCertificados obj = new EntPacCertificados();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public EntPacCertificados ObtenerObjeto(EntPacCertificados.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public EntPacCertificados ObtenerObjeto(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        #endregion

        #region ObtenerLista

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerLista()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerLista(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntPacCertificados>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerLista(EntPacCertificados.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerLista(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntPacCertificados>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerLista(EntPacCertificados.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerLista(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerLista(Hashtable htbFiltro)
        {
            return ObtenerLista(htbFiltro, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntPacCertificados>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans)
        {
            return ObtenerLista(htbFiltro, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntPacCertificados>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerListaDesdeVista(String strVista)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntPacCertificados>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
        {
            return ObtenerListaDesdeVista(strVista, htbFiltro, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntPacCertificados>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, EntPacCertificados.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntPacCertificados>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans)
        {
            return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntPacCertificados>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, EntPacCertificados.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        #endregion

        #region ObtenerCola y Obtener Pila

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntPacCertificados> ObtenerCola()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerCola(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntPacCertificados> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntPacCertificados> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearCola(table);
                }
                else
                    return new Queue<EntPacCertificados>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntPacCertificados> ObtenerCola(EntPacCertificados.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntPacCertificados> ObtenerCola(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntPacCertificados> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntPacCertificados> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearCola(table);
                }
                else
                    return new Queue<EntPacCertificados>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntPacCertificados> ObtenerCola(EntPacCertificados.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntPacCertificados> ObtenerCola(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntPacCertificados> ObtenerPila()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerPila(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntPacCertificados> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntPacCertificados> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearPila(table);
                }
                else
                    return new Stack<EntPacCertificados>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntPacCertificados> ObtenerPila(EntPacCertificados.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntPacCertificados> ObtenerPila(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntPacCertificados> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntPacCertificados> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearPila(table);
                }
                else
                    return new Stack<EntPacCertificados>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntPacCertificados> ObtenerPila(EntPacCertificados.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntPacCertificados> ObtenerPila(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        #endregion

        #region ObtenerDataTable

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla paccertificados
        /// </summary>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable NuevoDataTable()
        {
            try
            {
                DataTable table = new DataTable();
                DataColumn dc;
                dc = new DataColumn(EntPacCertificados.Fields.idpcm.ToString(), typeof(EntPacCertificados).GetProperty(EntPacCertificados.Fields.idpcm.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntPacCertificados.Fields.idppa.ToString(), typeof(EntPacCertificados).GetProperty(EntPacCertificados.Fields.idppa.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntPacCertificados.Fields.fechapcm.ToString(), typeof(EntPacCertificados).GetProperty(EntPacCertificados.Fields.fechapcm.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntPacCertificados.Fields.matriculapcm.ToString(), typeof(EntPacCertificados).GetProperty(EntPacCertificados.Fields.matriculapcm.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntPacCertificados.Fields.textopcm.ToString(), typeof(EntPacCertificados).GetProperty(EntPacCertificados.Fields.textopcm.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntPacCertificados.Fields.rtfpcm.ToString(), typeof(EntPacCertificados).GetProperty(EntPacCertificados.Fields.rtfpcm.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntPacCertificados.Fields.htmlpcm.ToString(), typeof(EntPacCertificados).GetProperty(EntPacCertificados.Fields.htmlpcm.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntPacCertificados.Fields.apiestado.ToString(), typeof(EntPacCertificados).GetProperty(EntPacCertificados.Fields.apiestado.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntPacCertificados.Fields.apitransaccion.ToString(), typeof(EntPacCertificados).GetProperty(EntPacCertificados.Fields.apitransaccion.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntPacCertificados.Fields.usucre.ToString(), typeof(EntPacCertificados).GetProperty(EntPacCertificados.Fields.usucre.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntPacCertificados.Fields.feccre.ToString(), typeof(EntPacCertificados).GetProperty(EntPacCertificados.Fields.feccre.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntPacCertificados.Fields.usumod.ToString(), typeof(EntPacCertificados).GetProperty(EntPacCertificados.Fields.usumod.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntPacCertificados.Fields.fecmod.ToString(), typeof(EntPacCertificados).GetProperty(EntPacCertificados.Fields.fecmod.ToString()).PropertyType);
                table.Columns.Add(dc);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que genera un DataTable con determinadas columnas de una paccertificados
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable NuevoDataTable(ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'2'");

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla paccertificados
        /// </summary>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable ObtenerDataTable()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE paccertificados
        /// </summary>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable ObtenerDataTable(String strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
        {
            try
            {
                return ObtenerDataTable(arrColumnas, htbFiltro, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable ObtenerDataTable(Hashtable htbFiltro)
        {
            try
            {
                return ObtenerDataTable(htbFiltro, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());
                return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de paccertificados
        /// </returns>
        public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableOr(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(EntPacCertificados.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(EntPacCertificados.Fields searchField, object searchValue, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacCertificados.Fields searchField, object searchValue, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacCertificados.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region ObtenerDiccionario

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntPacCertificados> ObtenerDiccionario()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntPacCertificados> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntPacCertificados> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, EntPacCertificados>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntPacCertificados> ObtenerDiccionario(EntPacCertificados.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntPacCertificados> ObtenerDiccionario(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntPacCertificados> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntPacCertificados> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, EntPacCertificados>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntPacCertificados> ObtenerDiccionario(EntPacCertificados.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntPacCertificados a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntPacCertificados> ObtenerDiccionario(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        public Dictionary<String, EntPacCertificados> ObtenerDiccionarioKey(EntPacCertificados.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
        }

        public Dictionary<String, EntPacCertificados> ObtenerDiccionarioKey(String strParamAdic, EntPacCertificados.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdic, dicKey);
        }

        public Dictionary<String, EntPacCertificados> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, EntPacCertificados.Fields dicKey)
        {
            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, "", dicKey);
        }

        public Dictionary<String, EntPacCertificados> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, EntPacCertificados.Fields dicKey)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table, dicKey);
                }
                else
                    return new Dictionary<string, EntPacCertificados>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public Dictionary<String, EntPacCertificados> ObtenerDiccionarioKey(EntPacCertificados.Fields searchField, object searchValue, EntPacCertificados.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
        }

        public Dictionary<String, EntPacCertificados> ObtenerDiccionarioKey(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales, EntPacCertificados.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey);
        }

        #endregion

        #region ObjetoASp

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="strNombreSp" type="System.string">
        ///     <para>
        /// 		 Nombre del Procedimiento a ejecutar sobre el SP
        ///     </para>
        /// </param>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se va a ejecutar el SP de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor de registros afectados en el Procedimiento de la tabla paccertificados
        /// </returns>
        public int EjecutarSpDesdeObjeto(string strNombreSp, EntPacCertificados obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrNombreParam = new ArrayList();
                arrNombreParam.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.idppa.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.usucre.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.feccre.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.usumod.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.fecmod.ToString());

                ArrayList arrValoresParam = new ArrayList();
                arrValoresParam.Add(obj.idpcm == null ? null : "'" + obj.idpcm + "'");
                arrValoresParam.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
                arrValoresParam.Add(obj.fechapcm == null ? null : "'" + Convert.ToDateTime(obj.fechapcm).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValoresParam.Add(obj.matriculapcm == null ? null : "'" + obj.matriculapcm + "'");
                arrValoresParam.Add(obj.textopcm == null ? null : "'" + obj.textopcm + "'");
                arrValoresParam.Add(obj.rtfpcm);
                arrValoresParam.Add(obj.htmlpcm == null ? null : "'" + obj.htmlpcm + "'");
                arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
                arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
                arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
                arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
                arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="strNombreSp" type="System.string">
        ///     <para>
        /// 		 Nombre del Procedimiento a ejecutar sobre el SP
        ///     </para>
        /// </param>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se va a ejecutar el SP de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor de registros afectados en el Procedimiento de la tabla paccertificados
        /// </returns>
        public int EjecutarSpDesdeObjeto(string strNombreSp, EntPacCertificados obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrNombreParam = new ArrayList();
                arrNombreParam.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.idppa.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.usucre.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.feccre.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.usumod.ToString());
                arrNombreParam.Add(EntPacCertificados.Fields.fecmod.ToString());

                ArrayList arrValoresParam = new ArrayList();
                arrValoresParam.Add(obj.idpcm == null ? null : "'" + obj.idpcm + "'");
                arrValoresParam.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
                arrValoresParam.Add(obj.fechapcm == null ? null : "'" + Convert.ToDateTime(obj.fechapcm).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValoresParam.Add(obj.matriculapcm == null ? null : "'" + obj.matriculapcm + "'");
                arrValoresParam.Add(obj.textopcm == null ? null : "'" + obj.textopcm + "'");
                arrValoresParam.Add(obj.rtfpcm);
                arrValoresParam.Add(obj.htmlpcm == null ? null : "'" + obj.htmlpcm + "'");
                arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
                arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
                arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
                arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
                arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region FuncionesAgregadas

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesCount(EntPacCertificados.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesCount(EntPacCertificados.Fields refField, EntPacCertificados.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesCount(EntPacCertificados.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("count(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMin(EntPacCertificados.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMin(EntPacCertificados.Fields refField, EntPacCertificados.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMin(EntPacCertificados.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("min(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMax(EntPacCertificados.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMax(EntPacCertificados.Fields refField, EntPacCertificados.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMax(EntPacCertificados.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("max(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesSum(EntPacCertificados.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesSum(EntPacCertificados.Fields refField, EntPacCertificados.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesSum(EntPacCertificados.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("sum(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesAvg(EntPacCertificados.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesAvg(EntPacCertificados.Fields refField, EntPacCertificados.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntPacCertificados que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesAvg(EntPacCertificados.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("avg(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region ABMs SP

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionpaccertificados
        /// </returns>
        public bool Insert(EntPacCertificados obj, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add("idpcm");
                arrValoresParam.Add(null);
                if (obj.idppa == null)
                    throw new Exception("El Parametro idppa no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.idppa.ToString());
                arrValoresParam.Add(obj.idppa);

                if (obj.matriculapcm == null)
                    throw new Exception("El Parametro matriculapcm no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrValoresParam.Add(obj.matriculapcm);

                if (obj.fechapcm == null)
                    throw new Exception("El Parametro fechapcm no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrValoresParam.Add(obj.fechapcm);

                if (obj.rtfpcm == null)
                    throw new Exception("El Parametro rtfpcm no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrValoresParam.Add(obj.rtfpcm);

                if (obj.textopcm == null)
                    throw new Exception("El Parametro textopcm no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrValoresParam.Add(obj.textopcm);

                if (obj.usucre == null)
                    throw new Exception("El Parametro usucre no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.usucre.ToString());
                arrValoresParam.Add(obj.usucre);

                if (obj.htmlpcm == null)
                    throw new Exception("El Parametro htmlpcm no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrValoresParam.Add(obj.htmlpcm);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.PacCertificados.SpPcmIns.ToString();
                return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam) > 0);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionpaccertificados
        /// </returns>
        public bool Insert(EntPacCertificados obj, ref CTrans localTrans, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add("idpcm");
                arrValoresParam.Add(null);

                arrNombreParam.Add(EntPacCertificados.Fields.idppa.ToString());
                arrValoresParam.Add(obj.idppa);

                arrNombreParam.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrValoresParam.Add(obj.matriculapcm);

                arrNombreParam.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrValoresParam.Add(obj.fechapcm);

                arrNombreParam.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrValoresParam.Add(obj.rtfpcm);

                arrNombreParam.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrValoresParam.Add(obj.textopcm);

                arrNombreParam.Add(EntPacCertificados.Fields.usucre.ToString());
                arrValoresParam.Add(obj.usucre);

                arrNombreParam.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrValoresParam.Add(obj.htmlpcm);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.PacCertificados.SpPcmIns.ToString();
                return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans) > 0);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor que indica la cantidad de registros actualizados en paccertificados
        /// </returns>
        public int Update(EntPacCertificados obj, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();

                arrNombreParam.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrValoresParam.Add(obj.idpcm);

                arrNombreParam.Add(EntPacCertificados.Fields.idppa.ToString());
                arrValoresParam.Add(obj.idppa);

                arrNombreParam.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrValoresParam.Add(obj.matriculapcm);

                arrNombreParam.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrValoresParam.Add(obj.fechapcm);

                arrNombreParam.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrValoresParam.Add(obj.rtfpcm);

                arrNombreParam.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrValoresParam.Add(obj.textopcm);

                arrNombreParam.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrValoresParam.Add(obj.apitransaccion);

                arrNombreParam.Add(EntPacCertificados.Fields.usumod.ToString());
                arrValoresParam.Add(obj.usumod);

                arrNombreParam.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrValoresParam.Add(obj.htmlpcm);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.PacCertificados.SpPcmUpd.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionpaccertificados
        /// </returns>
        public int Update(EntPacCertificados obj, ref CTrans localTrans, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                if (obj.idpcm == null)
                    throw new Exception("El Parametro idpcm no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrValoresParam.Add(obj.idpcm);

                if (obj.idppa == null)
                    throw new Exception("El Parametro idppa no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.idppa.ToString());
                arrValoresParam.Add(obj.idppa);

                if (obj.matriculapcm == null)
                    throw new Exception("El Parametro matriculapcm no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrValoresParam.Add(obj.matriculapcm);

                if (obj.fechapcm == null)
                    throw new Exception("El Parametro fechapcm no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrValoresParam.Add(obj.fechapcm);

                if (obj.rtfpcm == null)
                    throw new Exception("El Parametro rtfpcm no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrValoresParam.Add(obj.rtfpcm);

                if (obj.textopcm == null)
                    throw new Exception("El Parametro textopcm no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrValoresParam.Add(obj.textopcm);

                if (obj.apitransaccion == null)
                    throw new Exception("El Parametro apitransaccion no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrValoresParam.Add(obj.apitransaccion);

                if (obj.usumod == null)
                    throw new Exception("El Parametro usumod no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.usumod.ToString());
                arrValoresParam.Add(obj.usumod);

                if (obj.htmlpcm == null)
                    throw new Exception("El Parametro htmlpcm no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrValoresParam.Add(obj.htmlpcm);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.PacCertificados.SpPcmUpd.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionpaccertificados
        /// </returns>
        public int Delete(EntPacCertificados obj, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                if (obj.idpcm == null)
                    throw new Exception("El Parametro idpcm no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrValoresParam.Add(obj.idpcm);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.PacCertificados.SpPcmDel.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionpaccertificados
        /// </returns>
        public int Delete(EntPacCertificados obj, ref CTrans localTrans, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                if (obj.idpcm == null)
                    throw new Exception("El Parametro idpcm no puede ser NULO.");
                arrNombreParam.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrValoresParam.Add(obj.idpcm);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.PacCertificados.SpPcmDel.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionpaccertificados
        /// </returns>
        public int InsertUpdate(EntPacCertificados obj)
        {
            try
            {
                bool esInsertar = true;

                esInsertar = (esInsertar && (obj.idpcm == null));

                if (esInsertar)
                    return Insert(obj) ? 1 : 0;
                else
                    return Update(obj);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionpaccertificados
        /// </returns>
        public int InsertUpdate(EntPacCertificados obj, ref CTrans localTrans)
        {
            try
            {
                bool esInsertar = false;

                esInsertar = (esInsertar && (obj.idpcm == null));

                if (esInsertar)
                    return Insert(obj, ref localTrans) ? 1 : 0;
                else
                    return Update(obj, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region ABMs Query

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionpaccertificados
        /// </returns>
        public bool InsertQuery(EntPacCertificados obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                //arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());

                ArrayList arrValores = new ArrayList();
                //arrValores.Add(obj.idpcm == null ? null : "'" + obj.idpcm + "'");
                arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
                arrValores.Add(obj.fechapcm == null ? null : "'" + Convert.ToDateTime(obj.fechapcm).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValores.Add(obj.matriculapcm == null ? null : "'" + obj.matriculapcm + "'");
                arrValores.Add(obj.textopcm == null ? null : "'" + obj.textopcm + "'");
                arrValores.Add(obj.rtfpcm);
                arrValores.Add(obj.htmlpcm == null ? null : "'" + obj.htmlpcm + "'");
                arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
                arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

                CConn local = new CConn();
                return local.InsertBd(EntPacCertificados.StrNombreTabla, arrColumnas, arrValores);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionpaccertificados
        /// </returns>
        public bool InsertQuery(EntPacCertificados obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                //arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());

                ArrayList arrValores = new ArrayList();
                //arrValores.Add(obj.idpcm == null ? null : "'" + obj.idpcm + "'");
                arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
                arrValores.Add(obj.fechapcm == null ? null : "'" + Convert.ToDateTime(obj.fechapcm).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValores.Add(obj.matriculapcm == null ? null : "'" + obj.matriculapcm + "'");
                arrValores.Add(obj.textopcm == null ? null : "'" + obj.textopcm + "'");
                arrValores.Add(obj.rtfpcm);
                arrValores.Add(obj.htmlpcm == null ? null : "'" + obj.htmlpcm + "'");
                arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
                arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

                CConn local = new CConn();
                return local.InsertBd(EntPacCertificados.StrNombreTabla, arrColumnas, arrValores, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionpaccertificados
        /// </returns>
        public bool InsertQueryIdentity(EntPacCertificados obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                //arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());

                ArrayList arrValores = new ArrayList();
                //arrValores.Add(obj.idpcm == null ? null : "'" + obj.idpcm + "'");
                arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
                arrValores.Add(obj.fechapcm == null ? null : "'" + Convert.ToDateTime(obj.fechapcm).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValores.Add(obj.matriculapcm == null ? null : "'" + obj.matriculapcm + "'");
                arrValores.Add(obj.textopcm == null ? null : "'" + obj.textopcm + "'");
                arrValores.Add(obj.rtfpcm);
                arrValores.Add(obj.htmlpcm == null ? null : "'" + obj.htmlpcm + "'");
                arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
                arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

                CConn local = new CConn();
                int intIdentidad = -1;
                bool res = local.InsertBd(EntPacCertificados.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad);
                obj.idpcm = intIdentidad;
                return res;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionpaccertificados
        /// </returns>
        public bool InsertQueryIdentity(EntPacCertificados obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                //arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());

                ArrayList arrValores = new ArrayList();
                //arrValores.Add(obj.idpcm == null ? null : "'" + obj.idpcm + "'");
                arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
                arrValores.Add(obj.fechapcm == null ? null : "'" + Convert.ToDateTime(obj.fechapcm).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValores.Add(obj.matriculapcm == null ? null : "'" + obj.matriculapcm + "'");
                arrValores.Add(obj.textopcm == null ? null : "'" + obj.textopcm + "'");
                arrValores.Add(obj.rtfpcm);
                arrValores.Add(obj.htmlpcm == null ? null : "'" + obj.htmlpcm + "'");
                arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
                arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

                CConn local = new CConn();
                int intIdentidad = -1;
                bool res = local.InsertBd(EntPacCertificados.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
                obj.idpcm = intIdentidad;
                return res;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionpaccertificados
        /// </returns>
        public int UpdateQueryAll(EntPacCertificados obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
                arrValores.Add(obj.fechapcm == null ? null : "'" + Convert.ToDateTime(obj.fechapcm).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValores.Add(obj.matriculapcm == null ? null : "'" + obj.matriculapcm + "'");
                arrValores.Add(obj.textopcm == null ? null : "'" + obj.textopcm + "'");
                arrValores.Add(obj.rtfpcm);
                arrValores.Add(obj.htmlpcm == null ? null : "'" + obj.htmlpcm + "'");
                arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
                arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
                arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntPacCertificados.Fields.idpcm.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.idpcm == null ? null : "'" + obj.idpcm + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntPacCertificados.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla paccertificados a partir de una clase del tipo epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacion
        /// </returns>
        public int UpdateQueryAll(EntPacCertificados obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
                arrValores.Add(obj.fechapcm == null ? null : "'" + Convert.ToDateTime(obj.fechapcm).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValores.Add(obj.matriculapcm == null ? null : "'" + obj.matriculapcm + "'");
                arrValores.Add(obj.textopcm == null ? null : "'" + obj.textopcm + "'");
                arrValores.Add(obj.rtfpcm);
                arrValores.Add(obj.htmlpcm == null ? null : "'" + obj.htmlpcm + "'");
                arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
                arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
                arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntPacCertificados.Fields.idpcm.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.idpcm == null ? null : "'" + obj.idpcm + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntPacCertificados.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla paccertificados a partir de una clase del tipo Epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionpaccertificados
        /// </returns>
        public int UpdateQuery(EntPacCertificados obj)
        {
            try
            {
                //Obtenemos el Objeto original
                EntPacCertificados objOriginal = this.ObtenerObjeto(obj.idpcm);

                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                ArrayList arrValores = new ArrayList();

                if (obj.idppa != objOriginal.idppa)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                    arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
                }
                if (obj.fechapcm != objOriginal.fechapcm)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                    arrValores.Add(obj.fechapcm == null ? null : "'" + Convert.ToDateTime(obj.fechapcm).ToString(CParametros.ParFormatoFechaHora) + "'");
                }
                if (obj.matriculapcm != objOriginal.matriculapcm)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                    arrValores.Add(obj.matriculapcm == null ? null : "'" + obj.matriculapcm + "'");
                }
                if (obj.textopcm != objOriginal.textopcm)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                    arrValores.Add(obj.textopcm == null ? null : "'" + obj.textopcm + "'");
                }
                if (obj.rtfpcm != objOriginal.rtfpcm)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                    arrValores.Add(obj.rtfpcm);
                }
                if (obj.htmlpcm != objOriginal.htmlpcm)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                    arrValores.Add(obj.htmlpcm == null ? null : "'" + obj.htmlpcm + "'");
                }
                if (obj.apiestado != objOriginal.apiestado)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                    arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
                }
                if (obj.apitransaccion != objOriginal.apitransaccion)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                    arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
                }
                if (obj.usumod != objOriginal.usumod)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                    arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
                }

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntPacCertificados.Fields.idpcm.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.idpcm == null ? null : "'" + obj.idpcm + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntPacCertificados.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla paccertificados a partir de una clase del tipo epaccertificados
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacion
        /// </returns>
        public int UpdateQuery(EntPacCertificados obj, ref CTrans localTrans)
        {
            try
            {
                //Obtenemos el Objeto original
                EntPacCertificados objOriginal = this.ObtenerObjeto(obj.idpcm, ref localTrans);

                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                ArrayList arrValores = new ArrayList();

                if (obj.idppa != objOriginal.idppa)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                    arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
                }
                if (obj.fechapcm != objOriginal.fechapcm)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                    arrValores.Add(obj.fechapcm == null ? null : "'" + Convert.ToDateTime(obj.fechapcm).ToString(CParametros.ParFormatoFechaHora) + "'");
                }
                if (obj.matriculapcm != objOriginal.matriculapcm)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                    arrValores.Add(obj.matriculapcm == null ? null : "'" + obj.matriculapcm + "'");
                }
                if (obj.textopcm != objOriginal.textopcm)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                    arrValores.Add(obj.textopcm == null ? null : "'" + obj.textopcm + "'");
                }
                if (obj.rtfpcm != objOriginal.rtfpcm)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                    arrValores.Add(obj.rtfpcm);
                }
                if (obj.htmlpcm != objOriginal.htmlpcm)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                    arrValores.Add(obj.htmlpcm == null ? null : "'" + obj.htmlpcm + "'");
                }
                if (obj.apiestado != objOriginal.apiestado)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                    arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
                }
                if (obj.apitransaccion != objOriginal.apitransaccion)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                    arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
                }
                if (obj.usumod != objOriginal.usumod)
                {
                    arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                    arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
                }

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntPacCertificados.Fields.idpcm.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.idpcm == null ? null : "'" + obj.idpcm + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntPacCertificados.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla paccertificados a partir de una clase del tipo EntPacCertificados y su respectiva PK
        /// </summary>
        /// <param name="obj" type="Entidades.EntPacCertificados">
        ///     <para>
        /// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionpaccertificados
        /// </returns>
        public int DeleteQuery(EntPacCertificados obj)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntPacCertificados.Fields.idpcm.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.idpcm == null ? null : "'" + obj.idpcm + "'");

                CConn local = new CConn();
                return local.DeleteBd(EntPacCertificados.StrNombreTabla, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla paccertificados a partir de una clase del tipo EntPacCertificados y su PK
        /// </summary>
        /// <param name="obj" type="Entidades.epaccertificados">
        ///     <para>
        /// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacionpaccertificados
        /// </returns>
        public int DeleteQuery(EntPacCertificados obj, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntPacCertificados.Fields.idpcm.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.idpcm == null ? null : "'" + obj.idpcm + "'");

                CConn local = new CConn();
                return local.DeleteBd(EntPacCertificados.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla paccertificados a partir de una clase del tipo epaccertificados
        /// </summary>
        /// <param name="arrColumnasWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Valores para cada una de las columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionpaccertificados
        /// </returns>
        public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CConn local = new CConn();
                return local.DeleteBd("paccertificados", arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla paccertificados a partir de una clase del tipo epaccertificados
        /// </summary>
        /// <param name="arrColumnasWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Valores para cada una de las columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion paccertificados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacionpaccertificados
        /// </returns>
        public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            try
            {
                CConn local = new CConn();
                return local.DeleteBd(EntPacCertificados.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region Llenado de elementos

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField)
        {
            try
            {
                CargarDropDownList(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntPacCertificados.Fields valueField, String textField)
        {
            try
            {
                CargarDropDownList(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntPacCertificados.Fields valueField, String textField, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField, EntPacCertificados.Fields searchField, object searchValue)
        {
            try
            {
                CargarDropDownList(ref cmb, valueField, textField.ToString(), searchField, searchValue);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntPacCertificados.Fields valueField, String textField, EntPacCertificados.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField, EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntPacCertificados.Fields valueField, String textField, EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntPacCertificados.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntPacCertificados.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Columns.Count > 0)
                {
                    cmb.DataValueField = table.Columns[0].ColumnName;
                    cmb.DataTextField = table.Columns[1].ColumnName;
                    cmb.DataSource = table;
                    cmb.DataBind();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                dtg.DataSource = dsReader;
                dtg.DataBind();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, EntPacCertificados.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, EntPacCertificados.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacCertificados.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacCertificados.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarGridViewOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
                dtg.DataBind();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, String textField)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, String textField, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField, EntPacCertificados.Fields searchField, object searchValue)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, String textField, EntPacCertificados.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField, EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, String textField, EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla paccertificados
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla paccertificados
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                    cmb.DataSource = table;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, EntPacCertificados.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, EntPacCertificados.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntPacCertificados.Fields.idpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.idppa.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fechapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.matriculapcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.textopcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.rtfpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.htmlpcm.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apiestado.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.apitransaccion.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usucre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.feccre.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.usumod.ToString());
                arrColumnas.Add(EntPacCertificados.Fields.fecmod.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacCertificados.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntPacCertificados.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacCertificados.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla paccertificados
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntPacCertificados.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #endregion

        #region Funciones Internas

        /// <summary>
        /// 	 Funcion que devuelve un objeto a partir de un DataRow
        /// </summary>
        /// <param name="row" type="System.Data.DataRow">
        ///     <para>
        /// 		 DataRow con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Objeto paccertificados
        /// </returns>
        internal EntPacCertificados crearObjeto(DataRow row)
        {
            var obj = new EntPacCertificados();
            obj.idpcm = GetColumnType(row[EntPacCertificados.Fields.idpcm.ToString()], EntPacCertificados.Fields.idpcm);
            obj.idppa = GetColumnType(row[EntPacCertificados.Fields.idppa.ToString()], EntPacCertificados.Fields.idppa);
            obj.fechapcm = GetColumnType(row[EntPacCertificados.Fields.fechapcm.ToString()], EntPacCertificados.Fields.fechapcm);
            obj.matriculapcm = GetColumnType(row[EntPacCertificados.Fields.matriculapcm.ToString()], EntPacCertificados.Fields.matriculapcm);
            obj.textopcm = GetColumnType(row[EntPacCertificados.Fields.textopcm.ToString()], EntPacCertificados.Fields.textopcm);
            obj.rtfpcm = GetColumnType(row[EntPacCertificados.Fields.rtfpcm.ToString()], EntPacCertificados.Fields.rtfpcm);
            obj.htmlpcm = GetColumnType(row[EntPacCertificados.Fields.htmlpcm.ToString()], EntPacCertificados.Fields.htmlpcm);
            obj.apiestado = GetColumnType(row[EntPacCertificados.Fields.apiestado.ToString()], EntPacCertificados.Fields.apiestado);
            obj.apitransaccion = GetColumnType(row[EntPacCertificados.Fields.apitransaccion.ToString()], EntPacCertificados.Fields.apitransaccion);
            obj.usucre = GetColumnType(row[EntPacCertificados.Fields.usucre.ToString()], EntPacCertificados.Fields.usucre);
            obj.feccre = GetColumnType(row[EntPacCertificados.Fields.feccre.ToString()], EntPacCertificados.Fields.feccre);
            obj.usumod = GetColumnType(row[EntPacCertificados.Fields.usumod.ToString()], EntPacCertificados.Fields.usumod);
            obj.fecmod = GetColumnType(row[EntPacCertificados.Fields.fecmod.ToString()], EntPacCertificados.Fields.fecmod);
            return obj;
        }

        /// <summary>
        /// 	 Funcion que devuelve un objeto a partir de un DataRow
        /// </summary>
        /// <param name="row" type="System.Data.DataRow">
        ///     <para>
        /// 		 DataRow con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Objeto paccertificados
        /// </returns>
        internal EntPacCertificados crearObjetoRevisado(DataRow row)
        {
            var obj = new EntPacCertificados();
            if (row.Table.Columns.Contains(EntPacCertificados.Fields.idpcm.ToString()))
                obj.idpcm = GetColumnType(row[EntPacCertificados.Fields.idpcm.ToString()], EntPacCertificados.Fields.idpcm);
            if (row.Table.Columns.Contains(EntPacCertificados.Fields.idppa.ToString()))
                obj.idppa = GetColumnType(row[EntPacCertificados.Fields.idppa.ToString()], EntPacCertificados.Fields.idppa);
            if (row.Table.Columns.Contains(EntPacCertificados.Fields.fechapcm.ToString()))
                obj.fechapcm = GetColumnType(row[EntPacCertificados.Fields.fechapcm.ToString()], EntPacCertificados.Fields.fechapcm);
            if (row.Table.Columns.Contains(EntPacCertificados.Fields.matriculapcm.ToString()))
                obj.matriculapcm = GetColumnType(row[EntPacCertificados.Fields.matriculapcm.ToString()], EntPacCertificados.Fields.matriculapcm);
            if (row.Table.Columns.Contains(EntPacCertificados.Fields.textopcm.ToString()))
                obj.textopcm = GetColumnType(row[EntPacCertificados.Fields.textopcm.ToString()], EntPacCertificados.Fields.textopcm);
            if (row.Table.Columns.Contains(EntPacCertificados.Fields.rtfpcm.ToString()))
                obj.rtfpcm = GetColumnType(row[EntPacCertificados.Fields.rtfpcm.ToString()], EntPacCertificados.Fields.rtfpcm);
            if (row.Table.Columns.Contains(EntPacCertificados.Fields.htmlpcm.ToString()))
                obj.htmlpcm = GetColumnType(row[EntPacCertificados.Fields.htmlpcm.ToString()], EntPacCertificados.Fields.htmlpcm);
            if (row.Table.Columns.Contains(EntPacCertificados.Fields.apiestado.ToString()))
                obj.apiestado = GetColumnType(row[EntPacCertificados.Fields.apiestado.ToString()], EntPacCertificados.Fields.apiestado);
            if (row.Table.Columns.Contains(EntPacCertificados.Fields.apitransaccion.ToString()))
                obj.apitransaccion = GetColumnType(row[EntPacCertificados.Fields.apitransaccion.ToString()], EntPacCertificados.Fields.apitransaccion);
            if (row.Table.Columns.Contains(EntPacCertificados.Fields.usucre.ToString()))
                obj.usucre = GetColumnType(row[EntPacCertificados.Fields.usucre.ToString()], EntPacCertificados.Fields.usucre);
            if (row.Table.Columns.Contains(EntPacCertificados.Fields.feccre.ToString()))
                obj.feccre = GetColumnType(row[EntPacCertificados.Fields.feccre.ToString()], EntPacCertificados.Fields.feccre);
            if (row.Table.Columns.Contains(EntPacCertificados.Fields.usumod.ToString()))
                obj.usumod = GetColumnType(row[EntPacCertificados.Fields.usumod.ToString()], EntPacCertificados.Fields.usumod);
            if (row.Table.Columns.Contains(EntPacCertificados.Fields.fecmod.ToString()))
                obj.fecmod = GetColumnType(row[EntPacCertificados.Fields.fecmod.ToString()], EntPacCertificados.Fields.fecmod);
            return obj;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtpaccertificados" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Lista de Objetos paccertificados
        /// </returns>
        internal List<EntPacCertificados> crearLista(DataTable dtpaccertificados)
        {
            var list = new List<EntPacCertificados>();

            foreach (DataRow row in dtpaccertificados.Rows)
            {
                var obj = crearObjeto(row);
                list.Add(obj);
            }
            return list;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
        /// </summary>
        /// <param name="dtpaccertificados" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Lista de Objetos paccertificados
        /// </returns>
        internal List<EntPacCertificados> crearListaRevisada(DataTable dtpaccertificados)
        {
            List<EntPacCertificados> list = new List<EntPacCertificados>();

            foreach (DataRow row in dtpaccertificados.Rows)
            {
                var obj = crearObjetoRevisado(row);
                list.Add(obj);
            }
            return list;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtpaccertificados" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cola de Objetos paccertificados
        /// </returns>
        internal Queue<EntPacCertificados> crearCola(DataTable dtpaccertificados)
        {
            Queue<EntPacCertificados> cola = new Queue<EntPacCertificados>();

            EntPacCertificados obj = new EntPacCertificados();
            foreach (DataRow row in dtpaccertificados.Rows)
            {
                obj = crearObjeto(row);
                cola.Enqueue(obj);
            }
            return cola;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtpaccertificados" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Pila de Objetos paccertificados
        /// </returns>
        internal Stack<EntPacCertificados> crearPila(DataTable dtpaccertificados)
        {
            Stack<EntPacCertificados> pila = new Stack<EntPacCertificados>();

            foreach (DataRow row in dtpaccertificados.Rows)
            {
                var obj = crearObjeto(row);
                pila.Push(obj);
            }
            return pila;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable
        /// </summary>
        /// <param name="dtpaccertificados" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Diccionario de Objetos paccertificados
        /// </returns>
        internal Dictionary<String, EntPacCertificados> crearDiccionario(DataTable dtpaccertificados)
        {
            Dictionary<String, EntPacCertificados> miDic = new Dictionary<String, EntPacCertificados>();

            foreach (DataRow row in dtpaccertificados.Rows)
            {
                var obj = crearObjeto(row);
                miDic.Add(obj.idpcm.ToString(), obj);
            }
            return miDic;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable
        /// </summary>
        /// <param name="dtpaccertificados" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 HashTable de Objetos paccertificados
        /// </returns>
        internal Hashtable crearHashTable(DataTable dtpaccertificados)
        {
            Hashtable miTabla = new Hashtable();

            foreach (DataRow row in dtpaccertificados.Rows)
            {
                var obj = crearObjeto(row);
                miTabla.Add(obj.idpcm.ToString(), obj);
            }
            return miTabla;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
        /// </summary>
        /// <param name="dtpaccertificados" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Diccionario de Objetos paccertificados
        /// </returns>
        internal Dictionary<String, EntPacCertificados> crearDiccionarioRevisado(DataTable dtpaccertificados)
        {
            Dictionary<String, EntPacCertificados> miDic = new Dictionary<String, EntPacCertificados>();

            foreach (DataRow row in dtpaccertificados.Rows)
            {
                var obj = crearObjetoRevisado(row);
                miDic.Add(obj.idpcm.ToString(), obj);
            }
            return miDic;
        }

        internal Dictionary<String, EntPacCertificados> crearDiccionario(DataTable dtpaccertificados, EntPacCertificados.Fields dicKey)
        {
            Dictionary<String, EntPacCertificados> miDic = new Dictionary<String, EntPacCertificados>();

            foreach (DataRow row in dtpaccertificados.Rows)
            {
                var obj = crearObjeto(row);

                var nameOfProperty = dicKey.ToString();
                var propertyInfo = obj.GetType().GetProperty(nameOfProperty);
                var value = propertyInfo.GetValue(obj, null);

                miDic.Add(value.ToString(), obj);
            }
            return miDic;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        protected void Finalize()
        {
            Dispose();
        }

        #endregion
    }
}