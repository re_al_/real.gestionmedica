#region 
/***********************************************************************************************************
	NOMBRE:       RnCalCitas
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla calcitas

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/04/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Interface;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Modelo
{
	public class RnCalCitas: ICalCitas
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region ICalCitas Members

		#region Reflection

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string GetTableScript()
		{
			TableClass tabla = new TableClass(typeof(EntCalCitas));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, EntCalCitas.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntCalCitas).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="strField">Nombre de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, string strField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntCalCitas).GetProperty(strField).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

/// <summary>
/// Inserta una valor a una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">Es el nombre de la propiedad</param>
/// <param name="dynValor">Es el valor que se insertara a la propiedad</param>
public void SetDato(ref EntCalCitas obj, string strPropiedad, dynamic dynValor)
{
	if (obj == null) throw new ArgumentNullException();
	obj.GetType().GetProperty(strPropiedad).SetValue(obj, GetColumnType(dynValor, strPropiedad), null);
}

/// <summary>
/// Obtiene el valor de una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">El nombre de la propiedad de la que se obtendra el valor</param>
/// <returns>Devuelve el valor del a propiedad seleccionada</returns>
public dynamic GetDato(ref EntCalCitas obj, string strPropiedad)
{
	if (obj == null) return null;
	var propertyInfo = obj.GetType().GetProperty(strPropiedad);
	return GetColumnType(propertyInfo.GetValue(obj, null), strPropiedad);
}

		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla calcitas a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla calcitas
		/// </returns>
		public string CreatePk(string[] args)
		{
			return args[0];
		}
		
		#endregion 

		#region ObtenerObjeto

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntCalCitas a partir de la llave primaria
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public EntCalCitas ObtenerObjeto(Int64 Int64idcci)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntCalCitas.Fields.idcci.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64idcci + "'");
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntCalCitas a partir del usuario que inserta
		/// </summary>
		/// <param name="strUsuCre">Login o nombre de usuario</param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public EntCalCitas ObtenerObjetoInsertado(string strUsuCre)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntCalCitas.Fields.usucre.ToString());
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + strUsuCre + "'");
			
			int iIdInsertado = FuncionesMax(EntCalCitas.Fields.idcci, arrColumnasWhere, arrValoresWhere);
			
			return ObtenerObjeto(iIdInsertado);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public EntCalCitas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public EntCalCitas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntCalCitas obj = new EntCalCitas();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public EntCalCitas ObtenerObjeto(Hashtable htbFiltro)
		{
			return ObtenerObjeto(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public EntCalCitas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntCalCitas obj = new EntCalCitas();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public EntCalCitas ObtenerObjeto(EntCalCitas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public EntCalCitas ObtenerObjeto(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo EntCalCitas a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo EntCalCitas
		/// </returns>
		public EntCalCitas ObtenerObjeto(Int64 Int64idcci, ref CTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntCalCitas.Fields.idcci.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64idcci + "'");
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public EntCalCitas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public EntCalCitas ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerObjeto(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public EntCalCitas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public EntCalCitas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					EntCalCitas obj = new EntCalCitas();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public EntCalCitas ObtenerObjeto(EntCalCitas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public EntCalCitas ObtenerObjeto(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion

		#region ObtenerLista

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntCalCitas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerLista(EntCalCitas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerLista(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntCalCitas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerLista(EntCalCitas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerLista(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerLista(Hashtable htbFiltro)
		{
			return ObtenerLista(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntCalCitas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerLista(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntCalCitas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntCalCitas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntCalCitas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerListaDesdeVista(String strVista, EntCalCitas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerListaDesdeVista(String strVista, EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntCalCitas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntCalCitas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerListaDesdeVista(String strVista, EntCalCitas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntCalCitas> ObtenerListaDesdeVista(String strVista, EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerCola y Obtener Pila

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntCalCitas> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntCalCitas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntCalCitas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntCalCitas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntCalCitas> ObtenerCola(EntCalCitas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntCalCitas> ObtenerCola(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntCalCitas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntCalCitas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntCalCitas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntCalCitas> ObtenerCola(EntCalCitas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntCalCitas> ObtenerCola(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntCalCitas> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntCalCitas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntCalCitas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntCalCitas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntCalCitas> ObtenerPila(EntCalCitas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntCalCitas> ObtenerPila(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntCalCitas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntCalCitas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntCalCitas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntCalCitas> ObtenerPila(EntCalCitas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntCalCitas> ObtenerPila(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerDataTable

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla calcitas
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(EntCalCitas.Fields.idcci.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.idcci.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.idppa.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.idppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.asuntocci.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.asuntocci.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.descripcioncci.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.descripcioncci.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.iniciocci.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.iniciocci.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.finalcci.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.finalcci.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.lugarcci.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.lugarcci.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.statuscci.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.statuscci.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.labelcci.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.labelcci.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.motivofaltacci.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.motivofaltacci.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.apiestado.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.apitransaccion.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.apitransaccion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.usucre.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.feccre.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.usumod.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.fecmod.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntCalCitas.Fields.usuasignacion.ToString(),typeof(EntCalCitas).GetProperty(EntCalCitas.Fields.usuasignacion.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una calcitas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla calcitas
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable ObtenerDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE calcitas
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable ObtenerDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla calcitas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla calcitas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla calcitas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla calcitas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla calcitas
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla calcitas
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla calcitas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla calcitas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla calcitas
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla calcitas
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla calcitas
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla calcitas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla calcitas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de calcitas
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableOr(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntCalCitas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntCalCitas.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntCalCitas.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntCalCitas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region ObtenerDiccionario

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntCalCitas> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntCalCitas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntCalCitas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntCalCitas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntCalCitas> ObtenerDiccionario(EntCalCitas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntCalCitas> ObtenerDiccionario(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntCalCitas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntCalCitas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntCalCitas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntCalCitas> ObtenerDiccionario(EntCalCitas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntCalCitas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntCalCitas> ObtenerDiccionario(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		public Dictionary<String, EntCalCitas> ObtenerDiccionarioKey(EntCalCitas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntCalCitas> ObtenerDiccionarioKey(String strParamAdic, EntCalCitas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdic, dicKey);
		}
		
		public Dictionary<String, EntCalCitas> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, EntCalCitas.Fields dicKey)
		{
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, "", dicKey);
		}
		
		public Dictionary<String, EntCalCitas> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, EntCalCitas.Fields dicKey)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table, dicKey);
				}
				else
					return new Dictionary<string, EntCalCitas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		public Dictionary<String, EntCalCitas> ObtenerDiccionarioKey(EntCalCitas.Fields searchField, object searchValue, EntCalCitas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntCalCitas> ObtenerDiccionarioKey(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales, EntCalCitas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey);
		}
		

		#endregion 

		#region ObjetoASp

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla calcitas
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntCalCitas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntCalCitas.Fields.idcci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.idppa.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.finalcci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.statuscci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.labelcci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.apiestado.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.usucre.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.feccre.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.usumod.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.fecmod.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.idcci == null ? null : "'" + obj.idcci + "'");
				arrValoresParam.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValoresParam.Add(obj.asuntocci == null ? null : "'" + obj.asuntocci + "'");
				arrValoresParam.Add(obj.descripcioncci == null ? null : "'" + obj.descripcioncci + "'");
				arrValoresParam.Add(obj.iniciocci == null ? null : "'" + Convert.ToDateTime(obj.iniciocci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.finalcci == null ? null : "'" + Convert.ToDateTime(obj.finalcci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.lugarcci == null ? null : "'" + obj.lugarcci + "'");
				arrValoresParam.Add(obj.statuscci);
				arrValoresParam.Add(obj.labelcci);
				arrValoresParam.Add(obj.motivofaltacci == null ? null : "'" + obj.motivofaltacci + "'");
				arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usuasignacion == null ? null : "'" + obj.usuasignacion + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla calcitas
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntCalCitas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntCalCitas.Fields.idcci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.idppa.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.finalcci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.statuscci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.labelcci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.apiestado.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.usucre.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.feccre.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.usumod.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.fecmod.ToString());
				arrNombreParam.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.idcci == null ? null : "'" + obj.idcci + "'");
				arrValoresParam.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValoresParam.Add(obj.asuntocci == null ? null : "'" + obj.asuntocci + "'");
				arrValoresParam.Add(obj.descripcioncci == null ? null : "'" + obj.descripcioncci + "'");
				arrValoresParam.Add(obj.iniciocci == null ? null : "'" + Convert.ToDateTime(obj.iniciocci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.finalcci == null ? null : "'" + Convert.ToDateTime(obj.finalcci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.lugarcci == null ? null : "'" + obj.lugarcci + "'");
				arrValoresParam.Add(obj.statuscci);
				arrValoresParam.Add(obj.labelcci);
				arrValoresParam.Add(obj.motivofaltacci == null ? null : "'" + obj.motivofaltacci + "'");
				arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usuasignacion == null ? null : "'" + obj.usuasignacion + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region FuncionesAgregadas

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntCalCitas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntCalCitas.Fields refField, EntCalCitas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntCalCitas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntCalCitas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntCalCitas.Fields refField, EntCalCitas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntCalCitas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntCalCitas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntCalCitas.Fields refField, EntCalCitas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntCalCitas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntCalCitas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntCalCitas.Fields refField, EntCalCitas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntCalCitas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntCalCitas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntCalCitas.Fields refField, EntCalCitas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntCalCitas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntCalCitas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs SP

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacioncalcitas
		/// </returns>
		public bool Insert(EntCalCitas obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("idcci");
				arrValoresParam.Add(null);
				arrNombreParam.Add(EntCalCitas.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				arrNombreParam.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrValoresParam.Add(obj.asuntocci);
				
				arrNombreParam.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrValoresParam.Add(obj.descripcioncci);
				
				arrNombreParam.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrValoresParam.Add(obj.iniciocci);
				
				arrNombreParam.Add(EntCalCitas.Fields.finalcci.ToString());
				arrValoresParam.Add(obj.finalcci);
				
				arrNombreParam.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrValoresParam.Add(obj.lugarcci);
				
				arrNombreParam.Add(EntCalCitas.Fields.statuscci.ToString());
				arrValoresParam.Add(obj.statuscci);
				
				arrNombreParam.Add(EntCalCitas.Fields.labelcci.ToString());
				arrValoresParam.Add(obj.labelcci);
				
				arrNombreParam.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrValoresParam.Add(obj.motivofaltacci);
				
				arrNombreParam.Add(EntCalCitas.Fields.usucre.ToString());
				arrValoresParam.Add(obj.usucre);
				
				arrNombreParam.Add(EntCalCitas.Fields.usuasignacion.ToString());
				arrValoresParam.Add(obj.usuasignacion);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.CalCitas.SpCciIns.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacioncalcitas
		/// </returns>
		public bool Insert(EntCalCitas obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("idcci");
				arrValoresParam.Add("");
				arrNombreParam.Add(EntCalCitas.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				arrNombreParam.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrValoresParam.Add(obj.asuntocci);
				
				arrNombreParam.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrValoresParam.Add(obj.descripcioncci);
				
				arrNombreParam.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrValoresParam.Add(obj.iniciocci);
				
				arrNombreParam.Add(EntCalCitas.Fields.finalcci.ToString());
				arrValoresParam.Add(obj.finalcci);
				
				arrNombreParam.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrValoresParam.Add(obj.lugarcci);
				
				arrNombreParam.Add(EntCalCitas.Fields.statuscci.ToString());
				arrValoresParam.Add(obj.statuscci);
				
				arrNombreParam.Add(EntCalCitas.Fields.labelcci.ToString());
				arrValoresParam.Add(obj.labelcci);
				
				arrNombreParam.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrValoresParam.Add(obj.motivofaltacci);
				
				arrNombreParam.Add(EntCalCitas.Fields.usucre.ToString());
				arrValoresParam.Add(obj.usucre);
				
				arrNombreParam.Add(EntCalCitas.Fields.usuasignacion.ToString());
				arrValoresParam.Add(obj.usuasignacion);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.CalCitas.SpCciIns.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor que indica la cantidad de registros actualizados en calcitas
		/// </returns>
		public int Update(EntCalCitas obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntCalCitas.Fields.idcci.ToString());
				arrValoresParam.Add(obj.idcci);
				
				arrNombreParam.Add(EntCalCitas.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				arrNombreParam.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrValoresParam.Add(obj.asuntocci);
				
				arrNombreParam.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrValoresParam.Add(obj.descripcioncci);
				
				arrNombreParam.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrValoresParam.Add(obj.iniciocci);
				
				arrNombreParam.Add(EntCalCitas.Fields.finalcci.ToString());
				arrValoresParam.Add(obj.finalcci);
				
				arrNombreParam.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrValoresParam.Add(obj.lugarcci);
				
				arrNombreParam.Add(EntCalCitas.Fields.statuscci.ToString());
				arrValoresParam.Add(obj.statuscci);
				
				arrNombreParam.Add(EntCalCitas.Fields.labelcci.ToString());
				arrValoresParam.Add(obj.labelcci);
				
				arrNombreParam.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrValoresParam.Add(obj.motivofaltacci);
				
				arrNombreParam.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrValoresParam.Add(obj.apitransaccion);
				
				arrNombreParam.Add(EntCalCitas.Fields.usumod.ToString());
				arrValoresParam.Add(obj.usumod);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.CalCitas.SpCciUpd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacioncalcitas
		/// </returns>
		public int Update(EntCalCitas obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntCalCitas.Fields.idcci.ToString());
				arrValoresParam.Add(obj.idcci);
				
				arrNombreParam.Add(EntCalCitas.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				arrNombreParam.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrValoresParam.Add(obj.asuntocci);
				
				arrNombreParam.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrValoresParam.Add(obj.descripcioncci);
				
				arrNombreParam.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrValoresParam.Add(obj.iniciocci);
				
				arrNombreParam.Add(EntCalCitas.Fields.finalcci.ToString());
				arrValoresParam.Add(obj.finalcci);
				
				arrNombreParam.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrValoresParam.Add(obj.lugarcci);
				
				arrNombreParam.Add(EntCalCitas.Fields.statuscci.ToString());
				arrValoresParam.Add(obj.statuscci);
				
				arrNombreParam.Add(EntCalCitas.Fields.labelcci.ToString());
				arrValoresParam.Add(obj.labelcci);
				
				arrNombreParam.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrValoresParam.Add(obj.motivofaltacci);
				
				arrNombreParam.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrValoresParam.Add(obj.apitransaccion);
				
				arrNombreParam.Add(EntCalCitas.Fields.usumod.ToString());
				arrValoresParam.Add(obj.usumod);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.CalCitas.SpCciUpd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacioncalcitas
		/// </returns>
		public int Delete(EntCalCitas obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntCalCitas.Fields.idcci.ToString());
				arrValoresParam.Add(obj.idcci);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.CalCitas.SpCciDel.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacioncalcitas
		/// </returns>
		public int Delete(EntCalCitas obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntCalCitas.Fields.idcci.ToString());
				arrValoresParam.Add(obj.idcci);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.CalCitas.SpCciDel.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacioncalcitas
		/// </returns>
		public int InsertUpdate(EntCalCitas obj)
		{
			try
			{
				bool esInsertar = true;
				
					esInsertar = (esInsertar && (obj.idcci == null));
				
				if (esInsertar)
					return Insert(obj) ? 1 : 0;
				else
					return Update(obj);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacioncalcitas
		/// </returns>
		public int InsertUpdate(EntCalCitas obj, ref CTrans localTrans)
		{
			try
			{
				bool esInsertar = false;
				
					esInsertar = (esInsertar && (obj.idcci == null));
				
				if (esInsertar)
					return Insert(obj, ref localTrans) ? 1 : 0;
				else
					return Update(obj, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs Query

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacioncalcitas
		/// </returns>
		public bool InsertQuery(EntCalCitas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idcci == null ? null : "'" + obj.idcci + "'");
				arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.asuntocci == null ? null : "'" + obj.asuntocci + "'");
				arrValores.Add(obj.descripcioncci == null ? null : "'" + obj.descripcioncci + "'");
				arrValores.Add(obj.iniciocci == null ? null : "'" + Convert.ToDateTime(obj.iniciocci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.finalcci == null ? null : "'" + Convert.ToDateTime(obj.finalcci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.lugarcci == null ? null : "'" + obj.lugarcci + "'");
				arrValores.Add(obj.statuscci);
				arrValores.Add(obj.labelcci);
				arrValores.Add(obj.motivofaltacci == null ? null : "'" + obj.motivofaltacci + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.usuasignacion == null ? null : "'" + obj.usuasignacion + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntCalCitas.StrNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacioncalcitas
		/// </returns>
		public bool InsertQuery(EntCalCitas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idcci == null ? null : "'" + obj.idcci + "'");
				arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.asuntocci == null ? null : "'" + obj.asuntocci + "'");
				arrValores.Add(obj.descripcioncci == null ? null : "'" + obj.descripcioncci + "'");
				arrValores.Add(obj.iniciocci == null ? null : "'" + Convert.ToDateTime(obj.iniciocci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.finalcci == null ? null : "'" + Convert.ToDateTime(obj.finalcci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.lugarcci == null ? null : "'" + obj.lugarcci + "'");
				arrValores.Add(obj.statuscci);
				arrValores.Add(obj.labelcci);
				arrValores.Add(obj.motivofaltacci == null ? null : "'" + obj.motivofaltacci + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.usuasignacion == null ? null : "'" + obj.usuasignacion + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntCalCitas.StrNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacioncalcitas
		/// </returns>
		public bool InsertQueryIdentity(EntCalCitas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idcci == null ? null : "'" + obj.idcci + "'");
				arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.asuntocci == null ? null : "'" + obj.asuntocci + "'");
				arrValores.Add(obj.descripcioncci == null ? null : "'" + obj.descripcioncci + "'");
				arrValores.Add(obj.iniciocci == null ? null : "'" + Convert.ToDateTime(obj.iniciocci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.finalcci == null ? null : "'" + Convert.ToDateTime(obj.finalcci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.lugarcci == null ? null : "'" + obj.lugarcci + "'");
				arrValores.Add(obj.statuscci);
				arrValores.Add(obj.labelcci);
				arrValores.Add(obj.motivofaltacci == null ? null : "'" + obj.motivofaltacci + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.usuasignacion == null ? null : "'" + obj.usuasignacion + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntCalCitas.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.idcci = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacioncalcitas
		/// </returns>
		public bool InsertQueryIdentity(EntCalCitas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idcci == null ? null : "'" + obj.idcci + "'");
				arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.asuntocci == null ? null : "'" + obj.asuntocci + "'");
				arrValores.Add(obj.descripcioncci == null ? null : "'" + obj.descripcioncci + "'");
				arrValores.Add(obj.iniciocci == null ? null : "'" + Convert.ToDateTime(obj.iniciocci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.finalcci == null ? null : "'" + Convert.ToDateTime(obj.finalcci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.lugarcci == null ? null : "'" + obj.lugarcci + "'");
				arrValores.Add(obj.statuscci);
				arrValores.Add(obj.labelcci);
				arrValores.Add(obj.motivofaltacci == null ? null : "'" + obj.motivofaltacci + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.usuasignacion == null ? null : "'" + obj.usuasignacion + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntCalCitas.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.idcci = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacioncalcitas
		/// </returns>
		public int UpdateQueryAll(EntCalCitas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.asuntocci == null ? null : "'" + obj.asuntocci + "'");
				arrValores.Add(obj.descripcioncci == null ? null : "'" + obj.descripcioncci + "'");
				arrValores.Add(obj.iniciocci == null ? null : "'" + Convert.ToDateTime(obj.iniciocci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.finalcci == null ? null : "'" + Convert.ToDateTime(obj.finalcci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.lugarcci == null ? null : "'" + obj.lugarcci + "'");
				arrValores.Add(obj.statuscci);
				arrValores.Add(obj.labelcci);
				arrValores.Add(obj.motivofaltacci == null ? null : "'" + obj.motivofaltacci + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValores.Add(obj.usuasignacion == null ? null : "'" + obj.usuasignacion + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntCalCitas.Fields.idcci.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idcci == null ? null : "'" + obj.idcci + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntCalCitas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla calcitas a partir de una clase del tipo ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQueryAll(EntCalCitas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.asuntocci == null ? null : "'" + obj.asuntocci + "'");
				arrValores.Add(obj.descripcioncci == null ? null : "'" + obj.descripcioncci + "'");
				arrValores.Add(obj.iniciocci == null ? null : "'" + Convert.ToDateTime(obj.iniciocci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.finalcci == null ? null : "'" + Convert.ToDateTime(obj.finalcci).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.lugarcci == null ? null : "'" + obj.lugarcci + "'");
				arrValores.Add(obj.statuscci);
				arrValores.Add(obj.labelcci);
				arrValores.Add(obj.motivofaltacci == null ? null : "'" + obj.motivofaltacci + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValores.Add(obj.usuasignacion == null ? null : "'" + obj.usuasignacion + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntCalCitas.Fields.idcci.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idcci == null ? null : "'" + obj.idcci + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntCalCitas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla calcitas a partir de una clase del tipo Ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacioncalcitas
		/// </returns>
		public int UpdateQuery(EntCalCitas obj)
		{
			try
			{
				//Obtenemos el Objeto original
				EntCalCitas objOriginal = this.ObtenerObjeto(obj.idcci);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.idppa != objOriginal.idppa )
				{
					arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
					arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				}
				if(obj.asuntocci != objOriginal.asuntocci )
				{
					arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
					arrValores.Add(obj.asuntocci == null ? null : "'" + obj.asuntocci + "'");
				}
				if(obj.descripcioncci != objOriginal.descripcioncci )
				{
					arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
					arrValores.Add(obj.descripcioncci == null ? null : "'" + obj.descripcioncci + "'");
				}
				if(obj.iniciocci != objOriginal.iniciocci )
				{
					arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
					arrValores.Add(obj.iniciocci == null ? null : "'" + Convert.ToDateTime(obj.iniciocci).ToString(CParametros.ParFormatoFechaHora) + "'");
				}
				if(obj.finalcci != objOriginal.finalcci )
				{
					arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
					arrValores.Add(obj.finalcci == null ? null : "'" + Convert.ToDateTime(obj.finalcci).ToString(CParametros.ParFormatoFechaHora) + "'");
				}
				if(obj.lugarcci != objOriginal.lugarcci )
				{
					arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
					arrValores.Add(obj.lugarcci == null ? null : "'" + obj.lugarcci + "'");
				}
				if(obj.statuscci != objOriginal.statuscci )
				{
					arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
					arrValores.Add(obj.statuscci);
				}
				if(obj.labelcci != objOriginal.labelcci )
				{
					arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
					arrValores.Add(obj.labelcci);
				}
				if(obj.motivofaltacci != objOriginal.motivofaltacci )
				{
					arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
					arrValores.Add(obj.motivofaltacci == null ? null : "'" + obj.motivofaltacci + "'");
				}
				if(obj.apiestado != objOriginal.apiestado )
				{
					arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
					arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				}
				if(obj.apitransaccion != objOriginal.apitransaccion )
				{
					arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
					arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				}
				if(obj.usumod != objOriginal.usumod )
				{
					arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
					arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				}
				if(obj.usuasignacion != objOriginal.usuasignacion )
				{
					arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
					arrValores.Add(obj.usuasignacion == null ? null : "'" + obj.usuasignacion + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntCalCitas.Fields.idcci.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idcci == null ? null : "'" + obj.idcci + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntCalCitas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla calcitas a partir de una clase del tipo ecalcitas
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQuery(EntCalCitas obj, ref CTrans localTrans)
		{
			try
			{
				//Obtenemos el Objeto original
				EntCalCitas objOriginal = this.ObtenerObjeto(obj.idcci, ref localTrans);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.idppa != objOriginal.idppa )
				{
					arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
					arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				}
				if(obj.asuntocci != objOriginal.asuntocci )
				{
					arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
					arrValores.Add(obj.asuntocci == null ? null : "'" + obj.asuntocci + "'");
				}
				if(obj.descripcioncci != objOriginal.descripcioncci )
				{
					arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
					arrValores.Add(obj.descripcioncci == null ? null : "'" + obj.descripcioncci + "'");
				}
				if(obj.iniciocci != objOriginal.iniciocci )
				{
					arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
					arrValores.Add(obj.iniciocci == null ? null : "'" + Convert.ToDateTime(obj.iniciocci).ToString(CParametros.ParFormatoFechaHora) + "'");
				}
				if(obj.finalcci != objOriginal.finalcci )
				{
					arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
					arrValores.Add(obj.finalcci == null ? null : "'" + Convert.ToDateTime(obj.finalcci).ToString(CParametros.ParFormatoFechaHora) + "'");
				}
				if(obj.lugarcci != objOriginal.lugarcci )
				{
					arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
					arrValores.Add(obj.lugarcci == null ? null : "'" + obj.lugarcci + "'");
				}
				if(obj.statuscci != objOriginal.statuscci )
				{
					arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
					arrValores.Add(obj.statuscci);
				}
				if(obj.labelcci != objOriginal.labelcci )
				{
					arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
					arrValores.Add(obj.labelcci);
				}
				if(obj.motivofaltacci != objOriginal.motivofaltacci )
				{
					arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
					arrValores.Add(obj.motivofaltacci == null ? null : "'" + obj.motivofaltacci + "'");
				}
				if(obj.apiestado != objOriginal.apiestado )
				{
					arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
					arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				}
				if(obj.apitransaccion != objOriginal.apitransaccion )
				{
					arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
					arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				}
				if(obj.usumod != objOriginal.usumod )
				{
					arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
					arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				}
				if(obj.usuasignacion != objOriginal.usuasignacion )
				{
					arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
					arrValores.Add(obj.usuasignacion == null ? null : "'" + obj.usuasignacion + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntCalCitas.Fields.idcci.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idcci == null ? null : "'" + obj.idcci + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntCalCitas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla calcitas a partir de una clase del tipo EntCalCitas y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.EntCalCitas">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacioncalcitas
		/// </returns>
		public int DeleteQuery(EntCalCitas obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntCalCitas.Fields.idcci.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idcci == null ? null : "'" + obj.idcci + "'");

			
				CConn local = new CConn();
				return local.DeleteBd(EntCalCitas.StrNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla calcitas a partir de una clase del tipo EntCalCitas y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.ecalcitas">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacioncalcitas
		/// </returns>
		public int DeleteQuery(EntCalCitas obj, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntCalCitas.Fields.idcci.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idcci == null ? null : "'" + obj.idcci + "'");

			
				CConn local = new CConn();
				return local.DeleteBd(EntCalCitas.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla calcitas a partir de una clase del tipo ecalcitas
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacioncalcitas
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd("calcitas", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla calcitas a partir de una clase del tipo ecalcitas
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion calcitas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacioncalcitas
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd(EntCalCitas.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region Llenado de elementos

		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, String textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, EntCalCitas.Fields searchField, object searchValue)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, String textField, EntCalCitas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, String textField, EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.DataValueField = table.Columns[0].ColumnName;
					cmb.DataTextField = table.Columns[1].ColumnName;
					cmb.DataSource = table;
					cmb.DataBind();

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntCalCitas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntCalCitas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntCalCitas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntCalCitas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridViewOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, EntCalCitas.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, String textField, EntCalCitas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, String textField, EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla calcitas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla calcitas
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntCalCitas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntCalCitas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntCalCitas.Fields.idcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.idppa.ToString());
				arrColumnas.Add(EntCalCitas.Fields.asuntocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.descripcioncci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.iniciocci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.finalcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.lugarcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.statuscci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.labelcci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.motivofaltacci.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apiestado.ToString());
				arrColumnas.Add(EntCalCitas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usucre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.feccre.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usumod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.fecmod.ToString());
				arrColumnas.Add(EntCalCitas.Fields.usuasignacion.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntCalCitas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntCalCitas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntCalCitas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla calcitas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntCalCitas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 


		#endregion

		#region Funciones Internas

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto calcitas
		/// </returns>
		internal EntCalCitas crearObjeto(DataRow row)
		{
			var obj = new EntCalCitas();
			obj.idcci = GetColumnType(row[EntCalCitas.Fields.idcci.ToString()], EntCalCitas.Fields.idcci);
			obj.idppa = GetColumnType(row[EntCalCitas.Fields.idppa.ToString()], EntCalCitas.Fields.idppa);
			obj.asuntocci = GetColumnType(row[EntCalCitas.Fields.asuntocci.ToString()], EntCalCitas.Fields.asuntocci);
			obj.descripcioncci = GetColumnType(row[EntCalCitas.Fields.descripcioncci.ToString()], EntCalCitas.Fields.descripcioncci);
			obj.iniciocci = GetColumnType(row[EntCalCitas.Fields.iniciocci.ToString()], EntCalCitas.Fields.iniciocci);
			obj.finalcci = GetColumnType(row[EntCalCitas.Fields.finalcci.ToString()], EntCalCitas.Fields.finalcci);
			obj.lugarcci = GetColumnType(row[EntCalCitas.Fields.lugarcci.ToString()], EntCalCitas.Fields.lugarcci);
			obj.statuscci = GetColumnType(row[EntCalCitas.Fields.statuscci.ToString()], EntCalCitas.Fields.statuscci);
			obj.labelcci = GetColumnType(row[EntCalCitas.Fields.labelcci.ToString()], EntCalCitas.Fields.labelcci);
			obj.motivofaltacci = GetColumnType(row[EntCalCitas.Fields.motivofaltacci.ToString()], EntCalCitas.Fields.motivofaltacci);
			obj.apiestado = GetColumnType(row[EntCalCitas.Fields.apiestado.ToString()], EntCalCitas.Fields.apiestado);
			obj.apitransaccion = GetColumnType(row[EntCalCitas.Fields.apitransaccion.ToString()], EntCalCitas.Fields.apitransaccion);
			obj.usucre = GetColumnType(row[EntCalCitas.Fields.usucre.ToString()], EntCalCitas.Fields.usucre);
			obj.feccre = GetColumnType(row[EntCalCitas.Fields.feccre.ToString()], EntCalCitas.Fields.feccre);
			obj.usumod = GetColumnType(row[EntCalCitas.Fields.usumod.ToString()], EntCalCitas.Fields.usumod);
			obj.fecmod = GetColumnType(row[EntCalCitas.Fields.fecmod.ToString()], EntCalCitas.Fields.fecmod);
			obj.usuasignacion = GetColumnType(row[EntCalCitas.Fields.usuasignacion.ToString()], EntCalCitas.Fields.usuasignacion);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto calcitas
		/// </returns>
		internal EntCalCitas crearObjetoRevisado(DataRow row)
		{
			var obj = new EntCalCitas();
			if (row.Table.Columns.Contains(EntCalCitas.Fields.idcci.ToString()))
				obj.idcci = GetColumnType(row[EntCalCitas.Fields.idcci.ToString()], EntCalCitas.Fields.idcci);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.idppa.ToString()))
				obj.idppa = GetColumnType(row[EntCalCitas.Fields.idppa.ToString()], EntCalCitas.Fields.idppa);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.asuntocci.ToString()))
				obj.asuntocci = GetColumnType(row[EntCalCitas.Fields.asuntocci.ToString()], EntCalCitas.Fields.asuntocci);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.descripcioncci.ToString()))
				obj.descripcioncci = GetColumnType(row[EntCalCitas.Fields.descripcioncci.ToString()], EntCalCitas.Fields.descripcioncci);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.iniciocci.ToString()))
				obj.iniciocci = GetColumnType(row[EntCalCitas.Fields.iniciocci.ToString()], EntCalCitas.Fields.iniciocci);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.finalcci.ToString()))
				obj.finalcci = GetColumnType(row[EntCalCitas.Fields.finalcci.ToString()], EntCalCitas.Fields.finalcci);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.lugarcci.ToString()))
				obj.lugarcci = GetColumnType(row[EntCalCitas.Fields.lugarcci.ToString()], EntCalCitas.Fields.lugarcci);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.statuscci.ToString()))
				obj.statuscci = GetColumnType(row[EntCalCitas.Fields.statuscci.ToString()], EntCalCitas.Fields.statuscci);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.labelcci.ToString()))
				obj.labelcci = GetColumnType(row[EntCalCitas.Fields.labelcci.ToString()], EntCalCitas.Fields.labelcci);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.motivofaltacci.ToString()))
				obj.motivofaltacci = GetColumnType(row[EntCalCitas.Fields.motivofaltacci.ToString()], EntCalCitas.Fields.motivofaltacci);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[EntCalCitas.Fields.apiestado.ToString()], EntCalCitas.Fields.apiestado);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.apitransaccion.ToString()))
				obj.apitransaccion = GetColumnType(row[EntCalCitas.Fields.apitransaccion.ToString()], EntCalCitas.Fields.apitransaccion);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[EntCalCitas.Fields.usucre.ToString()], EntCalCitas.Fields.usucre);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[EntCalCitas.Fields.feccre.ToString()], EntCalCitas.Fields.feccre);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[EntCalCitas.Fields.usumod.ToString()], EntCalCitas.Fields.usumod);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[EntCalCitas.Fields.fecmod.ToString()], EntCalCitas.Fields.fecmod);
			if (row.Table.Columns.Contains(EntCalCitas.Fields.usuasignacion.ToString()))
				obj.usuasignacion = GetColumnType(row[EntCalCitas.Fields.usuasignacion.ToString()], EntCalCitas.Fields.usuasignacion);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtcalcitas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos calcitas
		/// </returns>
		internal List<EntCalCitas> crearLista(DataTable dtcalcitas)
		{
			var list = new List<EntCalCitas>();
			
			foreach (DataRow row in dtcalcitas.Rows)
			{
				var obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtcalcitas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos calcitas
		/// </returns>
		internal List<EntCalCitas> crearListaRevisada(DataTable dtcalcitas)
		{
			List<EntCalCitas> list = new List<EntCalCitas>();
			
			foreach (DataRow row in dtcalcitas.Rows)
			{
				var obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtcalcitas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos calcitas
		/// </returns>
		internal Queue<EntCalCitas> crearCola(DataTable dtcalcitas)
		{
			Queue<EntCalCitas> cola = new Queue<EntCalCitas>();
			
			EntCalCitas obj = new EntCalCitas();
			foreach (DataRow row in dtcalcitas.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtcalcitas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos calcitas
		/// </returns>
		internal Stack<EntCalCitas> crearPila(DataTable dtcalcitas)
		{
			Stack<EntCalCitas> pila = new Stack<EntCalCitas>();
			
			foreach (DataRow row in dtcalcitas.Rows)
			{
				var obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtcalcitas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos calcitas
		/// </returns>
		internal Dictionary<String, EntCalCitas> crearDiccionario(DataTable dtcalcitas)
		{
			Dictionary<String, EntCalCitas>  miDic = new Dictionary<String, EntCalCitas>();
			
			foreach (DataRow row in dtcalcitas.Rows)
			{
				var obj = crearObjeto(row);
				miDic.Add(obj.idcci.ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtcalcitas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 HashTable de Objetos calcitas
		/// </returns>
		internal Hashtable crearHashTable(DataTable dtcalcitas)
		{
			Hashtable miTabla = new Hashtable();
			
			foreach (DataRow row in dtcalcitas.Rows)
			{
				var obj = crearObjeto(row);
				miTabla.Add(obj.idcci.ToString(), obj);
			}
			return miTabla;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtcalcitas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos calcitas
		/// </returns>
		internal Dictionary<String, EntCalCitas> crearDiccionarioRevisado(DataTable dtcalcitas)
		{
			Dictionary<String, EntCalCitas>  miDic = new Dictionary<String, EntCalCitas>();
			
			foreach (DataRow row in dtcalcitas.Rows)
			{
				var obj = crearObjetoRevisado(row);
				miDic.Add(obj.idcci.ToString(), obj);
			}
			return miDic;
		}
		
		internal Dictionary<String, EntCalCitas> crearDiccionario(DataTable dtcalcitas, EntCalCitas.Fields dicKey)
		{
			Dictionary<String, EntCalCitas>  miDic = new Dictionary<String, EntCalCitas>();
			
			foreach (DataRow row in dtcalcitas.Rows)
			{
				var obj = crearObjeto(row);
				
				var nameOfProperty = dicKey.ToString();
				var propertyInfo = obj.GetType().GetProperty(nameOfProperty);
				var value = propertyInfo.GetValue(obj, null);
				
				miDic.Add(value.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
		#endregion

	}
}

