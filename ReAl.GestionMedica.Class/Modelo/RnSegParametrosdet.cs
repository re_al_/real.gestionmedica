#region
/***********************************************************************************************************
	NOMBRE:       RnSegParametrosdet
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla segparametrosdet

	REVISIONES:
		Ver        FECHA       Autor            Descripcion
		---------  ----------  ---------------  ------------------------------------
		1.0        22/04/2021  R Alonzo Vera A  Creacion

*************************************************************************************************************/
#endregion

#region

using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Interface;
using System.Windows.Forms;
using System.Web.UI.WebControls;

#endregion

namespace ReAl.GestionMedica.Class.Modelo
{
    public class RnSegParametrosdet : ISegParametrosdet
    {
        //Debe implementar la Interface (Alt + Shift + F10)

        #region ISegParametrosdet Members

        #region Reflection

        /// <summary>
        /// Metodo que devuelve el Script SQL de la Tabla
        /// </summary>
        /// <returns>Script SQL</returns>
        public string GetTableScript()
        {
            TableClass tabla = new TableClass(typeof(EntSegParametrosdet));
            return tabla.CreateTableScript();
        }

        /// <summary>
        /// Metodo para castear Dinamicamente un Tipo
        /// </summary>
        /// <param name="valor">Tipo a ser casteado</param>
        /// <param name="myField">Enum de la columna</param>
        /// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
        public dynamic GetColumnType(object valor, EntSegParametrosdet.Fields myField)
        {
            if (DBNull.Value.Equals(valor))
                return null;
            Type destino = typeof(EntSegParametrosdet).GetProperty(myField.ToString()).PropertyType;
            var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;

            try
            {
                TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
                return tc.ConvertFrom(valor);
            }
            catch (Exception)
            {
                return Convert.ChangeType(valor, miTipo);
            }
        }

        /// <summary>
        /// Metodo para castear Dinamicamente un Tipo
        /// </summary>
        /// <param name="valor">Tipo a ser casteado</param>
        /// <param name="strField">Nombre de la columna</param>
        /// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
        public dynamic GetColumnType(object valor, string strField)
        {
            if (DBNull.Value.Equals(valor))
                return null;
            Type destino = typeof(EntSegParametrosdet).GetProperty(strField).PropertyType;
            var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;

            try
            {
                TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
                return tc.ConvertFrom(valor);
            }
            catch (Exception)
            {
                return Convert.ChangeType(valor, miTipo);
            }
        }

        /// <summary>
        /// Inserta una valor a una propiedad de un objeto instanciado
        /// </summary>
        /// <param name="obj">Objeto instanciado</param>
        /// <param name="strPropiedad">Es el nombre de la propiedad</param>
        /// <param name="dynValor">Es el valor que se insertara a la propiedad</param>
        public void SetDato(ref EntSegParametrosdet obj, string strPropiedad, dynamic dynValor)
        {
            if (obj == null) throw new ArgumentNullException();
            obj.GetType().GetProperty(strPropiedad).SetValue(obj, GetColumnType(dynValor, strPropiedad), null);
        }

        /// <summary>
        /// Obtiene el valor de una propiedad de un objeto instanciado
        /// </summary>
        /// <param name="obj">Objeto instanciado</param>
        /// <param name="strPropiedad">El nombre de la propiedad de la que se obtendra el valor</param>
        /// <returns>Devuelve el valor del a propiedad seleccionada</returns>
        public dynamic GetDato(ref EntSegParametrosdet obj, string strPropiedad)
        {
            if (obj == null) return null;
            var propertyInfo = obj.GetType().GetProperty(strPropiedad);
            return GetColumnType(propertyInfo.GetValue(obj, null), strPropiedad);
        }

        /// <summary>
        /// 	 Funcion que obtiene la llave primaria unica de la tabla segparametrosdet a partir de una cadena
        /// </summary>
        /// <param name="args" type="string[]">
        ///     <para>
        /// 		 Cadena desde la que se construye el identificador unico de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Identificador unico de la tabla segparametrosdet
        /// </returns>
        public string CreatePk(string[] args)
        {
            return args[0];
        }

        #endregion

        #region ObtenerObjeto

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametrosdet a partir de la llave primaria
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametrosdet ObtenerObjeto(string stringsiglaspa, int intcodigospd)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
            arrColumnasWhere.Add(EntSegParametrosdet.Fields.codigospd.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'" + stringsiglaspa + "'");
            arrValoresWhere.Add(intcodigospd);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametrosdet a partir del usuario que inserta
        /// </summary>
        /// <param name="strUsuCre">Login o nombre de usuario</param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametrosdet ObtenerObjetoInsertado(string strUsuCre)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametrosdet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametrosdet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count == 1)
                {
                    EntSegParametrosdet obj = new EntSegParametrosdet();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametrosdet ObtenerObjeto(Hashtable htbFiltro)
        {
            return ObtenerObjeto(htbFiltro, "");
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametrosdet ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count == 1)
                {
                    EntSegParametrosdet obj = new EntSegParametrosdet();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametrosdet ObtenerObjeto(EntSegParametrosdet.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametrosdet ObtenerObjeto(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un Business Object del Tipo EntSegParametrosdet a partir de su llave promaria
        /// </summary>
        /// <returns>
        /// 	Objeto del Tipo EntSegParametrosdet
        /// </returns>
        public EntSegParametrosdet ObtenerObjeto(string stringsiglaspa, int intcodigospd, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
            arrColumnasWhere.Add(EntSegParametrosdet.Fields.codigospd.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'" + stringsiglaspa + "'");
            arrValoresWhere.Add(intcodigospd);
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametrosdet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametrosdet ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans)
        {
            return ObtenerObjeto(htbFiltro, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametrosdet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametrosdet ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count == 1)
                {
                    EntSegParametrosdet obj = new EntSegParametrosdet();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametrosdet ObtenerObjeto(EntSegParametrosdet.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public EntSegParametrosdet ObtenerObjeto(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        #endregion

        #region ObtenerLista

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerLista()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerLista(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntSegParametrosdet>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerLista(EntSegParametrosdet.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerLista(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntSegParametrosdet>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerLista(EntSegParametrosdet.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerLista(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerLista(Hashtable htbFiltro)
        {
            return ObtenerLista(htbFiltro, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntSegParametrosdet>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans)
        {
            return ObtenerLista(htbFiltro, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntSegParametrosdet>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntSegParametrosdet>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
        {
            return ObtenerListaDesdeVista(strVista, htbFiltro, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntSegParametrosdet>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, EntSegParametrosdet.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntSegParametrosdet>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans)
        {
            return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntSegParametrosdet>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, EntSegParametrosdet.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        #endregion

        #region ObtenerCola y Obtener Pila

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametrosdet> ObtenerCola()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerCola(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametrosdet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametrosdet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearCola(table);
                }
                else
                    return new Queue<EntSegParametrosdet>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametrosdet> ObtenerCola(EntSegParametrosdet.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametrosdet> ObtenerCola(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametrosdet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametrosdet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearCola(table);
                }
                else
                    return new Queue<EntSegParametrosdet>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametrosdet> ObtenerCola(EntSegParametrosdet.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntSegParametrosdet> ObtenerCola(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametrosdet> ObtenerPila()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerPila(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametrosdet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametrosdet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearPila(table);
                }
                else
                    return new Stack<EntSegParametrosdet>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametrosdet> ObtenerPila(EntSegParametrosdet.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametrosdet> ObtenerPila(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametrosdet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametrosdet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearPila(table);
                }
                else
                    return new Stack<EntSegParametrosdet>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametrosdet> ObtenerPila(EntSegParametrosdet.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntSegParametrosdet> ObtenerPila(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        #endregion

        #region ObtenerDataTable

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla segparametrosdet
        /// </summary>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable NuevoDataTable()
        {
            try
            {
                DataTable table = new DataTable();
                DataColumn dc;
                dc = new DataColumn(EntSegParametrosdet.Fields.siglaspa.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.siglaspa.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametrosdet.Fields.codigospd.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.codigospd.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametrosdet.Fields.parint1spd.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.parint1spd.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametrosdet.Fields.parint2spd.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.parint2spd.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametrosdet.Fields.parchar1spd.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.parchar1spd.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametrosdet.Fields.parchar2spd.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.parchar2spd.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametrosdet.Fields.parnum1spd.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.parnum1spd.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametrosdet.Fields.parnum2spd.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.parnum2spd.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametrosdet.Fields.parbit1spd.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.parbit1spd.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametrosdet.Fields.parbit2spd.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.parbit2spd.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametrosdet.Fields.apiestadospd.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.apiestadospd.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametrosdet.Fields.apitransaccionspd.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.apitransaccionspd.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametrosdet.Fields.usucrespd.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.usucrespd.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametrosdet.Fields.feccrespd.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.feccrespd.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametrosdet.Fields.usumodspd.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.usumodspd.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntSegParametrosdet.Fields.fecmodspd.ToString(), typeof(EntSegParametrosdet).GetProperty(EntSegParametrosdet.Fields.fecmodspd.ToString()).PropertyType);
                table.Columns.Add(dc);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que genera un DataTable con determinadas columnas de una segparametrosdet
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable NuevoDataTable(ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'2'");

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla segparametrosdet
        /// </summary>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable ObtenerDataTable()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE segparametrosdet
        /// </summary>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable ObtenerDataTable(String strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
        {
            try
            {
                return ObtenerDataTable(arrColumnas, htbFiltro, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable ObtenerDataTable(Hashtable htbFiltro)
        {
            try
            {
                return ObtenerDataTable(htbFiltro, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());
                return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de segparametrosdet
        /// </returns>
        public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableOr(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(EntSegParametrosdet.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(EntSegParametrosdet.Fields searchField, object searchValue, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegParametrosdet.Fields searchField, object searchValue, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegParametrosdet.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region ObtenerDiccionario

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametrosdet> ObtenerDiccionario()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, EntSegParametrosdet>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(EntSegParametrosdet.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, EntSegParametrosdet>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(EntSegParametrosdet.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntSegParametrosdet a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        public Dictionary<String, EntSegParametrosdet> ObtenerDiccionarioKey(EntSegParametrosdet.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
        }

        public Dictionary<String, EntSegParametrosdet> ObtenerDiccionarioKey(String strParamAdic, EntSegParametrosdet.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdic, dicKey);
        }

        public Dictionary<String, EntSegParametrosdet> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, EntSegParametrosdet.Fields dicKey)
        {
            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, "", dicKey);
        }

        public Dictionary<String, EntSegParametrosdet> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, EntSegParametrosdet.Fields dicKey)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table, dicKey);
                }
                else
                    return new Dictionary<string, EntSegParametrosdet>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public Dictionary<String, EntSegParametrosdet> ObtenerDiccionarioKey(EntSegParametrosdet.Fields searchField, object searchValue, EntSegParametrosdet.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
        }

        public Dictionary<String, EntSegParametrosdet> ObtenerDiccionarioKey(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales, EntSegParametrosdet.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey);
        }

        #endregion

        #region ObjetoASp

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="strNombreSp" type="System.string">
        ///     <para>
        /// 		 Nombre del Procedimiento a ejecutar sobre el SP
        ///     </para>
        /// </param>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se va a ejecutar el SP de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor de registros afectados en el Procedimiento de la tabla segparametrosdet
        /// </returns>
        public int EjecutarSpDesdeObjeto(string strNombreSp, EntSegParametrosdet obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrNombreParam = new ArrayList();
                arrNombreParam.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                ArrayList arrValoresParam = new ArrayList();
                arrValoresParam.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");
                arrValoresParam.Add(obj.codigospd);
                arrValoresParam.Add(obj.parint1spd);
                arrValoresParam.Add(obj.parint2spd);
                arrValoresParam.Add(obj.parchar1spd == null ? null : "'" + obj.parchar1spd + "'");
                arrValoresParam.Add(obj.parchar2spd == null ? null : "'" + obj.parchar2spd + "'");
                arrValoresParam.Add(obj.parnum1spd);
                arrValoresParam.Add(obj.parnum2spd);
                arrValoresParam.Add(obj.parbit1spd);
                arrValoresParam.Add(obj.parbit2spd);
                arrValoresParam.Add(obj.apiestadospd == null ? null : "'" + obj.apiestadospd + "'");
                arrValoresParam.Add(obj.apitransaccionspd == null ? null : "'" + obj.apitransaccionspd + "'");
                arrValoresParam.Add(obj.usucrespd == null ? null : "'" + obj.usucrespd + "'");
                arrValoresParam.Add(obj.feccrespd == null ? null : "'" + Convert.ToDateTime(obj.feccrespd).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValoresParam.Add(obj.usumodspd == null ? null : "'" + obj.usumodspd + "'");
                arrValoresParam.Add(obj.fecmodspd == null ? null : "'" + Convert.ToDateTime(obj.fecmodspd).ToString(CParametros.ParFormatoFechaHora) + "'");

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="strNombreSp" type="System.string">
        ///     <para>
        /// 		 Nombre del Procedimiento a ejecutar sobre el SP
        ///     </para>
        /// </param>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se va a ejecutar el SP de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor de registros afectados en el Procedimiento de la tabla segparametrosdet
        /// </returns>
        public int EjecutarSpDesdeObjeto(string strNombreSp, EntSegParametrosdet obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrNombreParam = new ArrayList();
                arrNombreParam.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrNombreParam.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                ArrayList arrValoresParam = new ArrayList();
                arrValoresParam.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");
                arrValoresParam.Add(obj.codigospd);
                arrValoresParam.Add(obj.parint1spd);
                arrValoresParam.Add(obj.parint2spd);
                arrValoresParam.Add(obj.parchar1spd == null ? null : "'" + obj.parchar1spd + "'");
                arrValoresParam.Add(obj.parchar2spd == null ? null : "'" + obj.parchar2spd + "'");
                arrValoresParam.Add(obj.parnum1spd);
                arrValoresParam.Add(obj.parnum2spd);
                arrValoresParam.Add(obj.parbit1spd);
                arrValoresParam.Add(obj.parbit2spd);
                arrValoresParam.Add(obj.apiestadospd == null ? null : "'" + obj.apiestadospd + "'");
                arrValoresParam.Add(obj.apitransaccionspd == null ? null : "'" + obj.apitransaccionspd + "'");
                arrValoresParam.Add(obj.usucrespd == null ? null : "'" + obj.usucrespd + "'");
                arrValoresParam.Add(obj.feccrespd == null ? null : "'" + Convert.ToDateTime(obj.feccrespd).ToString(CParametros.ParFormatoFechaHora) + "'");
                arrValoresParam.Add(obj.usumodspd == null ? null : "'" + obj.usumodspd + "'");
                arrValoresParam.Add(obj.fecmodspd == null ? null : "'" + Convert.ToDateTime(obj.fecmodspd).ToString(CParametros.ParFormatoFechaHora) + "'");

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region FuncionesAgregadas

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesCount(EntSegParametrosdet.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesCount(EntSegParametrosdet.Fields refField, EntSegParametrosdet.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesCount(EntSegParametrosdet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("count(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMin(EntSegParametrosdet.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMin(EntSegParametrosdet.Fields refField, EntSegParametrosdet.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMin(EntSegParametrosdet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("min(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMax(EntSegParametrosdet.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMax(EntSegParametrosdet.Fields refField, EntSegParametrosdet.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMax(EntSegParametrosdet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("max(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesSum(EntSegParametrosdet.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesSum(EntSegParametrosdet.Fields refField, EntSegParametrosdet.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesSum(EntSegParametrosdet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("sum(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesAvg(EntSegParametrosdet.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesAvg(EntSegParametrosdet.Fields refField, EntSegParametrosdet.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntSegParametrosdet que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesAvg(EntSegParametrosdet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("avg(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region ABMs SP

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametrosdet
        /// </returns>
        public bool Insert(EntSegParametrosdet obj, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add("siglaspa");
                arrValoresParam.Add(null);
                arrNombreParam.Add("codigospd");
                arrValoresParam.Add(null);
                arrNombreParam.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrValoresParam.Add(obj.parint1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrValoresParam.Add(obj.parint2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrValoresParam.Add(obj.parchar1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrValoresParam.Add(obj.parchar2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrValoresParam.Add(obj.parnum1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrValoresParam.Add(obj.parnum2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrValoresParam.Add(obj.parbit1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrValoresParam.Add(obj.parbit2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrValoresParam.Add(obj.usucrespd);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegParametrosdet.SpSpdIns.ToString();
                return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam) > 0);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametrosdet
        /// </returns>
        public bool Insert(EntSegParametrosdet obj, ref CTrans localTrans, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add("siglaspa");
                arrValoresParam.Add("");
                arrNombreParam.Add("codigospd");
                arrValoresParam.Add("");
                arrNombreParam.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrValoresParam.Add(obj.parint1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrValoresParam.Add(obj.parint2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrValoresParam.Add(obj.parchar1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrValoresParam.Add(obj.parchar2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrValoresParam.Add(obj.parnum1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrValoresParam.Add(obj.parnum2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrValoresParam.Add(obj.parbit1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrValoresParam.Add(obj.parbit2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrValoresParam.Add(obj.usucrespd);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegParametrosdet.SpSpdIns.ToString();
                return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans) > 0);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor que indica la cantidad de registros actualizados en segparametrosdet
        /// </returns>
        public int Update(EntSegParametrosdet obj, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrValoresParam.Add(obj.siglaspa);

                arrNombreParam.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrValoresParam.Add(obj.codigospd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrValoresParam.Add(obj.parint1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrValoresParam.Add(obj.parint2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrValoresParam.Add(obj.parchar1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrValoresParam.Add(obj.parchar2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrValoresParam.Add(obj.parnum1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrValoresParam.Add(obj.parnum2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrValoresParam.Add(obj.parbit1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrValoresParam.Add(obj.parbit2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrValoresParam.Add(obj.apitransaccionspd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrValoresParam.Add(obj.usumodspd);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegParametrosdet.SpSpdUpd.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametrosdet
        /// </returns>
        public int Update(EntSegParametrosdet obj, ref CTrans localTrans, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrValoresParam.Add(obj.siglaspa);

                arrNombreParam.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrValoresParam.Add(obj.codigospd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrValoresParam.Add(obj.parint1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrValoresParam.Add(obj.parint2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrValoresParam.Add(obj.parchar1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrValoresParam.Add(obj.parchar2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrValoresParam.Add(obj.parnum1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrValoresParam.Add(obj.parnum2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrValoresParam.Add(obj.parbit1spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrValoresParam.Add(obj.parbit2spd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrValoresParam.Add(obj.apitransaccionspd);

                arrNombreParam.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrValoresParam.Add(obj.usumodspd);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegParametrosdet.SpSpdUpd.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametrosdet
        /// </returns>
        public int Delete(EntSegParametrosdet obj, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrValoresParam.Add(obj.siglaspa);

                arrNombreParam.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrValoresParam.Add(obj.codigospd);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegParametrosdet.SpSpdDel.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametrosdet
        /// </returns>
        public int Delete(EntSegParametrosdet obj, ref CTrans localTrans, bool bValidar = true)
        {
            try
            {
                if (bValidar)
                    if (!obj.IsValid())
                        throw new Exception(obj.ValidationErrorsString());
                ArrayList arrNombreParam = new ArrayList();
                ArrayList arrValoresParam = new ArrayList();
                arrNombreParam.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrValoresParam.Add(obj.siglaspa);

                arrNombreParam.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrValoresParam.Add(obj.codigospd);

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                string nombreSp = CListadoSP.SegParametrosdet.SpSpdDel.ToString();
                return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametrosdet
        /// </returns>
        public int InsertUpdate(EntSegParametrosdet obj)
        {
            try
            {
                bool esInsertar = true;

                esInsertar = (esInsertar && (obj.siglaspa == null));
                esInsertar = (esInsertar && (obj.codigospd == null));

                if (esInsertar)
                    return Insert(obj) ? 1 : 0;
                else
                    return Update(obj);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametrosdet
        /// </returns>
        public int InsertUpdate(EntSegParametrosdet obj, ref CTrans localTrans)
        {
            try
            {
                bool esInsertar = false;

                esInsertar = (esInsertar && (obj.siglaspa == null));
                esInsertar = (esInsertar && (obj.codigospd == null));

                if (esInsertar)
                    return Insert(obj, ref localTrans) ? 1 : 0;
                else
                    return Update(obj, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region ABMs Query

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametrosdet
        /// </returns>
        public bool InsertQuery(EntSegParametrosdet obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                //arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                //arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");
                //arrValores.Add(obj.codigospd);
                arrValores.Add(obj.parint1spd);
                arrValores.Add(obj.parint2spd);
                arrValores.Add(obj.parchar1spd == null ? null : "'" + obj.parchar1spd + "'");
                arrValores.Add(obj.parchar2spd == null ? null : "'" + obj.parchar2spd + "'");
                arrValores.Add(obj.parnum1spd);
                arrValores.Add(obj.parnum2spd);
                arrValores.Add(obj.parbit1spd);
                arrValores.Add(obj.parbit2spd);
                arrValores.Add(obj.apiestadospd == null ? null : "'" + obj.apiestadospd + "'");
                arrValores.Add(obj.usucrespd == null ? null : "'" + obj.usucrespd + "'");

                CConn local = new CConn();
                return local.InsertBd(EntSegParametrosdet.StrNombreTabla, arrColumnas, arrValores);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametrosdet
        /// </returns>
        public bool InsertQuery(EntSegParametrosdet obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                //arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                //arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");
                //arrValores.Add(obj.codigospd);
                arrValores.Add(obj.parint1spd);
                arrValores.Add(obj.parint2spd);
                arrValores.Add(obj.parchar1spd == null ? null : "'" + obj.parchar1spd + "'");
                arrValores.Add(obj.parchar2spd == null ? null : "'" + obj.parchar2spd + "'");
                arrValores.Add(obj.parnum1spd);
                arrValores.Add(obj.parnum2spd);
                arrValores.Add(obj.parbit1spd);
                arrValores.Add(obj.parbit2spd);
                arrValores.Add(obj.apiestadospd == null ? null : "'" + obj.apiestadospd + "'");
                arrValores.Add(obj.usucrespd == null ? null : "'" + obj.usucrespd + "'");

                CConn local = new CConn();
                return local.InsertBd(EntSegParametrosdet.StrNombreTabla, arrColumnas, arrValores, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametrosdet
        /// </returns>
        public bool InsertQueryIdentity(EntSegParametrosdet obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionsegparametrosdet
        /// </returns>
        public bool InsertQueryIdentity(EntSegParametrosdet obj, ref CTrans localTrans)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionsegparametrosdet
        /// </returns>
        public int UpdateQueryAll(EntSegParametrosdet obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.parint1spd);
                arrValores.Add(obj.parint2spd);
                arrValores.Add(obj.parchar1spd == null ? null : "'" + obj.parchar1spd + "'");
                arrValores.Add(obj.parchar2spd == null ? null : "'" + obj.parchar2spd + "'");
                arrValores.Add(obj.parnum1spd);
                arrValores.Add(obj.parnum2spd);
                arrValores.Add(obj.parbit1spd);
                arrValores.Add(obj.parbit2spd);
                arrValores.Add(obj.apiestadospd == null ? null : "'" + obj.apiestadospd + "'");
                arrValores.Add(obj.apitransaccionspd == null ? null : "'" + obj.apitransaccionspd + "'");
                arrValores.Add(obj.usumodspd == null ? null : "'" + obj.usumodspd + "'");

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnasWhere.Add(EntSegParametrosdet.Fields.codigospd.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");
                arrValoresWhere.Add(obj.codigospd);

                CConn local = new CConn();
                return local.UpdateBd(EntSegParametrosdet.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla segparametrosdet a partir de una clase del tipo esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacion
        /// </returns>
        public int UpdateQueryAll(EntSegParametrosdet obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.parint1spd);
                arrValores.Add(obj.parint2spd);
                arrValores.Add(obj.parchar1spd == null ? null : "'" + obj.parchar1spd + "'");
                arrValores.Add(obj.parchar2spd == null ? null : "'" + obj.parchar2spd + "'");
                arrValores.Add(obj.parnum1spd);
                arrValores.Add(obj.parnum2spd);
                arrValores.Add(obj.parbit1spd);
                arrValores.Add(obj.parbit2spd);
                arrValores.Add(obj.apiestadospd == null ? null : "'" + obj.apiestadospd + "'");
                arrValores.Add(obj.apitransaccionspd == null ? null : "'" + obj.apitransaccionspd + "'");
                arrValores.Add(obj.usumodspd == null ? null : "'" + obj.usumodspd + "'");

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnasWhere.Add(EntSegParametrosdet.Fields.codigospd.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");
                arrValoresWhere.Add(obj.codigospd);

                CConn local = new CConn();
                return local.UpdateBd(EntSegParametrosdet.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla segparametrosdet a partir de una clase del tipo Esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionsegparametrosdet
        /// </returns>
        public int UpdateQuery(EntSegParametrosdet obj)
        {
            try
            {
                //Obtenemos el Objeto original
                EntSegParametrosdet objOriginal = this.ObtenerObjeto(obj.siglaspa, obj.codigospd);

                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                ArrayList arrValores = new ArrayList();

                if (obj.parint1spd != objOriginal.parint1spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                    arrValores.Add(obj.parint1spd);
                }
                if (obj.parint2spd != objOriginal.parint2spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                    arrValores.Add(obj.parint2spd);
                }
                if (obj.parchar1spd != objOriginal.parchar1spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                    arrValores.Add(obj.parchar1spd == null ? null : "'" + obj.parchar1spd + "'");
                }
                if (obj.parchar2spd != objOriginal.parchar2spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                    arrValores.Add(obj.parchar2spd == null ? null : "'" + obj.parchar2spd + "'");
                }
                if (obj.parnum1spd != objOriginal.parnum1spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                    arrValores.Add(obj.parnum1spd);
                }
                if (obj.parnum2spd != objOriginal.parnum2spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                    arrValores.Add(obj.parnum2spd);
                }
                if (obj.parbit1spd != objOriginal.parbit1spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                    arrValores.Add(obj.parbit1spd);
                }
                if (obj.parbit2spd != objOriginal.parbit2spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                    arrValores.Add(obj.parbit2spd);
                }
                if (obj.apiestadospd != objOriginal.apiestadospd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                    arrValores.Add(obj.apiestadospd == null ? null : "'" + obj.apiestadospd + "'");
                }
                if (obj.apitransaccionspd != objOriginal.apitransaccionspd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                    arrValores.Add(obj.apitransaccionspd == null ? null : "'" + obj.apitransaccionspd + "'");
                }
                if (obj.usumodspd != objOriginal.usumodspd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                    arrValores.Add(obj.usumodspd == null ? null : "'" + obj.usumodspd + "'");
                }

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnasWhere.Add(EntSegParametrosdet.Fields.codigospd.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");
                arrValoresWhere.Add(obj.codigospd);

                CConn local = new CConn();
                return local.UpdateBd(EntSegParametrosdet.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla segparametrosdet a partir de una clase del tipo esegparametrosdet
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacion
        /// </returns>
        public int UpdateQuery(EntSegParametrosdet obj, ref CTrans localTrans)
        {
            try
            {
                //Obtenemos el Objeto original
                EntSegParametrosdet objOriginal = this.ObtenerObjeto(obj.siglaspa, obj.codigospd, ref localTrans);

                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                ArrayList arrValores = new ArrayList();

                if (obj.parint1spd != objOriginal.parint1spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                    arrValores.Add(obj.parint1spd);
                }
                if (obj.parint2spd != objOriginal.parint2spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                    arrValores.Add(obj.parint2spd);
                }
                if (obj.parchar1spd != objOriginal.parchar1spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                    arrValores.Add(obj.parchar1spd == null ? null : "'" + obj.parchar1spd + "'");
                }
                if (obj.parchar2spd != objOriginal.parchar2spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                    arrValores.Add(obj.parchar2spd == null ? null : "'" + obj.parchar2spd + "'");
                }
                if (obj.parnum1spd != objOriginal.parnum1spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                    arrValores.Add(obj.parnum1spd);
                }
                if (obj.parnum2spd != objOriginal.parnum2spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                    arrValores.Add(obj.parnum2spd);
                }
                if (obj.parbit1spd != objOriginal.parbit1spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                    arrValores.Add(obj.parbit1spd);
                }
                if (obj.parbit2spd != objOriginal.parbit2spd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                    arrValores.Add(obj.parbit2spd);
                }
                if (obj.apiestadospd != objOriginal.apiestadospd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                    arrValores.Add(obj.apiestadospd == null ? null : "'" + obj.apiestadospd + "'");
                }
                if (obj.apitransaccionspd != objOriginal.apitransaccionspd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                    arrValores.Add(obj.apitransaccionspd == null ? null : "'" + obj.apitransaccionspd + "'");
                }
                if (obj.usumodspd != objOriginal.usumodspd)
                {
                    arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                    arrValores.Add(obj.usumodspd == null ? null : "'" + obj.usumodspd + "'");
                }

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnasWhere.Add(EntSegParametrosdet.Fields.codigospd.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");
                arrValoresWhere.Add(obj.codigospd);

                CConn local = new CConn();
                return local.UpdateBd(EntSegParametrosdet.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla segparametrosdet a partir de una clase del tipo EntSegParametrosdet y su respectiva PK
        /// </summary>
        /// <param name="obj" type="Entidades.EntSegParametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionsegparametrosdet
        /// </returns>
        public int DeleteQuery(EntSegParametrosdet obj)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnasWhere.Add(EntSegParametrosdet.Fields.codigospd.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");
                arrValoresWhere.Add(obj.codigospd);

                CConn local = new CConn();
                return local.DeleteBd(EntSegParametrosdet.StrNombreTabla, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla segparametrosdet a partir de una clase del tipo EntSegParametrosdet y su PK
        /// </summary>
        /// <param name="obj" type="Entidades.esegparametrosdet">
        ///     <para>
        /// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacionsegparametrosdet
        /// </returns>
        public int DeleteQuery(EntSegParametrosdet obj, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnasWhere.Add(EntSegParametrosdet.Fields.codigospd.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.siglaspa == null ? null : "'" + obj.siglaspa + "'");
                arrValoresWhere.Add(obj.codigospd);

                CConn local = new CConn();
                return local.DeleteBd(EntSegParametrosdet.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla segparametrosdet a partir de una clase del tipo esegparametrosdet
        /// </summary>
        /// <param name="arrColumnasWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Valores para cada una de las columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionsegparametrosdet
        /// </returns>
        public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CConn local = new CConn();
                return local.DeleteBd("segparametrosdet", arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla segparametrosdet a partir de una clase del tipo esegparametrosdet
        /// </summary>
        /// <param name="arrColumnasWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Valores para cada una de las columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion segparametrosdet
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacionsegparametrosdet
        /// </returns>
        public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            try
            {
                CConn local = new CConn();
                return local.DeleteBd(EntSegParametrosdet.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region Llenado de elementos

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField)
        {
            try
            {
                CargarDropDownList(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, String textField)
        {
            try
            {
                CargarDropDownList(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, String textField, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, EntSegParametrosdet.Fields searchField, object searchValue)
        {
            try
            {
                CargarDropDownList(ref cmb, valueField, textField.ToString(), searchField, searchValue);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, String textField, EntSegParametrosdet.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, String textField, EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Columns.Count > 0)
                {
                    cmb.DataValueField = table.Columns[0].ColumnName;
                    cmb.DataTextField = table.Columns[1].ColumnName;
                    cmb.DataSource = table;
                    cmb.DataBind();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                dtg.DataSource = dsReader;
                dtg.DataBind();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, EntSegParametrosdet.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, EntSegParametrosdet.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegParametrosdet.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegParametrosdet.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarGridViewOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
                dtg.DataBind();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, String textField)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, String textField, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, EntSegParametrosdet.Fields searchField, object searchValue)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, String textField, EntSegParametrosdet.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, String textField, EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla segparametrosdet
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segparametrosdet
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                    cmb.DataSource = table;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, EntSegParametrosdet.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, EntSegParametrosdet.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntSegParametrosdet.Fields.siglaspa.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.codigospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parint2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parchar2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parnum2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit1spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.parbit2spd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apiestadospd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.apitransaccionspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usucrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.feccrespd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.usumodspd.ToString());
                arrColumnas.Add(EntSegParametrosdet.Fields.fecmodspd.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegParametrosdet.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntSegParametrosdet.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegParametrosdet.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla segparametrosdet
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntSegParametrosdet.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #endregion

        #region Funciones Internas

        /// <summary>
        /// 	 Funcion que devuelve un objeto a partir de un DataRow
        /// </summary>
        /// <param name="row" type="System.Data.DataRow">
        ///     <para>
        /// 		 DataRow con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Objeto segparametrosdet
        /// </returns>
        internal EntSegParametrosdet crearObjeto(DataRow row)
        {
            var obj = new EntSegParametrosdet();
            obj.siglaspa = GetColumnType(row[EntSegParametrosdet.Fields.siglaspa.ToString()], EntSegParametrosdet.Fields.siglaspa);
            obj.codigospd = GetColumnType(row[EntSegParametrosdet.Fields.codigospd.ToString()], EntSegParametrosdet.Fields.codigospd);
            obj.parint1spd = GetColumnType(row[EntSegParametrosdet.Fields.parint1spd.ToString()], EntSegParametrosdet.Fields.parint1spd);
            obj.parint2spd = GetColumnType(row[EntSegParametrosdet.Fields.parint2spd.ToString()], EntSegParametrosdet.Fields.parint2spd);
            obj.parchar1spd = GetColumnType(row[EntSegParametrosdet.Fields.parchar1spd.ToString()], EntSegParametrosdet.Fields.parchar1spd);
            obj.parchar2spd = GetColumnType(row[EntSegParametrosdet.Fields.parchar2spd.ToString()], EntSegParametrosdet.Fields.parchar2spd);
            obj.parnum1spd = GetColumnType(row[EntSegParametrosdet.Fields.parnum1spd.ToString()], EntSegParametrosdet.Fields.parnum1spd);
            obj.parnum2spd = GetColumnType(row[EntSegParametrosdet.Fields.parnum2spd.ToString()], EntSegParametrosdet.Fields.parnum2spd);
            obj.parbit1spd = GetColumnType(row[EntSegParametrosdet.Fields.parbit1spd.ToString()], EntSegParametrosdet.Fields.parbit1spd);
            obj.parbit2spd = GetColumnType(row[EntSegParametrosdet.Fields.parbit2spd.ToString()], EntSegParametrosdet.Fields.parbit2spd);
            obj.apiestadospd = GetColumnType(row[EntSegParametrosdet.Fields.apiestadospd.ToString()], EntSegParametrosdet.Fields.apiestadospd);
            obj.apitransaccionspd = GetColumnType(row[EntSegParametrosdet.Fields.apitransaccionspd.ToString()], EntSegParametrosdet.Fields.apitransaccionspd);
            obj.usucrespd = GetColumnType(row[EntSegParametrosdet.Fields.usucrespd.ToString()], EntSegParametrosdet.Fields.usucrespd);
            obj.feccrespd = GetColumnType(row[EntSegParametrosdet.Fields.feccrespd.ToString()], EntSegParametrosdet.Fields.feccrespd);
            obj.usumodspd = GetColumnType(row[EntSegParametrosdet.Fields.usumodspd.ToString()], EntSegParametrosdet.Fields.usumodspd);
            obj.fecmodspd = GetColumnType(row[EntSegParametrosdet.Fields.fecmodspd.ToString()], EntSegParametrosdet.Fields.fecmodspd);
            return obj;
        }

        /// <summary>
        /// 	 Funcion que devuelve un objeto a partir de un DataRow
        /// </summary>
        /// <param name="row" type="System.Data.DataRow">
        ///     <para>
        /// 		 DataRow con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Objeto segparametrosdet
        /// </returns>
        internal EntSegParametrosdet crearObjetoRevisado(DataRow row)
        {
            var obj = new EntSegParametrosdet();
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.siglaspa.ToString()))
                obj.siglaspa = GetColumnType(row[EntSegParametrosdet.Fields.siglaspa.ToString()], EntSegParametrosdet.Fields.siglaspa);
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.codigospd.ToString()))
                obj.codigospd = GetColumnType(row[EntSegParametrosdet.Fields.codigospd.ToString()], EntSegParametrosdet.Fields.codigospd);
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.parint1spd.ToString()))
                obj.parint1spd = GetColumnType(row[EntSegParametrosdet.Fields.parint1spd.ToString()], EntSegParametrosdet.Fields.parint1spd);
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.parint2spd.ToString()))
                obj.parint2spd = GetColumnType(row[EntSegParametrosdet.Fields.parint2spd.ToString()], EntSegParametrosdet.Fields.parint2spd);
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.parchar1spd.ToString()))
                obj.parchar1spd = GetColumnType(row[EntSegParametrosdet.Fields.parchar1spd.ToString()], EntSegParametrosdet.Fields.parchar1spd);
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.parchar2spd.ToString()))
                obj.parchar2spd = GetColumnType(row[EntSegParametrosdet.Fields.parchar2spd.ToString()], EntSegParametrosdet.Fields.parchar2spd);
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.parnum1spd.ToString()))
                obj.parnum1spd = GetColumnType(row[EntSegParametrosdet.Fields.parnum1spd.ToString()], EntSegParametrosdet.Fields.parnum1spd);
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.parnum2spd.ToString()))
                obj.parnum2spd = GetColumnType(row[EntSegParametrosdet.Fields.parnum2spd.ToString()], EntSegParametrosdet.Fields.parnum2spd);
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.parbit1spd.ToString()))
                obj.parbit1spd = GetColumnType(row[EntSegParametrosdet.Fields.parbit1spd.ToString()], EntSegParametrosdet.Fields.parbit1spd);
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.parbit2spd.ToString()))
                obj.parbit2spd = GetColumnType(row[EntSegParametrosdet.Fields.parbit2spd.ToString()], EntSegParametrosdet.Fields.parbit2spd);
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.apiestadospd.ToString()))
                obj.apiestadospd = GetColumnType(row[EntSegParametrosdet.Fields.apiestadospd.ToString()], EntSegParametrosdet.Fields.apiestadospd);
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.apitransaccionspd.ToString()))
                obj.apitransaccionspd = GetColumnType(row[EntSegParametrosdet.Fields.apitransaccionspd.ToString()], EntSegParametrosdet.Fields.apitransaccionspd);
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.usucrespd.ToString()))
                obj.usucrespd = GetColumnType(row[EntSegParametrosdet.Fields.usucrespd.ToString()], EntSegParametrosdet.Fields.usucrespd);
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.feccrespd.ToString()))
                obj.feccrespd = GetColumnType(row[EntSegParametrosdet.Fields.feccrespd.ToString()], EntSegParametrosdet.Fields.feccrespd);
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.usumodspd.ToString()))
                obj.usumodspd = GetColumnType(row[EntSegParametrosdet.Fields.usumodspd.ToString()], EntSegParametrosdet.Fields.usumodspd);
            if (row.Table.Columns.Contains(EntSegParametrosdet.Fields.fecmodspd.ToString()))
                obj.fecmodspd = GetColumnType(row[EntSegParametrosdet.Fields.fecmodspd.ToString()], EntSegParametrosdet.Fields.fecmodspd);
            return obj;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtsegparametrosdet" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Lista de Objetos segparametrosdet
        /// </returns>
        internal List<EntSegParametrosdet> crearLista(DataTable dtsegparametrosdet)
        {
            var list = new List<EntSegParametrosdet>();

            foreach (DataRow row in dtsegparametrosdet.Rows)
            {
                var obj = crearObjeto(row);
                list.Add(obj);
            }
            return list;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
        /// </summary>
        /// <param name="dtsegparametrosdet" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Lista de Objetos segparametrosdet
        /// </returns>
        internal List<EntSegParametrosdet> crearListaRevisada(DataTable dtsegparametrosdet)
        {
            List<EntSegParametrosdet> list = new List<EntSegParametrosdet>();

            foreach (DataRow row in dtsegparametrosdet.Rows)
            {
                var obj = crearObjetoRevisado(row);
                list.Add(obj);
            }
            return list;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtsegparametrosdet" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cola de Objetos segparametrosdet
        /// </returns>
        internal Queue<EntSegParametrosdet> crearCola(DataTable dtsegparametrosdet)
        {
            Queue<EntSegParametrosdet> cola = new Queue<EntSegParametrosdet>();

            EntSegParametrosdet obj = new EntSegParametrosdet();
            foreach (DataRow row in dtsegparametrosdet.Rows)
            {
                obj = crearObjeto(row);
                cola.Enqueue(obj);
            }
            return cola;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtsegparametrosdet" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Pila de Objetos segparametrosdet
        /// </returns>
        internal Stack<EntSegParametrosdet> crearPila(DataTable dtsegparametrosdet)
        {
            Stack<EntSegParametrosdet> pila = new Stack<EntSegParametrosdet>();

            foreach (DataRow row in dtsegparametrosdet.Rows)
            {
                var obj = crearObjeto(row);
                pila.Push(obj);
            }
            return pila;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable
        /// </summary>
        /// <param name="dtsegparametrosdet" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Diccionario de Objetos segparametrosdet
        /// </returns>
        internal Dictionary<String, EntSegParametrosdet> crearDiccionario(DataTable dtsegparametrosdet)
        {
            Dictionary<String, EntSegParametrosdet> miDic = new Dictionary<String, EntSegParametrosdet>();

            foreach (DataRow row in dtsegparametrosdet.Rows)
            {
                var obj = crearObjeto(row);
                miDic.Add(obj.GetHashCode().ToString(), obj);
            }
            return miDic;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable
        /// </summary>
        /// <param name="dtsegparametrosdet" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 HashTable de Objetos segparametrosdet
        /// </returns>
        internal Hashtable crearHashTable(DataTable dtsegparametrosdet)
        {
            Hashtable miTabla = new Hashtable();

            foreach (DataRow row in dtsegparametrosdet.Rows)
            {
                var obj = crearObjeto(row);
                miTabla.Add(obj.GetHashCode().ToString(), obj);
            }
            return miTabla;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
        /// </summary>
        /// <param name="dtsegparametrosdet" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Diccionario de Objetos segparametrosdet
        /// </returns>
        internal Dictionary<String, EntSegParametrosdet> crearDiccionarioRevisado(DataTable dtsegparametrosdet)
        {
            Dictionary<String, EntSegParametrosdet> miDic = new Dictionary<String, EntSegParametrosdet>();

            foreach (DataRow row in dtsegparametrosdet.Rows)
            {
                var obj = crearObjetoRevisado(row);
                miDic.Add(obj.GetHashCode().ToString(), obj);
            }
            return miDic;
        }

        internal Dictionary<String, EntSegParametrosdet> crearDiccionario(DataTable dtsegparametrosdet, EntSegParametrosdet.Fields dicKey)
        {
            Dictionary<String, EntSegParametrosdet> miDic = new Dictionary<String, EntSegParametrosdet>();

            foreach (DataRow row in dtsegparametrosdet.Rows)
            {
                var obj = crearObjeto(row);

                var nameOfProperty = dicKey.ToString();
                var propertyInfo = obj.GetType().GetProperty(nameOfProperty);
                var value = propertyInfo.GetValue(obj, null);

                miDic.Add(value.ToString(), obj);
            }
            return miDic;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        protected void Finalize()
        {
            Dispose();
        }

        #endregion
    }
}