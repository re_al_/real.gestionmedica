#region 
/***********************************************************************************************************
	NOMBRE:       RnClaPlantillas
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla claplantillas

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        16/07/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Interface;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Modelo
{
	public class RnClaPlantillas: IClaPlantillas
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region IClaPlantillas Members

		#region Reflection

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string GetTableScript()
		{
			TableClass tabla = new TableClass(typeof(EntClaPlantillas));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, EntClaPlantillas.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntClaPlantillas).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="strField">Nombre de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, string strField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntClaPlantillas).GetProperty(strField).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

/// <summary>
/// Inserta una valor a una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">Es el nombre de la propiedad</param>
/// <param name="dynValor">Es el valor que se insertara a la propiedad</param>
public void SetDato(ref EntClaPlantillas obj, string strPropiedad, dynamic dynValor)
{
	if (obj == null) throw new ArgumentNullException();
	obj.GetType().GetProperty(strPropiedad).SetValue(obj, GetColumnType(dynValor, strPropiedad), null);
}

/// <summary>
/// Obtiene el valor de una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">El nombre de la propiedad de la que se obtendra el valor</param>
/// <returns>Devuelve el valor del a propiedad seleccionada</returns>
public dynamic GetDato(ref EntClaPlantillas obj, string strPropiedad)
{
	if (obj == null) return null;
	var propertyInfo = obj.GetType().GetProperty(strPropiedad);
	return GetColumnType(propertyInfo.GetValue(obj, null), strPropiedad);
}

		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla claplantillas a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla claplantillas
		/// </returns>
		public string CreatePk(string[] args)
		{
			return args[0];
		}
		
		#endregion 

		#region ObtenerObjeto

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntClaPlantillas a partir de la llave primaria
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public EntClaPlantillas ObtenerObjeto(int intidcpl)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntClaPlantillas.Fields.idcpl.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intidcpl);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntClaPlantillas a partir del usuario que inserta
		/// </summary>
		/// <param name="strUsuCre">Login o nombre de usuario</param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public EntClaPlantillas ObtenerObjetoInsertado(string strUsuCre)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntClaPlantillas.Fields.usucre.ToString());
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + strUsuCre + "'");
			
			int iIdInsertado = FuncionesMax(EntClaPlantillas.Fields.idcpl, arrColumnasWhere, arrValoresWhere);
			
			return ObtenerObjeto(iIdInsertado);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public EntClaPlantillas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public EntClaPlantillas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntClaPlantillas obj = new EntClaPlantillas();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public EntClaPlantillas ObtenerObjeto(Hashtable htbFiltro)
		{
			return ObtenerObjeto(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public EntClaPlantillas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntClaPlantillas obj = new EntClaPlantillas();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public EntClaPlantillas ObtenerObjeto(EntClaPlantillas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public EntClaPlantillas ObtenerObjeto(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo EntClaPlantillas a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo EntClaPlantillas
		/// </returns>
		public EntClaPlantillas ObtenerObjeto(int intidcpl, ref CTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntClaPlantillas.Fields.idcpl.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intidcpl);
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public EntClaPlantillas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public EntClaPlantillas ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerObjeto(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public EntClaPlantillas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public EntClaPlantillas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					EntClaPlantillas obj = new EntClaPlantillas();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public EntClaPlantillas ObtenerObjeto(EntClaPlantillas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public EntClaPlantillas ObtenerObjeto(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion

		#region ObtenerLista

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntClaPlantillas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerLista(EntClaPlantillas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerLista(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntClaPlantillas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerLista(EntClaPlantillas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerLista(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerLista(Hashtable htbFiltro)
		{
			return ObtenerLista(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntClaPlantillas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerLista(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntClaPlantillas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntClaPlantillas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntClaPlantillas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, EntClaPlantillas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntClaPlantillas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntClaPlantillas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, EntClaPlantillas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerCola y Obtener Pila

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntClaPlantillas> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntClaPlantillas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntClaPlantillas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntClaPlantillas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntClaPlantillas> ObtenerCola(EntClaPlantillas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntClaPlantillas> ObtenerCola(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntClaPlantillas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntClaPlantillas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntClaPlantillas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntClaPlantillas> ObtenerCola(EntClaPlantillas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntClaPlantillas> ObtenerCola(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntClaPlantillas> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntClaPlantillas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntClaPlantillas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntClaPlantillas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntClaPlantillas> ObtenerPila(EntClaPlantillas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntClaPlantillas> ObtenerPila(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntClaPlantillas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntClaPlantillas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntClaPlantillas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntClaPlantillas> ObtenerPila(EntClaPlantillas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntClaPlantillas> ObtenerPila(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerDataTable

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla claplantillas
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(EntClaPlantillas.Fields.idcpl.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.idcpl.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.idctp.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.idctp.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.nombrecpl.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.nombrecpl.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.descripcioncpl.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.descripcioncpl.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.altocpl.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.altocpl.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.anchocpl.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.anchocpl.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.margensupcpl.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.margensupcpl.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.margeninfcpl.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.margeninfcpl.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.margendercpl.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.margendercpl.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.margenizqcpl.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.margenizqcpl.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.textocpl.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.textocpl.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.rtfcpl.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.rtfcpl.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.apiestado.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.apitransaccion.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.apitransaccion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.usucre.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.feccre.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.usumod.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.fecmod.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntClaPlantillas.Fields.htmlcpl.ToString(),typeof(EntClaPlantillas).GetProperty(EntClaPlantillas.Fields.htmlcpl.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una claplantillas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla claplantillas
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable ObtenerDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE claplantillas
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable ObtenerDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de claplantillas
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableOr(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntClaPlantillas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntClaPlantillas.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntClaPlantillas.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntClaPlantillas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region ObtenerDiccionario

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntClaPlantillas> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntClaPlantillas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntClaPlantillas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntClaPlantillas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntClaPlantillas> ObtenerDiccionario(EntClaPlantillas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntClaPlantillas> ObtenerDiccionario(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntClaPlantillas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntClaPlantillas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntClaPlantillas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntClaPlantillas> ObtenerDiccionario(EntClaPlantillas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntClaPlantillas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntClaPlantillas> ObtenerDiccionario(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		public Dictionary<String, EntClaPlantillas> ObtenerDiccionarioKey(EntClaPlantillas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntClaPlantillas> ObtenerDiccionarioKey(String strParamAdic, EntClaPlantillas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdic, dicKey);
		}
		
		public Dictionary<String, EntClaPlantillas> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, EntClaPlantillas.Fields dicKey)
		{
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, "", dicKey);
		}
		
		public Dictionary<String, EntClaPlantillas> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, EntClaPlantillas.Fields dicKey)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table, dicKey);
				}
				else
					return new Dictionary<string, EntClaPlantillas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		public Dictionary<String, EntClaPlantillas> ObtenerDiccionarioKey(EntClaPlantillas.Fields searchField, object searchValue, EntClaPlantillas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntClaPlantillas> ObtenerDiccionarioKey(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales, EntClaPlantillas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey);
		}
		

		#endregion 

		#region ObjetoASp

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla claplantillas
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntClaPlantillas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.idcpl);
				arrValoresParam.Add(obj.idctp);
				arrValoresParam.Add(obj.nombrecpl == null ? null : "'" + obj.nombrecpl + "'");
				arrValoresParam.Add(obj.descripcioncpl == null ? null : "'" + obj.descripcioncpl + "'");
				arrValoresParam.Add(obj.altocpl);
				arrValoresParam.Add(obj.anchocpl);
				arrValoresParam.Add(obj.margensupcpl);
				arrValoresParam.Add(obj.margeninfcpl);
				arrValoresParam.Add(obj.margendercpl);
				arrValoresParam.Add(obj.margenizqcpl);
				arrValoresParam.Add(obj.textocpl == null ? null : "'" + obj.textocpl + "'");
				arrValoresParam.Add(obj.rtfcpl);
				arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.htmlcpl == null ? null : "'" + obj.htmlcpl + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla claplantillas
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntClaPlantillas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrNombreParam.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.idcpl);
				arrValoresParam.Add(obj.idctp);
				arrValoresParam.Add(obj.nombrecpl == null ? null : "'" + obj.nombrecpl + "'");
				arrValoresParam.Add(obj.descripcioncpl == null ? null : "'" + obj.descripcioncpl + "'");
				arrValoresParam.Add(obj.altocpl);
				arrValoresParam.Add(obj.anchocpl);
				arrValoresParam.Add(obj.margensupcpl);
				arrValoresParam.Add(obj.margeninfcpl);
				arrValoresParam.Add(obj.margendercpl);
				arrValoresParam.Add(obj.margenizqcpl);
				arrValoresParam.Add(obj.textocpl == null ? null : "'" + obj.textocpl + "'");
				arrValoresParam.Add(obj.rtfcpl);
				arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.htmlcpl == null ? null : "'" + obj.htmlcpl + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region FuncionesAgregadas

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntClaPlantillas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntClaPlantillas.Fields refField, EntClaPlantillas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntClaPlantillas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0].ToString() == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntClaPlantillas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntClaPlantillas.Fields refField, EntClaPlantillas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntClaPlantillas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0].ToString() == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntClaPlantillas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntClaPlantillas.Fields refField, EntClaPlantillas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntClaPlantillas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0].ToString() == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntClaPlantillas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntClaPlantillas.Fields refField, EntClaPlantillas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntClaPlantillas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0].ToString() == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntClaPlantillas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntClaPlantillas.Fields refField, EntClaPlantillas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntClaPlantillas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntClaPlantillas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0].ToString() == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs SP

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionclaplantillas
		/// </returns>
		public bool Insert(EntClaPlantillas obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("idcpl");
				arrValoresParam.Add(null);
				if (obj.idctp == null)
					throw new Exception("El Parametro idctp no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrValoresParam.Add(obj.idctp);
				
				if (obj.nombrecpl == null)
					throw new Exception("El Parametro nombrecpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrValoresParam.Add(obj.nombrecpl);
				
				if (obj.descripcioncpl == null)
					throw new Exception("El Parametro descripcioncpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrValoresParam.Add(obj.descripcioncpl);
				
				if (obj.altocpl == null)
					throw new Exception("El Parametro altocpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrValoresParam.Add(obj.altocpl);
				
				if (obj.anchocpl == null)
					throw new Exception("El Parametro anchocpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrValoresParam.Add(obj.anchocpl);
				
				if (obj.margensupcpl == null)
					throw new Exception("El Parametro margensupcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrValoresParam.Add(obj.margensupcpl);
				
				if (obj.margeninfcpl == null)
					throw new Exception("El Parametro margeninfcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrValoresParam.Add(obj.margeninfcpl);
				
				if (obj.margendercpl == null)
					throw new Exception("El Parametro margendercpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrValoresParam.Add(obj.margendercpl);
				
				if (obj.margenizqcpl == null)
					throw new Exception("El Parametro margenizqcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrValoresParam.Add(obj.margenizqcpl);
				
				if (obj.textocpl == null)
					throw new Exception("El Parametro textocpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrValoresParam.Add(obj.textocpl);
				
				if (obj.rtfcpl == null)
					throw new Exception("El Parametro rtfcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrValoresParam.Add(obj.rtfcpl);
				
				if (obj.usucre == null)
					throw new Exception("El Parametro usucre no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrValoresParam.Add(obj.usucre);
				
				if (obj.htmlcpl == null)
					throw new Exception("El Parametro htmlcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				arrValoresParam.Add(obj.htmlcpl);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.ClaPlantillas.SpCplIns.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionclaplantillas
		/// </returns>
		public bool Insert(EntClaPlantillas obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("idcpl");
				arrValoresParam.Add("");
				if (obj.idctp == null)
					throw new Exception("El Parametro idctp no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrValoresParam.Add(obj.idctp);
				
				if (obj.nombrecpl == null)
					throw new Exception("El Parametro nombrecpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrValoresParam.Add(obj.nombrecpl);
				
				if (obj.descripcioncpl == null)
					throw new Exception("El Parametro descripcioncpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrValoresParam.Add(obj.descripcioncpl);
				
				if (obj.altocpl == null)
					throw new Exception("El Parametro altocpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrValoresParam.Add(obj.altocpl);
				
				if (obj.anchocpl == null)
					throw new Exception("El Parametro anchocpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrValoresParam.Add(obj.anchocpl);
				
				if (obj.margensupcpl == null)
					throw new Exception("El Parametro margensupcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrValoresParam.Add(obj.margensupcpl);
				
				if (obj.margeninfcpl == null)
					throw new Exception("El Parametro margeninfcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrValoresParam.Add(obj.margeninfcpl);
				
				if (obj.margendercpl == null)
					throw new Exception("El Parametro margendercpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrValoresParam.Add(obj.margendercpl);
				
				if (obj.margenizqcpl == null)
					throw new Exception("El Parametro margenizqcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrValoresParam.Add(obj.margenizqcpl);
				
				if (obj.textocpl == null)
					throw new Exception("El Parametro textocpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrValoresParam.Add(obj.textocpl);
				
				if (obj.rtfcpl == null)
					throw new Exception("El Parametro rtfcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrValoresParam.Add(obj.rtfcpl);
				
				if (obj.usucre == null)
					throw new Exception("El Parametro usucre no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrValoresParam.Add(obj.usucre);
				
				if (obj.htmlcpl == null)
					throw new Exception("El Parametro htmlcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				arrValoresParam.Add(obj.htmlcpl);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.ClaPlantillas.SpCplIns.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor que indica la cantidad de registros actualizados en claplantillas
		/// </returns>
		public int Update(EntClaPlantillas obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				if (obj.idcpl == null)
					throw new Exception("El Parametro idcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrValoresParam.Add(obj.idcpl);
				
				if (obj.idctp == null)
					throw new Exception("El Parametro idctp no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrValoresParam.Add(obj.idctp);
				
				if (obj.nombrecpl == null)
					throw new Exception("El Parametro nombrecpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrValoresParam.Add(obj.nombrecpl);
				
				if (obj.descripcioncpl == null)
					throw new Exception("El Parametro descripcioncpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrValoresParam.Add(obj.descripcioncpl);
				
				if (obj.altocpl == null)
					throw new Exception("El Parametro altocpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrValoresParam.Add(obj.altocpl);
				
				if (obj.anchocpl == null)
					throw new Exception("El Parametro anchocpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrValoresParam.Add(obj.anchocpl);
				
				if (obj.margensupcpl == null)
					throw new Exception("El Parametro margensupcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrValoresParam.Add(obj.margensupcpl);
				
				if (obj.margeninfcpl == null)
					throw new Exception("El Parametro margeninfcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrValoresParam.Add(obj.margeninfcpl);
				
				if (obj.margendercpl == null)
					throw new Exception("El Parametro margendercpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrValoresParam.Add(obj.margendercpl);
				
				if (obj.margenizqcpl == null)
					throw new Exception("El Parametro margenizqcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrValoresParam.Add(obj.margenizqcpl);
				
				if (obj.textocpl == null)
					throw new Exception("El Parametro textocpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrValoresParam.Add(obj.textocpl);
				
				if (obj.rtfcpl == null)
					throw new Exception("El Parametro rtfcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrValoresParam.Add(obj.rtfcpl);
				
				if (obj.apitransaccion == null)
					throw new Exception("El Parametro apitransaccion no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrValoresParam.Add(obj.apitransaccion);
				
				if (obj.usumod == null)
					throw new Exception("El Parametro usumod no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrValoresParam.Add(obj.usumod);
				
				if (obj.htmlcpl == null)
					throw new Exception("El Parametro htmlcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				arrValoresParam.Add(obj.htmlcpl);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.ClaPlantillas.SpCplUpd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionclaplantillas
		/// </returns>
		public int Update(EntClaPlantillas obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				if (obj.idcpl == null)
					throw new Exception("El Parametro idcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrValoresParam.Add(obj.idcpl);
				
				if (obj.idctp == null)
					throw new Exception("El Parametro idctp no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrValoresParam.Add(obj.idctp);
				
				if (obj.nombrecpl == null)
					throw new Exception("El Parametro nombrecpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrValoresParam.Add(obj.nombrecpl);
				
				if (obj.descripcioncpl == null)
					throw new Exception("El Parametro descripcioncpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrValoresParam.Add(obj.descripcioncpl);
				
				if (obj.altocpl == null)
					throw new Exception("El Parametro altocpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrValoresParam.Add(obj.altocpl);
				
				if (obj.anchocpl == null)
					throw new Exception("El Parametro anchocpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrValoresParam.Add(obj.anchocpl);
				
				if (obj.margensupcpl == null)
					throw new Exception("El Parametro margensupcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrValoresParam.Add(obj.margensupcpl);
				
				if (obj.margeninfcpl == null)
					throw new Exception("El Parametro margeninfcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrValoresParam.Add(obj.margeninfcpl);
				
				if (obj.margendercpl == null)
					throw new Exception("El Parametro margendercpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrValoresParam.Add(obj.margendercpl);
				
				if (obj.margenizqcpl == null)
					throw new Exception("El Parametro margenizqcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrValoresParam.Add(obj.margenizqcpl);
				
				if (obj.textocpl == null)
					throw new Exception("El Parametro textocpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrValoresParam.Add(obj.textocpl);
				
				if (obj.rtfcpl == null)
					throw new Exception("El Parametro rtfcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrValoresParam.Add(obj.rtfcpl);
				
				if (obj.apitransaccion == null)
					throw new Exception("El Parametro apitransaccion no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrValoresParam.Add(obj.apitransaccion);
				
				if (obj.usumod == null)
					throw new Exception("El Parametro usumod no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrValoresParam.Add(obj.usumod);
				
				if (obj.htmlcpl == null)
					throw new Exception("El Parametro htmlcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				arrValoresParam.Add(obj.htmlcpl);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.ClaPlantillas.SpCplUpd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionclaplantillas
		/// </returns>
		public int Delete(EntClaPlantillas obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				if (obj.idcpl == null)
					throw new Exception("El Parametro idcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrValoresParam.Add(obj.idcpl);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.ClaPlantillas.SpCplDel.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionclaplantillas
		/// </returns>
		public int Delete(EntClaPlantillas obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				if (obj.idcpl == null)
					throw new Exception("El Parametro idcpl no puede ser NULO.");
				arrNombreParam.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrValoresParam.Add(obj.idcpl);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.ClaPlantillas.SpCplDel.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionclaplantillas
		/// </returns>
		public int InsertUpdate(EntClaPlantillas obj)
		{
			try
			{
				bool esInsertar = true;
				
					esInsertar = (esInsertar && (obj.idcpl == null));
				
				if (esInsertar)
					return Insert(obj) ? 1 : 0;
				else
					return Update(obj);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionclaplantillas
		/// </returns>
		public int InsertUpdate(EntClaPlantillas obj, ref CTrans localTrans)
		{
			try
			{
				bool esInsertar = false;
				
					esInsertar = (esInsertar && (obj.idcpl == null));
				
				if (esInsertar)
					return Insert(obj, ref localTrans) ? 1 : 0;
				else
					return Update(obj, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs Query

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionclaplantillas
		/// </returns>
		public bool InsertQuery(EntClaPlantillas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idcpl);
				arrValores.Add(obj.idctp);
				arrValores.Add(obj.nombrecpl == null ? null : "'" + obj.nombrecpl + "'");
				arrValores.Add(obj.descripcioncpl == null ? null : "'" + obj.descripcioncpl + "'");
				arrValores.Add(obj.altocpl);
				arrValores.Add(obj.anchocpl);
				arrValores.Add(obj.margensupcpl);
				arrValores.Add(obj.margeninfcpl);
				arrValores.Add(obj.margendercpl);
				arrValores.Add(obj.margenizqcpl);
				arrValores.Add(obj.textocpl == null ? null : "'" + obj.textocpl + "'");
				arrValores.Add(obj.rtfcpl);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.htmlcpl == null ? null : "'" + obj.htmlcpl + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntClaPlantillas.StrNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionclaplantillas
		/// </returns>
		public bool InsertQuery(EntClaPlantillas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idcpl);
				arrValores.Add(obj.idctp);
				arrValores.Add(obj.nombrecpl == null ? null : "'" + obj.nombrecpl + "'");
				arrValores.Add(obj.descripcioncpl == null ? null : "'" + obj.descripcioncpl + "'");
				arrValores.Add(obj.altocpl);
				arrValores.Add(obj.anchocpl);
				arrValores.Add(obj.margensupcpl);
				arrValores.Add(obj.margeninfcpl);
				arrValores.Add(obj.margendercpl);
				arrValores.Add(obj.margenizqcpl);
				arrValores.Add(obj.textocpl == null ? null : "'" + obj.textocpl + "'");
				arrValores.Add(obj.rtfcpl);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.htmlcpl == null ? null : "'" + obj.htmlcpl + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntClaPlantillas.StrNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionclaplantillas
		/// </returns>
		public bool InsertQueryIdentity(EntClaPlantillas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idcpl);
				arrValores.Add(obj.idctp);
				arrValores.Add(obj.nombrecpl == null ? null : "'" + obj.nombrecpl + "'");
				arrValores.Add(obj.descripcioncpl == null ? null : "'" + obj.descripcioncpl + "'");
				arrValores.Add(obj.altocpl);
				arrValores.Add(obj.anchocpl);
				arrValores.Add(obj.margensupcpl);
				arrValores.Add(obj.margeninfcpl);
				arrValores.Add(obj.margendercpl);
				arrValores.Add(obj.margenizqcpl);
				arrValores.Add(obj.textocpl == null ? null : "'" + obj.textocpl + "'");
				arrValores.Add(obj.rtfcpl);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.htmlcpl == null ? null : "'" + obj.htmlcpl + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntClaPlantillas.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.idcpl = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionclaplantillas
		/// </returns>
		public bool InsertQueryIdentity(EntClaPlantillas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idcpl);
				arrValores.Add(obj.idctp);
				arrValores.Add(obj.nombrecpl == null ? null : "'" + obj.nombrecpl + "'");
				arrValores.Add(obj.descripcioncpl == null ? null : "'" + obj.descripcioncpl + "'");
				arrValores.Add(obj.altocpl);
				arrValores.Add(obj.anchocpl);
				arrValores.Add(obj.margensupcpl);
				arrValores.Add(obj.margeninfcpl);
				arrValores.Add(obj.margendercpl);
				arrValores.Add(obj.margenizqcpl);
				arrValores.Add(obj.textocpl == null ? null : "'" + obj.textocpl + "'");
				arrValores.Add(obj.rtfcpl);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.htmlcpl == null ? null : "'" + obj.htmlcpl + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntClaPlantillas.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.idcpl = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionclaplantillas
		/// </returns>
		public int UpdateQueryAll(EntClaPlantillas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.idctp);
				arrValores.Add(obj.nombrecpl == null ? null : "'" + obj.nombrecpl + "'");
				arrValores.Add(obj.descripcioncpl == null ? null : "'" + obj.descripcioncpl + "'");
				arrValores.Add(obj.altocpl);
				arrValores.Add(obj.anchocpl);
				arrValores.Add(obj.margensupcpl);
				arrValores.Add(obj.margeninfcpl);
				arrValores.Add(obj.margendercpl);
				arrValores.Add(obj.margenizqcpl);
				arrValores.Add(obj.textocpl == null ? null : "'" + obj.textocpl + "'");
				arrValores.Add(obj.rtfcpl);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValores.Add(obj.htmlcpl == null ? null : "'" + obj.htmlcpl + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntClaPlantillas.Fields.idcpl.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idcpl);

			
				CConn local = new CConn();
				return local.UpdateBd(EntClaPlantillas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla claplantillas a partir de una clase del tipo eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQueryAll(EntClaPlantillas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.idctp);
				arrValores.Add(obj.nombrecpl == null ? null : "'" + obj.nombrecpl + "'");
				arrValores.Add(obj.descripcioncpl == null ? null : "'" + obj.descripcioncpl + "'");
				arrValores.Add(obj.altocpl);
				arrValores.Add(obj.anchocpl);
				arrValores.Add(obj.margensupcpl);
				arrValores.Add(obj.margeninfcpl);
				arrValores.Add(obj.margendercpl);
				arrValores.Add(obj.margenizqcpl);
				arrValores.Add(obj.textocpl == null ? null : "'" + obj.textocpl + "'");
				arrValores.Add(obj.rtfcpl);
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValores.Add(obj.htmlcpl == null ? null : "'" + obj.htmlcpl + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntClaPlantillas.Fields.idcpl.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idcpl);

			
				CConn local = new CConn();
				return local.UpdateBd(EntClaPlantillas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla claplantillas a partir de una clase del tipo Eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionclaplantillas
		/// </returns>
		public int UpdateQuery(EntClaPlantillas obj)
		{
			try
			{
				//Obtenemos el Objeto original
				EntClaPlantillas objOriginal = this.ObtenerObjeto(obj.idcpl);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.idctp != objOriginal.idctp )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
					arrValores.Add(obj.idctp);
				}
				if(obj.nombrecpl != objOriginal.nombrecpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
					arrValores.Add(obj.nombrecpl == null ? null : "'" + obj.nombrecpl + "'");
				}
				if(obj.descripcioncpl != objOriginal.descripcioncpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
					arrValores.Add(obj.descripcioncpl == null ? null : "'" + obj.descripcioncpl + "'");
				}
				if(obj.altocpl != objOriginal.altocpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
					arrValores.Add(obj.altocpl);
				}
				if(obj.anchocpl != objOriginal.anchocpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
					arrValores.Add(obj.anchocpl);
				}
				if(obj.margensupcpl != objOriginal.margensupcpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
					arrValores.Add(obj.margensupcpl);
				}
				if(obj.margeninfcpl != objOriginal.margeninfcpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
					arrValores.Add(obj.margeninfcpl);
				}
				if(obj.margendercpl != objOriginal.margendercpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
					arrValores.Add(obj.margendercpl);
				}
				if(obj.margenizqcpl != objOriginal.margenizqcpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
					arrValores.Add(obj.margenizqcpl);
				}
				if(obj.textocpl != objOriginal.textocpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
					arrValores.Add(obj.textocpl == null ? null : "'" + obj.textocpl + "'");
				}
				if(obj.rtfcpl != objOriginal.rtfcpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
					arrValores.Add(obj.rtfcpl);
				}
				if(obj.apiestado != objOriginal.apiestado )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
					arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				}
				if(obj.apitransaccion != objOriginal.apitransaccion )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
					arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				}

				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");

				if(obj.htmlcpl != objOriginal.htmlcpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
					arrValores.Add(obj.htmlcpl == null ? null : "'" + obj.htmlcpl + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntClaPlantillas.Fields.idcpl.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idcpl);

			
				CConn local = new CConn();
				return local.UpdateBd(EntClaPlantillas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla claplantillas a partir de una clase del tipo eclaplantillas
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQuery(EntClaPlantillas obj, ref CTrans localTrans)
		{
			try
			{
				//Obtenemos el Objeto original
				EntClaPlantillas objOriginal = this.ObtenerObjeto(obj.idcpl, ref localTrans);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.idctp != objOriginal.idctp )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
					arrValores.Add(obj.idctp);
				}
				if(obj.nombrecpl != objOriginal.nombrecpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
					arrValores.Add(obj.nombrecpl == null ? null : "'" + obj.nombrecpl + "'");
				}
				if(obj.descripcioncpl != objOriginal.descripcioncpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
					arrValores.Add(obj.descripcioncpl == null ? null : "'" + obj.descripcioncpl + "'");
				}
				if(obj.altocpl != objOriginal.altocpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
					arrValores.Add(obj.altocpl);
				}
				if(obj.anchocpl != objOriginal.anchocpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
					arrValores.Add(obj.anchocpl);
				}
				if(obj.margensupcpl != objOriginal.margensupcpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
					arrValores.Add(obj.margensupcpl);
				}
				if(obj.margeninfcpl != objOriginal.margeninfcpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
					arrValores.Add(obj.margeninfcpl);
				}
				if(obj.margendercpl != objOriginal.margendercpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
					arrValores.Add(obj.margendercpl);
				}
				if(obj.margenizqcpl != objOriginal.margenizqcpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
					arrValores.Add(obj.margenizqcpl);
				}
				if(obj.textocpl != objOriginal.textocpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
					arrValores.Add(obj.textocpl == null ? null : "'" + obj.textocpl + "'");
				}
				if(obj.rtfcpl != objOriginal.rtfcpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
					arrValores.Add(obj.rtfcpl);
				}
				if(obj.apiestado != objOriginal.apiestado )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
					arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				}
				if(obj.apitransaccion != objOriginal.apitransaccion )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
					arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				}

				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");

				if(obj.htmlcpl != objOriginal.htmlcpl )
				{
					arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
					arrValores.Add(obj.htmlcpl == null ? null : "'" + obj.htmlcpl + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntClaPlantillas.Fields.idcpl.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idcpl);

			
				CConn local = new CConn();
				return local.UpdateBd(EntClaPlantillas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla claplantillas a partir de una clase del tipo EntClaPlantillas y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.EntClaPlantillas">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionclaplantillas
		/// </returns>
		public int DeleteQuery(EntClaPlantillas obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntClaPlantillas.Fields.idcpl.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idcpl);

			
				CConn local = new CConn();
				return local.DeleteBd(EntClaPlantillas.StrNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla claplantillas a partir de una clase del tipo EntClaPlantillas y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.eclaplantillas">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionclaplantillas
		/// </returns>
		public int DeleteQuery(EntClaPlantillas obj, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntClaPlantillas.Fields.idcpl.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idcpl);

			
				CConn local = new CConn();
				return local.DeleteBd(EntClaPlantillas.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla claplantillas a partir de una clase del tipo eclaplantillas
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionclaplantillas
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd("claplantillas", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla claplantillas a partir de una clase del tipo eclaplantillas
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion claplantillas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionclaplantillas
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd(EntClaPlantillas.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region Llenado de elementos

		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntClaPlantillas.Fields valueField, String textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntClaPlantillas.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField, EntClaPlantillas.Fields searchField, object searchValue)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntClaPlantillas.Fields valueField, String textField, EntClaPlantillas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField, EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntClaPlantillas.Fields valueField, String textField, EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntClaPlantillas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntClaPlantillas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.DataValueField = table.Columns[0].ColumnName;
					cmb.DataTextField = table.Columns[1].ColumnName;
					cmb.DataSource = table;
					cmb.DataBind();

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntClaPlantillas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntClaPlantillas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntClaPlantillas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntClaPlantillas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridViewOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField, EntClaPlantillas.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, String textField, EntClaPlantillas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField, EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, String textField, EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla claplantillas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla claplantillas
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntClaPlantillas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntClaPlantillas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntClaPlantillas.Fields.idcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.idctp.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.nombrecpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.descripcioncpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.altocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.anchocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margensupcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margeninfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margendercpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.margenizqcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.textocpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.rtfcpl.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apiestado.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usucre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.feccre.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.usumod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.fecmod.ToString());
				arrColumnas.Add(EntClaPlantillas.Fields.htmlcpl.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntClaPlantillas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntClaPlantillas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntClaPlantillas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla claplantillas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntClaPlantillas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 


		#endregion

		#region Funciones Internas

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto claplantillas
		/// </returns>
		internal EntClaPlantillas crearObjeto(DataRow row)
		{
			var obj = new EntClaPlantillas();
			obj.idcpl = GetColumnType(row[EntClaPlantillas.Fields.idcpl.ToString()], EntClaPlantillas.Fields.idcpl);
			obj.idctp = GetColumnType(row[EntClaPlantillas.Fields.idctp.ToString()], EntClaPlantillas.Fields.idctp);
			obj.nombrecpl = GetColumnType(row[EntClaPlantillas.Fields.nombrecpl.ToString()], EntClaPlantillas.Fields.nombrecpl);
			obj.descripcioncpl = GetColumnType(row[EntClaPlantillas.Fields.descripcioncpl.ToString()], EntClaPlantillas.Fields.descripcioncpl);
			obj.altocpl = GetColumnType(row[EntClaPlantillas.Fields.altocpl.ToString()], EntClaPlantillas.Fields.altocpl);
			obj.anchocpl = GetColumnType(row[EntClaPlantillas.Fields.anchocpl.ToString()], EntClaPlantillas.Fields.anchocpl);
			obj.margensupcpl = GetColumnType(row[EntClaPlantillas.Fields.margensupcpl.ToString()], EntClaPlantillas.Fields.margensupcpl);
			obj.margeninfcpl = GetColumnType(row[EntClaPlantillas.Fields.margeninfcpl.ToString()], EntClaPlantillas.Fields.margeninfcpl);
			obj.margendercpl = GetColumnType(row[EntClaPlantillas.Fields.margendercpl.ToString()], EntClaPlantillas.Fields.margendercpl);
			obj.margenizqcpl = GetColumnType(row[EntClaPlantillas.Fields.margenizqcpl.ToString()], EntClaPlantillas.Fields.margenizqcpl);
			obj.textocpl = GetColumnType(row[EntClaPlantillas.Fields.textocpl.ToString()], EntClaPlantillas.Fields.textocpl);
			obj.rtfcpl = GetColumnType(row[EntClaPlantillas.Fields.rtfcpl.ToString()], EntClaPlantillas.Fields.rtfcpl);
			obj.apiestado = GetColumnType(row[EntClaPlantillas.Fields.apiestado.ToString()], EntClaPlantillas.Fields.apiestado);
			obj.apitransaccion = GetColumnType(row[EntClaPlantillas.Fields.apitransaccion.ToString()], EntClaPlantillas.Fields.apitransaccion);
			obj.usucre = GetColumnType(row[EntClaPlantillas.Fields.usucre.ToString()], EntClaPlantillas.Fields.usucre);
			obj.feccre = GetColumnType(row[EntClaPlantillas.Fields.feccre.ToString()], EntClaPlantillas.Fields.feccre);
			obj.usumod = GetColumnType(row[EntClaPlantillas.Fields.usumod.ToString()], EntClaPlantillas.Fields.usumod);
			obj.fecmod = GetColumnType(row[EntClaPlantillas.Fields.fecmod.ToString()], EntClaPlantillas.Fields.fecmod);
			obj.htmlcpl = GetColumnType(row[EntClaPlantillas.Fields.htmlcpl.ToString()], EntClaPlantillas.Fields.htmlcpl);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto claplantillas
		/// </returns>
		internal EntClaPlantillas crearObjetoRevisado(DataRow row)
		{
			var obj = new EntClaPlantillas();
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.idcpl.ToString()))
				obj.idcpl = GetColumnType(row[EntClaPlantillas.Fields.idcpl.ToString()], EntClaPlantillas.Fields.idcpl);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.idctp.ToString()))
				obj.idctp = GetColumnType(row[EntClaPlantillas.Fields.idctp.ToString()], EntClaPlantillas.Fields.idctp);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.nombrecpl.ToString()))
				obj.nombrecpl = GetColumnType(row[EntClaPlantillas.Fields.nombrecpl.ToString()], EntClaPlantillas.Fields.nombrecpl);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.descripcioncpl.ToString()))
				obj.descripcioncpl = GetColumnType(row[EntClaPlantillas.Fields.descripcioncpl.ToString()], EntClaPlantillas.Fields.descripcioncpl);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.altocpl.ToString()))
				obj.altocpl = GetColumnType(row[EntClaPlantillas.Fields.altocpl.ToString()], EntClaPlantillas.Fields.altocpl);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.anchocpl.ToString()))
				obj.anchocpl = GetColumnType(row[EntClaPlantillas.Fields.anchocpl.ToString()], EntClaPlantillas.Fields.anchocpl);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.margensupcpl.ToString()))
				obj.margensupcpl = GetColumnType(row[EntClaPlantillas.Fields.margensupcpl.ToString()], EntClaPlantillas.Fields.margensupcpl);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.margeninfcpl.ToString()))
				obj.margeninfcpl = GetColumnType(row[EntClaPlantillas.Fields.margeninfcpl.ToString()], EntClaPlantillas.Fields.margeninfcpl);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.margendercpl.ToString()))
				obj.margendercpl = GetColumnType(row[EntClaPlantillas.Fields.margendercpl.ToString()], EntClaPlantillas.Fields.margendercpl);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.margenizqcpl.ToString()))
				obj.margenizqcpl = GetColumnType(row[EntClaPlantillas.Fields.margenizqcpl.ToString()], EntClaPlantillas.Fields.margenizqcpl);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.textocpl.ToString()))
				obj.textocpl = GetColumnType(row[EntClaPlantillas.Fields.textocpl.ToString()], EntClaPlantillas.Fields.textocpl);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.rtfcpl.ToString()))
				obj.rtfcpl = GetColumnType(row[EntClaPlantillas.Fields.rtfcpl.ToString()], EntClaPlantillas.Fields.rtfcpl);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[EntClaPlantillas.Fields.apiestado.ToString()], EntClaPlantillas.Fields.apiestado);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.apitransaccion.ToString()))
				obj.apitransaccion = GetColumnType(row[EntClaPlantillas.Fields.apitransaccion.ToString()], EntClaPlantillas.Fields.apitransaccion);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[EntClaPlantillas.Fields.usucre.ToString()], EntClaPlantillas.Fields.usucre);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[EntClaPlantillas.Fields.feccre.ToString()], EntClaPlantillas.Fields.feccre);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[EntClaPlantillas.Fields.usumod.ToString()], EntClaPlantillas.Fields.usumod);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[EntClaPlantillas.Fields.fecmod.ToString()], EntClaPlantillas.Fields.fecmod);
			if (row.Table.Columns.Contains(EntClaPlantillas.Fields.htmlcpl.ToString()))
				obj.htmlcpl = GetColumnType(row[EntClaPlantillas.Fields.htmlcpl.ToString()], EntClaPlantillas.Fields.htmlcpl);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtclaplantillas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos claplantillas
		/// </returns>
		internal List<EntClaPlantillas> crearLista(DataTable dtclaplantillas)
		{
			var list = new List<EntClaPlantillas>();
			
			foreach (DataRow row in dtclaplantillas.Rows)
			{
				var obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtclaplantillas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos claplantillas
		/// </returns>
		internal List<EntClaPlantillas> crearListaRevisada(DataTable dtclaplantillas)
		{
			List<EntClaPlantillas> list = new List<EntClaPlantillas>();
			
			foreach (DataRow row in dtclaplantillas.Rows)
			{
				var obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtclaplantillas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos claplantillas
		/// </returns>
		internal Queue<EntClaPlantillas> crearCola(DataTable dtclaplantillas)
		{
			Queue<EntClaPlantillas> cola = new Queue<EntClaPlantillas>();
			
			EntClaPlantillas obj = new EntClaPlantillas();
			foreach (DataRow row in dtclaplantillas.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtclaplantillas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos claplantillas
		/// </returns>
		internal Stack<EntClaPlantillas> crearPila(DataTable dtclaplantillas)
		{
			Stack<EntClaPlantillas> pila = new Stack<EntClaPlantillas>();
			
			foreach (DataRow row in dtclaplantillas.Rows)
			{
				var obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtclaplantillas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos claplantillas
		/// </returns>
		internal Dictionary<String, EntClaPlantillas> crearDiccionario(DataTable dtclaplantillas)
		{
			Dictionary<String, EntClaPlantillas>  miDic = new Dictionary<String, EntClaPlantillas>();
			
			foreach (DataRow row in dtclaplantillas.Rows)
			{
				var obj = crearObjeto(row);
				miDic.Add(obj.idcpl.ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtclaplantillas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 HashTable de Objetos claplantillas
		/// </returns>
		internal Hashtable crearHashTable(DataTable dtclaplantillas)
		{
			Hashtable miTabla = new Hashtable();
			
			foreach (DataRow row in dtclaplantillas.Rows)
			{
				var obj = crearObjeto(row);
				miTabla.Add(obj.idcpl.ToString(), obj);
			}
			return miTabla;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtclaplantillas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos claplantillas
		/// </returns>
		internal Dictionary<String, EntClaPlantillas> crearDiccionarioRevisado(DataTable dtclaplantillas)
		{
			Dictionary<String, EntClaPlantillas>  miDic = new Dictionary<String, EntClaPlantillas>();
			
			foreach (DataRow row in dtclaplantillas.Rows)
			{
				var obj = crearObjetoRevisado(row);
				miDic.Add(obj.idcpl.ToString(), obj);
			}
			return miDic;
		}
		
		internal Dictionary<String, EntClaPlantillas> crearDiccionario(DataTable dtclaplantillas, EntClaPlantillas.Fields dicKey)
		{
			Dictionary<String, EntClaPlantillas>  miDic = new Dictionary<String, EntClaPlantillas>();
			
			foreach (DataRow row in dtclaplantillas.Rows)
			{
				var obj = crearObjeto(row);
				
				var nameOfProperty = dicKey.ToString();
				var propertyInfo = obj.GetType().GetProperty(nameOfProperty);
				var value = propertyInfo.GetValue(obj, null);
				
				miDic.Add(value.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
		#endregion

	}
}

