#region 
/***********************************************************************************************************
	NOMBRE:       RnPacHistoriaclinica2
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla pachistoriaclinica2

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/04/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Interface;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Modelo
{
	public class RnPacHistoriaclinica2: IPacHistoriaclinica2
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region IPacHistoriaclinica2 Members

		#region Reflection

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string GetTableScript()
		{
			TableClass tabla = new TableClass(typeof(EntPacHistoriaclinica2));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, EntPacHistoriaclinica2.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntPacHistoriaclinica2).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="strField">Nombre de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, string strField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntPacHistoriaclinica2).GetProperty(strField).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

/// <summary>
/// Inserta una valor a una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">Es el nombre de la propiedad</param>
/// <param name="dynValor">Es el valor que se insertara a la propiedad</param>
public void SetDato(ref EntPacHistoriaclinica2 obj, string strPropiedad, dynamic dynValor)
{
	if (obj == null) throw new ArgumentNullException();
	obj.GetType().GetProperty(strPropiedad).SetValue(obj, GetColumnType(dynValor, strPropiedad), null);
}

/// <summary>
/// Obtiene el valor de una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">El nombre de la propiedad de la que se obtendra el valor</param>
/// <returns>Devuelve el valor del a propiedad seleccionada</returns>
public dynamic GetDato(ref EntPacHistoriaclinica2 obj, string strPropiedad)
{
	if (obj == null) return null;
	var propertyInfo = obj.GetType().GetProperty(strPropiedad);
	return GetColumnType(propertyInfo.GetValue(obj, null), strPropiedad);
}

		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla pachistoriaclinica2 a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla pachistoriaclinica2
		/// </returns>
		public string CreatePk(string[] args)
		{
			return args[0];
		}
		
		#endregion 

		#region ObtenerObjeto

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica2 a partir de la llave primaria
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica2 ObtenerObjeto(Int64 Int64idppa)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64idppa + "'");
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica2 a partir del usuario que inserta
		/// </summary>
		/// <param name="strUsuCre">Login o nombre de usuario</param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica2 ObtenerObjetoInsertado(string strUsuCre)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + strUsuCre + "'");
			
			int iIdInsertado = FuncionesMax(EntPacHistoriaclinica2.Fields.idppa, arrColumnasWhere, arrValoresWhere);
			
			return ObtenerObjeto(iIdInsertado);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica2 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica2 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntPacHistoriaclinica2 obj = new EntPacHistoriaclinica2();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica2 ObtenerObjeto(Hashtable htbFiltro)
		{
			return ObtenerObjeto(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica2 ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntPacHistoriaclinica2 obj = new EntPacHistoriaclinica2();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica2 ObtenerObjeto(EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica2 ObtenerObjeto(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo EntPacHistoriaclinica2 a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo EntPacHistoriaclinica2
		/// </returns>
		public EntPacHistoriaclinica2 ObtenerObjeto(Int64 Int64idppa, ref CTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64idppa + "'");
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica2 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica2 ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerObjeto(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica2 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica2 ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					EntPacHistoriaclinica2 obj = new EntPacHistoriaclinica2();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica2 ObtenerObjeto(EntPacHistoriaclinica2.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica2 ObtenerObjeto(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion

		#region ObtenerLista

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacHistoriaclinica2>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerLista(EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerLista(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacHistoriaclinica2>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerLista(EntPacHistoriaclinica2.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerLista(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerLista(Hashtable htbFiltro)
		{
			return ObtenerLista(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacHistoriaclinica2>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerLista(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacHistoriaclinica2>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacHistoriaclinica2>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacHistoriaclinica2>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacHistoriaclinica2>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacHistoriaclinica2>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica2.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerCola y Obtener Pila

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica2> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica2> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica2> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntPacHistoriaclinica2>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica2> ObtenerCola(EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica2> ObtenerCola(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica2> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica2> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntPacHistoriaclinica2>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica2> ObtenerCola(EntPacHistoriaclinica2.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica2> ObtenerCola(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica2> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica2> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica2> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntPacHistoriaclinica2>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica2> ObtenerPila(EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica2> ObtenerPila(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica2> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica2> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntPacHistoriaclinica2>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica2> ObtenerPila(EntPacHistoriaclinica2.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica2> ObtenerPila(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerDataTable

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla pachistoriaclinica2
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(EntPacHistoriaclinica2.Fields.idppa.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.idppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4formaphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4formaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5weberphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5weberphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5tonophc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5tonophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.apiestado.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.apitransaccion.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.apitransaccion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.usucre.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.feccre.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.usumod.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica2.Fields.fecmod.ToString(),typeof(EntPacHistoriaclinica2).GetProperty(EntPacHistoriaclinica2.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una pachistoriaclinica2
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla pachistoriaclinica2
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable ObtenerDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE pachistoriaclinica2
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable ObtenerDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica2
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableOr(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region ObtenerDiccionario

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntPacHistoriaclinica2>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntPacHistoriaclinica2>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(EntPacHistoriaclinica2.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica2 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		public Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionarioKey(EntPacHistoriaclinica2.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionarioKey(String strParamAdic, EntPacHistoriaclinica2.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdic, dicKey);
		}
		
		public Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, EntPacHistoriaclinica2.Fields dicKey)
		{
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, "", dicKey);
		}
		
		public Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, EntPacHistoriaclinica2.Fields dicKey)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table, dicKey);
				}
				else
					return new Dictionary<string, EntPacHistoriaclinica2>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		public Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionarioKey(EntPacHistoriaclinica2.Fields searchField, object searchValue, EntPacHistoriaclinica2.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionarioKey(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales, EntPacHistoriaclinica2.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey);
		}
		

		#endregion 

		#region ObjetoASp

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla pachistoriaclinica2
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntPacHistoriaclinica2 obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValoresParam.Add(obj.h4olfnormalphc == null ? null : "'" + obj.h4olfnormalphc + "'");
				arrValoresParam.Add(obj.h4olfanosmiaphc == null ? null : "'" + obj.h4olfanosmiaphc + "'");
				arrValoresParam.Add(obj.h4optscodphc == null ? null : "'" + obj.h4optscodphc + "'");
				arrValoresParam.Add(obj.h4optscoiphc == null ? null : "'" + obj.h4optscoiphc + "'");
				arrValoresParam.Add(obj.h4optccodphc == null ? null : "'" + obj.h4optccodphc + "'");
				arrValoresParam.Add(obj.h4optccoiphc == null ? null : "'" + obj.h4optccoiphc + "'");
				arrValoresParam.Add(obj.h4fondoodphc == null ? null : "'" + obj.h4fondoodphc + "'");
				arrValoresParam.Add(obj.h4fondooiphc == null ? null : "'" + obj.h4fondooiphc + "'");
				arrValoresParam.Add(obj.h4campimetriaphc == null ? null : "'" + obj.h4campimetriaphc + "'");
				arrValoresParam.Add(obj.h4prosisphc == null ? null : "'" + obj.h4prosisphc + "'");
				arrValoresParam.Add(obj.h4posicionojosphc == null ? null : "'" + obj.h4posicionojosphc + "'");
				arrValoresParam.Add(obj.h4exoftalmiaphc == null ? null : "'" + obj.h4exoftalmiaphc + "'");
				arrValoresParam.Add(obj.h4hornerphc == null ? null : "'" + obj.h4hornerphc + "'");
				arrValoresParam.Add(obj.h4movocularesphc == null ? null : "'" + obj.h4movocularesphc + "'");
				arrValoresParam.Add(obj.h4nistagmuxphc == null ? null : "'" + obj.h4nistagmuxphc + "'");
				arrValoresParam.Add(obj.h4diplopia1phc == null ? null : "'" + obj.h4diplopia1phc + "'");
				arrValoresParam.Add(obj.h4diplopia2phc == null ? null : "'" + obj.h4diplopia2phc + "'");
				arrValoresParam.Add(obj.h4diplopia3phc == null ? null : "'" + obj.h4diplopia3phc + "'");
				arrValoresParam.Add(obj.h4diplopia4phc == null ? null : "'" + obj.h4diplopia4phc + "'");
				arrValoresParam.Add(obj.h4diplopia5phc == null ? null : "'" + obj.h4diplopia5phc + "'");
				arrValoresParam.Add(obj.h4diplopia6phc == null ? null : "'" + obj.h4diplopia6phc + "'");
				arrValoresParam.Add(obj.h4diplopia7phc == null ? null : "'" + obj.h4diplopia7phc + "'");
				arrValoresParam.Add(obj.h4diplopia8phc == null ? null : "'" + obj.h4diplopia8phc + "'");
				arrValoresParam.Add(obj.h4diplopia9phc == null ? null : "'" + obj.h4diplopia9phc + "'");
				arrValoresParam.Add(obj.h4convergenciaphc == null ? null : "'" + obj.h4convergenciaphc + "'");
				arrValoresParam.Add(obj.h4acomodacionodphc == null ? null : "'" + obj.h4acomodacionodphc + "'");
				arrValoresParam.Add(obj.h4acomodacionoiphc == null ? null : "'" + obj.h4acomodacionoiphc + "'");
				arrValoresParam.Add(obj.h4pupulasodphc == null ? null : "'" + obj.h4pupulasodphc + "'");
				arrValoresParam.Add(obj.h4pupulasoiphc == null ? null : "'" + obj.h4pupulasoiphc + "'");
				arrValoresParam.Add(obj.h4formaphc == null ? null : "'" + obj.h4formaphc + "'");
				arrValoresParam.Add(obj.h4fotomotorodphc == null ? null : "'" + obj.h4fotomotorodphc + "'");
				arrValoresParam.Add(obj.h4fotomotoroiphc == null ? null : "'" + obj.h4fotomotoroiphc + "'");
				arrValoresParam.Add(obj.h4consensualdaiphc == null ? null : "'" + obj.h4consensualdaiphc + "'");
				arrValoresParam.Add(obj.h4consensualiadphc == null ? null : "'" + obj.h4consensualiadphc + "'");
				arrValoresParam.Add(obj.h4senfacialderphc == null ? null : "'" + obj.h4senfacialderphc + "'");
				arrValoresParam.Add(obj.h4senfacializqphc == null ? null : "'" + obj.h4senfacializqphc + "'");
				arrValoresParam.Add(obj.h4reflejoderphc == null ? null : "'" + obj.h4reflejoderphc + "'");
				arrValoresParam.Add(obj.h4reflejoizqphc == null ? null : "'" + obj.h4reflejoizqphc + "'");
				arrValoresParam.Add(obj.h4aberturabocaphc == null ? null : "'" + obj.h4aberturabocaphc + "'");
				arrValoresParam.Add(obj.h4movmasticatoriosphc == null ? null : "'" + obj.h4movmasticatoriosphc + "'");
				arrValoresParam.Add(obj.h4refmentonianophc == null ? null : "'" + obj.h4refmentonianophc + "'");
				arrValoresParam.Add(obj.h4facsimetriaphc == null ? null : "'" + obj.h4facsimetriaphc + "'");
				arrValoresParam.Add(obj.h4facmovimientosphc == null ? null : "'" + obj.h4facmovimientosphc + "'");
				arrValoresParam.Add(obj.h4facparalisisphc == null ? null : "'" + obj.h4facparalisisphc + "'");
				arrValoresParam.Add(obj.h4faccentralphc == null ? null : "'" + obj.h4faccentralphc + "'");
				arrValoresParam.Add(obj.h4facperifericaphc == null ? null : "'" + obj.h4facperifericaphc + "'");
				arrValoresParam.Add(obj.h4facgustophc == null ? null : "'" + obj.h4facgustophc + "'");
				arrValoresParam.Add(obj.h5otoscopiaphc == null ? null : "'" + obj.h5otoscopiaphc + "'");
				arrValoresParam.Add(obj.h5aguaudiderphc == null ? null : "'" + obj.h5aguaudiderphc + "'");
				arrValoresParam.Add(obj.h5aguaudiizqphc == null ? null : "'" + obj.h5aguaudiizqphc + "'");
				arrValoresParam.Add(obj.h5weberphc == null ? null : "'" + obj.h5weberphc + "'");
				arrValoresParam.Add(obj.h5rinnephc == null ? null : "'" + obj.h5rinnephc + "'");
				arrValoresParam.Add(obj.h5pruebaslabphc == null ? null : "'" + obj.h5pruebaslabphc + "'");
				arrValoresParam.Add(obj.h5elevpaladarphc == null ? null : "'" + obj.h5elevpaladarphc + "'");
				arrValoresParam.Add(obj.h5uvulaphc == null ? null : "'" + obj.h5uvulaphc + "'");
				arrValoresParam.Add(obj.h5refnauceosophc == null ? null : "'" + obj.h5refnauceosophc + "'");
				arrValoresParam.Add(obj.h5deglucionphc == null ? null : "'" + obj.h5deglucionphc + "'");
				arrValoresParam.Add(obj.h5tonovozphc == null ? null : "'" + obj.h5tonovozphc + "'");
				arrValoresParam.Add(obj.h5esternocleidomastoideophc == null ? null : "'" + obj.h5esternocleidomastoideophc + "'");
				arrValoresParam.Add(obj.h5trapeciophc == null ? null : "'" + obj.h5trapeciophc + "'");
				arrValoresParam.Add(obj.h5desviacionphc == null ? null : "'" + obj.h5desviacionphc + "'");
				arrValoresParam.Add(obj.h5atrofiaphc == null ? null : "'" + obj.h5atrofiaphc + "'");
				arrValoresParam.Add(obj.h5fasciculacionphc == null ? null : "'" + obj.h5fasciculacionphc + "'");
				arrValoresParam.Add(obj.h5fuerzaphc == null ? null : "'" + obj.h5fuerzaphc + "'");
				arrValoresParam.Add(obj.h5marchaphc == null ? null : "'" + obj.h5marchaphc + "'");
				arrValoresParam.Add(obj.h5tonophc == null ? null : "'" + obj.h5tonophc + "'");
				arrValoresParam.Add(obj.h5volumenphc == null ? null : "'" + obj.h5volumenphc + "'");
				arrValoresParam.Add(obj.h5fasciculacionesphc == null ? null : "'" + obj.h5fasciculacionesphc + "'");
				arrValoresParam.Add(obj.h5fuemuscularphc == null ? null : "'" + obj.h5fuemuscularphc + "'");
				arrValoresParam.Add(obj.h5movinvoluntariosphc == null ? null : "'" + obj.h5movinvoluntariosphc + "'");
				arrValoresParam.Add(obj.h5equilibratoriaphc == null ? null : "'" + obj.h5equilibratoriaphc + "'");
				arrValoresParam.Add(obj.h5rombergphc == null ? null : "'" + obj.h5rombergphc + "'");
				arrValoresParam.Add(obj.h5dednarderphc == null ? null : "'" + obj.h5dednarderphc + "'");
				arrValoresParam.Add(obj.h5dednarizqphc == null ? null : "'" + obj.h5dednarizqphc + "'");
				arrValoresParam.Add(obj.h5deddedderphc == null ? null : "'" + obj.h5deddedderphc + "'");
				arrValoresParam.Add(obj.h5deddedizqphc == null ? null : "'" + obj.h5deddedizqphc + "'");
				arrValoresParam.Add(obj.h5talrodderphc == null ? null : "'" + obj.h5talrodderphc + "'");
				arrValoresParam.Add(obj.h5talrodizqphc == null ? null : "'" + obj.h5talrodizqphc + "'");
				arrValoresParam.Add(obj.h5movrapidphc == null ? null : "'" + obj.h5movrapidphc + "'");
				arrValoresParam.Add(obj.h5rebotephc == null ? null : "'" + obj.h5rebotephc + "'");
				arrValoresParam.Add(obj.h5habilidadespecifphc == null ? null : "'" + obj.h5habilidadespecifphc + "'");
				arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla pachistoriaclinica2
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntPacHistoriaclinica2 obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValoresParam.Add(obj.h4olfnormalphc == null ? null : "'" + obj.h4olfnormalphc + "'");
				arrValoresParam.Add(obj.h4olfanosmiaphc == null ? null : "'" + obj.h4olfanosmiaphc + "'");
				arrValoresParam.Add(obj.h4optscodphc == null ? null : "'" + obj.h4optscodphc + "'");
				arrValoresParam.Add(obj.h4optscoiphc == null ? null : "'" + obj.h4optscoiphc + "'");
				arrValoresParam.Add(obj.h4optccodphc == null ? null : "'" + obj.h4optccodphc + "'");
				arrValoresParam.Add(obj.h4optccoiphc == null ? null : "'" + obj.h4optccoiphc + "'");
				arrValoresParam.Add(obj.h4fondoodphc == null ? null : "'" + obj.h4fondoodphc + "'");
				arrValoresParam.Add(obj.h4fondooiphc == null ? null : "'" + obj.h4fondooiphc + "'");
				arrValoresParam.Add(obj.h4campimetriaphc == null ? null : "'" + obj.h4campimetriaphc + "'");
				arrValoresParam.Add(obj.h4prosisphc == null ? null : "'" + obj.h4prosisphc + "'");
				arrValoresParam.Add(obj.h4posicionojosphc == null ? null : "'" + obj.h4posicionojosphc + "'");
				arrValoresParam.Add(obj.h4exoftalmiaphc == null ? null : "'" + obj.h4exoftalmiaphc + "'");
				arrValoresParam.Add(obj.h4hornerphc == null ? null : "'" + obj.h4hornerphc + "'");
				arrValoresParam.Add(obj.h4movocularesphc == null ? null : "'" + obj.h4movocularesphc + "'");
				arrValoresParam.Add(obj.h4nistagmuxphc == null ? null : "'" + obj.h4nistagmuxphc + "'");
				arrValoresParam.Add(obj.h4diplopia1phc == null ? null : "'" + obj.h4diplopia1phc + "'");
				arrValoresParam.Add(obj.h4diplopia2phc == null ? null : "'" + obj.h4diplopia2phc + "'");
				arrValoresParam.Add(obj.h4diplopia3phc == null ? null : "'" + obj.h4diplopia3phc + "'");
				arrValoresParam.Add(obj.h4diplopia4phc == null ? null : "'" + obj.h4diplopia4phc + "'");
				arrValoresParam.Add(obj.h4diplopia5phc == null ? null : "'" + obj.h4diplopia5phc + "'");
				arrValoresParam.Add(obj.h4diplopia6phc == null ? null : "'" + obj.h4diplopia6phc + "'");
				arrValoresParam.Add(obj.h4diplopia7phc == null ? null : "'" + obj.h4diplopia7phc + "'");
				arrValoresParam.Add(obj.h4diplopia8phc == null ? null : "'" + obj.h4diplopia8phc + "'");
				arrValoresParam.Add(obj.h4diplopia9phc == null ? null : "'" + obj.h4diplopia9phc + "'");
				arrValoresParam.Add(obj.h4convergenciaphc == null ? null : "'" + obj.h4convergenciaphc + "'");
				arrValoresParam.Add(obj.h4acomodacionodphc == null ? null : "'" + obj.h4acomodacionodphc + "'");
				arrValoresParam.Add(obj.h4acomodacionoiphc == null ? null : "'" + obj.h4acomodacionoiphc + "'");
				arrValoresParam.Add(obj.h4pupulasodphc == null ? null : "'" + obj.h4pupulasodphc + "'");
				arrValoresParam.Add(obj.h4pupulasoiphc == null ? null : "'" + obj.h4pupulasoiphc + "'");
				arrValoresParam.Add(obj.h4formaphc == null ? null : "'" + obj.h4formaphc + "'");
				arrValoresParam.Add(obj.h4fotomotorodphc == null ? null : "'" + obj.h4fotomotorodphc + "'");
				arrValoresParam.Add(obj.h4fotomotoroiphc == null ? null : "'" + obj.h4fotomotoroiphc + "'");
				arrValoresParam.Add(obj.h4consensualdaiphc == null ? null : "'" + obj.h4consensualdaiphc + "'");
				arrValoresParam.Add(obj.h4consensualiadphc == null ? null : "'" + obj.h4consensualiadphc + "'");
				arrValoresParam.Add(obj.h4senfacialderphc == null ? null : "'" + obj.h4senfacialderphc + "'");
				arrValoresParam.Add(obj.h4senfacializqphc == null ? null : "'" + obj.h4senfacializqphc + "'");
				arrValoresParam.Add(obj.h4reflejoderphc == null ? null : "'" + obj.h4reflejoderphc + "'");
				arrValoresParam.Add(obj.h4reflejoizqphc == null ? null : "'" + obj.h4reflejoizqphc + "'");
				arrValoresParam.Add(obj.h4aberturabocaphc == null ? null : "'" + obj.h4aberturabocaphc + "'");
				arrValoresParam.Add(obj.h4movmasticatoriosphc == null ? null : "'" + obj.h4movmasticatoriosphc + "'");
				arrValoresParam.Add(obj.h4refmentonianophc == null ? null : "'" + obj.h4refmentonianophc + "'");
				arrValoresParam.Add(obj.h4facsimetriaphc == null ? null : "'" + obj.h4facsimetriaphc + "'");
				arrValoresParam.Add(obj.h4facmovimientosphc == null ? null : "'" + obj.h4facmovimientosphc + "'");
				arrValoresParam.Add(obj.h4facparalisisphc == null ? null : "'" + obj.h4facparalisisphc + "'");
				arrValoresParam.Add(obj.h4faccentralphc == null ? null : "'" + obj.h4faccentralphc + "'");
				arrValoresParam.Add(obj.h4facperifericaphc == null ? null : "'" + obj.h4facperifericaphc + "'");
				arrValoresParam.Add(obj.h4facgustophc == null ? null : "'" + obj.h4facgustophc + "'");
				arrValoresParam.Add(obj.h5otoscopiaphc == null ? null : "'" + obj.h5otoscopiaphc + "'");
				arrValoresParam.Add(obj.h5aguaudiderphc == null ? null : "'" + obj.h5aguaudiderphc + "'");
				arrValoresParam.Add(obj.h5aguaudiizqphc == null ? null : "'" + obj.h5aguaudiizqphc + "'");
				arrValoresParam.Add(obj.h5weberphc == null ? null : "'" + obj.h5weberphc + "'");
				arrValoresParam.Add(obj.h5rinnephc == null ? null : "'" + obj.h5rinnephc + "'");
				arrValoresParam.Add(obj.h5pruebaslabphc == null ? null : "'" + obj.h5pruebaslabphc + "'");
				arrValoresParam.Add(obj.h5elevpaladarphc == null ? null : "'" + obj.h5elevpaladarphc + "'");
				arrValoresParam.Add(obj.h5uvulaphc == null ? null : "'" + obj.h5uvulaphc + "'");
				arrValoresParam.Add(obj.h5refnauceosophc == null ? null : "'" + obj.h5refnauceosophc + "'");
				arrValoresParam.Add(obj.h5deglucionphc == null ? null : "'" + obj.h5deglucionphc + "'");
				arrValoresParam.Add(obj.h5tonovozphc == null ? null : "'" + obj.h5tonovozphc + "'");
				arrValoresParam.Add(obj.h5esternocleidomastoideophc == null ? null : "'" + obj.h5esternocleidomastoideophc + "'");
				arrValoresParam.Add(obj.h5trapeciophc == null ? null : "'" + obj.h5trapeciophc + "'");
				arrValoresParam.Add(obj.h5desviacionphc == null ? null : "'" + obj.h5desviacionphc + "'");
				arrValoresParam.Add(obj.h5atrofiaphc == null ? null : "'" + obj.h5atrofiaphc + "'");
				arrValoresParam.Add(obj.h5fasciculacionphc == null ? null : "'" + obj.h5fasciculacionphc + "'");
				arrValoresParam.Add(obj.h5fuerzaphc == null ? null : "'" + obj.h5fuerzaphc + "'");
				arrValoresParam.Add(obj.h5marchaphc == null ? null : "'" + obj.h5marchaphc + "'");
				arrValoresParam.Add(obj.h5tonophc == null ? null : "'" + obj.h5tonophc + "'");
				arrValoresParam.Add(obj.h5volumenphc == null ? null : "'" + obj.h5volumenphc + "'");
				arrValoresParam.Add(obj.h5fasciculacionesphc == null ? null : "'" + obj.h5fasciculacionesphc + "'");
				arrValoresParam.Add(obj.h5fuemuscularphc == null ? null : "'" + obj.h5fuemuscularphc + "'");
				arrValoresParam.Add(obj.h5movinvoluntariosphc == null ? null : "'" + obj.h5movinvoluntariosphc + "'");
				arrValoresParam.Add(obj.h5equilibratoriaphc == null ? null : "'" + obj.h5equilibratoriaphc + "'");
				arrValoresParam.Add(obj.h5rombergphc == null ? null : "'" + obj.h5rombergphc + "'");
				arrValoresParam.Add(obj.h5dednarderphc == null ? null : "'" + obj.h5dednarderphc + "'");
				arrValoresParam.Add(obj.h5dednarizqphc == null ? null : "'" + obj.h5dednarizqphc + "'");
				arrValoresParam.Add(obj.h5deddedderphc == null ? null : "'" + obj.h5deddedderphc + "'");
				arrValoresParam.Add(obj.h5deddedizqphc == null ? null : "'" + obj.h5deddedizqphc + "'");
				arrValoresParam.Add(obj.h5talrodderphc == null ? null : "'" + obj.h5talrodderphc + "'");
				arrValoresParam.Add(obj.h5talrodizqphc == null ? null : "'" + obj.h5talrodizqphc + "'");
				arrValoresParam.Add(obj.h5movrapidphc == null ? null : "'" + obj.h5movrapidphc + "'");
				arrValoresParam.Add(obj.h5rebotephc == null ? null : "'" + obj.h5rebotephc + "'");
				arrValoresParam.Add(obj.h5habilidadespecifphc == null ? null : "'" + obj.h5habilidadespecifphc + "'");
				arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region FuncionesAgregadas

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntPacHistoriaclinica2.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntPacHistoriaclinica2.Fields refField, EntPacHistoriaclinica2.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntPacHistoriaclinica2.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntPacHistoriaclinica2.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntPacHistoriaclinica2.Fields refField, EntPacHistoriaclinica2.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntPacHistoriaclinica2.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntPacHistoriaclinica2.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntPacHistoriaclinica2.Fields refField, EntPacHistoriaclinica2.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntPacHistoriaclinica2.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntPacHistoriaclinica2.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntPacHistoriaclinica2.Fields refField, EntPacHistoriaclinica2.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntPacHistoriaclinica2.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntPacHistoriaclinica2.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntPacHistoriaclinica2.Fields refField, EntPacHistoriaclinica2.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica2 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntPacHistoriaclinica2.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs SP

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica2
		/// </returns>
		public bool Insert(EntPacHistoriaclinica2 obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("idppa");
				arrValoresParam.Add(null);
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrValoresParam.Add(obj.usucre);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacHistoriaclinica2.SpPh2Ins.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica2
		/// </returns>
		public bool Insert(EntPacHistoriaclinica2 obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("idppa");
				arrValoresParam.Add("");
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrValoresParam.Add(obj.usucre);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacHistoriaclinica2.SpPh2Ins.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor que indica la cantidad de registros actualizados en pachistoriaclinica2
		/// </returns>
		public int Update(EntPacHistoriaclinica2 obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrValoresParam.Add(obj.h4olfnormalphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrValoresParam.Add(obj.h4olfanosmiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrValoresParam.Add(obj.h4optscodphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrValoresParam.Add(obj.h4optscoiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrValoresParam.Add(obj.h4optccodphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrValoresParam.Add(obj.h4optccoiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrValoresParam.Add(obj.h4fondoodphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrValoresParam.Add(obj.h4fondooiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrValoresParam.Add(obj.h4campimetriaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrValoresParam.Add(obj.h4prosisphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrValoresParam.Add(obj.h4posicionojosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrValoresParam.Add(obj.h4exoftalmiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrValoresParam.Add(obj.h4hornerphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrValoresParam.Add(obj.h4movocularesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrValoresParam.Add(obj.h4nistagmuxphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrValoresParam.Add(obj.h4diplopia1phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrValoresParam.Add(obj.h4diplopia2phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrValoresParam.Add(obj.h4diplopia3phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrValoresParam.Add(obj.h4diplopia4phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrValoresParam.Add(obj.h4diplopia5phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrValoresParam.Add(obj.h4diplopia6phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrValoresParam.Add(obj.h4diplopia7phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrValoresParam.Add(obj.h4diplopia8phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrValoresParam.Add(obj.h4diplopia9phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrValoresParam.Add(obj.h4convergenciaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrValoresParam.Add(obj.h4acomodacionodphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrValoresParam.Add(obj.h4acomodacionoiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrValoresParam.Add(obj.h4pupulasodphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrValoresParam.Add(obj.h4pupulasoiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrValoresParam.Add(obj.h4formaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrValoresParam.Add(obj.h4fotomotorodphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrValoresParam.Add(obj.h4fotomotoroiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrValoresParam.Add(obj.h4consensualdaiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrValoresParam.Add(obj.h4consensualiadphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrValoresParam.Add(obj.h4senfacialderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrValoresParam.Add(obj.h4senfacializqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrValoresParam.Add(obj.h4reflejoderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrValoresParam.Add(obj.h4reflejoizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrValoresParam.Add(obj.h4aberturabocaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrValoresParam.Add(obj.h4movmasticatoriosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrValoresParam.Add(obj.h4refmentonianophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrValoresParam.Add(obj.h4facsimetriaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrValoresParam.Add(obj.h4facmovimientosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrValoresParam.Add(obj.h4facparalisisphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrValoresParam.Add(obj.h4faccentralphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrValoresParam.Add(obj.h4facperifericaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrValoresParam.Add(obj.h4facgustophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrValoresParam.Add(obj.h5otoscopiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrValoresParam.Add(obj.h5aguaudiderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrValoresParam.Add(obj.h5aguaudiizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrValoresParam.Add(obj.h5weberphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrValoresParam.Add(obj.h5rinnephc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrValoresParam.Add(obj.h5pruebaslabphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrValoresParam.Add(obj.h5elevpaladarphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrValoresParam.Add(obj.h5uvulaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrValoresParam.Add(obj.h5refnauceosophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrValoresParam.Add(obj.h5deglucionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrValoresParam.Add(obj.h5tonovozphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrValoresParam.Add(obj.h5esternocleidomastoideophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrValoresParam.Add(obj.h5trapeciophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrValoresParam.Add(obj.h5desviacionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrValoresParam.Add(obj.h5atrofiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrValoresParam.Add(obj.h5fasciculacionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrValoresParam.Add(obj.h5fuerzaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrValoresParam.Add(obj.h5marchaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrValoresParam.Add(obj.h5tonophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrValoresParam.Add(obj.h5volumenphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrValoresParam.Add(obj.h5fasciculacionesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrValoresParam.Add(obj.h5fuemuscularphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrValoresParam.Add(obj.h5movinvoluntariosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrValoresParam.Add(obj.h5equilibratoriaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrValoresParam.Add(obj.h5rombergphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrValoresParam.Add(obj.h5dednarderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrValoresParam.Add(obj.h5dednarizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrValoresParam.Add(obj.h5deddedderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrValoresParam.Add(obj.h5deddedizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrValoresParam.Add(obj.h5talrodderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrValoresParam.Add(obj.h5talrodizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrValoresParam.Add(obj.h5movrapidphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrValoresParam.Add(obj.h5rebotephc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrValoresParam.Add(obj.h5habilidadespecifphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrValoresParam.Add(obj.apitransaccion);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrValoresParam.Add(obj.usumod);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacHistoriaclinica2.SpPh2Upd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica2
		/// </returns>
		public int Update(EntPacHistoriaclinica2 obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrValoresParam.Add(obj.h4olfnormalphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrValoresParam.Add(obj.h4olfanosmiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrValoresParam.Add(obj.h4optscodphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrValoresParam.Add(obj.h4optscoiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrValoresParam.Add(obj.h4optccodphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrValoresParam.Add(obj.h4optccoiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrValoresParam.Add(obj.h4fondoodphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrValoresParam.Add(obj.h4fondooiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrValoresParam.Add(obj.h4campimetriaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrValoresParam.Add(obj.h4prosisphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrValoresParam.Add(obj.h4posicionojosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrValoresParam.Add(obj.h4exoftalmiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrValoresParam.Add(obj.h4hornerphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrValoresParam.Add(obj.h4movocularesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrValoresParam.Add(obj.h4nistagmuxphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrValoresParam.Add(obj.h4diplopia1phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrValoresParam.Add(obj.h4diplopia2phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrValoresParam.Add(obj.h4diplopia3phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrValoresParam.Add(obj.h4diplopia4phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrValoresParam.Add(obj.h4diplopia5phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrValoresParam.Add(obj.h4diplopia6phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrValoresParam.Add(obj.h4diplopia7phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrValoresParam.Add(obj.h4diplopia8phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrValoresParam.Add(obj.h4diplopia9phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrValoresParam.Add(obj.h4convergenciaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrValoresParam.Add(obj.h4acomodacionodphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrValoresParam.Add(obj.h4acomodacionoiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrValoresParam.Add(obj.h4pupulasodphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrValoresParam.Add(obj.h4pupulasoiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrValoresParam.Add(obj.h4formaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrValoresParam.Add(obj.h4fotomotorodphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrValoresParam.Add(obj.h4fotomotoroiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrValoresParam.Add(obj.h4consensualdaiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrValoresParam.Add(obj.h4consensualiadphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrValoresParam.Add(obj.h4senfacialderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrValoresParam.Add(obj.h4senfacializqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrValoresParam.Add(obj.h4reflejoderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrValoresParam.Add(obj.h4reflejoizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrValoresParam.Add(obj.h4aberturabocaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrValoresParam.Add(obj.h4movmasticatoriosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrValoresParam.Add(obj.h4refmentonianophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrValoresParam.Add(obj.h4facsimetriaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrValoresParam.Add(obj.h4facmovimientosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrValoresParam.Add(obj.h4facparalisisphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrValoresParam.Add(obj.h4faccentralphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrValoresParam.Add(obj.h4facperifericaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrValoresParam.Add(obj.h4facgustophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrValoresParam.Add(obj.h5otoscopiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrValoresParam.Add(obj.h5aguaudiderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrValoresParam.Add(obj.h5aguaudiizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrValoresParam.Add(obj.h5weberphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrValoresParam.Add(obj.h5rinnephc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrValoresParam.Add(obj.h5pruebaslabphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrValoresParam.Add(obj.h5elevpaladarphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrValoresParam.Add(obj.h5uvulaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrValoresParam.Add(obj.h5refnauceosophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrValoresParam.Add(obj.h5deglucionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrValoresParam.Add(obj.h5tonovozphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrValoresParam.Add(obj.h5esternocleidomastoideophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrValoresParam.Add(obj.h5trapeciophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrValoresParam.Add(obj.h5desviacionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrValoresParam.Add(obj.h5atrofiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrValoresParam.Add(obj.h5fasciculacionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrValoresParam.Add(obj.h5fuerzaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrValoresParam.Add(obj.h5marchaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrValoresParam.Add(obj.h5tonophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrValoresParam.Add(obj.h5volumenphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrValoresParam.Add(obj.h5fasciculacionesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrValoresParam.Add(obj.h5fuemuscularphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrValoresParam.Add(obj.h5movinvoluntariosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrValoresParam.Add(obj.h5equilibratoriaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrValoresParam.Add(obj.h5rombergphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrValoresParam.Add(obj.h5dednarderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrValoresParam.Add(obj.h5dednarizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrValoresParam.Add(obj.h5deddedderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrValoresParam.Add(obj.h5deddedizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrValoresParam.Add(obj.h5talrodderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrValoresParam.Add(obj.h5talrodizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrValoresParam.Add(obj.h5movrapidphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrValoresParam.Add(obj.h5rebotephc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrValoresParam.Add(obj.h5habilidadespecifphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrValoresParam.Add(obj.apitransaccion);
				
				arrNombreParam.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrValoresParam.Add(obj.usumod);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacHistoriaclinica2.SpPh2Upd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica2
		/// </returns>
		public int Delete(EntPacHistoriaclinica2 obj)
		{
			throw new Exception("No existe el Procedimiento Almacenado SpPh2Del.");
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica2
		/// </returns>
		public int Delete(EntPacHistoriaclinica2 obj, ref CTrans localTrans)
		{
			throw new Exception("No existe el Procedimiento Almacenado SpPh2Del.");
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica2
		/// </returns>
		public int InsertUpdate(EntPacHistoriaclinica2 obj)
		{
			try
			{
				bool esInsertar = true;
				
					esInsertar = (esInsertar && (obj.idppa == null));
				
				if (esInsertar)
					return Insert(obj) ? 1 : 0;
				else
					return Update(obj);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica2
		/// </returns>
		public int InsertUpdate(EntPacHistoriaclinica2 obj, ref CTrans localTrans)
		{
			try
			{
				bool esInsertar = false;
				
					esInsertar = (esInsertar && (obj.idppa == null));
				
				if (esInsertar)
					return Insert(obj, ref localTrans) ? 1 : 0;
				else
					return Update(obj, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs Query

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica2
		/// </returns>
		public bool InsertQuery(EntPacHistoriaclinica2 obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.h4olfnormalphc == null ? null : "'" + obj.h4olfnormalphc + "'");
				arrValores.Add(obj.h4olfanosmiaphc == null ? null : "'" + obj.h4olfanosmiaphc + "'");
				arrValores.Add(obj.h4optscodphc == null ? null : "'" + obj.h4optscodphc + "'");
				arrValores.Add(obj.h4optscoiphc == null ? null : "'" + obj.h4optscoiphc + "'");
				arrValores.Add(obj.h4optccodphc == null ? null : "'" + obj.h4optccodphc + "'");
				arrValores.Add(obj.h4optccoiphc == null ? null : "'" + obj.h4optccoiphc + "'");
				arrValores.Add(obj.h4fondoodphc == null ? null : "'" + obj.h4fondoodphc + "'");
				arrValores.Add(obj.h4fondooiphc == null ? null : "'" + obj.h4fondooiphc + "'");
				arrValores.Add(obj.h4campimetriaphc == null ? null : "'" + obj.h4campimetriaphc + "'");
				arrValores.Add(obj.h4prosisphc == null ? null : "'" + obj.h4prosisphc + "'");
				arrValores.Add(obj.h4posicionojosphc == null ? null : "'" + obj.h4posicionojosphc + "'");
				arrValores.Add(obj.h4exoftalmiaphc == null ? null : "'" + obj.h4exoftalmiaphc + "'");
				arrValores.Add(obj.h4hornerphc == null ? null : "'" + obj.h4hornerphc + "'");
				arrValores.Add(obj.h4movocularesphc == null ? null : "'" + obj.h4movocularesphc + "'");
				arrValores.Add(obj.h4nistagmuxphc == null ? null : "'" + obj.h4nistagmuxphc + "'");
				arrValores.Add(obj.h4diplopia1phc == null ? null : "'" + obj.h4diplopia1phc + "'");
				arrValores.Add(obj.h4diplopia2phc == null ? null : "'" + obj.h4diplopia2phc + "'");
				arrValores.Add(obj.h4diplopia3phc == null ? null : "'" + obj.h4diplopia3phc + "'");
				arrValores.Add(obj.h4diplopia4phc == null ? null : "'" + obj.h4diplopia4phc + "'");
				arrValores.Add(obj.h4diplopia5phc == null ? null : "'" + obj.h4diplopia5phc + "'");
				arrValores.Add(obj.h4diplopia6phc == null ? null : "'" + obj.h4diplopia6phc + "'");
				arrValores.Add(obj.h4diplopia7phc == null ? null : "'" + obj.h4diplopia7phc + "'");
				arrValores.Add(obj.h4diplopia8phc == null ? null : "'" + obj.h4diplopia8phc + "'");
				arrValores.Add(obj.h4diplopia9phc == null ? null : "'" + obj.h4diplopia9phc + "'");
				arrValores.Add(obj.h4convergenciaphc == null ? null : "'" + obj.h4convergenciaphc + "'");
				arrValores.Add(obj.h4acomodacionodphc == null ? null : "'" + obj.h4acomodacionodphc + "'");
				arrValores.Add(obj.h4acomodacionoiphc == null ? null : "'" + obj.h4acomodacionoiphc + "'");
				arrValores.Add(obj.h4pupulasodphc == null ? null : "'" + obj.h4pupulasodphc + "'");
				arrValores.Add(obj.h4pupulasoiphc == null ? null : "'" + obj.h4pupulasoiphc + "'");
				arrValores.Add(obj.h4formaphc == null ? null : "'" + obj.h4formaphc + "'");
				arrValores.Add(obj.h4fotomotorodphc == null ? null : "'" + obj.h4fotomotorodphc + "'");
				arrValores.Add(obj.h4fotomotoroiphc == null ? null : "'" + obj.h4fotomotoroiphc + "'");
				arrValores.Add(obj.h4consensualdaiphc == null ? null : "'" + obj.h4consensualdaiphc + "'");
				arrValores.Add(obj.h4consensualiadphc == null ? null : "'" + obj.h4consensualiadphc + "'");
				arrValores.Add(obj.h4senfacialderphc == null ? null : "'" + obj.h4senfacialderphc + "'");
				arrValores.Add(obj.h4senfacializqphc == null ? null : "'" + obj.h4senfacializqphc + "'");
				arrValores.Add(obj.h4reflejoderphc == null ? null : "'" + obj.h4reflejoderphc + "'");
				arrValores.Add(obj.h4reflejoizqphc == null ? null : "'" + obj.h4reflejoizqphc + "'");
				arrValores.Add(obj.h4aberturabocaphc == null ? null : "'" + obj.h4aberturabocaphc + "'");
				arrValores.Add(obj.h4movmasticatoriosphc == null ? null : "'" + obj.h4movmasticatoriosphc + "'");
				arrValores.Add(obj.h4refmentonianophc == null ? null : "'" + obj.h4refmentonianophc + "'");
				arrValores.Add(obj.h4facsimetriaphc == null ? null : "'" + obj.h4facsimetriaphc + "'");
				arrValores.Add(obj.h4facmovimientosphc == null ? null : "'" + obj.h4facmovimientosphc + "'");
				arrValores.Add(obj.h4facparalisisphc == null ? null : "'" + obj.h4facparalisisphc + "'");
				arrValores.Add(obj.h4faccentralphc == null ? null : "'" + obj.h4faccentralphc + "'");
				arrValores.Add(obj.h4facperifericaphc == null ? null : "'" + obj.h4facperifericaphc + "'");
				arrValores.Add(obj.h4facgustophc == null ? null : "'" + obj.h4facgustophc + "'");
				arrValores.Add(obj.h5otoscopiaphc == null ? null : "'" + obj.h5otoscopiaphc + "'");
				arrValores.Add(obj.h5aguaudiderphc == null ? null : "'" + obj.h5aguaudiderphc + "'");
				arrValores.Add(obj.h5aguaudiizqphc == null ? null : "'" + obj.h5aguaudiizqphc + "'");
				arrValores.Add(obj.h5weberphc == null ? null : "'" + obj.h5weberphc + "'");
				arrValores.Add(obj.h5rinnephc == null ? null : "'" + obj.h5rinnephc + "'");
				arrValores.Add(obj.h5pruebaslabphc == null ? null : "'" + obj.h5pruebaslabphc + "'");
				arrValores.Add(obj.h5elevpaladarphc == null ? null : "'" + obj.h5elevpaladarphc + "'");
				arrValores.Add(obj.h5uvulaphc == null ? null : "'" + obj.h5uvulaphc + "'");
				arrValores.Add(obj.h5refnauceosophc == null ? null : "'" + obj.h5refnauceosophc + "'");
				arrValores.Add(obj.h5deglucionphc == null ? null : "'" + obj.h5deglucionphc + "'");
				arrValores.Add(obj.h5tonovozphc == null ? null : "'" + obj.h5tonovozphc + "'");
				arrValores.Add(obj.h5esternocleidomastoideophc == null ? null : "'" + obj.h5esternocleidomastoideophc + "'");
				arrValores.Add(obj.h5trapeciophc == null ? null : "'" + obj.h5trapeciophc + "'");
				arrValores.Add(obj.h5desviacionphc == null ? null : "'" + obj.h5desviacionphc + "'");
				arrValores.Add(obj.h5atrofiaphc == null ? null : "'" + obj.h5atrofiaphc + "'");
				arrValores.Add(obj.h5fasciculacionphc == null ? null : "'" + obj.h5fasciculacionphc + "'");
				arrValores.Add(obj.h5fuerzaphc == null ? null : "'" + obj.h5fuerzaphc + "'");
				arrValores.Add(obj.h5marchaphc == null ? null : "'" + obj.h5marchaphc + "'");
				arrValores.Add(obj.h5tonophc == null ? null : "'" + obj.h5tonophc + "'");
				arrValores.Add(obj.h5volumenphc == null ? null : "'" + obj.h5volumenphc + "'");
				arrValores.Add(obj.h5fasciculacionesphc == null ? null : "'" + obj.h5fasciculacionesphc + "'");
				arrValores.Add(obj.h5fuemuscularphc == null ? null : "'" + obj.h5fuemuscularphc + "'");
				arrValores.Add(obj.h5movinvoluntariosphc == null ? null : "'" + obj.h5movinvoluntariosphc + "'");
				arrValores.Add(obj.h5equilibratoriaphc == null ? null : "'" + obj.h5equilibratoriaphc + "'");
				arrValores.Add(obj.h5rombergphc == null ? null : "'" + obj.h5rombergphc + "'");
				arrValores.Add(obj.h5dednarderphc == null ? null : "'" + obj.h5dednarderphc + "'");
				arrValores.Add(obj.h5dednarizqphc == null ? null : "'" + obj.h5dednarizqphc + "'");
				arrValores.Add(obj.h5deddedderphc == null ? null : "'" + obj.h5deddedderphc + "'");
				arrValores.Add(obj.h5deddedizqphc == null ? null : "'" + obj.h5deddedizqphc + "'");
				arrValores.Add(obj.h5talrodderphc == null ? null : "'" + obj.h5talrodderphc + "'");
				arrValores.Add(obj.h5talrodizqphc == null ? null : "'" + obj.h5talrodizqphc + "'");
				arrValores.Add(obj.h5movrapidphc == null ? null : "'" + obj.h5movrapidphc + "'");
				arrValores.Add(obj.h5rebotephc == null ? null : "'" + obj.h5rebotephc + "'");
				arrValores.Add(obj.h5habilidadespecifphc == null ? null : "'" + obj.h5habilidadespecifphc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica2
		/// </returns>
		public bool InsertQuery(EntPacHistoriaclinica2 obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.h4olfnormalphc == null ? null : "'" + obj.h4olfnormalphc + "'");
				arrValores.Add(obj.h4olfanosmiaphc == null ? null : "'" + obj.h4olfanosmiaphc + "'");
				arrValores.Add(obj.h4optscodphc == null ? null : "'" + obj.h4optscodphc + "'");
				arrValores.Add(obj.h4optscoiphc == null ? null : "'" + obj.h4optscoiphc + "'");
				arrValores.Add(obj.h4optccodphc == null ? null : "'" + obj.h4optccodphc + "'");
				arrValores.Add(obj.h4optccoiphc == null ? null : "'" + obj.h4optccoiphc + "'");
				arrValores.Add(obj.h4fondoodphc == null ? null : "'" + obj.h4fondoodphc + "'");
				arrValores.Add(obj.h4fondooiphc == null ? null : "'" + obj.h4fondooiphc + "'");
				arrValores.Add(obj.h4campimetriaphc == null ? null : "'" + obj.h4campimetriaphc + "'");
				arrValores.Add(obj.h4prosisphc == null ? null : "'" + obj.h4prosisphc + "'");
				arrValores.Add(obj.h4posicionojosphc == null ? null : "'" + obj.h4posicionojosphc + "'");
				arrValores.Add(obj.h4exoftalmiaphc == null ? null : "'" + obj.h4exoftalmiaphc + "'");
				arrValores.Add(obj.h4hornerphc == null ? null : "'" + obj.h4hornerphc + "'");
				arrValores.Add(obj.h4movocularesphc == null ? null : "'" + obj.h4movocularesphc + "'");
				arrValores.Add(obj.h4nistagmuxphc == null ? null : "'" + obj.h4nistagmuxphc + "'");
				arrValores.Add(obj.h4diplopia1phc == null ? null : "'" + obj.h4diplopia1phc + "'");
				arrValores.Add(obj.h4diplopia2phc == null ? null : "'" + obj.h4diplopia2phc + "'");
				arrValores.Add(obj.h4diplopia3phc == null ? null : "'" + obj.h4diplopia3phc + "'");
				arrValores.Add(obj.h4diplopia4phc == null ? null : "'" + obj.h4diplopia4phc + "'");
				arrValores.Add(obj.h4diplopia5phc == null ? null : "'" + obj.h4diplopia5phc + "'");
				arrValores.Add(obj.h4diplopia6phc == null ? null : "'" + obj.h4diplopia6phc + "'");
				arrValores.Add(obj.h4diplopia7phc == null ? null : "'" + obj.h4diplopia7phc + "'");
				arrValores.Add(obj.h4diplopia8phc == null ? null : "'" + obj.h4diplopia8phc + "'");
				arrValores.Add(obj.h4diplopia9phc == null ? null : "'" + obj.h4diplopia9phc + "'");
				arrValores.Add(obj.h4convergenciaphc == null ? null : "'" + obj.h4convergenciaphc + "'");
				arrValores.Add(obj.h4acomodacionodphc == null ? null : "'" + obj.h4acomodacionodphc + "'");
				arrValores.Add(obj.h4acomodacionoiphc == null ? null : "'" + obj.h4acomodacionoiphc + "'");
				arrValores.Add(obj.h4pupulasodphc == null ? null : "'" + obj.h4pupulasodphc + "'");
				arrValores.Add(obj.h4pupulasoiphc == null ? null : "'" + obj.h4pupulasoiphc + "'");
				arrValores.Add(obj.h4formaphc == null ? null : "'" + obj.h4formaphc + "'");
				arrValores.Add(obj.h4fotomotorodphc == null ? null : "'" + obj.h4fotomotorodphc + "'");
				arrValores.Add(obj.h4fotomotoroiphc == null ? null : "'" + obj.h4fotomotoroiphc + "'");
				arrValores.Add(obj.h4consensualdaiphc == null ? null : "'" + obj.h4consensualdaiphc + "'");
				arrValores.Add(obj.h4consensualiadphc == null ? null : "'" + obj.h4consensualiadphc + "'");
				arrValores.Add(obj.h4senfacialderphc == null ? null : "'" + obj.h4senfacialderphc + "'");
				arrValores.Add(obj.h4senfacializqphc == null ? null : "'" + obj.h4senfacializqphc + "'");
				arrValores.Add(obj.h4reflejoderphc == null ? null : "'" + obj.h4reflejoderphc + "'");
				arrValores.Add(obj.h4reflejoizqphc == null ? null : "'" + obj.h4reflejoizqphc + "'");
				arrValores.Add(obj.h4aberturabocaphc == null ? null : "'" + obj.h4aberturabocaphc + "'");
				arrValores.Add(obj.h4movmasticatoriosphc == null ? null : "'" + obj.h4movmasticatoriosphc + "'");
				arrValores.Add(obj.h4refmentonianophc == null ? null : "'" + obj.h4refmentonianophc + "'");
				arrValores.Add(obj.h4facsimetriaphc == null ? null : "'" + obj.h4facsimetriaphc + "'");
				arrValores.Add(obj.h4facmovimientosphc == null ? null : "'" + obj.h4facmovimientosphc + "'");
				arrValores.Add(obj.h4facparalisisphc == null ? null : "'" + obj.h4facparalisisphc + "'");
				arrValores.Add(obj.h4faccentralphc == null ? null : "'" + obj.h4faccentralphc + "'");
				arrValores.Add(obj.h4facperifericaphc == null ? null : "'" + obj.h4facperifericaphc + "'");
				arrValores.Add(obj.h4facgustophc == null ? null : "'" + obj.h4facgustophc + "'");
				arrValores.Add(obj.h5otoscopiaphc == null ? null : "'" + obj.h5otoscopiaphc + "'");
				arrValores.Add(obj.h5aguaudiderphc == null ? null : "'" + obj.h5aguaudiderphc + "'");
				arrValores.Add(obj.h5aguaudiizqphc == null ? null : "'" + obj.h5aguaudiizqphc + "'");
				arrValores.Add(obj.h5weberphc == null ? null : "'" + obj.h5weberphc + "'");
				arrValores.Add(obj.h5rinnephc == null ? null : "'" + obj.h5rinnephc + "'");
				arrValores.Add(obj.h5pruebaslabphc == null ? null : "'" + obj.h5pruebaslabphc + "'");
				arrValores.Add(obj.h5elevpaladarphc == null ? null : "'" + obj.h5elevpaladarphc + "'");
				arrValores.Add(obj.h5uvulaphc == null ? null : "'" + obj.h5uvulaphc + "'");
				arrValores.Add(obj.h5refnauceosophc == null ? null : "'" + obj.h5refnauceosophc + "'");
				arrValores.Add(obj.h5deglucionphc == null ? null : "'" + obj.h5deglucionphc + "'");
				arrValores.Add(obj.h5tonovozphc == null ? null : "'" + obj.h5tonovozphc + "'");
				arrValores.Add(obj.h5esternocleidomastoideophc == null ? null : "'" + obj.h5esternocleidomastoideophc + "'");
				arrValores.Add(obj.h5trapeciophc == null ? null : "'" + obj.h5trapeciophc + "'");
				arrValores.Add(obj.h5desviacionphc == null ? null : "'" + obj.h5desviacionphc + "'");
				arrValores.Add(obj.h5atrofiaphc == null ? null : "'" + obj.h5atrofiaphc + "'");
				arrValores.Add(obj.h5fasciculacionphc == null ? null : "'" + obj.h5fasciculacionphc + "'");
				arrValores.Add(obj.h5fuerzaphc == null ? null : "'" + obj.h5fuerzaphc + "'");
				arrValores.Add(obj.h5marchaphc == null ? null : "'" + obj.h5marchaphc + "'");
				arrValores.Add(obj.h5tonophc == null ? null : "'" + obj.h5tonophc + "'");
				arrValores.Add(obj.h5volumenphc == null ? null : "'" + obj.h5volumenphc + "'");
				arrValores.Add(obj.h5fasciculacionesphc == null ? null : "'" + obj.h5fasciculacionesphc + "'");
				arrValores.Add(obj.h5fuemuscularphc == null ? null : "'" + obj.h5fuemuscularphc + "'");
				arrValores.Add(obj.h5movinvoluntariosphc == null ? null : "'" + obj.h5movinvoluntariosphc + "'");
				arrValores.Add(obj.h5equilibratoriaphc == null ? null : "'" + obj.h5equilibratoriaphc + "'");
				arrValores.Add(obj.h5rombergphc == null ? null : "'" + obj.h5rombergphc + "'");
				arrValores.Add(obj.h5dednarderphc == null ? null : "'" + obj.h5dednarderphc + "'");
				arrValores.Add(obj.h5dednarizqphc == null ? null : "'" + obj.h5dednarizqphc + "'");
				arrValores.Add(obj.h5deddedderphc == null ? null : "'" + obj.h5deddedderphc + "'");
				arrValores.Add(obj.h5deddedizqphc == null ? null : "'" + obj.h5deddedizqphc + "'");
				arrValores.Add(obj.h5talrodderphc == null ? null : "'" + obj.h5talrodderphc + "'");
				arrValores.Add(obj.h5talrodizqphc == null ? null : "'" + obj.h5talrodizqphc + "'");
				arrValores.Add(obj.h5movrapidphc == null ? null : "'" + obj.h5movrapidphc + "'");
				arrValores.Add(obj.h5rebotephc == null ? null : "'" + obj.h5rebotephc + "'");
				arrValores.Add(obj.h5habilidadespecifphc == null ? null : "'" + obj.h5habilidadespecifphc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica2
		/// </returns>
		public bool InsertQueryIdentity(EntPacHistoriaclinica2 obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.h4olfnormalphc == null ? null : "'" + obj.h4olfnormalphc + "'");
				arrValores.Add(obj.h4olfanosmiaphc == null ? null : "'" + obj.h4olfanosmiaphc + "'");
				arrValores.Add(obj.h4optscodphc == null ? null : "'" + obj.h4optscodphc + "'");
				arrValores.Add(obj.h4optscoiphc == null ? null : "'" + obj.h4optscoiphc + "'");
				arrValores.Add(obj.h4optccodphc == null ? null : "'" + obj.h4optccodphc + "'");
				arrValores.Add(obj.h4optccoiphc == null ? null : "'" + obj.h4optccoiphc + "'");
				arrValores.Add(obj.h4fondoodphc == null ? null : "'" + obj.h4fondoodphc + "'");
				arrValores.Add(obj.h4fondooiphc == null ? null : "'" + obj.h4fondooiphc + "'");
				arrValores.Add(obj.h4campimetriaphc == null ? null : "'" + obj.h4campimetriaphc + "'");
				arrValores.Add(obj.h4prosisphc == null ? null : "'" + obj.h4prosisphc + "'");
				arrValores.Add(obj.h4posicionojosphc == null ? null : "'" + obj.h4posicionojosphc + "'");
				arrValores.Add(obj.h4exoftalmiaphc == null ? null : "'" + obj.h4exoftalmiaphc + "'");
				arrValores.Add(obj.h4hornerphc == null ? null : "'" + obj.h4hornerphc + "'");
				arrValores.Add(obj.h4movocularesphc == null ? null : "'" + obj.h4movocularesphc + "'");
				arrValores.Add(obj.h4nistagmuxphc == null ? null : "'" + obj.h4nistagmuxphc + "'");
				arrValores.Add(obj.h4diplopia1phc == null ? null : "'" + obj.h4diplopia1phc + "'");
				arrValores.Add(obj.h4diplopia2phc == null ? null : "'" + obj.h4diplopia2phc + "'");
				arrValores.Add(obj.h4diplopia3phc == null ? null : "'" + obj.h4diplopia3phc + "'");
				arrValores.Add(obj.h4diplopia4phc == null ? null : "'" + obj.h4diplopia4phc + "'");
				arrValores.Add(obj.h4diplopia5phc == null ? null : "'" + obj.h4diplopia5phc + "'");
				arrValores.Add(obj.h4diplopia6phc == null ? null : "'" + obj.h4diplopia6phc + "'");
				arrValores.Add(obj.h4diplopia7phc == null ? null : "'" + obj.h4diplopia7phc + "'");
				arrValores.Add(obj.h4diplopia8phc == null ? null : "'" + obj.h4diplopia8phc + "'");
				arrValores.Add(obj.h4diplopia9phc == null ? null : "'" + obj.h4diplopia9phc + "'");
				arrValores.Add(obj.h4convergenciaphc == null ? null : "'" + obj.h4convergenciaphc + "'");
				arrValores.Add(obj.h4acomodacionodphc == null ? null : "'" + obj.h4acomodacionodphc + "'");
				arrValores.Add(obj.h4acomodacionoiphc == null ? null : "'" + obj.h4acomodacionoiphc + "'");
				arrValores.Add(obj.h4pupulasodphc == null ? null : "'" + obj.h4pupulasodphc + "'");
				arrValores.Add(obj.h4pupulasoiphc == null ? null : "'" + obj.h4pupulasoiphc + "'");
				arrValores.Add(obj.h4formaphc == null ? null : "'" + obj.h4formaphc + "'");
				arrValores.Add(obj.h4fotomotorodphc == null ? null : "'" + obj.h4fotomotorodphc + "'");
				arrValores.Add(obj.h4fotomotoroiphc == null ? null : "'" + obj.h4fotomotoroiphc + "'");
				arrValores.Add(obj.h4consensualdaiphc == null ? null : "'" + obj.h4consensualdaiphc + "'");
				arrValores.Add(obj.h4consensualiadphc == null ? null : "'" + obj.h4consensualiadphc + "'");
				arrValores.Add(obj.h4senfacialderphc == null ? null : "'" + obj.h4senfacialderphc + "'");
				arrValores.Add(obj.h4senfacializqphc == null ? null : "'" + obj.h4senfacializqphc + "'");
				arrValores.Add(obj.h4reflejoderphc == null ? null : "'" + obj.h4reflejoderphc + "'");
				arrValores.Add(obj.h4reflejoizqphc == null ? null : "'" + obj.h4reflejoizqphc + "'");
				arrValores.Add(obj.h4aberturabocaphc == null ? null : "'" + obj.h4aberturabocaphc + "'");
				arrValores.Add(obj.h4movmasticatoriosphc == null ? null : "'" + obj.h4movmasticatoriosphc + "'");
				arrValores.Add(obj.h4refmentonianophc == null ? null : "'" + obj.h4refmentonianophc + "'");
				arrValores.Add(obj.h4facsimetriaphc == null ? null : "'" + obj.h4facsimetriaphc + "'");
				arrValores.Add(obj.h4facmovimientosphc == null ? null : "'" + obj.h4facmovimientosphc + "'");
				arrValores.Add(obj.h4facparalisisphc == null ? null : "'" + obj.h4facparalisisphc + "'");
				arrValores.Add(obj.h4faccentralphc == null ? null : "'" + obj.h4faccentralphc + "'");
				arrValores.Add(obj.h4facperifericaphc == null ? null : "'" + obj.h4facperifericaphc + "'");
				arrValores.Add(obj.h4facgustophc == null ? null : "'" + obj.h4facgustophc + "'");
				arrValores.Add(obj.h5otoscopiaphc == null ? null : "'" + obj.h5otoscopiaphc + "'");
				arrValores.Add(obj.h5aguaudiderphc == null ? null : "'" + obj.h5aguaudiderphc + "'");
				arrValores.Add(obj.h5aguaudiizqphc == null ? null : "'" + obj.h5aguaudiizqphc + "'");
				arrValores.Add(obj.h5weberphc == null ? null : "'" + obj.h5weberphc + "'");
				arrValores.Add(obj.h5rinnephc == null ? null : "'" + obj.h5rinnephc + "'");
				arrValores.Add(obj.h5pruebaslabphc == null ? null : "'" + obj.h5pruebaslabphc + "'");
				arrValores.Add(obj.h5elevpaladarphc == null ? null : "'" + obj.h5elevpaladarphc + "'");
				arrValores.Add(obj.h5uvulaphc == null ? null : "'" + obj.h5uvulaphc + "'");
				arrValores.Add(obj.h5refnauceosophc == null ? null : "'" + obj.h5refnauceosophc + "'");
				arrValores.Add(obj.h5deglucionphc == null ? null : "'" + obj.h5deglucionphc + "'");
				arrValores.Add(obj.h5tonovozphc == null ? null : "'" + obj.h5tonovozphc + "'");
				arrValores.Add(obj.h5esternocleidomastoideophc == null ? null : "'" + obj.h5esternocleidomastoideophc + "'");
				arrValores.Add(obj.h5trapeciophc == null ? null : "'" + obj.h5trapeciophc + "'");
				arrValores.Add(obj.h5desviacionphc == null ? null : "'" + obj.h5desviacionphc + "'");
				arrValores.Add(obj.h5atrofiaphc == null ? null : "'" + obj.h5atrofiaphc + "'");
				arrValores.Add(obj.h5fasciculacionphc == null ? null : "'" + obj.h5fasciculacionphc + "'");
				arrValores.Add(obj.h5fuerzaphc == null ? null : "'" + obj.h5fuerzaphc + "'");
				arrValores.Add(obj.h5marchaphc == null ? null : "'" + obj.h5marchaphc + "'");
				arrValores.Add(obj.h5tonophc == null ? null : "'" + obj.h5tonophc + "'");
				arrValores.Add(obj.h5volumenphc == null ? null : "'" + obj.h5volumenphc + "'");
				arrValores.Add(obj.h5fasciculacionesphc == null ? null : "'" + obj.h5fasciculacionesphc + "'");
				arrValores.Add(obj.h5fuemuscularphc == null ? null : "'" + obj.h5fuemuscularphc + "'");
				arrValores.Add(obj.h5movinvoluntariosphc == null ? null : "'" + obj.h5movinvoluntariosphc + "'");
				arrValores.Add(obj.h5equilibratoriaphc == null ? null : "'" + obj.h5equilibratoriaphc + "'");
				arrValores.Add(obj.h5rombergphc == null ? null : "'" + obj.h5rombergphc + "'");
				arrValores.Add(obj.h5dednarderphc == null ? null : "'" + obj.h5dednarderphc + "'");
				arrValores.Add(obj.h5dednarizqphc == null ? null : "'" + obj.h5dednarizqphc + "'");
				arrValores.Add(obj.h5deddedderphc == null ? null : "'" + obj.h5deddedderphc + "'");
				arrValores.Add(obj.h5deddedizqphc == null ? null : "'" + obj.h5deddedizqphc + "'");
				arrValores.Add(obj.h5talrodderphc == null ? null : "'" + obj.h5talrodderphc + "'");
				arrValores.Add(obj.h5talrodizqphc == null ? null : "'" + obj.h5talrodizqphc + "'");
				arrValores.Add(obj.h5movrapidphc == null ? null : "'" + obj.h5movrapidphc + "'");
				arrValores.Add(obj.h5rebotephc == null ? null : "'" + obj.h5rebotephc + "'");
				arrValores.Add(obj.h5habilidadespecifphc == null ? null : "'" + obj.h5habilidadespecifphc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.idppa = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica2
		/// </returns>
		public bool InsertQueryIdentity(EntPacHistoriaclinica2 obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.h4olfnormalphc == null ? null : "'" + obj.h4olfnormalphc + "'");
				arrValores.Add(obj.h4olfanosmiaphc == null ? null : "'" + obj.h4olfanosmiaphc + "'");
				arrValores.Add(obj.h4optscodphc == null ? null : "'" + obj.h4optscodphc + "'");
				arrValores.Add(obj.h4optscoiphc == null ? null : "'" + obj.h4optscoiphc + "'");
				arrValores.Add(obj.h4optccodphc == null ? null : "'" + obj.h4optccodphc + "'");
				arrValores.Add(obj.h4optccoiphc == null ? null : "'" + obj.h4optccoiphc + "'");
				arrValores.Add(obj.h4fondoodphc == null ? null : "'" + obj.h4fondoodphc + "'");
				arrValores.Add(obj.h4fondooiphc == null ? null : "'" + obj.h4fondooiphc + "'");
				arrValores.Add(obj.h4campimetriaphc == null ? null : "'" + obj.h4campimetriaphc + "'");
				arrValores.Add(obj.h4prosisphc == null ? null : "'" + obj.h4prosisphc + "'");
				arrValores.Add(obj.h4posicionojosphc == null ? null : "'" + obj.h4posicionojosphc + "'");
				arrValores.Add(obj.h4exoftalmiaphc == null ? null : "'" + obj.h4exoftalmiaphc + "'");
				arrValores.Add(obj.h4hornerphc == null ? null : "'" + obj.h4hornerphc + "'");
				arrValores.Add(obj.h4movocularesphc == null ? null : "'" + obj.h4movocularesphc + "'");
				arrValores.Add(obj.h4nistagmuxphc == null ? null : "'" + obj.h4nistagmuxphc + "'");
				arrValores.Add(obj.h4diplopia1phc == null ? null : "'" + obj.h4diplopia1phc + "'");
				arrValores.Add(obj.h4diplopia2phc == null ? null : "'" + obj.h4diplopia2phc + "'");
				arrValores.Add(obj.h4diplopia3phc == null ? null : "'" + obj.h4diplopia3phc + "'");
				arrValores.Add(obj.h4diplopia4phc == null ? null : "'" + obj.h4diplopia4phc + "'");
				arrValores.Add(obj.h4diplopia5phc == null ? null : "'" + obj.h4diplopia5phc + "'");
				arrValores.Add(obj.h4diplopia6phc == null ? null : "'" + obj.h4diplopia6phc + "'");
				arrValores.Add(obj.h4diplopia7phc == null ? null : "'" + obj.h4diplopia7phc + "'");
				arrValores.Add(obj.h4diplopia8phc == null ? null : "'" + obj.h4diplopia8phc + "'");
				arrValores.Add(obj.h4diplopia9phc == null ? null : "'" + obj.h4diplopia9phc + "'");
				arrValores.Add(obj.h4convergenciaphc == null ? null : "'" + obj.h4convergenciaphc + "'");
				arrValores.Add(obj.h4acomodacionodphc == null ? null : "'" + obj.h4acomodacionodphc + "'");
				arrValores.Add(obj.h4acomodacionoiphc == null ? null : "'" + obj.h4acomodacionoiphc + "'");
				arrValores.Add(obj.h4pupulasodphc == null ? null : "'" + obj.h4pupulasodphc + "'");
				arrValores.Add(obj.h4pupulasoiphc == null ? null : "'" + obj.h4pupulasoiphc + "'");
				arrValores.Add(obj.h4formaphc == null ? null : "'" + obj.h4formaphc + "'");
				arrValores.Add(obj.h4fotomotorodphc == null ? null : "'" + obj.h4fotomotorodphc + "'");
				arrValores.Add(obj.h4fotomotoroiphc == null ? null : "'" + obj.h4fotomotoroiphc + "'");
				arrValores.Add(obj.h4consensualdaiphc == null ? null : "'" + obj.h4consensualdaiphc + "'");
				arrValores.Add(obj.h4consensualiadphc == null ? null : "'" + obj.h4consensualiadphc + "'");
				arrValores.Add(obj.h4senfacialderphc == null ? null : "'" + obj.h4senfacialderphc + "'");
				arrValores.Add(obj.h4senfacializqphc == null ? null : "'" + obj.h4senfacializqphc + "'");
				arrValores.Add(obj.h4reflejoderphc == null ? null : "'" + obj.h4reflejoderphc + "'");
				arrValores.Add(obj.h4reflejoizqphc == null ? null : "'" + obj.h4reflejoizqphc + "'");
				arrValores.Add(obj.h4aberturabocaphc == null ? null : "'" + obj.h4aberturabocaphc + "'");
				arrValores.Add(obj.h4movmasticatoriosphc == null ? null : "'" + obj.h4movmasticatoriosphc + "'");
				arrValores.Add(obj.h4refmentonianophc == null ? null : "'" + obj.h4refmentonianophc + "'");
				arrValores.Add(obj.h4facsimetriaphc == null ? null : "'" + obj.h4facsimetriaphc + "'");
				arrValores.Add(obj.h4facmovimientosphc == null ? null : "'" + obj.h4facmovimientosphc + "'");
				arrValores.Add(obj.h4facparalisisphc == null ? null : "'" + obj.h4facparalisisphc + "'");
				arrValores.Add(obj.h4faccentralphc == null ? null : "'" + obj.h4faccentralphc + "'");
				arrValores.Add(obj.h4facperifericaphc == null ? null : "'" + obj.h4facperifericaphc + "'");
				arrValores.Add(obj.h4facgustophc == null ? null : "'" + obj.h4facgustophc + "'");
				arrValores.Add(obj.h5otoscopiaphc == null ? null : "'" + obj.h5otoscopiaphc + "'");
				arrValores.Add(obj.h5aguaudiderphc == null ? null : "'" + obj.h5aguaudiderphc + "'");
				arrValores.Add(obj.h5aguaudiizqphc == null ? null : "'" + obj.h5aguaudiizqphc + "'");
				arrValores.Add(obj.h5weberphc == null ? null : "'" + obj.h5weberphc + "'");
				arrValores.Add(obj.h5rinnephc == null ? null : "'" + obj.h5rinnephc + "'");
				arrValores.Add(obj.h5pruebaslabphc == null ? null : "'" + obj.h5pruebaslabphc + "'");
				arrValores.Add(obj.h5elevpaladarphc == null ? null : "'" + obj.h5elevpaladarphc + "'");
				arrValores.Add(obj.h5uvulaphc == null ? null : "'" + obj.h5uvulaphc + "'");
				arrValores.Add(obj.h5refnauceosophc == null ? null : "'" + obj.h5refnauceosophc + "'");
				arrValores.Add(obj.h5deglucionphc == null ? null : "'" + obj.h5deglucionphc + "'");
				arrValores.Add(obj.h5tonovozphc == null ? null : "'" + obj.h5tonovozphc + "'");
				arrValores.Add(obj.h5esternocleidomastoideophc == null ? null : "'" + obj.h5esternocleidomastoideophc + "'");
				arrValores.Add(obj.h5trapeciophc == null ? null : "'" + obj.h5trapeciophc + "'");
				arrValores.Add(obj.h5desviacionphc == null ? null : "'" + obj.h5desviacionphc + "'");
				arrValores.Add(obj.h5atrofiaphc == null ? null : "'" + obj.h5atrofiaphc + "'");
				arrValores.Add(obj.h5fasciculacionphc == null ? null : "'" + obj.h5fasciculacionphc + "'");
				arrValores.Add(obj.h5fuerzaphc == null ? null : "'" + obj.h5fuerzaphc + "'");
				arrValores.Add(obj.h5marchaphc == null ? null : "'" + obj.h5marchaphc + "'");
				arrValores.Add(obj.h5tonophc == null ? null : "'" + obj.h5tonophc + "'");
				arrValores.Add(obj.h5volumenphc == null ? null : "'" + obj.h5volumenphc + "'");
				arrValores.Add(obj.h5fasciculacionesphc == null ? null : "'" + obj.h5fasciculacionesphc + "'");
				arrValores.Add(obj.h5fuemuscularphc == null ? null : "'" + obj.h5fuemuscularphc + "'");
				arrValores.Add(obj.h5movinvoluntariosphc == null ? null : "'" + obj.h5movinvoluntariosphc + "'");
				arrValores.Add(obj.h5equilibratoriaphc == null ? null : "'" + obj.h5equilibratoriaphc + "'");
				arrValores.Add(obj.h5rombergphc == null ? null : "'" + obj.h5rombergphc + "'");
				arrValores.Add(obj.h5dednarderphc == null ? null : "'" + obj.h5dednarderphc + "'");
				arrValores.Add(obj.h5dednarizqphc == null ? null : "'" + obj.h5dednarizqphc + "'");
				arrValores.Add(obj.h5deddedderphc == null ? null : "'" + obj.h5deddedderphc + "'");
				arrValores.Add(obj.h5deddedizqphc == null ? null : "'" + obj.h5deddedizqphc + "'");
				arrValores.Add(obj.h5talrodderphc == null ? null : "'" + obj.h5talrodderphc + "'");
				arrValores.Add(obj.h5talrodizqphc == null ? null : "'" + obj.h5talrodizqphc + "'");
				arrValores.Add(obj.h5movrapidphc == null ? null : "'" + obj.h5movrapidphc + "'");
				arrValores.Add(obj.h5rebotephc == null ? null : "'" + obj.h5rebotephc + "'");
				arrValores.Add(obj.h5habilidadespecifphc == null ? null : "'" + obj.h5habilidadespecifphc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.idppa = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpachistoriaclinica2
		/// </returns>
		public int UpdateQueryAll(EntPacHistoriaclinica2 obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.h4olfnormalphc == null ? null : "'" + obj.h4olfnormalphc + "'");
				arrValores.Add(obj.h4olfanosmiaphc == null ? null : "'" + obj.h4olfanosmiaphc + "'");
				arrValores.Add(obj.h4optscodphc == null ? null : "'" + obj.h4optscodphc + "'");
				arrValores.Add(obj.h4optscoiphc == null ? null : "'" + obj.h4optscoiphc + "'");
				arrValores.Add(obj.h4optccodphc == null ? null : "'" + obj.h4optccodphc + "'");
				arrValores.Add(obj.h4optccoiphc == null ? null : "'" + obj.h4optccoiphc + "'");
				arrValores.Add(obj.h4fondoodphc == null ? null : "'" + obj.h4fondoodphc + "'");
				arrValores.Add(obj.h4fondooiphc == null ? null : "'" + obj.h4fondooiphc + "'");
				arrValores.Add(obj.h4campimetriaphc == null ? null : "'" + obj.h4campimetriaphc + "'");
				arrValores.Add(obj.h4prosisphc == null ? null : "'" + obj.h4prosisphc + "'");
				arrValores.Add(obj.h4posicionojosphc == null ? null : "'" + obj.h4posicionojosphc + "'");
				arrValores.Add(obj.h4exoftalmiaphc == null ? null : "'" + obj.h4exoftalmiaphc + "'");
				arrValores.Add(obj.h4hornerphc == null ? null : "'" + obj.h4hornerphc + "'");
				arrValores.Add(obj.h4movocularesphc == null ? null : "'" + obj.h4movocularesphc + "'");
				arrValores.Add(obj.h4nistagmuxphc == null ? null : "'" + obj.h4nistagmuxphc + "'");
				arrValores.Add(obj.h4diplopia1phc == null ? null : "'" + obj.h4diplopia1phc + "'");
				arrValores.Add(obj.h4diplopia2phc == null ? null : "'" + obj.h4diplopia2phc + "'");
				arrValores.Add(obj.h4diplopia3phc == null ? null : "'" + obj.h4diplopia3phc + "'");
				arrValores.Add(obj.h4diplopia4phc == null ? null : "'" + obj.h4diplopia4phc + "'");
				arrValores.Add(obj.h4diplopia5phc == null ? null : "'" + obj.h4diplopia5phc + "'");
				arrValores.Add(obj.h4diplopia6phc == null ? null : "'" + obj.h4diplopia6phc + "'");
				arrValores.Add(obj.h4diplopia7phc == null ? null : "'" + obj.h4diplopia7phc + "'");
				arrValores.Add(obj.h4diplopia8phc == null ? null : "'" + obj.h4diplopia8phc + "'");
				arrValores.Add(obj.h4diplopia9phc == null ? null : "'" + obj.h4diplopia9phc + "'");
				arrValores.Add(obj.h4convergenciaphc == null ? null : "'" + obj.h4convergenciaphc + "'");
				arrValores.Add(obj.h4acomodacionodphc == null ? null : "'" + obj.h4acomodacionodphc + "'");
				arrValores.Add(obj.h4acomodacionoiphc == null ? null : "'" + obj.h4acomodacionoiphc + "'");
				arrValores.Add(obj.h4pupulasodphc == null ? null : "'" + obj.h4pupulasodphc + "'");
				arrValores.Add(obj.h4pupulasoiphc == null ? null : "'" + obj.h4pupulasoiphc + "'");
				arrValores.Add(obj.h4formaphc == null ? null : "'" + obj.h4formaphc + "'");
				arrValores.Add(obj.h4fotomotorodphc == null ? null : "'" + obj.h4fotomotorodphc + "'");
				arrValores.Add(obj.h4fotomotoroiphc == null ? null : "'" + obj.h4fotomotoroiphc + "'");
				arrValores.Add(obj.h4consensualdaiphc == null ? null : "'" + obj.h4consensualdaiphc + "'");
				arrValores.Add(obj.h4consensualiadphc == null ? null : "'" + obj.h4consensualiadphc + "'");
				arrValores.Add(obj.h4senfacialderphc == null ? null : "'" + obj.h4senfacialderphc + "'");
				arrValores.Add(obj.h4senfacializqphc == null ? null : "'" + obj.h4senfacializqphc + "'");
				arrValores.Add(obj.h4reflejoderphc == null ? null : "'" + obj.h4reflejoderphc + "'");
				arrValores.Add(obj.h4reflejoizqphc == null ? null : "'" + obj.h4reflejoizqphc + "'");
				arrValores.Add(obj.h4aberturabocaphc == null ? null : "'" + obj.h4aberturabocaphc + "'");
				arrValores.Add(obj.h4movmasticatoriosphc == null ? null : "'" + obj.h4movmasticatoriosphc + "'");
				arrValores.Add(obj.h4refmentonianophc == null ? null : "'" + obj.h4refmentonianophc + "'");
				arrValores.Add(obj.h4facsimetriaphc == null ? null : "'" + obj.h4facsimetriaphc + "'");
				arrValores.Add(obj.h4facmovimientosphc == null ? null : "'" + obj.h4facmovimientosphc + "'");
				arrValores.Add(obj.h4facparalisisphc == null ? null : "'" + obj.h4facparalisisphc + "'");
				arrValores.Add(obj.h4faccentralphc == null ? null : "'" + obj.h4faccentralphc + "'");
				arrValores.Add(obj.h4facperifericaphc == null ? null : "'" + obj.h4facperifericaphc + "'");
				arrValores.Add(obj.h4facgustophc == null ? null : "'" + obj.h4facgustophc + "'");
				arrValores.Add(obj.h5otoscopiaphc == null ? null : "'" + obj.h5otoscopiaphc + "'");
				arrValores.Add(obj.h5aguaudiderphc == null ? null : "'" + obj.h5aguaudiderphc + "'");
				arrValores.Add(obj.h5aguaudiizqphc == null ? null : "'" + obj.h5aguaudiizqphc + "'");
				arrValores.Add(obj.h5weberphc == null ? null : "'" + obj.h5weberphc + "'");
				arrValores.Add(obj.h5rinnephc == null ? null : "'" + obj.h5rinnephc + "'");
				arrValores.Add(obj.h5pruebaslabphc == null ? null : "'" + obj.h5pruebaslabphc + "'");
				arrValores.Add(obj.h5elevpaladarphc == null ? null : "'" + obj.h5elevpaladarphc + "'");
				arrValores.Add(obj.h5uvulaphc == null ? null : "'" + obj.h5uvulaphc + "'");
				arrValores.Add(obj.h5refnauceosophc == null ? null : "'" + obj.h5refnauceosophc + "'");
				arrValores.Add(obj.h5deglucionphc == null ? null : "'" + obj.h5deglucionphc + "'");
				arrValores.Add(obj.h5tonovozphc == null ? null : "'" + obj.h5tonovozphc + "'");
				arrValores.Add(obj.h5esternocleidomastoideophc == null ? null : "'" + obj.h5esternocleidomastoideophc + "'");
				arrValores.Add(obj.h5trapeciophc == null ? null : "'" + obj.h5trapeciophc + "'");
				arrValores.Add(obj.h5desviacionphc == null ? null : "'" + obj.h5desviacionphc + "'");
				arrValores.Add(obj.h5atrofiaphc == null ? null : "'" + obj.h5atrofiaphc + "'");
				arrValores.Add(obj.h5fasciculacionphc == null ? null : "'" + obj.h5fasciculacionphc + "'");
				arrValores.Add(obj.h5fuerzaphc == null ? null : "'" + obj.h5fuerzaphc + "'");
				arrValores.Add(obj.h5marchaphc == null ? null : "'" + obj.h5marchaphc + "'");
				arrValores.Add(obj.h5tonophc == null ? null : "'" + obj.h5tonophc + "'");
				arrValores.Add(obj.h5volumenphc == null ? null : "'" + obj.h5volumenphc + "'");
				arrValores.Add(obj.h5fasciculacionesphc == null ? null : "'" + obj.h5fasciculacionesphc + "'");
				arrValores.Add(obj.h5fuemuscularphc == null ? null : "'" + obj.h5fuemuscularphc + "'");
				arrValores.Add(obj.h5movinvoluntariosphc == null ? null : "'" + obj.h5movinvoluntariosphc + "'");
				arrValores.Add(obj.h5equilibratoriaphc == null ? null : "'" + obj.h5equilibratoriaphc + "'");
				arrValores.Add(obj.h5rombergphc == null ? null : "'" + obj.h5rombergphc + "'");
				arrValores.Add(obj.h5dednarderphc == null ? null : "'" + obj.h5dednarderphc + "'");
				arrValores.Add(obj.h5dednarizqphc == null ? null : "'" + obj.h5dednarizqphc + "'");
				arrValores.Add(obj.h5deddedderphc == null ? null : "'" + obj.h5deddedderphc + "'");
				arrValores.Add(obj.h5deddedizqphc == null ? null : "'" + obj.h5deddedizqphc + "'");
				arrValores.Add(obj.h5talrodderphc == null ? null : "'" + obj.h5talrodderphc + "'");
				arrValores.Add(obj.h5talrodizqphc == null ? null : "'" + obj.h5talrodizqphc + "'");
				arrValores.Add(obj.h5movrapidphc == null ? null : "'" + obj.h5movrapidphc + "'");
				arrValores.Add(obj.h5rebotephc == null ? null : "'" + obj.h5rebotephc + "'");
				arrValores.Add(obj.h5habilidadespecifphc == null ? null : "'" + obj.h5habilidadespecifphc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pachistoriaclinica2 a partir de una clase del tipo epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQueryAll(EntPacHistoriaclinica2 obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.h4olfnormalphc == null ? null : "'" + obj.h4olfnormalphc + "'");
				arrValores.Add(obj.h4olfanosmiaphc == null ? null : "'" + obj.h4olfanosmiaphc + "'");
				arrValores.Add(obj.h4optscodphc == null ? null : "'" + obj.h4optscodphc + "'");
				arrValores.Add(obj.h4optscoiphc == null ? null : "'" + obj.h4optscoiphc + "'");
				arrValores.Add(obj.h4optccodphc == null ? null : "'" + obj.h4optccodphc + "'");
				arrValores.Add(obj.h4optccoiphc == null ? null : "'" + obj.h4optccoiphc + "'");
				arrValores.Add(obj.h4fondoodphc == null ? null : "'" + obj.h4fondoodphc + "'");
				arrValores.Add(obj.h4fondooiphc == null ? null : "'" + obj.h4fondooiphc + "'");
				arrValores.Add(obj.h4campimetriaphc == null ? null : "'" + obj.h4campimetriaphc + "'");
				arrValores.Add(obj.h4prosisphc == null ? null : "'" + obj.h4prosisphc + "'");
				arrValores.Add(obj.h4posicionojosphc == null ? null : "'" + obj.h4posicionojosphc + "'");
				arrValores.Add(obj.h4exoftalmiaphc == null ? null : "'" + obj.h4exoftalmiaphc + "'");
				arrValores.Add(obj.h4hornerphc == null ? null : "'" + obj.h4hornerphc + "'");
				arrValores.Add(obj.h4movocularesphc == null ? null : "'" + obj.h4movocularesphc + "'");
				arrValores.Add(obj.h4nistagmuxphc == null ? null : "'" + obj.h4nistagmuxphc + "'");
				arrValores.Add(obj.h4diplopia1phc == null ? null : "'" + obj.h4diplopia1phc + "'");
				arrValores.Add(obj.h4diplopia2phc == null ? null : "'" + obj.h4diplopia2phc + "'");
				arrValores.Add(obj.h4diplopia3phc == null ? null : "'" + obj.h4diplopia3phc + "'");
				arrValores.Add(obj.h4diplopia4phc == null ? null : "'" + obj.h4diplopia4phc + "'");
				arrValores.Add(obj.h4diplopia5phc == null ? null : "'" + obj.h4diplopia5phc + "'");
				arrValores.Add(obj.h4diplopia6phc == null ? null : "'" + obj.h4diplopia6phc + "'");
				arrValores.Add(obj.h4diplopia7phc == null ? null : "'" + obj.h4diplopia7phc + "'");
				arrValores.Add(obj.h4diplopia8phc == null ? null : "'" + obj.h4diplopia8phc + "'");
				arrValores.Add(obj.h4diplopia9phc == null ? null : "'" + obj.h4diplopia9phc + "'");
				arrValores.Add(obj.h4convergenciaphc == null ? null : "'" + obj.h4convergenciaphc + "'");
				arrValores.Add(obj.h4acomodacionodphc == null ? null : "'" + obj.h4acomodacionodphc + "'");
				arrValores.Add(obj.h4acomodacionoiphc == null ? null : "'" + obj.h4acomodacionoiphc + "'");
				arrValores.Add(obj.h4pupulasodphc == null ? null : "'" + obj.h4pupulasodphc + "'");
				arrValores.Add(obj.h4pupulasoiphc == null ? null : "'" + obj.h4pupulasoiphc + "'");
				arrValores.Add(obj.h4formaphc == null ? null : "'" + obj.h4formaphc + "'");
				arrValores.Add(obj.h4fotomotorodphc == null ? null : "'" + obj.h4fotomotorodphc + "'");
				arrValores.Add(obj.h4fotomotoroiphc == null ? null : "'" + obj.h4fotomotoroiphc + "'");
				arrValores.Add(obj.h4consensualdaiphc == null ? null : "'" + obj.h4consensualdaiphc + "'");
				arrValores.Add(obj.h4consensualiadphc == null ? null : "'" + obj.h4consensualiadphc + "'");
				arrValores.Add(obj.h4senfacialderphc == null ? null : "'" + obj.h4senfacialderphc + "'");
				arrValores.Add(obj.h4senfacializqphc == null ? null : "'" + obj.h4senfacializqphc + "'");
				arrValores.Add(obj.h4reflejoderphc == null ? null : "'" + obj.h4reflejoderphc + "'");
				arrValores.Add(obj.h4reflejoizqphc == null ? null : "'" + obj.h4reflejoizqphc + "'");
				arrValores.Add(obj.h4aberturabocaphc == null ? null : "'" + obj.h4aberturabocaphc + "'");
				arrValores.Add(obj.h4movmasticatoriosphc == null ? null : "'" + obj.h4movmasticatoriosphc + "'");
				arrValores.Add(obj.h4refmentonianophc == null ? null : "'" + obj.h4refmentonianophc + "'");
				arrValores.Add(obj.h4facsimetriaphc == null ? null : "'" + obj.h4facsimetriaphc + "'");
				arrValores.Add(obj.h4facmovimientosphc == null ? null : "'" + obj.h4facmovimientosphc + "'");
				arrValores.Add(obj.h4facparalisisphc == null ? null : "'" + obj.h4facparalisisphc + "'");
				arrValores.Add(obj.h4faccentralphc == null ? null : "'" + obj.h4faccentralphc + "'");
				arrValores.Add(obj.h4facperifericaphc == null ? null : "'" + obj.h4facperifericaphc + "'");
				arrValores.Add(obj.h4facgustophc == null ? null : "'" + obj.h4facgustophc + "'");
				arrValores.Add(obj.h5otoscopiaphc == null ? null : "'" + obj.h5otoscopiaphc + "'");
				arrValores.Add(obj.h5aguaudiderphc == null ? null : "'" + obj.h5aguaudiderphc + "'");
				arrValores.Add(obj.h5aguaudiizqphc == null ? null : "'" + obj.h5aguaudiizqphc + "'");
				arrValores.Add(obj.h5weberphc == null ? null : "'" + obj.h5weberphc + "'");
				arrValores.Add(obj.h5rinnephc == null ? null : "'" + obj.h5rinnephc + "'");
				arrValores.Add(obj.h5pruebaslabphc == null ? null : "'" + obj.h5pruebaslabphc + "'");
				arrValores.Add(obj.h5elevpaladarphc == null ? null : "'" + obj.h5elevpaladarphc + "'");
				arrValores.Add(obj.h5uvulaphc == null ? null : "'" + obj.h5uvulaphc + "'");
				arrValores.Add(obj.h5refnauceosophc == null ? null : "'" + obj.h5refnauceosophc + "'");
				arrValores.Add(obj.h5deglucionphc == null ? null : "'" + obj.h5deglucionphc + "'");
				arrValores.Add(obj.h5tonovozphc == null ? null : "'" + obj.h5tonovozphc + "'");
				arrValores.Add(obj.h5esternocleidomastoideophc == null ? null : "'" + obj.h5esternocleidomastoideophc + "'");
				arrValores.Add(obj.h5trapeciophc == null ? null : "'" + obj.h5trapeciophc + "'");
				arrValores.Add(obj.h5desviacionphc == null ? null : "'" + obj.h5desviacionphc + "'");
				arrValores.Add(obj.h5atrofiaphc == null ? null : "'" + obj.h5atrofiaphc + "'");
				arrValores.Add(obj.h5fasciculacionphc == null ? null : "'" + obj.h5fasciculacionphc + "'");
				arrValores.Add(obj.h5fuerzaphc == null ? null : "'" + obj.h5fuerzaphc + "'");
				arrValores.Add(obj.h5marchaphc == null ? null : "'" + obj.h5marchaphc + "'");
				arrValores.Add(obj.h5tonophc == null ? null : "'" + obj.h5tonophc + "'");
				arrValores.Add(obj.h5volumenphc == null ? null : "'" + obj.h5volumenphc + "'");
				arrValores.Add(obj.h5fasciculacionesphc == null ? null : "'" + obj.h5fasciculacionesphc + "'");
				arrValores.Add(obj.h5fuemuscularphc == null ? null : "'" + obj.h5fuemuscularphc + "'");
				arrValores.Add(obj.h5movinvoluntariosphc == null ? null : "'" + obj.h5movinvoluntariosphc + "'");
				arrValores.Add(obj.h5equilibratoriaphc == null ? null : "'" + obj.h5equilibratoriaphc + "'");
				arrValores.Add(obj.h5rombergphc == null ? null : "'" + obj.h5rombergphc + "'");
				arrValores.Add(obj.h5dednarderphc == null ? null : "'" + obj.h5dednarderphc + "'");
				arrValores.Add(obj.h5dednarizqphc == null ? null : "'" + obj.h5dednarizqphc + "'");
				arrValores.Add(obj.h5deddedderphc == null ? null : "'" + obj.h5deddedderphc + "'");
				arrValores.Add(obj.h5deddedizqphc == null ? null : "'" + obj.h5deddedizqphc + "'");
				arrValores.Add(obj.h5talrodderphc == null ? null : "'" + obj.h5talrodderphc + "'");
				arrValores.Add(obj.h5talrodizqphc == null ? null : "'" + obj.h5talrodizqphc + "'");
				arrValores.Add(obj.h5movrapidphc == null ? null : "'" + obj.h5movrapidphc + "'");
				arrValores.Add(obj.h5rebotephc == null ? null : "'" + obj.h5rebotephc + "'");
				arrValores.Add(obj.h5habilidadespecifphc == null ? null : "'" + obj.h5habilidadespecifphc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pachistoriaclinica2 a partir de una clase del tipo Epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpachistoriaclinica2
		/// </returns>
		public int UpdateQuery(EntPacHistoriaclinica2 obj)
		{
			try
			{
				//Obtenemos el Objeto original
				EntPacHistoriaclinica2 objOriginal = this.ObtenerObjeto(obj.idppa);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.h4olfnormalphc != objOriginal.h4olfnormalphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
					arrValores.Add(obj.h4olfnormalphc == null ? null : "'" + obj.h4olfnormalphc + "'");
				}
				if(obj.h4olfanosmiaphc != objOriginal.h4olfanosmiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
					arrValores.Add(obj.h4olfanosmiaphc == null ? null : "'" + obj.h4olfanosmiaphc + "'");
				}
				if(obj.h4optscodphc != objOriginal.h4optscodphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
					arrValores.Add(obj.h4optscodphc == null ? null : "'" + obj.h4optscodphc + "'");
				}
				if(obj.h4optscoiphc != objOriginal.h4optscoiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
					arrValores.Add(obj.h4optscoiphc == null ? null : "'" + obj.h4optscoiphc + "'");
				}
				if(obj.h4optccodphc != objOriginal.h4optccodphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
					arrValores.Add(obj.h4optccodphc == null ? null : "'" + obj.h4optccodphc + "'");
				}
				if(obj.h4optccoiphc != objOriginal.h4optccoiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
					arrValores.Add(obj.h4optccoiphc == null ? null : "'" + obj.h4optccoiphc + "'");
				}
				if(obj.h4fondoodphc != objOriginal.h4fondoodphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
					arrValores.Add(obj.h4fondoodphc == null ? null : "'" + obj.h4fondoodphc + "'");
				}
				if(obj.h4fondooiphc != objOriginal.h4fondooiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
					arrValores.Add(obj.h4fondooiphc == null ? null : "'" + obj.h4fondooiphc + "'");
				}
				if(obj.h4campimetriaphc != objOriginal.h4campimetriaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
					arrValores.Add(obj.h4campimetriaphc == null ? null : "'" + obj.h4campimetriaphc + "'");
				}
				if(obj.h4prosisphc != objOriginal.h4prosisphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
					arrValores.Add(obj.h4prosisphc == null ? null : "'" + obj.h4prosisphc + "'");
				}
				if(obj.h4posicionojosphc != objOriginal.h4posicionojosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
					arrValores.Add(obj.h4posicionojosphc == null ? null : "'" + obj.h4posicionojosphc + "'");
				}
				if(obj.h4exoftalmiaphc != objOriginal.h4exoftalmiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
					arrValores.Add(obj.h4exoftalmiaphc == null ? null : "'" + obj.h4exoftalmiaphc + "'");
				}
				if(obj.h4hornerphc != objOriginal.h4hornerphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
					arrValores.Add(obj.h4hornerphc == null ? null : "'" + obj.h4hornerphc + "'");
				}
				if(obj.h4movocularesphc != objOriginal.h4movocularesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
					arrValores.Add(obj.h4movocularesphc == null ? null : "'" + obj.h4movocularesphc + "'");
				}
				if(obj.h4nistagmuxphc != objOriginal.h4nistagmuxphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
					arrValores.Add(obj.h4nistagmuxphc == null ? null : "'" + obj.h4nistagmuxphc + "'");
				}
				if(obj.h4diplopia1phc != objOriginal.h4diplopia1phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
					arrValores.Add(obj.h4diplopia1phc == null ? null : "'" + obj.h4diplopia1phc + "'");
				}
				if(obj.h4diplopia2phc != objOriginal.h4diplopia2phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
					arrValores.Add(obj.h4diplopia2phc == null ? null : "'" + obj.h4diplopia2phc + "'");
				}
				if(obj.h4diplopia3phc != objOriginal.h4diplopia3phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
					arrValores.Add(obj.h4diplopia3phc == null ? null : "'" + obj.h4diplopia3phc + "'");
				}
				if(obj.h4diplopia4phc != objOriginal.h4diplopia4phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
					arrValores.Add(obj.h4diplopia4phc == null ? null : "'" + obj.h4diplopia4phc + "'");
				}
				if(obj.h4diplopia5phc != objOriginal.h4diplopia5phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
					arrValores.Add(obj.h4diplopia5phc == null ? null : "'" + obj.h4diplopia5phc + "'");
				}
				if(obj.h4diplopia6phc != objOriginal.h4diplopia6phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
					arrValores.Add(obj.h4diplopia6phc == null ? null : "'" + obj.h4diplopia6phc + "'");
				}
				if(obj.h4diplopia7phc != objOriginal.h4diplopia7phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
					arrValores.Add(obj.h4diplopia7phc == null ? null : "'" + obj.h4diplopia7phc + "'");
				}
				if(obj.h4diplopia8phc != objOriginal.h4diplopia8phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
					arrValores.Add(obj.h4diplopia8phc == null ? null : "'" + obj.h4diplopia8phc + "'");
				}
				if(obj.h4diplopia9phc != objOriginal.h4diplopia9phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
					arrValores.Add(obj.h4diplopia9phc == null ? null : "'" + obj.h4diplopia9phc + "'");
				}
				if(obj.h4convergenciaphc != objOriginal.h4convergenciaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
					arrValores.Add(obj.h4convergenciaphc == null ? null : "'" + obj.h4convergenciaphc + "'");
				}
				if(obj.h4acomodacionodphc != objOriginal.h4acomodacionodphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
					arrValores.Add(obj.h4acomodacionodphc == null ? null : "'" + obj.h4acomodacionodphc + "'");
				}
				if(obj.h4acomodacionoiphc != objOriginal.h4acomodacionoiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
					arrValores.Add(obj.h4acomodacionoiphc == null ? null : "'" + obj.h4acomodacionoiphc + "'");
				}
				if(obj.h4pupulasodphc != objOriginal.h4pupulasodphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
					arrValores.Add(obj.h4pupulasodphc == null ? null : "'" + obj.h4pupulasodphc + "'");
				}
				if(obj.h4pupulasoiphc != objOriginal.h4pupulasoiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
					arrValores.Add(obj.h4pupulasoiphc == null ? null : "'" + obj.h4pupulasoiphc + "'");
				}
				if(obj.h4formaphc != objOriginal.h4formaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
					arrValores.Add(obj.h4formaphc == null ? null : "'" + obj.h4formaphc + "'");
				}
				if(obj.h4fotomotorodphc != objOriginal.h4fotomotorodphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
					arrValores.Add(obj.h4fotomotorodphc == null ? null : "'" + obj.h4fotomotorodphc + "'");
				}
				if(obj.h4fotomotoroiphc != objOriginal.h4fotomotoroiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
					arrValores.Add(obj.h4fotomotoroiphc == null ? null : "'" + obj.h4fotomotoroiphc + "'");
				}
				if(obj.h4consensualdaiphc != objOriginal.h4consensualdaiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
					arrValores.Add(obj.h4consensualdaiphc == null ? null : "'" + obj.h4consensualdaiphc + "'");
				}
				if(obj.h4consensualiadphc != objOriginal.h4consensualiadphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
					arrValores.Add(obj.h4consensualiadphc == null ? null : "'" + obj.h4consensualiadphc + "'");
				}
				if(obj.h4senfacialderphc != objOriginal.h4senfacialderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
					arrValores.Add(obj.h4senfacialderphc == null ? null : "'" + obj.h4senfacialderphc + "'");
				}
				if(obj.h4senfacializqphc != objOriginal.h4senfacializqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
					arrValores.Add(obj.h4senfacializqphc == null ? null : "'" + obj.h4senfacializqphc + "'");
				}
				if(obj.h4reflejoderphc != objOriginal.h4reflejoderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
					arrValores.Add(obj.h4reflejoderphc == null ? null : "'" + obj.h4reflejoderphc + "'");
				}
				if(obj.h4reflejoizqphc != objOriginal.h4reflejoizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
					arrValores.Add(obj.h4reflejoizqphc == null ? null : "'" + obj.h4reflejoizqphc + "'");
				}
				if(obj.h4aberturabocaphc != objOriginal.h4aberturabocaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
					arrValores.Add(obj.h4aberturabocaphc == null ? null : "'" + obj.h4aberturabocaphc + "'");
				}
				if(obj.h4movmasticatoriosphc != objOriginal.h4movmasticatoriosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
					arrValores.Add(obj.h4movmasticatoriosphc == null ? null : "'" + obj.h4movmasticatoriosphc + "'");
				}
				if(obj.h4refmentonianophc != objOriginal.h4refmentonianophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
					arrValores.Add(obj.h4refmentonianophc == null ? null : "'" + obj.h4refmentonianophc + "'");
				}
				if(obj.h4facsimetriaphc != objOriginal.h4facsimetriaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
					arrValores.Add(obj.h4facsimetriaphc == null ? null : "'" + obj.h4facsimetriaphc + "'");
				}
				if(obj.h4facmovimientosphc != objOriginal.h4facmovimientosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
					arrValores.Add(obj.h4facmovimientosphc == null ? null : "'" + obj.h4facmovimientosphc + "'");
				}
				if(obj.h4facparalisisphc != objOriginal.h4facparalisisphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
					arrValores.Add(obj.h4facparalisisphc == null ? null : "'" + obj.h4facparalisisphc + "'");
				}
				if(obj.h4faccentralphc != objOriginal.h4faccentralphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
					arrValores.Add(obj.h4faccentralphc == null ? null : "'" + obj.h4faccentralphc + "'");
				}
				if(obj.h4facperifericaphc != objOriginal.h4facperifericaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
					arrValores.Add(obj.h4facperifericaphc == null ? null : "'" + obj.h4facperifericaphc + "'");
				}
				if(obj.h4facgustophc != objOriginal.h4facgustophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
					arrValores.Add(obj.h4facgustophc == null ? null : "'" + obj.h4facgustophc + "'");
				}
				if(obj.h5otoscopiaphc != objOriginal.h5otoscopiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
					arrValores.Add(obj.h5otoscopiaphc == null ? null : "'" + obj.h5otoscopiaphc + "'");
				}
				if(obj.h5aguaudiderphc != objOriginal.h5aguaudiderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
					arrValores.Add(obj.h5aguaudiderphc == null ? null : "'" + obj.h5aguaudiderphc + "'");
				}
				if(obj.h5aguaudiizqphc != objOriginal.h5aguaudiizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
					arrValores.Add(obj.h5aguaudiizqphc == null ? null : "'" + obj.h5aguaudiizqphc + "'");
				}
				if(obj.h5weberphc != objOriginal.h5weberphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
					arrValores.Add(obj.h5weberphc == null ? null : "'" + obj.h5weberphc + "'");
				}
				if(obj.h5rinnephc != objOriginal.h5rinnephc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
					arrValores.Add(obj.h5rinnephc == null ? null : "'" + obj.h5rinnephc + "'");
				}
				if(obj.h5pruebaslabphc != objOriginal.h5pruebaslabphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
					arrValores.Add(obj.h5pruebaslabphc == null ? null : "'" + obj.h5pruebaslabphc + "'");
				}
				if(obj.h5elevpaladarphc != objOriginal.h5elevpaladarphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
					arrValores.Add(obj.h5elevpaladarphc == null ? null : "'" + obj.h5elevpaladarphc + "'");
				}
				if(obj.h5uvulaphc != objOriginal.h5uvulaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
					arrValores.Add(obj.h5uvulaphc == null ? null : "'" + obj.h5uvulaphc + "'");
				}
				if(obj.h5refnauceosophc != objOriginal.h5refnauceosophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
					arrValores.Add(obj.h5refnauceosophc == null ? null : "'" + obj.h5refnauceosophc + "'");
				}
				if(obj.h5deglucionphc != objOriginal.h5deglucionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
					arrValores.Add(obj.h5deglucionphc == null ? null : "'" + obj.h5deglucionphc + "'");
				}
				if(obj.h5tonovozphc != objOriginal.h5tonovozphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
					arrValores.Add(obj.h5tonovozphc == null ? null : "'" + obj.h5tonovozphc + "'");
				}
				if(obj.h5esternocleidomastoideophc != objOriginal.h5esternocleidomastoideophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
					arrValores.Add(obj.h5esternocleidomastoideophc == null ? null : "'" + obj.h5esternocleidomastoideophc + "'");
				}
				if(obj.h5trapeciophc != objOriginal.h5trapeciophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
					arrValores.Add(obj.h5trapeciophc == null ? null : "'" + obj.h5trapeciophc + "'");
				}
				if(obj.h5desviacionphc != objOriginal.h5desviacionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
					arrValores.Add(obj.h5desviacionphc == null ? null : "'" + obj.h5desviacionphc + "'");
				}
				if(obj.h5atrofiaphc != objOriginal.h5atrofiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
					arrValores.Add(obj.h5atrofiaphc == null ? null : "'" + obj.h5atrofiaphc + "'");
				}
				if(obj.h5fasciculacionphc != objOriginal.h5fasciculacionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
					arrValores.Add(obj.h5fasciculacionphc == null ? null : "'" + obj.h5fasciculacionphc + "'");
				}
				if(obj.h5fuerzaphc != objOriginal.h5fuerzaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
					arrValores.Add(obj.h5fuerzaphc == null ? null : "'" + obj.h5fuerzaphc + "'");
				}
				if(obj.h5marchaphc != objOriginal.h5marchaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
					arrValores.Add(obj.h5marchaphc == null ? null : "'" + obj.h5marchaphc + "'");
				}
				if(obj.h5tonophc != objOriginal.h5tonophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
					arrValores.Add(obj.h5tonophc == null ? null : "'" + obj.h5tonophc + "'");
				}
				if(obj.h5volumenphc != objOriginal.h5volumenphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
					arrValores.Add(obj.h5volumenphc == null ? null : "'" + obj.h5volumenphc + "'");
				}
				if(obj.h5fasciculacionesphc != objOriginal.h5fasciculacionesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
					arrValores.Add(obj.h5fasciculacionesphc == null ? null : "'" + obj.h5fasciculacionesphc + "'");
				}
				if(obj.h5fuemuscularphc != objOriginal.h5fuemuscularphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
					arrValores.Add(obj.h5fuemuscularphc == null ? null : "'" + obj.h5fuemuscularphc + "'");
				}
				if(obj.h5movinvoluntariosphc != objOriginal.h5movinvoluntariosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
					arrValores.Add(obj.h5movinvoluntariosphc == null ? null : "'" + obj.h5movinvoluntariosphc + "'");
				}
				if(obj.h5equilibratoriaphc != objOriginal.h5equilibratoriaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
					arrValores.Add(obj.h5equilibratoriaphc == null ? null : "'" + obj.h5equilibratoriaphc + "'");
				}
				if(obj.h5rombergphc != objOriginal.h5rombergphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
					arrValores.Add(obj.h5rombergphc == null ? null : "'" + obj.h5rombergphc + "'");
				}
				if(obj.h5dednarderphc != objOriginal.h5dednarderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
					arrValores.Add(obj.h5dednarderphc == null ? null : "'" + obj.h5dednarderphc + "'");
				}
				if(obj.h5dednarizqphc != objOriginal.h5dednarizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
					arrValores.Add(obj.h5dednarizqphc == null ? null : "'" + obj.h5dednarizqphc + "'");
				}
				if(obj.h5deddedderphc != objOriginal.h5deddedderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
					arrValores.Add(obj.h5deddedderphc == null ? null : "'" + obj.h5deddedderphc + "'");
				}
				if(obj.h5deddedizqphc != objOriginal.h5deddedizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
					arrValores.Add(obj.h5deddedizqphc == null ? null : "'" + obj.h5deddedizqphc + "'");
				}
				if(obj.h5talrodderphc != objOriginal.h5talrodderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
					arrValores.Add(obj.h5talrodderphc == null ? null : "'" + obj.h5talrodderphc + "'");
				}
				if(obj.h5talrodizqphc != objOriginal.h5talrodizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
					arrValores.Add(obj.h5talrodizqphc == null ? null : "'" + obj.h5talrodizqphc + "'");
				}
				if(obj.h5movrapidphc != objOriginal.h5movrapidphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
					arrValores.Add(obj.h5movrapidphc == null ? null : "'" + obj.h5movrapidphc + "'");
				}
				if(obj.h5rebotephc != objOriginal.h5rebotephc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
					arrValores.Add(obj.h5rebotephc == null ? null : "'" + obj.h5rebotephc + "'");
				}
				if(obj.h5habilidadespecifphc != objOriginal.h5habilidadespecifphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
					arrValores.Add(obj.h5habilidadespecifphc == null ? null : "'" + obj.h5habilidadespecifphc + "'");
				}
				if(obj.apiestado != objOriginal.apiestado )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
					arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				}
				if(obj.apitransaccion != objOriginal.apitransaccion )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
					arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				}
				if(obj.usumod != objOriginal.usumod )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
					arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pachistoriaclinica2 a partir de una clase del tipo epachistoriaclinica2
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQuery(EntPacHistoriaclinica2 obj, ref CTrans localTrans)
		{
			try
			{
				//Obtenemos el Objeto original
				EntPacHistoriaclinica2 objOriginal = this.ObtenerObjeto(obj.idppa, ref localTrans);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.h4olfnormalphc != objOriginal.h4olfnormalphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
					arrValores.Add(obj.h4olfnormalphc == null ? null : "'" + obj.h4olfnormalphc + "'");
				}
				if(obj.h4olfanosmiaphc != objOriginal.h4olfanosmiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
					arrValores.Add(obj.h4olfanosmiaphc == null ? null : "'" + obj.h4olfanosmiaphc + "'");
				}
				if(obj.h4optscodphc != objOriginal.h4optscodphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
					arrValores.Add(obj.h4optscodphc == null ? null : "'" + obj.h4optscodphc + "'");
				}
				if(obj.h4optscoiphc != objOriginal.h4optscoiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
					arrValores.Add(obj.h4optscoiphc == null ? null : "'" + obj.h4optscoiphc + "'");
				}
				if(obj.h4optccodphc != objOriginal.h4optccodphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
					arrValores.Add(obj.h4optccodphc == null ? null : "'" + obj.h4optccodphc + "'");
				}
				if(obj.h4optccoiphc != objOriginal.h4optccoiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
					arrValores.Add(obj.h4optccoiphc == null ? null : "'" + obj.h4optccoiphc + "'");
				}
				if(obj.h4fondoodphc != objOriginal.h4fondoodphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
					arrValores.Add(obj.h4fondoodphc == null ? null : "'" + obj.h4fondoodphc + "'");
				}
				if(obj.h4fondooiphc != objOriginal.h4fondooiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
					arrValores.Add(obj.h4fondooiphc == null ? null : "'" + obj.h4fondooiphc + "'");
				}
				if(obj.h4campimetriaphc != objOriginal.h4campimetriaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
					arrValores.Add(obj.h4campimetriaphc == null ? null : "'" + obj.h4campimetriaphc + "'");
				}
				if(obj.h4prosisphc != objOriginal.h4prosisphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
					arrValores.Add(obj.h4prosisphc == null ? null : "'" + obj.h4prosisphc + "'");
				}
				if(obj.h4posicionojosphc != objOriginal.h4posicionojosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
					arrValores.Add(obj.h4posicionojosphc == null ? null : "'" + obj.h4posicionojosphc + "'");
				}
				if(obj.h4exoftalmiaphc != objOriginal.h4exoftalmiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
					arrValores.Add(obj.h4exoftalmiaphc == null ? null : "'" + obj.h4exoftalmiaphc + "'");
				}
				if(obj.h4hornerphc != objOriginal.h4hornerphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
					arrValores.Add(obj.h4hornerphc == null ? null : "'" + obj.h4hornerphc + "'");
				}
				if(obj.h4movocularesphc != objOriginal.h4movocularesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
					arrValores.Add(obj.h4movocularesphc == null ? null : "'" + obj.h4movocularesphc + "'");
				}
				if(obj.h4nistagmuxphc != objOriginal.h4nistagmuxphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
					arrValores.Add(obj.h4nistagmuxphc == null ? null : "'" + obj.h4nistagmuxphc + "'");
				}
				if(obj.h4diplopia1phc != objOriginal.h4diplopia1phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
					arrValores.Add(obj.h4diplopia1phc == null ? null : "'" + obj.h4diplopia1phc + "'");
				}
				if(obj.h4diplopia2phc != objOriginal.h4diplopia2phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
					arrValores.Add(obj.h4diplopia2phc == null ? null : "'" + obj.h4diplopia2phc + "'");
				}
				if(obj.h4diplopia3phc != objOriginal.h4diplopia3phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
					arrValores.Add(obj.h4diplopia3phc == null ? null : "'" + obj.h4diplopia3phc + "'");
				}
				if(obj.h4diplopia4phc != objOriginal.h4diplopia4phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
					arrValores.Add(obj.h4diplopia4phc == null ? null : "'" + obj.h4diplopia4phc + "'");
				}
				if(obj.h4diplopia5phc != objOriginal.h4diplopia5phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
					arrValores.Add(obj.h4diplopia5phc == null ? null : "'" + obj.h4diplopia5phc + "'");
				}
				if(obj.h4diplopia6phc != objOriginal.h4diplopia6phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
					arrValores.Add(obj.h4diplopia6phc == null ? null : "'" + obj.h4diplopia6phc + "'");
				}
				if(obj.h4diplopia7phc != objOriginal.h4diplopia7phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
					arrValores.Add(obj.h4diplopia7phc == null ? null : "'" + obj.h4diplopia7phc + "'");
				}
				if(obj.h4diplopia8phc != objOriginal.h4diplopia8phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
					arrValores.Add(obj.h4diplopia8phc == null ? null : "'" + obj.h4diplopia8phc + "'");
				}
				if(obj.h4diplopia9phc != objOriginal.h4diplopia9phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
					arrValores.Add(obj.h4diplopia9phc == null ? null : "'" + obj.h4diplopia9phc + "'");
				}
				if(obj.h4convergenciaphc != objOriginal.h4convergenciaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
					arrValores.Add(obj.h4convergenciaphc == null ? null : "'" + obj.h4convergenciaphc + "'");
				}
				if(obj.h4acomodacionodphc != objOriginal.h4acomodacionodphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
					arrValores.Add(obj.h4acomodacionodphc == null ? null : "'" + obj.h4acomodacionodphc + "'");
				}
				if(obj.h4acomodacionoiphc != objOriginal.h4acomodacionoiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
					arrValores.Add(obj.h4acomodacionoiphc == null ? null : "'" + obj.h4acomodacionoiphc + "'");
				}
				if(obj.h4pupulasodphc != objOriginal.h4pupulasodphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
					arrValores.Add(obj.h4pupulasodphc == null ? null : "'" + obj.h4pupulasodphc + "'");
				}
				if(obj.h4pupulasoiphc != objOriginal.h4pupulasoiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
					arrValores.Add(obj.h4pupulasoiphc == null ? null : "'" + obj.h4pupulasoiphc + "'");
				}
				if(obj.h4formaphc != objOriginal.h4formaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
					arrValores.Add(obj.h4formaphc == null ? null : "'" + obj.h4formaphc + "'");
				}
				if(obj.h4fotomotorodphc != objOriginal.h4fotomotorodphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
					arrValores.Add(obj.h4fotomotorodphc == null ? null : "'" + obj.h4fotomotorodphc + "'");
				}
				if(obj.h4fotomotoroiphc != objOriginal.h4fotomotoroiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
					arrValores.Add(obj.h4fotomotoroiphc == null ? null : "'" + obj.h4fotomotoroiphc + "'");
				}
				if(obj.h4consensualdaiphc != objOriginal.h4consensualdaiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
					arrValores.Add(obj.h4consensualdaiphc == null ? null : "'" + obj.h4consensualdaiphc + "'");
				}
				if(obj.h4consensualiadphc != objOriginal.h4consensualiadphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
					arrValores.Add(obj.h4consensualiadphc == null ? null : "'" + obj.h4consensualiadphc + "'");
				}
				if(obj.h4senfacialderphc != objOriginal.h4senfacialderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
					arrValores.Add(obj.h4senfacialderphc == null ? null : "'" + obj.h4senfacialderphc + "'");
				}
				if(obj.h4senfacializqphc != objOriginal.h4senfacializqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
					arrValores.Add(obj.h4senfacializqphc == null ? null : "'" + obj.h4senfacializqphc + "'");
				}
				if(obj.h4reflejoderphc != objOriginal.h4reflejoderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
					arrValores.Add(obj.h4reflejoderphc == null ? null : "'" + obj.h4reflejoderphc + "'");
				}
				if(obj.h4reflejoizqphc != objOriginal.h4reflejoizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
					arrValores.Add(obj.h4reflejoizqphc == null ? null : "'" + obj.h4reflejoizqphc + "'");
				}
				if(obj.h4aberturabocaphc != objOriginal.h4aberturabocaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
					arrValores.Add(obj.h4aberturabocaphc == null ? null : "'" + obj.h4aberturabocaphc + "'");
				}
				if(obj.h4movmasticatoriosphc != objOriginal.h4movmasticatoriosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
					arrValores.Add(obj.h4movmasticatoriosphc == null ? null : "'" + obj.h4movmasticatoriosphc + "'");
				}
				if(obj.h4refmentonianophc != objOriginal.h4refmentonianophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
					arrValores.Add(obj.h4refmentonianophc == null ? null : "'" + obj.h4refmentonianophc + "'");
				}
				if(obj.h4facsimetriaphc != objOriginal.h4facsimetriaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
					arrValores.Add(obj.h4facsimetriaphc == null ? null : "'" + obj.h4facsimetriaphc + "'");
				}
				if(obj.h4facmovimientosphc != objOriginal.h4facmovimientosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
					arrValores.Add(obj.h4facmovimientosphc == null ? null : "'" + obj.h4facmovimientosphc + "'");
				}
				if(obj.h4facparalisisphc != objOriginal.h4facparalisisphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
					arrValores.Add(obj.h4facparalisisphc == null ? null : "'" + obj.h4facparalisisphc + "'");
				}
				if(obj.h4faccentralphc != objOriginal.h4faccentralphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
					arrValores.Add(obj.h4faccentralphc == null ? null : "'" + obj.h4faccentralphc + "'");
				}
				if(obj.h4facperifericaphc != objOriginal.h4facperifericaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
					arrValores.Add(obj.h4facperifericaphc == null ? null : "'" + obj.h4facperifericaphc + "'");
				}
				if(obj.h4facgustophc != objOriginal.h4facgustophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
					arrValores.Add(obj.h4facgustophc == null ? null : "'" + obj.h4facgustophc + "'");
				}
				if(obj.h5otoscopiaphc != objOriginal.h5otoscopiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
					arrValores.Add(obj.h5otoscopiaphc == null ? null : "'" + obj.h5otoscopiaphc + "'");
				}
				if(obj.h5aguaudiderphc != objOriginal.h5aguaudiderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
					arrValores.Add(obj.h5aguaudiderphc == null ? null : "'" + obj.h5aguaudiderphc + "'");
				}
				if(obj.h5aguaudiizqphc != objOriginal.h5aguaudiizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
					arrValores.Add(obj.h5aguaudiizqphc == null ? null : "'" + obj.h5aguaudiizqphc + "'");
				}
				if(obj.h5weberphc != objOriginal.h5weberphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
					arrValores.Add(obj.h5weberphc == null ? null : "'" + obj.h5weberphc + "'");
				}
				if(obj.h5rinnephc != objOriginal.h5rinnephc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
					arrValores.Add(obj.h5rinnephc == null ? null : "'" + obj.h5rinnephc + "'");
				}
				if(obj.h5pruebaslabphc != objOriginal.h5pruebaslabphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
					arrValores.Add(obj.h5pruebaslabphc == null ? null : "'" + obj.h5pruebaslabphc + "'");
				}
				if(obj.h5elevpaladarphc != objOriginal.h5elevpaladarphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
					arrValores.Add(obj.h5elevpaladarphc == null ? null : "'" + obj.h5elevpaladarphc + "'");
				}
				if(obj.h5uvulaphc != objOriginal.h5uvulaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
					arrValores.Add(obj.h5uvulaphc == null ? null : "'" + obj.h5uvulaphc + "'");
				}
				if(obj.h5refnauceosophc != objOriginal.h5refnauceosophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
					arrValores.Add(obj.h5refnauceosophc == null ? null : "'" + obj.h5refnauceosophc + "'");
				}
				if(obj.h5deglucionphc != objOriginal.h5deglucionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
					arrValores.Add(obj.h5deglucionphc == null ? null : "'" + obj.h5deglucionphc + "'");
				}
				if(obj.h5tonovozphc != objOriginal.h5tonovozphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
					arrValores.Add(obj.h5tonovozphc == null ? null : "'" + obj.h5tonovozphc + "'");
				}
				if(obj.h5esternocleidomastoideophc != objOriginal.h5esternocleidomastoideophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
					arrValores.Add(obj.h5esternocleidomastoideophc == null ? null : "'" + obj.h5esternocleidomastoideophc + "'");
				}
				if(obj.h5trapeciophc != objOriginal.h5trapeciophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
					arrValores.Add(obj.h5trapeciophc == null ? null : "'" + obj.h5trapeciophc + "'");
				}
				if(obj.h5desviacionphc != objOriginal.h5desviacionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
					arrValores.Add(obj.h5desviacionphc == null ? null : "'" + obj.h5desviacionphc + "'");
				}
				if(obj.h5atrofiaphc != objOriginal.h5atrofiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
					arrValores.Add(obj.h5atrofiaphc == null ? null : "'" + obj.h5atrofiaphc + "'");
				}
				if(obj.h5fasciculacionphc != objOriginal.h5fasciculacionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
					arrValores.Add(obj.h5fasciculacionphc == null ? null : "'" + obj.h5fasciculacionphc + "'");
				}
				if(obj.h5fuerzaphc != objOriginal.h5fuerzaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
					arrValores.Add(obj.h5fuerzaphc == null ? null : "'" + obj.h5fuerzaphc + "'");
				}
				if(obj.h5marchaphc != objOriginal.h5marchaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
					arrValores.Add(obj.h5marchaphc == null ? null : "'" + obj.h5marchaphc + "'");
				}
				if(obj.h5tonophc != objOriginal.h5tonophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
					arrValores.Add(obj.h5tonophc == null ? null : "'" + obj.h5tonophc + "'");
				}
				if(obj.h5volumenphc != objOriginal.h5volumenphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
					arrValores.Add(obj.h5volumenphc == null ? null : "'" + obj.h5volumenphc + "'");
				}
				if(obj.h5fasciculacionesphc != objOriginal.h5fasciculacionesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
					arrValores.Add(obj.h5fasciculacionesphc == null ? null : "'" + obj.h5fasciculacionesphc + "'");
				}
				if(obj.h5fuemuscularphc != objOriginal.h5fuemuscularphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
					arrValores.Add(obj.h5fuemuscularphc == null ? null : "'" + obj.h5fuemuscularphc + "'");
				}
				if(obj.h5movinvoluntariosphc != objOriginal.h5movinvoluntariosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
					arrValores.Add(obj.h5movinvoluntariosphc == null ? null : "'" + obj.h5movinvoluntariosphc + "'");
				}
				if(obj.h5equilibratoriaphc != objOriginal.h5equilibratoriaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
					arrValores.Add(obj.h5equilibratoriaphc == null ? null : "'" + obj.h5equilibratoriaphc + "'");
				}
				if(obj.h5rombergphc != objOriginal.h5rombergphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
					arrValores.Add(obj.h5rombergphc == null ? null : "'" + obj.h5rombergphc + "'");
				}
				if(obj.h5dednarderphc != objOriginal.h5dednarderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
					arrValores.Add(obj.h5dednarderphc == null ? null : "'" + obj.h5dednarderphc + "'");
				}
				if(obj.h5dednarizqphc != objOriginal.h5dednarizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
					arrValores.Add(obj.h5dednarizqphc == null ? null : "'" + obj.h5dednarizqphc + "'");
				}
				if(obj.h5deddedderphc != objOriginal.h5deddedderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
					arrValores.Add(obj.h5deddedderphc == null ? null : "'" + obj.h5deddedderphc + "'");
				}
				if(obj.h5deddedizqphc != objOriginal.h5deddedizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
					arrValores.Add(obj.h5deddedizqphc == null ? null : "'" + obj.h5deddedizqphc + "'");
				}
				if(obj.h5talrodderphc != objOriginal.h5talrodderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
					arrValores.Add(obj.h5talrodderphc == null ? null : "'" + obj.h5talrodderphc + "'");
				}
				if(obj.h5talrodizqphc != objOriginal.h5talrodizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
					arrValores.Add(obj.h5talrodizqphc == null ? null : "'" + obj.h5talrodizqphc + "'");
				}
				if(obj.h5movrapidphc != objOriginal.h5movrapidphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
					arrValores.Add(obj.h5movrapidphc == null ? null : "'" + obj.h5movrapidphc + "'");
				}
				if(obj.h5rebotephc != objOriginal.h5rebotephc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
					arrValores.Add(obj.h5rebotephc == null ? null : "'" + obj.h5rebotephc + "'");
				}
				if(obj.h5habilidadespecifphc != objOriginal.h5habilidadespecifphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
					arrValores.Add(obj.h5habilidadespecifphc == null ? null : "'" + obj.h5habilidadespecifphc + "'");
				}
				if(obj.apiestado != objOriginal.apiestado )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
					arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				}
				if(obj.apitransaccion != objOriginal.apitransaccion )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
					arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				}
				if(obj.usumod != objOriginal.usumod )
				{
					arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
					arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pachistoriaclinica2 a partir de una clase del tipo EntPacHistoriaclinica2 y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpachistoriaclinica2
		/// </returns>
		public int DeleteQuery(EntPacHistoriaclinica2 obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.DeleteBd(EntPacHistoriaclinica2.StrNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pachistoriaclinica2 a partir de una clase del tipo EntPacHistoriaclinica2 y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.epachistoriaclinica2">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionpachistoriaclinica2
		/// </returns>
		public int DeleteQuery(EntPacHistoriaclinica2 obj, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.DeleteBd(EntPacHistoriaclinica2.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pachistoriaclinica2 a partir de una clase del tipo epachistoriaclinica2
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpachistoriaclinica2
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd("pachistoriaclinica2", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pachistoriaclinica2 a partir de una clase del tipo epachistoriaclinica2
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica2
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionpachistoriaclinica2
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd(EntPacHistoriaclinica2.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region Llenado de elementos

		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, String textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, String textField, EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, String textField, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.DataValueField = table.Columns[0].ColumnName;
					cmb.DataTextField = table.Columns[1].ColumnName;
					cmb.DataSource = table;
					cmb.DataBind();

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntPacHistoriaclinica2.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica2.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridViewOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, String textField, EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, String textField, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica2
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica2
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntPacHistoriaclinica2.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4formaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5weberphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5tonophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica2.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica2.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica2.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica2.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica2
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntPacHistoriaclinica2.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 


		#endregion

		#region Funciones Internas

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto pachistoriaclinica2
		/// </returns>
		internal EntPacHistoriaclinica2 crearObjeto(DataRow row)
		{
			var obj = new EntPacHistoriaclinica2();
			obj.idppa = GetColumnType(row[EntPacHistoriaclinica2.Fields.idppa.ToString()], EntPacHistoriaclinica2.Fields.idppa);
			obj.h4olfnormalphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString()], EntPacHistoriaclinica2.Fields.h4olfnormalphc);
			obj.h4olfanosmiaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString()], EntPacHistoriaclinica2.Fields.h4olfanosmiaphc);
			obj.h4optscodphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4optscodphc.ToString()], EntPacHistoriaclinica2.Fields.h4optscodphc);
			obj.h4optscoiphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString()], EntPacHistoriaclinica2.Fields.h4optscoiphc);
			obj.h4optccodphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4optccodphc.ToString()], EntPacHistoriaclinica2.Fields.h4optccodphc);
			obj.h4optccoiphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString()], EntPacHistoriaclinica2.Fields.h4optccoiphc);
			obj.h4fondoodphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString()], EntPacHistoriaclinica2.Fields.h4fondoodphc);
			obj.h4fondooiphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString()], EntPacHistoriaclinica2.Fields.h4fondooiphc);
			obj.h4campimetriaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString()], EntPacHistoriaclinica2.Fields.h4campimetriaphc);
			obj.h4prosisphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4prosisphc.ToString()], EntPacHistoriaclinica2.Fields.h4prosisphc);
			obj.h4posicionojosphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString()], EntPacHistoriaclinica2.Fields.h4posicionojosphc);
			obj.h4exoftalmiaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString()], EntPacHistoriaclinica2.Fields.h4exoftalmiaphc);
			obj.h4hornerphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4hornerphc.ToString()], EntPacHistoriaclinica2.Fields.h4hornerphc);
			obj.h4movocularesphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString()], EntPacHistoriaclinica2.Fields.h4movocularesphc);
			obj.h4nistagmuxphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString()], EntPacHistoriaclinica2.Fields.h4nistagmuxphc);
			obj.h4diplopia1phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia1phc);
			obj.h4diplopia2phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia2phc);
			obj.h4diplopia3phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia3phc);
			obj.h4diplopia4phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia4phc);
			obj.h4diplopia5phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia5phc);
			obj.h4diplopia6phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia6phc);
			obj.h4diplopia7phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia7phc);
			obj.h4diplopia8phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia8phc);
			obj.h4diplopia9phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia9phc);
			obj.h4convergenciaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString()], EntPacHistoriaclinica2.Fields.h4convergenciaphc);
			obj.h4acomodacionodphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString()], EntPacHistoriaclinica2.Fields.h4acomodacionodphc);
			obj.h4acomodacionoiphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString()], EntPacHistoriaclinica2.Fields.h4acomodacionoiphc);
			obj.h4pupulasodphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString()], EntPacHistoriaclinica2.Fields.h4pupulasodphc);
			obj.h4pupulasoiphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString()], EntPacHistoriaclinica2.Fields.h4pupulasoiphc);
			obj.h4formaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4formaphc.ToString()], EntPacHistoriaclinica2.Fields.h4formaphc);
			obj.h4fotomotorodphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString()], EntPacHistoriaclinica2.Fields.h4fotomotorodphc);
			obj.h4fotomotoroiphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString()], EntPacHistoriaclinica2.Fields.h4fotomotoroiphc);
			obj.h4consensualdaiphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString()], EntPacHistoriaclinica2.Fields.h4consensualdaiphc);
			obj.h4consensualiadphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString()], EntPacHistoriaclinica2.Fields.h4consensualiadphc);
			obj.h4senfacialderphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString()], EntPacHistoriaclinica2.Fields.h4senfacialderphc);
			obj.h4senfacializqphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString()], EntPacHistoriaclinica2.Fields.h4senfacializqphc);
			obj.h4reflejoderphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString()], EntPacHistoriaclinica2.Fields.h4reflejoderphc);
			obj.h4reflejoizqphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString()], EntPacHistoriaclinica2.Fields.h4reflejoizqphc);
			obj.h4aberturabocaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString()], EntPacHistoriaclinica2.Fields.h4aberturabocaphc);
			obj.h4movmasticatoriosphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString()], EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc);
			obj.h4refmentonianophc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString()], EntPacHistoriaclinica2.Fields.h4refmentonianophc);
			obj.h4facsimetriaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString()], EntPacHistoriaclinica2.Fields.h4facsimetriaphc);
			obj.h4facmovimientosphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString()], EntPacHistoriaclinica2.Fields.h4facmovimientosphc);
			obj.h4facparalisisphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString()], EntPacHistoriaclinica2.Fields.h4facparalisisphc);
			obj.h4faccentralphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString()], EntPacHistoriaclinica2.Fields.h4faccentralphc);
			obj.h4facperifericaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString()], EntPacHistoriaclinica2.Fields.h4facperifericaphc);
			obj.h4facgustophc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4facgustophc.ToString()], EntPacHistoriaclinica2.Fields.h4facgustophc);
			obj.h5otoscopiaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString()], EntPacHistoriaclinica2.Fields.h5otoscopiaphc);
			obj.h5aguaudiderphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString()], EntPacHistoriaclinica2.Fields.h5aguaudiderphc);
			obj.h5aguaudiizqphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString()], EntPacHistoriaclinica2.Fields.h5aguaudiizqphc);
			obj.h5weberphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5weberphc.ToString()], EntPacHistoriaclinica2.Fields.h5weberphc);
			obj.h5rinnephc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5rinnephc.ToString()], EntPacHistoriaclinica2.Fields.h5rinnephc);
			obj.h5pruebaslabphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString()], EntPacHistoriaclinica2.Fields.h5pruebaslabphc);
			obj.h5elevpaladarphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString()], EntPacHistoriaclinica2.Fields.h5elevpaladarphc);
			obj.h5uvulaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString()], EntPacHistoriaclinica2.Fields.h5uvulaphc);
			obj.h5refnauceosophc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString()], EntPacHistoriaclinica2.Fields.h5refnauceosophc);
			obj.h5deglucionphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString()], EntPacHistoriaclinica2.Fields.h5deglucionphc);
			obj.h5tonovozphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString()], EntPacHistoriaclinica2.Fields.h5tonovozphc);
			obj.h5esternocleidomastoideophc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString()], EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc);
			obj.h5trapeciophc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString()], EntPacHistoriaclinica2.Fields.h5trapeciophc);
			obj.h5desviacionphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString()], EntPacHistoriaclinica2.Fields.h5desviacionphc);
			obj.h5atrofiaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString()], EntPacHistoriaclinica2.Fields.h5atrofiaphc);
			obj.h5fasciculacionphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString()], EntPacHistoriaclinica2.Fields.h5fasciculacionphc);
			obj.h5fuerzaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString()], EntPacHistoriaclinica2.Fields.h5fuerzaphc);
			obj.h5marchaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5marchaphc.ToString()], EntPacHistoriaclinica2.Fields.h5marchaphc);
			obj.h5tonophc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5tonophc.ToString()], EntPacHistoriaclinica2.Fields.h5tonophc);
			obj.h5volumenphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5volumenphc.ToString()], EntPacHistoriaclinica2.Fields.h5volumenphc);
			obj.h5fasciculacionesphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString()], EntPacHistoriaclinica2.Fields.h5fasciculacionesphc);
			obj.h5fuemuscularphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString()], EntPacHistoriaclinica2.Fields.h5fuemuscularphc);
			obj.h5movinvoluntariosphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString()], EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc);
			obj.h5equilibratoriaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString()], EntPacHistoriaclinica2.Fields.h5equilibratoriaphc);
			obj.h5rombergphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5rombergphc.ToString()], EntPacHistoriaclinica2.Fields.h5rombergphc);
			obj.h5dednarderphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString()], EntPacHistoriaclinica2.Fields.h5dednarderphc);
			obj.h5dednarizqphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString()], EntPacHistoriaclinica2.Fields.h5dednarizqphc);
			obj.h5deddedderphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString()], EntPacHistoriaclinica2.Fields.h5deddedderphc);
			obj.h5deddedizqphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString()], EntPacHistoriaclinica2.Fields.h5deddedizqphc);
			obj.h5talrodderphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString()], EntPacHistoriaclinica2.Fields.h5talrodderphc);
			obj.h5talrodizqphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString()], EntPacHistoriaclinica2.Fields.h5talrodizqphc);
			obj.h5movrapidphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString()], EntPacHistoriaclinica2.Fields.h5movrapidphc);
			obj.h5rebotephc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5rebotephc.ToString()], EntPacHistoriaclinica2.Fields.h5rebotephc);
			obj.h5habilidadespecifphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString()], EntPacHistoriaclinica2.Fields.h5habilidadespecifphc);
			obj.apiestado = GetColumnType(row[EntPacHistoriaclinica2.Fields.apiestado.ToString()], EntPacHistoriaclinica2.Fields.apiestado);
			obj.apitransaccion = GetColumnType(row[EntPacHistoriaclinica2.Fields.apitransaccion.ToString()], EntPacHistoriaclinica2.Fields.apitransaccion);
			obj.usucre = GetColumnType(row[EntPacHistoriaclinica2.Fields.usucre.ToString()], EntPacHistoriaclinica2.Fields.usucre);
			obj.feccre = GetColumnType(row[EntPacHistoriaclinica2.Fields.feccre.ToString()], EntPacHistoriaclinica2.Fields.feccre);
			obj.usumod = GetColumnType(row[EntPacHistoriaclinica2.Fields.usumod.ToString()], EntPacHistoriaclinica2.Fields.usumod);
			obj.fecmod = GetColumnType(row[EntPacHistoriaclinica2.Fields.fecmod.ToString()], EntPacHistoriaclinica2.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto pachistoriaclinica2
		/// </returns>
		internal EntPacHistoriaclinica2 crearObjetoRevisado(DataRow row)
		{
			var obj = new EntPacHistoriaclinica2();
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.idppa.ToString()))
				obj.idppa = GetColumnType(row[EntPacHistoriaclinica2.Fields.idppa.ToString()], EntPacHistoriaclinica2.Fields.idppa);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString()))
				obj.h4olfnormalphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4olfnormalphc.ToString()], EntPacHistoriaclinica2.Fields.h4olfnormalphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString()))
				obj.h4olfanosmiaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4olfanosmiaphc.ToString()], EntPacHistoriaclinica2.Fields.h4olfanosmiaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4optscodphc.ToString()))
				obj.h4optscodphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4optscodphc.ToString()], EntPacHistoriaclinica2.Fields.h4optscodphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString()))
				obj.h4optscoiphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4optscoiphc.ToString()], EntPacHistoriaclinica2.Fields.h4optscoiphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4optccodphc.ToString()))
				obj.h4optccodphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4optccodphc.ToString()], EntPacHistoriaclinica2.Fields.h4optccodphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString()))
				obj.h4optccoiphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4optccoiphc.ToString()], EntPacHistoriaclinica2.Fields.h4optccoiphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString()))
				obj.h4fondoodphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4fondoodphc.ToString()], EntPacHistoriaclinica2.Fields.h4fondoodphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString()))
				obj.h4fondooiphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4fondooiphc.ToString()], EntPacHistoriaclinica2.Fields.h4fondooiphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString()))
				obj.h4campimetriaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4campimetriaphc.ToString()], EntPacHistoriaclinica2.Fields.h4campimetriaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4prosisphc.ToString()))
				obj.h4prosisphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4prosisphc.ToString()], EntPacHistoriaclinica2.Fields.h4prosisphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString()))
				obj.h4posicionojosphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4posicionojosphc.ToString()], EntPacHistoriaclinica2.Fields.h4posicionojosphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString()))
				obj.h4exoftalmiaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4exoftalmiaphc.ToString()], EntPacHistoriaclinica2.Fields.h4exoftalmiaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4hornerphc.ToString()))
				obj.h4hornerphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4hornerphc.ToString()], EntPacHistoriaclinica2.Fields.h4hornerphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString()))
				obj.h4movocularesphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4movocularesphc.ToString()], EntPacHistoriaclinica2.Fields.h4movocularesphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString()))
				obj.h4nistagmuxphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4nistagmuxphc.ToString()], EntPacHistoriaclinica2.Fields.h4nistagmuxphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString()))
				obj.h4diplopia1phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia1phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia1phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString()))
				obj.h4diplopia2phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia2phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia2phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString()))
				obj.h4diplopia3phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia3phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia3phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString()))
				obj.h4diplopia4phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia4phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia4phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString()))
				obj.h4diplopia5phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia5phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia5phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString()))
				obj.h4diplopia6phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia6phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia6phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString()))
				obj.h4diplopia7phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia7phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia7phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString()))
				obj.h4diplopia8phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia8phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia8phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString()))
				obj.h4diplopia9phc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4diplopia9phc.ToString()], EntPacHistoriaclinica2.Fields.h4diplopia9phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString()))
				obj.h4convergenciaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4convergenciaphc.ToString()], EntPacHistoriaclinica2.Fields.h4convergenciaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString()))
				obj.h4acomodacionodphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4acomodacionodphc.ToString()], EntPacHistoriaclinica2.Fields.h4acomodacionodphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString()))
				obj.h4acomodacionoiphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4acomodacionoiphc.ToString()], EntPacHistoriaclinica2.Fields.h4acomodacionoiphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString()))
				obj.h4pupulasodphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4pupulasodphc.ToString()], EntPacHistoriaclinica2.Fields.h4pupulasodphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString()))
				obj.h4pupulasoiphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4pupulasoiphc.ToString()], EntPacHistoriaclinica2.Fields.h4pupulasoiphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4formaphc.ToString()))
				obj.h4formaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4formaphc.ToString()], EntPacHistoriaclinica2.Fields.h4formaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString()))
				obj.h4fotomotorodphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4fotomotorodphc.ToString()], EntPacHistoriaclinica2.Fields.h4fotomotorodphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString()))
				obj.h4fotomotoroiphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4fotomotoroiphc.ToString()], EntPacHistoriaclinica2.Fields.h4fotomotoroiphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString()))
				obj.h4consensualdaiphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4consensualdaiphc.ToString()], EntPacHistoriaclinica2.Fields.h4consensualdaiphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString()))
				obj.h4consensualiadphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4consensualiadphc.ToString()], EntPacHistoriaclinica2.Fields.h4consensualiadphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString()))
				obj.h4senfacialderphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4senfacialderphc.ToString()], EntPacHistoriaclinica2.Fields.h4senfacialderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString()))
				obj.h4senfacializqphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4senfacializqphc.ToString()], EntPacHistoriaclinica2.Fields.h4senfacializqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString()))
				obj.h4reflejoderphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4reflejoderphc.ToString()], EntPacHistoriaclinica2.Fields.h4reflejoderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString()))
				obj.h4reflejoizqphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4reflejoizqphc.ToString()], EntPacHistoriaclinica2.Fields.h4reflejoizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString()))
				obj.h4aberturabocaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4aberturabocaphc.ToString()], EntPacHistoriaclinica2.Fields.h4aberturabocaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString()))
				obj.h4movmasticatoriosphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc.ToString()], EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString()))
				obj.h4refmentonianophc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4refmentonianophc.ToString()], EntPacHistoriaclinica2.Fields.h4refmentonianophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString()))
				obj.h4facsimetriaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4facsimetriaphc.ToString()], EntPacHistoriaclinica2.Fields.h4facsimetriaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString()))
				obj.h4facmovimientosphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4facmovimientosphc.ToString()], EntPacHistoriaclinica2.Fields.h4facmovimientosphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString()))
				obj.h4facparalisisphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4facparalisisphc.ToString()], EntPacHistoriaclinica2.Fields.h4facparalisisphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString()))
				obj.h4faccentralphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4faccentralphc.ToString()], EntPacHistoriaclinica2.Fields.h4faccentralphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString()))
				obj.h4facperifericaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4facperifericaphc.ToString()], EntPacHistoriaclinica2.Fields.h4facperifericaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h4facgustophc.ToString()))
				obj.h4facgustophc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h4facgustophc.ToString()], EntPacHistoriaclinica2.Fields.h4facgustophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString()))
				obj.h5otoscopiaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5otoscopiaphc.ToString()], EntPacHistoriaclinica2.Fields.h5otoscopiaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString()))
				obj.h5aguaudiderphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5aguaudiderphc.ToString()], EntPacHistoriaclinica2.Fields.h5aguaudiderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString()))
				obj.h5aguaudiizqphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5aguaudiizqphc.ToString()], EntPacHistoriaclinica2.Fields.h5aguaudiizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5weberphc.ToString()))
				obj.h5weberphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5weberphc.ToString()], EntPacHistoriaclinica2.Fields.h5weberphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5rinnephc.ToString()))
				obj.h5rinnephc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5rinnephc.ToString()], EntPacHistoriaclinica2.Fields.h5rinnephc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString()))
				obj.h5pruebaslabphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5pruebaslabphc.ToString()], EntPacHistoriaclinica2.Fields.h5pruebaslabphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString()))
				obj.h5elevpaladarphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5elevpaladarphc.ToString()], EntPacHistoriaclinica2.Fields.h5elevpaladarphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString()))
				obj.h5uvulaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5uvulaphc.ToString()], EntPacHistoriaclinica2.Fields.h5uvulaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString()))
				obj.h5refnauceosophc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5refnauceosophc.ToString()], EntPacHistoriaclinica2.Fields.h5refnauceosophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString()))
				obj.h5deglucionphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5deglucionphc.ToString()], EntPacHistoriaclinica2.Fields.h5deglucionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString()))
				obj.h5tonovozphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5tonovozphc.ToString()], EntPacHistoriaclinica2.Fields.h5tonovozphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString()))
				obj.h5esternocleidomastoideophc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc.ToString()], EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString()))
				obj.h5trapeciophc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5trapeciophc.ToString()], EntPacHistoriaclinica2.Fields.h5trapeciophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString()))
				obj.h5desviacionphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5desviacionphc.ToString()], EntPacHistoriaclinica2.Fields.h5desviacionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString()))
				obj.h5atrofiaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5atrofiaphc.ToString()], EntPacHistoriaclinica2.Fields.h5atrofiaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString()))
				obj.h5fasciculacionphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5fasciculacionphc.ToString()], EntPacHistoriaclinica2.Fields.h5fasciculacionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString()))
				obj.h5fuerzaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5fuerzaphc.ToString()], EntPacHistoriaclinica2.Fields.h5fuerzaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5marchaphc.ToString()))
				obj.h5marchaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5marchaphc.ToString()], EntPacHistoriaclinica2.Fields.h5marchaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5tonophc.ToString()))
				obj.h5tonophc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5tonophc.ToString()], EntPacHistoriaclinica2.Fields.h5tonophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5volumenphc.ToString()))
				obj.h5volumenphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5volumenphc.ToString()], EntPacHistoriaclinica2.Fields.h5volumenphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString()))
				obj.h5fasciculacionesphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5fasciculacionesphc.ToString()], EntPacHistoriaclinica2.Fields.h5fasciculacionesphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString()))
				obj.h5fuemuscularphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5fuemuscularphc.ToString()], EntPacHistoriaclinica2.Fields.h5fuemuscularphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString()))
				obj.h5movinvoluntariosphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc.ToString()], EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString()))
				obj.h5equilibratoriaphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5equilibratoriaphc.ToString()], EntPacHistoriaclinica2.Fields.h5equilibratoriaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5rombergphc.ToString()))
				obj.h5rombergphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5rombergphc.ToString()], EntPacHistoriaclinica2.Fields.h5rombergphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString()))
				obj.h5dednarderphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5dednarderphc.ToString()], EntPacHistoriaclinica2.Fields.h5dednarderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString()))
				obj.h5dednarizqphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5dednarizqphc.ToString()], EntPacHistoriaclinica2.Fields.h5dednarizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString()))
				obj.h5deddedderphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5deddedderphc.ToString()], EntPacHistoriaclinica2.Fields.h5deddedderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString()))
				obj.h5deddedizqphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5deddedizqphc.ToString()], EntPacHistoriaclinica2.Fields.h5deddedizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString()))
				obj.h5talrodderphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5talrodderphc.ToString()], EntPacHistoriaclinica2.Fields.h5talrodderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString()))
				obj.h5talrodizqphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5talrodizqphc.ToString()], EntPacHistoriaclinica2.Fields.h5talrodizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString()))
				obj.h5movrapidphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5movrapidphc.ToString()], EntPacHistoriaclinica2.Fields.h5movrapidphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5rebotephc.ToString()))
				obj.h5rebotephc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5rebotephc.ToString()], EntPacHistoriaclinica2.Fields.h5rebotephc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString()))
				obj.h5habilidadespecifphc = GetColumnType(row[EntPacHistoriaclinica2.Fields.h5habilidadespecifphc.ToString()], EntPacHistoriaclinica2.Fields.h5habilidadespecifphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[EntPacHistoriaclinica2.Fields.apiestado.ToString()], EntPacHistoriaclinica2.Fields.apiestado);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.apitransaccion.ToString()))
				obj.apitransaccion = GetColumnType(row[EntPacHistoriaclinica2.Fields.apitransaccion.ToString()], EntPacHistoriaclinica2.Fields.apitransaccion);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[EntPacHistoriaclinica2.Fields.usucre.ToString()], EntPacHistoriaclinica2.Fields.usucre);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[EntPacHistoriaclinica2.Fields.feccre.ToString()], EntPacHistoriaclinica2.Fields.feccre);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[EntPacHistoriaclinica2.Fields.usumod.ToString()], EntPacHistoriaclinica2.Fields.usumod);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica2.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[EntPacHistoriaclinica2.Fields.fecmod.ToString()], EntPacHistoriaclinica2.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtpachistoriaclinica2" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos pachistoriaclinica2
		/// </returns>
		internal List<EntPacHistoriaclinica2> crearLista(DataTable dtpachistoriaclinica2)
		{
			var list = new List<EntPacHistoriaclinica2>();
			
			foreach (DataRow row in dtpachistoriaclinica2.Rows)
			{
				var obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtpachistoriaclinica2" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos pachistoriaclinica2
		/// </returns>
		internal List<EntPacHistoriaclinica2> crearListaRevisada(DataTable dtpachistoriaclinica2)
		{
			List<EntPacHistoriaclinica2> list = new List<EntPacHistoriaclinica2>();
			
			foreach (DataRow row in dtpachistoriaclinica2.Rows)
			{
				var obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtpachistoriaclinica2" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos pachistoriaclinica2
		/// </returns>
		internal Queue<EntPacHistoriaclinica2> crearCola(DataTable dtpachistoriaclinica2)
		{
			Queue<EntPacHistoriaclinica2> cola = new Queue<EntPacHistoriaclinica2>();
			
			EntPacHistoriaclinica2 obj = new EntPacHistoriaclinica2();
			foreach (DataRow row in dtpachistoriaclinica2.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtpachistoriaclinica2" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos pachistoriaclinica2
		/// </returns>
		internal Stack<EntPacHistoriaclinica2> crearPila(DataTable dtpachistoriaclinica2)
		{
			Stack<EntPacHistoriaclinica2> pila = new Stack<EntPacHistoriaclinica2>();
			
			foreach (DataRow row in dtpachistoriaclinica2.Rows)
			{
				var obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtpachistoriaclinica2" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos pachistoriaclinica2
		/// </returns>
		internal Dictionary<String, EntPacHistoriaclinica2> crearDiccionario(DataTable dtpachistoriaclinica2)
		{
			Dictionary<String, EntPacHistoriaclinica2>  miDic = new Dictionary<String, EntPacHistoriaclinica2>();
			
			foreach (DataRow row in dtpachistoriaclinica2.Rows)
			{
				var obj = crearObjeto(row);
				miDic.Add(obj.idppa.ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtpachistoriaclinica2" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 HashTable de Objetos pachistoriaclinica2
		/// </returns>
		internal Hashtable crearHashTable(DataTable dtpachistoriaclinica2)
		{
			Hashtable miTabla = new Hashtable();
			
			foreach (DataRow row in dtpachistoriaclinica2.Rows)
			{
				var obj = crearObjeto(row);
				miTabla.Add(obj.idppa.ToString(), obj);
			}
			return miTabla;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtpachistoriaclinica2" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos pachistoriaclinica2
		/// </returns>
		internal Dictionary<String, EntPacHistoriaclinica2> crearDiccionarioRevisado(DataTable dtpachistoriaclinica2)
		{
			Dictionary<String, EntPacHistoriaclinica2>  miDic = new Dictionary<String, EntPacHistoriaclinica2>();
			
			foreach (DataRow row in dtpachistoriaclinica2.Rows)
			{
				var obj = crearObjetoRevisado(row);
				miDic.Add(obj.idppa.ToString(), obj);
			}
			return miDic;
		}
		
		internal Dictionary<String, EntPacHistoriaclinica2> crearDiccionario(DataTable dtpachistoriaclinica2, EntPacHistoriaclinica2.Fields dicKey)
		{
			Dictionary<String, EntPacHistoriaclinica2>  miDic = new Dictionary<String, EntPacHistoriaclinica2>();
			
			foreach (DataRow row in dtpachistoriaclinica2.Rows)
			{
				var obj = crearObjeto(row);
				
				var nameOfProperty = dicKey.ToString();
				var propertyInfo = obj.GetType().GetProperty(nameOfProperty);
				var value = propertyInfo.GetValue(obj, null);
				
				miDic.Add(value.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
		#endregion

	}
}

