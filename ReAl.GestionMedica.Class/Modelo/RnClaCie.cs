#region
/***********************************************************************************************************
	NOMBRE:       RnClaCie
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla clacie

	REVISIONES:
		Ver        FECHA       Autor            Descripcion
		---------  ----------  ---------------  ------------------------------------
		1.0        22/04/2021  R Alonzo Vera A  Creacion

*************************************************************************************************************/
#endregion

#region

using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Interface;
using System.Windows.Forms;
using System.Web.UI.WebControls;

#endregion

namespace ReAl.GestionMedica.Class.Modelo
{
    public class RnClaCie : IClaCie
    {
        //Debe implementar la Interface (Alt + Shift + F10)

        #region IClaCie Members

        #region Reflection

        /// <summary>
        /// Metodo que devuelve el Script SQL de la Tabla
        /// </summary>
        /// <returns>Script SQL</returns>
        public string GetTableScript()
        {
            TableClass tabla = new TableClass(typeof(EntClaCie));
            return tabla.CreateTableScript();
        }

        /// <summary>
        /// Metodo para castear Dinamicamente un Tipo
        /// </summary>
        /// <param name="valor">Tipo a ser casteado</param>
        /// <param name="myField">Enum de la columna</param>
        /// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
        public dynamic GetColumnType(object valor, EntClaCie.Fields myField)
        {
            if (DBNull.Value.Equals(valor))
                return null;
            Type destino = typeof(EntClaCie).GetProperty(myField.ToString()).PropertyType;
            var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;

            try
            {
                TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
                return tc.ConvertFrom(valor);
            }
            catch (Exception)
            {
                return Convert.ChangeType(valor, miTipo);
            }
        }

        /// <summary>
        /// Metodo para castear Dinamicamente un Tipo
        /// </summary>
        /// <param name="valor">Tipo a ser casteado</param>
        /// <param name="strField">Nombre de la columna</param>
        /// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
        public dynamic GetColumnType(object valor, string strField)
        {
            if (DBNull.Value.Equals(valor))
                return null;
            Type destino = typeof(EntClaCie).GetProperty(strField).PropertyType;
            var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;

            try
            {
                TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
                return tc.ConvertFrom(valor);
            }
            catch (Exception)
            {
                return Convert.ChangeType(valor, miTipo);
            }
        }

        /// <summary>
        /// Inserta una valor a una propiedad de un objeto instanciado
        /// </summary>
        /// <param name="obj">Objeto instanciado</param>
        /// <param name="strPropiedad">Es el nombre de la propiedad</param>
        /// <param name="dynValor">Es el valor que se insertara a la propiedad</param>
        public void SetDato(ref EntClaCie obj, string strPropiedad, dynamic dynValor)
        {
            if (obj == null) throw new ArgumentNullException();
            obj.GetType().GetProperty(strPropiedad).SetValue(obj, GetColumnType(dynValor, strPropiedad), null);
        }

        /// <summary>
        /// Obtiene el valor de una propiedad de un objeto instanciado
        /// </summary>
        /// <param name="obj">Objeto instanciado</param>
        /// <param name="strPropiedad">El nombre de la propiedad de la que se obtendra el valor</param>
        /// <returns>Devuelve el valor del a propiedad seleccionada</returns>
        public dynamic GetDato(ref EntClaCie obj, string strPropiedad)
        {
            if (obj == null) return null;
            var propertyInfo = obj.GetType().GetProperty(strPropiedad);
            return GetColumnType(propertyInfo.GetValue(obj, null), strPropiedad);
        }

        /// <summary>
        /// 	 Funcion que obtiene la llave primaria unica de la tabla clacie a partir de una cadena
        /// </summary>
        /// <param name="args" type="string[]">
        ///     <para>
        /// 		 Cadena desde la que se construye el identificador unico de la tabla clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Identificador unico de la tabla clacie
        /// </returns>
        public string CreatePk(string[] args)
        {
            return args[0];
        }

        #endregion

        #region ObtenerObjeto

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntClaCie a partir de la llave primaria
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public EntClaCie ObtenerObjeto(string stringcodcie)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(EntClaCie.Fields.codcie.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'" + stringcodcie + "'");

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntClaCie a partir del usuario que inserta
        /// </summary>
        /// <param name="strUsuCre">Login o nombre de usuario</param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public EntClaCie ObtenerObjetoInsertado(string strUsuCre)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public EntClaCie ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public EntClaCie ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count == 1)
                {
                    EntClaCie obj = new EntClaCie();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public EntClaCie ObtenerObjeto(Hashtable htbFiltro)
        {
            return ObtenerObjeto(htbFiltro, "");
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public EntClaCie ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count == 1)
                {
                    EntClaCie obj = new EntClaCie();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public EntClaCie ObtenerObjeto(EntClaCie.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public EntClaCie ObtenerObjeto(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un Business Object del Tipo EntClaCie a partir de su llave promaria
        /// </summary>
        /// <returns>
        /// 	Objeto del Tipo EntClaCie
        /// </returns>
        public EntClaCie ObtenerObjeto(string stringcodcie, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(EntClaCie.Fields.codcie.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'" + stringcodcie + "'");
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public EntClaCie ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public EntClaCie ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans)
        {
            return ObtenerObjeto(htbFiltro, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public EntClaCie ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public EntClaCie ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count == 1)
                {
                    EntClaCie obj = new EntClaCie();
                    obj = crearObjeto(table.Rows[0]);
                    return obj;
                }
                else if (table.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de un objeto");
                else
                    return null;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public EntClaCie ObtenerObjeto(EntClaCie.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene los datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public EntClaCie ObtenerObjeto(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        #endregion

        #region ObtenerLista

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerLista()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerLista(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntClaCie>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerLista(EntClaCie.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerLista(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntClaCie>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerLista(EntClaCie.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerLista(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerLista(Hashtable htbFiltro)
        {
            return ObtenerLista(htbFiltro, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntClaCie>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans)
        {
            return ObtenerLista(htbFiltro, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearLista(table);
                }
                else
                    return new List<EntClaCie>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerListaDesdeVista(String strVista)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntClaCie>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
        {
            return ObtenerListaDesdeVista(strVista, htbFiltro, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntClaCie>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerListaDesdeVista(String strVista, EntClaCie.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerListaDesdeVista(String strVista, EntClaCie.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntClaCie>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans)
        {
            return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("*");

                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearListaRevisada(table);
                }
                else
                    return new List<EntClaCie>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerListaDesdeVista(String strVista, EntClaCie.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="strVista" type="String">
        ///     <para>
        /// 		 Nombre de la Vista desde que se van ha obtener los datos
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
        /// </returns>
        public List<EntClaCie> ObtenerListaDesdeVista(String strVista, EntClaCie.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        #endregion

        #region ObtenerCola y Obtener Pila

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntClaCie> ObtenerCola()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerCola(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntClaCie> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntClaCie> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearCola(table);
                }
                else
                    return new Queue<EntClaCie>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntClaCie> ObtenerCola(EntClaCie.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntClaCie> ObtenerCola(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntClaCie> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntClaCie> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearCola(table);
                }
                else
                    return new Queue<EntClaCie>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntClaCie> ObtenerCola(EntClaCie.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Queue<EntClaCie> ObtenerCola(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntClaCie> ObtenerPila()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerPila(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntClaCie> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntClaCie> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearPila(table);
                }
                else
                    return new Stack<EntClaCie>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntClaCie> ObtenerPila(EntClaCie.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntClaCie> ObtenerPila(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntClaCie> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntClaCie> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearPila(table);
                }
                else
                    return new Stack<EntClaCie>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntClaCie> ObtenerPila(EntClaCie.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
        /// </returns>
        public Stack<EntClaCie> ObtenerPila(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        #endregion

        #region ObtenerDataTable

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla clacie
        /// </summary>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable NuevoDataTable()
        {
            try
            {
                DataTable table = new DataTable();
                DataColumn dc;
                dc = new DataColumn(EntClaCie.Fields.codcie.ToString(), typeof(EntClaCie).GetProperty(EntClaCie.Fields.codcie.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntClaCie.Fields.desccie.ToString(), typeof(EntClaCie).GetProperty(EntClaCie.Fields.desccie.ToString()).PropertyType);
                table.Columns.Add(dc);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que genera un DataTable con determinadas columnas de una clacie
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable NuevoDataTable(ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'2'");

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla clacie
        /// </summary>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable ObtenerDataTable()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE clacie
        /// </summary>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable ObtenerDataTable(String strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla clacie
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla clacie
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla clacie
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla clacie
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
        {
            try
            {
                return ObtenerDataTable(arrColumnas, htbFiltro, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla clacie
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla clacie
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable ObtenerDataTable(Hashtable htbFiltro)
        {
            try
            {
                return ObtenerDataTable(htbFiltro, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla clacie
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla clacie
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla clacie
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla clacie
        /// </summary>
        /// <param name="htbFiltro" type="System.Collections.Hashtable">
        ///     <para>
        /// 		 Hashtable que contienen los pares para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                ArrayList arrValoresWhere = new ArrayList();

                foreach (DictionaryEntry entry in htbFiltro)
                {
                    arrColumnasWhere.Add(entry.Key.ToString());
                    arrValoresWhere.Add(entry.Value.ToString());
                }

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla clacie
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());
                return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla clacie
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataTable con los registro de una tabla clacie
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 DataTable con los datos obtenidos de clacie
        /// </returns>
        public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableOr(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(EntClaCie.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(EntClaCie.Fields searchField, object searchValue, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Parametros adicionales
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, EntClaCie.Fields searchField, object searchValue, string strParametrosAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
        /// </summary>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	DataTable que cumple con los filtros de los parametros
        /// </returns>
        public DataTable ObtenerDataTable(ArrayList arrColumnas, EntClaCie.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region ObtenerDiccionario

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntClaCie> ObtenerDiccionario()
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntClaCie> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntClaCie> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, EntClaCie>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntClaCie> ObtenerDiccionario(EntClaCie.Fields searchField, object searchValue)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntClaCie> ObtenerDiccionario(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntClaCie> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntClaCie> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table);
                }
                else
                    return new Dictionary<string, EntClaCie>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntClaCie> ObtenerDiccionario(EntClaCie.Fields searchField, object searchValue, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
        }

        /// <summary>
        /// 	Funcion que obtiene un conjunto de datos de una Clase EntClaCie a partir de condiciones WHERE
        /// </summary>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
        /// </returns>
        public Dictionary<String, EntClaCie> ObtenerDiccionario(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
        }

        public Dictionary<String, EntClaCie> ObtenerDiccionarioKey(EntClaCie.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
        }

        public Dictionary<String, EntClaCie> ObtenerDiccionarioKey(String strParamAdic, EntClaCie.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add("'1'");
            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add("'1'");

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdic, dicKey);
        }

        public Dictionary<String, EntClaCie> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, EntClaCie.Fields dicKey)
        {
            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, "", dicKey);
        }

        public Dictionary<String, EntClaCie> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, EntClaCie.Fields dicKey)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Rows.Count > 0)
                {
                    return crearDiccionario(table, dicKey);
                }
                else
                    return new Dictionary<string, EntClaCie>();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public Dictionary<String, EntClaCie> ObtenerDiccionarioKey(EntClaCie.Fields searchField, object searchValue, EntClaCie.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
        }

        public Dictionary<String, EntClaCie> ObtenerDiccionarioKey(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales, EntClaCie.Fields dicKey)
        {
            ArrayList arrColumnasWhere = new ArrayList();
            arrColumnasWhere.Add(searchField.ToString());

            ArrayList arrValoresWhere = new ArrayList();
            arrValoresWhere.Add(searchValue);

            return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey);
        }

        #endregion

        #region ObjetoASp

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="strNombreSp" type="System.string">
        ///     <para>
        /// 		 Nombre del Procedimiento a ejecutar sobre el SP
        ///     </para>
        /// </param>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se va a ejecutar el SP de la tabla clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor de registros afectados en el Procedimiento de la tabla clacie
        /// </returns>
        public int EjecutarSpDesdeObjeto(string strNombreSp, EntClaCie obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrNombreParam = new ArrayList();
                arrNombreParam.Add(EntClaCie.Fields.codcie.ToString());
                arrNombreParam.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrValoresParam = new ArrayList();
                arrValoresParam.Add(obj.codcie == null ? null : "'" + obj.codcie + "'");
                arrValoresParam.Add(obj.desccie == null ? null : "'" + obj.desccie + "'");

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="strNombreSp" type="System.string">
        ///     <para>
        /// 		 Nombre del Procedimiento a ejecutar sobre el SP
        ///     </para>
        /// </param>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se va a ejecutar el SP de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor de registros afectados en el Procedimiento de la tabla clacie
        /// </returns>
        public int EjecutarSpDesdeObjeto(string strNombreSp, EntClaCie obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrNombreParam = new ArrayList();
                arrNombreParam.Add(EntClaCie.Fields.codcie.ToString());
                arrNombreParam.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrValoresParam = new ArrayList();
                arrValoresParam.Add(obj.codcie == null ? null : "'" + obj.codcie + "'");
                arrValoresParam.Add(obj.desccie == null ? null : "'" + obj.desccie + "'");

                //Llamamos al Procedmiento Almacenado
                CConn local = new CConn();
                return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region FuncionesAgregadas

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesCount(EntClaCie.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesCount(EntClaCie.Fields refField, EntClaCie.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
        /// </summary>
        /// <param name="refField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesCount(EntClaCie.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("count(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMin(EntClaCie.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMin(EntClaCie.Fields refField, EntClaCie.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
        /// </summary>
        /// <param name="refField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMin(EntClaCie.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("min(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMax(EntClaCie.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMax(EntClaCie.Fields refField, EntClaCie.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
        /// </summary>
        /// <param name="refField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesMax(EntClaCie.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("max(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesSum(EntClaCie.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesSum(EntClaCie.Fields refField, EntClaCie.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
        /// </summary>
        /// <param name="refField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesSum(EntClaCie.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("sum(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesAvg(EntClaCie.Fields refField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="whereField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Columna que va a filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="valueField" type="System.Object">
        ///     <para>
        /// 		 Valor para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesAvg(EntClaCie.Fields refField, EntClaCie.Fields whereField, object valueField)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(whereField.ToString());
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(valueField.ToString());

                return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
        /// </summary>
        /// <param name="refField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo al que se va a aplicar la funcion
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <returns>
        /// 	Valor del Tipo EntClaCie que cumple con los filtros de los parametros
        /// </returns>
        public int FuncionesAvg(EntClaCie.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add("avg(" + refField + ")");
                DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
                if (dtTemp.Rows.Count == 0)
                    throw new Exception("La consulta no ha devuelto resultados.");
                if (dtTemp.Rows.Count > 1)
                    throw new Exception("Se ha devuelto mas de una fila.");
                if (dtTemp.Rows[0][0] == null)
                    return 0;
                if (dtTemp.Rows[0][0] == "")
                    return 0;
                return int.Parse(dtTemp.Rows[0][0].ToString());
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region ABMs SP

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionclacie
        /// </returns>
        public bool Insert(EntClaCie obj)
        {
            throw new Exception("No existe el Procedimiento Almacenado SpclacieIns.");
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionclacie
        /// </returns>
        public bool Insert(EntClaCie obj, ref CTrans localTrans)
        {
            throw new Exception("No existe el Procedimiento Almacenado SpclacieIns.");
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionclacie
        /// </returns>
        public int Update(EntClaCie obj)
        {
            throw new Exception("No existe el Procedimiento Almacenado SpclacieUpd.");
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionclacie
        /// </returns>
        public int Update(EntClaCie obj, ref CTrans localTrans)
        {
            throw new Exception("No existe el Procedimiento Almacenado SpclacieUpd.");
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionclacie
        /// </returns>
        public int Delete(EntClaCie obj)
        {
            throw new Exception("No existe el Procedimiento Almacenado SpclacieDel.");
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionclacie
        /// </returns>
        public int Delete(EntClaCie obj, ref CTrans localTrans)
        {
            throw new Exception("No existe el Procedimiento Almacenado SpclacieDel.");
        }

        /// <summary>
        /// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionclacie
        /// </returns>
        public int InsertUpdate(EntClaCie obj)
        {
            try
            {
                bool esInsertar = true;

                esInsertar = (esInsertar && (obj.codcie == null));

                if (esInsertar)
                    return Insert(obj) ? 1 : 0;
                else
                    return Update(obj);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionclacie
        /// </returns>
        public int InsertUpdate(EntClaCie obj, ref CTrans localTrans)
        {
            try
            {
                bool esInsertar = false;

                esInsertar = (esInsertar && (obj.codcie == null));

                if (esInsertar)
                    return Insert(obj, ref localTrans) ? 1 : 0;
                else
                    return Update(obj, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region ABMs Query

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionclacie
        /// </returns>
        public bool InsertQuery(EntClaCie obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                //arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.codcie == null ? null : "'" + obj.codcie + "'");
                arrValores.Add(obj.desccie == null ? null : "'" + obj.desccie + "'");

                CConn local = new CConn();
                return local.InsertBd(EntClaCie.StrNombreTabla, arrColumnas, arrValores);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionclacie
        /// </returns>
        public bool InsertQuery(EntClaCie obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                //arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.codcie == null ? null : "'" + obj.codcie + "'");
                arrValores.Add(obj.desccie == null ? null : "'" + obj.desccie + "'");

                CConn local = new CConn();
                return local.InsertBd(EntClaCie.StrNombreTabla, arrColumnas, arrValores, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionclacie
        /// </returns>
        public bool InsertQueryIdentity(EntClaCie obj)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 	Funcion que inserta un nuevo registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a insertar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Valor TRUE or FALSE que indica el exito de la operacionclacie
        /// </returns>
        public bool InsertQueryIdentity(EntClaCie obj, ref CTrans localTrans)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionclacie
        /// </returns>
        public int UpdateQueryAll(EntClaCie obj)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.desccie == null ? null : "'" + obj.desccie + "'");

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntClaCie.Fields.codcie.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.codcie == null ? null : "'" + obj.codcie + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntClaCie.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla clacie a partir de una clase del tipo eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacion
        /// </returns>
        public int UpdateQueryAll(EntClaCie obj, ref CTrans localTrans)
        {
            try
            {
                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrValores = new ArrayList();
                arrValores.Add(obj.desccie == null ? null : "'" + obj.desccie + "'");

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntClaCie.Fields.codcie.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.codcie == null ? null : "'" + obj.codcie + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntClaCie.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla clacie a partir de una clase del tipo Eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionclacie
        /// </returns>
        public int UpdateQuery(EntClaCie obj)
        {
            try
            {
                //Obtenemos el Objeto original
                EntClaCie objOriginal = this.ObtenerObjeto(obj.codcie);

                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                ArrayList arrValores = new ArrayList();

                if (obj.desccie != objOriginal.desccie)
                {
                    arrColumnas.Add(EntClaCie.Fields.desccie.ToString());
                    arrValores.Add(obj.desccie == null ? null : "'" + obj.desccie + "'");
                }

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntClaCie.Fields.codcie.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.codcie == null ? null : "'" + obj.codcie + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntClaCie.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que actualiza un registro en la tabla clacie a partir de una clase del tipo eclacie
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a actualizar los valores a la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacion
        /// </returns>
        public int UpdateQuery(EntClaCie obj, ref CTrans localTrans)
        {
            try
            {
                //Obtenemos el Objeto original
                EntClaCie objOriginal = this.ObtenerObjeto(obj.codcie, ref localTrans);

                if (!obj.IsValid())
                {
                    throw new Exception(obj.ValidationErrorsString());
                }

                ArrayList arrColumnas = new ArrayList();
                ArrayList arrValores = new ArrayList();

                if (obj.desccie != objOriginal.desccie)
                {
                    arrColumnas.Add(EntClaCie.Fields.desccie.ToString());
                    arrValores.Add(obj.desccie == null ? null : "'" + obj.desccie + "'");
                }

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntClaCie.Fields.codcie.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.codcie == null ? null : "'" + obj.codcie + "'");

                CConn local = new CConn();
                return local.UpdateBd(EntClaCie.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla clacie a partir de una clase del tipo EntClaCie y su respectiva PK
        /// </summary>
        /// <param name="obj" type="Entidades.EntClaCie">
        ///     <para>
        /// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionclacie
        /// </returns>
        public int DeleteQuery(EntClaCie obj)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntClaCie.Fields.codcie.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.codcie == null ? null : "'" + obj.codcie + "'");

                CConn local = new CConn();
                return local.DeleteBd(EntClaCie.StrNombreTabla, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla clacie a partir de una clase del tipo EntClaCie y su PK
        /// </summary>
        /// <param name="obj" type="Entidades.eclacie">
        ///     <para>
        /// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacionclacie
        /// </returns>
        public int DeleteQuery(EntClaCie obj, ref CTrans localTrans)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(EntClaCie.Fields.codcie.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(obj.codcie == null ? null : "'" + obj.codcie + "'");

                CConn local = new CConn();
                return local.DeleteBd(EntClaCie.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla clacie a partir de una clase del tipo eclacie
        /// </summary>
        /// <param name="arrColumnasWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Valores para cada una de las columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cantidad de registros afectados por el exito de la operacionclacie
        /// </returns>
        public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CConn local = new CConn();
                return local.DeleteBd("clacie", arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que elimina un registro en la tabla clacie a partir de una clase del tipo eclacie
        /// </summary>
        /// <param name="arrColumnasWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="ArrayList">
        ///     <para>
        /// 		 Array de Valores para cada una de las columnas en la clausa WHERE
        ///     </para>
        /// </param>
        /// <param name="localTrans" type="App_Class.Conexion.CTrans">
        ///     <para>
        /// 		 Clase desde la que se va a utilizar una transaccion clacie
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Exito de la operacionclacie
        /// </returns>
        public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
        {
            try
            {
                CConn local = new CConn();
                return local.DeleteBd(EntClaCie.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #region Llenado de elementos

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField)
        {
            try
            {
                CargarDropDownList(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, String textField)
        {
            try
            {
                CargarDropDownList(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, String textField, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, EntClaCie.Fields searchField, object searchValue)
        {
            try
            {
                CargarDropDownList(ref cmb, valueField, textField.ToString(), searchField, searchValue);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, String textField, EntClaCie.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, EntClaCie.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, String textField, EntClaCie.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un DropDownList con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Columns.Count > 0)
                {
                    cmb.DataValueField = table.Columns[0].ColumnName;
                    cmb.DataTextField = table.Columns[1].ColumnName;
                    cmb.DataSource = table;
                    cmb.DataBind();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                dtg.DataSource = dsReader;
                dtg.DataBind();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, EntClaCie.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, EntClaCie.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntClaCie.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntClaCie.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarGridViewOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Web.UI.WebControls.GridView">
        ///     <para>
        /// 		 Control del tipo System.Web.UI.WebControls.GridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
                dtg.DataBind();
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, String textField)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, String textField, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(1);

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(1);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, EntClaCie.Fields searchField, object searchValue)
        {
            try
            {
                CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, String textField, EntClaCie.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, EntClaCie.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, String textField, EntClaCie.Fields searchField, object searchValue, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField.ToString());

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="valueField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="textField" type="String">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(valueField.ToString());
                arrColumnas.Add(textField);

                CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que carga un ComboBox con los valores de la tabla clacie
        /// </summary>
        /// <param name="cmb" type="System.Windows.Forms.ComboBox">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla clacie
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                if (table.Columns.Count > 0)
                {
                    cmb.ValueMember = table.Columns[0].ColumnName;
                    cmb.DisplayMember = table.Columns[1].ColumnName;
                    cmb.DataSource = table;
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="String">
        ///     <para>
        /// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add("'1'");
                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add("'1'");

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, EntClaCie.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, EntClaCie.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnas = new ArrayList();
                arrColumnas.Add(EntClaCie.Fields.codcie.ToString());
                arrColumnas.Add(EntClaCie.Fields.desccie.ToString());

                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntClaCie.Fields searchField, object searchValue)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un GridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="searchField" type="EntClaCie.Fileds">
        ///     <para>
        /// 		 Campo por el que se va a filtrar la busqueda
        ///     </para>
        /// </param>
        /// <param name="searchValue" type="object">
        ///     <para>
        /// 		 Valor para la busqueda
        ///     </para>
        /// </param>
        /// <param name="strParamAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntClaCie.Fields searchField, object searchValue, String strParamAdicionales)
        {
            try
            {
                ArrayList arrColumnasWhere = new ArrayList();
                arrColumnasWhere.Add(searchField.ToString());

                ArrayList arrValoresWhere = new ArrayList();
                arrValoresWhere.Add(searchValue);
                CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
        {
            try
            {
                CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        /// <summary>
        /// 	 Funcion que llena un DataGridView con los registro de una tabla clacie
        /// </summary>
        /// <param name="dtg" type="System.Windows.Forms.DataGridView">
        ///     <para>
        /// 		 Control del tipo System.Windows.Forms.DataGridView
        ///     </para>
        /// </param>
        /// <param name="arrColumnas" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
        ///     </para>
        /// </param>
        /// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las columnas WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="arrValoresWhere" type="System.Collections.ArrayList">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        /// <param name="strParametrosAdicionales" type="string">
        ///     <para>
        /// 		 Array de las valores WHERE para filtrar el resultado
        ///     </para>
        /// </param>
        public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
        {
            try
            {
                CConn local = new CConn();
                DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntClaCie.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);

                dtg.DataSource = dsReader;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        #endregion

        #endregion

        #region Funciones Internas

        /// <summary>
        /// 	 Funcion que devuelve un objeto a partir de un DataRow
        /// </summary>
        /// <param name="row" type="System.Data.DataRow">
        ///     <para>
        /// 		 DataRow con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Objeto clacie
        /// </returns>
        internal EntClaCie crearObjeto(DataRow row)
        {
            var obj = new EntClaCie();
            obj.codcie = GetColumnType(row[EntClaCie.Fields.codcie.ToString()], EntClaCie.Fields.codcie);
            obj.desccie = GetColumnType(row[EntClaCie.Fields.desccie.ToString()], EntClaCie.Fields.desccie);
            return obj;
        }

        /// <summary>
        /// 	 Funcion que devuelve un objeto a partir de un DataRow
        /// </summary>
        /// <param name="row" type="System.Data.DataRow">
        ///     <para>
        /// 		 DataRow con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Objeto clacie
        /// </returns>
        internal EntClaCie crearObjetoRevisado(DataRow row)
        {
            var obj = new EntClaCie();
            if (row.Table.Columns.Contains(EntClaCie.Fields.codcie.ToString()))
                obj.codcie = GetColumnType(row[EntClaCie.Fields.codcie.ToString()], EntClaCie.Fields.codcie);
            if (row.Table.Columns.Contains(EntClaCie.Fields.desccie.ToString()))
                obj.desccie = GetColumnType(row[EntClaCie.Fields.desccie.ToString()], EntClaCie.Fields.desccie);
            return obj;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtclacie" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Lista de Objetos clacie
        /// </returns>
        internal List<EntClaCie> crearLista(DataTable dtclacie)
        {
            var list = new List<EntClaCie>();

            foreach (DataRow row in dtclacie.Rows)
            {
                var obj = crearObjeto(row);
                list.Add(obj);
            }
            return list;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
        /// </summary>
        /// <param name="dtclacie" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Lista de Objetos clacie
        /// </returns>
        internal List<EntClaCie> crearListaRevisada(DataTable dtclacie)
        {
            List<EntClaCie> list = new List<EntClaCie>();

            foreach (DataRow row in dtclacie.Rows)
            {
                var obj = crearObjetoRevisado(row);
                list.Add(obj);
            }
            return list;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtclacie" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Cola de Objetos clacie
        /// </returns>
        internal Queue<EntClaCie> crearCola(DataTable dtclacie)
        {
            Queue<EntClaCie> cola = new Queue<EntClaCie>();

            EntClaCie obj = new EntClaCie();
            foreach (DataRow row in dtclacie.Rows)
            {
                obj = crearObjeto(row);
                cola.Enqueue(obj);
            }
            return cola;
        }

        /// <summary>
        /// 	 Funcion que crea una Lista de objetos a partir de un DataTable
        /// </summary>
        /// <param name="dtclacie" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Pila de Objetos clacie
        /// </returns>
        internal Stack<EntClaCie> crearPila(DataTable dtclacie)
        {
            Stack<EntClaCie> pila = new Stack<EntClaCie>();

            foreach (DataRow row in dtclacie.Rows)
            {
                var obj = crearObjeto(row);
                pila.Push(obj);
            }
            return pila;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable
        /// </summary>
        /// <param name="dtclacie" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Diccionario de Objetos clacie
        /// </returns>
        internal Dictionary<String, EntClaCie> crearDiccionario(DataTable dtclacie)
        {
            Dictionary<String, EntClaCie> miDic = new Dictionary<String, EntClaCie>();

            foreach (DataRow row in dtclacie.Rows)
            {
                var obj = crearObjeto(row);
                miDic.Add(obj.codcie.ToString(), obj);
            }
            return miDic;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable
        /// </summary>
        /// <param name="dtclacie" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 HashTable de Objetos clacie
        /// </returns>
        internal Hashtable crearHashTable(DataTable dtclacie)
        {
            Hashtable miTabla = new Hashtable();

            foreach (DataRow row in dtclacie.Rows)
            {
                var obj = crearObjeto(row);
                miTabla.Add(obj.codcie.ToString(), obj);
            }
            return miTabla;
        }

        /// <summary>
        /// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
        /// </summary>
        /// <param name="dtclacie" type="System.Data.DateTable">
        ///     <para>
        /// 		 DataTable con el conjunto de Datos recuperados
        ///     </para>
        /// </param>
        /// <returns>
        /// 	 Diccionario de Objetos clacie
        /// </returns>
        internal Dictionary<String, EntClaCie> crearDiccionarioRevisado(DataTable dtclacie)
        {
            Dictionary<String, EntClaCie> miDic = new Dictionary<String, EntClaCie>();

            foreach (DataRow row in dtclacie.Rows)
            {
                var obj = crearObjetoRevisado(row);
                miDic.Add(obj.codcie.ToString(), obj);
            }
            return miDic;
        }

        internal Dictionary<String, EntClaCie> crearDiccionario(DataTable dtclacie, EntClaCie.Fields dicKey)
        {
            Dictionary<String, EntClaCie> miDic = new Dictionary<String, EntClaCie>();

            foreach (DataRow row in dtclacie.Rows)
            {
                var obj = crearObjeto(row);

                var nameOfProperty = dicKey.ToString();
                var propertyInfo = obj.GetType().GetProperty(nameOfProperty);
                var value = propertyInfo.GetValue(obj, null);

                miDic.Add(value.ToString(), obj);
            }
            return miDic;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        protected void Finalize()
        {
            Dispose();
        }

        #endregion
    }
}