#region 
/***********************************************************************************************************
	NOMBRE:       RnSegPaginas
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla segpaginas

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/04/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Interface;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Modelo
{
	public class RnSegPaginas: ISegPaginas
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region ISegPaginas Members

		#region Reflection

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string GetTableScript()
		{
			TableClass tabla = new TableClass(typeof(EntSegPaginas));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, EntSegPaginas.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntSegPaginas).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="strField">Nombre de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, string strField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntSegPaginas).GetProperty(strField).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

/// <summary>
/// Inserta una valor a una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">Es el nombre de la propiedad</param>
/// <param name="dynValor">Es el valor que se insertara a la propiedad</param>
public void SetDato(ref EntSegPaginas obj, string strPropiedad, dynamic dynValor)
{
	if (obj == null) throw new ArgumentNullException();
	obj.GetType().GetProperty(strPropiedad).SetValue(obj, GetColumnType(dynValor, strPropiedad), null);
}

/// <summary>
/// Obtiene el valor de una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">El nombre de la propiedad de la que se obtendra el valor</param>
/// <returns>Devuelve el valor del a propiedad seleccionada</returns>
public dynamic GetDato(ref EntSegPaginas obj, string strPropiedad)
{
	if (obj == null) return null;
	var propertyInfo = obj.GetType().GetProperty(strPropiedad);
	return GetColumnType(propertyInfo.GetValue(obj, null), strPropiedad);
}

		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla segpaginas a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla segpaginas
		/// </returns>
		public string CreatePk(string[] args)
		{
			return args[0];
		}
		
		#endregion 

		#region ObtenerObjeto

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntSegPaginas a partir de la llave primaria
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public EntSegPaginas ObtenerObjeto(int intpaginaspg)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntSegPaginas.Fields.paginaspg.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intpaginaspg);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntSegPaginas a partir del usuario que inserta
		/// </summary>
		/// <param name="strUsuCre">Login o nombre de usuario</param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public EntSegPaginas ObtenerObjetoInsertado(string strUsuCre)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntSegPaginas.Fields.usucrespg.ToString());
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + strUsuCre + "'");
			
			int iIdInsertado = FuncionesMax(EntSegPaginas.Fields.paginaspg, arrColumnasWhere, arrValoresWhere);
			
			return ObtenerObjeto(iIdInsertado);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public EntSegPaginas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public EntSegPaginas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntSegPaginas obj = new EntSegPaginas();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public EntSegPaginas ObtenerObjeto(Hashtable htbFiltro)
		{
			return ObtenerObjeto(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public EntSegPaginas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntSegPaginas obj = new EntSegPaginas();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public EntSegPaginas ObtenerObjeto(EntSegPaginas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public EntSegPaginas ObtenerObjeto(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo EntSegPaginas a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo EntSegPaginas
		/// </returns>
		public EntSegPaginas ObtenerObjeto(int intpaginaspg, ref CTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntSegPaginas.Fields.paginaspg.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(intpaginaspg);
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public EntSegPaginas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public EntSegPaginas ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerObjeto(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public EntSegPaginas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public EntSegPaginas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					EntSegPaginas obj = new EntSegPaginas();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public EntSegPaginas ObtenerObjeto(EntSegPaginas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public EntSegPaginas ObtenerObjeto(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion

		#region ObtenerLista

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntSegPaginas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerLista(EntSegPaginas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerLista(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntSegPaginas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerLista(EntSegPaginas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerLista(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerLista(Hashtable htbFiltro)
		{
			return ObtenerLista(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntSegPaginas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerLista(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntSegPaginas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntSegPaginas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntSegPaginas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, EntSegPaginas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntSegPaginas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntSegPaginas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, EntSegPaginas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerCola y Obtener Pila

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntSegPaginas> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntSegPaginas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntSegPaginas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntSegPaginas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntSegPaginas> ObtenerCola(EntSegPaginas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntSegPaginas> ObtenerCola(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntSegPaginas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntSegPaginas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntSegPaginas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntSegPaginas> ObtenerCola(EntSegPaginas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntSegPaginas> ObtenerCola(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntSegPaginas> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntSegPaginas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntSegPaginas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntSegPaginas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntSegPaginas> ObtenerPila(EntSegPaginas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntSegPaginas> ObtenerPila(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntSegPaginas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntSegPaginas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntSegPaginas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntSegPaginas> ObtenerPila(EntSegPaginas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntSegPaginas> ObtenerPila(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerDataTable

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla segpaginas
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(EntSegPaginas.Fields.paginaspg.ToString(),typeof(EntSegPaginas).GetProperty(EntSegPaginas.Fields.paginaspg.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntSegPaginas.Fields.aplicacionsap.ToString(),typeof(EntSegPaginas).GetProperty(EntSegPaginas.Fields.aplicacionsap.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntSegPaginas.Fields.paginapadrespg.ToString(),typeof(EntSegPaginas).GetProperty(EntSegPaginas.Fields.paginapadrespg.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntSegPaginas.Fields.nombrespg.ToString(),typeof(EntSegPaginas).GetProperty(EntSegPaginas.Fields.nombrespg.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntSegPaginas.Fields.nombremenuspg.ToString(),typeof(EntSegPaginas).GetProperty(EntSegPaginas.Fields.nombremenuspg.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntSegPaginas.Fields.descripcionspg.ToString(),typeof(EntSegPaginas).GetProperty(EntSegPaginas.Fields.descripcionspg.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntSegPaginas.Fields.prioridadspg.ToString(),typeof(EntSegPaginas).GetProperty(EntSegPaginas.Fields.prioridadspg.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntSegPaginas.Fields.apiestadospg.ToString(),typeof(EntSegPaginas).GetProperty(EntSegPaginas.Fields.apiestadospg.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntSegPaginas.Fields.apitransaccionspg.ToString(),typeof(EntSegPaginas).GetProperty(EntSegPaginas.Fields.apitransaccionspg.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntSegPaginas.Fields.usucrespg.ToString(),typeof(EntSegPaginas).GetProperty(EntSegPaginas.Fields.usucrespg.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntSegPaginas.Fields.feccrespg.ToString(),typeof(EntSegPaginas).GetProperty(EntSegPaginas.Fields.feccrespg.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntSegPaginas.Fields.usumodspg.ToString(),typeof(EntSegPaginas).GetProperty(EntSegPaginas.Fields.usumodspg.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntSegPaginas.Fields.fecmodspg.ToString(),typeof(EntSegPaginas).GetProperty(EntSegPaginas.Fields.fecmodspg.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntSegPaginas.Fields.iconospg.ToString(),typeof(EntSegPaginas).GetProperty(EntSegPaginas.Fields.iconospg.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una segpaginas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla segpaginas
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable ObtenerDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE segpaginas
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable ObtenerDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de segpaginas
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableOr(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntSegPaginas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntSegPaginas.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegPaginas.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegPaginas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region ObtenerDiccionario

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntSegPaginas> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntSegPaginas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntSegPaginas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntSegPaginas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntSegPaginas> ObtenerDiccionario(EntSegPaginas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntSegPaginas> ObtenerDiccionario(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntSegPaginas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntSegPaginas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntSegPaginas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntSegPaginas> ObtenerDiccionario(EntSegPaginas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntSegPaginas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntSegPaginas> ObtenerDiccionario(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		public Dictionary<String, EntSegPaginas> ObtenerDiccionarioKey(EntSegPaginas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntSegPaginas> ObtenerDiccionarioKey(String strParamAdic, EntSegPaginas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdic, dicKey);
		}
		
		public Dictionary<String, EntSegPaginas> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, EntSegPaginas.Fields dicKey)
		{
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, "", dicKey);
		}
		
		public Dictionary<String, EntSegPaginas> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, EntSegPaginas.Fields dicKey)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table, dicKey);
				}
				else
					return new Dictionary<string, EntSegPaginas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		public Dictionary<String, EntSegPaginas> ObtenerDiccionarioKey(EntSegPaginas.Fields searchField, object searchValue, EntSegPaginas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntSegPaginas> ObtenerDiccionarioKey(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales, EntSegPaginas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey);
		}
		

		#endregion 

		#region ObjetoASp

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla segpaginas
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntSegPaginas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.paginaspg);
				arrValoresParam.Add(obj.aplicacionsap == null ? null : "'" + obj.aplicacionsap + "'");
				arrValoresParam.Add(obj.paginapadrespg);
				arrValoresParam.Add(obj.nombrespg == null ? null : "'" + obj.nombrespg + "'");
				arrValoresParam.Add(obj.nombremenuspg == null ? null : "'" + obj.nombremenuspg + "'");
				arrValoresParam.Add(obj.descripcionspg == null ? null : "'" + obj.descripcionspg + "'");
				arrValoresParam.Add(obj.prioridadspg);
				arrValoresParam.Add(obj.apiestadospg == null ? null : "'" + obj.apiestadospg + "'");
				arrValoresParam.Add(obj.apitransaccionspg == null ? null : "'" + obj.apitransaccionspg + "'");
				arrValoresParam.Add(obj.usucrespg == null ? null : "'" + obj.usucrespg + "'");
				arrValoresParam.Add(obj.feccrespg == null ? null : "'" + Convert.ToDateTime(obj.feccrespg).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumodspg == null ? null : "'" + obj.usumodspg + "'");
				arrValoresParam.Add(obj.fecmodspg == null ? null : "'" + Convert.ToDateTime(obj.fecmodspg).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.iconospg == null ? null : "'" + obj.iconospg + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla segpaginas
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntSegPaginas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrNombreParam.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.paginaspg);
				arrValoresParam.Add(obj.aplicacionsap == null ? null : "'" + obj.aplicacionsap + "'");
				arrValoresParam.Add(obj.paginapadrespg);
				arrValoresParam.Add(obj.nombrespg == null ? null : "'" + obj.nombrespg + "'");
				arrValoresParam.Add(obj.nombremenuspg == null ? null : "'" + obj.nombremenuspg + "'");
				arrValoresParam.Add(obj.descripcionspg == null ? null : "'" + obj.descripcionspg + "'");
				arrValoresParam.Add(obj.prioridadspg);
				arrValoresParam.Add(obj.apiestadospg == null ? null : "'" + obj.apiestadospg + "'");
				arrValoresParam.Add(obj.apitransaccionspg == null ? null : "'" + obj.apitransaccionspg + "'");
				arrValoresParam.Add(obj.usucrespg == null ? null : "'" + obj.usucrespg + "'");
				arrValoresParam.Add(obj.feccrespg == null ? null : "'" + Convert.ToDateTime(obj.feccrespg).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumodspg == null ? null : "'" + obj.usumodspg + "'");
				arrValoresParam.Add(obj.fecmodspg == null ? null : "'" + Convert.ToDateTime(obj.fecmodspg).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.iconospg == null ? null : "'" + obj.iconospg + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region FuncionesAgregadas

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntSegPaginas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntSegPaginas.Fields refField, EntSegPaginas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntSegPaginas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntSegPaginas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntSegPaginas.Fields refField, EntSegPaginas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntSegPaginas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntSegPaginas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntSegPaginas.Fields refField, EntSegPaginas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntSegPaginas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntSegPaginas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntSegPaginas.Fields refField, EntSegPaginas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntSegPaginas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntSegPaginas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntSegPaginas.Fields refField, EntSegPaginas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntSegPaginas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntSegPaginas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs SP

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionsegpaginas
		/// </returns>
		public bool Insert(EntSegPaginas obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("paginaspg");
				arrValoresParam.Add(null);
				arrNombreParam.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrValoresParam.Add(obj.aplicacionsap);
				
				arrNombreParam.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrValoresParam.Add(obj.paginapadrespg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrValoresParam.Add(obj.nombrespg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrValoresParam.Add(obj.nombremenuspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrValoresParam.Add(obj.descripcionspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrValoresParam.Add(obj.prioridadspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrValoresParam.Add(obj.usucrespg);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.SegPaginas.SpSpgIns.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionsegpaginas
		/// </returns>
		public bool Insert(EntSegPaginas obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("paginaspg");
				arrValoresParam.Add("");
				arrNombreParam.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrValoresParam.Add(obj.aplicacionsap);
				
				arrNombreParam.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrValoresParam.Add(obj.paginapadrespg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrValoresParam.Add(obj.nombrespg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrValoresParam.Add(obj.nombremenuspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrValoresParam.Add(obj.descripcionspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrValoresParam.Add(obj.prioridadspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrValoresParam.Add(obj.usucrespg);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.SegPaginas.SpSpgIns.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor que indica la cantidad de registros actualizados en segpaginas
		/// </returns>
		public int Update(EntSegPaginas obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrValoresParam.Add(obj.aplicacionsap);
				
				arrNombreParam.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrValoresParam.Add(obj.paginaspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrValoresParam.Add(obj.paginapadrespg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrValoresParam.Add(obj.nombrespg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrValoresParam.Add(obj.nombremenuspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrValoresParam.Add(obj.descripcionspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrValoresParam.Add(obj.prioridadspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrValoresParam.Add(obj.apitransaccionspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrValoresParam.Add(obj.usumodspg);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.SegPaginas.SpSpgUpd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionsegpaginas
		/// </returns>
		public int Update(EntSegPaginas obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrValoresParam.Add(obj.aplicacionsap);
				
				arrNombreParam.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrValoresParam.Add(obj.paginaspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrValoresParam.Add(obj.paginapadrespg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrValoresParam.Add(obj.nombrespg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrValoresParam.Add(obj.nombremenuspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrValoresParam.Add(obj.descripcionspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrValoresParam.Add(obj.prioridadspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrValoresParam.Add(obj.apitransaccionspg);
				
				arrNombreParam.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrValoresParam.Add(obj.usumodspg);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.SegPaginas.SpSpgUpd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionsegpaginas
		/// </returns>
		public int Delete(EntSegPaginas obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrValoresParam.Add(obj.aplicacionsap);
				
				arrNombreParam.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrValoresParam.Add(obj.paginaspg);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.SegPaginas.SpSpgDel.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionsegpaginas
		/// </returns>
		public int Delete(EntSegPaginas obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrValoresParam.Add(obj.aplicacionsap);
				
				arrNombreParam.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrValoresParam.Add(obj.paginaspg);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.SegPaginas.SpSpgDel.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionsegpaginas
		/// </returns>
		public int InsertUpdate(EntSegPaginas obj)
		{
			try
			{
				bool esInsertar = true;
				
					esInsertar = (esInsertar && (obj.paginaspg == null));
				
				if (esInsertar)
					return Insert(obj) ? 1 : 0;
				else
					return Update(obj);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionsegpaginas
		/// </returns>
		public int InsertUpdate(EntSegPaginas obj, ref CTrans localTrans)
		{
			try
			{
				bool esInsertar = false;
				
					esInsertar = (esInsertar && (obj.paginaspg == null));
				
				if (esInsertar)
					return Insert(obj, ref localTrans) ? 1 : 0;
				else
					return Update(obj, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs Query

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionsegpaginas
		/// </returns>
		public bool InsertQuery(EntSegPaginas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.paginaspg);
				arrValores.Add(obj.aplicacionsap == null ? null : "'" + obj.aplicacionsap + "'");
				arrValores.Add(obj.paginapadrespg);
				arrValores.Add(obj.nombrespg == null ? null : "'" + obj.nombrespg + "'");
				arrValores.Add(obj.nombremenuspg == null ? null : "'" + obj.nombremenuspg + "'");
				arrValores.Add(obj.descripcionspg == null ? null : "'" + obj.descripcionspg + "'");
				arrValores.Add(obj.prioridadspg);
				arrValores.Add(obj.apiestadospg == null ? null : "'" + obj.apiestadospg + "'");
				arrValores.Add(obj.usucrespg == null ? null : "'" + obj.usucrespg + "'");
				arrValores.Add(obj.iconospg == null ? null : "'" + obj.iconospg + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntSegPaginas.StrNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionsegpaginas
		/// </returns>
		public bool InsertQuery(EntSegPaginas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.paginaspg);
				arrValores.Add(obj.aplicacionsap == null ? null : "'" + obj.aplicacionsap + "'");
				arrValores.Add(obj.paginapadrespg);
				arrValores.Add(obj.nombrespg == null ? null : "'" + obj.nombrespg + "'");
				arrValores.Add(obj.nombremenuspg == null ? null : "'" + obj.nombremenuspg + "'");
				arrValores.Add(obj.descripcionspg == null ? null : "'" + obj.descripcionspg + "'");
				arrValores.Add(obj.prioridadspg);
				arrValores.Add(obj.apiestadospg == null ? null : "'" + obj.apiestadospg + "'");
				arrValores.Add(obj.usucrespg == null ? null : "'" + obj.usucrespg + "'");
				arrValores.Add(obj.iconospg == null ? null : "'" + obj.iconospg + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntSegPaginas.StrNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionsegpaginas
		/// </returns>
		public bool InsertQueryIdentity(EntSegPaginas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.paginaspg);
				arrValores.Add(obj.aplicacionsap == null ? null : "'" + obj.aplicacionsap + "'");
				arrValores.Add(obj.paginapadrespg);
				arrValores.Add(obj.nombrespg == null ? null : "'" + obj.nombrespg + "'");
				arrValores.Add(obj.nombremenuspg == null ? null : "'" + obj.nombremenuspg + "'");
				arrValores.Add(obj.descripcionspg == null ? null : "'" + obj.descripcionspg + "'");
				arrValores.Add(obj.prioridadspg);
				arrValores.Add(obj.apiestadospg == null ? null : "'" + obj.apiestadospg + "'");
				arrValores.Add(obj.usucrespg == null ? null : "'" + obj.usucrespg + "'");
				arrValores.Add(obj.iconospg == null ? null : "'" + obj.iconospg + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntSegPaginas.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.paginaspg = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionsegpaginas
		/// </returns>
		public bool InsertQueryIdentity(EntSegPaginas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.paginaspg);
				arrValores.Add(obj.aplicacionsap == null ? null : "'" + obj.aplicacionsap + "'");
				arrValores.Add(obj.paginapadrespg);
				arrValores.Add(obj.nombrespg == null ? null : "'" + obj.nombrespg + "'");
				arrValores.Add(obj.nombremenuspg == null ? null : "'" + obj.nombremenuspg + "'");
				arrValores.Add(obj.descripcionspg == null ? null : "'" + obj.descripcionspg + "'");
				arrValores.Add(obj.prioridadspg);
				arrValores.Add(obj.apiestadospg == null ? null : "'" + obj.apiestadospg + "'");
				arrValores.Add(obj.usucrespg == null ? null : "'" + obj.usucrespg + "'");
				arrValores.Add(obj.iconospg == null ? null : "'" + obj.iconospg + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntSegPaginas.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.paginaspg = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionsegpaginas
		/// </returns>
		public int UpdateQueryAll(EntSegPaginas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.aplicacionsap == null ? null : "'" + obj.aplicacionsap + "'");
				arrValores.Add(obj.paginapadrespg);
				arrValores.Add(obj.nombrespg == null ? null : "'" + obj.nombrespg + "'");
				arrValores.Add(obj.nombremenuspg == null ? null : "'" + obj.nombremenuspg + "'");
				arrValores.Add(obj.descripcionspg == null ? null : "'" + obj.descripcionspg + "'");
				arrValores.Add(obj.prioridadspg);
				arrValores.Add(obj.apiestadospg == null ? null : "'" + obj.apiestadospg + "'");
				arrValores.Add(obj.apitransaccionspg == null ? null : "'" + obj.apitransaccionspg + "'");
				arrValores.Add(obj.usumodspg == null ? null : "'" + obj.usumodspg + "'");
				arrValores.Add(obj.iconospg == null ? null : "'" + obj.iconospg + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntSegPaginas.Fields.paginaspg.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.paginaspg);

			
				CConn local = new CConn();
				return local.UpdateBd(EntSegPaginas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla segpaginas a partir de una clase del tipo esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQueryAll(EntSegPaginas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.aplicacionsap == null ? null : "'" + obj.aplicacionsap + "'");
				arrValores.Add(obj.paginapadrespg);
				arrValores.Add(obj.nombrespg == null ? null : "'" + obj.nombrespg + "'");
				arrValores.Add(obj.nombremenuspg == null ? null : "'" + obj.nombremenuspg + "'");
				arrValores.Add(obj.descripcionspg == null ? null : "'" + obj.descripcionspg + "'");
				arrValores.Add(obj.prioridadspg);
				arrValores.Add(obj.apiestadospg == null ? null : "'" + obj.apiestadospg + "'");
				arrValores.Add(obj.apitransaccionspg == null ? null : "'" + obj.apitransaccionspg + "'");
				arrValores.Add(obj.usumodspg == null ? null : "'" + obj.usumodspg + "'");
				arrValores.Add(obj.iconospg == null ? null : "'" + obj.iconospg + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntSegPaginas.Fields.paginaspg.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.paginaspg);

			
				CConn local = new CConn();
				return local.UpdateBd(EntSegPaginas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla segpaginas a partir de una clase del tipo Esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionsegpaginas
		/// </returns>
		public int UpdateQuery(EntSegPaginas obj)
		{
			try
			{
				//Obtenemos el Objeto original
				EntSegPaginas objOriginal = this.ObtenerObjeto(obj.paginaspg);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.aplicacionsap != objOriginal.aplicacionsap )
				{
					arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
					arrValores.Add(obj.aplicacionsap == null ? null : "'" + obj.aplicacionsap + "'");
				}
				if(obj.paginapadrespg != objOriginal.paginapadrespg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
					arrValores.Add(obj.paginapadrespg);
				}
				if(obj.nombrespg != objOriginal.nombrespg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
					arrValores.Add(obj.nombrespg == null ? null : "'" + obj.nombrespg + "'");
				}
				if(obj.nombremenuspg != objOriginal.nombremenuspg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
					arrValores.Add(obj.nombremenuspg == null ? null : "'" + obj.nombremenuspg + "'");
				}
				if(obj.descripcionspg != objOriginal.descripcionspg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
					arrValores.Add(obj.descripcionspg == null ? null : "'" + obj.descripcionspg + "'");
				}
				if(obj.prioridadspg != objOriginal.prioridadspg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
					arrValores.Add(obj.prioridadspg);
				}
				if(obj.apiestadospg != objOriginal.apiestadospg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
					arrValores.Add(obj.apiestadospg == null ? null : "'" + obj.apiestadospg + "'");
				}
				if(obj.apitransaccionspg != objOriginal.apitransaccionspg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
					arrValores.Add(obj.apitransaccionspg == null ? null : "'" + obj.apitransaccionspg + "'");
				}
				if(obj.usumodspg != objOriginal.usumodspg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
					arrValores.Add(obj.usumodspg == null ? null : "'" + obj.usumodspg + "'");
				}
				if(obj.iconospg != objOriginal.iconospg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
					arrValores.Add(obj.iconospg == null ? null : "'" + obj.iconospg + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntSegPaginas.Fields.paginaspg.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.paginaspg);

			
				CConn local = new CConn();
				return local.UpdateBd(EntSegPaginas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla segpaginas a partir de una clase del tipo esegpaginas
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQuery(EntSegPaginas obj, ref CTrans localTrans)
		{
			try
			{
				//Obtenemos el Objeto original
				EntSegPaginas objOriginal = this.ObtenerObjeto(obj.paginaspg, ref localTrans);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.aplicacionsap != objOriginal.aplicacionsap )
				{
					arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
					arrValores.Add(obj.aplicacionsap == null ? null : "'" + obj.aplicacionsap + "'");
				}
				if(obj.paginapadrespg != objOriginal.paginapadrespg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
					arrValores.Add(obj.paginapadrespg);
				}
				if(obj.nombrespg != objOriginal.nombrespg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
					arrValores.Add(obj.nombrespg == null ? null : "'" + obj.nombrespg + "'");
				}
				if(obj.nombremenuspg != objOriginal.nombremenuspg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
					arrValores.Add(obj.nombremenuspg == null ? null : "'" + obj.nombremenuspg + "'");
				}
				if(obj.descripcionspg != objOriginal.descripcionspg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
					arrValores.Add(obj.descripcionspg == null ? null : "'" + obj.descripcionspg + "'");
				}
				if(obj.prioridadspg != objOriginal.prioridadspg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
					arrValores.Add(obj.prioridadspg);
				}
				if(obj.apiestadospg != objOriginal.apiestadospg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
					arrValores.Add(obj.apiestadospg == null ? null : "'" + obj.apiestadospg + "'");
				}
				if(obj.apitransaccionspg != objOriginal.apitransaccionspg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
					arrValores.Add(obj.apitransaccionspg == null ? null : "'" + obj.apitransaccionspg + "'");
				}
				if(obj.usumodspg != objOriginal.usumodspg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
					arrValores.Add(obj.usumodspg == null ? null : "'" + obj.usumodspg + "'");
				}
				if(obj.iconospg != objOriginal.iconospg )
				{
					arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
					arrValores.Add(obj.iconospg == null ? null : "'" + obj.iconospg + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntSegPaginas.Fields.paginaspg.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.paginaspg);

			
				CConn local = new CConn();
				return local.UpdateBd(EntSegPaginas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla segpaginas a partir de una clase del tipo EntSegPaginas y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.EntSegPaginas">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionsegpaginas
		/// </returns>
		public int DeleteQuery(EntSegPaginas obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntSegPaginas.Fields.paginaspg.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.paginaspg);

			
				CConn local = new CConn();
				return local.DeleteBd(EntSegPaginas.StrNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla segpaginas a partir de una clase del tipo EntSegPaginas y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.esegpaginas">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionsegpaginas
		/// </returns>
		public int DeleteQuery(EntSegPaginas obj, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntSegPaginas.Fields.paginaspg.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.paginaspg);

			
				CConn local = new CConn();
				return local.DeleteBd(EntSegPaginas.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla segpaginas a partir de una clase del tipo esegpaginas
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionsegpaginas
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd("segpaginas", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla segpaginas a partir de una clase del tipo esegpaginas
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion segpaginas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionsegpaginas
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd(EntSegPaginas.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region Llenado de elementos

		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, String textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, EntSegPaginas.Fields searchField, object searchValue)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, String textField, EntSegPaginas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, String textField, EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.DataValueField = table.Columns[0].ColumnName;
					cmb.DataTextField = table.Columns[1].ColumnName;
					cmb.DataSource = table;
					cmb.DataBind();

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntSegPaginas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntSegPaginas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegPaginas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegPaginas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridViewOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, EntSegPaginas.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, String textField, EntSegPaginas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, String textField, EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla segpaginas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla segpaginas
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntSegPaginas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntSegPaginas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntSegPaginas.Fields.paginaspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.aplicacionsap.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.paginapadrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.nombremenuspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.descripcionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.prioridadspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apiestadospg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.apitransaccionspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usucrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.feccrespg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.usumodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.fecmodspg.ToString());
				arrColumnas.Add(EntSegPaginas.Fields.iconospg.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegPaginas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntSegPaginas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegPaginas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla segpaginas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntSegPaginas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 


		#endregion

		#region Funciones Internas

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto segpaginas
		/// </returns>
		internal EntSegPaginas crearObjeto(DataRow row)
		{
			var obj = new EntSegPaginas();
			obj.paginaspg = GetColumnType(row[EntSegPaginas.Fields.paginaspg.ToString()], EntSegPaginas.Fields.paginaspg);
			obj.aplicacionsap = GetColumnType(row[EntSegPaginas.Fields.aplicacionsap.ToString()], EntSegPaginas.Fields.aplicacionsap);
			obj.paginapadrespg = GetColumnType(row[EntSegPaginas.Fields.paginapadrespg.ToString()], EntSegPaginas.Fields.paginapadrespg);
			obj.nombrespg = GetColumnType(row[EntSegPaginas.Fields.nombrespg.ToString()], EntSegPaginas.Fields.nombrespg);
			obj.nombremenuspg = GetColumnType(row[EntSegPaginas.Fields.nombremenuspg.ToString()], EntSegPaginas.Fields.nombremenuspg);
			obj.descripcionspg = GetColumnType(row[EntSegPaginas.Fields.descripcionspg.ToString()], EntSegPaginas.Fields.descripcionspg);
			obj.prioridadspg = GetColumnType(row[EntSegPaginas.Fields.prioridadspg.ToString()], EntSegPaginas.Fields.prioridadspg);
			obj.apiestadospg = GetColumnType(row[EntSegPaginas.Fields.apiestadospg.ToString()], EntSegPaginas.Fields.apiestadospg);
			obj.apitransaccionspg = GetColumnType(row[EntSegPaginas.Fields.apitransaccionspg.ToString()], EntSegPaginas.Fields.apitransaccionspg);
			obj.usucrespg = GetColumnType(row[EntSegPaginas.Fields.usucrespg.ToString()], EntSegPaginas.Fields.usucrespg);
			obj.feccrespg = GetColumnType(row[EntSegPaginas.Fields.feccrespg.ToString()], EntSegPaginas.Fields.feccrespg);
			obj.usumodspg = GetColumnType(row[EntSegPaginas.Fields.usumodspg.ToString()], EntSegPaginas.Fields.usumodspg);
			obj.fecmodspg = GetColumnType(row[EntSegPaginas.Fields.fecmodspg.ToString()], EntSegPaginas.Fields.fecmodspg);
			obj.iconospg = GetColumnType(row[EntSegPaginas.Fields.iconospg.ToString()], EntSegPaginas.Fields.iconospg);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto segpaginas
		/// </returns>
		internal EntSegPaginas crearObjetoRevisado(DataRow row)
		{
			var obj = new EntSegPaginas();
			if (row.Table.Columns.Contains(EntSegPaginas.Fields.paginaspg.ToString()))
				obj.paginaspg = GetColumnType(row[EntSegPaginas.Fields.paginaspg.ToString()], EntSegPaginas.Fields.paginaspg);
			if (row.Table.Columns.Contains(EntSegPaginas.Fields.aplicacionsap.ToString()))
				obj.aplicacionsap = GetColumnType(row[EntSegPaginas.Fields.aplicacionsap.ToString()], EntSegPaginas.Fields.aplicacionsap);
			if (row.Table.Columns.Contains(EntSegPaginas.Fields.paginapadrespg.ToString()))
				obj.paginapadrespg = GetColumnType(row[EntSegPaginas.Fields.paginapadrespg.ToString()], EntSegPaginas.Fields.paginapadrespg);
			if (row.Table.Columns.Contains(EntSegPaginas.Fields.nombrespg.ToString()))
				obj.nombrespg = GetColumnType(row[EntSegPaginas.Fields.nombrespg.ToString()], EntSegPaginas.Fields.nombrespg);
			if (row.Table.Columns.Contains(EntSegPaginas.Fields.nombremenuspg.ToString()))
				obj.nombremenuspg = GetColumnType(row[EntSegPaginas.Fields.nombremenuspg.ToString()], EntSegPaginas.Fields.nombremenuspg);
			if (row.Table.Columns.Contains(EntSegPaginas.Fields.descripcionspg.ToString()))
				obj.descripcionspg = GetColumnType(row[EntSegPaginas.Fields.descripcionspg.ToString()], EntSegPaginas.Fields.descripcionspg);
			if (row.Table.Columns.Contains(EntSegPaginas.Fields.prioridadspg.ToString()))
				obj.prioridadspg = GetColumnType(row[EntSegPaginas.Fields.prioridadspg.ToString()], EntSegPaginas.Fields.prioridadspg);
			if (row.Table.Columns.Contains(EntSegPaginas.Fields.apiestadospg.ToString()))
				obj.apiestadospg = GetColumnType(row[EntSegPaginas.Fields.apiestadospg.ToString()], EntSegPaginas.Fields.apiestadospg);
			if (row.Table.Columns.Contains(EntSegPaginas.Fields.apitransaccionspg.ToString()))
				obj.apitransaccionspg = GetColumnType(row[EntSegPaginas.Fields.apitransaccionspg.ToString()], EntSegPaginas.Fields.apitransaccionspg);
			if (row.Table.Columns.Contains(EntSegPaginas.Fields.usucrespg.ToString()))
				obj.usucrespg = GetColumnType(row[EntSegPaginas.Fields.usucrespg.ToString()], EntSegPaginas.Fields.usucrespg);
			if (row.Table.Columns.Contains(EntSegPaginas.Fields.feccrespg.ToString()))
				obj.feccrespg = GetColumnType(row[EntSegPaginas.Fields.feccrespg.ToString()], EntSegPaginas.Fields.feccrespg);
			if (row.Table.Columns.Contains(EntSegPaginas.Fields.usumodspg.ToString()))
				obj.usumodspg = GetColumnType(row[EntSegPaginas.Fields.usumodspg.ToString()], EntSegPaginas.Fields.usumodspg);
			if (row.Table.Columns.Contains(EntSegPaginas.Fields.fecmodspg.ToString()))
				obj.fecmodspg = GetColumnType(row[EntSegPaginas.Fields.fecmodspg.ToString()], EntSegPaginas.Fields.fecmodspg);
			if (row.Table.Columns.Contains(EntSegPaginas.Fields.iconospg.ToString()))
				obj.iconospg = GetColumnType(row[EntSegPaginas.Fields.iconospg.ToString()], EntSegPaginas.Fields.iconospg);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtsegpaginas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos segpaginas
		/// </returns>
		internal List<EntSegPaginas> crearLista(DataTable dtsegpaginas)
		{
			var list = new List<EntSegPaginas>();
			
			foreach (DataRow row in dtsegpaginas.Rows)
			{
				var obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtsegpaginas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos segpaginas
		/// </returns>
		internal List<EntSegPaginas> crearListaRevisada(DataTable dtsegpaginas)
		{
			List<EntSegPaginas> list = new List<EntSegPaginas>();
			
			foreach (DataRow row in dtsegpaginas.Rows)
			{
				var obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtsegpaginas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos segpaginas
		/// </returns>
		internal Queue<EntSegPaginas> crearCola(DataTable dtsegpaginas)
		{
			Queue<EntSegPaginas> cola = new Queue<EntSegPaginas>();
			
			EntSegPaginas obj = new EntSegPaginas();
			foreach (DataRow row in dtsegpaginas.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtsegpaginas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos segpaginas
		/// </returns>
		internal Stack<EntSegPaginas> crearPila(DataTable dtsegpaginas)
		{
			Stack<EntSegPaginas> pila = new Stack<EntSegPaginas>();
			
			foreach (DataRow row in dtsegpaginas.Rows)
			{
				var obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtsegpaginas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos segpaginas
		/// </returns>
		internal Dictionary<String, EntSegPaginas> crearDiccionario(DataTable dtsegpaginas)
		{
			Dictionary<String, EntSegPaginas>  miDic = new Dictionary<String, EntSegPaginas>();
			
			foreach (DataRow row in dtsegpaginas.Rows)
			{
				var obj = crearObjeto(row);
				miDic.Add(obj.paginaspg.ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtsegpaginas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 HashTable de Objetos segpaginas
		/// </returns>
		internal Hashtable crearHashTable(DataTable dtsegpaginas)
		{
			Hashtable miTabla = new Hashtable();
			
			foreach (DataRow row in dtsegpaginas.Rows)
			{
				var obj = crearObjeto(row);
				miTabla.Add(obj.paginaspg.ToString(), obj);
			}
			return miTabla;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtsegpaginas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos segpaginas
		/// </returns>
		internal Dictionary<String, EntSegPaginas> crearDiccionarioRevisado(DataTable dtsegpaginas)
		{
			Dictionary<String, EntSegPaginas>  miDic = new Dictionary<String, EntSegPaginas>();
			
			foreach (DataRow row in dtsegpaginas.Rows)
			{
				var obj = crearObjetoRevisado(row);
				miDic.Add(obj.paginaspg.ToString(), obj);
			}
			return miDic;
		}
		
		internal Dictionary<String, EntSegPaginas> crearDiccionario(DataTable dtsegpaginas, EntSegPaginas.Fields dicKey)
		{
			Dictionary<String, EntSegPaginas>  miDic = new Dictionary<String, EntSegPaginas>();
			
			foreach (DataRow row in dtsegpaginas.Rows)
			{
				var obj = crearObjeto(row);
				
				var nameOfProperty = dicKey.ToString();
				var propertyInfo = obj.GetType().GetProperty(nameOfProperty);
				var value = propertyInfo.GetValue(obj, null);
				
				miDic.Add(value.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
		#endregion

	}
}

