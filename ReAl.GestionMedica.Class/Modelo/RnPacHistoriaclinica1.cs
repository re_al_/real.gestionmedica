#region 
/***********************************************************************************************************
	NOMBRE:       RnPacHistoriaclinica1
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla pachistoriaclinica1

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/04/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Interface;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Modelo
{
	public class RnPacHistoriaclinica1: IPacHistoriaclinica1
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region IPacHistoriaclinica1 Members

		#region Reflection

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string GetTableScript()
		{
			TableClass tabla = new TableClass(typeof(EntPacHistoriaclinica1));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, EntPacHistoriaclinica1.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntPacHistoriaclinica1).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="strField">Nombre de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, string strField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntPacHistoriaclinica1).GetProperty(strField).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

/// <summary>
/// Inserta una valor a una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">Es el nombre de la propiedad</param>
/// <param name="dynValor">Es el valor que se insertara a la propiedad</param>
public void SetDato(ref EntPacHistoriaclinica1 obj, string strPropiedad, dynamic dynValor)
{
	if (obj == null) throw new ArgumentNullException();
	obj.GetType().GetProperty(strPropiedad).SetValue(obj, GetColumnType(dynValor, strPropiedad), null);
}

/// <summary>
/// Obtiene el valor de una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">El nombre de la propiedad de la que se obtendra el valor</param>
/// <returns>Devuelve el valor del a propiedad seleccionada</returns>
public dynamic GetDato(ref EntPacHistoriaclinica1 obj, string strPropiedad)
{
	if (obj == null) return null;
	var propertyInfo = obj.GetType().GetProperty(strPropiedad);
	return GetColumnType(propertyInfo.GetValue(obj, null), strPropiedad);
}

		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla pachistoriaclinica1 a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla pachistoriaclinica1
		/// </returns>
		public string CreatePk(string[] args)
		{
			return args[0];
		}
		
		#endregion 

		#region ObtenerObjeto

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica1 a partir de la llave primaria
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica1 ObtenerObjeto(Int64 Int64idppa)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64idppa + "'");
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica1 a partir del usuario que inserta
		/// </summary>
		/// <param name="strUsuCre">Login o nombre de usuario</param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica1 ObtenerObjetoInsertado(string strUsuCre)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + strUsuCre + "'");
			
			int iIdInsertado = FuncionesMax(EntPacHistoriaclinica1.Fields.idppa, arrColumnasWhere, arrValoresWhere);
			
			return ObtenerObjeto(iIdInsertado);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica1 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica1 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntPacHistoriaclinica1 obj = new EntPacHistoriaclinica1();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica1 ObtenerObjeto(Hashtable htbFiltro)
		{
			return ObtenerObjeto(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica1 ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntPacHistoriaclinica1 obj = new EntPacHistoriaclinica1();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica1 ObtenerObjeto(EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica1 ObtenerObjeto(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo EntPacHistoriaclinica1 a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo EntPacHistoriaclinica1
		/// </returns>
		public EntPacHistoriaclinica1 ObtenerObjeto(Int64 Int64idppa, ref CTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64idppa + "'");
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica1 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica1 ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerObjeto(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica1 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica1 ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					EntPacHistoriaclinica1 obj = new EntPacHistoriaclinica1();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica1 ObtenerObjeto(EntPacHistoriaclinica1.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica1 ObtenerObjeto(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion

		#region ObtenerLista

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacHistoriaclinica1>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerLista(EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerLista(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacHistoriaclinica1>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerLista(EntPacHistoriaclinica1.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerLista(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerLista(Hashtable htbFiltro)
		{
			return ObtenerLista(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacHistoriaclinica1>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerLista(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacHistoriaclinica1>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacHistoriaclinica1>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacHistoriaclinica1>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacHistoriaclinica1>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacHistoriaclinica1>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica1.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerCola y Obtener Pila

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica1> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica1> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica1> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntPacHistoriaclinica1>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica1> ObtenerCola(EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica1> ObtenerCola(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica1> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica1> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntPacHistoriaclinica1>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica1> ObtenerCola(EntPacHistoriaclinica1.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica1> ObtenerCola(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica1> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica1> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica1> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntPacHistoriaclinica1>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica1> ObtenerPila(EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica1> ObtenerPila(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica1> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica1> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntPacHistoriaclinica1>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica1> ObtenerPila(EntPacHistoriaclinica1.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica1> ObtenerPila(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerDataTable

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla pachistoriaclinica1
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(EntPacHistoriaclinica1.Fields.idppa.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.idppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1motivophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1motivophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1fumphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1fumphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1fupphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1fupphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1taphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1taphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1fcphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1fcphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1frphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1frphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1tempphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1tempphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1pesophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1pesophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h1pcphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h1pcphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.apiestado.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.apitransaccion.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.apitransaccion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.usucre.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.feccre.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.usumod.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica1.Fields.fecmod.ToString(),typeof(EntPacHistoriaclinica1).GetProperty(EntPacHistoriaclinica1.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una pachistoriaclinica1
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla pachistoriaclinica1
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable ObtenerDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE pachistoriaclinica1
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable ObtenerDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica1
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableOr(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region ObtenerDiccionario

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntPacHistoriaclinica1>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntPacHistoriaclinica1>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(EntPacHistoriaclinica1.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica1 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		public Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionarioKey(EntPacHistoriaclinica1.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionarioKey(String strParamAdic, EntPacHistoriaclinica1.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdic, dicKey);
		}
		
		public Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, EntPacHistoriaclinica1.Fields dicKey)
		{
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, "", dicKey);
		}
		
		public Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, EntPacHistoriaclinica1.Fields dicKey)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table, dicKey);
				}
				else
					return new Dictionary<string, EntPacHistoriaclinica1>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		public Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionarioKey(EntPacHistoriaclinica1.Fields searchField, object searchValue, EntPacHistoriaclinica1.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionarioKey(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales, EntPacHistoriaclinica1.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey);
		}
		

		#endregion 

		#region ObjetoASp

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla pachistoriaclinica1
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntPacHistoriaclinica1 obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValoresParam.Add(obj.h1motivophc == null ? null : "'" + obj.h1motivophc + "'");
				arrValoresParam.Add(obj.h1enfermedadphc == null ? null : "'" + obj.h1enfermedadphc + "'");
				arrValoresParam.Add(obj.h1cirujiasphc == null ? null : "'" + obj.h1cirujiasphc + "'");
				arrValoresParam.Add(obj.h1infeccionesphc == null ? null : "'" + obj.h1infeccionesphc + "'");
				arrValoresParam.Add(obj.h1traumatismosphc == null ? null : "'" + obj.h1traumatismosphc + "'");
				arrValoresParam.Add(obj.h1cardiovascularphc == null ? null : "'" + obj.h1cardiovascularphc + "'");
				arrValoresParam.Add(obj.h1metabolicophc == null ? null : "'" + obj.h1metabolicophc + "'");
				arrValoresParam.Add(obj.h1toxicophc == null ? null : "'" + obj.h1toxicophc + "'");
				arrValoresParam.Add(obj.h1familiarphc == null ? null : "'" + obj.h1familiarphc + "'");
				arrValoresParam.Add(obj.h1ginecoobsphc == null ? null : "'" + obj.h1ginecoobsphc + "'");
				arrValoresParam.Add(obj.h1otrosphc == null ? null : "'" + obj.h1otrosphc + "'");
				arrValoresParam.Add(obj.h1embarazosphc);
				arrValoresParam.Add(obj.h1gestaphc);
				arrValoresParam.Add(obj.h1abortosphc);
				arrValoresParam.Add(obj.h1mesntruacionesphc == null ? null : "'" + obj.h1mesntruacionesphc + "'");
				arrValoresParam.Add(obj.h1fumphc == null ? null : "'" + obj.h1fumphc + "'");
				arrValoresParam.Add(obj.h1fupphc == null ? null : "'" + obj.h1fupphc + "'");
				arrValoresParam.Add(obj.h1revisionphc == null ? null : "'" + obj.h1revisionphc + "'");
				arrValoresParam.Add(obj.h1examenfisicophc == null ? null : "'" + obj.h1examenfisicophc + "'");
				arrValoresParam.Add(obj.h1taphc == null ? null : "'" + obj.h1taphc + "'");
				arrValoresParam.Add(obj.h1fcphc == null ? null : "'" + obj.h1fcphc + "'");
				arrValoresParam.Add(obj.h1frphc == null ? null : "'" + obj.h1frphc + "'");
				arrValoresParam.Add(obj.h1tempphc == null ? null : "'" + obj.h1tempphc + "'");
				arrValoresParam.Add(obj.h1tallaphc == null ? null : "'" + obj.h1tallaphc + "'");
				arrValoresParam.Add(obj.h1pesophc == null ? null : "'" + obj.h1pesophc + "'");
				arrValoresParam.Add(obj.h1pcphc == null ? null : "'" + obj.h1pcphc + "'");
				arrValoresParam.Add(obj.h2cabezaphc == null ? null : "'" + obj.h2cabezaphc + "'");
				arrValoresParam.Add(obj.h2cuellophc == null ? null : "'" + obj.h2cuellophc + "'");
				arrValoresParam.Add(obj.h2toraxphc == null ? null : "'" + obj.h2toraxphc + "'");
				arrValoresParam.Add(obj.h2abdomenphc == null ? null : "'" + obj.h2abdomenphc + "'");
				arrValoresParam.Add(obj.h2genitourinariophc == null ? null : "'" + obj.h2genitourinariophc + "'");
				arrValoresParam.Add(obj.h2extremidadesphc == null ? null : "'" + obj.h2extremidadesphc + "'");
				arrValoresParam.Add(obj.h2conalertaphc == null ? null : "'" + obj.h2conalertaphc + "'");
				arrValoresParam.Add(obj.h2conconfusionphc == null ? null : "'" + obj.h2conconfusionphc + "'");
				arrValoresParam.Add(obj.h2consomnolenciaphc == null ? null : "'" + obj.h2consomnolenciaphc + "'");
				arrValoresParam.Add(obj.h2conestuforphc == null ? null : "'" + obj.h2conestuforphc + "'");
				arrValoresParam.Add(obj.h2concomaphc == null ? null : "'" + obj.h2concomaphc + "'");
				arrValoresParam.Add(obj.h2oriespaciophc == null ? null : "'" + obj.h2oriespaciophc + "'");
				arrValoresParam.Add(obj.h2orilugarphc == null ? null : "'" + obj.h2orilugarphc + "'");
				arrValoresParam.Add(obj.h2oripersonaphc == null ? null : "'" + obj.h2oripersonaphc + "'");
				arrValoresParam.Add(obj.h2razideaphc == null ? null : "'" + obj.h2razideaphc + "'");
				arrValoresParam.Add(obj.h2razjuiciophc == null ? null : "'" + obj.h2razjuiciophc + "'");
				arrValoresParam.Add(obj.h2razabstraccionphc == null ? null : "'" + obj.h2razabstraccionphc + "'");
				arrValoresParam.Add(obj.h2conocimientosgenerales == null ? null : "'" + obj.h2conocimientosgenerales + "'");
				arrValoresParam.Add(obj.h2atgdesatentophc == null ? null : "'" + obj.h2atgdesatentophc + "'");
				arrValoresParam.Add(obj.h2atgfluctuantephc == null ? null : "'" + obj.h2atgfluctuantephc + "'");
				arrValoresParam.Add(obj.h2atgnegativophc == null ? null : "'" + obj.h2atgnegativophc + "'");
				arrValoresParam.Add(obj.h2atgnegligentephc == null ? null : "'" + obj.h2atgnegligentephc + "'");
				arrValoresParam.Add(obj.h2pagconfusionphc == null ? null : "'" + obj.h2pagconfusionphc + "'");
				arrValoresParam.Add(obj.h2pagdeliriophc == null ? null : "'" + obj.h2pagdeliriophc + "'");
				arrValoresParam.Add(obj.h2paedelusionesphc == null ? null : "'" + obj.h2paedelusionesphc + "'");
				arrValoresParam.Add(obj.h2paeilsuionesphc == null ? null : "'" + obj.h2paeilsuionesphc + "'");
				arrValoresParam.Add(obj.h2paealucinacionesphc == null ? null : "'" + obj.h2paealucinacionesphc + "'");
				arrValoresParam.Add(obj.h3conrecientephc == null ? null : "'" + obj.h3conrecientephc + "'");
				arrValoresParam.Add(obj.h3conremotaphc == null ? null : "'" + obj.h3conremotaphc + "'");
				arrValoresParam.Add(obj.h3afeeuforicophc == null ? null : "'" + obj.h3afeeuforicophc + "'");
				arrValoresParam.Add(obj.h3afeplanophc == null ? null : "'" + obj.h3afeplanophc + "'");
				arrValoresParam.Add(obj.h3afedeprimidophc == null ? null : "'" + obj.h3afedeprimidophc + "'");
				arrValoresParam.Add(obj.h3afelabilphc == null ? null : "'" + obj.h3afelabilphc + "'");
				arrValoresParam.Add(obj.h3afehostilphc == null ? null : "'" + obj.h3afehostilphc + "'");
				arrValoresParam.Add(obj.h3afeinadecuadophc == null ? null : "'" + obj.h3afeinadecuadophc + "'");
				arrValoresParam.Add(obj.h3hdoizquierdophc == null ? null : "'" + obj.h3hdoizquierdophc + "'");
				arrValoresParam.Add(obj.h3hdoderechophc == null ? null : "'" + obj.h3hdoderechophc + "'");
				arrValoresParam.Add(obj.h3cgeexpresionphc == null ? null : "'" + obj.h3cgeexpresionphc + "'");
				arrValoresParam.Add(obj.h3cgenominacionphc == null ? null : "'" + obj.h3cgenominacionphc + "'");
				arrValoresParam.Add(obj.h3cgerepeticionphc == null ? null : "'" + obj.h3cgerepeticionphc + "'");
				arrValoresParam.Add(obj.h3cgecomprensionphc == null ? null : "'" + obj.h3cgecomprensionphc + "'");
				arrValoresParam.Add(obj.h3apebrocaphc == null ? null : "'" + obj.h3apebrocaphc + "'");
				arrValoresParam.Add(obj.h3apeconduccionphc == null ? null : "'" + obj.h3apeconduccionphc + "'");
				arrValoresParam.Add(obj.h3apewernickephc == null ? null : "'" + obj.h3apewernickephc + "'");
				arrValoresParam.Add(obj.h3apeglobalphc == null ? null : "'" + obj.h3apeglobalphc + "'");
				arrValoresParam.Add(obj.h3apeanomiaphc == null ? null : "'" + obj.h3apeanomiaphc + "'");
				arrValoresParam.Add(obj.h3atrmotoraphc == null ? null : "'" + obj.h3atrmotoraphc + "'");
				arrValoresParam.Add(obj.h3atrsensitivaphc == null ? null : "'" + obj.h3atrsensitivaphc + "'");
				arrValoresParam.Add(obj.h3atrmixtaphc == null ? null : "'" + obj.h3atrmixtaphc + "'");
				arrValoresParam.Add(obj.h3ataexpresionphc == null ? null : "'" + obj.h3ataexpresionphc + "'");
				arrValoresParam.Add(obj.h3atacomprensionphc == null ? null : "'" + obj.h3atacomprensionphc + "'");
				arrValoresParam.Add(obj.h3atamixtaphc == null ? null : "'" + obj.h3atamixtaphc + "'");
				arrValoresParam.Add(obj.h3aprmotoraphc == null ? null : "'" + obj.h3aprmotoraphc + "'");
				arrValoresParam.Add(obj.h3aprsensorialphc == null ? null : "'" + obj.h3aprsensorialphc + "'");
				arrValoresParam.Add(obj.h3lesalexiaphc == null ? null : "'" + obj.h3lesalexiaphc + "'");
				arrValoresParam.Add(obj.h3lesagrafiaphc == null ? null : "'" + obj.h3lesagrafiaphc + "'");
				arrValoresParam.Add(obj.h3lpdacalculiaphc == null ? null : "'" + obj.h3lpdacalculiaphc + "'");
				arrValoresParam.Add(obj.h3lpdagrafiaphc == null ? null : "'" + obj.h3lpdagrafiaphc + "'");
				arrValoresParam.Add(obj.h3lpddesorientacionphc == null ? null : "'" + obj.h3lpddesorientacionphc + "'");
				arrValoresParam.Add(obj.h3lpdagnosiaphc == null ? null : "'" + obj.h3lpdagnosiaphc + "'");
				arrValoresParam.Add(obj.h3agnauditivaphc == null ? null : "'" + obj.h3agnauditivaphc + "'");
				arrValoresParam.Add(obj.h3agnvisualphc == null ? null : "'" + obj.h3agnvisualphc + "'");
				arrValoresParam.Add(obj.h3agntactilphc == null ? null : "'" + obj.h3agntactilphc + "'");
				arrValoresParam.Add(obj.h3aprideacionalphc == null ? null : "'" + obj.h3aprideacionalphc + "'");
				arrValoresParam.Add(obj.h3aprideomotoraphc == null ? null : "'" + obj.h3aprideomotoraphc + "'");
				arrValoresParam.Add(obj.h3aprdesatencionphc == null ? null : "'" + obj.h3aprdesatencionphc + "'");
				arrValoresParam.Add(obj.h3anosognosiaphc == null ? null : "'" + obj.h3anosognosiaphc + "'");
				arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla pachistoriaclinica1
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntPacHistoriaclinica1 obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValoresParam.Add(obj.h1motivophc == null ? null : "'" + obj.h1motivophc + "'");
				arrValoresParam.Add(obj.h1enfermedadphc == null ? null : "'" + obj.h1enfermedadphc + "'");
				arrValoresParam.Add(obj.h1cirujiasphc == null ? null : "'" + obj.h1cirujiasphc + "'");
				arrValoresParam.Add(obj.h1infeccionesphc == null ? null : "'" + obj.h1infeccionesphc + "'");
				arrValoresParam.Add(obj.h1traumatismosphc == null ? null : "'" + obj.h1traumatismosphc + "'");
				arrValoresParam.Add(obj.h1cardiovascularphc == null ? null : "'" + obj.h1cardiovascularphc + "'");
				arrValoresParam.Add(obj.h1metabolicophc == null ? null : "'" + obj.h1metabolicophc + "'");
				arrValoresParam.Add(obj.h1toxicophc == null ? null : "'" + obj.h1toxicophc + "'");
				arrValoresParam.Add(obj.h1familiarphc == null ? null : "'" + obj.h1familiarphc + "'");
				arrValoresParam.Add(obj.h1ginecoobsphc == null ? null : "'" + obj.h1ginecoobsphc + "'");
				arrValoresParam.Add(obj.h1otrosphc == null ? null : "'" + obj.h1otrosphc + "'");
				arrValoresParam.Add(obj.h1embarazosphc);
				arrValoresParam.Add(obj.h1gestaphc);
				arrValoresParam.Add(obj.h1abortosphc);
				arrValoresParam.Add(obj.h1mesntruacionesphc == null ? null : "'" + obj.h1mesntruacionesphc + "'");
				arrValoresParam.Add(obj.h1fumphc == null ? null : "'" + obj.h1fumphc + "'");
				arrValoresParam.Add(obj.h1fupphc == null ? null : "'" + obj.h1fupphc + "'");
				arrValoresParam.Add(obj.h1revisionphc == null ? null : "'" + obj.h1revisionphc + "'");
				arrValoresParam.Add(obj.h1examenfisicophc == null ? null : "'" + obj.h1examenfisicophc + "'");
				arrValoresParam.Add(obj.h1taphc == null ? null : "'" + obj.h1taphc + "'");
				arrValoresParam.Add(obj.h1fcphc == null ? null : "'" + obj.h1fcphc + "'");
				arrValoresParam.Add(obj.h1frphc == null ? null : "'" + obj.h1frphc + "'");
				arrValoresParam.Add(obj.h1tempphc == null ? null : "'" + obj.h1tempphc + "'");
				arrValoresParam.Add(obj.h1tallaphc == null ? null : "'" + obj.h1tallaphc + "'");
				arrValoresParam.Add(obj.h1pesophc == null ? null : "'" + obj.h1pesophc + "'");
				arrValoresParam.Add(obj.h1pcphc == null ? null : "'" + obj.h1pcphc + "'");
				arrValoresParam.Add(obj.h2cabezaphc == null ? null : "'" + obj.h2cabezaphc + "'");
				arrValoresParam.Add(obj.h2cuellophc == null ? null : "'" + obj.h2cuellophc + "'");
				arrValoresParam.Add(obj.h2toraxphc == null ? null : "'" + obj.h2toraxphc + "'");
				arrValoresParam.Add(obj.h2abdomenphc == null ? null : "'" + obj.h2abdomenphc + "'");
				arrValoresParam.Add(obj.h2genitourinariophc == null ? null : "'" + obj.h2genitourinariophc + "'");
				arrValoresParam.Add(obj.h2extremidadesphc == null ? null : "'" + obj.h2extremidadesphc + "'");
				arrValoresParam.Add(obj.h2conalertaphc == null ? null : "'" + obj.h2conalertaphc + "'");
				arrValoresParam.Add(obj.h2conconfusionphc == null ? null : "'" + obj.h2conconfusionphc + "'");
				arrValoresParam.Add(obj.h2consomnolenciaphc == null ? null : "'" + obj.h2consomnolenciaphc + "'");
				arrValoresParam.Add(obj.h2conestuforphc == null ? null : "'" + obj.h2conestuforphc + "'");
				arrValoresParam.Add(obj.h2concomaphc == null ? null : "'" + obj.h2concomaphc + "'");
				arrValoresParam.Add(obj.h2oriespaciophc == null ? null : "'" + obj.h2oriespaciophc + "'");
				arrValoresParam.Add(obj.h2orilugarphc == null ? null : "'" + obj.h2orilugarphc + "'");
				arrValoresParam.Add(obj.h2oripersonaphc == null ? null : "'" + obj.h2oripersonaphc + "'");
				arrValoresParam.Add(obj.h2razideaphc == null ? null : "'" + obj.h2razideaphc + "'");
				arrValoresParam.Add(obj.h2razjuiciophc == null ? null : "'" + obj.h2razjuiciophc + "'");
				arrValoresParam.Add(obj.h2razabstraccionphc == null ? null : "'" + obj.h2razabstraccionphc + "'");
				arrValoresParam.Add(obj.h2conocimientosgenerales == null ? null : "'" + obj.h2conocimientosgenerales + "'");
				arrValoresParam.Add(obj.h2atgdesatentophc == null ? null : "'" + obj.h2atgdesatentophc + "'");
				arrValoresParam.Add(obj.h2atgfluctuantephc == null ? null : "'" + obj.h2atgfluctuantephc + "'");
				arrValoresParam.Add(obj.h2atgnegativophc == null ? null : "'" + obj.h2atgnegativophc + "'");
				arrValoresParam.Add(obj.h2atgnegligentephc == null ? null : "'" + obj.h2atgnegligentephc + "'");
				arrValoresParam.Add(obj.h2pagconfusionphc == null ? null : "'" + obj.h2pagconfusionphc + "'");
				arrValoresParam.Add(obj.h2pagdeliriophc == null ? null : "'" + obj.h2pagdeliriophc + "'");
				arrValoresParam.Add(obj.h2paedelusionesphc == null ? null : "'" + obj.h2paedelusionesphc + "'");
				arrValoresParam.Add(obj.h2paeilsuionesphc == null ? null : "'" + obj.h2paeilsuionesphc + "'");
				arrValoresParam.Add(obj.h2paealucinacionesphc == null ? null : "'" + obj.h2paealucinacionesphc + "'");
				arrValoresParam.Add(obj.h3conrecientephc == null ? null : "'" + obj.h3conrecientephc + "'");
				arrValoresParam.Add(obj.h3conremotaphc == null ? null : "'" + obj.h3conremotaphc + "'");
				arrValoresParam.Add(obj.h3afeeuforicophc == null ? null : "'" + obj.h3afeeuforicophc + "'");
				arrValoresParam.Add(obj.h3afeplanophc == null ? null : "'" + obj.h3afeplanophc + "'");
				arrValoresParam.Add(obj.h3afedeprimidophc == null ? null : "'" + obj.h3afedeprimidophc + "'");
				arrValoresParam.Add(obj.h3afelabilphc == null ? null : "'" + obj.h3afelabilphc + "'");
				arrValoresParam.Add(obj.h3afehostilphc == null ? null : "'" + obj.h3afehostilphc + "'");
				arrValoresParam.Add(obj.h3afeinadecuadophc == null ? null : "'" + obj.h3afeinadecuadophc + "'");
				arrValoresParam.Add(obj.h3hdoizquierdophc == null ? null : "'" + obj.h3hdoizquierdophc + "'");
				arrValoresParam.Add(obj.h3hdoderechophc == null ? null : "'" + obj.h3hdoderechophc + "'");
				arrValoresParam.Add(obj.h3cgeexpresionphc == null ? null : "'" + obj.h3cgeexpresionphc + "'");
				arrValoresParam.Add(obj.h3cgenominacionphc == null ? null : "'" + obj.h3cgenominacionphc + "'");
				arrValoresParam.Add(obj.h3cgerepeticionphc == null ? null : "'" + obj.h3cgerepeticionphc + "'");
				arrValoresParam.Add(obj.h3cgecomprensionphc == null ? null : "'" + obj.h3cgecomprensionphc + "'");
				arrValoresParam.Add(obj.h3apebrocaphc == null ? null : "'" + obj.h3apebrocaphc + "'");
				arrValoresParam.Add(obj.h3apeconduccionphc == null ? null : "'" + obj.h3apeconduccionphc + "'");
				arrValoresParam.Add(obj.h3apewernickephc == null ? null : "'" + obj.h3apewernickephc + "'");
				arrValoresParam.Add(obj.h3apeglobalphc == null ? null : "'" + obj.h3apeglobalphc + "'");
				arrValoresParam.Add(obj.h3apeanomiaphc == null ? null : "'" + obj.h3apeanomiaphc + "'");
				arrValoresParam.Add(obj.h3atrmotoraphc == null ? null : "'" + obj.h3atrmotoraphc + "'");
				arrValoresParam.Add(obj.h3atrsensitivaphc == null ? null : "'" + obj.h3atrsensitivaphc + "'");
				arrValoresParam.Add(obj.h3atrmixtaphc == null ? null : "'" + obj.h3atrmixtaphc + "'");
				arrValoresParam.Add(obj.h3ataexpresionphc == null ? null : "'" + obj.h3ataexpresionphc + "'");
				arrValoresParam.Add(obj.h3atacomprensionphc == null ? null : "'" + obj.h3atacomprensionphc + "'");
				arrValoresParam.Add(obj.h3atamixtaphc == null ? null : "'" + obj.h3atamixtaphc + "'");
				arrValoresParam.Add(obj.h3aprmotoraphc == null ? null : "'" + obj.h3aprmotoraphc + "'");
				arrValoresParam.Add(obj.h3aprsensorialphc == null ? null : "'" + obj.h3aprsensorialphc + "'");
				arrValoresParam.Add(obj.h3lesalexiaphc == null ? null : "'" + obj.h3lesalexiaphc + "'");
				arrValoresParam.Add(obj.h3lesagrafiaphc == null ? null : "'" + obj.h3lesagrafiaphc + "'");
				arrValoresParam.Add(obj.h3lpdacalculiaphc == null ? null : "'" + obj.h3lpdacalculiaphc + "'");
				arrValoresParam.Add(obj.h3lpdagrafiaphc == null ? null : "'" + obj.h3lpdagrafiaphc + "'");
				arrValoresParam.Add(obj.h3lpddesorientacionphc == null ? null : "'" + obj.h3lpddesorientacionphc + "'");
				arrValoresParam.Add(obj.h3lpdagnosiaphc == null ? null : "'" + obj.h3lpdagnosiaphc + "'");
				arrValoresParam.Add(obj.h3agnauditivaphc == null ? null : "'" + obj.h3agnauditivaphc + "'");
				arrValoresParam.Add(obj.h3agnvisualphc == null ? null : "'" + obj.h3agnvisualphc + "'");
				arrValoresParam.Add(obj.h3agntactilphc == null ? null : "'" + obj.h3agntactilphc + "'");
				arrValoresParam.Add(obj.h3aprideacionalphc == null ? null : "'" + obj.h3aprideacionalphc + "'");
				arrValoresParam.Add(obj.h3aprideomotoraphc == null ? null : "'" + obj.h3aprideomotoraphc + "'");
				arrValoresParam.Add(obj.h3aprdesatencionphc == null ? null : "'" + obj.h3aprdesatencionphc + "'");
				arrValoresParam.Add(obj.h3anosognosiaphc == null ? null : "'" + obj.h3anosognosiaphc + "'");
				arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region FuncionesAgregadas

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntPacHistoriaclinica1.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntPacHistoriaclinica1.Fields refField, EntPacHistoriaclinica1.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntPacHistoriaclinica1.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntPacHistoriaclinica1.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntPacHistoriaclinica1.Fields refField, EntPacHistoriaclinica1.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntPacHistoriaclinica1.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntPacHistoriaclinica1.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntPacHistoriaclinica1.Fields refField, EntPacHistoriaclinica1.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntPacHistoriaclinica1.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntPacHistoriaclinica1.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntPacHistoriaclinica1.Fields refField, EntPacHistoriaclinica1.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntPacHistoriaclinica1.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntPacHistoriaclinica1.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntPacHistoriaclinica1.Fields refField, EntPacHistoriaclinica1.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica1 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntPacHistoriaclinica1.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs SP

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica1
		/// </returns>
		public bool Insert(EntPacHistoriaclinica1 obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("idppa");
				arrValoresParam.Add(null);
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrValoresParam.Add(obj.usucre);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacHistoriaclinica1.SpPh1Ins.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica1
		/// </returns>
		public bool Insert(EntPacHistoriaclinica1 obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("idppa");
				arrValoresParam.Add("");
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrValoresParam.Add(obj.usucre);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacHistoriaclinica1.SpPh1Ins.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor que indica la cantidad de registros actualizados en pachistoriaclinica1
		/// </returns>
		public int Update(EntPacHistoriaclinica1 obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrValoresParam.Add(obj.h1motivophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrValoresParam.Add(obj.h1enfermedadphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrValoresParam.Add(obj.h1cirujiasphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrValoresParam.Add(obj.h1infeccionesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrValoresParam.Add(obj.h1traumatismosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrValoresParam.Add(obj.h1cardiovascularphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrValoresParam.Add(obj.h1metabolicophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrValoresParam.Add(obj.h1toxicophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrValoresParam.Add(obj.h1familiarphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrValoresParam.Add(obj.h1ginecoobsphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrValoresParam.Add(obj.h1otrosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrValoresParam.Add(obj.h1embarazosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrValoresParam.Add(obj.h1gestaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrValoresParam.Add(obj.h1abortosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrValoresParam.Add(obj.h1mesntruacionesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrValoresParam.Add(obj.h1fumphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrValoresParam.Add(obj.h1fupphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrValoresParam.Add(obj.h1revisionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrValoresParam.Add(obj.h1examenfisicophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrValoresParam.Add(obj.h1taphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrValoresParam.Add(obj.h1fcphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrValoresParam.Add(obj.h1frphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrValoresParam.Add(obj.h1tempphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrValoresParam.Add(obj.h1tallaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrValoresParam.Add(obj.h1pesophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrValoresParam.Add(obj.h1pcphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrValoresParam.Add(obj.h2cabezaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrValoresParam.Add(obj.h2cuellophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrValoresParam.Add(obj.h2toraxphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrValoresParam.Add(obj.h2abdomenphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrValoresParam.Add(obj.h2genitourinariophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrValoresParam.Add(obj.h2extremidadesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrValoresParam.Add(obj.h2conalertaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrValoresParam.Add(obj.h2conconfusionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrValoresParam.Add(obj.h2consomnolenciaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrValoresParam.Add(obj.h2conestuforphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrValoresParam.Add(obj.h2concomaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrValoresParam.Add(obj.h2oriespaciophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrValoresParam.Add(obj.h2orilugarphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrValoresParam.Add(obj.h2oripersonaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrValoresParam.Add(obj.h2razideaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrValoresParam.Add(obj.h2razjuiciophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrValoresParam.Add(obj.h2razabstraccionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrValoresParam.Add(obj.h2conocimientosgenerales);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrValoresParam.Add(obj.h2atgdesatentophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrValoresParam.Add(obj.h2atgfluctuantephc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrValoresParam.Add(obj.h2atgnegativophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrValoresParam.Add(obj.h2atgnegligentephc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrValoresParam.Add(obj.h2pagconfusionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrValoresParam.Add(obj.h2pagdeliriophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrValoresParam.Add(obj.h2paedelusionesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrValoresParam.Add(obj.h2paeilsuionesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrValoresParam.Add(obj.h2paealucinacionesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrValoresParam.Add(obj.h3conrecientephc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrValoresParam.Add(obj.h3conremotaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrValoresParam.Add(obj.h3afeeuforicophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrValoresParam.Add(obj.h3afeplanophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrValoresParam.Add(obj.h3afedeprimidophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrValoresParam.Add(obj.h3afelabilphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrValoresParam.Add(obj.h3afehostilphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrValoresParam.Add(obj.h3afeinadecuadophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrValoresParam.Add(obj.h3hdoizquierdophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrValoresParam.Add(obj.h3hdoderechophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrValoresParam.Add(obj.h3cgeexpresionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrValoresParam.Add(obj.h3cgenominacionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrValoresParam.Add(obj.h3cgerepeticionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrValoresParam.Add(obj.h3cgecomprensionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrValoresParam.Add(obj.h3apebrocaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrValoresParam.Add(obj.h3apeconduccionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrValoresParam.Add(obj.h3apewernickephc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrValoresParam.Add(obj.h3apeglobalphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrValoresParam.Add(obj.h3apeanomiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrValoresParam.Add(obj.h3atrmotoraphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrValoresParam.Add(obj.h3atrsensitivaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrValoresParam.Add(obj.h3atrmixtaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrValoresParam.Add(obj.h3ataexpresionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrValoresParam.Add(obj.h3atacomprensionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrValoresParam.Add(obj.h3atamixtaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrValoresParam.Add(obj.h3aprmotoraphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrValoresParam.Add(obj.h3aprsensorialphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrValoresParam.Add(obj.h3lesalexiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrValoresParam.Add(obj.h3lesagrafiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrValoresParam.Add(obj.h3lpdacalculiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrValoresParam.Add(obj.h3lpdagrafiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrValoresParam.Add(obj.h3lpddesorientacionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrValoresParam.Add(obj.h3lpdagnosiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrValoresParam.Add(obj.h3agnauditivaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrValoresParam.Add(obj.h3agnvisualphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrValoresParam.Add(obj.h3agntactilphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrValoresParam.Add(obj.h3aprideacionalphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrValoresParam.Add(obj.h3aprideomotoraphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrValoresParam.Add(obj.h3aprdesatencionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrValoresParam.Add(obj.h3anosognosiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrValoresParam.Add(obj.apitransaccion);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrValoresParam.Add(obj.usumod);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacHistoriaclinica1.SpPh1Upd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica1
		/// </returns>
		public int Update(EntPacHistoriaclinica1 obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrValoresParam.Add(obj.h1motivophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrValoresParam.Add(obj.h1enfermedadphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrValoresParam.Add(obj.h1cirujiasphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrValoresParam.Add(obj.h1infeccionesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrValoresParam.Add(obj.h1traumatismosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrValoresParam.Add(obj.h1cardiovascularphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrValoresParam.Add(obj.h1metabolicophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrValoresParam.Add(obj.h1toxicophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrValoresParam.Add(obj.h1familiarphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrValoresParam.Add(obj.h1ginecoobsphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrValoresParam.Add(obj.h1otrosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrValoresParam.Add(obj.h1embarazosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrValoresParam.Add(obj.h1gestaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrValoresParam.Add(obj.h1abortosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrValoresParam.Add(obj.h1mesntruacionesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrValoresParam.Add(obj.h1fumphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrValoresParam.Add(obj.h1fupphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrValoresParam.Add(obj.h1revisionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrValoresParam.Add(obj.h1examenfisicophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrValoresParam.Add(obj.h1taphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrValoresParam.Add(obj.h1fcphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrValoresParam.Add(obj.h1frphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrValoresParam.Add(obj.h1tempphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrValoresParam.Add(obj.h1tallaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrValoresParam.Add(obj.h1pesophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrValoresParam.Add(obj.h1pcphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrValoresParam.Add(obj.h2cabezaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrValoresParam.Add(obj.h2cuellophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrValoresParam.Add(obj.h2toraxphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrValoresParam.Add(obj.h2abdomenphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrValoresParam.Add(obj.h2genitourinariophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrValoresParam.Add(obj.h2extremidadesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrValoresParam.Add(obj.h2conalertaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrValoresParam.Add(obj.h2conconfusionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrValoresParam.Add(obj.h2consomnolenciaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrValoresParam.Add(obj.h2conestuforphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrValoresParam.Add(obj.h2concomaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrValoresParam.Add(obj.h2oriespaciophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrValoresParam.Add(obj.h2orilugarphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrValoresParam.Add(obj.h2oripersonaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrValoresParam.Add(obj.h2razideaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrValoresParam.Add(obj.h2razjuiciophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrValoresParam.Add(obj.h2razabstraccionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrValoresParam.Add(obj.h2conocimientosgenerales);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrValoresParam.Add(obj.h2atgdesatentophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrValoresParam.Add(obj.h2atgfluctuantephc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrValoresParam.Add(obj.h2atgnegativophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrValoresParam.Add(obj.h2atgnegligentephc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrValoresParam.Add(obj.h2pagconfusionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrValoresParam.Add(obj.h2pagdeliriophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrValoresParam.Add(obj.h2paedelusionesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrValoresParam.Add(obj.h2paeilsuionesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrValoresParam.Add(obj.h2paealucinacionesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrValoresParam.Add(obj.h3conrecientephc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrValoresParam.Add(obj.h3conremotaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrValoresParam.Add(obj.h3afeeuforicophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrValoresParam.Add(obj.h3afeplanophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrValoresParam.Add(obj.h3afedeprimidophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrValoresParam.Add(obj.h3afelabilphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrValoresParam.Add(obj.h3afehostilphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrValoresParam.Add(obj.h3afeinadecuadophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrValoresParam.Add(obj.h3hdoizquierdophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrValoresParam.Add(obj.h3hdoderechophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrValoresParam.Add(obj.h3cgeexpresionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrValoresParam.Add(obj.h3cgenominacionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrValoresParam.Add(obj.h3cgerepeticionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrValoresParam.Add(obj.h3cgecomprensionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrValoresParam.Add(obj.h3apebrocaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrValoresParam.Add(obj.h3apeconduccionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrValoresParam.Add(obj.h3apewernickephc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrValoresParam.Add(obj.h3apeglobalphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrValoresParam.Add(obj.h3apeanomiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrValoresParam.Add(obj.h3atrmotoraphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrValoresParam.Add(obj.h3atrsensitivaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrValoresParam.Add(obj.h3atrmixtaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrValoresParam.Add(obj.h3ataexpresionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrValoresParam.Add(obj.h3atacomprensionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrValoresParam.Add(obj.h3atamixtaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrValoresParam.Add(obj.h3aprmotoraphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrValoresParam.Add(obj.h3aprsensorialphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrValoresParam.Add(obj.h3lesalexiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrValoresParam.Add(obj.h3lesagrafiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrValoresParam.Add(obj.h3lpdacalculiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrValoresParam.Add(obj.h3lpdagrafiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrValoresParam.Add(obj.h3lpddesorientacionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrValoresParam.Add(obj.h3lpdagnosiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrValoresParam.Add(obj.h3agnauditivaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrValoresParam.Add(obj.h3agnvisualphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrValoresParam.Add(obj.h3agntactilphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrValoresParam.Add(obj.h3aprideacionalphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrValoresParam.Add(obj.h3aprideomotoraphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrValoresParam.Add(obj.h3aprdesatencionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrValoresParam.Add(obj.h3anosognosiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrValoresParam.Add(obj.apitransaccion);
				
				arrNombreParam.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrValoresParam.Add(obj.usumod);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacHistoriaclinica1.SpPh1Upd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica1
		/// </returns>
		public int Delete(EntPacHistoriaclinica1 obj)
		{
			throw new Exception("No existe el Procedimiento Almacenado SpPh1Del.");
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica1
		/// </returns>
		public int Delete(EntPacHistoriaclinica1 obj, ref CTrans localTrans)
		{
			throw new Exception("No existe el Procedimiento Almacenado SpPh1Del.");
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica1
		/// </returns>
		public int InsertUpdate(EntPacHistoriaclinica1 obj)
		{
			try
			{
				bool esInsertar = true;
				
					esInsertar = (esInsertar && (obj.idppa == null));
				
				if (esInsertar)
					return Insert(obj) ? 1 : 0;
				else
					return Update(obj);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica1
		/// </returns>
		public int InsertUpdate(EntPacHistoriaclinica1 obj, ref CTrans localTrans)
		{
			try
			{
				bool esInsertar = false;
				
					esInsertar = (esInsertar && (obj.idppa == null));
				
				if (esInsertar)
					return Insert(obj, ref localTrans) ? 1 : 0;
				else
					return Update(obj, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs Query

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica1
		/// </returns>
		public bool InsertQuery(EntPacHistoriaclinica1 obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.h1motivophc == null ? null : "'" + obj.h1motivophc + "'");
				arrValores.Add(obj.h1enfermedadphc == null ? null : "'" + obj.h1enfermedadphc + "'");
				arrValores.Add(obj.h1cirujiasphc == null ? null : "'" + obj.h1cirujiasphc + "'");
				arrValores.Add(obj.h1infeccionesphc == null ? null : "'" + obj.h1infeccionesphc + "'");
				arrValores.Add(obj.h1traumatismosphc == null ? null : "'" + obj.h1traumatismosphc + "'");
				arrValores.Add(obj.h1cardiovascularphc == null ? null : "'" + obj.h1cardiovascularphc + "'");
				arrValores.Add(obj.h1metabolicophc == null ? null : "'" + obj.h1metabolicophc + "'");
				arrValores.Add(obj.h1toxicophc == null ? null : "'" + obj.h1toxicophc + "'");
				arrValores.Add(obj.h1familiarphc == null ? null : "'" + obj.h1familiarphc + "'");
				arrValores.Add(obj.h1ginecoobsphc == null ? null : "'" + obj.h1ginecoobsphc + "'");
				arrValores.Add(obj.h1otrosphc == null ? null : "'" + obj.h1otrosphc + "'");
				arrValores.Add(obj.h1embarazosphc);
				arrValores.Add(obj.h1gestaphc);
				arrValores.Add(obj.h1abortosphc);
				arrValores.Add(obj.h1mesntruacionesphc == null ? null : "'" + obj.h1mesntruacionesphc + "'");
				arrValores.Add(obj.h1fumphc == null ? null : "'" + obj.h1fumphc + "'");
				arrValores.Add(obj.h1fupphc == null ? null : "'" + obj.h1fupphc + "'");
				arrValores.Add(obj.h1revisionphc == null ? null : "'" + obj.h1revisionphc + "'");
				arrValores.Add(obj.h1examenfisicophc == null ? null : "'" + obj.h1examenfisicophc + "'");
				arrValores.Add(obj.h1taphc == null ? null : "'" + obj.h1taphc + "'");
				arrValores.Add(obj.h1fcphc == null ? null : "'" + obj.h1fcphc + "'");
				arrValores.Add(obj.h1frphc == null ? null : "'" + obj.h1frphc + "'");
				arrValores.Add(obj.h1tempphc == null ? null : "'" + obj.h1tempphc + "'");
				arrValores.Add(obj.h1tallaphc == null ? null : "'" + obj.h1tallaphc + "'");
				arrValores.Add(obj.h1pesophc == null ? null : "'" + obj.h1pesophc + "'");
				arrValores.Add(obj.h1pcphc == null ? null : "'" + obj.h1pcphc + "'");
				arrValores.Add(obj.h2cabezaphc == null ? null : "'" + obj.h2cabezaphc + "'");
				arrValores.Add(obj.h2cuellophc == null ? null : "'" + obj.h2cuellophc + "'");
				arrValores.Add(obj.h2toraxphc == null ? null : "'" + obj.h2toraxphc + "'");
				arrValores.Add(obj.h2abdomenphc == null ? null : "'" + obj.h2abdomenphc + "'");
				arrValores.Add(obj.h2genitourinariophc == null ? null : "'" + obj.h2genitourinariophc + "'");
				arrValores.Add(obj.h2extremidadesphc == null ? null : "'" + obj.h2extremidadesphc + "'");
				arrValores.Add(obj.h2conalertaphc == null ? null : "'" + obj.h2conalertaphc + "'");
				arrValores.Add(obj.h2conconfusionphc == null ? null : "'" + obj.h2conconfusionphc + "'");
				arrValores.Add(obj.h2consomnolenciaphc == null ? null : "'" + obj.h2consomnolenciaphc + "'");
				arrValores.Add(obj.h2conestuforphc == null ? null : "'" + obj.h2conestuforphc + "'");
				arrValores.Add(obj.h2concomaphc == null ? null : "'" + obj.h2concomaphc + "'");
				arrValores.Add(obj.h2oriespaciophc == null ? null : "'" + obj.h2oriespaciophc + "'");
				arrValores.Add(obj.h2orilugarphc == null ? null : "'" + obj.h2orilugarphc + "'");
				arrValores.Add(obj.h2oripersonaphc == null ? null : "'" + obj.h2oripersonaphc + "'");
				arrValores.Add(obj.h2razideaphc == null ? null : "'" + obj.h2razideaphc + "'");
				arrValores.Add(obj.h2razjuiciophc == null ? null : "'" + obj.h2razjuiciophc + "'");
				arrValores.Add(obj.h2razabstraccionphc == null ? null : "'" + obj.h2razabstraccionphc + "'");
				arrValores.Add(obj.h2conocimientosgenerales == null ? null : "'" + obj.h2conocimientosgenerales + "'");
				arrValores.Add(obj.h2atgdesatentophc == null ? null : "'" + obj.h2atgdesatentophc + "'");
				arrValores.Add(obj.h2atgfluctuantephc == null ? null : "'" + obj.h2atgfluctuantephc + "'");
				arrValores.Add(obj.h2atgnegativophc == null ? null : "'" + obj.h2atgnegativophc + "'");
				arrValores.Add(obj.h2atgnegligentephc == null ? null : "'" + obj.h2atgnegligentephc + "'");
				arrValores.Add(obj.h2pagconfusionphc == null ? null : "'" + obj.h2pagconfusionphc + "'");
				arrValores.Add(obj.h2pagdeliriophc == null ? null : "'" + obj.h2pagdeliriophc + "'");
				arrValores.Add(obj.h2paedelusionesphc == null ? null : "'" + obj.h2paedelusionesphc + "'");
				arrValores.Add(obj.h2paeilsuionesphc == null ? null : "'" + obj.h2paeilsuionesphc + "'");
				arrValores.Add(obj.h2paealucinacionesphc == null ? null : "'" + obj.h2paealucinacionesphc + "'");
				arrValores.Add(obj.h3conrecientephc == null ? null : "'" + obj.h3conrecientephc + "'");
				arrValores.Add(obj.h3conremotaphc == null ? null : "'" + obj.h3conremotaphc + "'");
				arrValores.Add(obj.h3afeeuforicophc == null ? null : "'" + obj.h3afeeuforicophc + "'");
				arrValores.Add(obj.h3afeplanophc == null ? null : "'" + obj.h3afeplanophc + "'");
				arrValores.Add(obj.h3afedeprimidophc == null ? null : "'" + obj.h3afedeprimidophc + "'");
				arrValores.Add(obj.h3afelabilphc == null ? null : "'" + obj.h3afelabilphc + "'");
				arrValores.Add(obj.h3afehostilphc == null ? null : "'" + obj.h3afehostilphc + "'");
				arrValores.Add(obj.h3afeinadecuadophc == null ? null : "'" + obj.h3afeinadecuadophc + "'");
				arrValores.Add(obj.h3hdoizquierdophc == null ? null : "'" + obj.h3hdoizquierdophc + "'");
				arrValores.Add(obj.h3hdoderechophc == null ? null : "'" + obj.h3hdoderechophc + "'");
				arrValores.Add(obj.h3cgeexpresionphc == null ? null : "'" + obj.h3cgeexpresionphc + "'");
				arrValores.Add(obj.h3cgenominacionphc == null ? null : "'" + obj.h3cgenominacionphc + "'");
				arrValores.Add(obj.h3cgerepeticionphc == null ? null : "'" + obj.h3cgerepeticionphc + "'");
				arrValores.Add(obj.h3cgecomprensionphc == null ? null : "'" + obj.h3cgecomprensionphc + "'");
				arrValores.Add(obj.h3apebrocaphc == null ? null : "'" + obj.h3apebrocaphc + "'");
				arrValores.Add(obj.h3apeconduccionphc == null ? null : "'" + obj.h3apeconduccionphc + "'");
				arrValores.Add(obj.h3apewernickephc == null ? null : "'" + obj.h3apewernickephc + "'");
				arrValores.Add(obj.h3apeglobalphc == null ? null : "'" + obj.h3apeglobalphc + "'");
				arrValores.Add(obj.h3apeanomiaphc == null ? null : "'" + obj.h3apeanomiaphc + "'");
				arrValores.Add(obj.h3atrmotoraphc == null ? null : "'" + obj.h3atrmotoraphc + "'");
				arrValores.Add(obj.h3atrsensitivaphc == null ? null : "'" + obj.h3atrsensitivaphc + "'");
				arrValores.Add(obj.h3atrmixtaphc == null ? null : "'" + obj.h3atrmixtaphc + "'");
				arrValores.Add(obj.h3ataexpresionphc == null ? null : "'" + obj.h3ataexpresionphc + "'");
				arrValores.Add(obj.h3atacomprensionphc == null ? null : "'" + obj.h3atacomprensionphc + "'");
				arrValores.Add(obj.h3atamixtaphc == null ? null : "'" + obj.h3atamixtaphc + "'");
				arrValores.Add(obj.h3aprmotoraphc == null ? null : "'" + obj.h3aprmotoraphc + "'");
				arrValores.Add(obj.h3aprsensorialphc == null ? null : "'" + obj.h3aprsensorialphc + "'");
				arrValores.Add(obj.h3lesalexiaphc == null ? null : "'" + obj.h3lesalexiaphc + "'");
				arrValores.Add(obj.h3lesagrafiaphc == null ? null : "'" + obj.h3lesagrafiaphc + "'");
				arrValores.Add(obj.h3lpdacalculiaphc == null ? null : "'" + obj.h3lpdacalculiaphc + "'");
				arrValores.Add(obj.h3lpdagrafiaphc == null ? null : "'" + obj.h3lpdagrafiaphc + "'");
				arrValores.Add(obj.h3lpddesorientacionphc == null ? null : "'" + obj.h3lpddesorientacionphc + "'");
				arrValores.Add(obj.h3lpdagnosiaphc == null ? null : "'" + obj.h3lpdagnosiaphc + "'");
				arrValores.Add(obj.h3agnauditivaphc == null ? null : "'" + obj.h3agnauditivaphc + "'");
				arrValores.Add(obj.h3agnvisualphc == null ? null : "'" + obj.h3agnvisualphc + "'");
				arrValores.Add(obj.h3agntactilphc == null ? null : "'" + obj.h3agntactilphc + "'");
				arrValores.Add(obj.h3aprideacionalphc == null ? null : "'" + obj.h3aprideacionalphc + "'");
				arrValores.Add(obj.h3aprideomotoraphc == null ? null : "'" + obj.h3aprideomotoraphc + "'");
				arrValores.Add(obj.h3aprdesatencionphc == null ? null : "'" + obj.h3aprdesatencionphc + "'");
				arrValores.Add(obj.h3anosognosiaphc == null ? null : "'" + obj.h3anosognosiaphc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica1
		/// </returns>
		public bool InsertQuery(EntPacHistoriaclinica1 obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.h1motivophc == null ? null : "'" + obj.h1motivophc + "'");
				arrValores.Add(obj.h1enfermedadphc == null ? null : "'" + obj.h1enfermedadphc + "'");
				arrValores.Add(obj.h1cirujiasphc == null ? null : "'" + obj.h1cirujiasphc + "'");
				arrValores.Add(obj.h1infeccionesphc == null ? null : "'" + obj.h1infeccionesphc + "'");
				arrValores.Add(obj.h1traumatismosphc == null ? null : "'" + obj.h1traumatismosphc + "'");
				arrValores.Add(obj.h1cardiovascularphc == null ? null : "'" + obj.h1cardiovascularphc + "'");
				arrValores.Add(obj.h1metabolicophc == null ? null : "'" + obj.h1metabolicophc + "'");
				arrValores.Add(obj.h1toxicophc == null ? null : "'" + obj.h1toxicophc + "'");
				arrValores.Add(obj.h1familiarphc == null ? null : "'" + obj.h1familiarphc + "'");
				arrValores.Add(obj.h1ginecoobsphc == null ? null : "'" + obj.h1ginecoobsphc + "'");
				arrValores.Add(obj.h1otrosphc == null ? null : "'" + obj.h1otrosphc + "'");
				arrValores.Add(obj.h1embarazosphc);
				arrValores.Add(obj.h1gestaphc);
				arrValores.Add(obj.h1abortosphc);
				arrValores.Add(obj.h1mesntruacionesphc == null ? null : "'" + obj.h1mesntruacionesphc + "'");
				arrValores.Add(obj.h1fumphc == null ? null : "'" + obj.h1fumphc + "'");
				arrValores.Add(obj.h1fupphc == null ? null : "'" + obj.h1fupphc + "'");
				arrValores.Add(obj.h1revisionphc == null ? null : "'" + obj.h1revisionphc + "'");
				arrValores.Add(obj.h1examenfisicophc == null ? null : "'" + obj.h1examenfisicophc + "'");
				arrValores.Add(obj.h1taphc == null ? null : "'" + obj.h1taphc + "'");
				arrValores.Add(obj.h1fcphc == null ? null : "'" + obj.h1fcphc + "'");
				arrValores.Add(obj.h1frphc == null ? null : "'" + obj.h1frphc + "'");
				arrValores.Add(obj.h1tempphc == null ? null : "'" + obj.h1tempphc + "'");
				arrValores.Add(obj.h1tallaphc == null ? null : "'" + obj.h1tallaphc + "'");
				arrValores.Add(obj.h1pesophc == null ? null : "'" + obj.h1pesophc + "'");
				arrValores.Add(obj.h1pcphc == null ? null : "'" + obj.h1pcphc + "'");
				arrValores.Add(obj.h2cabezaphc == null ? null : "'" + obj.h2cabezaphc + "'");
				arrValores.Add(obj.h2cuellophc == null ? null : "'" + obj.h2cuellophc + "'");
				arrValores.Add(obj.h2toraxphc == null ? null : "'" + obj.h2toraxphc + "'");
				arrValores.Add(obj.h2abdomenphc == null ? null : "'" + obj.h2abdomenphc + "'");
				arrValores.Add(obj.h2genitourinariophc == null ? null : "'" + obj.h2genitourinariophc + "'");
				arrValores.Add(obj.h2extremidadesphc == null ? null : "'" + obj.h2extremidadesphc + "'");
				arrValores.Add(obj.h2conalertaphc == null ? null : "'" + obj.h2conalertaphc + "'");
				arrValores.Add(obj.h2conconfusionphc == null ? null : "'" + obj.h2conconfusionphc + "'");
				arrValores.Add(obj.h2consomnolenciaphc == null ? null : "'" + obj.h2consomnolenciaphc + "'");
				arrValores.Add(obj.h2conestuforphc == null ? null : "'" + obj.h2conestuforphc + "'");
				arrValores.Add(obj.h2concomaphc == null ? null : "'" + obj.h2concomaphc + "'");
				arrValores.Add(obj.h2oriespaciophc == null ? null : "'" + obj.h2oriespaciophc + "'");
				arrValores.Add(obj.h2orilugarphc == null ? null : "'" + obj.h2orilugarphc + "'");
				arrValores.Add(obj.h2oripersonaphc == null ? null : "'" + obj.h2oripersonaphc + "'");
				arrValores.Add(obj.h2razideaphc == null ? null : "'" + obj.h2razideaphc + "'");
				arrValores.Add(obj.h2razjuiciophc == null ? null : "'" + obj.h2razjuiciophc + "'");
				arrValores.Add(obj.h2razabstraccionphc == null ? null : "'" + obj.h2razabstraccionphc + "'");
				arrValores.Add(obj.h2conocimientosgenerales == null ? null : "'" + obj.h2conocimientosgenerales + "'");
				arrValores.Add(obj.h2atgdesatentophc == null ? null : "'" + obj.h2atgdesatentophc + "'");
				arrValores.Add(obj.h2atgfluctuantephc == null ? null : "'" + obj.h2atgfluctuantephc + "'");
				arrValores.Add(obj.h2atgnegativophc == null ? null : "'" + obj.h2atgnegativophc + "'");
				arrValores.Add(obj.h2atgnegligentephc == null ? null : "'" + obj.h2atgnegligentephc + "'");
				arrValores.Add(obj.h2pagconfusionphc == null ? null : "'" + obj.h2pagconfusionphc + "'");
				arrValores.Add(obj.h2pagdeliriophc == null ? null : "'" + obj.h2pagdeliriophc + "'");
				arrValores.Add(obj.h2paedelusionesphc == null ? null : "'" + obj.h2paedelusionesphc + "'");
				arrValores.Add(obj.h2paeilsuionesphc == null ? null : "'" + obj.h2paeilsuionesphc + "'");
				arrValores.Add(obj.h2paealucinacionesphc == null ? null : "'" + obj.h2paealucinacionesphc + "'");
				arrValores.Add(obj.h3conrecientephc == null ? null : "'" + obj.h3conrecientephc + "'");
				arrValores.Add(obj.h3conremotaphc == null ? null : "'" + obj.h3conremotaphc + "'");
				arrValores.Add(obj.h3afeeuforicophc == null ? null : "'" + obj.h3afeeuforicophc + "'");
				arrValores.Add(obj.h3afeplanophc == null ? null : "'" + obj.h3afeplanophc + "'");
				arrValores.Add(obj.h3afedeprimidophc == null ? null : "'" + obj.h3afedeprimidophc + "'");
				arrValores.Add(obj.h3afelabilphc == null ? null : "'" + obj.h3afelabilphc + "'");
				arrValores.Add(obj.h3afehostilphc == null ? null : "'" + obj.h3afehostilphc + "'");
				arrValores.Add(obj.h3afeinadecuadophc == null ? null : "'" + obj.h3afeinadecuadophc + "'");
				arrValores.Add(obj.h3hdoizquierdophc == null ? null : "'" + obj.h3hdoizquierdophc + "'");
				arrValores.Add(obj.h3hdoderechophc == null ? null : "'" + obj.h3hdoderechophc + "'");
				arrValores.Add(obj.h3cgeexpresionphc == null ? null : "'" + obj.h3cgeexpresionphc + "'");
				arrValores.Add(obj.h3cgenominacionphc == null ? null : "'" + obj.h3cgenominacionphc + "'");
				arrValores.Add(obj.h3cgerepeticionphc == null ? null : "'" + obj.h3cgerepeticionphc + "'");
				arrValores.Add(obj.h3cgecomprensionphc == null ? null : "'" + obj.h3cgecomprensionphc + "'");
				arrValores.Add(obj.h3apebrocaphc == null ? null : "'" + obj.h3apebrocaphc + "'");
				arrValores.Add(obj.h3apeconduccionphc == null ? null : "'" + obj.h3apeconduccionphc + "'");
				arrValores.Add(obj.h3apewernickephc == null ? null : "'" + obj.h3apewernickephc + "'");
				arrValores.Add(obj.h3apeglobalphc == null ? null : "'" + obj.h3apeglobalphc + "'");
				arrValores.Add(obj.h3apeanomiaphc == null ? null : "'" + obj.h3apeanomiaphc + "'");
				arrValores.Add(obj.h3atrmotoraphc == null ? null : "'" + obj.h3atrmotoraphc + "'");
				arrValores.Add(obj.h3atrsensitivaphc == null ? null : "'" + obj.h3atrsensitivaphc + "'");
				arrValores.Add(obj.h3atrmixtaphc == null ? null : "'" + obj.h3atrmixtaphc + "'");
				arrValores.Add(obj.h3ataexpresionphc == null ? null : "'" + obj.h3ataexpresionphc + "'");
				arrValores.Add(obj.h3atacomprensionphc == null ? null : "'" + obj.h3atacomprensionphc + "'");
				arrValores.Add(obj.h3atamixtaphc == null ? null : "'" + obj.h3atamixtaphc + "'");
				arrValores.Add(obj.h3aprmotoraphc == null ? null : "'" + obj.h3aprmotoraphc + "'");
				arrValores.Add(obj.h3aprsensorialphc == null ? null : "'" + obj.h3aprsensorialphc + "'");
				arrValores.Add(obj.h3lesalexiaphc == null ? null : "'" + obj.h3lesalexiaphc + "'");
				arrValores.Add(obj.h3lesagrafiaphc == null ? null : "'" + obj.h3lesagrafiaphc + "'");
				arrValores.Add(obj.h3lpdacalculiaphc == null ? null : "'" + obj.h3lpdacalculiaphc + "'");
				arrValores.Add(obj.h3lpdagrafiaphc == null ? null : "'" + obj.h3lpdagrafiaphc + "'");
				arrValores.Add(obj.h3lpddesorientacionphc == null ? null : "'" + obj.h3lpddesorientacionphc + "'");
				arrValores.Add(obj.h3lpdagnosiaphc == null ? null : "'" + obj.h3lpdagnosiaphc + "'");
				arrValores.Add(obj.h3agnauditivaphc == null ? null : "'" + obj.h3agnauditivaphc + "'");
				arrValores.Add(obj.h3agnvisualphc == null ? null : "'" + obj.h3agnvisualphc + "'");
				arrValores.Add(obj.h3agntactilphc == null ? null : "'" + obj.h3agntactilphc + "'");
				arrValores.Add(obj.h3aprideacionalphc == null ? null : "'" + obj.h3aprideacionalphc + "'");
				arrValores.Add(obj.h3aprideomotoraphc == null ? null : "'" + obj.h3aprideomotoraphc + "'");
				arrValores.Add(obj.h3aprdesatencionphc == null ? null : "'" + obj.h3aprdesatencionphc + "'");
				arrValores.Add(obj.h3anosognosiaphc == null ? null : "'" + obj.h3anosognosiaphc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica1
		/// </returns>
		public bool InsertQueryIdentity(EntPacHistoriaclinica1 obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.h1motivophc == null ? null : "'" + obj.h1motivophc + "'");
				arrValores.Add(obj.h1enfermedadphc == null ? null : "'" + obj.h1enfermedadphc + "'");
				arrValores.Add(obj.h1cirujiasphc == null ? null : "'" + obj.h1cirujiasphc + "'");
				arrValores.Add(obj.h1infeccionesphc == null ? null : "'" + obj.h1infeccionesphc + "'");
				arrValores.Add(obj.h1traumatismosphc == null ? null : "'" + obj.h1traumatismosphc + "'");
				arrValores.Add(obj.h1cardiovascularphc == null ? null : "'" + obj.h1cardiovascularphc + "'");
				arrValores.Add(obj.h1metabolicophc == null ? null : "'" + obj.h1metabolicophc + "'");
				arrValores.Add(obj.h1toxicophc == null ? null : "'" + obj.h1toxicophc + "'");
				arrValores.Add(obj.h1familiarphc == null ? null : "'" + obj.h1familiarphc + "'");
				arrValores.Add(obj.h1ginecoobsphc == null ? null : "'" + obj.h1ginecoobsphc + "'");
				arrValores.Add(obj.h1otrosphc == null ? null : "'" + obj.h1otrosphc + "'");
				arrValores.Add(obj.h1embarazosphc);
				arrValores.Add(obj.h1gestaphc);
				arrValores.Add(obj.h1abortosphc);
				arrValores.Add(obj.h1mesntruacionesphc == null ? null : "'" + obj.h1mesntruacionesphc + "'");
				arrValores.Add(obj.h1fumphc == null ? null : "'" + obj.h1fumphc + "'");
				arrValores.Add(obj.h1fupphc == null ? null : "'" + obj.h1fupphc + "'");
				arrValores.Add(obj.h1revisionphc == null ? null : "'" + obj.h1revisionphc + "'");
				arrValores.Add(obj.h1examenfisicophc == null ? null : "'" + obj.h1examenfisicophc + "'");
				arrValores.Add(obj.h1taphc == null ? null : "'" + obj.h1taphc + "'");
				arrValores.Add(obj.h1fcphc == null ? null : "'" + obj.h1fcphc + "'");
				arrValores.Add(obj.h1frphc == null ? null : "'" + obj.h1frphc + "'");
				arrValores.Add(obj.h1tempphc == null ? null : "'" + obj.h1tempphc + "'");
				arrValores.Add(obj.h1tallaphc == null ? null : "'" + obj.h1tallaphc + "'");
				arrValores.Add(obj.h1pesophc == null ? null : "'" + obj.h1pesophc + "'");
				arrValores.Add(obj.h1pcphc == null ? null : "'" + obj.h1pcphc + "'");
				arrValores.Add(obj.h2cabezaphc == null ? null : "'" + obj.h2cabezaphc + "'");
				arrValores.Add(obj.h2cuellophc == null ? null : "'" + obj.h2cuellophc + "'");
				arrValores.Add(obj.h2toraxphc == null ? null : "'" + obj.h2toraxphc + "'");
				arrValores.Add(obj.h2abdomenphc == null ? null : "'" + obj.h2abdomenphc + "'");
				arrValores.Add(obj.h2genitourinariophc == null ? null : "'" + obj.h2genitourinariophc + "'");
				arrValores.Add(obj.h2extremidadesphc == null ? null : "'" + obj.h2extremidadesphc + "'");
				arrValores.Add(obj.h2conalertaphc == null ? null : "'" + obj.h2conalertaphc + "'");
				arrValores.Add(obj.h2conconfusionphc == null ? null : "'" + obj.h2conconfusionphc + "'");
				arrValores.Add(obj.h2consomnolenciaphc == null ? null : "'" + obj.h2consomnolenciaphc + "'");
				arrValores.Add(obj.h2conestuforphc == null ? null : "'" + obj.h2conestuforphc + "'");
				arrValores.Add(obj.h2concomaphc == null ? null : "'" + obj.h2concomaphc + "'");
				arrValores.Add(obj.h2oriespaciophc == null ? null : "'" + obj.h2oriespaciophc + "'");
				arrValores.Add(obj.h2orilugarphc == null ? null : "'" + obj.h2orilugarphc + "'");
				arrValores.Add(obj.h2oripersonaphc == null ? null : "'" + obj.h2oripersonaphc + "'");
				arrValores.Add(obj.h2razideaphc == null ? null : "'" + obj.h2razideaphc + "'");
				arrValores.Add(obj.h2razjuiciophc == null ? null : "'" + obj.h2razjuiciophc + "'");
				arrValores.Add(obj.h2razabstraccionphc == null ? null : "'" + obj.h2razabstraccionphc + "'");
				arrValores.Add(obj.h2conocimientosgenerales == null ? null : "'" + obj.h2conocimientosgenerales + "'");
				arrValores.Add(obj.h2atgdesatentophc == null ? null : "'" + obj.h2atgdesatentophc + "'");
				arrValores.Add(obj.h2atgfluctuantephc == null ? null : "'" + obj.h2atgfluctuantephc + "'");
				arrValores.Add(obj.h2atgnegativophc == null ? null : "'" + obj.h2atgnegativophc + "'");
				arrValores.Add(obj.h2atgnegligentephc == null ? null : "'" + obj.h2atgnegligentephc + "'");
				arrValores.Add(obj.h2pagconfusionphc == null ? null : "'" + obj.h2pagconfusionphc + "'");
				arrValores.Add(obj.h2pagdeliriophc == null ? null : "'" + obj.h2pagdeliriophc + "'");
				arrValores.Add(obj.h2paedelusionesphc == null ? null : "'" + obj.h2paedelusionesphc + "'");
				arrValores.Add(obj.h2paeilsuionesphc == null ? null : "'" + obj.h2paeilsuionesphc + "'");
				arrValores.Add(obj.h2paealucinacionesphc == null ? null : "'" + obj.h2paealucinacionesphc + "'");
				arrValores.Add(obj.h3conrecientephc == null ? null : "'" + obj.h3conrecientephc + "'");
				arrValores.Add(obj.h3conremotaphc == null ? null : "'" + obj.h3conremotaphc + "'");
				arrValores.Add(obj.h3afeeuforicophc == null ? null : "'" + obj.h3afeeuforicophc + "'");
				arrValores.Add(obj.h3afeplanophc == null ? null : "'" + obj.h3afeplanophc + "'");
				arrValores.Add(obj.h3afedeprimidophc == null ? null : "'" + obj.h3afedeprimidophc + "'");
				arrValores.Add(obj.h3afelabilphc == null ? null : "'" + obj.h3afelabilphc + "'");
				arrValores.Add(obj.h3afehostilphc == null ? null : "'" + obj.h3afehostilphc + "'");
				arrValores.Add(obj.h3afeinadecuadophc == null ? null : "'" + obj.h3afeinadecuadophc + "'");
				arrValores.Add(obj.h3hdoizquierdophc == null ? null : "'" + obj.h3hdoizquierdophc + "'");
				arrValores.Add(obj.h3hdoderechophc == null ? null : "'" + obj.h3hdoderechophc + "'");
				arrValores.Add(obj.h3cgeexpresionphc == null ? null : "'" + obj.h3cgeexpresionphc + "'");
				arrValores.Add(obj.h3cgenominacionphc == null ? null : "'" + obj.h3cgenominacionphc + "'");
				arrValores.Add(obj.h3cgerepeticionphc == null ? null : "'" + obj.h3cgerepeticionphc + "'");
				arrValores.Add(obj.h3cgecomprensionphc == null ? null : "'" + obj.h3cgecomprensionphc + "'");
				arrValores.Add(obj.h3apebrocaphc == null ? null : "'" + obj.h3apebrocaphc + "'");
				arrValores.Add(obj.h3apeconduccionphc == null ? null : "'" + obj.h3apeconduccionphc + "'");
				arrValores.Add(obj.h3apewernickephc == null ? null : "'" + obj.h3apewernickephc + "'");
				arrValores.Add(obj.h3apeglobalphc == null ? null : "'" + obj.h3apeglobalphc + "'");
				arrValores.Add(obj.h3apeanomiaphc == null ? null : "'" + obj.h3apeanomiaphc + "'");
				arrValores.Add(obj.h3atrmotoraphc == null ? null : "'" + obj.h3atrmotoraphc + "'");
				arrValores.Add(obj.h3atrsensitivaphc == null ? null : "'" + obj.h3atrsensitivaphc + "'");
				arrValores.Add(obj.h3atrmixtaphc == null ? null : "'" + obj.h3atrmixtaphc + "'");
				arrValores.Add(obj.h3ataexpresionphc == null ? null : "'" + obj.h3ataexpresionphc + "'");
				arrValores.Add(obj.h3atacomprensionphc == null ? null : "'" + obj.h3atacomprensionphc + "'");
				arrValores.Add(obj.h3atamixtaphc == null ? null : "'" + obj.h3atamixtaphc + "'");
				arrValores.Add(obj.h3aprmotoraphc == null ? null : "'" + obj.h3aprmotoraphc + "'");
				arrValores.Add(obj.h3aprsensorialphc == null ? null : "'" + obj.h3aprsensorialphc + "'");
				arrValores.Add(obj.h3lesalexiaphc == null ? null : "'" + obj.h3lesalexiaphc + "'");
				arrValores.Add(obj.h3lesagrafiaphc == null ? null : "'" + obj.h3lesagrafiaphc + "'");
				arrValores.Add(obj.h3lpdacalculiaphc == null ? null : "'" + obj.h3lpdacalculiaphc + "'");
				arrValores.Add(obj.h3lpdagrafiaphc == null ? null : "'" + obj.h3lpdagrafiaphc + "'");
				arrValores.Add(obj.h3lpddesorientacionphc == null ? null : "'" + obj.h3lpddesorientacionphc + "'");
				arrValores.Add(obj.h3lpdagnosiaphc == null ? null : "'" + obj.h3lpdagnosiaphc + "'");
				arrValores.Add(obj.h3agnauditivaphc == null ? null : "'" + obj.h3agnauditivaphc + "'");
				arrValores.Add(obj.h3agnvisualphc == null ? null : "'" + obj.h3agnvisualphc + "'");
				arrValores.Add(obj.h3agntactilphc == null ? null : "'" + obj.h3agntactilphc + "'");
				arrValores.Add(obj.h3aprideacionalphc == null ? null : "'" + obj.h3aprideacionalphc + "'");
				arrValores.Add(obj.h3aprideomotoraphc == null ? null : "'" + obj.h3aprideomotoraphc + "'");
				arrValores.Add(obj.h3aprdesatencionphc == null ? null : "'" + obj.h3aprdesatencionphc + "'");
				arrValores.Add(obj.h3anosognosiaphc == null ? null : "'" + obj.h3anosognosiaphc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.idppa = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica1
		/// </returns>
		public bool InsertQueryIdentity(EntPacHistoriaclinica1 obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.h1motivophc == null ? null : "'" + obj.h1motivophc + "'");
				arrValores.Add(obj.h1enfermedadphc == null ? null : "'" + obj.h1enfermedadphc + "'");
				arrValores.Add(obj.h1cirujiasphc == null ? null : "'" + obj.h1cirujiasphc + "'");
				arrValores.Add(obj.h1infeccionesphc == null ? null : "'" + obj.h1infeccionesphc + "'");
				arrValores.Add(obj.h1traumatismosphc == null ? null : "'" + obj.h1traumatismosphc + "'");
				arrValores.Add(obj.h1cardiovascularphc == null ? null : "'" + obj.h1cardiovascularphc + "'");
				arrValores.Add(obj.h1metabolicophc == null ? null : "'" + obj.h1metabolicophc + "'");
				arrValores.Add(obj.h1toxicophc == null ? null : "'" + obj.h1toxicophc + "'");
				arrValores.Add(obj.h1familiarphc == null ? null : "'" + obj.h1familiarphc + "'");
				arrValores.Add(obj.h1ginecoobsphc == null ? null : "'" + obj.h1ginecoobsphc + "'");
				arrValores.Add(obj.h1otrosphc == null ? null : "'" + obj.h1otrosphc + "'");
				arrValores.Add(obj.h1embarazosphc);
				arrValores.Add(obj.h1gestaphc);
				arrValores.Add(obj.h1abortosphc);
				arrValores.Add(obj.h1mesntruacionesphc == null ? null : "'" + obj.h1mesntruacionesphc + "'");
				arrValores.Add(obj.h1fumphc == null ? null : "'" + obj.h1fumphc + "'");
				arrValores.Add(obj.h1fupphc == null ? null : "'" + obj.h1fupphc + "'");
				arrValores.Add(obj.h1revisionphc == null ? null : "'" + obj.h1revisionphc + "'");
				arrValores.Add(obj.h1examenfisicophc == null ? null : "'" + obj.h1examenfisicophc + "'");
				arrValores.Add(obj.h1taphc == null ? null : "'" + obj.h1taphc + "'");
				arrValores.Add(obj.h1fcphc == null ? null : "'" + obj.h1fcphc + "'");
				arrValores.Add(obj.h1frphc == null ? null : "'" + obj.h1frphc + "'");
				arrValores.Add(obj.h1tempphc == null ? null : "'" + obj.h1tempphc + "'");
				arrValores.Add(obj.h1tallaphc == null ? null : "'" + obj.h1tallaphc + "'");
				arrValores.Add(obj.h1pesophc == null ? null : "'" + obj.h1pesophc + "'");
				arrValores.Add(obj.h1pcphc == null ? null : "'" + obj.h1pcphc + "'");
				arrValores.Add(obj.h2cabezaphc == null ? null : "'" + obj.h2cabezaphc + "'");
				arrValores.Add(obj.h2cuellophc == null ? null : "'" + obj.h2cuellophc + "'");
				arrValores.Add(obj.h2toraxphc == null ? null : "'" + obj.h2toraxphc + "'");
				arrValores.Add(obj.h2abdomenphc == null ? null : "'" + obj.h2abdomenphc + "'");
				arrValores.Add(obj.h2genitourinariophc == null ? null : "'" + obj.h2genitourinariophc + "'");
				arrValores.Add(obj.h2extremidadesphc == null ? null : "'" + obj.h2extremidadesphc + "'");
				arrValores.Add(obj.h2conalertaphc == null ? null : "'" + obj.h2conalertaphc + "'");
				arrValores.Add(obj.h2conconfusionphc == null ? null : "'" + obj.h2conconfusionphc + "'");
				arrValores.Add(obj.h2consomnolenciaphc == null ? null : "'" + obj.h2consomnolenciaphc + "'");
				arrValores.Add(obj.h2conestuforphc == null ? null : "'" + obj.h2conestuforphc + "'");
				arrValores.Add(obj.h2concomaphc == null ? null : "'" + obj.h2concomaphc + "'");
				arrValores.Add(obj.h2oriespaciophc == null ? null : "'" + obj.h2oriespaciophc + "'");
				arrValores.Add(obj.h2orilugarphc == null ? null : "'" + obj.h2orilugarphc + "'");
				arrValores.Add(obj.h2oripersonaphc == null ? null : "'" + obj.h2oripersonaphc + "'");
				arrValores.Add(obj.h2razideaphc == null ? null : "'" + obj.h2razideaphc + "'");
				arrValores.Add(obj.h2razjuiciophc == null ? null : "'" + obj.h2razjuiciophc + "'");
				arrValores.Add(obj.h2razabstraccionphc == null ? null : "'" + obj.h2razabstraccionphc + "'");
				arrValores.Add(obj.h2conocimientosgenerales == null ? null : "'" + obj.h2conocimientosgenerales + "'");
				arrValores.Add(obj.h2atgdesatentophc == null ? null : "'" + obj.h2atgdesatentophc + "'");
				arrValores.Add(obj.h2atgfluctuantephc == null ? null : "'" + obj.h2atgfluctuantephc + "'");
				arrValores.Add(obj.h2atgnegativophc == null ? null : "'" + obj.h2atgnegativophc + "'");
				arrValores.Add(obj.h2atgnegligentephc == null ? null : "'" + obj.h2atgnegligentephc + "'");
				arrValores.Add(obj.h2pagconfusionphc == null ? null : "'" + obj.h2pagconfusionphc + "'");
				arrValores.Add(obj.h2pagdeliriophc == null ? null : "'" + obj.h2pagdeliriophc + "'");
				arrValores.Add(obj.h2paedelusionesphc == null ? null : "'" + obj.h2paedelusionesphc + "'");
				arrValores.Add(obj.h2paeilsuionesphc == null ? null : "'" + obj.h2paeilsuionesphc + "'");
				arrValores.Add(obj.h2paealucinacionesphc == null ? null : "'" + obj.h2paealucinacionesphc + "'");
				arrValores.Add(obj.h3conrecientephc == null ? null : "'" + obj.h3conrecientephc + "'");
				arrValores.Add(obj.h3conremotaphc == null ? null : "'" + obj.h3conremotaphc + "'");
				arrValores.Add(obj.h3afeeuforicophc == null ? null : "'" + obj.h3afeeuforicophc + "'");
				arrValores.Add(obj.h3afeplanophc == null ? null : "'" + obj.h3afeplanophc + "'");
				arrValores.Add(obj.h3afedeprimidophc == null ? null : "'" + obj.h3afedeprimidophc + "'");
				arrValores.Add(obj.h3afelabilphc == null ? null : "'" + obj.h3afelabilphc + "'");
				arrValores.Add(obj.h3afehostilphc == null ? null : "'" + obj.h3afehostilphc + "'");
				arrValores.Add(obj.h3afeinadecuadophc == null ? null : "'" + obj.h3afeinadecuadophc + "'");
				arrValores.Add(obj.h3hdoizquierdophc == null ? null : "'" + obj.h3hdoizquierdophc + "'");
				arrValores.Add(obj.h3hdoderechophc == null ? null : "'" + obj.h3hdoderechophc + "'");
				arrValores.Add(obj.h3cgeexpresionphc == null ? null : "'" + obj.h3cgeexpresionphc + "'");
				arrValores.Add(obj.h3cgenominacionphc == null ? null : "'" + obj.h3cgenominacionphc + "'");
				arrValores.Add(obj.h3cgerepeticionphc == null ? null : "'" + obj.h3cgerepeticionphc + "'");
				arrValores.Add(obj.h3cgecomprensionphc == null ? null : "'" + obj.h3cgecomprensionphc + "'");
				arrValores.Add(obj.h3apebrocaphc == null ? null : "'" + obj.h3apebrocaphc + "'");
				arrValores.Add(obj.h3apeconduccionphc == null ? null : "'" + obj.h3apeconduccionphc + "'");
				arrValores.Add(obj.h3apewernickephc == null ? null : "'" + obj.h3apewernickephc + "'");
				arrValores.Add(obj.h3apeglobalphc == null ? null : "'" + obj.h3apeglobalphc + "'");
				arrValores.Add(obj.h3apeanomiaphc == null ? null : "'" + obj.h3apeanomiaphc + "'");
				arrValores.Add(obj.h3atrmotoraphc == null ? null : "'" + obj.h3atrmotoraphc + "'");
				arrValores.Add(obj.h3atrsensitivaphc == null ? null : "'" + obj.h3atrsensitivaphc + "'");
				arrValores.Add(obj.h3atrmixtaphc == null ? null : "'" + obj.h3atrmixtaphc + "'");
				arrValores.Add(obj.h3ataexpresionphc == null ? null : "'" + obj.h3ataexpresionphc + "'");
				arrValores.Add(obj.h3atacomprensionphc == null ? null : "'" + obj.h3atacomprensionphc + "'");
				arrValores.Add(obj.h3atamixtaphc == null ? null : "'" + obj.h3atamixtaphc + "'");
				arrValores.Add(obj.h3aprmotoraphc == null ? null : "'" + obj.h3aprmotoraphc + "'");
				arrValores.Add(obj.h3aprsensorialphc == null ? null : "'" + obj.h3aprsensorialphc + "'");
				arrValores.Add(obj.h3lesalexiaphc == null ? null : "'" + obj.h3lesalexiaphc + "'");
				arrValores.Add(obj.h3lesagrafiaphc == null ? null : "'" + obj.h3lesagrafiaphc + "'");
				arrValores.Add(obj.h3lpdacalculiaphc == null ? null : "'" + obj.h3lpdacalculiaphc + "'");
				arrValores.Add(obj.h3lpdagrafiaphc == null ? null : "'" + obj.h3lpdagrafiaphc + "'");
				arrValores.Add(obj.h3lpddesorientacionphc == null ? null : "'" + obj.h3lpddesorientacionphc + "'");
				arrValores.Add(obj.h3lpdagnosiaphc == null ? null : "'" + obj.h3lpdagnosiaphc + "'");
				arrValores.Add(obj.h3agnauditivaphc == null ? null : "'" + obj.h3agnauditivaphc + "'");
				arrValores.Add(obj.h3agnvisualphc == null ? null : "'" + obj.h3agnvisualphc + "'");
				arrValores.Add(obj.h3agntactilphc == null ? null : "'" + obj.h3agntactilphc + "'");
				arrValores.Add(obj.h3aprideacionalphc == null ? null : "'" + obj.h3aprideacionalphc + "'");
				arrValores.Add(obj.h3aprideomotoraphc == null ? null : "'" + obj.h3aprideomotoraphc + "'");
				arrValores.Add(obj.h3aprdesatencionphc == null ? null : "'" + obj.h3aprdesatencionphc + "'");
				arrValores.Add(obj.h3anosognosiaphc == null ? null : "'" + obj.h3anosognosiaphc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.idppa = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpachistoriaclinica1
		/// </returns>
		public int UpdateQueryAll(EntPacHistoriaclinica1 obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.h1motivophc == null ? null : "'" + obj.h1motivophc + "'");
				arrValores.Add(obj.h1enfermedadphc == null ? null : "'" + obj.h1enfermedadphc + "'");
				arrValores.Add(obj.h1cirujiasphc == null ? null : "'" + obj.h1cirujiasphc + "'");
				arrValores.Add(obj.h1infeccionesphc == null ? null : "'" + obj.h1infeccionesphc + "'");
				arrValores.Add(obj.h1traumatismosphc == null ? null : "'" + obj.h1traumatismosphc + "'");
				arrValores.Add(obj.h1cardiovascularphc == null ? null : "'" + obj.h1cardiovascularphc + "'");
				arrValores.Add(obj.h1metabolicophc == null ? null : "'" + obj.h1metabolicophc + "'");
				arrValores.Add(obj.h1toxicophc == null ? null : "'" + obj.h1toxicophc + "'");
				arrValores.Add(obj.h1familiarphc == null ? null : "'" + obj.h1familiarphc + "'");
				arrValores.Add(obj.h1ginecoobsphc == null ? null : "'" + obj.h1ginecoobsphc + "'");
				arrValores.Add(obj.h1otrosphc == null ? null : "'" + obj.h1otrosphc + "'");
				arrValores.Add(obj.h1embarazosphc);
				arrValores.Add(obj.h1gestaphc);
				arrValores.Add(obj.h1abortosphc);
				arrValores.Add(obj.h1mesntruacionesphc == null ? null : "'" + obj.h1mesntruacionesphc + "'");
				arrValores.Add(obj.h1fumphc == null ? null : "'" + obj.h1fumphc + "'");
				arrValores.Add(obj.h1fupphc == null ? null : "'" + obj.h1fupphc + "'");
				arrValores.Add(obj.h1revisionphc == null ? null : "'" + obj.h1revisionphc + "'");
				arrValores.Add(obj.h1examenfisicophc == null ? null : "'" + obj.h1examenfisicophc + "'");
				arrValores.Add(obj.h1taphc == null ? null : "'" + obj.h1taphc + "'");
				arrValores.Add(obj.h1fcphc == null ? null : "'" + obj.h1fcphc + "'");
				arrValores.Add(obj.h1frphc == null ? null : "'" + obj.h1frphc + "'");
				arrValores.Add(obj.h1tempphc == null ? null : "'" + obj.h1tempphc + "'");
				arrValores.Add(obj.h1tallaphc == null ? null : "'" + obj.h1tallaphc + "'");
				arrValores.Add(obj.h1pesophc == null ? null : "'" + obj.h1pesophc + "'");
				arrValores.Add(obj.h1pcphc == null ? null : "'" + obj.h1pcphc + "'");
				arrValores.Add(obj.h2cabezaphc == null ? null : "'" + obj.h2cabezaphc + "'");
				arrValores.Add(obj.h2cuellophc == null ? null : "'" + obj.h2cuellophc + "'");
				arrValores.Add(obj.h2toraxphc == null ? null : "'" + obj.h2toraxphc + "'");
				arrValores.Add(obj.h2abdomenphc == null ? null : "'" + obj.h2abdomenphc + "'");
				arrValores.Add(obj.h2genitourinariophc == null ? null : "'" + obj.h2genitourinariophc + "'");
				arrValores.Add(obj.h2extremidadesphc == null ? null : "'" + obj.h2extremidadesphc + "'");
				arrValores.Add(obj.h2conalertaphc == null ? null : "'" + obj.h2conalertaphc + "'");
				arrValores.Add(obj.h2conconfusionphc == null ? null : "'" + obj.h2conconfusionphc + "'");
				arrValores.Add(obj.h2consomnolenciaphc == null ? null : "'" + obj.h2consomnolenciaphc + "'");
				arrValores.Add(obj.h2conestuforphc == null ? null : "'" + obj.h2conestuforphc + "'");
				arrValores.Add(obj.h2concomaphc == null ? null : "'" + obj.h2concomaphc + "'");
				arrValores.Add(obj.h2oriespaciophc == null ? null : "'" + obj.h2oriespaciophc + "'");
				arrValores.Add(obj.h2orilugarphc == null ? null : "'" + obj.h2orilugarphc + "'");
				arrValores.Add(obj.h2oripersonaphc == null ? null : "'" + obj.h2oripersonaphc + "'");
				arrValores.Add(obj.h2razideaphc == null ? null : "'" + obj.h2razideaphc + "'");
				arrValores.Add(obj.h2razjuiciophc == null ? null : "'" + obj.h2razjuiciophc + "'");
				arrValores.Add(obj.h2razabstraccionphc == null ? null : "'" + obj.h2razabstraccionphc + "'");
				arrValores.Add(obj.h2conocimientosgenerales == null ? null : "'" + obj.h2conocimientosgenerales + "'");
				arrValores.Add(obj.h2atgdesatentophc == null ? null : "'" + obj.h2atgdesatentophc + "'");
				arrValores.Add(obj.h2atgfluctuantephc == null ? null : "'" + obj.h2atgfluctuantephc + "'");
				arrValores.Add(obj.h2atgnegativophc == null ? null : "'" + obj.h2atgnegativophc + "'");
				arrValores.Add(obj.h2atgnegligentephc == null ? null : "'" + obj.h2atgnegligentephc + "'");
				arrValores.Add(obj.h2pagconfusionphc == null ? null : "'" + obj.h2pagconfusionphc + "'");
				arrValores.Add(obj.h2pagdeliriophc == null ? null : "'" + obj.h2pagdeliriophc + "'");
				arrValores.Add(obj.h2paedelusionesphc == null ? null : "'" + obj.h2paedelusionesphc + "'");
				arrValores.Add(obj.h2paeilsuionesphc == null ? null : "'" + obj.h2paeilsuionesphc + "'");
				arrValores.Add(obj.h2paealucinacionesphc == null ? null : "'" + obj.h2paealucinacionesphc + "'");
				arrValores.Add(obj.h3conrecientephc == null ? null : "'" + obj.h3conrecientephc + "'");
				arrValores.Add(obj.h3conremotaphc == null ? null : "'" + obj.h3conremotaphc + "'");
				arrValores.Add(obj.h3afeeuforicophc == null ? null : "'" + obj.h3afeeuforicophc + "'");
				arrValores.Add(obj.h3afeplanophc == null ? null : "'" + obj.h3afeplanophc + "'");
				arrValores.Add(obj.h3afedeprimidophc == null ? null : "'" + obj.h3afedeprimidophc + "'");
				arrValores.Add(obj.h3afelabilphc == null ? null : "'" + obj.h3afelabilphc + "'");
				arrValores.Add(obj.h3afehostilphc == null ? null : "'" + obj.h3afehostilphc + "'");
				arrValores.Add(obj.h3afeinadecuadophc == null ? null : "'" + obj.h3afeinadecuadophc + "'");
				arrValores.Add(obj.h3hdoizquierdophc == null ? null : "'" + obj.h3hdoizquierdophc + "'");
				arrValores.Add(obj.h3hdoderechophc == null ? null : "'" + obj.h3hdoderechophc + "'");
				arrValores.Add(obj.h3cgeexpresionphc == null ? null : "'" + obj.h3cgeexpresionphc + "'");
				arrValores.Add(obj.h3cgenominacionphc == null ? null : "'" + obj.h3cgenominacionphc + "'");
				arrValores.Add(obj.h3cgerepeticionphc == null ? null : "'" + obj.h3cgerepeticionphc + "'");
				arrValores.Add(obj.h3cgecomprensionphc == null ? null : "'" + obj.h3cgecomprensionphc + "'");
				arrValores.Add(obj.h3apebrocaphc == null ? null : "'" + obj.h3apebrocaphc + "'");
				arrValores.Add(obj.h3apeconduccionphc == null ? null : "'" + obj.h3apeconduccionphc + "'");
				arrValores.Add(obj.h3apewernickephc == null ? null : "'" + obj.h3apewernickephc + "'");
				arrValores.Add(obj.h3apeglobalphc == null ? null : "'" + obj.h3apeglobalphc + "'");
				arrValores.Add(obj.h3apeanomiaphc == null ? null : "'" + obj.h3apeanomiaphc + "'");
				arrValores.Add(obj.h3atrmotoraphc == null ? null : "'" + obj.h3atrmotoraphc + "'");
				arrValores.Add(obj.h3atrsensitivaphc == null ? null : "'" + obj.h3atrsensitivaphc + "'");
				arrValores.Add(obj.h3atrmixtaphc == null ? null : "'" + obj.h3atrmixtaphc + "'");
				arrValores.Add(obj.h3ataexpresionphc == null ? null : "'" + obj.h3ataexpresionphc + "'");
				arrValores.Add(obj.h3atacomprensionphc == null ? null : "'" + obj.h3atacomprensionphc + "'");
				arrValores.Add(obj.h3atamixtaphc == null ? null : "'" + obj.h3atamixtaphc + "'");
				arrValores.Add(obj.h3aprmotoraphc == null ? null : "'" + obj.h3aprmotoraphc + "'");
				arrValores.Add(obj.h3aprsensorialphc == null ? null : "'" + obj.h3aprsensorialphc + "'");
				arrValores.Add(obj.h3lesalexiaphc == null ? null : "'" + obj.h3lesalexiaphc + "'");
				arrValores.Add(obj.h3lesagrafiaphc == null ? null : "'" + obj.h3lesagrafiaphc + "'");
				arrValores.Add(obj.h3lpdacalculiaphc == null ? null : "'" + obj.h3lpdacalculiaphc + "'");
				arrValores.Add(obj.h3lpdagrafiaphc == null ? null : "'" + obj.h3lpdagrafiaphc + "'");
				arrValores.Add(obj.h3lpddesorientacionphc == null ? null : "'" + obj.h3lpddesorientacionphc + "'");
				arrValores.Add(obj.h3lpdagnosiaphc == null ? null : "'" + obj.h3lpdagnosiaphc + "'");
				arrValores.Add(obj.h3agnauditivaphc == null ? null : "'" + obj.h3agnauditivaphc + "'");
				arrValores.Add(obj.h3agnvisualphc == null ? null : "'" + obj.h3agnvisualphc + "'");
				arrValores.Add(obj.h3agntactilphc == null ? null : "'" + obj.h3agntactilphc + "'");
				arrValores.Add(obj.h3aprideacionalphc == null ? null : "'" + obj.h3aprideacionalphc + "'");
				arrValores.Add(obj.h3aprideomotoraphc == null ? null : "'" + obj.h3aprideomotoraphc + "'");
				arrValores.Add(obj.h3aprdesatencionphc == null ? null : "'" + obj.h3aprdesatencionphc + "'");
				arrValores.Add(obj.h3anosognosiaphc == null ? null : "'" + obj.h3anosognosiaphc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pachistoriaclinica1 a partir de una clase del tipo epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQueryAll(EntPacHistoriaclinica1 obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.h1motivophc == null ? null : "'" + obj.h1motivophc + "'");
				arrValores.Add(obj.h1enfermedadphc == null ? null : "'" + obj.h1enfermedadphc + "'");
				arrValores.Add(obj.h1cirujiasphc == null ? null : "'" + obj.h1cirujiasphc + "'");
				arrValores.Add(obj.h1infeccionesphc == null ? null : "'" + obj.h1infeccionesphc + "'");
				arrValores.Add(obj.h1traumatismosphc == null ? null : "'" + obj.h1traumatismosphc + "'");
				arrValores.Add(obj.h1cardiovascularphc == null ? null : "'" + obj.h1cardiovascularphc + "'");
				arrValores.Add(obj.h1metabolicophc == null ? null : "'" + obj.h1metabolicophc + "'");
				arrValores.Add(obj.h1toxicophc == null ? null : "'" + obj.h1toxicophc + "'");
				arrValores.Add(obj.h1familiarphc == null ? null : "'" + obj.h1familiarphc + "'");
				arrValores.Add(obj.h1ginecoobsphc == null ? null : "'" + obj.h1ginecoobsphc + "'");
				arrValores.Add(obj.h1otrosphc == null ? null : "'" + obj.h1otrosphc + "'");
				arrValores.Add(obj.h1embarazosphc);
				arrValores.Add(obj.h1gestaphc);
				arrValores.Add(obj.h1abortosphc);
				arrValores.Add(obj.h1mesntruacionesphc == null ? null : "'" + obj.h1mesntruacionesphc + "'");
				arrValores.Add(obj.h1fumphc == null ? null : "'" + obj.h1fumphc + "'");
				arrValores.Add(obj.h1fupphc == null ? null : "'" + obj.h1fupphc + "'");
				arrValores.Add(obj.h1revisionphc == null ? null : "'" + obj.h1revisionphc + "'");
				arrValores.Add(obj.h1examenfisicophc == null ? null : "'" + obj.h1examenfisicophc + "'");
				arrValores.Add(obj.h1taphc == null ? null : "'" + obj.h1taphc + "'");
				arrValores.Add(obj.h1fcphc == null ? null : "'" + obj.h1fcphc + "'");
				arrValores.Add(obj.h1frphc == null ? null : "'" + obj.h1frphc + "'");
				arrValores.Add(obj.h1tempphc == null ? null : "'" + obj.h1tempphc + "'");
				arrValores.Add(obj.h1tallaphc == null ? null : "'" + obj.h1tallaphc + "'");
				arrValores.Add(obj.h1pesophc == null ? null : "'" + obj.h1pesophc + "'");
				arrValores.Add(obj.h1pcphc == null ? null : "'" + obj.h1pcphc + "'");
				arrValores.Add(obj.h2cabezaphc == null ? null : "'" + obj.h2cabezaphc + "'");
				arrValores.Add(obj.h2cuellophc == null ? null : "'" + obj.h2cuellophc + "'");
				arrValores.Add(obj.h2toraxphc == null ? null : "'" + obj.h2toraxphc + "'");
				arrValores.Add(obj.h2abdomenphc == null ? null : "'" + obj.h2abdomenphc + "'");
				arrValores.Add(obj.h2genitourinariophc == null ? null : "'" + obj.h2genitourinariophc + "'");
				arrValores.Add(obj.h2extremidadesphc == null ? null : "'" + obj.h2extremidadesphc + "'");
				arrValores.Add(obj.h2conalertaphc == null ? null : "'" + obj.h2conalertaphc + "'");
				arrValores.Add(obj.h2conconfusionphc == null ? null : "'" + obj.h2conconfusionphc + "'");
				arrValores.Add(obj.h2consomnolenciaphc == null ? null : "'" + obj.h2consomnolenciaphc + "'");
				arrValores.Add(obj.h2conestuforphc == null ? null : "'" + obj.h2conestuforphc + "'");
				arrValores.Add(obj.h2concomaphc == null ? null : "'" + obj.h2concomaphc + "'");
				arrValores.Add(obj.h2oriespaciophc == null ? null : "'" + obj.h2oriespaciophc + "'");
				arrValores.Add(obj.h2orilugarphc == null ? null : "'" + obj.h2orilugarphc + "'");
				arrValores.Add(obj.h2oripersonaphc == null ? null : "'" + obj.h2oripersonaphc + "'");
				arrValores.Add(obj.h2razideaphc == null ? null : "'" + obj.h2razideaphc + "'");
				arrValores.Add(obj.h2razjuiciophc == null ? null : "'" + obj.h2razjuiciophc + "'");
				arrValores.Add(obj.h2razabstraccionphc == null ? null : "'" + obj.h2razabstraccionphc + "'");
				arrValores.Add(obj.h2conocimientosgenerales == null ? null : "'" + obj.h2conocimientosgenerales + "'");
				arrValores.Add(obj.h2atgdesatentophc == null ? null : "'" + obj.h2atgdesatentophc + "'");
				arrValores.Add(obj.h2atgfluctuantephc == null ? null : "'" + obj.h2atgfluctuantephc + "'");
				arrValores.Add(obj.h2atgnegativophc == null ? null : "'" + obj.h2atgnegativophc + "'");
				arrValores.Add(obj.h2atgnegligentephc == null ? null : "'" + obj.h2atgnegligentephc + "'");
				arrValores.Add(obj.h2pagconfusionphc == null ? null : "'" + obj.h2pagconfusionphc + "'");
				arrValores.Add(obj.h2pagdeliriophc == null ? null : "'" + obj.h2pagdeliriophc + "'");
				arrValores.Add(obj.h2paedelusionesphc == null ? null : "'" + obj.h2paedelusionesphc + "'");
				arrValores.Add(obj.h2paeilsuionesphc == null ? null : "'" + obj.h2paeilsuionesphc + "'");
				arrValores.Add(obj.h2paealucinacionesphc == null ? null : "'" + obj.h2paealucinacionesphc + "'");
				arrValores.Add(obj.h3conrecientephc == null ? null : "'" + obj.h3conrecientephc + "'");
				arrValores.Add(obj.h3conremotaphc == null ? null : "'" + obj.h3conremotaphc + "'");
				arrValores.Add(obj.h3afeeuforicophc == null ? null : "'" + obj.h3afeeuforicophc + "'");
				arrValores.Add(obj.h3afeplanophc == null ? null : "'" + obj.h3afeplanophc + "'");
				arrValores.Add(obj.h3afedeprimidophc == null ? null : "'" + obj.h3afedeprimidophc + "'");
				arrValores.Add(obj.h3afelabilphc == null ? null : "'" + obj.h3afelabilphc + "'");
				arrValores.Add(obj.h3afehostilphc == null ? null : "'" + obj.h3afehostilphc + "'");
				arrValores.Add(obj.h3afeinadecuadophc == null ? null : "'" + obj.h3afeinadecuadophc + "'");
				arrValores.Add(obj.h3hdoizquierdophc == null ? null : "'" + obj.h3hdoizquierdophc + "'");
				arrValores.Add(obj.h3hdoderechophc == null ? null : "'" + obj.h3hdoderechophc + "'");
				arrValores.Add(obj.h3cgeexpresionphc == null ? null : "'" + obj.h3cgeexpresionphc + "'");
				arrValores.Add(obj.h3cgenominacionphc == null ? null : "'" + obj.h3cgenominacionphc + "'");
				arrValores.Add(obj.h3cgerepeticionphc == null ? null : "'" + obj.h3cgerepeticionphc + "'");
				arrValores.Add(obj.h3cgecomprensionphc == null ? null : "'" + obj.h3cgecomprensionphc + "'");
				arrValores.Add(obj.h3apebrocaphc == null ? null : "'" + obj.h3apebrocaphc + "'");
				arrValores.Add(obj.h3apeconduccionphc == null ? null : "'" + obj.h3apeconduccionphc + "'");
				arrValores.Add(obj.h3apewernickephc == null ? null : "'" + obj.h3apewernickephc + "'");
				arrValores.Add(obj.h3apeglobalphc == null ? null : "'" + obj.h3apeglobalphc + "'");
				arrValores.Add(obj.h3apeanomiaphc == null ? null : "'" + obj.h3apeanomiaphc + "'");
				arrValores.Add(obj.h3atrmotoraphc == null ? null : "'" + obj.h3atrmotoraphc + "'");
				arrValores.Add(obj.h3atrsensitivaphc == null ? null : "'" + obj.h3atrsensitivaphc + "'");
				arrValores.Add(obj.h3atrmixtaphc == null ? null : "'" + obj.h3atrmixtaphc + "'");
				arrValores.Add(obj.h3ataexpresionphc == null ? null : "'" + obj.h3ataexpresionphc + "'");
				arrValores.Add(obj.h3atacomprensionphc == null ? null : "'" + obj.h3atacomprensionphc + "'");
				arrValores.Add(obj.h3atamixtaphc == null ? null : "'" + obj.h3atamixtaphc + "'");
				arrValores.Add(obj.h3aprmotoraphc == null ? null : "'" + obj.h3aprmotoraphc + "'");
				arrValores.Add(obj.h3aprsensorialphc == null ? null : "'" + obj.h3aprsensorialphc + "'");
				arrValores.Add(obj.h3lesalexiaphc == null ? null : "'" + obj.h3lesalexiaphc + "'");
				arrValores.Add(obj.h3lesagrafiaphc == null ? null : "'" + obj.h3lesagrafiaphc + "'");
				arrValores.Add(obj.h3lpdacalculiaphc == null ? null : "'" + obj.h3lpdacalculiaphc + "'");
				arrValores.Add(obj.h3lpdagrafiaphc == null ? null : "'" + obj.h3lpdagrafiaphc + "'");
				arrValores.Add(obj.h3lpddesorientacionphc == null ? null : "'" + obj.h3lpddesorientacionphc + "'");
				arrValores.Add(obj.h3lpdagnosiaphc == null ? null : "'" + obj.h3lpdagnosiaphc + "'");
				arrValores.Add(obj.h3agnauditivaphc == null ? null : "'" + obj.h3agnauditivaphc + "'");
				arrValores.Add(obj.h3agnvisualphc == null ? null : "'" + obj.h3agnvisualphc + "'");
				arrValores.Add(obj.h3agntactilphc == null ? null : "'" + obj.h3agntactilphc + "'");
				arrValores.Add(obj.h3aprideacionalphc == null ? null : "'" + obj.h3aprideacionalphc + "'");
				arrValores.Add(obj.h3aprideomotoraphc == null ? null : "'" + obj.h3aprideomotoraphc + "'");
				arrValores.Add(obj.h3aprdesatencionphc == null ? null : "'" + obj.h3aprdesatencionphc + "'");
				arrValores.Add(obj.h3anosognosiaphc == null ? null : "'" + obj.h3anosognosiaphc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pachistoriaclinica1 a partir de una clase del tipo Epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpachistoriaclinica1
		/// </returns>
		public int UpdateQuery(EntPacHistoriaclinica1 obj)
		{
			try
			{
				//Obtenemos el Objeto original
				EntPacHistoriaclinica1 objOriginal = this.ObtenerObjeto(obj.idppa);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.h1motivophc != objOriginal.h1motivophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
					arrValores.Add(obj.h1motivophc == null ? null : "'" + obj.h1motivophc + "'");
				}
				if(obj.h1enfermedadphc != objOriginal.h1enfermedadphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
					arrValores.Add(obj.h1enfermedadphc == null ? null : "'" + obj.h1enfermedadphc + "'");
				}
				if(obj.h1cirujiasphc != objOriginal.h1cirujiasphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
					arrValores.Add(obj.h1cirujiasphc == null ? null : "'" + obj.h1cirujiasphc + "'");
				}
				if(obj.h1infeccionesphc != objOriginal.h1infeccionesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
					arrValores.Add(obj.h1infeccionesphc == null ? null : "'" + obj.h1infeccionesphc + "'");
				}
				if(obj.h1traumatismosphc != objOriginal.h1traumatismosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
					arrValores.Add(obj.h1traumatismosphc == null ? null : "'" + obj.h1traumatismosphc + "'");
				}
				if(obj.h1cardiovascularphc != objOriginal.h1cardiovascularphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
					arrValores.Add(obj.h1cardiovascularphc == null ? null : "'" + obj.h1cardiovascularphc + "'");
				}
				if(obj.h1metabolicophc != objOriginal.h1metabolicophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
					arrValores.Add(obj.h1metabolicophc == null ? null : "'" + obj.h1metabolicophc + "'");
				}
				if(obj.h1toxicophc != objOriginal.h1toxicophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
					arrValores.Add(obj.h1toxicophc == null ? null : "'" + obj.h1toxicophc + "'");
				}
				if(obj.h1familiarphc != objOriginal.h1familiarphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
					arrValores.Add(obj.h1familiarphc == null ? null : "'" + obj.h1familiarphc + "'");
				}
				if(obj.h1ginecoobsphc != objOriginal.h1ginecoobsphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
					arrValores.Add(obj.h1ginecoobsphc == null ? null : "'" + obj.h1ginecoobsphc + "'");
				}
				if(obj.h1otrosphc != objOriginal.h1otrosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
					arrValores.Add(obj.h1otrosphc == null ? null : "'" + obj.h1otrosphc + "'");
				}
				if(obj.h1embarazosphc != objOriginal.h1embarazosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
					arrValores.Add(obj.h1embarazosphc);
				}
				if(obj.h1gestaphc != objOriginal.h1gestaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
					arrValores.Add(obj.h1gestaphc);
				}
				if(obj.h1abortosphc != objOriginal.h1abortosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
					arrValores.Add(obj.h1abortosphc);
				}
				if(obj.h1mesntruacionesphc != objOriginal.h1mesntruacionesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
					arrValores.Add(obj.h1mesntruacionesphc == null ? null : "'" + obj.h1mesntruacionesphc + "'");
				}
				if(obj.h1fumphc != objOriginal.h1fumphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
					arrValores.Add(obj.h1fumphc == null ? null : "'" + obj.h1fumphc + "'");
				}
				if(obj.h1fupphc != objOriginal.h1fupphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
					arrValores.Add(obj.h1fupphc == null ? null : "'" + obj.h1fupphc + "'");
				}
				if(obj.h1revisionphc != objOriginal.h1revisionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
					arrValores.Add(obj.h1revisionphc == null ? null : "'" + obj.h1revisionphc + "'");
				}
				if(obj.h1examenfisicophc != objOriginal.h1examenfisicophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
					arrValores.Add(obj.h1examenfisicophc == null ? null : "'" + obj.h1examenfisicophc + "'");
				}
				if(obj.h1taphc != objOriginal.h1taphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
					arrValores.Add(obj.h1taphc == null ? null : "'" + obj.h1taphc + "'");
				}
				if(obj.h1fcphc != objOriginal.h1fcphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
					arrValores.Add(obj.h1fcphc == null ? null : "'" + obj.h1fcphc + "'");
				}
				if(obj.h1frphc != objOriginal.h1frphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
					arrValores.Add(obj.h1frphc == null ? null : "'" + obj.h1frphc + "'");
				}
				if(obj.h1tempphc != objOriginal.h1tempphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
					arrValores.Add(obj.h1tempphc == null ? null : "'" + obj.h1tempphc + "'");
				}
				if(obj.h1tallaphc != objOriginal.h1tallaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
					arrValores.Add(obj.h1tallaphc == null ? null : "'" + obj.h1tallaphc + "'");
				}
				if(obj.h1pesophc != objOriginal.h1pesophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
					arrValores.Add(obj.h1pesophc == null ? null : "'" + obj.h1pesophc + "'");
				}
				if(obj.h1pcphc != objOriginal.h1pcphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
					arrValores.Add(obj.h1pcphc == null ? null : "'" + obj.h1pcphc + "'");
				}
				if(obj.h2cabezaphc != objOriginal.h2cabezaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
					arrValores.Add(obj.h2cabezaphc == null ? null : "'" + obj.h2cabezaphc + "'");
				}
				if(obj.h2cuellophc != objOriginal.h2cuellophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
					arrValores.Add(obj.h2cuellophc == null ? null : "'" + obj.h2cuellophc + "'");
				}
				if(obj.h2toraxphc != objOriginal.h2toraxphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
					arrValores.Add(obj.h2toraxphc == null ? null : "'" + obj.h2toraxphc + "'");
				}
				if(obj.h2abdomenphc != objOriginal.h2abdomenphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
					arrValores.Add(obj.h2abdomenphc == null ? null : "'" + obj.h2abdomenphc + "'");
				}
				if(obj.h2genitourinariophc != objOriginal.h2genitourinariophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
					arrValores.Add(obj.h2genitourinariophc == null ? null : "'" + obj.h2genitourinariophc + "'");
				}
				if(obj.h2extremidadesphc != objOriginal.h2extremidadesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
					arrValores.Add(obj.h2extremidadesphc == null ? null : "'" + obj.h2extremidadesphc + "'");
				}
				if(obj.h2conalertaphc != objOriginal.h2conalertaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
					arrValores.Add(obj.h2conalertaphc == null ? null : "'" + obj.h2conalertaphc + "'");
				}
				if(obj.h2conconfusionphc != objOriginal.h2conconfusionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
					arrValores.Add(obj.h2conconfusionphc == null ? null : "'" + obj.h2conconfusionphc + "'");
				}
				if(obj.h2consomnolenciaphc != objOriginal.h2consomnolenciaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
					arrValores.Add(obj.h2consomnolenciaphc == null ? null : "'" + obj.h2consomnolenciaphc + "'");
				}
				if(obj.h2conestuforphc != objOriginal.h2conestuforphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
					arrValores.Add(obj.h2conestuforphc == null ? null : "'" + obj.h2conestuforphc + "'");
				}
				if(obj.h2concomaphc != objOriginal.h2concomaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
					arrValores.Add(obj.h2concomaphc == null ? null : "'" + obj.h2concomaphc + "'");
				}
				if(obj.h2oriespaciophc != objOriginal.h2oriespaciophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
					arrValores.Add(obj.h2oriespaciophc == null ? null : "'" + obj.h2oriespaciophc + "'");
				}
				if(obj.h2orilugarphc != objOriginal.h2orilugarphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
					arrValores.Add(obj.h2orilugarphc == null ? null : "'" + obj.h2orilugarphc + "'");
				}
				if(obj.h2oripersonaphc != objOriginal.h2oripersonaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
					arrValores.Add(obj.h2oripersonaphc == null ? null : "'" + obj.h2oripersonaphc + "'");
				}
				if(obj.h2razideaphc != objOriginal.h2razideaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
					arrValores.Add(obj.h2razideaphc == null ? null : "'" + obj.h2razideaphc + "'");
				}
				if(obj.h2razjuiciophc != objOriginal.h2razjuiciophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
					arrValores.Add(obj.h2razjuiciophc == null ? null : "'" + obj.h2razjuiciophc + "'");
				}
				if(obj.h2razabstraccionphc != objOriginal.h2razabstraccionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
					arrValores.Add(obj.h2razabstraccionphc == null ? null : "'" + obj.h2razabstraccionphc + "'");
				}
				if(obj.h2conocimientosgenerales != objOriginal.h2conocimientosgenerales )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
					arrValores.Add(obj.h2conocimientosgenerales == null ? null : "'" + obj.h2conocimientosgenerales + "'");
				}
				if(obj.h2atgdesatentophc != objOriginal.h2atgdesatentophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
					arrValores.Add(obj.h2atgdesatentophc == null ? null : "'" + obj.h2atgdesatentophc + "'");
				}
				if(obj.h2atgfluctuantephc != objOriginal.h2atgfluctuantephc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
					arrValores.Add(obj.h2atgfluctuantephc == null ? null : "'" + obj.h2atgfluctuantephc + "'");
				}
				if(obj.h2atgnegativophc != objOriginal.h2atgnegativophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
					arrValores.Add(obj.h2atgnegativophc == null ? null : "'" + obj.h2atgnegativophc + "'");
				}
				if(obj.h2atgnegligentephc != objOriginal.h2atgnegligentephc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
					arrValores.Add(obj.h2atgnegligentephc == null ? null : "'" + obj.h2atgnegligentephc + "'");
				}
				if(obj.h2pagconfusionphc != objOriginal.h2pagconfusionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
					arrValores.Add(obj.h2pagconfusionphc == null ? null : "'" + obj.h2pagconfusionphc + "'");
				}
				if(obj.h2pagdeliriophc != objOriginal.h2pagdeliriophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
					arrValores.Add(obj.h2pagdeliriophc == null ? null : "'" + obj.h2pagdeliriophc + "'");
				}
				if(obj.h2paedelusionesphc != objOriginal.h2paedelusionesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
					arrValores.Add(obj.h2paedelusionesphc == null ? null : "'" + obj.h2paedelusionesphc + "'");
				}
				if(obj.h2paeilsuionesphc != objOriginal.h2paeilsuionesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
					arrValores.Add(obj.h2paeilsuionesphc == null ? null : "'" + obj.h2paeilsuionesphc + "'");
				}
				if(obj.h2paealucinacionesphc != objOriginal.h2paealucinacionesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
					arrValores.Add(obj.h2paealucinacionesphc == null ? null : "'" + obj.h2paealucinacionesphc + "'");
				}
				if(obj.h3conrecientephc != objOriginal.h3conrecientephc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
					arrValores.Add(obj.h3conrecientephc == null ? null : "'" + obj.h3conrecientephc + "'");
				}
				if(obj.h3conremotaphc != objOriginal.h3conremotaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
					arrValores.Add(obj.h3conremotaphc == null ? null : "'" + obj.h3conremotaphc + "'");
				}
				if(obj.h3afeeuforicophc != objOriginal.h3afeeuforicophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
					arrValores.Add(obj.h3afeeuforicophc == null ? null : "'" + obj.h3afeeuforicophc + "'");
				}
				if(obj.h3afeplanophc != objOriginal.h3afeplanophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
					arrValores.Add(obj.h3afeplanophc == null ? null : "'" + obj.h3afeplanophc + "'");
				}
				if(obj.h3afedeprimidophc != objOriginal.h3afedeprimidophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
					arrValores.Add(obj.h3afedeprimidophc == null ? null : "'" + obj.h3afedeprimidophc + "'");
				}
				if(obj.h3afelabilphc != objOriginal.h3afelabilphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
					arrValores.Add(obj.h3afelabilphc == null ? null : "'" + obj.h3afelabilphc + "'");
				}
				if(obj.h3afehostilphc != objOriginal.h3afehostilphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
					arrValores.Add(obj.h3afehostilphc == null ? null : "'" + obj.h3afehostilphc + "'");
				}
				if(obj.h3afeinadecuadophc != objOriginal.h3afeinadecuadophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
					arrValores.Add(obj.h3afeinadecuadophc == null ? null : "'" + obj.h3afeinadecuadophc + "'");
				}
				if(obj.h3hdoizquierdophc != objOriginal.h3hdoizquierdophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
					arrValores.Add(obj.h3hdoizquierdophc == null ? null : "'" + obj.h3hdoizquierdophc + "'");
				}
				if(obj.h3hdoderechophc != objOriginal.h3hdoderechophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
					arrValores.Add(obj.h3hdoderechophc == null ? null : "'" + obj.h3hdoderechophc + "'");
				}
				if(obj.h3cgeexpresionphc != objOriginal.h3cgeexpresionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
					arrValores.Add(obj.h3cgeexpresionphc == null ? null : "'" + obj.h3cgeexpresionphc + "'");
				}
				if(obj.h3cgenominacionphc != objOriginal.h3cgenominacionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
					arrValores.Add(obj.h3cgenominacionphc == null ? null : "'" + obj.h3cgenominacionphc + "'");
				}
				if(obj.h3cgerepeticionphc != objOriginal.h3cgerepeticionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
					arrValores.Add(obj.h3cgerepeticionphc == null ? null : "'" + obj.h3cgerepeticionphc + "'");
				}
				if(obj.h3cgecomprensionphc != objOriginal.h3cgecomprensionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
					arrValores.Add(obj.h3cgecomprensionphc == null ? null : "'" + obj.h3cgecomprensionphc + "'");
				}
				if(obj.h3apebrocaphc != objOriginal.h3apebrocaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
					arrValores.Add(obj.h3apebrocaphc == null ? null : "'" + obj.h3apebrocaphc + "'");
				}
				if(obj.h3apeconduccionphc != objOriginal.h3apeconduccionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
					arrValores.Add(obj.h3apeconduccionphc == null ? null : "'" + obj.h3apeconduccionphc + "'");
				}
				if(obj.h3apewernickephc != objOriginal.h3apewernickephc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
					arrValores.Add(obj.h3apewernickephc == null ? null : "'" + obj.h3apewernickephc + "'");
				}
				if(obj.h3apeglobalphc != objOriginal.h3apeglobalphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
					arrValores.Add(obj.h3apeglobalphc == null ? null : "'" + obj.h3apeglobalphc + "'");
				}
				if(obj.h3apeanomiaphc != objOriginal.h3apeanomiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
					arrValores.Add(obj.h3apeanomiaphc == null ? null : "'" + obj.h3apeanomiaphc + "'");
				}
				if(obj.h3atrmotoraphc != objOriginal.h3atrmotoraphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
					arrValores.Add(obj.h3atrmotoraphc == null ? null : "'" + obj.h3atrmotoraphc + "'");
				}
				if(obj.h3atrsensitivaphc != objOriginal.h3atrsensitivaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
					arrValores.Add(obj.h3atrsensitivaphc == null ? null : "'" + obj.h3atrsensitivaphc + "'");
				}
				if(obj.h3atrmixtaphc != objOriginal.h3atrmixtaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
					arrValores.Add(obj.h3atrmixtaphc == null ? null : "'" + obj.h3atrmixtaphc + "'");
				}
				if(obj.h3ataexpresionphc != objOriginal.h3ataexpresionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
					arrValores.Add(obj.h3ataexpresionphc == null ? null : "'" + obj.h3ataexpresionphc + "'");
				}
				if(obj.h3atacomprensionphc != objOriginal.h3atacomprensionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
					arrValores.Add(obj.h3atacomprensionphc == null ? null : "'" + obj.h3atacomprensionphc + "'");
				}
				if(obj.h3atamixtaphc != objOriginal.h3atamixtaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
					arrValores.Add(obj.h3atamixtaphc == null ? null : "'" + obj.h3atamixtaphc + "'");
				}
				if(obj.h3aprmotoraphc != objOriginal.h3aprmotoraphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
					arrValores.Add(obj.h3aprmotoraphc == null ? null : "'" + obj.h3aprmotoraphc + "'");
				}
				if(obj.h3aprsensorialphc != objOriginal.h3aprsensorialphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
					arrValores.Add(obj.h3aprsensorialphc == null ? null : "'" + obj.h3aprsensorialphc + "'");
				}
				if(obj.h3lesalexiaphc != objOriginal.h3lesalexiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
					arrValores.Add(obj.h3lesalexiaphc == null ? null : "'" + obj.h3lesalexiaphc + "'");
				}
				if(obj.h3lesagrafiaphc != objOriginal.h3lesagrafiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
					arrValores.Add(obj.h3lesagrafiaphc == null ? null : "'" + obj.h3lesagrafiaphc + "'");
				}
				if(obj.h3lpdacalculiaphc != objOriginal.h3lpdacalculiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
					arrValores.Add(obj.h3lpdacalculiaphc == null ? null : "'" + obj.h3lpdacalculiaphc + "'");
				}
				if(obj.h3lpdagrafiaphc != objOriginal.h3lpdagrafiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
					arrValores.Add(obj.h3lpdagrafiaphc == null ? null : "'" + obj.h3lpdagrafiaphc + "'");
				}
				if(obj.h3lpddesorientacionphc != objOriginal.h3lpddesorientacionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
					arrValores.Add(obj.h3lpddesorientacionphc == null ? null : "'" + obj.h3lpddesorientacionphc + "'");
				}
				if(obj.h3lpdagnosiaphc != objOriginal.h3lpdagnosiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
					arrValores.Add(obj.h3lpdagnosiaphc == null ? null : "'" + obj.h3lpdagnosiaphc + "'");
				}
				if(obj.h3agnauditivaphc != objOriginal.h3agnauditivaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
					arrValores.Add(obj.h3agnauditivaphc == null ? null : "'" + obj.h3agnauditivaphc + "'");
				}
				if(obj.h3agnvisualphc != objOriginal.h3agnvisualphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
					arrValores.Add(obj.h3agnvisualphc == null ? null : "'" + obj.h3agnvisualphc + "'");
				}
				if(obj.h3agntactilphc != objOriginal.h3agntactilphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
					arrValores.Add(obj.h3agntactilphc == null ? null : "'" + obj.h3agntactilphc + "'");
				}
				if(obj.h3aprideacionalphc != objOriginal.h3aprideacionalphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
					arrValores.Add(obj.h3aprideacionalphc == null ? null : "'" + obj.h3aprideacionalphc + "'");
				}
				if(obj.h3aprideomotoraphc != objOriginal.h3aprideomotoraphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
					arrValores.Add(obj.h3aprideomotoraphc == null ? null : "'" + obj.h3aprideomotoraphc + "'");
				}
				if(obj.h3aprdesatencionphc != objOriginal.h3aprdesatencionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
					arrValores.Add(obj.h3aprdesatencionphc == null ? null : "'" + obj.h3aprdesatencionphc + "'");
				}
				if(obj.h3anosognosiaphc != objOriginal.h3anosognosiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
					arrValores.Add(obj.h3anosognosiaphc == null ? null : "'" + obj.h3anosognosiaphc + "'");
				}
				if(obj.apiestado != objOriginal.apiestado )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
					arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				}
				if(obj.apitransaccion != objOriginal.apitransaccion )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
					arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				}
				if(obj.usumod != objOriginal.usumod )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
					arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pachistoriaclinica1 a partir de una clase del tipo epachistoriaclinica1
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQuery(EntPacHistoriaclinica1 obj, ref CTrans localTrans)
		{
			try
			{
				//Obtenemos el Objeto original
				EntPacHistoriaclinica1 objOriginal = this.ObtenerObjeto(obj.idppa, ref localTrans);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.h1motivophc != objOriginal.h1motivophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
					arrValores.Add(obj.h1motivophc == null ? null : "'" + obj.h1motivophc + "'");
				}
				if(obj.h1enfermedadphc != objOriginal.h1enfermedadphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
					arrValores.Add(obj.h1enfermedadphc == null ? null : "'" + obj.h1enfermedadphc + "'");
				}
				if(obj.h1cirujiasphc != objOriginal.h1cirujiasphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
					arrValores.Add(obj.h1cirujiasphc == null ? null : "'" + obj.h1cirujiasphc + "'");
				}
				if(obj.h1infeccionesphc != objOriginal.h1infeccionesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
					arrValores.Add(obj.h1infeccionesphc == null ? null : "'" + obj.h1infeccionesphc + "'");
				}
				if(obj.h1traumatismosphc != objOriginal.h1traumatismosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
					arrValores.Add(obj.h1traumatismosphc == null ? null : "'" + obj.h1traumatismosphc + "'");
				}
				if(obj.h1cardiovascularphc != objOriginal.h1cardiovascularphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
					arrValores.Add(obj.h1cardiovascularphc == null ? null : "'" + obj.h1cardiovascularphc + "'");
				}
				if(obj.h1metabolicophc != objOriginal.h1metabolicophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
					arrValores.Add(obj.h1metabolicophc == null ? null : "'" + obj.h1metabolicophc + "'");
				}
				if(obj.h1toxicophc != objOriginal.h1toxicophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
					arrValores.Add(obj.h1toxicophc == null ? null : "'" + obj.h1toxicophc + "'");
				}
				if(obj.h1familiarphc != objOriginal.h1familiarphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
					arrValores.Add(obj.h1familiarphc == null ? null : "'" + obj.h1familiarphc + "'");
				}
				if(obj.h1ginecoobsphc != objOriginal.h1ginecoobsphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
					arrValores.Add(obj.h1ginecoobsphc == null ? null : "'" + obj.h1ginecoobsphc + "'");
				}
				if(obj.h1otrosphc != objOriginal.h1otrosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
					arrValores.Add(obj.h1otrosphc == null ? null : "'" + obj.h1otrosphc + "'");
				}
				if(obj.h1embarazosphc != objOriginal.h1embarazosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
					arrValores.Add(obj.h1embarazosphc);
				}
				if(obj.h1gestaphc != objOriginal.h1gestaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
					arrValores.Add(obj.h1gestaphc);
				}
				if(obj.h1abortosphc != objOriginal.h1abortosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
					arrValores.Add(obj.h1abortosphc);
				}
				if(obj.h1mesntruacionesphc != objOriginal.h1mesntruacionesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
					arrValores.Add(obj.h1mesntruacionesphc == null ? null : "'" + obj.h1mesntruacionesphc + "'");
				}
				if(obj.h1fumphc != objOriginal.h1fumphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
					arrValores.Add(obj.h1fumphc == null ? null : "'" + obj.h1fumphc + "'");
				}
				if(obj.h1fupphc != objOriginal.h1fupphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
					arrValores.Add(obj.h1fupphc == null ? null : "'" + obj.h1fupphc + "'");
				}
				if(obj.h1revisionphc != objOriginal.h1revisionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
					arrValores.Add(obj.h1revisionphc == null ? null : "'" + obj.h1revisionphc + "'");
				}
				if(obj.h1examenfisicophc != objOriginal.h1examenfisicophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
					arrValores.Add(obj.h1examenfisicophc == null ? null : "'" + obj.h1examenfisicophc + "'");
				}
				if(obj.h1taphc != objOriginal.h1taphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
					arrValores.Add(obj.h1taphc == null ? null : "'" + obj.h1taphc + "'");
				}
				if(obj.h1fcphc != objOriginal.h1fcphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
					arrValores.Add(obj.h1fcphc == null ? null : "'" + obj.h1fcphc + "'");
				}
				if(obj.h1frphc != objOriginal.h1frphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
					arrValores.Add(obj.h1frphc == null ? null : "'" + obj.h1frphc + "'");
				}
				if(obj.h1tempphc != objOriginal.h1tempphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
					arrValores.Add(obj.h1tempphc == null ? null : "'" + obj.h1tempphc + "'");
				}
				if(obj.h1tallaphc != objOriginal.h1tallaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
					arrValores.Add(obj.h1tallaphc == null ? null : "'" + obj.h1tallaphc + "'");
				}
				if(obj.h1pesophc != objOriginal.h1pesophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
					arrValores.Add(obj.h1pesophc == null ? null : "'" + obj.h1pesophc + "'");
				}
				if(obj.h1pcphc != objOriginal.h1pcphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
					arrValores.Add(obj.h1pcphc == null ? null : "'" + obj.h1pcphc + "'");
				}
				if(obj.h2cabezaphc != objOriginal.h2cabezaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
					arrValores.Add(obj.h2cabezaphc == null ? null : "'" + obj.h2cabezaphc + "'");
				}
				if(obj.h2cuellophc != objOriginal.h2cuellophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
					arrValores.Add(obj.h2cuellophc == null ? null : "'" + obj.h2cuellophc + "'");
				}
				if(obj.h2toraxphc != objOriginal.h2toraxphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
					arrValores.Add(obj.h2toraxphc == null ? null : "'" + obj.h2toraxphc + "'");
				}
				if(obj.h2abdomenphc != objOriginal.h2abdomenphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
					arrValores.Add(obj.h2abdomenphc == null ? null : "'" + obj.h2abdomenphc + "'");
				}
				if(obj.h2genitourinariophc != objOriginal.h2genitourinariophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
					arrValores.Add(obj.h2genitourinariophc == null ? null : "'" + obj.h2genitourinariophc + "'");
				}
				if(obj.h2extremidadesphc != objOriginal.h2extremidadesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
					arrValores.Add(obj.h2extremidadesphc == null ? null : "'" + obj.h2extremidadesphc + "'");
				}
				if(obj.h2conalertaphc != objOriginal.h2conalertaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
					arrValores.Add(obj.h2conalertaphc == null ? null : "'" + obj.h2conalertaphc + "'");
				}
				if(obj.h2conconfusionphc != objOriginal.h2conconfusionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
					arrValores.Add(obj.h2conconfusionphc == null ? null : "'" + obj.h2conconfusionphc + "'");
				}
				if(obj.h2consomnolenciaphc != objOriginal.h2consomnolenciaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
					arrValores.Add(obj.h2consomnolenciaphc == null ? null : "'" + obj.h2consomnolenciaphc + "'");
				}
				if(obj.h2conestuforphc != objOriginal.h2conestuforphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
					arrValores.Add(obj.h2conestuforphc == null ? null : "'" + obj.h2conestuforphc + "'");
				}
				if(obj.h2concomaphc != objOriginal.h2concomaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
					arrValores.Add(obj.h2concomaphc == null ? null : "'" + obj.h2concomaphc + "'");
				}
				if(obj.h2oriespaciophc != objOriginal.h2oriespaciophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
					arrValores.Add(obj.h2oriespaciophc == null ? null : "'" + obj.h2oriespaciophc + "'");
				}
				if(obj.h2orilugarphc != objOriginal.h2orilugarphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
					arrValores.Add(obj.h2orilugarphc == null ? null : "'" + obj.h2orilugarphc + "'");
				}
				if(obj.h2oripersonaphc != objOriginal.h2oripersonaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
					arrValores.Add(obj.h2oripersonaphc == null ? null : "'" + obj.h2oripersonaphc + "'");
				}
				if(obj.h2razideaphc != objOriginal.h2razideaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
					arrValores.Add(obj.h2razideaphc == null ? null : "'" + obj.h2razideaphc + "'");
				}
				if(obj.h2razjuiciophc != objOriginal.h2razjuiciophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
					arrValores.Add(obj.h2razjuiciophc == null ? null : "'" + obj.h2razjuiciophc + "'");
				}
				if(obj.h2razabstraccionphc != objOriginal.h2razabstraccionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
					arrValores.Add(obj.h2razabstraccionphc == null ? null : "'" + obj.h2razabstraccionphc + "'");
				}
				if(obj.h2conocimientosgenerales != objOriginal.h2conocimientosgenerales )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
					arrValores.Add(obj.h2conocimientosgenerales == null ? null : "'" + obj.h2conocimientosgenerales + "'");
				}
				if(obj.h2atgdesatentophc != objOriginal.h2atgdesatentophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
					arrValores.Add(obj.h2atgdesatentophc == null ? null : "'" + obj.h2atgdesatentophc + "'");
				}
				if(obj.h2atgfluctuantephc != objOriginal.h2atgfluctuantephc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
					arrValores.Add(obj.h2atgfluctuantephc == null ? null : "'" + obj.h2atgfluctuantephc + "'");
				}
				if(obj.h2atgnegativophc != objOriginal.h2atgnegativophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
					arrValores.Add(obj.h2atgnegativophc == null ? null : "'" + obj.h2atgnegativophc + "'");
				}
				if(obj.h2atgnegligentephc != objOriginal.h2atgnegligentephc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
					arrValores.Add(obj.h2atgnegligentephc == null ? null : "'" + obj.h2atgnegligentephc + "'");
				}
				if(obj.h2pagconfusionphc != objOriginal.h2pagconfusionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
					arrValores.Add(obj.h2pagconfusionphc == null ? null : "'" + obj.h2pagconfusionphc + "'");
				}
				if(obj.h2pagdeliriophc != objOriginal.h2pagdeliriophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
					arrValores.Add(obj.h2pagdeliriophc == null ? null : "'" + obj.h2pagdeliriophc + "'");
				}
				if(obj.h2paedelusionesphc != objOriginal.h2paedelusionesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
					arrValores.Add(obj.h2paedelusionesphc == null ? null : "'" + obj.h2paedelusionesphc + "'");
				}
				if(obj.h2paeilsuionesphc != objOriginal.h2paeilsuionesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
					arrValores.Add(obj.h2paeilsuionesphc == null ? null : "'" + obj.h2paeilsuionesphc + "'");
				}
				if(obj.h2paealucinacionesphc != objOriginal.h2paealucinacionesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
					arrValores.Add(obj.h2paealucinacionesphc == null ? null : "'" + obj.h2paealucinacionesphc + "'");
				}
				if(obj.h3conrecientephc != objOriginal.h3conrecientephc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
					arrValores.Add(obj.h3conrecientephc == null ? null : "'" + obj.h3conrecientephc + "'");
				}
				if(obj.h3conremotaphc != objOriginal.h3conremotaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
					arrValores.Add(obj.h3conremotaphc == null ? null : "'" + obj.h3conremotaphc + "'");
				}
				if(obj.h3afeeuforicophc != objOriginal.h3afeeuforicophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
					arrValores.Add(obj.h3afeeuforicophc == null ? null : "'" + obj.h3afeeuforicophc + "'");
				}
				if(obj.h3afeplanophc != objOriginal.h3afeplanophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
					arrValores.Add(obj.h3afeplanophc == null ? null : "'" + obj.h3afeplanophc + "'");
				}
				if(obj.h3afedeprimidophc != objOriginal.h3afedeprimidophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
					arrValores.Add(obj.h3afedeprimidophc == null ? null : "'" + obj.h3afedeprimidophc + "'");
				}
				if(obj.h3afelabilphc != objOriginal.h3afelabilphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
					arrValores.Add(obj.h3afelabilphc == null ? null : "'" + obj.h3afelabilphc + "'");
				}
				if(obj.h3afehostilphc != objOriginal.h3afehostilphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
					arrValores.Add(obj.h3afehostilphc == null ? null : "'" + obj.h3afehostilphc + "'");
				}
				if(obj.h3afeinadecuadophc != objOriginal.h3afeinadecuadophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
					arrValores.Add(obj.h3afeinadecuadophc == null ? null : "'" + obj.h3afeinadecuadophc + "'");
				}
				if(obj.h3hdoizquierdophc != objOriginal.h3hdoizquierdophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
					arrValores.Add(obj.h3hdoizquierdophc == null ? null : "'" + obj.h3hdoizquierdophc + "'");
				}
				if(obj.h3hdoderechophc != objOriginal.h3hdoderechophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
					arrValores.Add(obj.h3hdoderechophc == null ? null : "'" + obj.h3hdoderechophc + "'");
				}
				if(obj.h3cgeexpresionphc != objOriginal.h3cgeexpresionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
					arrValores.Add(obj.h3cgeexpresionphc == null ? null : "'" + obj.h3cgeexpresionphc + "'");
				}
				if(obj.h3cgenominacionphc != objOriginal.h3cgenominacionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
					arrValores.Add(obj.h3cgenominacionphc == null ? null : "'" + obj.h3cgenominacionphc + "'");
				}
				if(obj.h3cgerepeticionphc != objOriginal.h3cgerepeticionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
					arrValores.Add(obj.h3cgerepeticionphc == null ? null : "'" + obj.h3cgerepeticionphc + "'");
				}
				if(obj.h3cgecomprensionphc != objOriginal.h3cgecomprensionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
					arrValores.Add(obj.h3cgecomprensionphc == null ? null : "'" + obj.h3cgecomprensionphc + "'");
				}
				if(obj.h3apebrocaphc != objOriginal.h3apebrocaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
					arrValores.Add(obj.h3apebrocaphc == null ? null : "'" + obj.h3apebrocaphc + "'");
				}
				if(obj.h3apeconduccionphc != objOriginal.h3apeconduccionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
					arrValores.Add(obj.h3apeconduccionphc == null ? null : "'" + obj.h3apeconduccionphc + "'");
				}
				if(obj.h3apewernickephc != objOriginal.h3apewernickephc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
					arrValores.Add(obj.h3apewernickephc == null ? null : "'" + obj.h3apewernickephc + "'");
				}
				if(obj.h3apeglobalphc != objOriginal.h3apeglobalphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
					arrValores.Add(obj.h3apeglobalphc == null ? null : "'" + obj.h3apeglobalphc + "'");
				}
				if(obj.h3apeanomiaphc != objOriginal.h3apeanomiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
					arrValores.Add(obj.h3apeanomiaphc == null ? null : "'" + obj.h3apeanomiaphc + "'");
				}
				if(obj.h3atrmotoraphc != objOriginal.h3atrmotoraphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
					arrValores.Add(obj.h3atrmotoraphc == null ? null : "'" + obj.h3atrmotoraphc + "'");
				}
				if(obj.h3atrsensitivaphc != objOriginal.h3atrsensitivaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
					arrValores.Add(obj.h3atrsensitivaphc == null ? null : "'" + obj.h3atrsensitivaphc + "'");
				}
				if(obj.h3atrmixtaphc != objOriginal.h3atrmixtaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
					arrValores.Add(obj.h3atrmixtaphc == null ? null : "'" + obj.h3atrmixtaphc + "'");
				}
				if(obj.h3ataexpresionphc != objOriginal.h3ataexpresionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
					arrValores.Add(obj.h3ataexpresionphc == null ? null : "'" + obj.h3ataexpresionphc + "'");
				}
				if(obj.h3atacomprensionphc != objOriginal.h3atacomprensionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
					arrValores.Add(obj.h3atacomprensionphc == null ? null : "'" + obj.h3atacomprensionphc + "'");
				}
				if(obj.h3atamixtaphc != objOriginal.h3atamixtaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
					arrValores.Add(obj.h3atamixtaphc == null ? null : "'" + obj.h3atamixtaphc + "'");
				}
				if(obj.h3aprmotoraphc != objOriginal.h3aprmotoraphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
					arrValores.Add(obj.h3aprmotoraphc == null ? null : "'" + obj.h3aprmotoraphc + "'");
				}
				if(obj.h3aprsensorialphc != objOriginal.h3aprsensorialphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
					arrValores.Add(obj.h3aprsensorialphc == null ? null : "'" + obj.h3aprsensorialphc + "'");
				}
				if(obj.h3lesalexiaphc != objOriginal.h3lesalexiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
					arrValores.Add(obj.h3lesalexiaphc == null ? null : "'" + obj.h3lesalexiaphc + "'");
				}
				if(obj.h3lesagrafiaphc != objOriginal.h3lesagrafiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
					arrValores.Add(obj.h3lesagrafiaphc == null ? null : "'" + obj.h3lesagrafiaphc + "'");
				}
				if(obj.h3lpdacalculiaphc != objOriginal.h3lpdacalculiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
					arrValores.Add(obj.h3lpdacalculiaphc == null ? null : "'" + obj.h3lpdacalculiaphc + "'");
				}
				if(obj.h3lpdagrafiaphc != objOriginal.h3lpdagrafiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
					arrValores.Add(obj.h3lpdagrafiaphc == null ? null : "'" + obj.h3lpdagrafiaphc + "'");
				}
				if(obj.h3lpddesorientacionphc != objOriginal.h3lpddesorientacionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
					arrValores.Add(obj.h3lpddesorientacionphc == null ? null : "'" + obj.h3lpddesorientacionphc + "'");
				}
				if(obj.h3lpdagnosiaphc != objOriginal.h3lpdagnosiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
					arrValores.Add(obj.h3lpdagnosiaphc == null ? null : "'" + obj.h3lpdagnosiaphc + "'");
				}
				if(obj.h3agnauditivaphc != objOriginal.h3agnauditivaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
					arrValores.Add(obj.h3agnauditivaphc == null ? null : "'" + obj.h3agnauditivaphc + "'");
				}
				if(obj.h3agnvisualphc != objOriginal.h3agnvisualphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
					arrValores.Add(obj.h3agnvisualphc == null ? null : "'" + obj.h3agnvisualphc + "'");
				}
				if(obj.h3agntactilphc != objOriginal.h3agntactilphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
					arrValores.Add(obj.h3agntactilphc == null ? null : "'" + obj.h3agntactilphc + "'");
				}
				if(obj.h3aprideacionalphc != objOriginal.h3aprideacionalphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
					arrValores.Add(obj.h3aprideacionalphc == null ? null : "'" + obj.h3aprideacionalphc + "'");
				}
				if(obj.h3aprideomotoraphc != objOriginal.h3aprideomotoraphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
					arrValores.Add(obj.h3aprideomotoraphc == null ? null : "'" + obj.h3aprideomotoraphc + "'");
				}
				if(obj.h3aprdesatencionphc != objOriginal.h3aprdesatencionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
					arrValores.Add(obj.h3aprdesatencionphc == null ? null : "'" + obj.h3aprdesatencionphc + "'");
				}
				if(obj.h3anosognosiaphc != objOriginal.h3anosognosiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
					arrValores.Add(obj.h3anosognosiaphc == null ? null : "'" + obj.h3anosognosiaphc + "'");
				}
				if(obj.apiestado != objOriginal.apiestado )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
					arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				}
				if(obj.apitransaccion != objOriginal.apitransaccion )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
					arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				}
				if(obj.usumod != objOriginal.usumod )
				{
					arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
					arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pachistoriaclinica1 a partir de una clase del tipo EntPacHistoriaclinica1 y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpachistoriaclinica1
		/// </returns>
		public int DeleteQuery(EntPacHistoriaclinica1 obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.DeleteBd(EntPacHistoriaclinica1.StrNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pachistoriaclinica1 a partir de una clase del tipo EntPacHistoriaclinica1 y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.epachistoriaclinica1">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionpachistoriaclinica1
		/// </returns>
		public int DeleteQuery(EntPacHistoriaclinica1 obj, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.DeleteBd(EntPacHistoriaclinica1.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pachistoriaclinica1 a partir de una clase del tipo epachistoriaclinica1
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpachistoriaclinica1
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd("pachistoriaclinica1", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pachistoriaclinica1 a partir de una clase del tipo epachistoriaclinica1
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica1
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionpachistoriaclinica1
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd(EntPacHistoriaclinica1.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region Llenado de elementos

		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, String textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, String textField, EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, String textField, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.DataValueField = table.Columns[0].ColumnName;
					cmb.DataTextField = table.Columns[1].ColumnName;
					cmb.DataSource = table;
					cmb.DataBind();

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntPacHistoriaclinica1.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica1.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridViewOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, String textField, EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, String textField, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica1
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica1
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntPacHistoriaclinica1.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1motivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fumphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fupphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1taphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1fcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1frphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tempphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pesophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h1pcphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica1.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica1.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica1.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica1.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica1
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntPacHistoriaclinica1.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 


		#endregion

		#region Funciones Internas

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto pachistoriaclinica1
		/// </returns>
		internal EntPacHistoriaclinica1 crearObjeto(DataRow row)
		{
			var obj = new EntPacHistoriaclinica1();
			obj.idppa = GetColumnType(row[EntPacHistoriaclinica1.Fields.idppa.ToString()], EntPacHistoriaclinica1.Fields.idppa);
			obj.h1motivophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1motivophc.ToString()], EntPacHistoriaclinica1.Fields.h1motivophc);
			obj.h1enfermedadphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString()], EntPacHistoriaclinica1.Fields.h1enfermedadphc);
			obj.h1cirujiasphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString()], EntPacHistoriaclinica1.Fields.h1cirujiasphc);
			obj.h1infeccionesphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString()], EntPacHistoriaclinica1.Fields.h1infeccionesphc);
			obj.h1traumatismosphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString()], EntPacHistoriaclinica1.Fields.h1traumatismosphc);
			obj.h1cardiovascularphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString()], EntPacHistoriaclinica1.Fields.h1cardiovascularphc);
			obj.h1metabolicophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString()], EntPacHistoriaclinica1.Fields.h1metabolicophc);
			obj.h1toxicophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1toxicophc.ToString()], EntPacHistoriaclinica1.Fields.h1toxicophc);
			obj.h1familiarphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1familiarphc.ToString()], EntPacHistoriaclinica1.Fields.h1familiarphc);
			obj.h1ginecoobsphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString()], EntPacHistoriaclinica1.Fields.h1ginecoobsphc);
			obj.h1otrosphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1otrosphc.ToString()], EntPacHistoriaclinica1.Fields.h1otrosphc);
			obj.h1embarazosphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString()], EntPacHistoriaclinica1.Fields.h1embarazosphc);
			obj.h1gestaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1gestaphc.ToString()], EntPacHistoriaclinica1.Fields.h1gestaphc);
			obj.h1abortosphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1abortosphc.ToString()], EntPacHistoriaclinica1.Fields.h1abortosphc);
			obj.h1mesntruacionesphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString()], EntPacHistoriaclinica1.Fields.h1mesntruacionesphc);
			obj.h1fumphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1fumphc.ToString()], EntPacHistoriaclinica1.Fields.h1fumphc);
			obj.h1fupphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1fupphc.ToString()], EntPacHistoriaclinica1.Fields.h1fupphc);
			obj.h1revisionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1revisionphc.ToString()], EntPacHistoriaclinica1.Fields.h1revisionphc);
			obj.h1examenfisicophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString()], EntPacHistoriaclinica1.Fields.h1examenfisicophc);
			obj.h1taphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1taphc.ToString()], EntPacHistoriaclinica1.Fields.h1taphc);
			obj.h1fcphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1fcphc.ToString()], EntPacHistoriaclinica1.Fields.h1fcphc);
			obj.h1frphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1frphc.ToString()], EntPacHistoriaclinica1.Fields.h1frphc);
			obj.h1tempphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1tempphc.ToString()], EntPacHistoriaclinica1.Fields.h1tempphc);
			obj.h1tallaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1tallaphc.ToString()], EntPacHistoriaclinica1.Fields.h1tallaphc);
			obj.h1pesophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1pesophc.ToString()], EntPacHistoriaclinica1.Fields.h1pesophc);
			obj.h1pcphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1pcphc.ToString()], EntPacHistoriaclinica1.Fields.h1pcphc);
			obj.h2cabezaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString()], EntPacHistoriaclinica1.Fields.h2cabezaphc);
			obj.h2cuellophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2cuellophc.ToString()], EntPacHistoriaclinica1.Fields.h2cuellophc);
			obj.h2toraxphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2toraxphc.ToString()], EntPacHistoriaclinica1.Fields.h2toraxphc);
			obj.h2abdomenphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString()], EntPacHistoriaclinica1.Fields.h2abdomenphc);
			obj.h2genitourinariophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString()], EntPacHistoriaclinica1.Fields.h2genitourinariophc);
			obj.h2extremidadesphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString()], EntPacHistoriaclinica1.Fields.h2extremidadesphc);
			obj.h2conalertaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString()], EntPacHistoriaclinica1.Fields.h2conalertaphc);
			obj.h2conconfusionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString()], EntPacHistoriaclinica1.Fields.h2conconfusionphc);
			obj.h2consomnolenciaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString()], EntPacHistoriaclinica1.Fields.h2consomnolenciaphc);
			obj.h2conestuforphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString()], EntPacHistoriaclinica1.Fields.h2conestuforphc);
			obj.h2concomaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2concomaphc.ToString()], EntPacHistoriaclinica1.Fields.h2concomaphc);
			obj.h2oriespaciophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString()], EntPacHistoriaclinica1.Fields.h2oriespaciophc);
			obj.h2orilugarphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString()], EntPacHistoriaclinica1.Fields.h2orilugarphc);
			obj.h2oripersonaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString()], EntPacHistoriaclinica1.Fields.h2oripersonaphc);
			obj.h2razideaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2razideaphc.ToString()], EntPacHistoriaclinica1.Fields.h2razideaphc);
			obj.h2razjuiciophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString()], EntPacHistoriaclinica1.Fields.h2razjuiciophc);
			obj.h2razabstraccionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString()], EntPacHistoriaclinica1.Fields.h2razabstraccionphc);
			obj.h2conocimientosgenerales = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString()], EntPacHistoriaclinica1.Fields.h2conocimientosgenerales);
			obj.h2atgdesatentophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString()], EntPacHistoriaclinica1.Fields.h2atgdesatentophc);
			obj.h2atgfluctuantephc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString()], EntPacHistoriaclinica1.Fields.h2atgfluctuantephc);
			obj.h2atgnegativophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString()], EntPacHistoriaclinica1.Fields.h2atgnegativophc);
			obj.h2atgnegligentephc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString()], EntPacHistoriaclinica1.Fields.h2atgnegligentephc);
			obj.h2pagconfusionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString()], EntPacHistoriaclinica1.Fields.h2pagconfusionphc);
			obj.h2pagdeliriophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString()], EntPacHistoriaclinica1.Fields.h2pagdeliriophc);
			obj.h2paedelusionesphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString()], EntPacHistoriaclinica1.Fields.h2paedelusionesphc);
			obj.h2paeilsuionesphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString()], EntPacHistoriaclinica1.Fields.h2paeilsuionesphc);
			obj.h2paealucinacionesphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString()], EntPacHistoriaclinica1.Fields.h2paealucinacionesphc);
			obj.h3conrecientephc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString()], EntPacHistoriaclinica1.Fields.h3conrecientephc);
			obj.h3conremotaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString()], EntPacHistoriaclinica1.Fields.h3conremotaphc);
			obj.h3afeeuforicophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString()], EntPacHistoriaclinica1.Fields.h3afeeuforicophc);
			obj.h3afeplanophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString()], EntPacHistoriaclinica1.Fields.h3afeplanophc);
			obj.h3afedeprimidophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString()], EntPacHistoriaclinica1.Fields.h3afedeprimidophc);
			obj.h3afelabilphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString()], EntPacHistoriaclinica1.Fields.h3afelabilphc);
			obj.h3afehostilphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString()], EntPacHistoriaclinica1.Fields.h3afehostilphc);
			obj.h3afeinadecuadophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString()], EntPacHistoriaclinica1.Fields.h3afeinadecuadophc);
			obj.h3hdoizquierdophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString()], EntPacHistoriaclinica1.Fields.h3hdoizquierdophc);
			obj.h3hdoderechophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString()], EntPacHistoriaclinica1.Fields.h3hdoderechophc);
			obj.h3cgeexpresionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString()], EntPacHistoriaclinica1.Fields.h3cgeexpresionphc);
			obj.h3cgenominacionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString()], EntPacHistoriaclinica1.Fields.h3cgenominacionphc);
			obj.h3cgerepeticionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString()], EntPacHistoriaclinica1.Fields.h3cgerepeticionphc);
			obj.h3cgecomprensionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString()], EntPacHistoriaclinica1.Fields.h3cgecomprensionphc);
			obj.h3apebrocaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString()], EntPacHistoriaclinica1.Fields.h3apebrocaphc);
			obj.h3apeconduccionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString()], EntPacHistoriaclinica1.Fields.h3apeconduccionphc);
			obj.h3apewernickephc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString()], EntPacHistoriaclinica1.Fields.h3apewernickephc);
			obj.h3apeglobalphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString()], EntPacHistoriaclinica1.Fields.h3apeglobalphc);
			obj.h3apeanomiaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString()], EntPacHistoriaclinica1.Fields.h3apeanomiaphc);
			obj.h3atrmotoraphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString()], EntPacHistoriaclinica1.Fields.h3atrmotoraphc);
			obj.h3atrsensitivaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString()], EntPacHistoriaclinica1.Fields.h3atrsensitivaphc);
			obj.h3atrmixtaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString()], EntPacHistoriaclinica1.Fields.h3atrmixtaphc);
			obj.h3ataexpresionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString()], EntPacHistoriaclinica1.Fields.h3ataexpresionphc);
			obj.h3atacomprensionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString()], EntPacHistoriaclinica1.Fields.h3atacomprensionphc);
			obj.h3atamixtaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString()], EntPacHistoriaclinica1.Fields.h3atamixtaphc);
			obj.h3aprmotoraphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString()], EntPacHistoriaclinica1.Fields.h3aprmotoraphc);
			obj.h3aprsensorialphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString()], EntPacHistoriaclinica1.Fields.h3aprsensorialphc);
			obj.h3lesalexiaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString()], EntPacHistoriaclinica1.Fields.h3lesalexiaphc);
			obj.h3lesagrafiaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString()], EntPacHistoriaclinica1.Fields.h3lesagrafiaphc);
			obj.h3lpdacalculiaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString()], EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc);
			obj.h3lpdagrafiaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString()], EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc);
			obj.h3lpddesorientacionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString()], EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc);
			obj.h3lpdagnosiaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString()], EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc);
			obj.h3agnauditivaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString()], EntPacHistoriaclinica1.Fields.h3agnauditivaphc);
			obj.h3agnvisualphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString()], EntPacHistoriaclinica1.Fields.h3agnvisualphc);
			obj.h3agntactilphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString()], EntPacHistoriaclinica1.Fields.h3agntactilphc);
			obj.h3aprideacionalphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString()], EntPacHistoriaclinica1.Fields.h3aprideacionalphc);
			obj.h3aprideomotoraphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString()], EntPacHistoriaclinica1.Fields.h3aprideomotoraphc);
			obj.h3aprdesatencionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString()], EntPacHistoriaclinica1.Fields.h3aprdesatencionphc);
			obj.h3anosognosiaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString()], EntPacHistoriaclinica1.Fields.h3anosognosiaphc);
			obj.apiestado = GetColumnType(row[EntPacHistoriaclinica1.Fields.apiestado.ToString()], EntPacHistoriaclinica1.Fields.apiestado);
			obj.apitransaccion = GetColumnType(row[EntPacHistoriaclinica1.Fields.apitransaccion.ToString()], EntPacHistoriaclinica1.Fields.apitransaccion);
			obj.usucre = GetColumnType(row[EntPacHistoriaclinica1.Fields.usucre.ToString()], EntPacHistoriaclinica1.Fields.usucre);
			obj.feccre = GetColumnType(row[EntPacHistoriaclinica1.Fields.feccre.ToString()], EntPacHistoriaclinica1.Fields.feccre);
			obj.usumod = GetColumnType(row[EntPacHistoriaclinica1.Fields.usumod.ToString()], EntPacHistoriaclinica1.Fields.usumod);
			obj.fecmod = GetColumnType(row[EntPacHistoriaclinica1.Fields.fecmod.ToString()], EntPacHistoriaclinica1.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto pachistoriaclinica1
		/// </returns>
		internal EntPacHistoriaclinica1 crearObjetoRevisado(DataRow row)
		{
			var obj = new EntPacHistoriaclinica1();
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.idppa.ToString()))
				obj.idppa = GetColumnType(row[EntPacHistoriaclinica1.Fields.idppa.ToString()], EntPacHistoriaclinica1.Fields.idppa);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1motivophc.ToString()))
				obj.h1motivophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1motivophc.ToString()], EntPacHistoriaclinica1.Fields.h1motivophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString()))
				obj.h1enfermedadphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1enfermedadphc.ToString()], EntPacHistoriaclinica1.Fields.h1enfermedadphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString()))
				obj.h1cirujiasphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1cirujiasphc.ToString()], EntPacHistoriaclinica1.Fields.h1cirujiasphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString()))
				obj.h1infeccionesphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1infeccionesphc.ToString()], EntPacHistoriaclinica1.Fields.h1infeccionesphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString()))
				obj.h1traumatismosphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1traumatismosphc.ToString()], EntPacHistoriaclinica1.Fields.h1traumatismosphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString()))
				obj.h1cardiovascularphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1cardiovascularphc.ToString()], EntPacHistoriaclinica1.Fields.h1cardiovascularphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString()))
				obj.h1metabolicophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1metabolicophc.ToString()], EntPacHistoriaclinica1.Fields.h1metabolicophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1toxicophc.ToString()))
				obj.h1toxicophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1toxicophc.ToString()], EntPacHistoriaclinica1.Fields.h1toxicophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1familiarphc.ToString()))
				obj.h1familiarphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1familiarphc.ToString()], EntPacHistoriaclinica1.Fields.h1familiarphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString()))
				obj.h1ginecoobsphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1ginecoobsphc.ToString()], EntPacHistoriaclinica1.Fields.h1ginecoobsphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1otrosphc.ToString()))
				obj.h1otrosphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1otrosphc.ToString()], EntPacHistoriaclinica1.Fields.h1otrosphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString()))
				obj.h1embarazosphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1embarazosphc.ToString()], EntPacHistoriaclinica1.Fields.h1embarazosphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1gestaphc.ToString()))
				obj.h1gestaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1gestaphc.ToString()], EntPacHistoriaclinica1.Fields.h1gestaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1abortosphc.ToString()))
				obj.h1abortosphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1abortosphc.ToString()], EntPacHistoriaclinica1.Fields.h1abortosphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString()))
				obj.h1mesntruacionesphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1mesntruacionesphc.ToString()], EntPacHistoriaclinica1.Fields.h1mesntruacionesphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1fumphc.ToString()))
				obj.h1fumphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1fumphc.ToString()], EntPacHistoriaclinica1.Fields.h1fumphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1fupphc.ToString()))
				obj.h1fupphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1fupphc.ToString()], EntPacHistoriaclinica1.Fields.h1fupphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1revisionphc.ToString()))
				obj.h1revisionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1revisionphc.ToString()], EntPacHistoriaclinica1.Fields.h1revisionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString()))
				obj.h1examenfisicophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1examenfisicophc.ToString()], EntPacHistoriaclinica1.Fields.h1examenfisicophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1taphc.ToString()))
				obj.h1taphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1taphc.ToString()], EntPacHistoriaclinica1.Fields.h1taphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1fcphc.ToString()))
				obj.h1fcphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1fcphc.ToString()], EntPacHistoriaclinica1.Fields.h1fcphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1frphc.ToString()))
				obj.h1frphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1frphc.ToString()], EntPacHistoriaclinica1.Fields.h1frphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1tempphc.ToString()))
				obj.h1tempphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1tempphc.ToString()], EntPacHistoriaclinica1.Fields.h1tempphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1tallaphc.ToString()))
				obj.h1tallaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1tallaphc.ToString()], EntPacHistoriaclinica1.Fields.h1tallaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1pesophc.ToString()))
				obj.h1pesophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1pesophc.ToString()], EntPacHistoriaclinica1.Fields.h1pesophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h1pcphc.ToString()))
				obj.h1pcphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h1pcphc.ToString()], EntPacHistoriaclinica1.Fields.h1pcphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString()))
				obj.h2cabezaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2cabezaphc.ToString()], EntPacHistoriaclinica1.Fields.h2cabezaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2cuellophc.ToString()))
				obj.h2cuellophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2cuellophc.ToString()], EntPacHistoriaclinica1.Fields.h2cuellophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2toraxphc.ToString()))
				obj.h2toraxphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2toraxphc.ToString()], EntPacHistoriaclinica1.Fields.h2toraxphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString()))
				obj.h2abdomenphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2abdomenphc.ToString()], EntPacHistoriaclinica1.Fields.h2abdomenphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString()))
				obj.h2genitourinariophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2genitourinariophc.ToString()], EntPacHistoriaclinica1.Fields.h2genitourinariophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString()))
				obj.h2extremidadesphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2extremidadesphc.ToString()], EntPacHistoriaclinica1.Fields.h2extremidadesphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString()))
				obj.h2conalertaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2conalertaphc.ToString()], EntPacHistoriaclinica1.Fields.h2conalertaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString()))
				obj.h2conconfusionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2conconfusionphc.ToString()], EntPacHistoriaclinica1.Fields.h2conconfusionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString()))
				obj.h2consomnolenciaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2consomnolenciaphc.ToString()], EntPacHistoriaclinica1.Fields.h2consomnolenciaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString()))
				obj.h2conestuforphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2conestuforphc.ToString()], EntPacHistoriaclinica1.Fields.h2conestuforphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2concomaphc.ToString()))
				obj.h2concomaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2concomaphc.ToString()], EntPacHistoriaclinica1.Fields.h2concomaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString()))
				obj.h2oriespaciophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2oriespaciophc.ToString()], EntPacHistoriaclinica1.Fields.h2oriespaciophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString()))
				obj.h2orilugarphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2orilugarphc.ToString()], EntPacHistoriaclinica1.Fields.h2orilugarphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString()))
				obj.h2oripersonaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2oripersonaphc.ToString()], EntPacHistoriaclinica1.Fields.h2oripersonaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2razideaphc.ToString()))
				obj.h2razideaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2razideaphc.ToString()], EntPacHistoriaclinica1.Fields.h2razideaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString()))
				obj.h2razjuiciophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2razjuiciophc.ToString()], EntPacHistoriaclinica1.Fields.h2razjuiciophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString()))
				obj.h2razabstraccionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2razabstraccionphc.ToString()], EntPacHistoriaclinica1.Fields.h2razabstraccionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString()))
				obj.h2conocimientosgenerales = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2conocimientosgenerales.ToString()], EntPacHistoriaclinica1.Fields.h2conocimientosgenerales);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString()))
				obj.h2atgdesatentophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2atgdesatentophc.ToString()], EntPacHistoriaclinica1.Fields.h2atgdesatentophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString()))
				obj.h2atgfluctuantephc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2atgfluctuantephc.ToString()], EntPacHistoriaclinica1.Fields.h2atgfluctuantephc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString()))
				obj.h2atgnegativophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2atgnegativophc.ToString()], EntPacHistoriaclinica1.Fields.h2atgnegativophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString()))
				obj.h2atgnegligentephc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2atgnegligentephc.ToString()], EntPacHistoriaclinica1.Fields.h2atgnegligentephc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString()))
				obj.h2pagconfusionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2pagconfusionphc.ToString()], EntPacHistoriaclinica1.Fields.h2pagconfusionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString()))
				obj.h2pagdeliriophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2pagdeliriophc.ToString()], EntPacHistoriaclinica1.Fields.h2pagdeliriophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString()))
				obj.h2paedelusionesphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2paedelusionesphc.ToString()], EntPacHistoriaclinica1.Fields.h2paedelusionesphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString()))
				obj.h2paeilsuionesphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2paeilsuionesphc.ToString()], EntPacHistoriaclinica1.Fields.h2paeilsuionesphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString()))
				obj.h2paealucinacionesphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h2paealucinacionesphc.ToString()], EntPacHistoriaclinica1.Fields.h2paealucinacionesphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString()))
				obj.h3conrecientephc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3conrecientephc.ToString()], EntPacHistoriaclinica1.Fields.h3conrecientephc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString()))
				obj.h3conremotaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3conremotaphc.ToString()], EntPacHistoriaclinica1.Fields.h3conremotaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString()))
				obj.h3afeeuforicophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3afeeuforicophc.ToString()], EntPacHistoriaclinica1.Fields.h3afeeuforicophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString()))
				obj.h3afeplanophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3afeplanophc.ToString()], EntPacHistoriaclinica1.Fields.h3afeplanophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString()))
				obj.h3afedeprimidophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3afedeprimidophc.ToString()], EntPacHistoriaclinica1.Fields.h3afedeprimidophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString()))
				obj.h3afelabilphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3afelabilphc.ToString()], EntPacHistoriaclinica1.Fields.h3afelabilphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString()))
				obj.h3afehostilphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3afehostilphc.ToString()], EntPacHistoriaclinica1.Fields.h3afehostilphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString()))
				obj.h3afeinadecuadophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3afeinadecuadophc.ToString()], EntPacHistoriaclinica1.Fields.h3afeinadecuadophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString()))
				obj.h3hdoizquierdophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3hdoizquierdophc.ToString()], EntPacHistoriaclinica1.Fields.h3hdoizquierdophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString()))
				obj.h3hdoderechophc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3hdoderechophc.ToString()], EntPacHistoriaclinica1.Fields.h3hdoderechophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString()))
				obj.h3cgeexpresionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3cgeexpresionphc.ToString()], EntPacHistoriaclinica1.Fields.h3cgeexpresionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString()))
				obj.h3cgenominacionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3cgenominacionphc.ToString()], EntPacHistoriaclinica1.Fields.h3cgenominacionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString()))
				obj.h3cgerepeticionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3cgerepeticionphc.ToString()], EntPacHistoriaclinica1.Fields.h3cgerepeticionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString()))
				obj.h3cgecomprensionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3cgecomprensionphc.ToString()], EntPacHistoriaclinica1.Fields.h3cgecomprensionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString()))
				obj.h3apebrocaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3apebrocaphc.ToString()], EntPacHistoriaclinica1.Fields.h3apebrocaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString()))
				obj.h3apeconduccionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3apeconduccionphc.ToString()], EntPacHistoriaclinica1.Fields.h3apeconduccionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString()))
				obj.h3apewernickephc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3apewernickephc.ToString()], EntPacHistoriaclinica1.Fields.h3apewernickephc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString()))
				obj.h3apeglobalphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3apeglobalphc.ToString()], EntPacHistoriaclinica1.Fields.h3apeglobalphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString()))
				obj.h3apeanomiaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3apeanomiaphc.ToString()], EntPacHistoriaclinica1.Fields.h3apeanomiaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString()))
				obj.h3atrmotoraphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3atrmotoraphc.ToString()], EntPacHistoriaclinica1.Fields.h3atrmotoraphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString()))
				obj.h3atrsensitivaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3atrsensitivaphc.ToString()], EntPacHistoriaclinica1.Fields.h3atrsensitivaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString()))
				obj.h3atrmixtaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3atrmixtaphc.ToString()], EntPacHistoriaclinica1.Fields.h3atrmixtaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString()))
				obj.h3ataexpresionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3ataexpresionphc.ToString()], EntPacHistoriaclinica1.Fields.h3ataexpresionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString()))
				obj.h3atacomprensionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3atacomprensionphc.ToString()], EntPacHistoriaclinica1.Fields.h3atacomprensionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString()))
				obj.h3atamixtaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3atamixtaphc.ToString()], EntPacHistoriaclinica1.Fields.h3atamixtaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString()))
				obj.h3aprmotoraphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3aprmotoraphc.ToString()], EntPacHistoriaclinica1.Fields.h3aprmotoraphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString()))
				obj.h3aprsensorialphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3aprsensorialphc.ToString()], EntPacHistoriaclinica1.Fields.h3aprsensorialphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString()))
				obj.h3lesalexiaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3lesalexiaphc.ToString()], EntPacHistoriaclinica1.Fields.h3lesalexiaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString()))
				obj.h3lesagrafiaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3lesagrafiaphc.ToString()], EntPacHistoriaclinica1.Fields.h3lesagrafiaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString()))
				obj.h3lpdacalculiaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc.ToString()], EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString()))
				obj.h3lpdagrafiaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc.ToString()], EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString()))
				obj.h3lpddesorientacionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc.ToString()], EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString()))
				obj.h3lpdagnosiaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc.ToString()], EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString()))
				obj.h3agnauditivaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3agnauditivaphc.ToString()], EntPacHistoriaclinica1.Fields.h3agnauditivaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString()))
				obj.h3agnvisualphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3agnvisualphc.ToString()], EntPacHistoriaclinica1.Fields.h3agnvisualphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString()))
				obj.h3agntactilphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3agntactilphc.ToString()], EntPacHistoriaclinica1.Fields.h3agntactilphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString()))
				obj.h3aprideacionalphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3aprideacionalphc.ToString()], EntPacHistoriaclinica1.Fields.h3aprideacionalphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString()))
				obj.h3aprideomotoraphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3aprideomotoraphc.ToString()], EntPacHistoriaclinica1.Fields.h3aprideomotoraphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString()))
				obj.h3aprdesatencionphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3aprdesatencionphc.ToString()], EntPacHistoriaclinica1.Fields.h3aprdesatencionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString()))
				obj.h3anosognosiaphc = GetColumnType(row[EntPacHistoriaclinica1.Fields.h3anosognosiaphc.ToString()], EntPacHistoriaclinica1.Fields.h3anosognosiaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[EntPacHistoriaclinica1.Fields.apiestado.ToString()], EntPacHistoriaclinica1.Fields.apiestado);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.apitransaccion.ToString()))
				obj.apitransaccion = GetColumnType(row[EntPacHistoriaclinica1.Fields.apitransaccion.ToString()], EntPacHistoriaclinica1.Fields.apitransaccion);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[EntPacHistoriaclinica1.Fields.usucre.ToString()], EntPacHistoriaclinica1.Fields.usucre);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[EntPacHistoriaclinica1.Fields.feccre.ToString()], EntPacHistoriaclinica1.Fields.feccre);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[EntPacHistoriaclinica1.Fields.usumod.ToString()], EntPacHistoriaclinica1.Fields.usumod);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica1.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[EntPacHistoriaclinica1.Fields.fecmod.ToString()], EntPacHistoriaclinica1.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtpachistoriaclinica1" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos pachistoriaclinica1
		/// </returns>
		internal List<EntPacHistoriaclinica1> crearLista(DataTable dtpachistoriaclinica1)
		{
			var list = new List<EntPacHistoriaclinica1>();
			
			foreach (DataRow row in dtpachistoriaclinica1.Rows)
			{
				var obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtpachistoriaclinica1" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos pachistoriaclinica1
		/// </returns>
		internal List<EntPacHistoriaclinica1> crearListaRevisada(DataTable dtpachistoriaclinica1)
		{
			List<EntPacHistoriaclinica1> list = new List<EntPacHistoriaclinica1>();
			
			foreach (DataRow row in dtpachistoriaclinica1.Rows)
			{
				var obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtpachistoriaclinica1" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos pachistoriaclinica1
		/// </returns>
		internal Queue<EntPacHistoriaclinica1> crearCola(DataTable dtpachistoriaclinica1)
		{
			Queue<EntPacHistoriaclinica1> cola = new Queue<EntPacHistoriaclinica1>();
			
			EntPacHistoriaclinica1 obj = new EntPacHistoriaclinica1();
			foreach (DataRow row in dtpachistoriaclinica1.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtpachistoriaclinica1" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos pachistoriaclinica1
		/// </returns>
		internal Stack<EntPacHistoriaclinica1> crearPila(DataTable dtpachistoriaclinica1)
		{
			Stack<EntPacHistoriaclinica1> pila = new Stack<EntPacHistoriaclinica1>();
			
			foreach (DataRow row in dtpachistoriaclinica1.Rows)
			{
				var obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtpachistoriaclinica1" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos pachistoriaclinica1
		/// </returns>
		internal Dictionary<String, EntPacHistoriaclinica1> crearDiccionario(DataTable dtpachistoriaclinica1)
		{
			Dictionary<String, EntPacHistoriaclinica1>  miDic = new Dictionary<String, EntPacHistoriaclinica1>();
			
			foreach (DataRow row in dtpachistoriaclinica1.Rows)
			{
				var obj = crearObjeto(row);
				miDic.Add(obj.idppa.ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtpachistoriaclinica1" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 HashTable de Objetos pachistoriaclinica1
		/// </returns>
		internal Hashtable crearHashTable(DataTable dtpachistoriaclinica1)
		{
			Hashtable miTabla = new Hashtable();
			
			foreach (DataRow row in dtpachistoriaclinica1.Rows)
			{
				var obj = crearObjeto(row);
				miTabla.Add(obj.idppa.ToString(), obj);
			}
			return miTabla;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtpachistoriaclinica1" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos pachistoriaclinica1
		/// </returns>
		internal Dictionary<String, EntPacHistoriaclinica1> crearDiccionarioRevisado(DataTable dtpachistoriaclinica1)
		{
			Dictionary<String, EntPacHistoriaclinica1>  miDic = new Dictionary<String, EntPacHistoriaclinica1>();
			
			foreach (DataRow row in dtpachistoriaclinica1.Rows)
			{
				var obj = crearObjetoRevisado(row);
				miDic.Add(obj.idppa.ToString(), obj);
			}
			return miDic;
		}
		
		internal Dictionary<String, EntPacHistoriaclinica1> crearDiccionario(DataTable dtpachistoriaclinica1, EntPacHistoriaclinica1.Fields dicKey)
		{
			Dictionary<String, EntPacHistoriaclinica1>  miDic = new Dictionary<String, EntPacHistoriaclinica1>();
			
			foreach (DataRow row in dtpachistoriaclinica1.Rows)
			{
				var obj = crearObjeto(row);
				
				var nameOfProperty = dicKey.ToString();
				var propertyInfo = obj.GetType().GetProperty(nameOfProperty);
				var value = propertyInfo.GetValue(obj, null);
				
				miDic.Add(value.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
		#endregion

	}
}

