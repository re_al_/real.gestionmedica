#region 
/***********************************************************************************************************
	NOMBRE:       RnPacRecetas
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla pacrecetas

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        16/07/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Interface;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Modelo
{
	public class RnPacRecetas: IPacRecetas
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region IPacRecetas Members

		#region Reflection

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string GetTableScript()
		{
			TableClass tabla = new TableClass(typeof(EntPacRecetas));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, EntPacRecetas.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntPacRecetas).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="strField">Nombre de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, string strField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntPacRecetas).GetProperty(strField).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

/// <summary>
/// Inserta una valor a una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">Es el nombre de la propiedad</param>
/// <param name="dynValor">Es el valor que se insertara a la propiedad</param>
public void SetDato(ref EntPacRecetas obj, string strPropiedad, dynamic dynValor)
{
	if (obj == null) throw new ArgumentNullException();
	obj.GetType().GetProperty(strPropiedad).SetValue(obj, GetColumnType(dynValor, strPropiedad), null);
}

/// <summary>
/// Obtiene el valor de una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">El nombre de la propiedad de la que se obtendra el valor</param>
/// <returns>Devuelve el valor del a propiedad seleccionada</returns>
public dynamic GetDato(ref EntPacRecetas obj, string strPropiedad)
{
	if (obj == null) return null;
	var propertyInfo = obj.GetType().GetProperty(strPropiedad);
	return GetColumnType(propertyInfo.GetValue(obj, null), strPropiedad);
}

		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla pacrecetas a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla pacrecetas
		/// </returns>
		public string CreatePk(string[] args)
		{
			return args[0];
		}
		
		#endregion 

		#region ObtenerObjeto

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacRecetas a partir de la llave primaria
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public EntPacRecetas ObtenerObjeto(Int64 Int64idpre)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntPacRecetas.Fields.idpre.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64idpre + "'");
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacRecetas a partir del usuario que inserta
		/// </summary>
		/// <param name="strUsuCre">Login o nombre de usuario</param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public EntPacRecetas ObtenerObjetoInsertado(string strUsuCre)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntPacRecetas.Fields.usucre.ToString());
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + strUsuCre + "'");
			
			int iIdInsertado = FuncionesMax(EntPacRecetas.Fields.idpre, arrColumnasWhere, arrValoresWhere);
			
			return ObtenerObjeto(iIdInsertado);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public EntPacRecetas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public EntPacRecetas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntPacRecetas obj = new EntPacRecetas();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public EntPacRecetas ObtenerObjeto(Hashtable htbFiltro)
		{
			return ObtenerObjeto(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public EntPacRecetas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntPacRecetas obj = new EntPacRecetas();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public EntPacRecetas ObtenerObjeto(EntPacRecetas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public EntPacRecetas ObtenerObjeto(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo EntPacRecetas a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo EntPacRecetas
		/// </returns>
		public EntPacRecetas ObtenerObjeto(Int64 Int64idpre, ref CTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntPacRecetas.Fields.idpre.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64idpre + "'");
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public EntPacRecetas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public EntPacRecetas ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerObjeto(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public EntPacRecetas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public EntPacRecetas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					EntPacRecetas obj = new EntPacRecetas();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public EntPacRecetas ObtenerObjeto(EntPacRecetas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public EntPacRecetas ObtenerObjeto(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion

		#region ObtenerLista

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacRecetas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerLista(EntPacRecetas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerLista(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacRecetas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerLista(EntPacRecetas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerLista(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerLista(Hashtable htbFiltro)
		{
			return ObtenerLista(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacRecetas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerLista(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacRecetas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacRecetas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacRecetas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, EntPacRecetas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacRecetas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacRecetas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, EntPacRecetas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerCola y Obtener Pila

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacRecetas> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacRecetas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacRecetas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntPacRecetas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacRecetas> ObtenerCola(EntPacRecetas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacRecetas> ObtenerCola(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacRecetas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacRecetas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntPacRecetas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacRecetas> ObtenerCola(EntPacRecetas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacRecetas> ObtenerCola(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacRecetas> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacRecetas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacRecetas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntPacRecetas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacRecetas> ObtenerPila(EntPacRecetas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacRecetas> ObtenerPila(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacRecetas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacRecetas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntPacRecetas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacRecetas> ObtenerPila(EntPacRecetas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacRecetas> ObtenerPila(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerDataTable

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla pacrecetas
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(EntPacRecetas.Fields.idpre.ToString(),typeof(EntPacRecetas).GetProperty(EntPacRecetas.Fields.idpre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacRecetas.Fields.idppa.ToString(),typeof(EntPacRecetas).GetProperty(EntPacRecetas.Fields.idppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacRecetas.Fields.idcpl.ToString(),typeof(EntPacRecetas).GetProperty(EntPacRecetas.Fields.idcpl.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacRecetas.Fields.fechapre.ToString(),typeof(EntPacRecetas).GetProperty(EntPacRecetas.Fields.fechapre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacRecetas.Fields.rtfpre.ToString(),typeof(EntPacRecetas).GetProperty(EntPacRecetas.Fields.rtfpre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacRecetas.Fields.textopre.ToString(),typeof(EntPacRecetas).GetProperty(EntPacRecetas.Fields.textopre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacRecetas.Fields.apiestado.ToString(),typeof(EntPacRecetas).GetProperty(EntPacRecetas.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacRecetas.Fields.apitransaccion.ToString(),typeof(EntPacRecetas).GetProperty(EntPacRecetas.Fields.apitransaccion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacRecetas.Fields.usucre.ToString(),typeof(EntPacRecetas).GetProperty(EntPacRecetas.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacRecetas.Fields.feccre.ToString(),typeof(EntPacRecetas).GetProperty(EntPacRecetas.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacRecetas.Fields.usumod.ToString(),typeof(EntPacRecetas).GetProperty(EntPacRecetas.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacRecetas.Fields.fecmod.ToString(),typeof(EntPacRecetas).GetProperty(EntPacRecetas.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacRecetas.Fields.htmlpre.ToString(),typeof(EntPacRecetas).GetProperty(EntPacRecetas.Fields.htmlpre.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una pacrecetas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla pacrecetas
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable ObtenerDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE pacrecetas
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable ObtenerDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacrecetas
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableOr(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntPacRecetas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntPacRecetas.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacRecetas.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacRecetas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region ObtenerDiccionario

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacRecetas> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacRecetas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacRecetas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntPacRecetas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacRecetas> ObtenerDiccionario(EntPacRecetas.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacRecetas> ObtenerDiccionario(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacRecetas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacRecetas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntPacRecetas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacRecetas> ObtenerDiccionario(EntPacRecetas.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacRecetas a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacRecetas> ObtenerDiccionario(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		public Dictionary<String, EntPacRecetas> ObtenerDiccionarioKey(EntPacRecetas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntPacRecetas> ObtenerDiccionarioKey(String strParamAdic, EntPacRecetas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdic, dicKey);
		}
		
		public Dictionary<String, EntPacRecetas> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, EntPacRecetas.Fields dicKey)
		{
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, "", dicKey);
		}
		
		public Dictionary<String, EntPacRecetas> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, EntPacRecetas.Fields dicKey)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table, dicKey);
				}
				else
					return new Dictionary<string, EntPacRecetas>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		public Dictionary<String, EntPacRecetas> ObtenerDiccionarioKey(EntPacRecetas.Fields searchField, object searchValue, EntPacRecetas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntPacRecetas> ObtenerDiccionarioKey(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales, EntPacRecetas.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey);
		}
		

		#endregion 

		#region ObjetoASp

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla pacrecetas
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntPacRecetas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntPacRecetas.Fields.idpre.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.idppa.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.textopre.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.usucre.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.feccre.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.usumod.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.idpre == null ? null : "'" + obj.idpre + "'");
				arrValoresParam.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValoresParam.Add(obj.idcpl);
				arrValoresParam.Add(obj.fechapre == null ? null : "'" + Convert.ToDateTime(obj.fechapre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.rtfpre);
				arrValoresParam.Add(obj.textopre == null ? null : "'" + obj.textopre + "'");
				arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.htmlpre == null ? null : "'" + obj.htmlpre + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla pacrecetas
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntPacRecetas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntPacRecetas.Fields.idpre.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.idppa.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.textopre.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.usucre.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.feccre.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.usumod.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrNombreParam.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.idpre == null ? null : "'" + obj.idpre + "'");
				arrValoresParam.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValoresParam.Add(obj.idcpl);
				arrValoresParam.Add(obj.fechapre == null ? null : "'" + Convert.ToDateTime(obj.fechapre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.rtfpre);
				arrValoresParam.Add(obj.textopre == null ? null : "'" + obj.textopre + "'");
				arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.htmlpre == null ? null : "'" + obj.htmlpre + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region FuncionesAgregadas

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntPacRecetas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntPacRecetas.Fields refField, EntPacRecetas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntPacRecetas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0].ToString() == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntPacRecetas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntPacRecetas.Fields refField, EntPacRecetas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntPacRecetas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0].ToString() == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntPacRecetas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntPacRecetas.Fields refField, EntPacRecetas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntPacRecetas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0].ToString() == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntPacRecetas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntPacRecetas.Fields refField, EntPacRecetas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntPacRecetas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0].ToString() == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntPacRecetas.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntPacRecetas.Fields refField, EntPacRecetas.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacRecetas que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntPacRecetas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0].ToString() == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs SP

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacrecetas
		/// </returns>
		public bool Insert(EntPacRecetas obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("idpre");
				arrValoresParam.Add(null);
				if (obj.idppa == null)
					throw new Exception("El Parametro idppa no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				if (obj.idcpl == null)
					throw new Exception("El Parametro idcpl no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrValoresParam.Add(obj.idcpl);
				
				if (obj.fechapre == null)
					throw new Exception("El Parametro fechapre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrValoresParam.Add(obj.fechapre);
				
				if (obj.rtfpre == null)
					throw new Exception("El Parametro rtfpre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrValoresParam.Add(obj.rtfpre);
				
				if (obj.textopre == null)
					throw new Exception("El Parametro textopre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.textopre.ToString());
				arrValoresParam.Add(obj.textopre);
				
				if (obj.usucre == null)
					throw new Exception("El Parametro usucre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.usucre.ToString());
				arrValoresParam.Add(obj.usucre);
				
				if (obj.htmlpre == null)
					throw new Exception("El Parametro htmlpre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.htmlpre.ToString());
				arrValoresParam.Add(obj.htmlpre);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacRecetas.SpPreIns.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacrecetas
		/// </returns>
		public bool Insert(EntPacRecetas obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("idpre");
				arrValoresParam.Add("");
				if (obj.idppa == null)
					throw new Exception("El Parametro idppa no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				if (obj.idcpl == null)
					throw new Exception("El Parametro idcpl no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrValoresParam.Add(obj.idcpl);
				
				if (obj.fechapre == null)
					throw new Exception("El Parametro fechapre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrValoresParam.Add(obj.fechapre);
				
				if (obj.rtfpre == null)
					throw new Exception("El Parametro rtfpre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrValoresParam.Add(obj.rtfpre);
				
				if (obj.textopre == null)
					throw new Exception("El Parametro textopre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.textopre.ToString());
				arrValoresParam.Add(obj.textopre);
				
				if (obj.usucre == null)
					throw new Exception("El Parametro usucre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.usucre.ToString());
				arrValoresParam.Add(obj.usucre);
				
				if (obj.htmlpre == null)
					throw new Exception("El Parametro htmlpre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.htmlpre.ToString());
				arrValoresParam.Add(obj.htmlpre);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacRecetas.SpPreIns.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor que indica la cantidad de registros actualizados en pacrecetas
		/// </returns>
		public int Update(EntPacRecetas obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				if (obj.idpre == null)
					throw new Exception("El Parametro idpre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.idpre.ToString());
				arrValoresParam.Add(obj.idpre);
				
				if (obj.idppa == null)
					throw new Exception("El Parametro idppa no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				if (obj.idcpl == null)
					throw new Exception("El Parametro idcpl no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrValoresParam.Add(obj.idcpl);
				
				if (obj.fechapre == null)
					throw new Exception("El Parametro fechapre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrValoresParam.Add(obj.fechapre);
				
				if (obj.rtfpre == null)
					throw new Exception("El Parametro rtfpre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrValoresParam.Add(obj.rtfpre);
				
				if (obj.textopre == null)
					throw new Exception("El Parametro textopre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.textopre.ToString());
				arrValoresParam.Add(obj.textopre);
				
				if (obj.apitransaccion == null)
					throw new Exception("El Parametro apitransaccion no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrValoresParam.Add(obj.apitransaccion);
				
				if (obj.usumod == null)
					throw new Exception("El Parametro usumod no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.usumod.ToString());
				arrValoresParam.Add(obj.usumod);
				
				if (obj.htmlpre == null)
					throw new Exception("El Parametro htmlpre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.htmlpre.ToString());
				arrValoresParam.Add(obj.htmlpre);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacRecetas.SpPreUpd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacrecetas
		/// </returns>
		public int Update(EntPacRecetas obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				if (obj.idpre == null)
					throw new Exception("El Parametro idpre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.idpre.ToString());
				arrValoresParam.Add(obj.idpre);
				
				if (obj.idppa == null)
					throw new Exception("El Parametro idppa no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				if (obj.idcpl == null)
					throw new Exception("El Parametro idcpl no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrValoresParam.Add(obj.idcpl);
				
				if (obj.fechapre == null)
					throw new Exception("El Parametro fechapre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrValoresParam.Add(obj.fechapre);
				
				if (obj.rtfpre == null)
					throw new Exception("El Parametro rtfpre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrValoresParam.Add(obj.rtfpre);
				
				if (obj.textopre == null)
					throw new Exception("El Parametro textopre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.textopre.ToString());
				arrValoresParam.Add(obj.textopre);
				
				if (obj.apitransaccion == null)
					throw new Exception("El Parametro apitransaccion no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrValoresParam.Add(obj.apitransaccion);
				
				if (obj.usumod == null)
					throw new Exception("El Parametro usumod no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.usumod.ToString());
				arrValoresParam.Add(obj.usumod);
				
				if (obj.htmlpre == null)
					throw new Exception("El Parametro htmlpre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.htmlpre.ToString());
				arrValoresParam.Add(obj.htmlpre);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacRecetas.SpPreUpd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacrecetas
		/// </returns>
		public int Delete(EntPacRecetas obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				if (obj.idpre == null)
					throw new Exception("El Parametro idpre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.idpre.ToString());
				arrValoresParam.Add(obj.idpre);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacRecetas.SpPreDel.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacrecetas
		/// </returns>
		public int Delete(EntPacRecetas obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				if (obj.idpre == null)
					throw new Exception("El Parametro idpre no puede ser NULO.");
				arrNombreParam.Add(EntPacRecetas.Fields.idpre.ToString());
				arrValoresParam.Add(obj.idpre);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacRecetas.SpPreDel.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacrecetas
		/// </returns>
		public int InsertUpdate(EntPacRecetas obj)
		{
			try
			{
				bool esInsertar = true;
				
					esInsertar = (esInsertar && (obj.idpre == null));
				
				if (esInsertar)
					return Insert(obj) ? 1 : 0;
				else
					return Update(obj);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacrecetas
		/// </returns>
		public int InsertUpdate(EntPacRecetas obj, ref CTrans localTrans)
		{
			try
			{
				bool esInsertar = false;
				
					esInsertar = (esInsertar && (obj.idpre == null));
				
				if (esInsertar)
					return Insert(obj, ref localTrans) ? 1 : 0;
				else
					return Update(obj, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs Query

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacrecetas
		/// </returns>
		public bool InsertQuery(EntPacRecetas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idpre == null ? null : "'" + obj.idpre + "'");
				arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.idcpl);
				arrValores.Add(obj.fechapre == null ? null : "'" + Convert.ToDateTime(obj.fechapre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.rtfpre);
				arrValores.Add(obj.textopre == null ? null : "'" + obj.textopre + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.htmlpre == null ? null : "'" + obj.htmlpre + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntPacRecetas.StrNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacrecetas
		/// </returns>
		public bool InsertQuery(EntPacRecetas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idpre == null ? null : "'" + obj.idpre + "'");
				arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.idcpl);
				arrValores.Add(obj.fechapre == null ? null : "'" + Convert.ToDateTime(obj.fechapre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.rtfpre);
				arrValores.Add(obj.textopre == null ? null : "'" + obj.textopre + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.htmlpre == null ? null : "'" + obj.htmlpre + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntPacRecetas.StrNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacrecetas
		/// </returns>
		public bool InsertQueryIdentity(EntPacRecetas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idpre == null ? null : "'" + obj.idpre + "'");
				arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.idcpl);
				arrValores.Add(obj.fechapre == null ? null : "'" + Convert.ToDateTime(obj.fechapre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.rtfpre);
				arrValores.Add(obj.textopre == null ? null : "'" + obj.textopre + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.htmlpre == null ? null : "'" + obj.htmlpre + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntPacRecetas.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.idpre = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacrecetas
		/// </returns>
		public bool InsertQueryIdentity(EntPacRecetas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idpre == null ? null : "'" + obj.idpre + "'");
				arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.idcpl);
				arrValores.Add(obj.fechapre == null ? null : "'" + Convert.ToDateTime(obj.fechapre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.rtfpre);
				arrValores.Add(obj.textopre == null ? null : "'" + obj.textopre + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValores.Add(obj.htmlpre == null ? null : "'" + obj.htmlpre + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntPacRecetas.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.idpre = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpacrecetas
		/// </returns>
		public int UpdateQueryAll(EntPacRecetas obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.idcpl);
				arrValores.Add(obj.fechapre == null ? null : "'" + Convert.ToDateTime(obj.fechapre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.rtfpre);
				arrValores.Add(obj.textopre == null ? null : "'" + obj.textopre + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValores.Add(obj.htmlpre == null ? null : "'" + obj.htmlpre + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacRecetas.Fields.idpre.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idpre == null ? null : "'" + obj.idpre + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacRecetas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pacrecetas a partir de una clase del tipo epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQueryAll(EntPacRecetas obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.idcpl);
				arrValores.Add(obj.fechapre == null ? null : "'" + Convert.ToDateTime(obj.fechapre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.rtfpre);
				arrValores.Add(obj.textopre == null ? null : "'" + obj.textopre + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValores.Add(obj.htmlpre == null ? null : "'" + obj.htmlpre + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacRecetas.Fields.idpre.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idpre == null ? null : "'" + obj.idpre + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacRecetas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pacrecetas a partir de una clase del tipo Epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpacrecetas
		/// </returns>
		public int UpdateQuery(EntPacRecetas obj)
		{
			try
			{
				//Obtenemos el Objeto original
				EntPacRecetas objOriginal = this.ObtenerObjeto(obj.idpre);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.idppa != objOriginal.idppa )
				{
					arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
					arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				}
				if(obj.idcpl != objOriginal.idcpl )
				{
					arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
					arrValores.Add(obj.idcpl);
				}
				if(obj.fechapre != objOriginal.fechapre )
				{
					arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
					arrValores.Add(obj.fechapre == null ? null : "'" + Convert.ToDateTime(obj.fechapre).ToString(CParametros.ParFormatoFechaHora) + "'");
				}
				if(obj.rtfpre != objOriginal.rtfpre )
				{
					arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
					arrValores.Add(obj.rtfpre);
				}
				if(obj.textopre != objOriginal.textopre )
				{
					arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
					arrValores.Add(obj.textopre == null ? null : "'" + obj.textopre + "'");
				}
				if(obj.apiestado != objOriginal.apiestado )
				{
					arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
					arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				}
				if(obj.apitransaccion != objOriginal.apitransaccion )
				{
					arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
					arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				}

				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");

				if(obj.htmlpre != objOriginal.htmlpre )
				{
					arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
					arrValores.Add(obj.htmlpre == null ? null : "'" + obj.htmlpre + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacRecetas.Fields.idpre.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idpre == null ? null : "'" + obj.idpre + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacRecetas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pacrecetas a partir de una clase del tipo epacrecetas
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQuery(EntPacRecetas obj, ref CTrans localTrans)
		{
			try
			{
				//Obtenemos el Objeto original
				EntPacRecetas objOriginal = this.ObtenerObjeto(obj.idpre, ref localTrans);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.idppa != objOriginal.idppa )
				{
					arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
					arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				}
				if(obj.idcpl != objOriginal.idcpl )
				{
					arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
					arrValores.Add(obj.idcpl);
				}
				if(obj.fechapre != objOriginal.fechapre )
				{
					arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
					arrValores.Add(obj.fechapre == null ? null : "'" + Convert.ToDateTime(obj.fechapre).ToString(CParametros.ParFormatoFechaHora) + "'");
				}
				if(obj.rtfpre != objOriginal.rtfpre )
				{
					arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
					arrValores.Add(obj.rtfpre);
				}
				if(obj.textopre != objOriginal.textopre )
				{
					arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
					arrValores.Add(obj.textopre == null ? null : "'" + obj.textopre + "'");
				}
				if(obj.apiestado != objOriginal.apiestado )
				{
					arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
					arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				}
				if(obj.apitransaccion != objOriginal.apitransaccion )
				{
					arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
					arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				}

				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");

				if(obj.htmlpre != objOriginal.htmlpre )
				{
					arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
					arrValores.Add(obj.htmlpre == null ? null : "'" + obj.htmlpre + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacRecetas.Fields.idpre.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idpre == null ? null : "'" + obj.idpre + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacRecetas.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pacrecetas a partir de una clase del tipo EntPacRecetas y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacRecetas">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpacrecetas
		/// </returns>
		public int DeleteQuery(EntPacRecetas obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacRecetas.Fields.idpre.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idpre == null ? null : "'" + obj.idpre + "'");

			
				CConn local = new CConn();
				return local.DeleteBd(EntPacRecetas.StrNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pacrecetas a partir de una clase del tipo EntPacRecetas y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.epacrecetas">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionpacrecetas
		/// </returns>
		public int DeleteQuery(EntPacRecetas obj, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacRecetas.Fields.idpre.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idpre == null ? null : "'" + obj.idpre + "'");

			
				CConn local = new CConn();
				return local.DeleteBd(EntPacRecetas.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pacrecetas a partir de una clase del tipo epacrecetas
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpacrecetas
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd("pacrecetas", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pacrecetas a partir de una clase del tipo epacrecetas
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacrecetas
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionpacrecetas
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd(EntPacRecetas.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region Llenado de elementos

		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacRecetas.Fields valueField, String textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacRecetas.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField, EntPacRecetas.Fields searchField, object searchValue)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacRecetas.Fields valueField, String textField, EntPacRecetas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField, EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacRecetas.Fields valueField, String textField, EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacRecetas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacRecetas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.DataValueField = table.Columns[0].ColumnName;
					cmb.DataTextField = table.Columns[1].ColumnName;
					cmb.DataSource = table;
					cmb.DataBind();

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntPacRecetas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntPacRecetas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacRecetas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacRecetas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridViewOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField, EntPacRecetas.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, String textField, EntPacRecetas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField, EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, String textField, EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacrecetas
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacrecetas
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntPacRecetas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntPacRecetas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacRecetas.Fields.idpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idppa.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.idcpl.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fechapre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.rtfpre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.textopre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usucre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.feccre.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.usumod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.fecmod.ToString());
				arrColumnas.Add(EntPacRecetas.Fields.htmlpre.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacRecetas.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacRecetas.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacRecetas.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacrecetas
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntPacRecetas.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 


		#endregion

		#region Funciones Internas

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto pacrecetas
		/// </returns>
		internal EntPacRecetas crearObjeto(DataRow row)
		{
			var obj = new EntPacRecetas();
			obj.idpre = GetColumnType(row[EntPacRecetas.Fields.idpre.ToString()], EntPacRecetas.Fields.idpre);
			obj.idppa = GetColumnType(row[EntPacRecetas.Fields.idppa.ToString()], EntPacRecetas.Fields.idppa);
			obj.idcpl = GetColumnType(row[EntPacRecetas.Fields.idcpl.ToString()], EntPacRecetas.Fields.idcpl);
			obj.fechapre = GetColumnType(row[EntPacRecetas.Fields.fechapre.ToString()], EntPacRecetas.Fields.fechapre);
			obj.rtfpre = GetColumnType(row[EntPacRecetas.Fields.rtfpre.ToString()], EntPacRecetas.Fields.rtfpre);
			obj.textopre = GetColumnType(row[EntPacRecetas.Fields.textopre.ToString()], EntPacRecetas.Fields.textopre);
			obj.apiestado = GetColumnType(row[EntPacRecetas.Fields.apiestado.ToString()], EntPacRecetas.Fields.apiestado);
			obj.apitransaccion = GetColumnType(row[EntPacRecetas.Fields.apitransaccion.ToString()], EntPacRecetas.Fields.apitransaccion);
			obj.usucre = GetColumnType(row[EntPacRecetas.Fields.usucre.ToString()], EntPacRecetas.Fields.usucre);
			obj.feccre = GetColumnType(row[EntPacRecetas.Fields.feccre.ToString()], EntPacRecetas.Fields.feccre);
			obj.usumod = GetColumnType(row[EntPacRecetas.Fields.usumod.ToString()], EntPacRecetas.Fields.usumod);
			obj.fecmod = GetColumnType(row[EntPacRecetas.Fields.fecmod.ToString()], EntPacRecetas.Fields.fecmod);
			obj.htmlpre = GetColumnType(row[EntPacRecetas.Fields.htmlpre.ToString()], EntPacRecetas.Fields.htmlpre);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto pacrecetas
		/// </returns>
		internal EntPacRecetas crearObjetoRevisado(DataRow row)
		{
			var obj = new EntPacRecetas();
			if (row.Table.Columns.Contains(EntPacRecetas.Fields.idpre.ToString()))
				obj.idpre = GetColumnType(row[EntPacRecetas.Fields.idpre.ToString()], EntPacRecetas.Fields.idpre);
			if (row.Table.Columns.Contains(EntPacRecetas.Fields.idppa.ToString()))
				obj.idppa = GetColumnType(row[EntPacRecetas.Fields.idppa.ToString()], EntPacRecetas.Fields.idppa);
			if (row.Table.Columns.Contains(EntPacRecetas.Fields.idcpl.ToString()))
				obj.idcpl = GetColumnType(row[EntPacRecetas.Fields.idcpl.ToString()], EntPacRecetas.Fields.idcpl);
			if (row.Table.Columns.Contains(EntPacRecetas.Fields.fechapre.ToString()))
				obj.fechapre = GetColumnType(row[EntPacRecetas.Fields.fechapre.ToString()], EntPacRecetas.Fields.fechapre);
			if (row.Table.Columns.Contains(EntPacRecetas.Fields.rtfpre.ToString()))
				obj.rtfpre = GetColumnType(row[EntPacRecetas.Fields.rtfpre.ToString()], EntPacRecetas.Fields.rtfpre);
			if (row.Table.Columns.Contains(EntPacRecetas.Fields.textopre.ToString()))
				obj.textopre = GetColumnType(row[EntPacRecetas.Fields.textopre.ToString()], EntPacRecetas.Fields.textopre);
			if (row.Table.Columns.Contains(EntPacRecetas.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[EntPacRecetas.Fields.apiestado.ToString()], EntPacRecetas.Fields.apiestado);
			if (row.Table.Columns.Contains(EntPacRecetas.Fields.apitransaccion.ToString()))
				obj.apitransaccion = GetColumnType(row[EntPacRecetas.Fields.apitransaccion.ToString()], EntPacRecetas.Fields.apitransaccion);
			if (row.Table.Columns.Contains(EntPacRecetas.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[EntPacRecetas.Fields.usucre.ToString()], EntPacRecetas.Fields.usucre);
			if (row.Table.Columns.Contains(EntPacRecetas.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[EntPacRecetas.Fields.feccre.ToString()], EntPacRecetas.Fields.feccre);
			if (row.Table.Columns.Contains(EntPacRecetas.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[EntPacRecetas.Fields.usumod.ToString()], EntPacRecetas.Fields.usumod);
			if (row.Table.Columns.Contains(EntPacRecetas.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[EntPacRecetas.Fields.fecmod.ToString()], EntPacRecetas.Fields.fecmod);
			if (row.Table.Columns.Contains(EntPacRecetas.Fields.htmlpre.ToString()))
				obj.htmlpre = GetColumnType(row[EntPacRecetas.Fields.htmlpre.ToString()], EntPacRecetas.Fields.htmlpre);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtpacrecetas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos pacrecetas
		/// </returns>
		internal List<EntPacRecetas> crearLista(DataTable dtpacrecetas)
		{
			var list = new List<EntPacRecetas>();
			
			foreach (DataRow row in dtpacrecetas.Rows)
			{
				var obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtpacrecetas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos pacrecetas
		/// </returns>
		internal List<EntPacRecetas> crearListaRevisada(DataTable dtpacrecetas)
		{
			List<EntPacRecetas> list = new List<EntPacRecetas>();
			
			foreach (DataRow row in dtpacrecetas.Rows)
			{
				var obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtpacrecetas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos pacrecetas
		/// </returns>
		internal Queue<EntPacRecetas> crearCola(DataTable dtpacrecetas)
		{
			Queue<EntPacRecetas> cola = new Queue<EntPacRecetas>();
			
			EntPacRecetas obj = new EntPacRecetas();
			foreach (DataRow row in dtpacrecetas.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtpacrecetas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos pacrecetas
		/// </returns>
		internal Stack<EntPacRecetas> crearPila(DataTable dtpacrecetas)
		{
			Stack<EntPacRecetas> pila = new Stack<EntPacRecetas>();
			
			foreach (DataRow row in dtpacrecetas.Rows)
			{
				var obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtpacrecetas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos pacrecetas
		/// </returns>
		internal Dictionary<String, EntPacRecetas> crearDiccionario(DataTable dtpacrecetas)
		{
			Dictionary<String, EntPacRecetas>  miDic = new Dictionary<String, EntPacRecetas>();
			
			foreach (DataRow row in dtpacrecetas.Rows)
			{
				var obj = crearObjeto(row);
				miDic.Add(obj.idpre.ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtpacrecetas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 HashTable de Objetos pacrecetas
		/// </returns>
		internal Hashtable crearHashTable(DataTable dtpacrecetas)
		{
			Hashtable miTabla = new Hashtable();
			
			foreach (DataRow row in dtpacrecetas.Rows)
			{
				var obj = crearObjeto(row);
				miTabla.Add(obj.idpre.ToString(), obj);
			}
			return miTabla;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtpacrecetas" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos pacrecetas
		/// </returns>
		internal Dictionary<String, EntPacRecetas> crearDiccionarioRevisado(DataTable dtpacrecetas)
		{
			Dictionary<String, EntPacRecetas>  miDic = new Dictionary<String, EntPacRecetas>();
			
			foreach (DataRow row in dtpacrecetas.Rows)
			{
				var obj = crearObjetoRevisado(row);
				miDic.Add(obj.idpre.ToString(), obj);
			}
			return miDic;
		}
		
		internal Dictionary<String, EntPacRecetas> crearDiccionario(DataTable dtpacrecetas, EntPacRecetas.Fields dicKey)
		{
			Dictionary<String, EntPacRecetas>  miDic = new Dictionary<String, EntPacRecetas>();
			
			foreach (DataRow row in dtpacrecetas.Rows)
			{
				var obj = crearObjeto(row);
				
				var nameOfProperty = dicKey.ToString();
				var propertyInfo = obj.GetType().GetProperty(nameOfProperty);
				var value = propertyInfo.GetValue(obj, null);
				
				miDic.Add(value.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
		#endregion

	}
}

