#region 
/***********************************************************************************************************
	NOMBRE:       RnPacPacientes
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla pacpacientes

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/04/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Interface;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Modelo
{
	public class RnPacPacientes: IPacPacientes
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region IPacPacientes Members

		#region Reflection

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string GetTableScript()
		{
			TableClass tabla = new TableClass(typeof(EntPacPacientes));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, EntPacPacientes.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntPacPacientes).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="strField">Nombre de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, string strField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntPacPacientes).GetProperty(strField).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

/// <summary>
/// Inserta una valor a una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">Es el nombre de la propiedad</param>
/// <param name="dynValor">Es el valor que se insertara a la propiedad</param>
public void SetDato(ref EntPacPacientes obj, string strPropiedad, dynamic dynValor)
{
	if (obj == null) throw new ArgumentNullException();
	obj.GetType().GetProperty(strPropiedad).SetValue(obj, GetColumnType(dynValor, strPropiedad), null);
}

/// <summary>
/// Obtiene el valor de una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">El nombre de la propiedad de la que se obtendra el valor</param>
/// <returns>Devuelve el valor del a propiedad seleccionada</returns>
public dynamic GetDato(ref EntPacPacientes obj, string strPropiedad)
{
	if (obj == null) return null;
	var propertyInfo = obj.GetType().GetProperty(strPropiedad);
	return GetColumnType(propertyInfo.GetValue(obj, null), strPropiedad);
}

		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla pacpacientes a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla pacpacientes
		/// </returns>
		public string CreatePk(string[] args)
		{
			return args[0];
		}
		
		#endregion 

		#region ObtenerObjeto

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacPacientes a partir de la llave primaria
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public EntPacPacientes ObtenerObjeto(Int64 Int64idppa)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntPacPacientes.Fields.idppa.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64idppa + "'");
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacPacientes a partir del usuario que inserta
		/// </summary>
		/// <param name="strUsuCre">Login o nombre de usuario</param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public EntPacPacientes ObtenerObjetoInsertado(string strUsuCre)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntPacPacientes.Fields.usucre.ToString());
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + strUsuCre + "'");
			
			int iIdInsertado = FuncionesMax(EntPacPacientes.Fields.idppa, arrColumnasWhere, arrValoresWhere);
			
			return ObtenerObjeto(iIdInsertado);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public EntPacPacientes ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public EntPacPacientes ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntPacPacientes obj = new EntPacPacientes();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public EntPacPacientes ObtenerObjeto(Hashtable htbFiltro)
		{
			return ObtenerObjeto(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public EntPacPacientes ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntPacPacientes obj = new EntPacPacientes();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public EntPacPacientes ObtenerObjeto(EntPacPacientes.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public EntPacPacientes ObtenerObjeto(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo EntPacPacientes a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo EntPacPacientes
		/// </returns>
		public EntPacPacientes ObtenerObjeto(Int64 Int64idppa, ref CTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntPacPacientes.Fields.idppa.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64idppa + "'");
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public EntPacPacientes ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public EntPacPacientes ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerObjeto(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public EntPacPacientes ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public EntPacPacientes ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					EntPacPacientes obj = new EntPacPacientes();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public EntPacPacientes ObtenerObjeto(EntPacPacientes.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public EntPacPacientes ObtenerObjeto(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion

		#region ObtenerLista

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacPacientes>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerLista(EntPacPacientes.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerLista(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacPacientes>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerLista(EntPacPacientes.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerLista(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerLista(Hashtable htbFiltro)
		{
			return ObtenerLista(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacPacientes>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerLista(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacPacientes>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacPacientes>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacPacientes>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, EntPacPacientes.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacPacientes>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacPacientes>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, EntPacPacientes.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerCola y Obtener Pila

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacPacientes> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacPacientes> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacPacientes> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntPacPacientes>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacPacientes> ObtenerCola(EntPacPacientes.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacPacientes> ObtenerCola(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacPacientes> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacPacientes> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntPacPacientes>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacPacientes> ObtenerCola(EntPacPacientes.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacPacientes> ObtenerCola(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacPacientes> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacPacientes> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacPacientes> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntPacPacientes>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacPacientes> ObtenerPila(EntPacPacientes.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacPacientes> ObtenerPila(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacPacientes> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacPacientes> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntPacPacientes>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacPacientes> ObtenerPila(EntPacPacientes.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacPacientes> ObtenerPila(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerDataTable

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla pacpacientes
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(EntPacPacientes.Fields.idppa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.idppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.nombresppa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.nombresppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.appaternoppa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.appaternoppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.apmaternoppa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.apmaternoppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.generoppa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.generoppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.fechanac.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.fechanac.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.idcts.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.idcts.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.idcec.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.idcec.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.ocupacionppa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.ocupacionppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.procedenciappa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.procedenciappa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.domicilioppa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.domicilioppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.dominanciappa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.dominanciappa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.telefonosppa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.telefonosppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.emailppa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.emailppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.religionppa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.religionppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.empresappa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.empresappa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.seguroppa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.seguroppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.remiteppa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.remiteppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.informanteppa.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.informanteppa.ToString()).PropertyType);
				table.Columns.Add(dc);

                dc = new DataColumn(EntPacPacientes.Fields.cippa.ToString(), typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.cippa.ToString()).PropertyType);
                table.Columns.Add(dc);

                dc = new DataColumn(EntPacPacientes.Fields.apiestado.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.apitransaccion.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.apitransaccion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.usucre.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.feccre.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.usumod.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacPacientes.Fields.fecmod.ToString(),typeof(EntPacPacientes).GetProperty(EntPacPacientes.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una pacpacientes
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla pacpacientes
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable ObtenerDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE pacpacientes
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable ObtenerDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pacpacientes
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableOr(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntPacPacientes.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntPacPacientes.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacPacientes.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacPacientes.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region ObtenerDiccionario

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacPacientes> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacPacientes> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacPacientes> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntPacPacientes>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacPacientes> ObtenerDiccionario(EntPacPacientes.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacPacientes> ObtenerDiccionario(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacPacientes> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacPacientes> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntPacPacientes>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacPacientes> ObtenerDiccionario(EntPacPacientes.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacPacientes a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacPacientes> ObtenerDiccionario(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		public Dictionary<String, EntPacPacientes> ObtenerDiccionarioKey(EntPacPacientes.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntPacPacientes> ObtenerDiccionarioKey(String strParamAdic, EntPacPacientes.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdic, dicKey);
		}
		
		public Dictionary<String, EntPacPacientes> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, EntPacPacientes.Fields dicKey)
		{
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, "", dicKey);
		}
		
		public Dictionary<String, EntPacPacientes> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, EntPacPacientes.Fields dicKey)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table, dicKey);
				}
				else
					return new Dictionary<string, EntPacPacientes>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		public Dictionary<String, EntPacPacientes> ObtenerDiccionarioKey(EntPacPacientes.Fields searchField, object searchValue, EntPacPacientes.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntPacPacientes> ObtenerDiccionarioKey(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales, EntPacPacientes.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey);
		}
		

		#endregion 

		#region ObjetoASp

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla pacpacientes
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntPacPacientes obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntPacPacientes.Fields.idppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.idcts.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.idcec.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.cippa.ToString());
                arrNombreParam.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.usucre.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.feccre.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.usumod.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValoresParam.Add(obj.nombresppa == null ? null : "'" + obj.nombresppa + "'");
				arrValoresParam.Add(obj.appaternoppa == null ? null : "'" + obj.appaternoppa + "'");
				arrValoresParam.Add(obj.apmaternoppa == null ? null : "'" + obj.apmaternoppa + "'");
				arrValoresParam.Add(obj.generoppa);
				arrValoresParam.Add(obj.fechanac == null ? null : "'" + Convert.ToDateTime(obj.fechanac).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.idcts);
				arrValoresParam.Add(obj.idcec);
				arrValoresParam.Add(obj.ocupacionppa == null ? null : "'" + obj.ocupacionppa + "'");
				arrValoresParam.Add(obj.procedenciappa == null ? null : "'" + obj.procedenciappa + "'");
				arrValoresParam.Add(obj.domicilioppa == null ? null : "'" + obj.domicilioppa + "'");
				arrValoresParam.Add(obj.dominanciappa == null ? null : "'" + obj.dominanciappa + "'");
				arrValoresParam.Add(obj.telefonosppa == null ? null : "'" + obj.telefonosppa + "'");
				arrValoresParam.Add(obj.emailppa == null ? null : "'" + obj.emailppa + "'");
				arrValoresParam.Add(obj.religionppa == null ? null : "'" + obj.religionppa + "'");
				arrValoresParam.Add(obj.empresappa == null ? null : "'" + obj.empresappa + "'");
				arrValoresParam.Add(obj.seguroppa == null ? null : "'" + obj.seguroppa + "'");
				arrValoresParam.Add(obj.remiteppa == null ? null : "'" + obj.remiteppa + "'");
				arrValoresParam.Add(obj.informanteppa == null ? null : "'" + obj.informanteppa + "'");
				arrValoresParam.Add(obj.cippa == null ? null : "'" + obj.cippa + "'");
                arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla pacpacientes
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntPacPacientes obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntPacPacientes.Fields.idppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.idcts.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.idcec.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.cippa.ToString());
                arrNombreParam.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.usucre.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.feccre.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.usumod.ToString());
				arrNombreParam.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValoresParam.Add(obj.nombresppa == null ? null : "'" + obj.nombresppa + "'");
				arrValoresParam.Add(obj.appaternoppa == null ? null : "'" + obj.appaternoppa + "'");
				arrValoresParam.Add(obj.apmaternoppa == null ? null : "'" + obj.apmaternoppa + "'");
				arrValoresParam.Add(obj.generoppa);
				arrValoresParam.Add(obj.fechanac == null ? null : "'" + Convert.ToDateTime(obj.fechanac).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.idcts);
				arrValoresParam.Add(obj.idcec);
				arrValoresParam.Add(obj.ocupacionppa == null ? null : "'" + obj.ocupacionppa + "'");
				arrValoresParam.Add(obj.procedenciappa == null ? null : "'" + obj.procedenciappa + "'");
				arrValoresParam.Add(obj.domicilioppa == null ? null : "'" + obj.domicilioppa + "'");
				arrValoresParam.Add(obj.dominanciappa == null ? null : "'" + obj.dominanciappa + "'");
				arrValoresParam.Add(obj.telefonosppa == null ? null : "'" + obj.telefonosppa + "'");
				arrValoresParam.Add(obj.emailppa == null ? null : "'" + obj.emailppa + "'");
				arrValoresParam.Add(obj.religionppa == null ? null : "'" + obj.religionppa + "'");
				arrValoresParam.Add(obj.empresappa == null ? null : "'" + obj.empresappa + "'");
				arrValoresParam.Add(obj.seguroppa == null ? null : "'" + obj.seguroppa + "'");
				arrValoresParam.Add(obj.remiteppa == null ? null : "'" + obj.remiteppa + "'");
				arrValoresParam.Add(obj.informanteppa == null ? null : "'" + obj.informanteppa + "'");
				arrValoresParam.Add(obj.cippa == null ? null : "'" + obj.cippa + "'");
				arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region FuncionesAgregadas

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntPacPacientes.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntPacPacientes.Fields refField, EntPacPacientes.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntPacPacientes.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntPacPacientes.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntPacPacientes.Fields refField, EntPacPacientes.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntPacPacientes.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntPacPacientes.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntPacPacientes.Fields refField, EntPacPacientes.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntPacPacientes.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntPacPacientes.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntPacPacientes.Fields refField, EntPacPacientes.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntPacPacientes.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntPacPacientes.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntPacPacientes.Fields refField, EntPacPacientes.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacPacientes que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntPacPacientes.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs SP

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacpacientes
		/// </returns>
		public bool Insert(EntPacPacientes obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("idppa");
				arrValoresParam.Add(null);
				arrNombreParam.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrValoresParam.Add(obj.nombresppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrValoresParam.Add(obj.appaternoppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrValoresParam.Add(obj.apmaternoppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrValoresParam.Add(obj.generoppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrValoresParam.Add(obj.fechanac);
				
				arrNombreParam.Add(EntPacPacientes.Fields.idcts.ToString());
				arrValoresParam.Add(obj.idcts);
				
				arrNombreParam.Add(EntPacPacientes.Fields.idcec.ToString());
				arrValoresParam.Add(obj.idcec);
				
				arrNombreParam.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrValoresParam.Add(obj.ocupacionppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrValoresParam.Add(obj.procedenciappa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrValoresParam.Add(obj.domicilioppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrValoresParam.Add(obj.dominanciappa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrValoresParam.Add(obj.telefonosppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrValoresParam.Add(obj.emailppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrValoresParam.Add(obj.religionppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrValoresParam.Add(obj.empresappa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrValoresParam.Add(obj.seguroppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrValoresParam.Add(obj.remiteppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrValoresParam.Add(obj.informanteppa);

                arrNombreParam.Add(EntPacPacientes.Fields.cippa.ToString());
                arrValoresParam.Add(obj.cippa);

                arrNombreParam.Add(EntPacPacientes.Fields.usucre.ToString());
				arrValoresParam.Add(obj.usucre);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacPacientes.SpPpaIns.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacpacientes
		/// </returns>
		public bool Insert(EntPacPacientes obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("idppa");
				arrValoresParam.Add("");
				arrNombreParam.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrValoresParam.Add(obj.nombresppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrValoresParam.Add(obj.appaternoppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrValoresParam.Add(obj.apmaternoppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrValoresParam.Add(obj.generoppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrValoresParam.Add(obj.fechanac);
				
				arrNombreParam.Add(EntPacPacientes.Fields.idcts.ToString());
				arrValoresParam.Add(obj.idcts);
				
				arrNombreParam.Add(EntPacPacientes.Fields.idcec.ToString());
				arrValoresParam.Add(obj.idcec);
				
				arrNombreParam.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrValoresParam.Add(obj.ocupacionppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrValoresParam.Add(obj.procedenciappa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrValoresParam.Add(obj.domicilioppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrValoresParam.Add(obj.dominanciappa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrValoresParam.Add(obj.telefonosppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrValoresParam.Add(obj.emailppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrValoresParam.Add(obj.religionppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrValoresParam.Add(obj.empresappa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrValoresParam.Add(obj.seguroppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrValoresParam.Add(obj.remiteppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrValoresParam.Add(obj.informanteppa);

                arrNombreParam.Add(EntPacPacientes.Fields.cippa.ToString());
                arrValoresParam.Add(obj.cippa);

                arrNombreParam.Add(EntPacPacientes.Fields.usucre.ToString());
				arrValoresParam.Add(obj.usucre);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacPacientes.SpPpaIns.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor que indica la cantidad de registros actualizados en pacpacientes
		/// </returns>
		public int Update(EntPacPacientes obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntPacPacientes.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrValoresParam.Add(obj.nombresppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrValoresParam.Add(obj.appaternoppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrValoresParam.Add(obj.apmaternoppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrValoresParam.Add(obj.generoppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrValoresParam.Add(obj.fechanac);
				
				arrNombreParam.Add(EntPacPacientes.Fields.idcts.ToString());
				arrValoresParam.Add(obj.idcts);
				
				arrNombreParam.Add(EntPacPacientes.Fields.idcec.ToString());
				arrValoresParam.Add(obj.idcec);
				
				arrNombreParam.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrValoresParam.Add(obj.ocupacionppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrValoresParam.Add(obj.procedenciappa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrValoresParam.Add(obj.domicilioppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrValoresParam.Add(obj.dominanciappa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrValoresParam.Add(obj.telefonosppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrValoresParam.Add(obj.emailppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrValoresParam.Add(obj.religionppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrValoresParam.Add(obj.empresappa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrValoresParam.Add(obj.seguroppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrValoresParam.Add(obj.remiteppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrValoresParam.Add(obj.informanteppa);

                arrNombreParam.Add(EntPacPacientes.Fields.cippa.ToString());
                arrValoresParam.Add(obj.cippa);

                arrNombreParam.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrValoresParam.Add(obj.apitransaccion);
				
				arrNombreParam.Add(EntPacPacientes.Fields.usumod.ToString());
				arrValoresParam.Add(obj.usumod);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacPacientes.SpPpaUpd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacpacientes
		/// </returns>
		public int Update(EntPacPacientes obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntPacPacientes.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrValoresParam.Add(obj.nombresppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrValoresParam.Add(obj.appaternoppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrValoresParam.Add(obj.apmaternoppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrValoresParam.Add(obj.generoppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrValoresParam.Add(obj.fechanac);
				
				arrNombreParam.Add(EntPacPacientes.Fields.idcts.ToString());
				arrValoresParam.Add(obj.idcts);
				
				arrNombreParam.Add(EntPacPacientes.Fields.idcec.ToString());
				arrValoresParam.Add(obj.idcec);
				
				arrNombreParam.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrValoresParam.Add(obj.ocupacionppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrValoresParam.Add(obj.procedenciappa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrValoresParam.Add(obj.domicilioppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrValoresParam.Add(obj.dominanciappa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrValoresParam.Add(obj.telefonosppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrValoresParam.Add(obj.emailppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrValoresParam.Add(obj.religionppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrValoresParam.Add(obj.empresappa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrValoresParam.Add(obj.seguroppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrValoresParam.Add(obj.remiteppa);
				
				arrNombreParam.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrValoresParam.Add(obj.informanteppa);

                arrNombreParam.Add(EntPacPacientes.Fields.cippa.ToString());
                arrValoresParam.Add(obj.cippa);

                arrNombreParam.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrValoresParam.Add(obj.apitransaccion);
				
				arrNombreParam.Add(EntPacPacientes.Fields.usumod.ToString());
				arrValoresParam.Add(obj.usumod);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacPacientes.SpPpaUpd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacpacientes
		/// </returns>
		public int Delete(EntPacPacientes obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntPacPacientes.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacPacientes.SpPpaDel.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacpacientes
		/// </returns>
		public int Delete(EntPacPacientes obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntPacPacientes.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacPacientes.SpPpaDel.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacpacientes
		/// </returns>
		public int InsertUpdate(EntPacPacientes obj)
		{
			try
			{
				bool esInsertar = true;
				
					esInsertar = (esInsertar && (obj.idppa == null));
				
				if (esInsertar)
					return Insert(obj) ? 1 : 0;
				else
					return Update(obj);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacpacientes
		/// </returns>
		public int InsertUpdate(EntPacPacientes obj, ref CTrans localTrans)
		{
			try
			{
				bool esInsertar = false;
				
					esInsertar = (esInsertar && (obj.idppa == null));
				
				if (esInsertar)
					return Insert(obj, ref localTrans) ? 1 : 0;
				else
					return Update(obj, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs Query

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacpacientes
		/// </returns>
		public bool InsertQuery(EntPacPacientes obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.nombresppa == null ? null : "'" + obj.nombresppa + "'");
				arrValores.Add(obj.appaternoppa == null ? null : "'" + obj.appaternoppa + "'");
				arrValores.Add(obj.apmaternoppa == null ? null : "'" + obj.apmaternoppa + "'");
				arrValores.Add(obj.generoppa);
				arrValores.Add(obj.fechanac == null ? null : "'" + Convert.ToDateTime(obj.fechanac).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.idcts);
				arrValores.Add(obj.idcec);
				arrValores.Add(obj.ocupacionppa == null ? null : "'" + obj.ocupacionppa + "'");
				arrValores.Add(obj.procedenciappa == null ? null : "'" + obj.procedenciappa + "'");
				arrValores.Add(obj.domicilioppa == null ? null : "'" + obj.domicilioppa + "'");
				arrValores.Add(obj.dominanciappa == null ? null : "'" + obj.dominanciappa + "'");
				arrValores.Add(obj.telefonosppa == null ? null : "'" + obj.telefonosppa + "'");
				arrValores.Add(obj.emailppa == null ? null : "'" + obj.emailppa + "'");
				arrValores.Add(obj.religionppa == null ? null : "'" + obj.religionppa + "'");
				arrValores.Add(obj.empresappa == null ? null : "'" + obj.empresappa + "'");
				arrValores.Add(obj.seguroppa == null ? null : "'" + obj.seguroppa + "'");
				arrValores.Add(obj.remiteppa == null ? null : "'" + obj.remiteppa + "'");
				arrValores.Add(obj.informanteppa == null ? null : "'" + obj.informanteppa + "'");
				arrValores.Add(obj.cippa == null ? null : "'" + obj.cippa + "'");
                arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntPacPacientes.StrNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacpacientes
		/// </returns>
		public bool InsertQuery(EntPacPacientes obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.nombresppa == null ? null : "'" + obj.nombresppa + "'");
				arrValores.Add(obj.appaternoppa == null ? null : "'" + obj.appaternoppa + "'");
				arrValores.Add(obj.apmaternoppa == null ? null : "'" + obj.apmaternoppa + "'");
				arrValores.Add(obj.generoppa);
				arrValores.Add(obj.fechanac == null ? null : "'" + Convert.ToDateTime(obj.fechanac).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.idcts);
				arrValores.Add(obj.idcec);
				arrValores.Add(obj.ocupacionppa == null ? null : "'" + obj.ocupacionppa + "'");
				arrValores.Add(obj.procedenciappa == null ? null : "'" + obj.procedenciappa + "'");
				arrValores.Add(obj.domicilioppa == null ? null : "'" + obj.domicilioppa + "'");
				arrValores.Add(obj.dominanciappa == null ? null : "'" + obj.dominanciappa + "'");
				arrValores.Add(obj.telefonosppa == null ? null : "'" + obj.telefonosppa + "'");
				arrValores.Add(obj.emailppa == null ? null : "'" + obj.emailppa + "'");
				arrValores.Add(obj.religionppa == null ? null : "'" + obj.religionppa + "'");
				arrValores.Add(obj.empresappa == null ? null : "'" + obj.empresappa + "'");
				arrValores.Add(obj.seguroppa == null ? null : "'" + obj.seguroppa + "'");
				arrValores.Add(obj.remiteppa == null ? null : "'" + obj.remiteppa + "'");
				arrValores.Add(obj.informanteppa == null ? null : "'" + obj.informanteppa + "'");
				arrValores.Add(obj.cippa == null ? null : "'" + obj.cippa + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntPacPacientes.StrNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacpacientes
		/// </returns>
		public bool InsertQueryIdentity(EntPacPacientes obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.nombresppa == null ? null : "'" + obj.nombresppa + "'");
				arrValores.Add(obj.appaternoppa == null ? null : "'" + obj.appaternoppa + "'");
				arrValores.Add(obj.apmaternoppa == null ? null : "'" + obj.apmaternoppa + "'");
				arrValores.Add(obj.generoppa);
				arrValores.Add(obj.fechanac == null ? null : "'" + Convert.ToDateTime(obj.fechanac).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.idcts);
				arrValores.Add(obj.idcec);
				arrValores.Add(obj.ocupacionppa == null ? null : "'" + obj.ocupacionppa + "'");
				arrValores.Add(obj.procedenciappa == null ? null : "'" + obj.procedenciappa + "'");
				arrValores.Add(obj.domicilioppa == null ? null : "'" + obj.domicilioppa + "'");
				arrValores.Add(obj.dominanciappa == null ? null : "'" + obj.dominanciappa + "'");
				arrValores.Add(obj.telefonosppa == null ? null : "'" + obj.telefonosppa + "'");
				arrValores.Add(obj.emailppa == null ? null : "'" + obj.emailppa + "'");
				arrValores.Add(obj.religionppa == null ? null : "'" + obj.religionppa + "'");
				arrValores.Add(obj.empresappa == null ? null : "'" + obj.empresappa + "'");
				arrValores.Add(obj.seguroppa == null ? null : "'" + obj.seguroppa + "'");
				arrValores.Add(obj.remiteppa == null ? null : "'" + obj.remiteppa + "'");
				arrValores.Add(obj.informanteppa == null ? null : "'" + obj.informanteppa + "'");
				arrValores.Add(obj.cippa == null ? null : "'" + obj.cippa + "'");
                arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntPacPacientes.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.idppa = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpacpacientes
		/// </returns>
		public bool InsertQueryIdentity(EntPacPacientes obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.nombresppa == null ? null : "'" + obj.nombresppa + "'");
				arrValores.Add(obj.appaternoppa == null ? null : "'" + obj.appaternoppa + "'");
				arrValores.Add(obj.apmaternoppa == null ? null : "'" + obj.apmaternoppa + "'");
				arrValores.Add(obj.generoppa);
				arrValores.Add(obj.fechanac == null ? null : "'" + Convert.ToDateTime(obj.fechanac).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.idcts);
				arrValores.Add(obj.idcec);
				arrValores.Add(obj.ocupacionppa == null ? null : "'" + obj.ocupacionppa + "'");
				arrValores.Add(obj.procedenciappa == null ? null : "'" + obj.procedenciappa + "'");
				arrValores.Add(obj.domicilioppa == null ? null : "'" + obj.domicilioppa + "'");
				arrValores.Add(obj.dominanciappa == null ? null : "'" + obj.dominanciappa + "'");
				arrValores.Add(obj.telefonosppa == null ? null : "'" + obj.telefonosppa + "'");
				arrValores.Add(obj.emailppa == null ? null : "'" + obj.emailppa + "'");
				arrValores.Add(obj.religionppa == null ? null : "'" + obj.religionppa + "'");
				arrValores.Add(obj.empresappa == null ? null : "'" + obj.empresappa + "'");
				arrValores.Add(obj.seguroppa == null ? null : "'" + obj.seguroppa + "'");
				arrValores.Add(obj.remiteppa == null ? null : "'" + obj.remiteppa + "'");
				arrValores.Add(obj.informanteppa == null ? null : "'" + obj.informanteppa + "'");
				arrValores.Add(obj.cippa == null ? null : "'" + obj.cippa + "'");
                arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntPacPacientes.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.idppa = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpacpacientes
		/// </returns>
		public int UpdateQueryAll(EntPacPacientes obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.nombresppa == null ? null : "'" + obj.nombresppa + "'");
				arrValores.Add(obj.appaternoppa == null ? null : "'" + obj.appaternoppa + "'");
				arrValores.Add(obj.apmaternoppa == null ? null : "'" + obj.apmaternoppa + "'");
				arrValores.Add(obj.generoppa);
				arrValores.Add(obj.fechanac == null ? null : "'" + Convert.ToDateTime(obj.fechanac).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.idcts);
				arrValores.Add(obj.idcec);
				arrValores.Add(obj.ocupacionppa == null ? null : "'" + obj.ocupacionppa + "'");
				arrValores.Add(obj.procedenciappa == null ? null : "'" + obj.procedenciappa + "'");
				arrValores.Add(obj.domicilioppa == null ? null : "'" + obj.domicilioppa + "'");
				arrValores.Add(obj.dominanciappa == null ? null : "'" + obj.dominanciappa + "'");
				arrValores.Add(obj.telefonosppa == null ? null : "'" + obj.telefonosppa + "'");
				arrValores.Add(obj.emailppa == null ? null : "'" + obj.emailppa + "'");
				arrValores.Add(obj.religionppa == null ? null : "'" + obj.religionppa + "'");
				arrValores.Add(obj.empresappa == null ? null : "'" + obj.empresappa + "'");
				arrValores.Add(obj.seguroppa == null ? null : "'" + obj.seguroppa + "'");
				arrValores.Add(obj.remiteppa == null ? null : "'" + obj.remiteppa + "'");
				arrValores.Add(obj.informanteppa == null ? null : "'" + obj.informanteppa + "'");
				arrValores.Add(obj.cippa == null ? null : "'" + obj.cippa + "'");
                arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacPacientes.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacPacientes.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pacpacientes a partir de una clase del tipo epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQueryAll(EntPacPacientes obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.nombresppa == null ? null : "'" + obj.nombresppa + "'");
				arrValores.Add(obj.appaternoppa == null ? null : "'" + obj.appaternoppa + "'");
				arrValores.Add(obj.apmaternoppa == null ? null : "'" + obj.apmaternoppa + "'");
				arrValores.Add(obj.generoppa);
				arrValores.Add(obj.fechanac == null ? null : "'" + Convert.ToDateTime(obj.fechanac).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValores.Add(obj.idcts);
				arrValores.Add(obj.idcec);
				arrValores.Add(obj.ocupacionppa == null ? null : "'" + obj.ocupacionppa + "'");
				arrValores.Add(obj.procedenciappa == null ? null : "'" + obj.procedenciappa + "'");
				arrValores.Add(obj.domicilioppa == null ? null : "'" + obj.domicilioppa + "'");
				arrValores.Add(obj.dominanciappa == null ? null : "'" + obj.dominanciappa + "'");
				arrValores.Add(obj.telefonosppa == null ? null : "'" + obj.telefonosppa + "'");
				arrValores.Add(obj.emailppa == null ? null : "'" + obj.emailppa + "'");
				arrValores.Add(obj.religionppa == null ? null : "'" + obj.religionppa + "'");
				arrValores.Add(obj.empresappa == null ? null : "'" + obj.empresappa + "'");
				arrValores.Add(obj.seguroppa == null ? null : "'" + obj.seguroppa + "'");
				arrValores.Add(obj.remiteppa == null ? null : "'" + obj.remiteppa + "'");
				arrValores.Add(obj.informanteppa == null ? null : "'" + obj.informanteppa + "'");
				arrValores.Add(obj.cippa == null ? null : "'" + obj.cippa + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacPacientes.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacPacientes.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pacpacientes a partir de una clase del tipo Epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpacpacientes
		/// </returns>
		public int UpdateQuery(EntPacPacientes obj)
		{
			try
			{
				//Obtenemos el Objeto original
				EntPacPacientes objOriginal = this.ObtenerObjeto(obj.idppa);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.nombresppa != objOriginal.nombresppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
					arrValores.Add(obj.nombresppa == null ? null : "'" + obj.nombresppa + "'");
				}
				if(obj.appaternoppa != objOriginal.appaternoppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
					arrValores.Add(obj.appaternoppa == null ? null : "'" + obj.appaternoppa + "'");
				}
				if(obj.apmaternoppa != objOriginal.apmaternoppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
					arrValores.Add(obj.apmaternoppa == null ? null : "'" + obj.apmaternoppa + "'");
				}
				if(obj.generoppa != objOriginal.generoppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
					arrValores.Add(obj.generoppa);
				}
				if(obj.fechanac != objOriginal.fechanac )
				{
					arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
					arrValores.Add(obj.fechanac == null ? null : "'" + Convert.ToDateTime(obj.fechanac).ToString(CParametros.ParFormatoFechaHora) + "'");
				}
				if(obj.idcts != objOriginal.idcts )
				{
					arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
					arrValores.Add(obj.idcts);
				}
				if(obj.idcec != objOriginal.idcec )
				{
					arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
					arrValores.Add(obj.idcec);
				}
				if(obj.ocupacionppa != objOriginal.ocupacionppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
					arrValores.Add(obj.ocupacionppa == null ? null : "'" + obj.ocupacionppa + "'");
				}
				if(obj.procedenciappa != objOriginal.procedenciappa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
					arrValores.Add(obj.procedenciappa == null ? null : "'" + obj.procedenciappa + "'");
				}
				if(obj.domicilioppa != objOriginal.domicilioppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
					arrValores.Add(obj.domicilioppa == null ? null : "'" + obj.domicilioppa + "'");
				}
				if(obj.dominanciappa != objOriginal.dominanciappa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
					arrValores.Add(obj.dominanciappa == null ? null : "'" + obj.dominanciappa + "'");
				}
				if(obj.telefonosppa != objOriginal.telefonosppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
					arrValores.Add(obj.telefonosppa == null ? null : "'" + obj.telefonosppa + "'");
				}
				if(obj.emailppa != objOriginal.emailppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
					arrValores.Add(obj.emailppa == null ? null : "'" + obj.emailppa + "'");
				}
				if(obj.religionppa != objOriginal.religionppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
					arrValores.Add(obj.religionppa == null ? null : "'" + obj.religionppa + "'");
				}
				if(obj.empresappa != objOriginal.empresappa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
					arrValores.Add(obj.empresappa == null ? null : "'" + obj.empresappa + "'");
				}
				if(obj.seguroppa != objOriginal.seguroppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
					arrValores.Add(obj.seguroppa == null ? null : "'" + obj.seguroppa + "'");
				}
				if(obj.remiteppa != objOriginal.remiteppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
					arrValores.Add(obj.remiteppa == null ? null : "'" + obj.remiteppa + "'");
				}
				if(obj.informanteppa != objOriginal.informanteppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
					arrValores.Add(obj.informanteppa == null ? null : "'" + obj.informanteppa + "'");
				}
                if (obj.cippa != objOriginal.cippa)
                {
                    arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                    arrValores.Add(obj.cippa == null ? null : "'" + obj.cippa + "'");
                }
                if (obj.apiestado != objOriginal.apiestado )
				{
					arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
					arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				}
				if(obj.apitransaccion != objOriginal.apitransaccion )
				{
					arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
					arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				}
				if(obj.usumod != objOriginal.usumod )
				{
					arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
					arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacPacientes.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacPacientes.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pacpacientes a partir de una clase del tipo epacpacientes
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQuery(EntPacPacientes obj, ref CTrans localTrans)
		{
			try
			{
				//Obtenemos el Objeto original
				EntPacPacientes objOriginal = this.ObtenerObjeto(obj.idppa, ref localTrans);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.nombresppa != objOriginal.nombresppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
					arrValores.Add(obj.nombresppa == null ? null : "'" + obj.nombresppa + "'");
				}
				if(obj.appaternoppa != objOriginal.appaternoppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
					arrValores.Add(obj.appaternoppa == null ? null : "'" + obj.appaternoppa + "'");
				}
				if(obj.apmaternoppa != objOriginal.apmaternoppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
					arrValores.Add(obj.apmaternoppa == null ? null : "'" + obj.apmaternoppa + "'");
				}
				if(obj.generoppa != objOriginal.generoppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
					arrValores.Add(obj.generoppa);
				}
				if(obj.fechanac != objOriginal.fechanac )
				{
					arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
					arrValores.Add(obj.fechanac == null ? null : "'" + Convert.ToDateTime(obj.fechanac).ToString(CParametros.ParFormatoFechaHora) + "'");
				}
				if(obj.idcts != objOriginal.idcts )
				{
					arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
					arrValores.Add(obj.idcts);
				}
				if(obj.idcec != objOriginal.idcec )
				{
					arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
					arrValores.Add(obj.idcec);
				}
				if(obj.ocupacionppa != objOriginal.ocupacionppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
					arrValores.Add(obj.ocupacionppa == null ? null : "'" + obj.ocupacionppa + "'");
				}
				if(obj.procedenciappa != objOriginal.procedenciappa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
					arrValores.Add(obj.procedenciappa == null ? null : "'" + obj.procedenciappa + "'");
				}
				if(obj.domicilioppa != objOriginal.domicilioppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
					arrValores.Add(obj.domicilioppa == null ? null : "'" + obj.domicilioppa + "'");
				}
				if(obj.dominanciappa != objOriginal.dominanciappa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
					arrValores.Add(obj.dominanciappa == null ? null : "'" + obj.dominanciappa + "'");
				}
				if(obj.telefonosppa != objOriginal.telefonosppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
					arrValores.Add(obj.telefonosppa == null ? null : "'" + obj.telefonosppa + "'");
				}
				if(obj.emailppa != objOriginal.emailppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
					arrValores.Add(obj.emailppa == null ? null : "'" + obj.emailppa + "'");
				}
				if(obj.religionppa != objOriginal.religionppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
					arrValores.Add(obj.religionppa == null ? null : "'" + obj.religionppa + "'");
				}
				if(obj.empresappa != objOriginal.empresappa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
					arrValores.Add(obj.empresappa == null ? null : "'" + obj.empresappa + "'");
				}
				if(obj.seguroppa != objOriginal.seguroppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
					arrValores.Add(obj.seguroppa == null ? null : "'" + obj.seguroppa + "'");
				}
				if(obj.remiteppa != objOriginal.remiteppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
					arrValores.Add(obj.remiteppa == null ? null : "'" + obj.remiteppa + "'");
				}
				if(obj.informanteppa != objOriginal.informanteppa )
				{
					arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
					arrValores.Add(obj.informanteppa == null ? null : "'" + obj.informanteppa + "'");
				}
                if (obj.cippa != objOriginal.cippa)
                {
                    arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                    arrValores.Add(obj.cippa == null ? null : "'" + obj.cippa + "'");
                }
                if (obj.apiestado != objOriginal.apiestado )
				{
					arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
					arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				}
				if(obj.apitransaccion != objOriginal.apitransaccion )
				{
					arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
					arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				}
				if(obj.usumod != objOriginal.usumod )
				{
					arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
					arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacPacientes.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacPacientes.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pacpacientes a partir de una clase del tipo EntPacPacientes y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacPacientes">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpacpacientes
		/// </returns>
		public int DeleteQuery(EntPacPacientes obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacPacientes.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.DeleteBd(EntPacPacientes.StrNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pacpacientes a partir de una clase del tipo EntPacPacientes y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.epacpacientes">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionpacpacientes
		/// </returns>
		public int DeleteQuery(EntPacPacientes obj, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacPacientes.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.DeleteBd(EntPacPacientes.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pacpacientes a partir de una clase del tipo epacpacientes
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpacpacientes
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd("pacpacientes", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pacpacientes a partir de una clase del tipo epacpacientes
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pacpacientes
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionpacpacientes
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd(EntPacPacientes.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region Llenado de elementos

		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, String textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, EntPacPacientes.Fields searchField, object searchValue)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, String textField, EntPacPacientes.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, String textField, EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.DataValueField = table.Columns[0].ColumnName;
					cmb.DataTextField = table.Columns[1].ColumnName;
					cmb.DataSource = table;
					cmb.DataBind();

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntPacPacientes.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntPacPacientes.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacPacientes.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacPacientes.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridViewOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, EntPacPacientes.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, String textField, EntPacPacientes.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, String textField, EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pacpacientes
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pacpacientes
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntPacPacientes.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
                arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntPacPacientes.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacPacientes.Fields.idppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.nombresppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.appaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.generoppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fechanac.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcts.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.idcec.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.ocupacionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.procedenciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.domicilioppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.dominanciappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.telefonosppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.emailppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.religionppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.empresappa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.seguroppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.remiteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.informanteppa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.cippa.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usucre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.feccre.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.usumod.ToString());
				arrColumnas.Add(EntPacPacientes.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacPacientes.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacPacientes.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacPacientes.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pacpacientes
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntPacPacientes.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 


		#endregion

		#region Funciones Internas

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto pacpacientes
		/// </returns>
		internal EntPacPacientes crearObjeto(DataRow row)
		{
			var obj = new EntPacPacientes();
			obj.idppa = GetColumnType(row[EntPacPacientes.Fields.idppa.ToString()], EntPacPacientes.Fields.idppa);
			obj.nombresppa = GetColumnType(row[EntPacPacientes.Fields.nombresppa.ToString()], EntPacPacientes.Fields.nombresppa);
			obj.appaternoppa = GetColumnType(row[EntPacPacientes.Fields.appaternoppa.ToString()], EntPacPacientes.Fields.appaternoppa);
			obj.apmaternoppa = GetColumnType(row[EntPacPacientes.Fields.apmaternoppa.ToString()], EntPacPacientes.Fields.apmaternoppa);
			obj.generoppa = GetColumnType(row[EntPacPacientes.Fields.generoppa.ToString()], EntPacPacientes.Fields.generoppa);
			obj.fechanac = GetColumnType(row[EntPacPacientes.Fields.fechanac.ToString()], EntPacPacientes.Fields.fechanac);
			obj.idcts = GetColumnType(row[EntPacPacientes.Fields.idcts.ToString()], EntPacPacientes.Fields.idcts);
			obj.idcec = GetColumnType(row[EntPacPacientes.Fields.idcec.ToString()], EntPacPacientes.Fields.idcec);
			obj.ocupacionppa = GetColumnType(row[EntPacPacientes.Fields.ocupacionppa.ToString()], EntPacPacientes.Fields.ocupacionppa);
			obj.procedenciappa = GetColumnType(row[EntPacPacientes.Fields.procedenciappa.ToString()], EntPacPacientes.Fields.procedenciappa);
			obj.domicilioppa = GetColumnType(row[EntPacPacientes.Fields.domicilioppa.ToString()], EntPacPacientes.Fields.domicilioppa);
			obj.dominanciappa = GetColumnType(row[EntPacPacientes.Fields.dominanciappa.ToString()], EntPacPacientes.Fields.dominanciappa);
			obj.telefonosppa = GetColumnType(row[EntPacPacientes.Fields.telefonosppa.ToString()], EntPacPacientes.Fields.telefonosppa);
			obj.emailppa = GetColumnType(row[EntPacPacientes.Fields.emailppa.ToString()], EntPacPacientes.Fields.emailppa);
			obj.religionppa = GetColumnType(row[EntPacPacientes.Fields.religionppa.ToString()], EntPacPacientes.Fields.religionppa);
			obj.empresappa = GetColumnType(row[EntPacPacientes.Fields.empresappa.ToString()], EntPacPacientes.Fields.empresappa);
			obj.seguroppa = GetColumnType(row[EntPacPacientes.Fields.seguroppa.ToString()], EntPacPacientes.Fields.seguroppa);
			obj.remiteppa = GetColumnType(row[EntPacPacientes.Fields.remiteppa.ToString()], EntPacPacientes.Fields.remiteppa);
			obj.informanteppa = GetColumnType(row[EntPacPacientes.Fields.informanteppa.ToString()], EntPacPacientes.Fields.informanteppa);
			obj.cippa = GetColumnType(row[EntPacPacientes.Fields.cippa.ToString()], EntPacPacientes.Fields.cippa);
			obj.apiestado = GetColumnType(row[EntPacPacientes.Fields.apiestado.ToString()], EntPacPacientes.Fields.apiestado);
			obj.apitransaccion = GetColumnType(row[EntPacPacientes.Fields.apitransaccion.ToString()], EntPacPacientes.Fields.apitransaccion);
			obj.usucre = GetColumnType(row[EntPacPacientes.Fields.usucre.ToString()], EntPacPacientes.Fields.usucre);
			obj.feccre = GetColumnType(row[EntPacPacientes.Fields.feccre.ToString()], EntPacPacientes.Fields.feccre);
			obj.usumod = GetColumnType(row[EntPacPacientes.Fields.usumod.ToString()], EntPacPacientes.Fields.usumod);
			obj.fecmod = GetColumnType(row[EntPacPacientes.Fields.fecmod.ToString()], EntPacPacientes.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto pacpacientes
		/// </returns>
		internal EntPacPacientes crearObjetoRevisado(DataRow row)
		{
			var obj = new EntPacPacientes();
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.idppa.ToString()))
				obj.idppa = GetColumnType(row[EntPacPacientes.Fields.idppa.ToString()], EntPacPacientes.Fields.idppa);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.nombresppa.ToString()))
				obj.nombresppa = GetColumnType(row[EntPacPacientes.Fields.nombresppa.ToString()], EntPacPacientes.Fields.nombresppa);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.appaternoppa.ToString()))
				obj.appaternoppa = GetColumnType(row[EntPacPacientes.Fields.appaternoppa.ToString()], EntPacPacientes.Fields.appaternoppa);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.apmaternoppa.ToString()))
				obj.apmaternoppa = GetColumnType(row[EntPacPacientes.Fields.apmaternoppa.ToString()], EntPacPacientes.Fields.apmaternoppa);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.generoppa.ToString()))
				obj.generoppa = GetColumnType(row[EntPacPacientes.Fields.generoppa.ToString()], EntPacPacientes.Fields.generoppa);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.fechanac.ToString()))
				obj.fechanac = GetColumnType(row[EntPacPacientes.Fields.fechanac.ToString()], EntPacPacientes.Fields.fechanac);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.idcts.ToString()))
				obj.idcts = GetColumnType(row[EntPacPacientes.Fields.idcts.ToString()], EntPacPacientes.Fields.idcts);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.idcec.ToString()))
				obj.idcec = GetColumnType(row[EntPacPacientes.Fields.idcec.ToString()], EntPacPacientes.Fields.idcec);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.ocupacionppa.ToString()))
				obj.ocupacionppa = GetColumnType(row[EntPacPacientes.Fields.ocupacionppa.ToString()], EntPacPacientes.Fields.ocupacionppa);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.procedenciappa.ToString()))
				obj.procedenciappa = GetColumnType(row[EntPacPacientes.Fields.procedenciappa.ToString()], EntPacPacientes.Fields.procedenciappa);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.domicilioppa.ToString()))
				obj.domicilioppa = GetColumnType(row[EntPacPacientes.Fields.domicilioppa.ToString()], EntPacPacientes.Fields.domicilioppa);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.dominanciappa.ToString()))
				obj.dominanciappa = GetColumnType(row[EntPacPacientes.Fields.dominanciappa.ToString()], EntPacPacientes.Fields.dominanciappa);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.telefonosppa.ToString()))
				obj.telefonosppa = GetColumnType(row[EntPacPacientes.Fields.telefonosppa.ToString()], EntPacPacientes.Fields.telefonosppa);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.emailppa.ToString()))
				obj.emailppa = GetColumnType(row[EntPacPacientes.Fields.emailppa.ToString()], EntPacPacientes.Fields.emailppa);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.religionppa.ToString()))
				obj.religionppa = GetColumnType(row[EntPacPacientes.Fields.religionppa.ToString()], EntPacPacientes.Fields.religionppa);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.empresappa.ToString()))
				obj.empresappa = GetColumnType(row[EntPacPacientes.Fields.empresappa.ToString()], EntPacPacientes.Fields.empresappa);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.seguroppa.ToString()))
				obj.seguroppa = GetColumnType(row[EntPacPacientes.Fields.seguroppa.ToString()], EntPacPacientes.Fields.seguroppa);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.remiteppa.ToString()))
				obj.remiteppa = GetColumnType(row[EntPacPacientes.Fields.remiteppa.ToString()], EntPacPacientes.Fields.remiteppa);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.informanteppa.ToString()))
				obj.informanteppa = GetColumnType(row[EntPacPacientes.Fields.informanteppa.ToString()], EntPacPacientes.Fields.informanteppa);
            if (row.Table.Columns.Contains(EntPacPacientes.Fields.cippa.ToString()))
                obj.cippa = GetColumnType(row[EntPacPacientes.Fields.cippa.ToString()], EntPacPacientes.Fields.cippa);
            if (row.Table.Columns.Contains(EntPacPacientes.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[EntPacPacientes.Fields.apiestado.ToString()], EntPacPacientes.Fields.apiestado);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.apitransaccion.ToString()))
				obj.apitransaccion = GetColumnType(row[EntPacPacientes.Fields.apitransaccion.ToString()], EntPacPacientes.Fields.apitransaccion);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[EntPacPacientes.Fields.usucre.ToString()], EntPacPacientes.Fields.usucre);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[EntPacPacientes.Fields.feccre.ToString()], EntPacPacientes.Fields.feccre);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[EntPacPacientes.Fields.usumod.ToString()], EntPacPacientes.Fields.usumod);
			if (row.Table.Columns.Contains(EntPacPacientes.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[EntPacPacientes.Fields.fecmod.ToString()], EntPacPacientes.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtpacpacientes" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos pacpacientes
		/// </returns>
		internal List<EntPacPacientes> crearLista(DataTable dtpacpacientes)
		{
			var list = new List<EntPacPacientes>();
			
			foreach (DataRow row in dtpacpacientes.Rows)
			{
				var obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtpacpacientes" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos pacpacientes
		/// </returns>
		internal List<EntPacPacientes> crearListaRevisada(DataTable dtpacpacientes)
		{
			List<EntPacPacientes> list = new List<EntPacPacientes>();
			
			foreach (DataRow row in dtpacpacientes.Rows)
			{
				var obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtpacpacientes" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos pacpacientes
		/// </returns>
		internal Queue<EntPacPacientes> crearCola(DataTable dtpacpacientes)
		{
			Queue<EntPacPacientes> cola = new Queue<EntPacPacientes>();
			
			EntPacPacientes obj = new EntPacPacientes();
			foreach (DataRow row in dtpacpacientes.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtpacpacientes" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos pacpacientes
		/// </returns>
		internal Stack<EntPacPacientes> crearPila(DataTable dtpacpacientes)
		{
			Stack<EntPacPacientes> pila = new Stack<EntPacPacientes>();
			
			foreach (DataRow row in dtpacpacientes.Rows)
			{
				var obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtpacpacientes" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos pacpacientes
		/// </returns>
		internal Dictionary<String, EntPacPacientes> crearDiccionario(DataTable dtpacpacientes)
		{
			Dictionary<String, EntPacPacientes>  miDic = new Dictionary<String, EntPacPacientes>();
			
			foreach (DataRow row in dtpacpacientes.Rows)
			{
				var obj = crearObjeto(row);
				miDic.Add(obj.idppa.ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtpacpacientes" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 HashTable de Objetos pacpacientes
		/// </returns>
		internal Hashtable crearHashTable(DataTable dtpacpacientes)
		{
			Hashtable miTabla = new Hashtable();
			
			foreach (DataRow row in dtpacpacientes.Rows)
			{
				var obj = crearObjeto(row);
				miTabla.Add(obj.idppa.ToString(), obj);
			}
			return miTabla;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtpacpacientes" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos pacpacientes
		/// </returns>
		internal Dictionary<String, EntPacPacientes> crearDiccionarioRevisado(DataTable dtpacpacientes)
		{
			Dictionary<String, EntPacPacientes>  miDic = new Dictionary<String, EntPacPacientes>();
			
			foreach (DataRow row in dtpacpacientes.Rows)
			{
				var obj = crearObjetoRevisado(row);
				miDic.Add(obj.idppa.ToString(), obj);
			}
			return miDic;
		}
		
		internal Dictionary<String, EntPacPacientes> crearDiccionario(DataTable dtpacpacientes, EntPacPacientes.Fields dicKey)
		{
			Dictionary<String, EntPacPacientes>  miDic = new Dictionary<String, EntPacPacientes>();
			
			foreach (DataRow row in dtpacpacientes.Rows)
			{
				var obj = crearObjeto(row);
				
				var nameOfProperty = dicKey.ToString();
				var propertyInfo = obj.GetType().GetProperty(nameOfProperty);
				var value = propertyInfo.GetValue(obj, null);
				
				miDic.Add(value.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
		#endregion

	}
}

