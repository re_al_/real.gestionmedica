#region 
/***********************************************************************************************************
	NOMBRE:       RnPacHistoriaclinica3
	DESCRIPCION:
		Clase que implementa los metodos y operaciones sobre la Tabla pachistoriaclinica3

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        22/04/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.ComponentModel;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Interface;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Modelo
{
	public class RnPacHistoriaclinica3: IPacHistoriaclinica3
	{
		//Debe implementar la Interface (Alt + Shift + F10)

		#region IPacHistoriaclinica3 Members

		#region Reflection

		/// <summary>
		/// Metodo que devuelve el Script SQL de la Tabla
		/// </summary>
		/// <returns>Script SQL</returns>
		public string GetTableScript()
		{
			TableClass tabla = new TableClass(typeof(EntPacHistoriaclinica3));
			return tabla.CreateTableScript();
		}
		
		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="myField">Enum de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, EntPacHistoriaclinica3.Fields myField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntPacHistoriaclinica3).GetProperty(myField.ToString()).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

		/// <summary>
		/// Metodo para castear Dinamicamente un Tipo
		/// </summary>
		/// <param name="valor">Tipo a ser casteado</param>
		/// <param name="strField">Nombre de la columna</param>
		/// <returns>Devuelve un objeto del Tipo de la columna especificada en el Enum</returns>
		public dynamic GetColumnType(object valor, string strField)
		{
			if (DBNull.Value.Equals(valor)) 
				return null;
			Type destino = typeof(EntPacHistoriaclinica3).GetProperty(strField).PropertyType;
			var miTipo = Nullable.GetUnderlyingType(destino) ?? destino;
			
			try
			{
				TypeConverter tc = TypeDescriptor.GetConverter(miTipo);
				return tc.ConvertFrom(valor);
			}
			catch (Exception)
			{
				return Convert.ChangeType(valor, miTipo);
			}
		}

/// <summary>
/// Inserta una valor a una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">Es el nombre de la propiedad</param>
/// <param name="dynValor">Es el valor que se insertara a la propiedad</param>
public void SetDato(ref EntPacHistoriaclinica3 obj, string strPropiedad, dynamic dynValor)
{
	if (obj == null) throw new ArgumentNullException();
	obj.GetType().GetProperty(strPropiedad).SetValue(obj, GetColumnType(dynValor, strPropiedad), null);
}

/// <summary>
/// Obtiene el valor de una propiedad de un objeto instanciado
/// </summary>
/// <param name="obj">Objeto instanciado</param>
/// <param name="strPropiedad">El nombre de la propiedad de la que se obtendra el valor</param>
/// <returns>Devuelve el valor del a propiedad seleccionada</returns>
public dynamic GetDato(ref EntPacHistoriaclinica3 obj, string strPropiedad)
{
	if (obj == null) return null;
	var propertyInfo = obj.GetType().GetProperty(strPropiedad);
	return GetColumnType(propertyInfo.GetValue(obj, null), strPropiedad);
}

		/// <summary>
		/// 	 Funcion que obtiene la llave primaria unica de la tabla pachistoriaclinica3 a partir de una cadena
		/// </summary>
		/// <param name="args" type="string[]">
		///     <para>
		/// 		 Cadena desde la que se construye el identificador unico de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Identificador unico de la tabla pachistoriaclinica3
		/// </returns>
		public string CreatePk(string[] args)
		{
			return args[0];
		}
		
		#endregion 

		#region ObtenerObjeto

		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica3 a partir de la llave primaria
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica3 ObtenerObjeto(Int64 Int64idppa)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64idppa + "'");
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica3 a partir del usuario que inserta
		/// </summary>
		/// <param name="strUsuCre">Login o nombre de usuario</param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica3 ObtenerObjetoInsertado(string strUsuCre)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + strUsuCre + "'");
			
			int iIdInsertado = FuncionesMax(EntPacHistoriaclinica3.Fields.idppa, arrColumnasWhere, arrValoresWhere);
			
			return ObtenerObjeto(iIdInsertado);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica3 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica3 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntPacHistoriaclinica3 obj = new EntPacHistoriaclinica3();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica3 ObtenerObjeto(Hashtable htbFiltro)
		{
			return ObtenerObjeto(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica3 ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count == 1)
				{
					EntPacHistoriaclinica3 obj = new EntPacHistoriaclinica3();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica3 ObtenerObjeto(EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica3 ObtenerObjeto(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un Business Object del Tipo EntPacHistoriaclinica3 a partir de su llave promaria
		/// </summary>
		/// <returns>
		/// 	Objeto del Tipo EntPacHistoriaclinica3
		/// </returns>
		public EntPacHistoriaclinica3 ObtenerObjeto(Int64 Int64idppa, ref CTrans localTrans )
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
		
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'" + Int64idppa + "'");
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica3 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica3 ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerObjeto(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica3 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales,  ref CTrans localTrans)
		{
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica3 ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count == 1)
				{
					EntPacHistoriaclinica3 obj = new EntPacHistoriaclinica3();
					obj = crearObjeto(table.Rows[0]);
					return obj;
				}
				else if (table.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de un objeto");
				else
					return null;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica3 ObtenerObjeto(EntPacHistoriaclinica3.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public EntPacHistoriaclinica3 ObtenerObjeto(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerObjeto(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion

		#region ObtenerLista

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerLista()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacHistoriaclinica3>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerLista(EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerLista(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacHistoriaclinica3>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerLista(EntPacHistoriaclinica3.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerLista(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerLista(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerLista(Hashtable htbFiltro)
		{
			return ObtenerLista(htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacHistoriaclinica3>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerLista(htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearLista(table);
				}
				else
					return new List<EntPacHistoriaclinica3>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacHistoriaclinica3>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacHistoriaclinica3>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacHistoriaclinica3>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans)
		{
			return ObtenerListaDesdeVista(strVista, htbFiltro, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("*");
				
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + strVista, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearListaRevisada(table);
				}
				else
					return new List<EntPacHistoriaclinica3>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica3.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="strVista" type="String">
		///     <para>
		/// 		 Nombre de la Vista desde que se van ha obtener los datos
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.List que cumple con los filtros de los parametros
		/// </returns>
		public List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerListaDesdeVista(strVista, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerCola y Obtener Pila

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica3> ObtenerCola()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica3> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica3> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntPacHistoriaclinica3>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica3> ObtenerCola(EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica3> ObtenerCola(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica3> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica3> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearCola(table);
				}
				else
					return new Queue<EntPacHistoriaclinica3>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Queue que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica3> ObtenerCola(EntPacHistoriaclinica3.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Queue<EntPacHistoriaclinica3> ObtenerCola(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerCola(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica3> ObtenerPila()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica3> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica3> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntPacHistoriaclinica3>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica3> ObtenerPila(EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica3> ObtenerPila(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica3> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica3> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearPila(table);
				}
				else
					return new Stack<EntPacHistoriaclinica3>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica3> ObtenerPila(EntPacHistoriaclinica3.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Stack que cumple con los filtros de los parametros
		/// </returns>
		public Stack<EntPacHistoriaclinica3> ObtenerPila(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerPila(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		

		#endregion 

		#region ObtenerDataTable

		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla pachistoriaclinica3
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable NuevoDataTable()
		{
			try
			{
				DataTable table = new DataTable ();
				DataColumn dc;
				dc = new DataColumn(EntPacHistoriaclinica3.Fields.idppa.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.idppa.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h6imagen.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h6imagen.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.apiestado.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.apiestado.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.apitransaccion.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.apitransaccion.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.usucre.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.usucre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.feccre.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.feccre.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.usumod.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.usumod.ToString()).PropertyType);
				table.Columns.Add(dc);

				dc = new DataColumn(EntPacHistoriaclinica3.Fields.fecmod.ToString(),typeof(EntPacHistoriaclinica3).GetProperty(EntPacHistoriaclinica3.Fields.fecmod.ToString()).PropertyType);
				table.Columns.Add(dc);

				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que genera un DataTable con determinadas columnas de una pachistoriaclinica3
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable NuevoDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'2'");
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla pachistoriaclinica3
		/// </summary>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable ObtenerDataTable()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registros de una tabla y n condicion WHERE pachistoriaclinica3
		/// </summary>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable ObtenerDataTable(String strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(arrColumnas, htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro)
		{
			try
			{
				return ObtenerDataTable(htbFiltro, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="htbFiltro" type="System.Collections.Hashtable">
		///     <para>
		/// 		 Hashtable que contienen los pares para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				ArrayList arrValoresWhere = new ArrayList();
				
				foreach (DictionaryEntry entry in htbFiltro)
				{
					arrColumnasWhere.Add(entry.Key.ToString());
					arrValoresWhere.Add(entry.Value.ToString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				return ObtenerDataTableOr(arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataTable con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	 DataTable con los datos obtenidos de pachistoriaclinica3
		/// </returns>
		public DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableOr(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				return table;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Parametros adicionales
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParametrosAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene los datos a partir de un filtro realizado por algun campo
		/// </summary>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	DataTable que cumple con los filtros de los parametros
		/// </returns>
		public DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				return ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region ObtenerDiccionario

		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario()
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "");
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntPacHistoriaclinica3>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, "", ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table);
				}
				else
					return new Dictionary<string, EntPacHistoriaclinica3>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(EntPacHistoriaclinica3.Fields searchField, object searchValue, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, ref localTrans);
		}
		
		/// <summary>
		/// 	Funcion que obtiene un conjunto de datos de una Clase EntPacHistoriaclinica3 a partir de condiciones WHERE
		/// </summary>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo System.Collections.Generic.Dictionary que cumple con los filtros de los parametros
		/// </returns>
		public Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionario(arrColumnasWhere, arrValoresWhere, strParamAdicionales, ref localTrans);
		}
		
		public Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionarioKey(EntPacHistoriaclinica3.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionarioKey(String strParamAdic, EntPacHistoriaclinica3.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add("'1'");
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add("'1'");
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdic, dicKey);
		}
		
		public Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, EntPacHistoriaclinica3.Fields dicKey)
		{
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, "", dicKey);
		}
		
		public Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionarioKey(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, EntPacHistoriaclinica3.Fields dicKey)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Rows.Count > 0)
				{
					return crearDiccionario(table, dicKey);
				}
				else
					return new Dictionary<string, EntPacHistoriaclinica3>();
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		public Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionarioKey(EntPacHistoriaclinica3.Fields searchField, object searchValue, EntPacHistoriaclinica3.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, dicKey);
		}
		
		public Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionarioKey(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales, EntPacHistoriaclinica3.Fields dicKey)
		{
			ArrayList arrColumnasWhere = new ArrayList();
			arrColumnasWhere.Add(searchField.ToString());
			
			ArrayList arrValoresWhere = new ArrayList();
			arrValoresWhere.Add(searchValue);
			
			return ObtenerDiccionarioKey(arrColumnasWhere, arrValoresWhere, strParamAdicionales, dicKey);
		}
		

		#endregion 

		#region ObjetoASp

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla pachistoriaclinica3
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntPacHistoriaclinica3 obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValoresParam.Add(obj.h6reflejos01phc == null ? null : "'" + obj.h6reflejos01phc + "'");
				arrValoresParam.Add(obj.h6reflejos02phc == null ? null : "'" + obj.h6reflejos02phc + "'");
				arrValoresParam.Add(obj.h6reflejos03phc == null ? null : "'" + obj.h6reflejos03phc + "'");
				arrValoresParam.Add(obj.h6reflejos04phc == null ? null : "'" + obj.h6reflejos04phc + "'");
				arrValoresParam.Add(obj.h6reflejos05phc == null ? null : "'" + obj.h6reflejos05phc + "'");
				arrValoresParam.Add(obj.h6reflejos06phc == null ? null : "'" + obj.h6reflejos06phc + "'");
				arrValoresParam.Add(obj.h6reflejos07phc == null ? null : "'" + obj.h6reflejos07phc + "'");
				arrValoresParam.Add(obj.h6reflejos08phc == null ? null : "'" + obj.h6reflejos08phc + "'");
				arrValoresParam.Add(obj.h6reflejos09phc == null ? null : "'" + obj.h6reflejos09phc + "'");
				arrValoresParam.Add(obj.h6reflejos10phc == null ? null : "'" + obj.h6reflejos10phc + "'");
				arrValoresParam.Add(obj.h6miotbicderphc == null ? null : "'" + obj.h6miotbicderphc + "'");
				arrValoresParam.Add(obj.h6miotbicizqphc == null ? null : "'" + obj.h6miotbicizqphc + "'");
				arrValoresParam.Add(obj.h6miottriderphc == null ? null : "'" + obj.h6miottriderphc + "'");
				arrValoresParam.Add(obj.h6miottriizqphc == null ? null : "'" + obj.h6miottriizqphc + "'");
				arrValoresParam.Add(obj.h6miotestderphc == null ? null : "'" + obj.h6miotestderphc + "'");
				arrValoresParam.Add(obj.h6miotestizqphc == null ? null : "'" + obj.h6miotestizqphc + "'");
				arrValoresParam.Add(obj.h6miotpatderphc == null ? null : "'" + obj.h6miotpatderphc + "'");
				arrValoresParam.Add(obj.h6miotpatizqphc == null ? null : "'" + obj.h6miotpatizqphc + "'");
				arrValoresParam.Add(obj.h6miotaquderphc == null ? null : "'" + obj.h6miotaquderphc + "'");
				arrValoresParam.Add(obj.h6miotaquizqphc == null ? null : "'" + obj.h6miotaquizqphc + "'");
				arrValoresParam.Add(obj.h6patpladerphc == null ? null : "'" + obj.h6patpladerphc + "'");
				arrValoresParam.Add(obj.h6patplaizqphc == null ? null : "'" + obj.h6patplaizqphc + "'");
				arrValoresParam.Add(obj.h6pathofderphc == null ? null : "'" + obj.h6pathofderphc + "'");
				arrValoresParam.Add(obj.h6pathofizqphc == null ? null : "'" + obj.h6pathofizqphc + "'");
				arrValoresParam.Add(obj.h6pattroderphc == null ? null : "'" + obj.h6pattroderphc + "'");
				arrValoresParam.Add(obj.h6pattroizqphc == null ? null : "'" + obj.h6pattroizqphc + "'");
				arrValoresParam.Add(obj.h6patprederphc == null ? null : "'" + obj.h6patprederphc + "'");
				arrValoresParam.Add(obj.h6patpreizqphc == null ? null : "'" + obj.h6patpreizqphc + "'");
				arrValoresParam.Add(obj.h6patpmenderphc == null ? null : "'" + obj.h6patpmenderphc + "'");
				arrValoresParam.Add(obj.h6patpmenizqphc == null ? null : "'" + obj.h6patpmenizqphc + "'");
				arrValoresParam.Add(obj.h6supbicderphc == null ? null : "'" + obj.h6supbicderphc + "'");
				arrValoresParam.Add(obj.h6supbicizqphc == null ? null : "'" + obj.h6supbicizqphc + "'");
				arrValoresParam.Add(obj.h6suptriderphc == null ? null : "'" + obj.h6suptriderphc + "'");
				arrValoresParam.Add(obj.h6suptriizqphc == null ? null : "'" + obj.h6suptriizqphc + "'");
				arrValoresParam.Add(obj.h6supestderphc == null ? null : "'" + obj.h6supestderphc + "'");
				arrValoresParam.Add(obj.h6supestizqphc == null ? null : "'" + obj.h6supestizqphc + "'");
				arrValoresParam.Add(obj.h6otrmaxilarphc == null ? null : "'" + obj.h6otrmaxilarphc + "'");
				arrValoresParam.Add(obj.h6otrglabelarphc == null ? null : "'" + obj.h6otrglabelarphc + "'");
				arrValoresParam.Add(obj.h6sistsensitivophc == null ? null : "'" + obj.h6sistsensitivophc + "'");
				arrValoresParam.Add(obj.h6dolorprofphc == null ? null : "'" + obj.h6dolorprofphc + "'");
				arrValoresParam.Add(obj.h6posicionphc == null ? null : "'" + obj.h6posicionphc + "'");
				arrValoresParam.Add(obj.h6vibracionphc == null ? null : "'" + obj.h6vibracionphc + "'");
				arrValoresParam.Add(obj.h6dospuntosphc == null ? null : "'" + obj.h6dospuntosphc + "'");
				arrValoresParam.Add(obj.h6extincionphc == null ? null : "'" + obj.h6extincionphc + "'");
				arrValoresParam.Add(obj.h6estereognosiaphc == null ? null : "'" + obj.h6estereognosiaphc + "'");
				arrValoresParam.Add(obj.h6grafestesiaphc == null ? null : "'" + obj.h6grafestesiaphc + "'");
				arrValoresParam.Add(obj.h6localizacionphc == null ? null : "'" + obj.h6localizacionphc + "'");
				arrValoresParam.Add(obj.h6imagen);
				arrValoresParam.Add(obj.h7rigideznucaphc == null ? null : "'" + obj.h7rigideznucaphc + "'");
				arrValoresParam.Add(obj.h7sigkerningphc == null ? null : "'" + obj.h7sigkerningphc + "'");
				arrValoresParam.Add(obj.h7fotofobiaphc == null ? null : "'" + obj.h7fotofobiaphc + "'");
				arrValoresParam.Add(obj.h7sigbrudzinskiphc == null ? null : "'" + obj.h7sigbrudzinskiphc + "'");
				arrValoresParam.Add(obj.h7hiperestesiaphc == null ? null : "'" + obj.h7hiperestesiaphc + "'");
				arrValoresParam.Add(obj.h7pulcarotideosphc == null ? null : "'" + obj.h7pulcarotideosphc + "'");
				arrValoresParam.Add(obj.h7pultemporalesphc == null ? null : "'" + obj.h7pultemporalesphc + "'");
				arrValoresParam.Add(obj.h7cabezaphc == null ? null : "'" + obj.h7cabezaphc + "'");
				arrValoresParam.Add(obj.h7cuellophc == null ? null : "'" + obj.h7cuellophc + "'");
				arrValoresParam.Add(obj.h7nerviosperifphc == null ? null : "'" + obj.h7nerviosperifphc + "'");
				arrValoresParam.Add(obj.h7colvertebralphc == null ? null : "'" + obj.h7colvertebralphc + "'");
				arrValoresParam.Add(obj.h7resumenphc == null ? null : "'" + obj.h7resumenphc + "'");
				arrValoresParam.Add(obj.h7diagnosticosphc == null ? null : "'" + obj.h7diagnosticosphc + "'");
				arrValoresParam.Add(obj.h7conductaphc == null ? null : "'" + obj.h7conductaphc + "'");
				arrValoresParam.Add(obj.h7tratamientophc == null ? null : "'" + obj.h7tratamientophc + "'");
				arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="strNombreSp" type="System.string">
		///     <para>
		/// 		 Nombre del Procedimiento a ejecutar sobre el SP
		///     </para>
		/// </param>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se va a ejecutar el SP de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor de registros afectados en el Procedimiento de la tabla pachistoriaclinica3
		/// </returns>
		public int EjecutarSpDesdeObjeto(string strNombreSp, EntPacHistoriaclinica3 obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrNombreParam = new ArrayList();
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				ArrayList arrValoresParam = new ArrayList();
				arrValoresParam.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValoresParam.Add(obj.h6reflejos01phc == null ? null : "'" + obj.h6reflejos01phc + "'");
				arrValoresParam.Add(obj.h6reflejos02phc == null ? null : "'" + obj.h6reflejos02phc + "'");
				arrValoresParam.Add(obj.h6reflejos03phc == null ? null : "'" + obj.h6reflejos03phc + "'");
				arrValoresParam.Add(obj.h6reflejos04phc == null ? null : "'" + obj.h6reflejos04phc + "'");
				arrValoresParam.Add(obj.h6reflejos05phc == null ? null : "'" + obj.h6reflejos05phc + "'");
				arrValoresParam.Add(obj.h6reflejos06phc == null ? null : "'" + obj.h6reflejos06phc + "'");
				arrValoresParam.Add(obj.h6reflejos07phc == null ? null : "'" + obj.h6reflejos07phc + "'");
				arrValoresParam.Add(obj.h6reflejos08phc == null ? null : "'" + obj.h6reflejos08phc + "'");
				arrValoresParam.Add(obj.h6reflejos09phc == null ? null : "'" + obj.h6reflejos09phc + "'");
				arrValoresParam.Add(obj.h6reflejos10phc == null ? null : "'" + obj.h6reflejos10phc + "'");
				arrValoresParam.Add(obj.h6miotbicderphc == null ? null : "'" + obj.h6miotbicderphc + "'");
				arrValoresParam.Add(obj.h6miotbicizqphc == null ? null : "'" + obj.h6miotbicizqphc + "'");
				arrValoresParam.Add(obj.h6miottriderphc == null ? null : "'" + obj.h6miottriderphc + "'");
				arrValoresParam.Add(obj.h6miottriizqphc == null ? null : "'" + obj.h6miottriizqphc + "'");
				arrValoresParam.Add(obj.h6miotestderphc == null ? null : "'" + obj.h6miotestderphc + "'");
				arrValoresParam.Add(obj.h6miotestizqphc == null ? null : "'" + obj.h6miotestizqphc + "'");
				arrValoresParam.Add(obj.h6miotpatderphc == null ? null : "'" + obj.h6miotpatderphc + "'");
				arrValoresParam.Add(obj.h6miotpatizqphc == null ? null : "'" + obj.h6miotpatizqphc + "'");
				arrValoresParam.Add(obj.h6miotaquderphc == null ? null : "'" + obj.h6miotaquderphc + "'");
				arrValoresParam.Add(obj.h6miotaquizqphc == null ? null : "'" + obj.h6miotaquizqphc + "'");
				arrValoresParam.Add(obj.h6patpladerphc == null ? null : "'" + obj.h6patpladerphc + "'");
				arrValoresParam.Add(obj.h6patplaizqphc == null ? null : "'" + obj.h6patplaizqphc + "'");
				arrValoresParam.Add(obj.h6pathofderphc == null ? null : "'" + obj.h6pathofderphc + "'");
				arrValoresParam.Add(obj.h6pathofizqphc == null ? null : "'" + obj.h6pathofizqphc + "'");
				arrValoresParam.Add(obj.h6pattroderphc == null ? null : "'" + obj.h6pattroderphc + "'");
				arrValoresParam.Add(obj.h6pattroizqphc == null ? null : "'" + obj.h6pattroizqphc + "'");
				arrValoresParam.Add(obj.h6patprederphc == null ? null : "'" + obj.h6patprederphc + "'");
				arrValoresParam.Add(obj.h6patpreizqphc == null ? null : "'" + obj.h6patpreizqphc + "'");
				arrValoresParam.Add(obj.h6patpmenderphc == null ? null : "'" + obj.h6patpmenderphc + "'");
				arrValoresParam.Add(obj.h6patpmenizqphc == null ? null : "'" + obj.h6patpmenizqphc + "'");
				arrValoresParam.Add(obj.h6supbicderphc == null ? null : "'" + obj.h6supbicderphc + "'");
				arrValoresParam.Add(obj.h6supbicizqphc == null ? null : "'" + obj.h6supbicizqphc + "'");
				arrValoresParam.Add(obj.h6suptriderphc == null ? null : "'" + obj.h6suptriderphc + "'");
				arrValoresParam.Add(obj.h6suptriizqphc == null ? null : "'" + obj.h6suptriizqphc + "'");
				arrValoresParam.Add(obj.h6supestderphc == null ? null : "'" + obj.h6supestderphc + "'");
				arrValoresParam.Add(obj.h6supestizqphc == null ? null : "'" + obj.h6supestizqphc + "'");
				arrValoresParam.Add(obj.h6otrmaxilarphc == null ? null : "'" + obj.h6otrmaxilarphc + "'");
				arrValoresParam.Add(obj.h6otrglabelarphc == null ? null : "'" + obj.h6otrglabelarphc + "'");
				arrValoresParam.Add(obj.h6sistsensitivophc == null ? null : "'" + obj.h6sistsensitivophc + "'");
				arrValoresParam.Add(obj.h6dolorprofphc == null ? null : "'" + obj.h6dolorprofphc + "'");
				arrValoresParam.Add(obj.h6posicionphc == null ? null : "'" + obj.h6posicionphc + "'");
				arrValoresParam.Add(obj.h6vibracionphc == null ? null : "'" + obj.h6vibracionphc + "'");
				arrValoresParam.Add(obj.h6dospuntosphc == null ? null : "'" + obj.h6dospuntosphc + "'");
				arrValoresParam.Add(obj.h6extincionphc == null ? null : "'" + obj.h6extincionphc + "'");
				arrValoresParam.Add(obj.h6estereognosiaphc == null ? null : "'" + obj.h6estereognosiaphc + "'");
				arrValoresParam.Add(obj.h6grafestesiaphc == null ? null : "'" + obj.h6grafestesiaphc + "'");
				arrValoresParam.Add(obj.h6localizacionphc == null ? null : "'" + obj.h6localizacionphc + "'");
				arrValoresParam.Add(obj.h6imagen);
				arrValoresParam.Add(obj.h7rigideznucaphc == null ? null : "'" + obj.h7rigideznucaphc + "'");
				arrValoresParam.Add(obj.h7sigkerningphc == null ? null : "'" + obj.h7sigkerningphc + "'");
				arrValoresParam.Add(obj.h7fotofobiaphc == null ? null : "'" + obj.h7fotofobiaphc + "'");
				arrValoresParam.Add(obj.h7sigbrudzinskiphc == null ? null : "'" + obj.h7sigbrudzinskiphc + "'");
				arrValoresParam.Add(obj.h7hiperestesiaphc == null ? null : "'" + obj.h7hiperestesiaphc + "'");
				arrValoresParam.Add(obj.h7pulcarotideosphc == null ? null : "'" + obj.h7pulcarotideosphc + "'");
				arrValoresParam.Add(obj.h7pultemporalesphc == null ? null : "'" + obj.h7pultemporalesphc + "'");
				arrValoresParam.Add(obj.h7cabezaphc == null ? null : "'" + obj.h7cabezaphc + "'");
				arrValoresParam.Add(obj.h7cuellophc == null ? null : "'" + obj.h7cuellophc + "'");
				arrValoresParam.Add(obj.h7nerviosperifphc == null ? null : "'" + obj.h7nerviosperifphc + "'");
				arrValoresParam.Add(obj.h7colvertebralphc == null ? null : "'" + obj.h7colvertebralphc + "'");
				arrValoresParam.Add(obj.h7resumenphc == null ? null : "'" + obj.h7resumenphc + "'");
				arrValoresParam.Add(obj.h7diagnosticosphc == null ? null : "'" + obj.h7diagnosticosphc + "'");
				arrValoresParam.Add(obj.h7conductaphc == null ? null : "'" + obj.h7conductaphc + "'");
				arrValoresParam.Add(obj.h7tratamientophc == null ? null : "'" + obj.h7tratamientophc + "'");
				arrValoresParam.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValoresParam.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValoresParam.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");
				arrValoresParam.Add(obj.feccre == null ? null : "'" + Convert.ToDateTime(obj.feccre).ToString(CParametros.ParFormatoFechaHora) + "'");
				arrValoresParam.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				arrValoresParam.Add(obj.fecmod == null ? null : "'" + Convert.ToDateTime(obj.fecmod).ToString(CParametros.ParFormatoFechaHora) + "'");

				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				return local.ExecStoreProcedure(strNombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region FuncionesAgregadas

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntPacHistoriaclinica3.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntPacHistoriaclinica3.Fields refField, EntPacHistoriaclinica3.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesCount(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] COUNT
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesCount(EntPacHistoriaclinica3.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("count(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntPacHistoriaclinica3.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntPacHistoriaclinica3.Fields refField, EntPacHistoriaclinica3.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMin(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MIN
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMin(EntPacHistoriaclinica3.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("min(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntPacHistoriaclinica3.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntPacHistoriaclinica3.Fields refField, EntPacHistoriaclinica3.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesMax(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] MAX
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesMax(EntPacHistoriaclinica3.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("max(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntPacHistoriaclinica3.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntPacHistoriaclinica3.Fields refField, EntPacHistoriaclinica3.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesSum(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] SUM
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesSum(EntPacHistoriaclinica3.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("sum(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntPacHistoriaclinica3.Fields refField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="whereField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Columna que va a filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="valueField" type="System.Object">
		///     <para>
		/// 		 Valor para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntPacHistoriaclinica3.Fields refField, EntPacHistoriaclinica3.Fields whereField, object valueField)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(whereField.ToString());
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(valueField.ToString());
				
				return FuncionesAvg(refField, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que devuelve el resultado de la funcion [SQL] AVG
		/// </summary>
		/// <param name="refField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo al que se va a aplicar la funcion
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <returns>
		/// 	Valor del Tipo EntPacHistoriaclinica3 que cumple con los filtros de los parametros
		/// </returns>
		public int FuncionesAvg(EntPacHistoriaclinica3.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add("avg(" + refField + ")");
				DataTable dtTemp = ObtenerDataTable(arrColumnas, arrColumnasWhere, arrValoresWhere);
				if (dtTemp.Rows.Count == 0)
					throw new Exception("La consulta no ha devuelto resultados.");
				if (dtTemp.Rows.Count > 1)
					throw new Exception("Se ha devuelto mas de una fila.");
				if (dtTemp.Rows[0][0] == null)
					return 0;
				if (dtTemp.Rows[0][0] == "")
					return 0;
				return int.Parse(dtTemp.Rows[0][0].ToString());
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs SP

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica3
		/// </returns>
		public bool Insert(EntPacHistoriaclinica3 obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("idppa");
				arrValoresParam.Add(null);
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrValoresParam.Add(obj.usucre);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacHistoriaclinica3.SpPh3Ins.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica3
		/// </returns>
		public bool Insert(EntPacHistoriaclinica3 obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add("idppa");
				arrValoresParam.Add("");
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrValoresParam.Add(obj.usucre);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacHistoriaclinica3.SpPh3Ins.ToString();
				return (local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans) > 0);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor que indica la cantidad de registros actualizados en pachistoriaclinica3
		/// </returns>
		public int Update(EntPacHistoriaclinica3 obj, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrValoresParam.Add(obj.h6reflejos01phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrValoresParam.Add(obj.h6reflejos02phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrValoresParam.Add(obj.h6reflejos03phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrValoresParam.Add(obj.h6reflejos04phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrValoresParam.Add(obj.h6reflejos05phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrValoresParam.Add(obj.h6reflejos06phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrValoresParam.Add(obj.h6reflejos07phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrValoresParam.Add(obj.h6reflejos08phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrValoresParam.Add(obj.h6reflejos09phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrValoresParam.Add(obj.h6reflejos10phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrValoresParam.Add(obj.h6miotbicderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrValoresParam.Add(obj.h6miotbicizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrValoresParam.Add(obj.h6miottriderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrValoresParam.Add(obj.h6miottriizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrValoresParam.Add(obj.h6miotestderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrValoresParam.Add(obj.h6miotestizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrValoresParam.Add(obj.h6miotpatderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrValoresParam.Add(obj.h6miotpatizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrValoresParam.Add(obj.h6miotaquderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrValoresParam.Add(obj.h6miotaquizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrValoresParam.Add(obj.h6patpladerphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrValoresParam.Add(obj.h6patplaizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrValoresParam.Add(obj.h6pathofderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrValoresParam.Add(obj.h6pathofizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrValoresParam.Add(obj.h6pattroderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrValoresParam.Add(obj.h6pattroizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrValoresParam.Add(obj.h6patprederphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrValoresParam.Add(obj.h6patpreizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrValoresParam.Add(obj.h6patpmenderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrValoresParam.Add(obj.h6patpmenizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrValoresParam.Add(obj.h6supbicderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrValoresParam.Add(obj.h6supbicizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrValoresParam.Add(obj.h6suptriderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrValoresParam.Add(obj.h6suptriizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrValoresParam.Add(obj.h6supestderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrValoresParam.Add(obj.h6supestizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrValoresParam.Add(obj.h6otrmaxilarphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrValoresParam.Add(obj.h6otrglabelarphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrValoresParam.Add(obj.h6sistsensitivophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrValoresParam.Add(obj.h6dolorprofphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrValoresParam.Add(obj.h6posicionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrValoresParam.Add(obj.h6vibracionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrValoresParam.Add(obj.h6dospuntosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrValoresParam.Add(obj.h6extincionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrValoresParam.Add(obj.h6estereognosiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrValoresParam.Add(obj.h6grafestesiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrValoresParam.Add(obj.h6localizacionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrValoresParam.Add(obj.h6imagen);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrValoresParam.Add(obj.h7rigideznucaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrValoresParam.Add(obj.h7sigkerningphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrValoresParam.Add(obj.h7fotofobiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrValoresParam.Add(obj.h7sigbrudzinskiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrValoresParam.Add(obj.h7hiperestesiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrValoresParam.Add(obj.h7pulcarotideosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrValoresParam.Add(obj.h7pultemporalesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrValoresParam.Add(obj.h7cabezaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrValoresParam.Add(obj.h7cuellophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrValoresParam.Add(obj.h7nerviosperifphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrValoresParam.Add(obj.h7colvertebralphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrValoresParam.Add(obj.h7resumenphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrValoresParam.Add(obj.h7diagnosticosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrValoresParam.Add(obj.h7conductaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrValoresParam.Add(obj.h7tratamientophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrValoresParam.Add(obj.apitransaccion);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrValoresParam.Add(obj.usumod);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacHistoriaclinica3.SpPh3Upd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica3
		/// </returns>
		public int Update(EntPacHistoriaclinica3 obj, ref CTrans localTrans, bool bValidar = true)
		{
			try
			{
				if (bValidar)
					if (!obj.IsValid())
						throw new Exception(obj.ValidationErrorsString());
				ArrayList arrNombreParam = new ArrayList();
				ArrayList arrValoresParam = new ArrayList();
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrValoresParam.Add(obj.idppa);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrValoresParam.Add(obj.h6reflejos01phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrValoresParam.Add(obj.h6reflejos02phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrValoresParam.Add(obj.h6reflejos03phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrValoresParam.Add(obj.h6reflejos04phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrValoresParam.Add(obj.h6reflejos05phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrValoresParam.Add(obj.h6reflejos06phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrValoresParam.Add(obj.h6reflejos07phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrValoresParam.Add(obj.h6reflejos08phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrValoresParam.Add(obj.h6reflejos09phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrValoresParam.Add(obj.h6reflejos10phc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrValoresParam.Add(obj.h6miotbicderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrValoresParam.Add(obj.h6miotbicizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrValoresParam.Add(obj.h6miottriderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrValoresParam.Add(obj.h6miottriizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrValoresParam.Add(obj.h6miotestderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrValoresParam.Add(obj.h6miotestizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrValoresParam.Add(obj.h6miotpatderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrValoresParam.Add(obj.h6miotpatizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrValoresParam.Add(obj.h6miotaquderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrValoresParam.Add(obj.h6miotaquizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrValoresParam.Add(obj.h6patpladerphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrValoresParam.Add(obj.h6patplaizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrValoresParam.Add(obj.h6pathofderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrValoresParam.Add(obj.h6pathofizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrValoresParam.Add(obj.h6pattroderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrValoresParam.Add(obj.h6pattroizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrValoresParam.Add(obj.h6patprederphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrValoresParam.Add(obj.h6patpreizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrValoresParam.Add(obj.h6patpmenderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrValoresParam.Add(obj.h6patpmenizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrValoresParam.Add(obj.h6supbicderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrValoresParam.Add(obj.h6supbicizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrValoresParam.Add(obj.h6suptriderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrValoresParam.Add(obj.h6suptriizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrValoresParam.Add(obj.h6supestderphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrValoresParam.Add(obj.h6supestizqphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrValoresParam.Add(obj.h6otrmaxilarphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrValoresParam.Add(obj.h6otrglabelarphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrValoresParam.Add(obj.h6sistsensitivophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrValoresParam.Add(obj.h6dolorprofphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrValoresParam.Add(obj.h6posicionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrValoresParam.Add(obj.h6vibracionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrValoresParam.Add(obj.h6dospuntosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrValoresParam.Add(obj.h6extincionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrValoresParam.Add(obj.h6estereognosiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrValoresParam.Add(obj.h6grafestesiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrValoresParam.Add(obj.h6localizacionphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrValoresParam.Add(obj.h6imagen);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrValoresParam.Add(obj.h7rigideznucaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrValoresParam.Add(obj.h7sigkerningphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrValoresParam.Add(obj.h7fotofobiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrValoresParam.Add(obj.h7sigbrudzinskiphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrValoresParam.Add(obj.h7hiperestesiaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrValoresParam.Add(obj.h7pulcarotideosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrValoresParam.Add(obj.h7pultemporalesphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrValoresParam.Add(obj.h7cabezaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrValoresParam.Add(obj.h7cuellophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrValoresParam.Add(obj.h7nerviosperifphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrValoresParam.Add(obj.h7colvertebralphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrValoresParam.Add(obj.h7resumenphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrValoresParam.Add(obj.h7diagnosticosphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrValoresParam.Add(obj.h7conductaphc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrValoresParam.Add(obj.h7tratamientophc);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrValoresParam.Add(obj.apitransaccion);
				
				arrNombreParam.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrValoresParam.Add(obj.usumod);
				
				
				//Llamamos al Procedmiento Almacenado
				CConn local = new CConn();
				string nombreSp = CListadoSP.PacHistoriaclinica3.SpPh3Upd.ToString();
				return local.ExecStoreProcedure(nombreSp, arrNombreParam, arrValoresParam, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica3
		/// </returns>
		public int Delete(EntPacHistoriaclinica3 obj)
		{
			throw new Exception("No existe el Procedimiento Almacenado SpPh3Del.");
		}

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica3
		/// </returns>
		public int Delete(EntPacHistoriaclinica3 obj, ref CTrans localTrans)
		{
			throw new Exception("No existe el Procedimiento Almacenado SpPh3Del.");
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica3
		/// </returns>
		public int InsertUpdate(EntPacHistoriaclinica3 obj)
		{
			try
			{
				bool esInsertar = true;
				
					esInsertar = (esInsertar && (obj.idppa == null));
				
				if (esInsertar)
					return Insert(obj) ? 1 : 0;
				else
					return Update(obj);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	Funcion que inserta o actualiza un registro un nuevo registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a insertar o actualizar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica3
		/// </returns>
		public int InsertUpdate(EntPacHistoriaclinica3 obj, ref CTrans localTrans)
		{
			try
			{
				bool esInsertar = false;
				
					esInsertar = (esInsertar && (obj.idppa == null));
				
				if (esInsertar)
					return Insert(obj, ref localTrans) ? 1 : 0;
				else
					return Update(obj, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 

		#region ABMs Query

		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica3
		/// </returns>
		public bool InsertQuery(EntPacHistoriaclinica3 obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.h6reflejos01phc == null ? null : "'" + obj.h6reflejos01phc + "'");
				arrValores.Add(obj.h6reflejos02phc == null ? null : "'" + obj.h6reflejos02phc + "'");
				arrValores.Add(obj.h6reflejos03phc == null ? null : "'" + obj.h6reflejos03phc + "'");
				arrValores.Add(obj.h6reflejos04phc == null ? null : "'" + obj.h6reflejos04phc + "'");
				arrValores.Add(obj.h6reflejos05phc == null ? null : "'" + obj.h6reflejos05phc + "'");
				arrValores.Add(obj.h6reflejos06phc == null ? null : "'" + obj.h6reflejos06phc + "'");
				arrValores.Add(obj.h6reflejos07phc == null ? null : "'" + obj.h6reflejos07phc + "'");
				arrValores.Add(obj.h6reflejos08phc == null ? null : "'" + obj.h6reflejos08phc + "'");
				arrValores.Add(obj.h6reflejos09phc == null ? null : "'" + obj.h6reflejos09phc + "'");
				arrValores.Add(obj.h6reflejos10phc == null ? null : "'" + obj.h6reflejos10phc + "'");
				arrValores.Add(obj.h6miotbicderphc == null ? null : "'" + obj.h6miotbicderphc + "'");
				arrValores.Add(obj.h6miotbicizqphc == null ? null : "'" + obj.h6miotbicizqphc + "'");
				arrValores.Add(obj.h6miottriderphc == null ? null : "'" + obj.h6miottriderphc + "'");
				arrValores.Add(obj.h6miottriizqphc == null ? null : "'" + obj.h6miottriizqphc + "'");
				arrValores.Add(obj.h6miotestderphc == null ? null : "'" + obj.h6miotestderphc + "'");
				arrValores.Add(obj.h6miotestizqphc == null ? null : "'" + obj.h6miotestizqphc + "'");
				arrValores.Add(obj.h6miotpatderphc == null ? null : "'" + obj.h6miotpatderphc + "'");
				arrValores.Add(obj.h6miotpatizqphc == null ? null : "'" + obj.h6miotpatizqphc + "'");
				arrValores.Add(obj.h6miotaquderphc == null ? null : "'" + obj.h6miotaquderphc + "'");
				arrValores.Add(obj.h6miotaquizqphc == null ? null : "'" + obj.h6miotaquizqphc + "'");
				arrValores.Add(obj.h6patpladerphc == null ? null : "'" + obj.h6patpladerphc + "'");
				arrValores.Add(obj.h6patplaizqphc == null ? null : "'" + obj.h6patplaizqphc + "'");
				arrValores.Add(obj.h6pathofderphc == null ? null : "'" + obj.h6pathofderphc + "'");
				arrValores.Add(obj.h6pathofizqphc == null ? null : "'" + obj.h6pathofizqphc + "'");
				arrValores.Add(obj.h6pattroderphc == null ? null : "'" + obj.h6pattroderphc + "'");
				arrValores.Add(obj.h6pattroizqphc == null ? null : "'" + obj.h6pattroizqphc + "'");
				arrValores.Add(obj.h6patprederphc == null ? null : "'" + obj.h6patprederphc + "'");
				arrValores.Add(obj.h6patpreizqphc == null ? null : "'" + obj.h6patpreizqphc + "'");
				arrValores.Add(obj.h6patpmenderphc == null ? null : "'" + obj.h6patpmenderphc + "'");
				arrValores.Add(obj.h6patpmenizqphc == null ? null : "'" + obj.h6patpmenizqphc + "'");
				arrValores.Add(obj.h6supbicderphc == null ? null : "'" + obj.h6supbicderphc + "'");
				arrValores.Add(obj.h6supbicizqphc == null ? null : "'" + obj.h6supbicizqphc + "'");
				arrValores.Add(obj.h6suptriderphc == null ? null : "'" + obj.h6suptriderphc + "'");
				arrValores.Add(obj.h6suptriizqphc == null ? null : "'" + obj.h6suptriizqphc + "'");
				arrValores.Add(obj.h6supestderphc == null ? null : "'" + obj.h6supestderphc + "'");
				arrValores.Add(obj.h6supestizqphc == null ? null : "'" + obj.h6supestizqphc + "'");
				arrValores.Add(obj.h6otrmaxilarphc == null ? null : "'" + obj.h6otrmaxilarphc + "'");
				arrValores.Add(obj.h6otrglabelarphc == null ? null : "'" + obj.h6otrglabelarphc + "'");
				arrValores.Add(obj.h6sistsensitivophc == null ? null : "'" + obj.h6sistsensitivophc + "'");
				arrValores.Add(obj.h6dolorprofphc == null ? null : "'" + obj.h6dolorprofphc + "'");
				arrValores.Add(obj.h6posicionphc == null ? null : "'" + obj.h6posicionphc + "'");
				arrValores.Add(obj.h6vibracionphc == null ? null : "'" + obj.h6vibracionphc + "'");
				arrValores.Add(obj.h6dospuntosphc == null ? null : "'" + obj.h6dospuntosphc + "'");
				arrValores.Add(obj.h6extincionphc == null ? null : "'" + obj.h6extincionphc + "'");
				arrValores.Add(obj.h6estereognosiaphc == null ? null : "'" + obj.h6estereognosiaphc + "'");
				arrValores.Add(obj.h6grafestesiaphc == null ? null : "'" + obj.h6grafestesiaphc + "'");
				arrValores.Add(obj.h6localizacionphc == null ? null : "'" + obj.h6localizacionphc + "'");
				arrValores.Add(obj.h6imagen);
				arrValores.Add(obj.h7rigideznucaphc == null ? null : "'" + obj.h7rigideznucaphc + "'");
				arrValores.Add(obj.h7sigkerningphc == null ? null : "'" + obj.h7sigkerningphc + "'");
				arrValores.Add(obj.h7fotofobiaphc == null ? null : "'" + obj.h7fotofobiaphc + "'");
				arrValores.Add(obj.h7sigbrudzinskiphc == null ? null : "'" + obj.h7sigbrudzinskiphc + "'");
				arrValores.Add(obj.h7hiperestesiaphc == null ? null : "'" + obj.h7hiperestesiaphc + "'");
				arrValores.Add(obj.h7pulcarotideosphc == null ? null : "'" + obj.h7pulcarotideosphc + "'");
				arrValores.Add(obj.h7pultemporalesphc == null ? null : "'" + obj.h7pultemporalesphc + "'");
				arrValores.Add(obj.h7cabezaphc == null ? null : "'" + obj.h7cabezaphc + "'");
				arrValores.Add(obj.h7cuellophc == null ? null : "'" + obj.h7cuellophc + "'");
				arrValores.Add(obj.h7nerviosperifphc == null ? null : "'" + obj.h7nerviosperifphc + "'");
				arrValores.Add(obj.h7colvertebralphc == null ? null : "'" + obj.h7colvertebralphc + "'");
				arrValores.Add(obj.h7resumenphc == null ? null : "'" + obj.h7resumenphc + "'");
				arrValores.Add(obj.h7diagnosticosphc == null ? null : "'" + obj.h7diagnosticosphc + "'");
				arrValores.Add(obj.h7conductaphc == null ? null : "'" + obj.h7conductaphc + "'");
				arrValores.Add(obj.h7tratamientophc == null ? null : "'" + obj.h7tratamientophc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrValores);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica3
		/// </returns>
		public bool InsertQuery(EntPacHistoriaclinica3 obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.h6reflejos01phc == null ? null : "'" + obj.h6reflejos01phc + "'");
				arrValores.Add(obj.h6reflejos02phc == null ? null : "'" + obj.h6reflejos02phc + "'");
				arrValores.Add(obj.h6reflejos03phc == null ? null : "'" + obj.h6reflejos03phc + "'");
				arrValores.Add(obj.h6reflejos04phc == null ? null : "'" + obj.h6reflejos04phc + "'");
				arrValores.Add(obj.h6reflejos05phc == null ? null : "'" + obj.h6reflejos05phc + "'");
				arrValores.Add(obj.h6reflejos06phc == null ? null : "'" + obj.h6reflejos06phc + "'");
				arrValores.Add(obj.h6reflejos07phc == null ? null : "'" + obj.h6reflejos07phc + "'");
				arrValores.Add(obj.h6reflejos08phc == null ? null : "'" + obj.h6reflejos08phc + "'");
				arrValores.Add(obj.h6reflejos09phc == null ? null : "'" + obj.h6reflejos09phc + "'");
				arrValores.Add(obj.h6reflejos10phc == null ? null : "'" + obj.h6reflejos10phc + "'");
				arrValores.Add(obj.h6miotbicderphc == null ? null : "'" + obj.h6miotbicderphc + "'");
				arrValores.Add(obj.h6miotbicizqphc == null ? null : "'" + obj.h6miotbicizqphc + "'");
				arrValores.Add(obj.h6miottriderphc == null ? null : "'" + obj.h6miottriderphc + "'");
				arrValores.Add(obj.h6miottriizqphc == null ? null : "'" + obj.h6miottriizqphc + "'");
				arrValores.Add(obj.h6miotestderphc == null ? null : "'" + obj.h6miotestderphc + "'");
				arrValores.Add(obj.h6miotestizqphc == null ? null : "'" + obj.h6miotestizqphc + "'");
				arrValores.Add(obj.h6miotpatderphc == null ? null : "'" + obj.h6miotpatderphc + "'");
				arrValores.Add(obj.h6miotpatizqphc == null ? null : "'" + obj.h6miotpatizqphc + "'");
				arrValores.Add(obj.h6miotaquderphc == null ? null : "'" + obj.h6miotaquderphc + "'");
				arrValores.Add(obj.h6miotaquizqphc == null ? null : "'" + obj.h6miotaquizqphc + "'");
				arrValores.Add(obj.h6patpladerphc == null ? null : "'" + obj.h6patpladerphc + "'");
				arrValores.Add(obj.h6patplaizqphc == null ? null : "'" + obj.h6patplaizqphc + "'");
				arrValores.Add(obj.h6pathofderphc == null ? null : "'" + obj.h6pathofderphc + "'");
				arrValores.Add(obj.h6pathofizqphc == null ? null : "'" + obj.h6pathofizqphc + "'");
				arrValores.Add(obj.h6pattroderphc == null ? null : "'" + obj.h6pattroderphc + "'");
				arrValores.Add(obj.h6pattroizqphc == null ? null : "'" + obj.h6pattroizqphc + "'");
				arrValores.Add(obj.h6patprederphc == null ? null : "'" + obj.h6patprederphc + "'");
				arrValores.Add(obj.h6patpreizqphc == null ? null : "'" + obj.h6patpreizqphc + "'");
				arrValores.Add(obj.h6patpmenderphc == null ? null : "'" + obj.h6patpmenderphc + "'");
				arrValores.Add(obj.h6patpmenizqphc == null ? null : "'" + obj.h6patpmenizqphc + "'");
				arrValores.Add(obj.h6supbicderphc == null ? null : "'" + obj.h6supbicderphc + "'");
				arrValores.Add(obj.h6supbicizqphc == null ? null : "'" + obj.h6supbicizqphc + "'");
				arrValores.Add(obj.h6suptriderphc == null ? null : "'" + obj.h6suptriderphc + "'");
				arrValores.Add(obj.h6suptriizqphc == null ? null : "'" + obj.h6suptriizqphc + "'");
				arrValores.Add(obj.h6supestderphc == null ? null : "'" + obj.h6supestderphc + "'");
				arrValores.Add(obj.h6supestizqphc == null ? null : "'" + obj.h6supestizqphc + "'");
				arrValores.Add(obj.h6otrmaxilarphc == null ? null : "'" + obj.h6otrmaxilarphc + "'");
				arrValores.Add(obj.h6otrglabelarphc == null ? null : "'" + obj.h6otrglabelarphc + "'");
				arrValores.Add(obj.h6sistsensitivophc == null ? null : "'" + obj.h6sistsensitivophc + "'");
				arrValores.Add(obj.h6dolorprofphc == null ? null : "'" + obj.h6dolorprofphc + "'");
				arrValores.Add(obj.h6posicionphc == null ? null : "'" + obj.h6posicionphc + "'");
				arrValores.Add(obj.h6vibracionphc == null ? null : "'" + obj.h6vibracionphc + "'");
				arrValores.Add(obj.h6dospuntosphc == null ? null : "'" + obj.h6dospuntosphc + "'");
				arrValores.Add(obj.h6extincionphc == null ? null : "'" + obj.h6extincionphc + "'");
				arrValores.Add(obj.h6estereognosiaphc == null ? null : "'" + obj.h6estereognosiaphc + "'");
				arrValores.Add(obj.h6grafestesiaphc == null ? null : "'" + obj.h6grafestesiaphc + "'");
				arrValores.Add(obj.h6localizacionphc == null ? null : "'" + obj.h6localizacionphc + "'");
				arrValores.Add(obj.h6imagen);
				arrValores.Add(obj.h7rigideznucaphc == null ? null : "'" + obj.h7rigideznucaphc + "'");
				arrValores.Add(obj.h7sigkerningphc == null ? null : "'" + obj.h7sigkerningphc + "'");
				arrValores.Add(obj.h7fotofobiaphc == null ? null : "'" + obj.h7fotofobiaphc + "'");
				arrValores.Add(obj.h7sigbrudzinskiphc == null ? null : "'" + obj.h7sigbrudzinskiphc + "'");
				arrValores.Add(obj.h7hiperestesiaphc == null ? null : "'" + obj.h7hiperestesiaphc + "'");
				arrValores.Add(obj.h7pulcarotideosphc == null ? null : "'" + obj.h7pulcarotideosphc + "'");
				arrValores.Add(obj.h7pultemporalesphc == null ? null : "'" + obj.h7pultemporalesphc + "'");
				arrValores.Add(obj.h7cabezaphc == null ? null : "'" + obj.h7cabezaphc + "'");
				arrValores.Add(obj.h7cuellophc == null ? null : "'" + obj.h7cuellophc + "'");
				arrValores.Add(obj.h7nerviosperifphc == null ? null : "'" + obj.h7nerviosperifphc + "'");
				arrValores.Add(obj.h7colvertebralphc == null ? null : "'" + obj.h7colvertebralphc + "'");
				arrValores.Add(obj.h7resumenphc == null ? null : "'" + obj.h7resumenphc + "'");
				arrValores.Add(obj.h7diagnosticosphc == null ? null : "'" + obj.h7diagnosticosphc + "'");
				arrValores.Add(obj.h7conductaphc == null ? null : "'" + obj.h7conductaphc + "'");
				arrValores.Add(obj.h7tratamientophc == null ? null : "'" + obj.h7tratamientophc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				return local.InsertBd(EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrValores, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica3
		/// </returns>
		public bool InsertQueryIdentity(EntPacHistoriaclinica3 obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.h6reflejos01phc == null ? null : "'" + obj.h6reflejos01phc + "'");
				arrValores.Add(obj.h6reflejos02phc == null ? null : "'" + obj.h6reflejos02phc + "'");
				arrValores.Add(obj.h6reflejos03phc == null ? null : "'" + obj.h6reflejos03phc + "'");
				arrValores.Add(obj.h6reflejos04phc == null ? null : "'" + obj.h6reflejos04phc + "'");
				arrValores.Add(obj.h6reflejos05phc == null ? null : "'" + obj.h6reflejos05phc + "'");
				arrValores.Add(obj.h6reflejos06phc == null ? null : "'" + obj.h6reflejos06phc + "'");
				arrValores.Add(obj.h6reflejos07phc == null ? null : "'" + obj.h6reflejos07phc + "'");
				arrValores.Add(obj.h6reflejos08phc == null ? null : "'" + obj.h6reflejos08phc + "'");
				arrValores.Add(obj.h6reflejos09phc == null ? null : "'" + obj.h6reflejos09phc + "'");
				arrValores.Add(obj.h6reflejos10phc == null ? null : "'" + obj.h6reflejos10phc + "'");
				arrValores.Add(obj.h6miotbicderphc == null ? null : "'" + obj.h6miotbicderphc + "'");
				arrValores.Add(obj.h6miotbicizqphc == null ? null : "'" + obj.h6miotbicizqphc + "'");
				arrValores.Add(obj.h6miottriderphc == null ? null : "'" + obj.h6miottriderphc + "'");
				arrValores.Add(obj.h6miottriizqphc == null ? null : "'" + obj.h6miottriizqphc + "'");
				arrValores.Add(obj.h6miotestderphc == null ? null : "'" + obj.h6miotestderphc + "'");
				arrValores.Add(obj.h6miotestizqphc == null ? null : "'" + obj.h6miotestizqphc + "'");
				arrValores.Add(obj.h6miotpatderphc == null ? null : "'" + obj.h6miotpatderphc + "'");
				arrValores.Add(obj.h6miotpatizqphc == null ? null : "'" + obj.h6miotpatizqphc + "'");
				arrValores.Add(obj.h6miotaquderphc == null ? null : "'" + obj.h6miotaquderphc + "'");
				arrValores.Add(obj.h6miotaquizqphc == null ? null : "'" + obj.h6miotaquizqphc + "'");
				arrValores.Add(obj.h6patpladerphc == null ? null : "'" + obj.h6patpladerphc + "'");
				arrValores.Add(obj.h6patplaizqphc == null ? null : "'" + obj.h6patplaizqphc + "'");
				arrValores.Add(obj.h6pathofderphc == null ? null : "'" + obj.h6pathofderphc + "'");
				arrValores.Add(obj.h6pathofizqphc == null ? null : "'" + obj.h6pathofizqphc + "'");
				arrValores.Add(obj.h6pattroderphc == null ? null : "'" + obj.h6pattroderphc + "'");
				arrValores.Add(obj.h6pattroizqphc == null ? null : "'" + obj.h6pattroizqphc + "'");
				arrValores.Add(obj.h6patprederphc == null ? null : "'" + obj.h6patprederphc + "'");
				arrValores.Add(obj.h6patpreizqphc == null ? null : "'" + obj.h6patpreizqphc + "'");
				arrValores.Add(obj.h6patpmenderphc == null ? null : "'" + obj.h6patpmenderphc + "'");
				arrValores.Add(obj.h6patpmenizqphc == null ? null : "'" + obj.h6patpmenizqphc + "'");
				arrValores.Add(obj.h6supbicderphc == null ? null : "'" + obj.h6supbicderphc + "'");
				arrValores.Add(obj.h6supbicizqphc == null ? null : "'" + obj.h6supbicizqphc + "'");
				arrValores.Add(obj.h6suptriderphc == null ? null : "'" + obj.h6suptriderphc + "'");
				arrValores.Add(obj.h6suptriizqphc == null ? null : "'" + obj.h6suptriizqphc + "'");
				arrValores.Add(obj.h6supestderphc == null ? null : "'" + obj.h6supestderphc + "'");
				arrValores.Add(obj.h6supestizqphc == null ? null : "'" + obj.h6supestizqphc + "'");
				arrValores.Add(obj.h6otrmaxilarphc == null ? null : "'" + obj.h6otrmaxilarphc + "'");
				arrValores.Add(obj.h6otrglabelarphc == null ? null : "'" + obj.h6otrglabelarphc + "'");
				arrValores.Add(obj.h6sistsensitivophc == null ? null : "'" + obj.h6sistsensitivophc + "'");
				arrValores.Add(obj.h6dolorprofphc == null ? null : "'" + obj.h6dolorprofphc + "'");
				arrValores.Add(obj.h6posicionphc == null ? null : "'" + obj.h6posicionphc + "'");
				arrValores.Add(obj.h6vibracionphc == null ? null : "'" + obj.h6vibracionphc + "'");
				arrValores.Add(obj.h6dospuntosphc == null ? null : "'" + obj.h6dospuntosphc + "'");
				arrValores.Add(obj.h6extincionphc == null ? null : "'" + obj.h6extincionphc + "'");
				arrValores.Add(obj.h6estereognosiaphc == null ? null : "'" + obj.h6estereognosiaphc + "'");
				arrValores.Add(obj.h6grafestesiaphc == null ? null : "'" + obj.h6grafestesiaphc + "'");
				arrValores.Add(obj.h6localizacionphc == null ? null : "'" + obj.h6localizacionphc + "'");
				arrValores.Add(obj.h6imagen);
				arrValores.Add(obj.h7rigideznucaphc == null ? null : "'" + obj.h7rigideznucaphc + "'");
				arrValores.Add(obj.h7sigkerningphc == null ? null : "'" + obj.h7sigkerningphc + "'");
				arrValores.Add(obj.h7fotofobiaphc == null ? null : "'" + obj.h7fotofobiaphc + "'");
				arrValores.Add(obj.h7sigbrudzinskiphc == null ? null : "'" + obj.h7sigbrudzinskiphc + "'");
				arrValores.Add(obj.h7hiperestesiaphc == null ? null : "'" + obj.h7hiperestesiaphc + "'");
				arrValores.Add(obj.h7pulcarotideosphc == null ? null : "'" + obj.h7pulcarotideosphc + "'");
				arrValores.Add(obj.h7pultemporalesphc == null ? null : "'" + obj.h7pultemporalesphc + "'");
				arrValores.Add(obj.h7cabezaphc == null ? null : "'" + obj.h7cabezaphc + "'");
				arrValores.Add(obj.h7cuellophc == null ? null : "'" + obj.h7cuellophc + "'");
				arrValores.Add(obj.h7nerviosperifphc == null ? null : "'" + obj.h7nerviosperifphc + "'");
				arrValores.Add(obj.h7colvertebralphc == null ? null : "'" + obj.h7colvertebralphc + "'");
				arrValores.Add(obj.h7resumenphc == null ? null : "'" + obj.h7resumenphc + "'");
				arrValores.Add(obj.h7diagnosticosphc == null ? null : "'" + obj.h7diagnosticosphc + "'");
				arrValores.Add(obj.h7conductaphc == null ? null : "'" + obj.h7conductaphc + "'");
				arrValores.Add(obj.h7tratamientophc == null ? null : "'" + obj.h7tratamientophc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad);
				obj.idppa = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	Funcion que inserta un nuevo registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a insertar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Valor TRUE or FALSE que indica el exito de la operacionpachistoriaclinica3
		/// </returns>
		public bool InsertQueryIdentity(EntPacHistoriaclinica3 obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				//arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				
				ArrayList arrValores = new ArrayList();
				//arrValores.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");
				arrValores.Add(obj.h6reflejos01phc == null ? null : "'" + obj.h6reflejos01phc + "'");
				arrValores.Add(obj.h6reflejos02phc == null ? null : "'" + obj.h6reflejos02phc + "'");
				arrValores.Add(obj.h6reflejos03phc == null ? null : "'" + obj.h6reflejos03phc + "'");
				arrValores.Add(obj.h6reflejos04phc == null ? null : "'" + obj.h6reflejos04phc + "'");
				arrValores.Add(obj.h6reflejos05phc == null ? null : "'" + obj.h6reflejos05phc + "'");
				arrValores.Add(obj.h6reflejos06phc == null ? null : "'" + obj.h6reflejos06phc + "'");
				arrValores.Add(obj.h6reflejos07phc == null ? null : "'" + obj.h6reflejos07phc + "'");
				arrValores.Add(obj.h6reflejos08phc == null ? null : "'" + obj.h6reflejos08phc + "'");
				arrValores.Add(obj.h6reflejos09phc == null ? null : "'" + obj.h6reflejos09phc + "'");
				arrValores.Add(obj.h6reflejos10phc == null ? null : "'" + obj.h6reflejos10phc + "'");
				arrValores.Add(obj.h6miotbicderphc == null ? null : "'" + obj.h6miotbicderphc + "'");
				arrValores.Add(obj.h6miotbicizqphc == null ? null : "'" + obj.h6miotbicizqphc + "'");
				arrValores.Add(obj.h6miottriderphc == null ? null : "'" + obj.h6miottriderphc + "'");
				arrValores.Add(obj.h6miottriizqphc == null ? null : "'" + obj.h6miottriizqphc + "'");
				arrValores.Add(obj.h6miotestderphc == null ? null : "'" + obj.h6miotestderphc + "'");
				arrValores.Add(obj.h6miotestizqphc == null ? null : "'" + obj.h6miotestizqphc + "'");
				arrValores.Add(obj.h6miotpatderphc == null ? null : "'" + obj.h6miotpatderphc + "'");
				arrValores.Add(obj.h6miotpatizqphc == null ? null : "'" + obj.h6miotpatizqphc + "'");
				arrValores.Add(obj.h6miotaquderphc == null ? null : "'" + obj.h6miotaquderphc + "'");
				arrValores.Add(obj.h6miotaquizqphc == null ? null : "'" + obj.h6miotaquizqphc + "'");
				arrValores.Add(obj.h6patpladerphc == null ? null : "'" + obj.h6patpladerphc + "'");
				arrValores.Add(obj.h6patplaizqphc == null ? null : "'" + obj.h6patplaizqphc + "'");
				arrValores.Add(obj.h6pathofderphc == null ? null : "'" + obj.h6pathofderphc + "'");
				arrValores.Add(obj.h6pathofizqphc == null ? null : "'" + obj.h6pathofizqphc + "'");
				arrValores.Add(obj.h6pattroderphc == null ? null : "'" + obj.h6pattroderphc + "'");
				arrValores.Add(obj.h6pattroizqphc == null ? null : "'" + obj.h6pattroizqphc + "'");
				arrValores.Add(obj.h6patprederphc == null ? null : "'" + obj.h6patprederphc + "'");
				arrValores.Add(obj.h6patpreizqphc == null ? null : "'" + obj.h6patpreizqphc + "'");
				arrValores.Add(obj.h6patpmenderphc == null ? null : "'" + obj.h6patpmenderphc + "'");
				arrValores.Add(obj.h6patpmenizqphc == null ? null : "'" + obj.h6patpmenizqphc + "'");
				arrValores.Add(obj.h6supbicderphc == null ? null : "'" + obj.h6supbicderphc + "'");
				arrValores.Add(obj.h6supbicizqphc == null ? null : "'" + obj.h6supbicizqphc + "'");
				arrValores.Add(obj.h6suptriderphc == null ? null : "'" + obj.h6suptriderphc + "'");
				arrValores.Add(obj.h6suptriizqphc == null ? null : "'" + obj.h6suptriizqphc + "'");
				arrValores.Add(obj.h6supestderphc == null ? null : "'" + obj.h6supestderphc + "'");
				arrValores.Add(obj.h6supestizqphc == null ? null : "'" + obj.h6supestizqphc + "'");
				arrValores.Add(obj.h6otrmaxilarphc == null ? null : "'" + obj.h6otrmaxilarphc + "'");
				arrValores.Add(obj.h6otrglabelarphc == null ? null : "'" + obj.h6otrglabelarphc + "'");
				arrValores.Add(obj.h6sistsensitivophc == null ? null : "'" + obj.h6sistsensitivophc + "'");
				arrValores.Add(obj.h6dolorprofphc == null ? null : "'" + obj.h6dolorprofphc + "'");
				arrValores.Add(obj.h6posicionphc == null ? null : "'" + obj.h6posicionphc + "'");
				arrValores.Add(obj.h6vibracionphc == null ? null : "'" + obj.h6vibracionphc + "'");
				arrValores.Add(obj.h6dospuntosphc == null ? null : "'" + obj.h6dospuntosphc + "'");
				arrValores.Add(obj.h6extincionphc == null ? null : "'" + obj.h6extincionphc + "'");
				arrValores.Add(obj.h6estereognosiaphc == null ? null : "'" + obj.h6estereognosiaphc + "'");
				arrValores.Add(obj.h6grafestesiaphc == null ? null : "'" + obj.h6grafestesiaphc + "'");
				arrValores.Add(obj.h6localizacionphc == null ? null : "'" + obj.h6localizacionphc + "'");
				arrValores.Add(obj.h6imagen);
				arrValores.Add(obj.h7rigideznucaphc == null ? null : "'" + obj.h7rigideznucaphc + "'");
				arrValores.Add(obj.h7sigkerningphc == null ? null : "'" + obj.h7sigkerningphc + "'");
				arrValores.Add(obj.h7fotofobiaphc == null ? null : "'" + obj.h7fotofobiaphc + "'");
				arrValores.Add(obj.h7sigbrudzinskiphc == null ? null : "'" + obj.h7sigbrudzinskiphc + "'");
				arrValores.Add(obj.h7hiperestesiaphc == null ? null : "'" + obj.h7hiperestesiaphc + "'");
				arrValores.Add(obj.h7pulcarotideosphc == null ? null : "'" + obj.h7pulcarotideosphc + "'");
				arrValores.Add(obj.h7pultemporalesphc == null ? null : "'" + obj.h7pultemporalesphc + "'");
				arrValores.Add(obj.h7cabezaphc == null ? null : "'" + obj.h7cabezaphc + "'");
				arrValores.Add(obj.h7cuellophc == null ? null : "'" + obj.h7cuellophc + "'");
				arrValores.Add(obj.h7nerviosperifphc == null ? null : "'" + obj.h7nerviosperifphc + "'");
				arrValores.Add(obj.h7colvertebralphc == null ? null : "'" + obj.h7colvertebralphc + "'");
				arrValores.Add(obj.h7resumenphc == null ? null : "'" + obj.h7resumenphc + "'");
				arrValores.Add(obj.h7diagnosticosphc == null ? null : "'" + obj.h7diagnosticosphc + "'");
				arrValores.Add(obj.h7conductaphc == null ? null : "'" + obj.h7conductaphc + "'");
				arrValores.Add(obj.h7tratamientophc == null ? null : "'" + obj.h7tratamientophc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.usucre == null ? null : "'" + obj.usucre + "'");

			
				CConn local = new CConn();
				int intIdentidad = -1;
				bool res = local.InsertBd(EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrValores, ref intIdentidad, ref localTrans);
				obj.idppa = intIdentidad;
				return res;
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpachistoriaclinica3
		/// </returns>
		public int UpdateQueryAll(EntPacHistoriaclinica3 obj)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.h6reflejos01phc == null ? null : "'" + obj.h6reflejos01phc + "'");
				arrValores.Add(obj.h6reflejos02phc == null ? null : "'" + obj.h6reflejos02phc + "'");
				arrValores.Add(obj.h6reflejos03phc == null ? null : "'" + obj.h6reflejos03phc + "'");
				arrValores.Add(obj.h6reflejos04phc == null ? null : "'" + obj.h6reflejos04phc + "'");
				arrValores.Add(obj.h6reflejos05phc == null ? null : "'" + obj.h6reflejos05phc + "'");
				arrValores.Add(obj.h6reflejos06phc == null ? null : "'" + obj.h6reflejos06phc + "'");
				arrValores.Add(obj.h6reflejos07phc == null ? null : "'" + obj.h6reflejos07phc + "'");
				arrValores.Add(obj.h6reflejos08phc == null ? null : "'" + obj.h6reflejos08phc + "'");
				arrValores.Add(obj.h6reflejos09phc == null ? null : "'" + obj.h6reflejos09phc + "'");
				arrValores.Add(obj.h6reflejos10phc == null ? null : "'" + obj.h6reflejos10phc + "'");
				arrValores.Add(obj.h6miotbicderphc == null ? null : "'" + obj.h6miotbicderphc + "'");
				arrValores.Add(obj.h6miotbicizqphc == null ? null : "'" + obj.h6miotbicizqphc + "'");
				arrValores.Add(obj.h6miottriderphc == null ? null : "'" + obj.h6miottriderphc + "'");
				arrValores.Add(obj.h6miottriizqphc == null ? null : "'" + obj.h6miottriizqphc + "'");
				arrValores.Add(obj.h6miotestderphc == null ? null : "'" + obj.h6miotestderphc + "'");
				arrValores.Add(obj.h6miotestizqphc == null ? null : "'" + obj.h6miotestizqphc + "'");
				arrValores.Add(obj.h6miotpatderphc == null ? null : "'" + obj.h6miotpatderphc + "'");
				arrValores.Add(obj.h6miotpatizqphc == null ? null : "'" + obj.h6miotpatizqphc + "'");
				arrValores.Add(obj.h6miotaquderphc == null ? null : "'" + obj.h6miotaquderphc + "'");
				arrValores.Add(obj.h6miotaquizqphc == null ? null : "'" + obj.h6miotaquizqphc + "'");
				arrValores.Add(obj.h6patpladerphc == null ? null : "'" + obj.h6patpladerphc + "'");
				arrValores.Add(obj.h6patplaizqphc == null ? null : "'" + obj.h6patplaizqphc + "'");
				arrValores.Add(obj.h6pathofderphc == null ? null : "'" + obj.h6pathofderphc + "'");
				arrValores.Add(obj.h6pathofizqphc == null ? null : "'" + obj.h6pathofizqphc + "'");
				arrValores.Add(obj.h6pattroderphc == null ? null : "'" + obj.h6pattroderphc + "'");
				arrValores.Add(obj.h6pattroizqphc == null ? null : "'" + obj.h6pattroizqphc + "'");
				arrValores.Add(obj.h6patprederphc == null ? null : "'" + obj.h6patprederphc + "'");
				arrValores.Add(obj.h6patpreizqphc == null ? null : "'" + obj.h6patpreizqphc + "'");
				arrValores.Add(obj.h6patpmenderphc == null ? null : "'" + obj.h6patpmenderphc + "'");
				arrValores.Add(obj.h6patpmenizqphc == null ? null : "'" + obj.h6patpmenizqphc + "'");
				arrValores.Add(obj.h6supbicderphc == null ? null : "'" + obj.h6supbicderphc + "'");
				arrValores.Add(obj.h6supbicizqphc == null ? null : "'" + obj.h6supbicizqphc + "'");
				arrValores.Add(obj.h6suptriderphc == null ? null : "'" + obj.h6suptriderphc + "'");
				arrValores.Add(obj.h6suptriizqphc == null ? null : "'" + obj.h6suptriizqphc + "'");
				arrValores.Add(obj.h6supestderphc == null ? null : "'" + obj.h6supestderphc + "'");
				arrValores.Add(obj.h6supestizqphc == null ? null : "'" + obj.h6supestizqphc + "'");
				arrValores.Add(obj.h6otrmaxilarphc == null ? null : "'" + obj.h6otrmaxilarphc + "'");
				arrValores.Add(obj.h6otrglabelarphc == null ? null : "'" + obj.h6otrglabelarphc + "'");
				arrValores.Add(obj.h6sistsensitivophc == null ? null : "'" + obj.h6sistsensitivophc + "'");
				arrValores.Add(obj.h6dolorprofphc == null ? null : "'" + obj.h6dolorprofphc + "'");
				arrValores.Add(obj.h6posicionphc == null ? null : "'" + obj.h6posicionphc + "'");
				arrValores.Add(obj.h6vibracionphc == null ? null : "'" + obj.h6vibracionphc + "'");
				arrValores.Add(obj.h6dospuntosphc == null ? null : "'" + obj.h6dospuntosphc + "'");
				arrValores.Add(obj.h6extincionphc == null ? null : "'" + obj.h6extincionphc + "'");
				arrValores.Add(obj.h6estereognosiaphc == null ? null : "'" + obj.h6estereognosiaphc + "'");
				arrValores.Add(obj.h6grafestesiaphc == null ? null : "'" + obj.h6grafestesiaphc + "'");
				arrValores.Add(obj.h6localizacionphc == null ? null : "'" + obj.h6localizacionphc + "'");
				arrValores.Add(obj.h6imagen);
				arrValores.Add(obj.h7rigideznucaphc == null ? null : "'" + obj.h7rigideznucaphc + "'");
				arrValores.Add(obj.h7sigkerningphc == null ? null : "'" + obj.h7sigkerningphc + "'");
				arrValores.Add(obj.h7fotofobiaphc == null ? null : "'" + obj.h7fotofobiaphc + "'");
				arrValores.Add(obj.h7sigbrudzinskiphc == null ? null : "'" + obj.h7sigbrudzinskiphc + "'");
				arrValores.Add(obj.h7hiperestesiaphc == null ? null : "'" + obj.h7hiperestesiaphc + "'");
				arrValores.Add(obj.h7pulcarotideosphc == null ? null : "'" + obj.h7pulcarotideosphc + "'");
				arrValores.Add(obj.h7pultemporalesphc == null ? null : "'" + obj.h7pultemporalesphc + "'");
				arrValores.Add(obj.h7cabezaphc == null ? null : "'" + obj.h7cabezaphc + "'");
				arrValores.Add(obj.h7cuellophc == null ? null : "'" + obj.h7cuellophc + "'");
				arrValores.Add(obj.h7nerviosperifphc == null ? null : "'" + obj.h7nerviosperifphc + "'");
				arrValores.Add(obj.h7colvertebralphc == null ? null : "'" + obj.h7colvertebralphc + "'");
				arrValores.Add(obj.h7resumenphc == null ? null : "'" + obj.h7resumenphc + "'");
				arrValores.Add(obj.h7diagnosticosphc == null ? null : "'" + obj.h7diagnosticosphc + "'");
				arrValores.Add(obj.h7conductaphc == null ? null : "'" + obj.h7conductaphc + "'");
				arrValores.Add(obj.h7tratamientophc == null ? null : "'" + obj.h7tratamientophc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pachistoriaclinica3 a partir de una clase del tipo epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQueryAll(EntPacHistoriaclinica3 obj, ref CTrans localTrans)
		{
			try
			{
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
			
				ArrayList arrValores = new ArrayList();
				arrValores.Add(obj.h6reflejos01phc == null ? null : "'" + obj.h6reflejos01phc + "'");
				arrValores.Add(obj.h6reflejos02phc == null ? null : "'" + obj.h6reflejos02phc + "'");
				arrValores.Add(obj.h6reflejos03phc == null ? null : "'" + obj.h6reflejos03phc + "'");
				arrValores.Add(obj.h6reflejos04phc == null ? null : "'" + obj.h6reflejos04phc + "'");
				arrValores.Add(obj.h6reflejos05phc == null ? null : "'" + obj.h6reflejos05phc + "'");
				arrValores.Add(obj.h6reflejos06phc == null ? null : "'" + obj.h6reflejos06phc + "'");
				arrValores.Add(obj.h6reflejos07phc == null ? null : "'" + obj.h6reflejos07phc + "'");
				arrValores.Add(obj.h6reflejos08phc == null ? null : "'" + obj.h6reflejos08phc + "'");
				arrValores.Add(obj.h6reflejos09phc == null ? null : "'" + obj.h6reflejos09phc + "'");
				arrValores.Add(obj.h6reflejos10phc == null ? null : "'" + obj.h6reflejos10phc + "'");
				arrValores.Add(obj.h6miotbicderphc == null ? null : "'" + obj.h6miotbicderphc + "'");
				arrValores.Add(obj.h6miotbicizqphc == null ? null : "'" + obj.h6miotbicizqphc + "'");
				arrValores.Add(obj.h6miottriderphc == null ? null : "'" + obj.h6miottriderphc + "'");
				arrValores.Add(obj.h6miottriizqphc == null ? null : "'" + obj.h6miottriizqphc + "'");
				arrValores.Add(obj.h6miotestderphc == null ? null : "'" + obj.h6miotestderphc + "'");
				arrValores.Add(obj.h6miotestizqphc == null ? null : "'" + obj.h6miotestizqphc + "'");
				arrValores.Add(obj.h6miotpatderphc == null ? null : "'" + obj.h6miotpatderphc + "'");
				arrValores.Add(obj.h6miotpatizqphc == null ? null : "'" + obj.h6miotpatizqphc + "'");
				arrValores.Add(obj.h6miotaquderphc == null ? null : "'" + obj.h6miotaquderphc + "'");
				arrValores.Add(obj.h6miotaquizqphc == null ? null : "'" + obj.h6miotaquizqphc + "'");
				arrValores.Add(obj.h6patpladerphc == null ? null : "'" + obj.h6patpladerphc + "'");
				arrValores.Add(obj.h6patplaizqphc == null ? null : "'" + obj.h6patplaizqphc + "'");
				arrValores.Add(obj.h6pathofderphc == null ? null : "'" + obj.h6pathofderphc + "'");
				arrValores.Add(obj.h6pathofizqphc == null ? null : "'" + obj.h6pathofizqphc + "'");
				arrValores.Add(obj.h6pattroderphc == null ? null : "'" + obj.h6pattroderphc + "'");
				arrValores.Add(obj.h6pattroizqphc == null ? null : "'" + obj.h6pattroizqphc + "'");
				arrValores.Add(obj.h6patprederphc == null ? null : "'" + obj.h6patprederphc + "'");
				arrValores.Add(obj.h6patpreizqphc == null ? null : "'" + obj.h6patpreizqphc + "'");
				arrValores.Add(obj.h6patpmenderphc == null ? null : "'" + obj.h6patpmenderphc + "'");
				arrValores.Add(obj.h6patpmenizqphc == null ? null : "'" + obj.h6patpmenizqphc + "'");
				arrValores.Add(obj.h6supbicderphc == null ? null : "'" + obj.h6supbicderphc + "'");
				arrValores.Add(obj.h6supbicizqphc == null ? null : "'" + obj.h6supbicizqphc + "'");
				arrValores.Add(obj.h6suptriderphc == null ? null : "'" + obj.h6suptriderphc + "'");
				arrValores.Add(obj.h6suptriizqphc == null ? null : "'" + obj.h6suptriizqphc + "'");
				arrValores.Add(obj.h6supestderphc == null ? null : "'" + obj.h6supestderphc + "'");
				arrValores.Add(obj.h6supestizqphc == null ? null : "'" + obj.h6supestizqphc + "'");
				arrValores.Add(obj.h6otrmaxilarphc == null ? null : "'" + obj.h6otrmaxilarphc + "'");
				arrValores.Add(obj.h6otrglabelarphc == null ? null : "'" + obj.h6otrglabelarphc + "'");
				arrValores.Add(obj.h6sistsensitivophc == null ? null : "'" + obj.h6sistsensitivophc + "'");
				arrValores.Add(obj.h6dolorprofphc == null ? null : "'" + obj.h6dolorprofphc + "'");
				arrValores.Add(obj.h6posicionphc == null ? null : "'" + obj.h6posicionphc + "'");
				arrValores.Add(obj.h6vibracionphc == null ? null : "'" + obj.h6vibracionphc + "'");
				arrValores.Add(obj.h6dospuntosphc == null ? null : "'" + obj.h6dospuntosphc + "'");
				arrValores.Add(obj.h6extincionphc == null ? null : "'" + obj.h6extincionphc + "'");
				arrValores.Add(obj.h6estereognosiaphc == null ? null : "'" + obj.h6estereognosiaphc + "'");
				arrValores.Add(obj.h6grafestesiaphc == null ? null : "'" + obj.h6grafestesiaphc + "'");
				arrValores.Add(obj.h6localizacionphc == null ? null : "'" + obj.h6localizacionphc + "'");
				arrValores.Add(obj.h6imagen);
				arrValores.Add(obj.h7rigideznucaphc == null ? null : "'" + obj.h7rigideznucaphc + "'");
				arrValores.Add(obj.h7sigkerningphc == null ? null : "'" + obj.h7sigkerningphc + "'");
				arrValores.Add(obj.h7fotofobiaphc == null ? null : "'" + obj.h7fotofobiaphc + "'");
				arrValores.Add(obj.h7sigbrudzinskiphc == null ? null : "'" + obj.h7sigbrudzinskiphc + "'");
				arrValores.Add(obj.h7hiperestesiaphc == null ? null : "'" + obj.h7hiperestesiaphc + "'");
				arrValores.Add(obj.h7pulcarotideosphc == null ? null : "'" + obj.h7pulcarotideosphc + "'");
				arrValores.Add(obj.h7pultemporalesphc == null ? null : "'" + obj.h7pultemporalesphc + "'");
				arrValores.Add(obj.h7cabezaphc == null ? null : "'" + obj.h7cabezaphc + "'");
				arrValores.Add(obj.h7cuellophc == null ? null : "'" + obj.h7cuellophc + "'");
				arrValores.Add(obj.h7nerviosperifphc == null ? null : "'" + obj.h7nerviosperifphc + "'");
				arrValores.Add(obj.h7colvertebralphc == null ? null : "'" + obj.h7colvertebralphc + "'");
				arrValores.Add(obj.h7resumenphc == null ? null : "'" + obj.h7resumenphc + "'");
				arrValores.Add(obj.h7diagnosticosphc == null ? null : "'" + obj.h7diagnosticosphc + "'");
				arrValores.Add(obj.h7conductaphc == null ? null : "'" + obj.h7conductaphc + "'");
				arrValores.Add(obj.h7tratamientophc == null ? null : "'" + obj.h7tratamientophc + "'");
				arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pachistoriaclinica3 a partir de una clase del tipo Epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpachistoriaclinica3
		/// </returns>
		public int UpdateQuery(EntPacHistoriaclinica3 obj)
		{
			try
			{
				//Obtenemos el Objeto original
				EntPacHistoriaclinica3 objOriginal = this.ObtenerObjeto(obj.idppa);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.h6reflejos01phc != objOriginal.h6reflejos01phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
					arrValores.Add(obj.h6reflejos01phc == null ? null : "'" + obj.h6reflejos01phc + "'");
				}
				if(obj.h6reflejos02phc != objOriginal.h6reflejos02phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
					arrValores.Add(obj.h6reflejos02phc == null ? null : "'" + obj.h6reflejos02phc + "'");
				}
				if(obj.h6reflejos03phc != objOriginal.h6reflejos03phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
					arrValores.Add(obj.h6reflejos03phc == null ? null : "'" + obj.h6reflejos03phc + "'");
				}
				if(obj.h6reflejos04phc != objOriginal.h6reflejos04phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
					arrValores.Add(obj.h6reflejos04phc == null ? null : "'" + obj.h6reflejos04phc + "'");
				}
				if(obj.h6reflejos05phc != objOriginal.h6reflejos05phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
					arrValores.Add(obj.h6reflejos05phc == null ? null : "'" + obj.h6reflejos05phc + "'");
				}
				if(obj.h6reflejos06phc != objOriginal.h6reflejos06phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
					arrValores.Add(obj.h6reflejos06phc == null ? null : "'" + obj.h6reflejos06phc + "'");
				}
				if(obj.h6reflejos07phc != objOriginal.h6reflejos07phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
					arrValores.Add(obj.h6reflejos07phc == null ? null : "'" + obj.h6reflejos07phc + "'");
				}
				if(obj.h6reflejos08phc != objOriginal.h6reflejos08phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
					arrValores.Add(obj.h6reflejos08phc == null ? null : "'" + obj.h6reflejos08phc + "'");
				}
				if(obj.h6reflejos09phc != objOriginal.h6reflejos09phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
					arrValores.Add(obj.h6reflejos09phc == null ? null : "'" + obj.h6reflejos09phc + "'");
				}
				if(obj.h6reflejos10phc != objOriginal.h6reflejos10phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
					arrValores.Add(obj.h6reflejos10phc == null ? null : "'" + obj.h6reflejos10phc + "'");
				}
				if(obj.h6miotbicderphc != objOriginal.h6miotbicderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
					arrValores.Add(obj.h6miotbicderphc == null ? null : "'" + obj.h6miotbicderphc + "'");
				}
				if(obj.h6miotbicizqphc != objOriginal.h6miotbicizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
					arrValores.Add(obj.h6miotbicizqphc == null ? null : "'" + obj.h6miotbicizqphc + "'");
				}
				if(obj.h6miottriderphc != objOriginal.h6miottriderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
					arrValores.Add(obj.h6miottriderphc == null ? null : "'" + obj.h6miottriderphc + "'");
				}
				if(obj.h6miottriizqphc != objOriginal.h6miottriizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
					arrValores.Add(obj.h6miottriizqphc == null ? null : "'" + obj.h6miottriizqphc + "'");
				}
				if(obj.h6miotestderphc != objOriginal.h6miotestderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
					arrValores.Add(obj.h6miotestderphc == null ? null : "'" + obj.h6miotestderphc + "'");
				}
				if(obj.h6miotestizqphc != objOriginal.h6miotestizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
					arrValores.Add(obj.h6miotestizqphc == null ? null : "'" + obj.h6miotestizqphc + "'");
				}
				if(obj.h6miotpatderphc != objOriginal.h6miotpatderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
					arrValores.Add(obj.h6miotpatderphc == null ? null : "'" + obj.h6miotpatderphc + "'");
				}
				if(obj.h6miotpatizqphc != objOriginal.h6miotpatizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
					arrValores.Add(obj.h6miotpatizqphc == null ? null : "'" + obj.h6miotpatizqphc + "'");
				}
				if(obj.h6miotaquderphc != objOriginal.h6miotaquderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
					arrValores.Add(obj.h6miotaquderphc == null ? null : "'" + obj.h6miotaquderphc + "'");
				}
				if(obj.h6miotaquizqphc != objOriginal.h6miotaquizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
					arrValores.Add(obj.h6miotaquizqphc == null ? null : "'" + obj.h6miotaquizqphc + "'");
				}
				if(obj.h6patpladerphc != objOriginal.h6patpladerphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
					arrValores.Add(obj.h6patpladerphc == null ? null : "'" + obj.h6patpladerphc + "'");
				}
				if(obj.h6patplaizqphc != objOriginal.h6patplaizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
					arrValores.Add(obj.h6patplaizqphc == null ? null : "'" + obj.h6patplaizqphc + "'");
				}
				if(obj.h6pathofderphc != objOriginal.h6pathofderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
					arrValores.Add(obj.h6pathofderphc == null ? null : "'" + obj.h6pathofderphc + "'");
				}
				if(obj.h6pathofizqphc != objOriginal.h6pathofizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
					arrValores.Add(obj.h6pathofizqphc == null ? null : "'" + obj.h6pathofizqphc + "'");
				}
				if(obj.h6pattroderphc != objOriginal.h6pattroderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
					arrValores.Add(obj.h6pattroderphc == null ? null : "'" + obj.h6pattroderphc + "'");
				}
				if(obj.h6pattroizqphc != objOriginal.h6pattroizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
					arrValores.Add(obj.h6pattroizqphc == null ? null : "'" + obj.h6pattroizqphc + "'");
				}
				if(obj.h6patprederphc != objOriginal.h6patprederphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
					arrValores.Add(obj.h6patprederphc == null ? null : "'" + obj.h6patprederphc + "'");
				}
				if(obj.h6patpreizqphc != objOriginal.h6patpreizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
					arrValores.Add(obj.h6patpreizqphc == null ? null : "'" + obj.h6patpreizqphc + "'");
				}
				if(obj.h6patpmenderphc != objOriginal.h6patpmenderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
					arrValores.Add(obj.h6patpmenderphc == null ? null : "'" + obj.h6patpmenderphc + "'");
				}
				if(obj.h6patpmenizqphc != objOriginal.h6patpmenizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
					arrValores.Add(obj.h6patpmenizqphc == null ? null : "'" + obj.h6patpmenizqphc + "'");
				}
				if(obj.h6supbicderphc != objOriginal.h6supbicderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
					arrValores.Add(obj.h6supbicderphc == null ? null : "'" + obj.h6supbicderphc + "'");
				}
				if(obj.h6supbicizqphc != objOriginal.h6supbicizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
					arrValores.Add(obj.h6supbicizqphc == null ? null : "'" + obj.h6supbicizqphc + "'");
				}
				if(obj.h6suptriderphc != objOriginal.h6suptriderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
					arrValores.Add(obj.h6suptriderphc == null ? null : "'" + obj.h6suptriderphc + "'");
				}
				if(obj.h6suptriizqphc != objOriginal.h6suptriizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
					arrValores.Add(obj.h6suptriizqphc == null ? null : "'" + obj.h6suptriizqphc + "'");
				}
				if(obj.h6supestderphc != objOriginal.h6supestderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
					arrValores.Add(obj.h6supestderphc == null ? null : "'" + obj.h6supestderphc + "'");
				}
				if(obj.h6supestizqphc != objOriginal.h6supestizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
					arrValores.Add(obj.h6supestizqphc == null ? null : "'" + obj.h6supestizqphc + "'");
				}
				if(obj.h6otrmaxilarphc != objOriginal.h6otrmaxilarphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
					arrValores.Add(obj.h6otrmaxilarphc == null ? null : "'" + obj.h6otrmaxilarphc + "'");
				}
				if(obj.h6otrglabelarphc != objOriginal.h6otrglabelarphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
					arrValores.Add(obj.h6otrglabelarphc == null ? null : "'" + obj.h6otrglabelarphc + "'");
				}
				if(obj.h6sistsensitivophc != objOriginal.h6sistsensitivophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
					arrValores.Add(obj.h6sistsensitivophc == null ? null : "'" + obj.h6sistsensitivophc + "'");
				}
				if(obj.h6dolorprofphc != objOriginal.h6dolorprofphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
					arrValores.Add(obj.h6dolorprofphc == null ? null : "'" + obj.h6dolorprofphc + "'");
				}
				if(obj.h6posicionphc != objOriginal.h6posicionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
					arrValores.Add(obj.h6posicionphc == null ? null : "'" + obj.h6posicionphc + "'");
				}
				if(obj.h6vibracionphc != objOriginal.h6vibracionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
					arrValores.Add(obj.h6vibracionphc == null ? null : "'" + obj.h6vibracionphc + "'");
				}
				if(obj.h6dospuntosphc != objOriginal.h6dospuntosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
					arrValores.Add(obj.h6dospuntosphc == null ? null : "'" + obj.h6dospuntosphc + "'");
				}
				if(obj.h6extincionphc != objOriginal.h6extincionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
					arrValores.Add(obj.h6extincionphc == null ? null : "'" + obj.h6extincionphc + "'");
				}
				if(obj.h6estereognosiaphc != objOriginal.h6estereognosiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
					arrValores.Add(obj.h6estereognosiaphc == null ? null : "'" + obj.h6estereognosiaphc + "'");
				}
				if(obj.h6grafestesiaphc != objOriginal.h6grafestesiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
					arrValores.Add(obj.h6grafestesiaphc == null ? null : "'" + obj.h6grafestesiaphc + "'");
				}
				if(obj.h6localizacionphc != objOriginal.h6localizacionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
					arrValores.Add(obj.h6localizacionphc == null ? null : "'" + obj.h6localizacionphc + "'");
				}
				if(obj.h6imagen != objOriginal.h6imagen )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
					arrValores.Add(obj.h6imagen);
				}
				if(obj.h7rigideznucaphc != objOriginal.h7rigideznucaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
					arrValores.Add(obj.h7rigideznucaphc == null ? null : "'" + obj.h7rigideznucaphc + "'");
				}
				if(obj.h7sigkerningphc != objOriginal.h7sigkerningphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
					arrValores.Add(obj.h7sigkerningphc == null ? null : "'" + obj.h7sigkerningphc + "'");
				}
				if(obj.h7fotofobiaphc != objOriginal.h7fotofobiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
					arrValores.Add(obj.h7fotofobiaphc == null ? null : "'" + obj.h7fotofobiaphc + "'");
				}
				if(obj.h7sigbrudzinskiphc != objOriginal.h7sigbrudzinskiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
					arrValores.Add(obj.h7sigbrudzinskiphc == null ? null : "'" + obj.h7sigbrudzinskiphc + "'");
				}
				if(obj.h7hiperestesiaphc != objOriginal.h7hiperestesiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
					arrValores.Add(obj.h7hiperestesiaphc == null ? null : "'" + obj.h7hiperestesiaphc + "'");
				}
				if(obj.h7pulcarotideosphc != objOriginal.h7pulcarotideosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
					arrValores.Add(obj.h7pulcarotideosphc == null ? null : "'" + obj.h7pulcarotideosphc + "'");
				}
				if(obj.h7pultemporalesphc != objOriginal.h7pultemporalesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
					arrValores.Add(obj.h7pultemporalesphc == null ? null : "'" + obj.h7pultemporalesphc + "'");
				}
				if(obj.h7cabezaphc != objOriginal.h7cabezaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
					arrValores.Add(obj.h7cabezaphc == null ? null : "'" + obj.h7cabezaphc + "'");
				}
				if(obj.h7cuellophc != objOriginal.h7cuellophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
					arrValores.Add(obj.h7cuellophc == null ? null : "'" + obj.h7cuellophc + "'");
				}
				if(obj.h7nerviosperifphc != objOriginal.h7nerviosperifphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
					arrValores.Add(obj.h7nerviosperifphc == null ? null : "'" + obj.h7nerviosperifphc + "'");
				}
				if(obj.h7colvertebralphc != objOriginal.h7colvertebralphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
					arrValores.Add(obj.h7colvertebralphc == null ? null : "'" + obj.h7colvertebralphc + "'");
				}
				if(obj.h7resumenphc != objOriginal.h7resumenphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
					arrValores.Add(obj.h7resumenphc == null ? null : "'" + obj.h7resumenphc + "'");
				}
				if(obj.h7diagnosticosphc != objOriginal.h7diagnosticosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
					arrValores.Add(obj.h7diagnosticosphc == null ? null : "'" + obj.h7diagnosticosphc + "'");
				}
				if(obj.h7conductaphc != objOriginal.h7conductaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
					arrValores.Add(obj.h7conductaphc == null ? null : "'" + obj.h7conductaphc + "'");
				}
				if(obj.h7tratamientophc != objOriginal.h7tratamientophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
					arrValores.Add(obj.h7tratamientophc == null ? null : "'" + obj.h7tratamientophc + "'");
				}
				if(obj.apiestado != objOriginal.apiestado )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
					arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				}
				if(obj.apitransaccion != objOriginal.apitransaccion )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
					arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				}
				if(obj.usumod != objOriginal.usumod )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
					arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que actualiza un registro en la tabla pachistoriaclinica3 a partir de una clase del tipo epachistoriaclinica3
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a actualizar los valores a la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacion
		/// </returns>
		public int UpdateQuery(EntPacHistoriaclinica3 obj, ref CTrans localTrans)
		{
			try
			{
				//Obtenemos el Objeto original
				EntPacHistoriaclinica3 objOriginal = this.ObtenerObjeto(obj.idppa, ref localTrans);
				
				if (!obj.IsValid())
				{
					throw new Exception(obj.ValidationErrorsString());
				}
				
				ArrayList arrColumnas = new ArrayList();
				ArrayList arrValores = new ArrayList();
				
				if(obj.h6reflejos01phc != objOriginal.h6reflejos01phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
					arrValores.Add(obj.h6reflejos01phc == null ? null : "'" + obj.h6reflejos01phc + "'");
				}
				if(obj.h6reflejos02phc != objOriginal.h6reflejos02phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
					arrValores.Add(obj.h6reflejos02phc == null ? null : "'" + obj.h6reflejos02phc + "'");
				}
				if(obj.h6reflejos03phc != objOriginal.h6reflejos03phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
					arrValores.Add(obj.h6reflejos03phc == null ? null : "'" + obj.h6reflejos03phc + "'");
				}
				if(obj.h6reflejos04phc != objOriginal.h6reflejos04phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
					arrValores.Add(obj.h6reflejos04phc == null ? null : "'" + obj.h6reflejos04phc + "'");
				}
				if(obj.h6reflejos05phc != objOriginal.h6reflejos05phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
					arrValores.Add(obj.h6reflejos05phc == null ? null : "'" + obj.h6reflejos05phc + "'");
				}
				if(obj.h6reflejos06phc != objOriginal.h6reflejos06phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
					arrValores.Add(obj.h6reflejos06phc == null ? null : "'" + obj.h6reflejos06phc + "'");
				}
				if(obj.h6reflejos07phc != objOriginal.h6reflejos07phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
					arrValores.Add(obj.h6reflejos07phc == null ? null : "'" + obj.h6reflejos07phc + "'");
				}
				if(obj.h6reflejos08phc != objOriginal.h6reflejos08phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
					arrValores.Add(obj.h6reflejos08phc == null ? null : "'" + obj.h6reflejos08phc + "'");
				}
				if(obj.h6reflejos09phc != objOriginal.h6reflejos09phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
					arrValores.Add(obj.h6reflejos09phc == null ? null : "'" + obj.h6reflejos09phc + "'");
				}
				if(obj.h6reflejos10phc != objOriginal.h6reflejos10phc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
					arrValores.Add(obj.h6reflejos10phc == null ? null : "'" + obj.h6reflejos10phc + "'");
				}
				if(obj.h6miotbicderphc != objOriginal.h6miotbicderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
					arrValores.Add(obj.h6miotbicderphc == null ? null : "'" + obj.h6miotbicderphc + "'");
				}
				if(obj.h6miotbicizqphc != objOriginal.h6miotbicizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
					arrValores.Add(obj.h6miotbicizqphc == null ? null : "'" + obj.h6miotbicizqphc + "'");
				}
				if(obj.h6miottriderphc != objOriginal.h6miottriderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
					arrValores.Add(obj.h6miottriderphc == null ? null : "'" + obj.h6miottriderphc + "'");
				}
				if(obj.h6miottriizqphc != objOriginal.h6miottriizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
					arrValores.Add(obj.h6miottriizqphc == null ? null : "'" + obj.h6miottriizqphc + "'");
				}
				if(obj.h6miotestderphc != objOriginal.h6miotestderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
					arrValores.Add(obj.h6miotestderphc == null ? null : "'" + obj.h6miotestderphc + "'");
				}
				if(obj.h6miotestizqphc != objOriginal.h6miotestizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
					arrValores.Add(obj.h6miotestizqphc == null ? null : "'" + obj.h6miotestizqphc + "'");
				}
				if(obj.h6miotpatderphc != objOriginal.h6miotpatderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
					arrValores.Add(obj.h6miotpatderphc == null ? null : "'" + obj.h6miotpatderphc + "'");
				}
				if(obj.h6miotpatizqphc != objOriginal.h6miotpatizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
					arrValores.Add(obj.h6miotpatizqphc == null ? null : "'" + obj.h6miotpatizqphc + "'");
				}
				if(obj.h6miotaquderphc != objOriginal.h6miotaquderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
					arrValores.Add(obj.h6miotaquderphc == null ? null : "'" + obj.h6miotaquderphc + "'");
				}
				if(obj.h6miotaquizqphc != objOriginal.h6miotaquizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
					arrValores.Add(obj.h6miotaquizqphc == null ? null : "'" + obj.h6miotaquizqphc + "'");
				}
				if(obj.h6patpladerphc != objOriginal.h6patpladerphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
					arrValores.Add(obj.h6patpladerphc == null ? null : "'" + obj.h6patpladerphc + "'");
				}
				if(obj.h6patplaizqphc != objOriginal.h6patplaizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
					arrValores.Add(obj.h6patplaizqphc == null ? null : "'" + obj.h6patplaizqphc + "'");
				}
				if(obj.h6pathofderphc != objOriginal.h6pathofderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
					arrValores.Add(obj.h6pathofderphc == null ? null : "'" + obj.h6pathofderphc + "'");
				}
				if(obj.h6pathofizqphc != objOriginal.h6pathofizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
					arrValores.Add(obj.h6pathofizqphc == null ? null : "'" + obj.h6pathofizqphc + "'");
				}
				if(obj.h6pattroderphc != objOriginal.h6pattroderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
					arrValores.Add(obj.h6pattroderphc == null ? null : "'" + obj.h6pattroderphc + "'");
				}
				if(obj.h6pattroizqphc != objOriginal.h6pattroizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
					arrValores.Add(obj.h6pattroizqphc == null ? null : "'" + obj.h6pattroizqphc + "'");
				}
				if(obj.h6patprederphc != objOriginal.h6patprederphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
					arrValores.Add(obj.h6patprederphc == null ? null : "'" + obj.h6patprederphc + "'");
				}
				if(obj.h6patpreizqphc != objOriginal.h6patpreizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
					arrValores.Add(obj.h6patpreizqphc == null ? null : "'" + obj.h6patpreizqphc + "'");
				}
				if(obj.h6patpmenderphc != objOriginal.h6patpmenderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
					arrValores.Add(obj.h6patpmenderphc == null ? null : "'" + obj.h6patpmenderphc + "'");
				}
				if(obj.h6patpmenizqphc != objOriginal.h6patpmenizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
					arrValores.Add(obj.h6patpmenizqphc == null ? null : "'" + obj.h6patpmenizqphc + "'");
				}
				if(obj.h6supbicderphc != objOriginal.h6supbicderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
					arrValores.Add(obj.h6supbicderphc == null ? null : "'" + obj.h6supbicderphc + "'");
				}
				if(obj.h6supbicizqphc != objOriginal.h6supbicizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
					arrValores.Add(obj.h6supbicizqphc == null ? null : "'" + obj.h6supbicizqphc + "'");
				}
				if(obj.h6suptriderphc != objOriginal.h6suptriderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
					arrValores.Add(obj.h6suptriderphc == null ? null : "'" + obj.h6suptriderphc + "'");
				}
				if(obj.h6suptriizqphc != objOriginal.h6suptriizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
					arrValores.Add(obj.h6suptriizqphc == null ? null : "'" + obj.h6suptriizqphc + "'");
				}
				if(obj.h6supestderphc != objOriginal.h6supestderphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
					arrValores.Add(obj.h6supestderphc == null ? null : "'" + obj.h6supestderphc + "'");
				}
				if(obj.h6supestizqphc != objOriginal.h6supestizqphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
					arrValores.Add(obj.h6supestizqphc == null ? null : "'" + obj.h6supestizqphc + "'");
				}
				if(obj.h6otrmaxilarphc != objOriginal.h6otrmaxilarphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
					arrValores.Add(obj.h6otrmaxilarphc == null ? null : "'" + obj.h6otrmaxilarphc + "'");
				}
				if(obj.h6otrglabelarphc != objOriginal.h6otrglabelarphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
					arrValores.Add(obj.h6otrglabelarphc == null ? null : "'" + obj.h6otrglabelarphc + "'");
				}
				if(obj.h6sistsensitivophc != objOriginal.h6sistsensitivophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
					arrValores.Add(obj.h6sistsensitivophc == null ? null : "'" + obj.h6sistsensitivophc + "'");
				}
				if(obj.h6dolorprofphc != objOriginal.h6dolorprofphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
					arrValores.Add(obj.h6dolorprofphc == null ? null : "'" + obj.h6dolorprofphc + "'");
				}
				if(obj.h6posicionphc != objOriginal.h6posicionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
					arrValores.Add(obj.h6posicionphc == null ? null : "'" + obj.h6posicionphc + "'");
				}
				if(obj.h6vibracionphc != objOriginal.h6vibracionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
					arrValores.Add(obj.h6vibracionphc == null ? null : "'" + obj.h6vibracionphc + "'");
				}
				if(obj.h6dospuntosphc != objOriginal.h6dospuntosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
					arrValores.Add(obj.h6dospuntosphc == null ? null : "'" + obj.h6dospuntosphc + "'");
				}
				if(obj.h6extincionphc != objOriginal.h6extincionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
					arrValores.Add(obj.h6extincionphc == null ? null : "'" + obj.h6extincionphc + "'");
				}
				if(obj.h6estereognosiaphc != objOriginal.h6estereognosiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
					arrValores.Add(obj.h6estereognosiaphc == null ? null : "'" + obj.h6estereognosiaphc + "'");
				}
				if(obj.h6grafestesiaphc != objOriginal.h6grafestesiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
					arrValores.Add(obj.h6grafestesiaphc == null ? null : "'" + obj.h6grafestesiaphc + "'");
				}
				if(obj.h6localizacionphc != objOriginal.h6localizacionphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
					arrValores.Add(obj.h6localizacionphc == null ? null : "'" + obj.h6localizacionphc + "'");
				}
				if(obj.h6imagen != objOriginal.h6imagen )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
					arrValores.Add(obj.h6imagen);
				}
				if(obj.h7rigideznucaphc != objOriginal.h7rigideznucaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
					arrValores.Add(obj.h7rigideznucaphc == null ? null : "'" + obj.h7rigideznucaphc + "'");
				}
				if(obj.h7sigkerningphc != objOriginal.h7sigkerningphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
					arrValores.Add(obj.h7sigkerningphc == null ? null : "'" + obj.h7sigkerningphc + "'");
				}
				if(obj.h7fotofobiaphc != objOriginal.h7fotofobiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
					arrValores.Add(obj.h7fotofobiaphc == null ? null : "'" + obj.h7fotofobiaphc + "'");
				}
				if(obj.h7sigbrudzinskiphc != objOriginal.h7sigbrudzinskiphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
					arrValores.Add(obj.h7sigbrudzinskiphc == null ? null : "'" + obj.h7sigbrudzinskiphc + "'");
				}
				if(obj.h7hiperestesiaphc != objOriginal.h7hiperestesiaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
					arrValores.Add(obj.h7hiperestesiaphc == null ? null : "'" + obj.h7hiperestesiaphc + "'");
				}
				if(obj.h7pulcarotideosphc != objOriginal.h7pulcarotideosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
					arrValores.Add(obj.h7pulcarotideosphc == null ? null : "'" + obj.h7pulcarotideosphc + "'");
				}
				if(obj.h7pultemporalesphc != objOriginal.h7pultemporalesphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
					arrValores.Add(obj.h7pultemporalesphc == null ? null : "'" + obj.h7pultemporalesphc + "'");
				}
				if(obj.h7cabezaphc != objOriginal.h7cabezaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
					arrValores.Add(obj.h7cabezaphc == null ? null : "'" + obj.h7cabezaphc + "'");
				}
				if(obj.h7cuellophc != objOriginal.h7cuellophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
					arrValores.Add(obj.h7cuellophc == null ? null : "'" + obj.h7cuellophc + "'");
				}
				if(obj.h7nerviosperifphc != objOriginal.h7nerviosperifphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
					arrValores.Add(obj.h7nerviosperifphc == null ? null : "'" + obj.h7nerviosperifphc + "'");
				}
				if(obj.h7colvertebralphc != objOriginal.h7colvertebralphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
					arrValores.Add(obj.h7colvertebralphc == null ? null : "'" + obj.h7colvertebralphc + "'");
				}
				if(obj.h7resumenphc != objOriginal.h7resumenphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
					arrValores.Add(obj.h7resumenphc == null ? null : "'" + obj.h7resumenphc + "'");
				}
				if(obj.h7diagnosticosphc != objOriginal.h7diagnosticosphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
					arrValores.Add(obj.h7diagnosticosphc == null ? null : "'" + obj.h7diagnosticosphc + "'");
				}
				if(obj.h7conductaphc != objOriginal.h7conductaphc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
					arrValores.Add(obj.h7conductaphc == null ? null : "'" + obj.h7conductaphc + "'");
				}
				if(obj.h7tratamientophc != objOriginal.h7tratamientophc )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
					arrValores.Add(obj.h7tratamientophc == null ? null : "'" + obj.h7tratamientophc + "'");
				}
				if(obj.apiestado != objOriginal.apiestado )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
					arrValores.Add(obj.apiestado == null ? null : "'" + obj.apiestado + "'");
				}
				if(obj.apitransaccion != objOriginal.apitransaccion )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
					arrValores.Add(obj.apitransaccion == null ? null : "'" + obj.apitransaccion + "'");
				}
				if(obj.usumod != objOriginal.usumod )
				{
					arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
					arrValores.Add(obj.usumod == null ? null : "'" + obj.usumod + "'");
				}
			

			
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.UpdateBd(EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrValores, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pachistoriaclinica3 a partir de una clase del tipo EntPacHistoriaclinica3 y su respectiva PK
		/// </summary>
		/// <param name="obj" type="Entidades.EntPacHistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpachistoriaclinica3
		/// </returns>
		public int DeleteQuery(EntPacHistoriaclinica3 obj)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.DeleteBd(EntPacHistoriaclinica3.StrNombreTabla, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pachistoriaclinica3 a partir de una clase del tipo EntPacHistoriaclinica3 y su PK
		/// </summary>
		/// <param name="obj" type="Entidades.epachistoriaclinica3">
		///     <para>
		/// 		 Clase desde la que se van a eliminar los valores a la tabla. Solo se necesita tener cargado la propiedad que representa a la llave primaria de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionpachistoriaclinica3
		/// </returns>
		public int DeleteQuery(EntPacHistoriaclinica3 obj, ref CTrans localTrans)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
			
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(obj.idppa == null ? null : "'" + obj.idppa + "'");

			
				CConn local = new CConn();
				return local.DeleteBd(EntPacHistoriaclinica3.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pachistoriaclinica3 a partir de una clase del tipo epachistoriaclinica3
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cantidad de registros afectados por el exito de la operacionpachistoriaclinica3
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd("pachistoriaclinica3", arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que elimina un registro en la tabla pachistoriaclinica3 a partir de una clase del tipo epachistoriaclinica3
		/// </summary>
		/// <param name="arrColumnasWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="ArrayList">
		///     <para>
		/// 		 Array de Valores para cada una de las columnas en la clausa WHERE 
		///     </para>
		/// </param>
		/// <param name="localTrans" type="App_Class.Conexion.CTrans">
		///     <para>
		/// 		 Clase desde la que se va a utilizar una transaccion pachistoriaclinica3
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Exito de la operacionpachistoriaclinica3
		/// </returns>
		public int DeleteQuery(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans)
		{
			try
			{
				CConn local = new CConn();
				return local.DeleteBd(EntPacHistoriaclinica3.StrNombreTabla, arrColumnasWhere, arrValoresWhere, ref localTrans);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		

		#endregion 

		#region Llenado de elementos

		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDropDownList(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, String textField)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			try
			{
				CargarDropDownList(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, String textField, EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, String textField, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarDropDownList(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un DropDownList con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Web.UI.WebControls.DropDownList">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DropDownList en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.DataValueField = table.Columns[0].ColumnName;
					cmb.DataTextField = table.Columns[1].ColumnName;
					cmb.DataSource = table;
					cmb.DataBind();

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
			
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, EntPacHistoriaclinica3.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica3.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarGridView(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarGridViewOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Web.UI.WebControls.GridView">
		///     <para>
		/// 		 Control del tipo System.Web.UI.WebControls.GridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;
				dtg.DataBind();

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}

		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarComboBox(ref cmb, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, String textField)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, String textField, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(1);
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(1);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			try
			{
				CargarComboBox(ref cmb, valueField, textField.ToString(), searchField, searchValue);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, String textField, EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, String textField, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField.ToString());
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="valueField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="textField" type="String">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(valueField.ToString());
				arrColumnas.Add(textField);
				
				CargarComboBox(ref cmb, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que carga un ComboBox con los valores de la tabla pachistoriaclinica3
		/// </summary>
		/// <param name="cmb" type="System.Windows.Forms.ComboBox">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.ComboBox en el que se van a cargar los datos de la tabla pachistoriaclinica3
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		 Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DataTable table = local.CargarDataTableAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				if (table.Columns.Count > 0)
				{
					cmb.ValueMember = table.Columns[0].ColumnName;
					cmb.DisplayMember = table.Columns[1].ColumnName;
					cmb.DataSource = table;

				}
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="String">
		///     <para>
		/// 		  Condiciones que van en la clausula WHERE. Deben ir sin WHERE
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
			
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add("'1'");
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add("'1'");
				
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
			
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderAnd(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, EntPacHistoriaclinica3.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnas = new ArrayList();
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.idppa.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h6imagen.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apiestado.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.apitransaccion.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usucre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.feccre.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.usumod.ToString());
				arrColumnas.Add(EntPacHistoriaclinica3.Fields.fecmod.ToString());
				
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica3.Fields searchField, object searchValue)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un GridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="searchField" type="EntPacHistoriaclinica3.Fileds">
		///     <para>
		/// 		 Campo por el que se va a filtrar la busqueda
		///     </para>
		/// </param>
		/// <param name="searchValue" type="object">
		///     <para>
		/// 		 Valor para la busqueda
		///     </para>
		/// </param>
		/// <param name="strParamAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica3.Fields searchField, object searchValue, String strParamAdicionales)
		{
			try
			{
				ArrayList arrColumnasWhere = new ArrayList();
				arrColumnasWhere.Add(searchField.ToString());
				
				ArrayList arrValoresWhere = new ArrayList();
				arrValoresWhere.Add(searchValue);
				CargarDataGrid(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, strParamAdicionales);
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere)
		{
			try
			{
				CargarDataGridOr(ref dtg, arrColumnas, arrColumnasWhere, arrValoresWhere, "");
			}
			catch (Exception exp)
			{
				throw exp;
			}
		}
		/// <summary>
		/// 	 Funcion que llena un DataGridView con los registro de una tabla pachistoriaclinica3
		/// </summary>
		/// <param name="dtg" type="System.Windows.Forms.DataGridView">
		///     <para>
		/// 		 Control del tipo System.Windows.Forms.DataGridView 
		///     </para>
		/// </param>
		/// <param name="arrColumnas" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas que se va a seleccionar para mostrarlas en el resultado
		///     </para>
		/// </param>
		/// <param name="arrColumnasWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las columnas WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="arrValoresWhere" type="System.Collections.ArrayList">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		/// <param name="strParametrosAdicionales" type="string">
		///     <para>
		/// 		 Array de las valores WHERE para filtrar el resultado
		///     </para>
		/// </param>
		public void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales)
		{
			try
			{
				CConn local = new CConn();
				DbDataReader dsReader = local.CargarDataReaderOr(CParametros.Schema + EntPacHistoriaclinica3.StrNombreTabla, arrColumnas, arrColumnasWhere, arrValoresWhere, strParametrosAdicionales);
				
				dtg.DataSource = dsReader;

			}
			catch (Exception exp)
			{
				throw exp;
			}
		}


		#endregion 


		#endregion

		#region Funciones Internas

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto pachistoriaclinica3
		/// </returns>
		internal EntPacHistoriaclinica3 crearObjeto(DataRow row)
		{
			var obj = new EntPacHistoriaclinica3();
			obj.idppa = GetColumnType(row[EntPacHistoriaclinica3.Fields.idppa.ToString()], EntPacHistoriaclinica3.Fields.idppa);
			obj.h6reflejos01phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos01phc);
			obj.h6reflejos02phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos02phc);
			obj.h6reflejos03phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos03phc);
			obj.h6reflejos04phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos04phc);
			obj.h6reflejos05phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos05phc);
			obj.h6reflejos06phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos06phc);
			obj.h6reflejos07phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos07phc);
			obj.h6reflejos08phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos08phc);
			obj.h6reflejos09phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos09phc);
			obj.h6reflejos10phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos10phc);
			obj.h6miotbicderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotbicderphc);
			obj.h6miotbicizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotbicizqphc);
			obj.h6miottriderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString()], EntPacHistoriaclinica3.Fields.h6miottriderphc);
			obj.h6miottriizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6miottriizqphc);
			obj.h6miotestderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotestderphc);
			obj.h6miotestizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotestizqphc);
			obj.h6miotpatderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotpatderphc);
			obj.h6miotpatizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotpatizqphc);
			obj.h6miotaquderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotaquderphc);
			obj.h6miotaquizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotaquizqphc);
			obj.h6patpladerphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString()], EntPacHistoriaclinica3.Fields.h6patpladerphc);
			obj.h6patplaizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6patplaizqphc);
			obj.h6pathofderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString()], EntPacHistoriaclinica3.Fields.h6pathofderphc);
			obj.h6pathofizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6pathofizqphc);
			obj.h6pattroderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString()], EntPacHistoriaclinica3.Fields.h6pattroderphc);
			obj.h6pattroizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6pattroizqphc);
			obj.h6patprederphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6patprederphc.ToString()], EntPacHistoriaclinica3.Fields.h6patprederphc);
			obj.h6patpreizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6patpreizqphc);
			obj.h6patpmenderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString()], EntPacHistoriaclinica3.Fields.h6patpmenderphc);
			obj.h6patpmenizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6patpmenizqphc);
			obj.h6supbicderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString()], EntPacHistoriaclinica3.Fields.h6supbicderphc);
			obj.h6supbicizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6supbicizqphc);
			obj.h6suptriderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString()], EntPacHistoriaclinica3.Fields.h6suptriderphc);
			obj.h6suptriizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6suptriizqphc);
			obj.h6supestderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6supestderphc.ToString()], EntPacHistoriaclinica3.Fields.h6supestderphc);
			obj.h6supestizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6supestizqphc);
			obj.h6otrmaxilarphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString()], EntPacHistoriaclinica3.Fields.h6otrmaxilarphc);
			obj.h6otrglabelarphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString()], EntPacHistoriaclinica3.Fields.h6otrglabelarphc);
			obj.h6sistsensitivophc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString()], EntPacHistoriaclinica3.Fields.h6sistsensitivophc);
			obj.h6dolorprofphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString()], EntPacHistoriaclinica3.Fields.h6dolorprofphc);
			obj.h6posicionphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6posicionphc.ToString()], EntPacHistoriaclinica3.Fields.h6posicionphc);
			obj.h6vibracionphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString()], EntPacHistoriaclinica3.Fields.h6vibracionphc);
			obj.h6dospuntosphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString()], EntPacHistoriaclinica3.Fields.h6dospuntosphc);
			obj.h6extincionphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6extincionphc.ToString()], EntPacHistoriaclinica3.Fields.h6extincionphc);
			obj.h6estereognosiaphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString()], EntPacHistoriaclinica3.Fields.h6estereognosiaphc);
			obj.h6grafestesiaphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString()], EntPacHistoriaclinica3.Fields.h6grafestesiaphc);
			obj.h6localizacionphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString()], EntPacHistoriaclinica3.Fields.h6localizacionphc);
			obj.h6imagen = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6imagen.ToString()], EntPacHistoriaclinica3.Fields.h6imagen);
			obj.h7rigideznucaphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString()], EntPacHistoriaclinica3.Fields.h7rigideznucaphc);
			obj.h7sigkerningphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString()], EntPacHistoriaclinica3.Fields.h7sigkerningphc);
			obj.h7fotofobiaphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString()], EntPacHistoriaclinica3.Fields.h7fotofobiaphc);
			obj.h7sigbrudzinskiphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString()], EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc);
			obj.h7hiperestesiaphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString()], EntPacHistoriaclinica3.Fields.h7hiperestesiaphc);
			obj.h7pulcarotideosphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString()], EntPacHistoriaclinica3.Fields.h7pulcarotideosphc);
			obj.h7pultemporalesphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString()], EntPacHistoriaclinica3.Fields.h7pultemporalesphc);
			obj.h7cabezaphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString()], EntPacHistoriaclinica3.Fields.h7cabezaphc);
			obj.h7cuellophc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7cuellophc.ToString()], EntPacHistoriaclinica3.Fields.h7cuellophc);
			obj.h7nerviosperifphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString()], EntPacHistoriaclinica3.Fields.h7nerviosperifphc);
			obj.h7colvertebralphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString()], EntPacHistoriaclinica3.Fields.h7colvertebralphc);
			obj.h7resumenphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7resumenphc.ToString()], EntPacHistoriaclinica3.Fields.h7resumenphc);
			obj.h7diagnosticosphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString()], EntPacHistoriaclinica3.Fields.h7diagnosticosphc);
			obj.h7conductaphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7conductaphc.ToString()], EntPacHistoriaclinica3.Fields.h7conductaphc);
			obj.h7tratamientophc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString()], EntPacHistoriaclinica3.Fields.h7tratamientophc);
			obj.apiestado = GetColumnType(row[EntPacHistoriaclinica3.Fields.apiestado.ToString()], EntPacHistoriaclinica3.Fields.apiestado);
			obj.apitransaccion = GetColumnType(row[EntPacHistoriaclinica3.Fields.apitransaccion.ToString()], EntPacHistoriaclinica3.Fields.apitransaccion);
			obj.usucre = GetColumnType(row[EntPacHistoriaclinica3.Fields.usucre.ToString()], EntPacHistoriaclinica3.Fields.usucre);
			obj.feccre = GetColumnType(row[EntPacHistoriaclinica3.Fields.feccre.ToString()], EntPacHistoriaclinica3.Fields.feccre);
			obj.usumod = GetColumnType(row[EntPacHistoriaclinica3.Fields.usumod.ToString()], EntPacHistoriaclinica3.Fields.usumod);
			obj.fecmod = GetColumnType(row[EntPacHistoriaclinica3.Fields.fecmod.ToString()], EntPacHistoriaclinica3.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que devuelve un objeto a partir de un DataRow
		/// </summary>
		/// <param name="row" type="System.Data.DataRow">
		///     <para>
		/// 		 DataRow con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Objeto pachistoriaclinica3
		/// </returns>
		internal EntPacHistoriaclinica3 crearObjetoRevisado(DataRow row)
		{
			var obj = new EntPacHistoriaclinica3();
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.idppa.ToString()))
				obj.idppa = GetColumnType(row[EntPacHistoriaclinica3.Fields.idppa.ToString()], EntPacHistoriaclinica3.Fields.idppa);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString()))
				obj.h6reflejos01phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos01phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos01phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString()))
				obj.h6reflejos02phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos02phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos02phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString()))
				obj.h6reflejos03phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos03phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos03phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString()))
				obj.h6reflejos04phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos04phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos04phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString()))
				obj.h6reflejos05phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos05phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos05phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString()))
				obj.h6reflejos06phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos06phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos06phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString()))
				obj.h6reflejos07phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos07phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos07phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString()))
				obj.h6reflejos08phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos08phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos08phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString()))
				obj.h6reflejos09phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos09phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos09phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString()))
				obj.h6reflejos10phc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6reflejos10phc.ToString()], EntPacHistoriaclinica3.Fields.h6reflejos10phc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString()))
				obj.h6miotbicderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotbicderphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotbicderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString()))
				obj.h6miotbicizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotbicizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotbicizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString()))
				obj.h6miottriderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miottriderphc.ToString()], EntPacHistoriaclinica3.Fields.h6miottriderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString()))
				obj.h6miottriizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miottriizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6miottriizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString()))
				obj.h6miotestderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotestderphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotestderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString()))
				obj.h6miotestizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotestizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotestizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString()))
				obj.h6miotpatderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotpatderphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotpatderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString()))
				obj.h6miotpatizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotpatizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotpatizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString()))
				obj.h6miotaquderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotaquderphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotaquderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString()))
				obj.h6miotaquizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6miotaquizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6miotaquizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString()))
				obj.h6patpladerphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6patpladerphc.ToString()], EntPacHistoriaclinica3.Fields.h6patpladerphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString()))
				obj.h6patplaizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6patplaizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6patplaizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString()))
				obj.h6pathofderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6pathofderphc.ToString()], EntPacHistoriaclinica3.Fields.h6pathofderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString()))
				obj.h6pathofizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6pathofizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6pathofizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString()))
				obj.h6pattroderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6pattroderphc.ToString()], EntPacHistoriaclinica3.Fields.h6pattroderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString()))
				obj.h6pattroizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6pattroizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6pattroizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6patprederphc.ToString()))
				obj.h6patprederphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6patprederphc.ToString()], EntPacHistoriaclinica3.Fields.h6patprederphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString()))
				obj.h6patpreizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6patpreizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6patpreizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString()))
				obj.h6patpmenderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6patpmenderphc.ToString()], EntPacHistoriaclinica3.Fields.h6patpmenderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString()))
				obj.h6patpmenizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6patpmenizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6patpmenizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString()))
				obj.h6supbicderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6supbicderphc.ToString()], EntPacHistoriaclinica3.Fields.h6supbicderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString()))
				obj.h6supbicizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6supbicizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6supbicizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString()))
				obj.h6suptriderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6suptriderphc.ToString()], EntPacHistoriaclinica3.Fields.h6suptriderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString()))
				obj.h6suptriizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6suptriizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6suptriizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6supestderphc.ToString()))
				obj.h6supestderphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6supestderphc.ToString()], EntPacHistoriaclinica3.Fields.h6supestderphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString()))
				obj.h6supestizqphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6supestizqphc.ToString()], EntPacHistoriaclinica3.Fields.h6supestizqphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString()))
				obj.h6otrmaxilarphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6otrmaxilarphc.ToString()], EntPacHistoriaclinica3.Fields.h6otrmaxilarphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString()))
				obj.h6otrglabelarphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6otrglabelarphc.ToString()], EntPacHistoriaclinica3.Fields.h6otrglabelarphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString()))
				obj.h6sistsensitivophc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6sistsensitivophc.ToString()], EntPacHistoriaclinica3.Fields.h6sistsensitivophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString()))
				obj.h6dolorprofphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6dolorprofphc.ToString()], EntPacHistoriaclinica3.Fields.h6dolorprofphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6posicionphc.ToString()))
				obj.h6posicionphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6posicionphc.ToString()], EntPacHistoriaclinica3.Fields.h6posicionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString()))
				obj.h6vibracionphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6vibracionphc.ToString()], EntPacHistoriaclinica3.Fields.h6vibracionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString()))
				obj.h6dospuntosphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6dospuntosphc.ToString()], EntPacHistoriaclinica3.Fields.h6dospuntosphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6extincionphc.ToString()))
				obj.h6extincionphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6extincionphc.ToString()], EntPacHistoriaclinica3.Fields.h6extincionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString()))
				obj.h6estereognosiaphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6estereognosiaphc.ToString()], EntPacHistoriaclinica3.Fields.h6estereognosiaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString()))
				obj.h6grafestesiaphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6grafestesiaphc.ToString()], EntPacHistoriaclinica3.Fields.h6grafestesiaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString()))
				obj.h6localizacionphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6localizacionphc.ToString()], EntPacHistoriaclinica3.Fields.h6localizacionphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h6imagen.ToString()))
				obj.h6imagen = GetColumnType(row[EntPacHistoriaclinica3.Fields.h6imagen.ToString()], EntPacHistoriaclinica3.Fields.h6imagen);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString()))
				obj.h7rigideznucaphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7rigideznucaphc.ToString()], EntPacHistoriaclinica3.Fields.h7rigideznucaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString()))
				obj.h7sigkerningphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7sigkerningphc.ToString()], EntPacHistoriaclinica3.Fields.h7sigkerningphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString()))
				obj.h7fotofobiaphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7fotofobiaphc.ToString()], EntPacHistoriaclinica3.Fields.h7fotofobiaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString()))
				obj.h7sigbrudzinskiphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc.ToString()], EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString()))
				obj.h7hiperestesiaphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7hiperestesiaphc.ToString()], EntPacHistoriaclinica3.Fields.h7hiperestesiaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString()))
				obj.h7pulcarotideosphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7pulcarotideosphc.ToString()], EntPacHistoriaclinica3.Fields.h7pulcarotideosphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString()))
				obj.h7pultemporalesphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7pultemporalesphc.ToString()], EntPacHistoriaclinica3.Fields.h7pultemporalesphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString()))
				obj.h7cabezaphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7cabezaphc.ToString()], EntPacHistoriaclinica3.Fields.h7cabezaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h7cuellophc.ToString()))
				obj.h7cuellophc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7cuellophc.ToString()], EntPacHistoriaclinica3.Fields.h7cuellophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString()))
				obj.h7nerviosperifphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7nerviosperifphc.ToString()], EntPacHistoriaclinica3.Fields.h7nerviosperifphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString()))
				obj.h7colvertebralphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7colvertebralphc.ToString()], EntPacHistoriaclinica3.Fields.h7colvertebralphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h7resumenphc.ToString()))
				obj.h7resumenphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7resumenphc.ToString()], EntPacHistoriaclinica3.Fields.h7resumenphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString()))
				obj.h7diagnosticosphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7diagnosticosphc.ToString()], EntPacHistoriaclinica3.Fields.h7diagnosticosphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h7conductaphc.ToString()))
				obj.h7conductaphc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7conductaphc.ToString()], EntPacHistoriaclinica3.Fields.h7conductaphc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString()))
				obj.h7tratamientophc = GetColumnType(row[EntPacHistoriaclinica3.Fields.h7tratamientophc.ToString()], EntPacHistoriaclinica3.Fields.h7tratamientophc);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.apiestado.ToString()))
				obj.apiestado = GetColumnType(row[EntPacHistoriaclinica3.Fields.apiestado.ToString()], EntPacHistoriaclinica3.Fields.apiestado);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.apitransaccion.ToString()))
				obj.apitransaccion = GetColumnType(row[EntPacHistoriaclinica3.Fields.apitransaccion.ToString()], EntPacHistoriaclinica3.Fields.apitransaccion);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.usucre.ToString()))
				obj.usucre = GetColumnType(row[EntPacHistoriaclinica3.Fields.usucre.ToString()], EntPacHistoriaclinica3.Fields.usucre);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.feccre.ToString()))
				obj.feccre = GetColumnType(row[EntPacHistoriaclinica3.Fields.feccre.ToString()], EntPacHistoriaclinica3.Fields.feccre);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.usumod.ToString()))
				obj.usumod = GetColumnType(row[EntPacHistoriaclinica3.Fields.usumod.ToString()], EntPacHistoriaclinica3.Fields.usumod);
			if (row.Table.Columns.Contains(EntPacHistoriaclinica3.Fields.fecmod.ToString()))
				obj.fecmod = GetColumnType(row[EntPacHistoriaclinica3.Fields.fecmod.ToString()], EntPacHistoriaclinica3.Fields.fecmod);
			return obj;
		}

		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtpachistoriaclinica3" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos pachistoriaclinica3
		/// </returns>
		internal List<EntPacHistoriaclinica3> crearLista(DataTable dtpachistoriaclinica3)
		{
			var list = new List<EntPacHistoriaclinica3>();
			
			foreach (DataRow row in dtpachistoriaclinica3.Rows)
			{
				var obj = crearObjeto(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable y con solo algunas columnas
		/// </summary>
		/// <param name="dtpachistoriaclinica3" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Lista de Objetos pachistoriaclinica3
		/// </returns>
		internal List<EntPacHistoriaclinica3> crearListaRevisada(DataTable dtpachistoriaclinica3)
		{
			List<EntPacHistoriaclinica3> list = new List<EntPacHistoriaclinica3>();
			
			foreach (DataRow row in dtpachistoriaclinica3.Rows)
			{
				var obj = crearObjetoRevisado(row);
				list.Add(obj);
			}
			return list;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtpachistoriaclinica3" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Cola de Objetos pachistoriaclinica3
		/// </returns>
		internal Queue<EntPacHistoriaclinica3> crearCola(DataTable dtpachistoriaclinica3)
		{
			Queue<EntPacHistoriaclinica3> cola = new Queue<EntPacHistoriaclinica3>();
			
			EntPacHistoriaclinica3 obj = new EntPacHistoriaclinica3();
			foreach (DataRow row in dtpachistoriaclinica3.Rows)
			{
				obj = crearObjeto(row);
				cola.Enqueue(obj);
			}
			return cola;
		}
		
		/// <summary>
		/// 	 Funcion que crea una Lista de objetos a partir de un DataTable
		/// </summary>
		/// <param name="dtpachistoriaclinica3" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Pila de Objetos pachistoriaclinica3
		/// </returns>
		internal Stack<EntPacHistoriaclinica3> crearPila(DataTable dtpachistoriaclinica3)
		{
			Stack<EntPacHistoriaclinica3> pila = new Stack<EntPacHistoriaclinica3>();
			
			foreach (DataRow row in dtpachistoriaclinica3.Rows)
			{
				var obj = crearObjeto(row);
				pila.Push(obj);
			}
			return pila;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtpachistoriaclinica3" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos pachistoriaclinica3
		/// </returns>
		internal Dictionary<String, EntPacHistoriaclinica3> crearDiccionario(DataTable dtpachistoriaclinica3)
		{
			Dictionary<String, EntPacHistoriaclinica3>  miDic = new Dictionary<String, EntPacHistoriaclinica3>();
			
			foreach (DataRow row in dtpachistoriaclinica3.Rows)
			{
				var obj = crearObjeto(row);
				miDic.Add(obj.idppa.ToString(), obj);
			}
			return miDic;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable
		/// </summary>
		/// <param name="dtpachistoriaclinica3" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 HashTable de Objetos pachistoriaclinica3
		/// </returns>
		internal Hashtable crearHashTable(DataTable dtpachistoriaclinica3)
		{
			Hashtable miTabla = new Hashtable();
			
			foreach (DataRow row in dtpachistoriaclinica3.Rows)
			{
				var obj = crearObjeto(row);
				miTabla.Add(obj.idppa.ToString(), obj);
			}
			return miTabla;
		}
		
		/// <summary>
		/// 	 Funcion que crea un Dicionario a partir de un DataTable y solo con columnas existentes
		/// </summary>
		/// <param name="dtpachistoriaclinica3" type="System.Data.DateTable">
		///     <para>
		/// 		 DataTable con el conjunto de Datos recuperados 
		///     </para>
		/// </param>
		/// <returns>
		/// 	 Diccionario de Objetos pachistoriaclinica3
		/// </returns>
		internal Dictionary<String, EntPacHistoriaclinica3> crearDiccionarioRevisado(DataTable dtpachistoriaclinica3)
		{
			Dictionary<String, EntPacHistoriaclinica3>  miDic = new Dictionary<String, EntPacHistoriaclinica3>();
			
			foreach (DataRow row in dtpachistoriaclinica3.Rows)
			{
				var obj = crearObjetoRevisado(row);
				miDic.Add(obj.idppa.ToString(), obj);
			}
			return miDic;
		}
		
		internal Dictionary<String, EntPacHistoriaclinica3> crearDiccionario(DataTable dtpachistoriaclinica3, EntPacHistoriaclinica3.Fields dicKey)
		{
			Dictionary<String, EntPacHistoriaclinica3>  miDic = new Dictionary<String, EntPacHistoriaclinica3>();
			
			foreach (DataRow row in dtpachistoriaclinica3.Rows)
			{
				var obj = crearObjeto(row);
				
				var nameOfProperty = dicKey.ToString();
				var propertyInfo = obj.GetType().GetProperty(nameOfProperty);
				var value = propertyInfo.GetValue(obj, null);
				
				miDic.Add(value.ToString(), obj);
			}
			return miDic;
		}
		
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}
		
		protected void Finalize()
		{
			Dispose();
		}
		#endregion

	}
}

