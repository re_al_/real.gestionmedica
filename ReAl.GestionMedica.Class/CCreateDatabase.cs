using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace ReAl.GestionMedica.Class
{
    internal static class ExtensionMethod
    {
        public static T GetMyAttributeFrom<T>(this object instance, string propertyName) where T : Attribute
        {
            var attrType = typeof(T);
            var property = instance.GetType().GetProperty(propertyName);

            if (property.GetCustomAttributes(attrType, false).Length > 0)
                return (T)property.GetCustomAttributes(attrType, false).First();
            else
                return null;
        }
    }

    public class TableClass
    {
        private Type _miClase = null;

        private Dictionary<Type, string> DataMapper
        {
            get
            {
                //
                var miDataMapper = new Dictionary<Type, string>
                {
                    {typeof(Int64), "BIGINT"},
                    {typeof(Int64?), "BIGINT"},
                    {typeof(int), "INTEGER"},
                    {typeof(int?), "INTEGER"},
                    {typeof(string), "CHARACTER VARYING"},
                    {typeof(bool), "BOOLEAN"},
                    {typeof(bool?), "BOOLEAN"},
                    {typeof(DateTime), "TIMESTAMP WITHOUT TIME ZONE"},
                    {typeof(DateTime?), "TIMESTAMP WITHOUT TIME ZONE"},
                    {typeof(float), "FLOAT"},
                    {typeof(float?), "FLOAT"},
                    {typeof(decimal), "NUMERIC"},
                    {typeof(decimal?), "NUMERIC"},
                    {typeof(Byte), "BYTEA"},
                    {typeof(Byte?), "BYTEA"},
                    {typeof(Guid), "UNIQUEIDENTIFIER"}
                };
                //miDataMapper.Add(typeof(decimal), "DECIMAL(10,2)");
                //miDataMapper.Add(typeof(decimal?), "DECIMAL(10,2)");

                return miDataMapper;
            }
        }

        public List<KeyValuePair<string, Type>> Fields { get; set; } = new List<KeyValuePair<string, Type>>();

        public string ClassName { get; set; }

        public TableClass(Type t)
        {
            ClassName = t.Name;
            _miClase = t;

            foreach (var property in t.GetProperties())
            {
                var field = new KeyValuePair<string, Type>(property.Name, property.PropertyType);
                Fields.Add(field);
            }
        }

        public string CreateTableScript()
        {
            var script = new StringBuilder();

            var scriptPk = new StringBuilder();
            var bPrimerElemntoPk = true;

            var instance = Activator.CreateInstance(_miClase);
            script.AppendLine("CREATE TABLE " + ClassName);
            script.AppendLine("(");
            for (var i = 0; i < Fields.Count; i++)
            {
                var field = Fields[i];

                if (DataMapper.ContainsKey(field.Value))
                {
                    if (DataMapper[field.Value] == "CHARACTER VARYING")
                    {
                        if (instance.GetMyAttributeFrom<StringLengthAttribute>(field.Key) != null)
                            script.Append("\t" + field.Key + " " + DataMapper[field.Value] + "(" + instance.GetMyAttributeFrom<StringLengthAttribute>(field.Key).MaximumLength + ")");
                        else
                            script.Append("\t" + field.Key + " " + DataMapper[field.Value] + "(100)");
                    }
                    else
                    {
                        script.Append("\t" + field.Key + " " + DataMapper[field.Value]);
                    }
                }
                else
                {
                    //Complex Type
                    script.Append("\t" + field.Key + " COMPLEX");
                }

                //Ahora los DataAnnotations
                if (instance.GetMyAttributeFrom<RequiredAttribute>(field.Key) != null)
                    script.Append(" NOT NULL ");

                //Para los Defaults
                if (field.Key.ToUpper() == "APIESTADO")
                    script.Append(" DEFAULT 'ELABORADO'::character varying ");
                if (field.Key.ToUpper() == "APITRANSACCION")
                    script.Append(" DEFAULT 'CREAR'::character varying ");
                if (field.Key.ToUpper() == "USUCRE")
                    script.Append(" DEFAULT \"current_user\"() ");
                if (field.Key.ToUpper() == "FECCRE")
                    script.Append(" DEFAULT now() ");

                //Para los PK
                if (instance.GetMyAttributeFrom<KeyAttribute>(field.Key) != null)
                {
                    if (bPrimerElemntoPk)
                    {
                        scriptPk.Append(field.Key);
                        bPrimerElemntoPk = false;
                    }
                    else
                        scriptPk.Append(", " + field.Key);
                }

                if (i != Fields.Count - 1)
                    script.Append(",");

                //Para los COmentarios
                //if (instance.GetMyAttributeFrom<DisplayAttribute>(field.Key) != null)
                //    script.Append("//" + instance.GetMyAttributeFrom<DisplayAttribute>(field.Key).Description);

                script.Append(Environment.NewLine);
            }

            if (!bPrimerElemntoPk)
                script.AppendLine("\t, CONSTRAINT " + ClassName + "_pk PRIMARY KEY (" + scriptPk.ToString() + ")");

            script.AppendLine(");");

            return script.ToString();
        }
    }
}