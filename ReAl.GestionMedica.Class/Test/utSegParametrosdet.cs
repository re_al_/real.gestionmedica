#region 
/***********************************************************************************************************
	NOMBRE:       utSegParametrosdet
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla SegParametrosdet

	REVISIONES:
		Ver        FECHA       Autor            Descripción 
		---------  ----------  ---------------  ------------------------------------
		1.0        14/05/2013  R Alonzo Vera A  Creación 

*************************************************************************************************************/
#endregion



#region
using System.Collections.Generic;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GesMed.Conn; 
using GesMed.Class.Entidades;
using GesMed.Class.Modelo;
#endregion

namespace GesMed.Class.Test
{
	[TestClass()]
	public class utSegParametrosdet
	{
		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion

		/// <summary>
		///Prueba para el metodo Insert(rnSegParametrosdet)
		///</summary>
		[TestMethod()]
		public void InsertTest()
		{
			rnSegParametrosdet target = new rnSegParametrosdet();
			entSegParametrosdet obj = new entSegParametrosdet();
			bool expected = true;
			bool actual;
			
			//obj.Codigospd = 0;
			//obj.Siglaspa = "";
			//obj.Parint1spd = null;
			//obj.Parint2spd = null;
			//obj.Parchar1spd = null;
			//obj.Parchar2spd = null;
			//obj.Parnum1spd = null;
			//obj.Parnum2spd = null;
			//obj.Parbit1spd = null;
			//obj.Parbit2spd = null;
			//obj.Usucrespd = "";
			//obj.Parestadospd = null;
			//obj.Partransaccionspd = null;
			
			actual = target.Insert(obj);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Insert(rnSegParametrosdet, cTrans)
		///</summary>
		[TestMethod()]
		public void InsertTestTrans()
		{
			rnSegParametrosdet target = new rnSegParametrosdet();
			entSegParametrosdet obj = new entSegParametrosdet();
			cTrans localTrans = new cTrans();
			bool expected = true;
			bool actual;
			
			//obj.Codigospd = 0;
			//obj.Siglaspa = "";
			//obj.Parint1spd = null;
			//obj.Parint2spd = null;
			//obj.Parchar1spd = null;
			//obj.Parchar2spd = null;
			//obj.Parnum1spd = null;
			//obj.Parnum2spd = null;
			//obj.Parbit1spd = null;
			//obj.Parbit2spd = null;
			//obj.Usucrespd = "";
			//obj.Parestadospd = null;
			//obj.Partransaccionspd = null;
			
			actual = target.Insert(obj, ref localTrans);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegParametrosdet)
		///</summary>
		[TestMethod()]
		public void UpdateTest()
		{
			rnSegParametrosdet target = new rnSegParametrosdet();
			entSegParametrosdet obj = new entSegParametrosdet();
			int expected = 1;
			int actual;
			
			//obj.Siglaspa = "";
			//obj.Codigospd = 0;
			//obj.Parint1spd = null;
			//obj.Parint2spd = null;
			//obj.Parchar1spd = null;
			//obj.Parchar2spd = null;
			//obj.Parnum1spd = null;
			//obj.Parnum2spd = null;
			//obj.Parbit1spd = null;
			//obj.Parbit2spd = null;
			//obj.Apitransaccionspd = "";
			//obj.Usumodspd = null;
			//obj.Parestadospd = null;
			//obj.Partransaccionspd = null;
			
			actual = target.Update(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegParametrosdet, cTrans)
		///</summary>
		[TestMethod()]
		public void UpdateTestTrans()
		{
			rnSegParametrosdet target = new rnSegParametrosdet();
			entSegParametrosdet obj = new entSegParametrosdet();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Siglaspa = "";
			//obj.Codigospd = 0;
			//obj.Parint1spd = null;
			//obj.Parint2spd = null;
			//obj.Parchar1spd = null;
			//obj.Parchar2spd = null;
			//obj.Parnum1spd = null;
			//obj.Parnum2spd = null;
			//obj.Parbit1spd = null;
			//obj.Parbit2spd = null;
			//obj.Apitransaccionspd = "";
			//obj.Usumodspd = null;
			//obj.Parestadospd = null;
			//obj.Partransaccionspd = null;
			
			actual = target.Update(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegParametrosdet)
		///</summary>
		[TestMethod()]
		public void DeleteTest()
		{
			rnSegParametrosdet target = new rnSegParametrosdet();
			entSegParametrosdet obj = new entSegParametrosdet();
			int expected = 1;
			int actual;
			
			//obj.Siglaspa = "";
			//obj.Codigospd = 0;
			
			actual = target.Delete(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegParametrosdet, cTrans)
		///</summary>
		[TestMethod()]
		public void DeleteTestTrans()
		{
			rnSegParametrosdet target = new rnSegParametrosdet();
			entSegParametrosdet obj = new entSegParametrosdet();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Siglaspa = "";
			//obj.Codigospd = 0;
			
			actual = target.Delete(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el método ObtenerLista()
		///</summary>
		[TestMethod()]
		public void ObtenerListaTest()
		{
			rnSegParametrosdet target = new rnSegParametrosdet();
			List<entSegParametrosdet> notExpected = null;
			List<entSegParametrosdet> actual = target.ObtenerLista();
			Assert.AreNotEqual(notExpected, actual);
		}
		
		/// <summary>
		///Prueba para el método CargarDataTable()
		///</summary>
		[TestMethod()]
		public void CargarDataTableTest()
		{
			rnSegParametrosdet target = new rnSegParametrosdet();
			int notExpected = 0; 
			int actualColumns = target.CargarDataTable().Columns.Count;
			Assert.AreNotEqual(notExpected, actualColumns);
		}
		
	}
}

