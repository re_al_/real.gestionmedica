#region 
/***********************************************************************************************************
	NOMBRE:       utSegUsuarios
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla SegUsuarios

	REVISIONES:
		Ver        FECHA       Autor            Descripción 
		---------  ----------  ---------------  ------------------------------------
		1.0        14/05/2013  R Alonzo Vera A  Creación 

*************************************************************************************************************/
#endregion



#region
using System.Collections.Generic;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GesMed.Conn; 
using GesMed.Class.Entidades;
using GesMed.Class.Modelo;
#endregion

namespace GesMed.Class.Test
{
	[TestClass()]
	public class utSegUsuarios
	{
		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion

		/// <summary>
		///Prueba para el metodo Insert(rnSegUsuarios)
		///</summary>
		[TestMethod()]
		public void InsertTest()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			entSegUsuarios obj = new entSegUsuarios();
			bool expected = true;
			bool actual;
			
			//obj.Idsus = 0;
			//obj.Idsus = 0;
			//obj.Loginsus = "";
			//obj.Loginsus = "";
			//obj.Passsus = "";
			//obj.Passsus = "";
			//obj.Nombresus = "";
			//obj.Nombresus = "";
			//obj.Apellidosus = "";
			//obj.Apellidosus = "";
			//obj.Vigentesus = 0;
			//obj.Vigentesus = 0;
			//obj.Fechavigentesus = null;
			//obj.Fechavigentesus = null;
			//obj.Fechapasssus = null;
			//obj.Fechapasssus = null;
			//obj.Usucresus = "";
			//obj.Usucresus = "";
			
			actual = target.Insert(obj);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Insert(rnSegUsuarios, cTrans)
		///</summary>
		[TestMethod()]
		public void InsertTestTrans()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			entSegUsuarios obj = new entSegUsuarios();
			cTrans localTrans = new cTrans();
			bool expected = true;
			bool actual;
			
			//obj.Idsus = 0;
			//obj.Idsus = 0;
			//obj.Loginsus = "";
			//obj.Loginsus = "";
			//obj.Passsus = "";
			//obj.Passsus = "";
			//obj.Nombresus = "";
			//obj.Nombresus = "";
			//obj.Apellidosus = "";
			//obj.Apellidosus = "";
			//obj.Vigentesus = 0;
			//obj.Vigentesus = 0;
			//obj.Fechavigentesus = null;
			//obj.Fechavigentesus = null;
			//obj.Fechapasssus = null;
			//obj.Fechapasssus = null;
			//obj.Usucresus = "";
			//obj.Usucresus = "";
			
			actual = target.Insert(obj, ref localTrans);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegUsuarios)
		///</summary>
		[TestMethod()]
		public void UpdateTest()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			entSegUsuarios obj = new entSegUsuarios();
			int expected = 1;
			int actual;
			
			//obj.Idsus = 0;
			//obj.Loginsus = "";
			//obj.Passsus = "";
			//obj.Nombresus = "";
			//obj.Apellidosus = "";
			//obj.Vigentesus = 0;
			//obj.Fechavigentesus = null;
			//obj.Fechapasssus = null;
			//obj.Apitransaccionsus = "";
			//obj.Usumodsus = null;
			
			actual = target.Update(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegUsuarios, cTrans)
		///</summary>
		[TestMethod()]
		public void UpdateTestTrans()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			entSegUsuarios obj = new entSegUsuarios();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Idsus = 0;
			//obj.Loginsus = "";
			//obj.Passsus = "";
			//obj.Nombresus = "";
			//obj.Apellidosus = "";
			//obj.Vigentesus = 0;
			//obj.Fechavigentesus = null;
			//obj.Fechapasssus = null;
			//obj.Apitransaccionsus = "";
			//obj.Usumodsus = null;
			
			actual = target.Update(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegUsuarios)
		///</summary>
		[TestMethod()]
		public void DeleteTest()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			entSegUsuarios obj = new entSegUsuarios();
			int expected = 1;
			int actual;
			
			//obj.Idsus = 0;
			
			actual = target.Delete(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegUsuarios, cTrans)
		///</summary>
		[TestMethod()]
		public void DeleteTestTrans()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			entSegUsuarios obj = new entSegUsuarios();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Idsus = 0;
			
			actual = target.Delete(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el método ObtenerLista()
		///</summary>
		[TestMethod()]
		public void ObtenerListaTest()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			List<entSegUsuarios> notExpected = null;
			List<entSegUsuarios> actual = target.ObtenerLista();
			Assert.AreNotEqual(notExpected, actual);
		}
		
		/// <summary>
		///Prueba para el método CargarDataTable()
		///</summary>
		[TestMethod()]
		public void CargarDataTableTest()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			int notExpected = 0; 
			int actualColumns = target.CargarDataTable().Columns.Count;
			Assert.AreNotEqual(notExpected, actualColumns);
		}
		
	}
}

