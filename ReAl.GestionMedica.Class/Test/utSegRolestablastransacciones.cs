#region 
/***********************************************************************************************************
	NOMBRE:       utSegRolestablastransacciones
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla SegRolestablastransacciones

	REVISIONES:
		Ver        FECHA       Autor            Descripción 
		---------  ----------  ---------------  ------------------------------------
		1.0        14/05/2013  R Alonzo Vera A  Creación 

*************************************************************************************************************/
#endregion



#region
using System.Collections.Generic;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GesMed.Conn; 
using GesMed.Class.Entidades;
using GesMed.Class.Modelo;
#endregion

namespace GesMed.Class.Test
{
	[TestClass()]
	public class utSegRolestablastransacciones
	{
		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion

		/// <summary>
		///Prueba para el metodo Insert(rnSegRolestablastransacciones)
		///</summary>
		[TestMethod()]
		public void InsertTest()
		{
			rnSegRolestablastransacciones target = new rnSegRolestablastransacciones();
			entSegRolestablastransacciones obj = new entSegRolestablastransacciones();
			bool expected = true;
			bool actual;
			
			//obj.Rolsro = 0;
			//obj.Tablasta = "";
			//obj.Transaccionstr = "";
			//obj.Usucresrt = "";
			
			actual = target.Insert(obj);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Insert(rnSegRolestablastransacciones, cTrans)
		///</summary>
		[TestMethod()]
		public void InsertTestTrans()
		{
			rnSegRolestablastransacciones target = new rnSegRolestablastransacciones();
			entSegRolestablastransacciones obj = new entSegRolestablastransacciones();
			cTrans localTrans = new cTrans();
			bool expected = true;
			bool actual;
			
			//obj.Rolsro = 0;
			//obj.Tablasta = "";
			//obj.Transaccionstr = "";
			//obj.Usucresrt = "";
			
			actual = target.Insert(obj, ref localTrans);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegRolestablastransacciones)
		///</summary>
		[TestMethod()]
		public void UpdateTest()
		{
			rnSegRolestablastransacciones target = new rnSegRolestablastransacciones();
			entSegRolestablastransacciones obj = new entSegRolestablastransacciones();
			int expected = 1;
			int actual;
			
			//obj.Rolsro = 0;
			//obj.Tablasta = "";
			//obj.Transaccionstr = "";
			//obj.Apitransaccionsrt = null;
			//obj.Usumodsrt = null;
			
			actual = target.Update(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegRolestablastransacciones, cTrans)
		///</summary>
		[TestMethod()]
		public void UpdateTestTrans()
		{
			rnSegRolestablastransacciones target = new rnSegRolestablastransacciones();
			entSegRolestablastransacciones obj = new entSegRolestablastransacciones();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Rolsro = 0;
			//obj.Tablasta = "";
			//obj.Transaccionstr = "";
			//obj.Apitransaccionsrt = null;
			//obj.Usumodsrt = null;
			
			actual = target.Update(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegRolestablastransacciones)
		///</summary>
		[TestMethod()]
		public void DeleteTest()
		{
			rnSegRolestablastransacciones target = new rnSegRolestablastransacciones();
			entSegRolestablastransacciones obj = new entSegRolestablastransacciones();
			int expected = 1;
			int actual;
			
			//obj.Rolsro = 0;
			//obj.Tablasta = "";
			//obj.Transaccionstr = "";
			
			actual = target.Delete(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegRolestablastransacciones, cTrans)
		///</summary>
		[TestMethod()]
		public void DeleteTestTrans()
		{
			rnSegRolestablastransacciones target = new rnSegRolestablastransacciones();
			entSegRolestablastransacciones obj = new entSegRolestablastransacciones();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Rolsro = 0;
			//obj.Tablasta = "";
			//obj.Transaccionstr = "";
			
			actual = target.Delete(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el método ObtenerLista()
		///</summary>
		[TestMethod()]
		public void ObtenerListaTest()
		{
			rnSegRolestablastransacciones target = new rnSegRolestablastransacciones();
			List<entSegRolestablastransacciones> notExpected = null;
			List<entSegRolestablastransacciones> actual = target.ObtenerLista();
			Assert.AreNotEqual(notExpected, actual);
		}
		
		/// <summary>
		///Prueba para el método CargarDataTable()
		///</summary>
		[TestMethod()]
		public void CargarDataTableTest()
		{
			rnSegRolestablastransacciones target = new rnSegRolestablastransacciones();
			int notExpected = 0; 
			int actualColumns = target.CargarDataTable().Columns.Count;
			Assert.AreNotEqual(notExpected, actualColumns);
		}
		
	}
}

