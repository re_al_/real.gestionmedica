#region 
/***********************************************************************************************************
	NOMBRE:       utSegPaginas
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla SegPaginas

	REVISIONES:
		Ver        FECHA       Autor            Descripción 
		---------  ----------  ---------------  ------------------------------------
		1.0        14/05/2013  R Alonzo Vera A  Creación 

*************************************************************************************************************/
#endregion



#region
using System.Collections.Generic;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GesMed.Conn; 
using GesMed.Class.Entidades;
using GesMed.Class.Modelo;
#endregion

namespace GesMed.Class.Test
{
	[TestClass()]
	public class utSegPaginas
	{
		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion

		/// <summary>
		///Prueba para el metodo Insert(rnSegPaginas)
		///</summary>
		[TestMethod()]
		public void InsertTest()
		{
			rnSegPaginas target = new rnSegPaginas();
			entSegPaginas obj = new entSegPaginas();
			bool expected = true;
			bool actual;
			
			//obj.Paginaspg = 0;
			//obj.Aplicacionsap = "";
			//obj.Paginapadrespg = null;
			//obj.Nombrespg = "";
			//obj.Nombremenuspg = null;
			//obj.Descripcionspg = "";
			//obj.Prioridadspg = null;
			//obj.Usucrespg = "";
			
			actual = target.Insert(obj);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Insert(rnSegPaginas, cTrans)
		///</summary>
		[TestMethod()]
		public void InsertTestTrans()
		{
			rnSegPaginas target = new rnSegPaginas();
			entSegPaginas obj = new entSegPaginas();
			cTrans localTrans = new cTrans();
			bool expected = true;
			bool actual;
			
			//obj.Paginaspg = 0;
			//obj.Aplicacionsap = "";
			//obj.Paginapadrespg = null;
			//obj.Nombrespg = "";
			//obj.Nombremenuspg = null;
			//obj.Descripcionspg = "";
			//obj.Prioridadspg = null;
			//obj.Usucrespg = "";
			
			actual = target.Insert(obj, ref localTrans);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegPaginas)
		///</summary>
		[TestMethod()]
		public void UpdateTest()
		{
			rnSegPaginas target = new rnSegPaginas();
			entSegPaginas obj = new entSegPaginas();
			int expected = 1;
			int actual;
			
			//obj.Aplicacionsap = "";
			//obj.Paginaspg = 0;
			//obj.Paginapadrespg = null;
			//obj.Nombrespg = "";
			//obj.Nombremenuspg = null;
			//obj.Descripcionspg = "";
			//obj.Prioridadspg = null;
			//obj.Apitransaccionspg = "";
			//obj.Usumodspg = null;
			
			actual = target.Update(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegPaginas, cTrans)
		///</summary>
		[TestMethod()]
		public void UpdateTestTrans()
		{
			rnSegPaginas target = new rnSegPaginas();
			entSegPaginas obj = new entSegPaginas();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Aplicacionsap = "";
			//obj.Paginaspg = 0;
			//obj.Paginapadrespg = null;
			//obj.Nombrespg = "";
			//obj.Nombremenuspg = null;
			//obj.Descripcionspg = "";
			//obj.Prioridadspg = null;
			//obj.Apitransaccionspg = "";
			//obj.Usumodspg = null;
			
			actual = target.Update(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegPaginas)
		///</summary>
		[TestMethod()]
		public void DeleteTest()
		{
			rnSegPaginas target = new rnSegPaginas();
			entSegPaginas obj = new entSegPaginas();
			int expected = 1;
			int actual;
			
			//obj.Aplicacionsap = "";
			//obj.Paginaspg = 0;
			
			actual = target.Delete(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegPaginas, cTrans)
		///</summary>
		[TestMethod()]
		public void DeleteTestTrans()
		{
			rnSegPaginas target = new rnSegPaginas();
			entSegPaginas obj = new entSegPaginas();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Aplicacionsap = "";
			//obj.Paginaspg = 0;
			
			actual = target.Delete(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el método ObtenerLista()
		///</summary>
		[TestMethod()]
		public void ObtenerListaTest()
		{
			rnSegPaginas target = new rnSegPaginas();
			List<entSegPaginas> notExpected = null;
			List<entSegPaginas> actual = target.ObtenerLista();
			Assert.AreNotEqual(notExpected, actual);
		}
		
		/// <summary>
		///Prueba para el método CargarDataTable()
		///</summary>
		[TestMethod()]
		public void CargarDataTableTest()
		{
			rnSegPaginas target = new rnSegPaginas();
			int notExpected = 0; 
			int actualColumns = target.CargarDataTable().Columns.Count;
			Assert.AreNotEqual(notExpected, actualColumns);
		}
		
	}
}

