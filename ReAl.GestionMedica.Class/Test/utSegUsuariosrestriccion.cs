#region 
/***********************************************************************************************************
	NOMBRE:       utSegUsuariosrestriccion
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla SegUsuariosrestriccion

	REVISIONES:
		Ver        FECHA       Autor            Descripción 
		---------  ----------  ---------------  ------------------------------------
		1.0        14/05/2013  R Alonzo Vera A  Creación 

*************************************************************************************************************/
#endregion



#region
using System.Collections.Generic;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GesMed.Conn; 
using GesMed.Class.Entidades;
using GesMed.Class.Modelo;
#endregion

namespace GesMed.Class.Test
{
	[TestClass()]
	public class utSegUsuariosrestriccion
	{
		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion

		/// <summary>
		///Prueba para el metodo Insert(rnSegUsuariosrestriccion)
		///</summary>
		[TestMethod()]
		public void InsertTest()
		{
			rnSegUsuariosrestriccion target = new rnSegUsuariosrestriccion();
			entSegUsuariosrestriccion obj = new entSegUsuariosrestriccion();
			bool expected = true;
			bool actual;
			
			//obj.Gestion = 0;
			//obj.Institucioncin = 0;
			//obj.Gacga = null;
			//obj.Uecue = null;
			//obj.Ufcuf = null;
			//obj.Loginsus = "";
			//obj.Rolsro = 0;
			//obj.Rolactivosur = 0;
			//obj.Restriccionsur = "";
			//obj.Vigentesur = 0;
			//obj.Fechavigentesur = null;
			//obj.Usucresur = "";
			//obj.Id = 0;
			
			actual = target.Insert(obj);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Insert(rnSegUsuariosrestriccion, cTrans)
		///</summary>
		[TestMethod()]
		public void InsertTestTrans()
		{
			rnSegUsuariosrestriccion target = new rnSegUsuariosrestriccion();
			entSegUsuariosrestriccion obj = new entSegUsuariosrestriccion();
			cTrans localTrans = new cTrans();
			bool expected = true;
			bool actual;
			
			//obj.Gestion = 0;
			//obj.Institucioncin = 0;
			//obj.Gacga = null;
			//obj.Uecue = null;
			//obj.Ufcuf = null;
			//obj.Loginsus = "";
			//obj.Rolsro = 0;
			//obj.Rolactivosur = 0;
			//obj.Restriccionsur = "";
			//obj.Vigentesur = 0;
			//obj.Fechavigentesur = null;
			//obj.Usucresur = "";
			//obj.Id = 0;
			
			actual = target.Insert(obj, ref localTrans);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegUsuariosrestriccion)
		///</summary>
		[TestMethod()]
		public void UpdateTest()
		{
			rnSegUsuariosrestriccion target = new rnSegUsuariosrestriccion();
			entSegUsuariosrestriccion obj = new entSegUsuariosrestriccion();
			int expected = 1;
			int actual;
			
			//obj.Gestion = 0;
			//obj.Institucioncin = 0;
			//obj.Gacga = null;
			//obj.Uecue = null;
			//obj.Ufcuf = null;
			//obj.Loginsus = "";
			//obj.Rolsro = 0;
			//obj.Rolactivosur = 0;
			//obj.Restriccionsur = "";
			//obj.Vigentesur = 0;
			//obj.Fechavigentesur = null;
			//obj.Apitransaccionsur = "";
			//obj.Usumodsur = null;
			//obj.Id = 0;
			
			actual = target.Update(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegUsuariosrestriccion, cTrans)
		///</summary>
		[TestMethod()]
		public void UpdateTestTrans()
		{
			rnSegUsuariosrestriccion target = new rnSegUsuariosrestriccion();
			entSegUsuariosrestriccion obj = new entSegUsuariosrestriccion();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Gestion = 0;
			//obj.Institucioncin = 0;
			//obj.Gacga = null;
			//obj.Uecue = null;
			//obj.Ufcuf = null;
			//obj.Loginsus = "";
			//obj.Rolsro = 0;
			//obj.Rolactivosur = 0;
			//obj.Restriccionsur = "";
			//obj.Vigentesur = 0;
			//obj.Fechavigentesur = null;
			//obj.Apitransaccionsur = "";
			//obj.Usumodsur = null;
			//obj.Id = 0;
			
			actual = target.Update(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegUsuariosrestriccion)
		///</summary>
		[TestMethod()]
		public void DeleteTest()
		{
			rnSegUsuariosrestriccion target = new rnSegUsuariosrestriccion();
			entSegUsuariosrestriccion obj = new entSegUsuariosrestriccion();
			int expected = 1;
			int actual;
			
			//obj.Id = 0;
			
			actual = target.Delete(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegUsuariosrestriccion, cTrans)
		///</summary>
		[TestMethod()]
		public void DeleteTestTrans()
		{
			rnSegUsuariosrestriccion target = new rnSegUsuariosrestriccion();
			entSegUsuariosrestriccion obj = new entSegUsuariosrestriccion();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Id = 0;
			
			actual = target.Delete(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el método ObtenerLista()
		///</summary>
		[TestMethod()]
		public void ObtenerListaTest()
		{
			rnSegUsuariosrestriccion target = new rnSegUsuariosrestriccion();
			List<entSegUsuariosrestriccion> notExpected = null;
			List<entSegUsuariosrestriccion> actual = target.ObtenerLista();
			Assert.AreNotEqual(notExpected, actual);
		}
		
		/// <summary>
		///Prueba para el método CargarDataTable()
		///</summary>
		[TestMethod()]
		public void CargarDataTableTest()
		{
			rnSegUsuariosrestriccion target = new rnSegUsuariosrestriccion();
			int notExpected = 0; 
			int actualColumns = target.CargarDataTable().Columns.Count;
			Assert.AreNotEqual(notExpected, actualColumns);
		}
		
	}
}

