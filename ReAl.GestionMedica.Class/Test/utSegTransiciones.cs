#region 
/***********************************************************************************************************
	NOMBRE:       utSegTransiciones
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla SegTransiciones

	REVISIONES:
		Ver        FECHA       Autor            Descripción 
		---------  ----------  ---------------  ------------------------------------
		1.0        14/05/2013  R Alonzo Vera A  Creación 

*************************************************************************************************************/
#endregion



#region
using System.Collections.Generic;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GesMed.Conn; 
using GesMed.Class.Entidades;
using GesMed.Class.Modelo;
#endregion

namespace GesMed.Class.Test
{
	[TestClass()]
	public class utSegTransiciones
	{
		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion

		/// <summary>
		///Prueba para el metodo Insert(rnSegTransiciones)
		///</summary>
		[TestMethod()]
		public void InsertTest()
		{
			rnSegTransiciones target = new rnSegTransiciones();
			entSegTransiciones obj = new entSegTransiciones();
			bool expected = true;
			bool actual;
			
			//obj.Tablasta = "";
			//obj.Estadoinicialsts = "";
			//obj.Transaccionstr = "";
			//obj.Estadofinalsts = "";
			//obj.Usucrests = "";
			
			actual = target.Insert(obj);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Insert(rnSegTransiciones, cTrans)
		///</summary>
		[TestMethod()]
		public void InsertTestTrans()
		{
			rnSegTransiciones target = new rnSegTransiciones();
			entSegTransiciones obj = new entSegTransiciones();
			cTrans localTrans = new cTrans();
			bool expected = true;
			bool actual;
			
			//obj.Tablasta = "";
			//obj.Estadoinicialsts = "";
			//obj.Transaccionstr = "";
			//obj.Estadofinalsts = "";
			//obj.Usucrests = "";
			
			actual = target.Insert(obj, ref localTrans);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegTransiciones)
		///</summary>
		[TestMethod()]
		public void UpdateTest()
		{
			rnSegTransiciones target = new rnSegTransiciones();
			entSegTransiciones obj = new entSegTransiciones();
			int expected = 1;
			int actual;
			
			//obj.Tablasta = "";
			//obj.Estadoinicialsts = "";
			//obj.Transaccionstr = "";
			//obj.Estadofinalsts = "";
			//obj.Apitransaccionsts = "";
			//obj.Usumodsts = null;
			
			actual = target.Update(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegTransiciones, cTrans)
		///</summary>
		[TestMethod()]
		public void UpdateTestTrans()
		{
			rnSegTransiciones target = new rnSegTransiciones();
			entSegTransiciones obj = new entSegTransiciones();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Tablasta = "";
			//obj.Estadoinicialsts = "";
			//obj.Transaccionstr = "";
			//obj.Estadofinalsts = "";
			//obj.Apitransaccionsts = "";
			//obj.Usumodsts = null;
			
			actual = target.Update(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegTransiciones)
		///</summary>
		[TestMethod()]
		public void DeleteTest()
		{
			rnSegTransiciones target = new rnSegTransiciones();
			entSegTransiciones obj = new entSegTransiciones();
			int expected = 1;
			int actual;
			
			//obj.Tablasta = "";
			//obj.Estadoinicialsts = "";
			//obj.Transaccionstr = "";
			//obj.Estadofinalsts = "";
			
			actual = target.Delete(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegTransiciones, cTrans)
		///</summary>
		[TestMethod()]
		public void DeleteTestTrans()
		{
			rnSegTransiciones target = new rnSegTransiciones();
			entSegTransiciones obj = new entSegTransiciones();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Tablasta = "";
			//obj.Estadoinicialsts = "";
			//obj.Transaccionstr = "";
			//obj.Estadofinalsts = "";
			
			actual = target.Delete(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el método ObtenerLista()
		///</summary>
		[TestMethod()]
		public void ObtenerListaTest()
		{
			rnSegTransiciones target = new rnSegTransiciones();
			List<entSegTransiciones> notExpected = null;
			List<entSegTransiciones> actual = target.ObtenerLista();
			Assert.AreNotEqual(notExpected, actual);
		}
		
		/// <summary>
		///Prueba para el método CargarDataTable()
		///</summary>
		[TestMethod()]
		public void CargarDataTableTest()
		{
			rnSegTransiciones target = new rnSegTransiciones();
			int notExpected = 0; 
			int actualColumns = target.CargarDataTable().Columns.Count;
			Assert.AreNotEqual(notExpected, actualColumns);
		}
		
	}
}

