#region 
/***********************************************************************************************************
	NOMBRE:       utSegMensajeserror
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla SegMensajeserror

	REVISIONES:
		Ver        FECHA       Autor            Descripción 
		---------  ----------  ---------------  ------------------------------------
		1.0        14/05/2013  R Alonzo Vera A  Creación 

*************************************************************************************************************/
#endregion



#region
using System.Collections.Generic;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GesMed.Conn; 
using GesMed.Class.Entidades;
using GesMed.Class.Modelo;
#endregion

namespace GesMed.Class.Test
{
	[TestClass()]
	public class utSegMensajeserror
	{
		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion

		/// <summary>
		///Prueba para el metodo Insert(rnSegMensajeserror)
		///</summary>
		[TestMethod()]
		public void InsertTest()
		{
			rnSegMensajeserror target = new rnSegMensajeserror();
			entSegMensajeserror obj = new entSegMensajeserror();
			bool expected = true;
			bool actual;
			
			//obj.Errorsme = 0;
			//obj.Aplicacionsap = "";
			//obj.Aplicacionerrorsme = "";
			//obj.Descripcionsme = "";
			//obj.Causasme = "";
			//obj.Accionsme = "";
			//obj.Comentariosme = null;
			//obj.Origensme = null;
			//obj.Usucresme = "";
			
			actual = target.Insert(obj);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Insert(rnSegMensajeserror, cTrans)
		///</summary>
		[TestMethod()]
		public void InsertTestTrans()
		{
			rnSegMensajeserror target = new rnSegMensajeserror();
			entSegMensajeserror obj = new entSegMensajeserror();
			cTrans localTrans = new cTrans();
			bool expected = true;
			bool actual;
			
			//obj.Errorsme = 0;
			//obj.Aplicacionsap = "";
			//obj.Aplicacionerrorsme = "";
			//obj.Descripcionsme = "";
			//obj.Causasme = "";
			//obj.Accionsme = "";
			//obj.Comentariosme = null;
			//obj.Origensme = null;
			//obj.Usucresme = "";
			
			actual = target.Insert(obj, ref localTrans);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegMensajeserror)
		///</summary>
		[TestMethod()]
		public void UpdateTest()
		{
			rnSegMensajeserror target = new rnSegMensajeserror();
			entSegMensajeserror obj = new entSegMensajeserror();
			int expected = 1;
			int actual;
			
			//obj.Errorsme = 0;
			//obj.Aplicacionsap = "";
			//obj.Aplicacionerrorsme = "";
			//obj.Descripcionsme = "";
			//obj.Causasme = "";
			//obj.Accionsme = "";
			//obj.Comentariosme = null;
			//obj.Origensme = null;
			//obj.Apitransaccionsme = "";
			//obj.Usumodsme = null;
			
			actual = target.Update(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegMensajeserror, cTrans)
		///</summary>
		[TestMethod()]
		public void UpdateTestTrans()
		{
			rnSegMensajeserror target = new rnSegMensajeserror();
			entSegMensajeserror obj = new entSegMensajeserror();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Errorsme = 0;
			//obj.Aplicacionsap = "";
			//obj.Aplicacionerrorsme = "";
			//obj.Descripcionsme = "";
			//obj.Causasme = "";
			//obj.Accionsme = "";
			//obj.Comentariosme = null;
			//obj.Origensme = null;
			//obj.Apitransaccionsme = "";
			//obj.Usumodsme = null;
			
			actual = target.Update(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegMensajeserror)
		///</summary>
		[TestMethod()]
		public void DeleteTest()
		{
			rnSegMensajeserror target = new rnSegMensajeserror();
			entSegMensajeserror obj = new entSegMensajeserror();
			int expected = 1;
			int actual;
			
			//obj.Errorsme = 0;
			//obj.Aplicacionsap = "";
			
			actual = target.Delete(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegMensajeserror, cTrans)
		///</summary>
		[TestMethod()]
		public void DeleteTestTrans()
		{
			rnSegMensajeserror target = new rnSegMensajeserror();
			entSegMensajeserror obj = new entSegMensajeserror();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Errorsme = 0;
			//obj.Aplicacionsap = "";
			
			actual = target.Delete(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el método ObtenerLista()
		///</summary>
		[TestMethod()]
		public void ObtenerListaTest()
		{
			rnSegMensajeserror target = new rnSegMensajeserror();
			List<entSegMensajeserror> notExpected = null;
			List<entSegMensajeserror> actual = target.ObtenerLista();
			Assert.AreNotEqual(notExpected, actual);
		}
		
		/// <summary>
		///Prueba para el método CargarDataTable()
		///</summary>
		[TestMethod()]
		public void CargarDataTableTest()
		{
			rnSegMensajeserror target = new rnSegMensajeserror();
			int notExpected = 0; 
			int actualColumns = target.CargarDataTable().Columns.Count;
			Assert.AreNotEqual(notExpected, actualColumns);
		}
		
	}
}

