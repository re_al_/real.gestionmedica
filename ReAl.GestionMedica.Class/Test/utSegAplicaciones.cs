#region 
/***********************************************************************************************************
	NOMBRE:       utSegAplicaciones
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla SegAplicaciones

	REVISIONES:
		Ver        FECHA       Autor            Descripción 
		---------  ----------  ---------------  ------------------------------------
		1.0        14/05/2013  R Alonzo Vera A  Creación 

*************************************************************************************************************/
#endregion



#region
using System.Collections.Generic;
using System.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GesMed.Conn; 
using GesMed.Class.Entidades;
using GesMed.Class.Modelo;
#endregion

namespace GesMed.Class.Test
{
	[TestClass()]
	public class utSegAplicaciones
	{
		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion

		/// <summary>
		///Prueba para el metodo Insert(rnSegAplicaciones)
		///</summary>
		[TestMethod()]
		public void InsertTest()
		{
			rnSegAplicaciones target = new rnSegAplicaciones();
			entSegAplicaciones obj = new entSegAplicaciones();
			bool expected = true;
			bool actual;
			
			//obj.Aplicacionsap = "";
			//obj.Descripcionsap = "";
			//obj.Usucresap = "";
			//obj.Nombresap = null;
			
			actual = target.Insert(obj);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Insert(rnSegAplicaciones, cTrans)
		///</summary>
		[TestMethod()]
		public void InsertTestTrans()
		{
			rnSegAplicaciones target = new rnSegAplicaciones();
			entSegAplicaciones obj = new entSegAplicaciones();
			cTrans localTrans = new cTrans();
			bool expected = true;
			bool actual;
			
			//obj.Aplicacionsap = "";
			//obj.Descripcionsap = "";
			//obj.Usucresap = "";
			//obj.Nombresap = null;
			
			actual = target.Insert(obj, ref localTrans);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegAplicaciones)
		///</summary>
		[TestMethod()]
		public void UpdateTest()
		{
			rnSegAplicaciones target = new rnSegAplicaciones();
			entSegAplicaciones obj = new entSegAplicaciones();
			int expected = 1;
			int actual;
			
			//obj.Aplicacionsap = "";
			//obj.Descripcionsap = "";
			//obj.Apitransaccionsap = "";
			//obj.Usumodsap = null;
			//obj.Nombresap = null;
			
			actual = target.Update(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegAplicaciones, cTrans)
		///</summary>
		[TestMethod()]
		public void UpdateTestTrans()
		{
			rnSegAplicaciones target = new rnSegAplicaciones();
			entSegAplicaciones obj = new entSegAplicaciones();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Aplicacionsap = "";
			//obj.Descripcionsap = "";
			//obj.Apitransaccionsap = "";
			//obj.Usumodsap = null;
			//obj.Nombresap = null;
			
			actual = target.Update(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegAplicaciones)
		///</summary>
		[TestMethod()]
		public void DeleteTest()
		{
			rnSegAplicaciones target = new rnSegAplicaciones();
			entSegAplicaciones obj = new entSegAplicaciones();
			int expected = 1;
			int actual;
			
			//obj.Aplicacionsap = "";
			
			actual = target.Delete(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegAplicaciones, cTrans)
		///</summary>
		[TestMethod()]
		public void DeleteTestTrans()
		{
			rnSegAplicaciones target = new rnSegAplicaciones();
			entSegAplicaciones obj = new entSegAplicaciones();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.Aplicacionsap = "";
			
			actual = target.Delete(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el método ObtenerLista()
		///</summary>
		[TestMethod()]
		public void ObtenerListaTest()
		{
			rnSegAplicaciones target = new rnSegAplicaciones();
			List<entSegAplicaciones> notExpected = null;
			List<entSegAplicaciones> actual = target.ObtenerLista();
			Assert.AreNotEqual(notExpected, actual);
		}
		
		/// <summary>
		///Prueba para el método CargarDataTable()
		///</summary>
		[TestMethod()]
		public void CargarDataTableTest()
		{
			rnSegAplicaciones target = new rnSegAplicaciones();
			int notExpected = 0; 
			int actualColumns = target.CargarDataTable().Columns.Count;
			Assert.AreNotEqual(notExpected, actualColumns);
		}
		
	}
}

