using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Xml.Serialization;

namespace ReAl.GestionMedica.Class
{
    public class CBaseClass : INotifyPropertyChanged, ICloneable
    {
        #region Atributos adicionales

        private string api_estado_original { get; set; }
        private string api_transaccion_orginal { get; set; }

        #endregion Atributos adicionales

        # region IClonable

        public object Clone()
        {
            return MemberwiseClone();
        }

        #endregion

        #region Metodos Privados

        /// <summary>
        /// Obtiene el Hash a partir de un array de Bytes
        /// </summary>
        /// <param name="objectAsBytes"></param>
        /// <returns>string</returns>
        private static string ComputeHash(byte[] objectAsBytes)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            try
            {
                var result = md5.ComputeHash(objectAsBytes);

                var sb = new StringBuilder();
                foreach (var t in result)
                    sb.Append(t.ToString($"X2"));

                return sb.ToString();
            }
            catch (ArgumentNullException)
            {
                return null;
            }
        }

        /// <summary>
        ///     Obtienen el Hash basado en algun algoritmo de Encriptación
        /// </summary>
        /// <typeparam name="T">
        ///     Algoritmo de encriptación
        /// </typeparam>
        /// <param name="cryptoServiceProvider">
        ///     Provvedor de Servicios de Criptografía
        /// </param>
        /// <returns>
        ///     String que representa el Hash calculado
        /// </returns>
        private string ComputeHash<T>(T cryptoServiceProvider) where T : HashAlgorithm, new()
        {
            DataContractSerializer serializer = new DataContractSerializer(this.GetType());
            using (MemoryStream memoryStream = new MemoryStream())
            {
                serializer.WriteObject(memoryStream, this);
                cryptoServiceProvider.ComputeHash(memoryStream.ToArray());
                return Convert.ToBase64String(cryptoServiceProvider.Hash);
            }
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Devuelve un String que representa al Objeto
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            //Evitar parametros NULL
            if (this == null)
                throw new ArgumentNullException($"Parametro NULL no es valido");

            //Se verifica si el objeto es serializable.
            try
            {
                var memStream = new MemoryStream();
                var serializer = new XmlSerializer(typeof(CBaseClass));
                serializer.Serialize(memStream, this);

                //Ahora se obtiene el Hash del Objeto.
                var hashString = ComputeHash(memStream.ToArray());

                return hashString;
            }
            catch (AmbiguousMatchException ame)
            {
                throw new ApplicationException("El Objeto no es Serializable. Message:" + ame.Message);
            }
        }

        /// <summary>
        /// Verifica que dos objetos sean identicos
        /// </summary>
        public static bool operator ==(CBaseClass first, CBaseClass second)
        {
            // Verifica si el puntero en memoria es el mismo
            if (ReferenceEquals(first, second))
                return true;

            // Verifica valores nulos
            if ((object)first == null || (object)second == null)
                return false;

            return first.GetHashCode() == second.GetHashCode();
        }

        /// <summary>
        /// Verifica que dos objetos sean distintos
        /// </summary>
        public static bool operator !=(CBaseClass first, CBaseClass second)
        {
            return !(first == second);
        }

        /// <summary>
        /// Compara este objeto con otro
        /// </summary>
        /// <param name="obj">El objeto a comparar</param>
        /// <returns>Devuelve Verdadero si ambos objetos son iguales</returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (obj.GetType() == GetType())
                return obj.GetHashCode() == GetHashCode();

            return false;
        }

        protected bool Equals(CBaseClass other)
        {
            return api_estado_original == other.api_estado_original && api_transaccion_orginal == other.api_transaccion_orginal;
        }

        #endregion

        #region DataAnnotations

        /// <summary>
        /// Obtiene los errores basado en los DataAnnotations
        /// </summary>
        /// <returns>Devuelve un IList del tipo ValidationResult con los errores obtenidos</returns>
        public IList<ValidationResult> ValidationErrors()
        {
            var context = new ValidationContext(this, null, null);
            IList<ValidationResult> errors = new List<ValidationResult>();

            if (!Validator.TryValidateObject(this, context, errors, true))
                return errors;

            return new List<ValidationResult>(0);
        }

        /// <summary>
        /// Obtiene los errores basado en los DataAnnotations
        /// </summary>
        /// <returns>Devuelve un String con los errores obtenidos</returns>
        public string ValidationErrorsString()
        {
            var strErrors = "";
            var context = new ValidationContext(this, null, null);
            IList<ValidationResult> errors = new List<ValidationResult>();

            if (!Validator.TryValidateObject(this, context, errors, true))
            {
                foreach (var result in errors)
                    strErrors = strErrors + result.ErrorMessage + Environment.NewLine;
            }
            return strErrors;
        }

        /// <summary>
        /// Funcion que determina si un objeto es valido o no
        /// </summary>
        /// <returns>Resultado de la validacion</returns>
        public bool IsValid()
        {
            var context = new ValidationContext(this, null, null);
            IList<ValidationResult> errors = new List<ValidationResult>();

            return Validator.TryValidateObject(this, context, errors, true);
        }

        #endregion

        #region Eventos

        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string prop)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }

        public event PropertyChangingEventHandler PropertyChanging;

        protected void RaisePropertyChanging(string prop)
        {
            PropertyChanging?.Invoke(this, new PropertyChangingEventArgs(prop));

            if (prop.ToLower().StartsWith("apiestado"))
            {
                if (GetType().GetProperty(prop)?.GetValue(this, null) != null)
                    api_estado_original = GetType().GetProperty(prop)?.GetValue(this, null)?.ToString();
            }

            if (prop.ToLower().StartsWith("apitransaccion"))
            {
                if (GetType().GetProperty(prop)?.GetValue(this, null) != null)
                    api_transaccion_orginal = GetType().GetProperty(prop)?.GetValue(this, null)?.ToString();
            }
        }

        #endregion
    }
}