#region 
/***********************************************************************************************************
	NOMBRE:       ICalCitas
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla calcitas

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface ICalCitas: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntCalCitas.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntCalCitas obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntCalCitas obj, string strPropiedad);
		EntCalCitas ObtenerObjetoInsertado(string strUsuCre);
		EntCalCitas ObtenerObjeto(Int64 Int64idcci);
		EntCalCitas ObtenerObjeto(Int64 Int64idcci, ref CTrans localTrans);
		EntCalCitas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntCalCitas ObtenerObjeto(Hashtable htbFiltro);
		EntCalCitas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntCalCitas ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntCalCitas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntCalCitas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntCalCitas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntCalCitas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntCalCitas ObtenerObjeto(EntCalCitas.Fields searchField, object searchValue);
		EntCalCitas ObtenerObjeto(EntCalCitas.Fields searchField, object searchValue, ref CTrans localTrans);
		EntCalCitas ObtenerObjeto(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales);
		EntCalCitas ObtenerObjeto(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntCalCitas> ObtenerDiccionario();
		Dictionary<String, EntCalCitas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntCalCitas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntCalCitas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntCalCitas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntCalCitas> ObtenerDiccionario(EntCalCitas.Fields searchField, object searchValue);
		Dictionary<String, EntCalCitas> ObtenerDiccionario(EntCalCitas.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntCalCitas> ObtenerDiccionario(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntCalCitas> ObtenerDiccionario(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntCalCitas> ObtenerLista();
		List<EntCalCitas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntCalCitas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntCalCitas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntCalCitas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntCalCitas> ObtenerLista(EntCalCitas.Fields searchField, object searchValue);
		List<EntCalCitas> ObtenerLista(EntCalCitas.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntCalCitas> ObtenerLista(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntCalCitas> ObtenerLista(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntCalCitas> ObtenerLista(Hashtable htbFiltro);
		List<EntCalCitas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntCalCitas> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntCalCitas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntCalCitas> ObtenerListaDesdeVista(String strVista);
		List<EntCalCitas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntCalCitas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntCalCitas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntCalCitas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntCalCitas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntCalCitas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntCalCitas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntCalCitas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntCalCitas> ObtenerListaDesdeVista(String strVista, EntCalCitas.Fields searchField, object searchValue);
		List<EntCalCitas> ObtenerListaDesdeVista(String strVista, EntCalCitas.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntCalCitas> ObtenerListaDesdeVista(String strVista, EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntCalCitas> ObtenerListaDesdeVista(String strVista, EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntCalCitas> ObtenerCola();
		Queue<EntCalCitas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntCalCitas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntCalCitas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntCalCitas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntCalCitas> ObtenerCola(EntCalCitas.Fields searchField, object searchValue);
		Queue<EntCalCitas> ObtenerCola(EntCalCitas.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntCalCitas> ObtenerCola(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntCalCitas> ObtenerCola(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntCalCitas> ObtenerPila();
		Stack<EntCalCitas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntCalCitas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntCalCitas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntCalCitas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntCalCitas> ObtenerPila(EntCalCitas.Fields searchField, object searchValue);
		Stack<EntCalCitas> ObtenerPila(EntCalCitas.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntCalCitas> ObtenerPila(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntCalCitas> ObtenerPila(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntCalCitas obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntCalCitas obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntCalCitas obj, bool bValidar = true);
		bool Insert(EntCalCitas obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntCalCitas obj, bool bValidar = true);
		int Update(EntCalCitas obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntCalCitas obj, bool bValidar = true);
		int Delete(EntCalCitas obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntCalCitas obj);
		int InsertUpdate(EntCalCitas obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntCalCitas.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntCalCitas.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntCalCitas.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, String textField, EntCalCitas.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, String textField, EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, EntCalCitas.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntCalCitas.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntCalCitas.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntCalCitas.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntCalCitas.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, String textField, EntCalCitas.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, String textField, EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, EntCalCitas.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntCalCitas.Fields valueField, EntCalCitas.Fields textField, EntCalCitas.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntCalCitas.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntCalCitas.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntCalCitas.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntCalCitas.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntCalCitas.Fields refField);
		int FuncionesCount(EntCalCitas.Fields refField, EntCalCitas.Fields whereField, object valueField);
		int FuncionesCount(EntCalCitas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntCalCitas.Fields refField);
		int FuncionesMin(EntCalCitas.Fields refField, EntCalCitas.Fields whereField, object valueField);
		int FuncionesMin(EntCalCitas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntCalCitas.Fields refField);
		int FuncionesMax(EntCalCitas.Fields refField, EntCalCitas.Fields whereField, object valueField);
		int FuncionesMax(EntCalCitas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntCalCitas.Fields refField);
		int FuncionesSum(EntCalCitas.Fields refField, EntCalCitas.Fields whereField, object valueField);
		int FuncionesSum(EntCalCitas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntCalCitas.Fields refField);
		int FuncionesAvg(EntCalCitas.Fields refField, EntCalCitas.Fields whereField, object valueField);
		int FuncionesAvg(EntCalCitas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

