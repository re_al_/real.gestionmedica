#region 
/***********************************************************************************************************
	NOMBRE:       ISegParametrosdet
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla segparametrosdet

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface ISegParametrosdet: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntSegParametrosdet.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntSegParametrosdet obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntSegParametrosdet obj, string strPropiedad);
		EntSegParametrosdet ObtenerObjetoInsertado(string strUsuCre);
		EntSegParametrosdet ObtenerObjeto(string stringsiglaspa, int intcodigospd);
		EntSegParametrosdet ObtenerObjeto(string stringsiglaspa, int intcodigospd, ref CTrans localTrans);
		EntSegParametrosdet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntSegParametrosdet ObtenerObjeto(Hashtable htbFiltro);
		EntSegParametrosdet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntSegParametrosdet ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntSegParametrosdet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntSegParametrosdet ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntSegParametrosdet ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntSegParametrosdet ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntSegParametrosdet ObtenerObjeto(EntSegParametrosdet.Fields searchField, object searchValue);
		EntSegParametrosdet ObtenerObjeto(EntSegParametrosdet.Fields searchField, object searchValue, ref CTrans localTrans);
		EntSegParametrosdet ObtenerObjeto(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales);
		EntSegParametrosdet ObtenerObjeto(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntSegParametrosdet> ObtenerDiccionario();
		Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(EntSegParametrosdet.Fields searchField, object searchValue);
		Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(EntSegParametrosdet.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntSegParametrosdet> ObtenerDiccionario(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegParametrosdet> ObtenerLista();
		List<EntSegParametrosdet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegParametrosdet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegParametrosdet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegParametrosdet> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegParametrosdet> ObtenerLista(EntSegParametrosdet.Fields searchField, object searchValue);
		List<EntSegParametrosdet> ObtenerLista(EntSegParametrosdet.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegParametrosdet> ObtenerLista(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegParametrosdet> ObtenerLista(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegParametrosdet> ObtenerLista(Hashtable htbFiltro);
		List<EntSegParametrosdet> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegParametrosdet> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegParametrosdet> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista);
		List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, EntSegParametrosdet.Fields searchField, object searchValue);
		List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, EntSegParametrosdet.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegParametrosdet> ObtenerListaDesdeVista(String strVista, EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntSegParametrosdet> ObtenerCola();
		Queue<EntSegParametrosdet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntSegParametrosdet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntSegParametrosdet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntSegParametrosdet> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntSegParametrosdet> ObtenerCola(EntSegParametrosdet.Fields searchField, object searchValue);
		Queue<EntSegParametrosdet> ObtenerCola(EntSegParametrosdet.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntSegParametrosdet> ObtenerCola(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntSegParametrosdet> ObtenerCola(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntSegParametrosdet> ObtenerPila();
		Stack<EntSegParametrosdet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntSegParametrosdet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntSegParametrosdet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntSegParametrosdet> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntSegParametrosdet> ObtenerPila(EntSegParametrosdet.Fields searchField, object searchValue);
		Stack<EntSegParametrosdet> ObtenerPila(EntSegParametrosdet.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntSegParametrosdet> ObtenerPila(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntSegParametrosdet> ObtenerPila(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegParametrosdet obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegParametrosdet obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntSegParametrosdet obj, bool bValidar = true);
		bool Insert(EntSegParametrosdet obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntSegParametrosdet obj, bool bValidar = true);
		int Update(EntSegParametrosdet obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntSegParametrosdet obj, bool bValidar = true);
		int Delete(EntSegParametrosdet obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntSegParametrosdet obj);
		int InsertUpdate(EntSegParametrosdet obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntSegParametrosdet.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegParametrosdet.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegParametrosdet.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, String textField, EntSegParametrosdet.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, String textField, EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, EntSegParametrosdet.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntSegParametrosdet.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntSegParametrosdet.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegParametrosdet.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegParametrosdet.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, String textField, EntSegParametrosdet.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, String textField, EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, EntSegParametrosdet.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegParametrosdet.Fields valueField, EntSegParametrosdet.Fields textField, EntSegParametrosdet.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntSegParametrosdet.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntSegParametrosdet.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegParametrosdet.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegParametrosdet.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntSegParametrosdet.Fields refField);
		int FuncionesCount(EntSegParametrosdet.Fields refField, EntSegParametrosdet.Fields whereField, object valueField);
		int FuncionesCount(EntSegParametrosdet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntSegParametrosdet.Fields refField);
		int FuncionesMin(EntSegParametrosdet.Fields refField, EntSegParametrosdet.Fields whereField, object valueField);
		int FuncionesMin(EntSegParametrosdet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntSegParametrosdet.Fields refField);
		int FuncionesMax(EntSegParametrosdet.Fields refField, EntSegParametrosdet.Fields whereField, object valueField);
		int FuncionesMax(EntSegParametrosdet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntSegParametrosdet.Fields refField);
		int FuncionesSum(EntSegParametrosdet.Fields refField, EntSegParametrosdet.Fields whereField, object valueField);
		int FuncionesSum(EntSegParametrosdet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntSegParametrosdet.Fields refField);
		int FuncionesAvg(EntSegParametrosdet.Fields refField, EntSegParametrosdet.Fields whereField, object valueField);
		int FuncionesAvg(EntSegParametrosdet.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

