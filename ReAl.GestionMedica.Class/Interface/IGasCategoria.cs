#region 
/***********************************************************************************************************
	NOMBRE:       IGasCategoria
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla gascategoria

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IGasCategoria: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntGasCategoria.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntGasCategoria obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntGasCategoria obj, string strPropiedad);
		EntGasCategoria ObtenerObjetoInsertado(string strUsuCre);
		EntGasCategoria ObtenerObjeto(int intidgca);
		EntGasCategoria ObtenerObjeto(int intidgca, ref CTrans localTrans);
		EntGasCategoria ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntGasCategoria ObtenerObjeto(Hashtable htbFiltro);
		EntGasCategoria ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntGasCategoria ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntGasCategoria ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntGasCategoria ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntGasCategoria ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntGasCategoria ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntGasCategoria ObtenerObjeto(EntGasCategoria.Fields searchField, object searchValue);
		EntGasCategoria ObtenerObjeto(EntGasCategoria.Fields searchField, object searchValue, ref CTrans localTrans);
		EntGasCategoria ObtenerObjeto(EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales);
		EntGasCategoria ObtenerObjeto(EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntGasCategoria> ObtenerDiccionario();
		Dictionary<String, EntGasCategoria> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntGasCategoria> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntGasCategoria> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntGasCategoria> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntGasCategoria> ObtenerDiccionario(EntGasCategoria.Fields searchField, object searchValue);
		Dictionary<String, EntGasCategoria> ObtenerDiccionario(EntGasCategoria.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntGasCategoria> ObtenerDiccionario(EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntGasCategoria> ObtenerDiccionario(EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntGasCategoria> ObtenerLista();
		List<EntGasCategoria> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntGasCategoria> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntGasCategoria> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntGasCategoria> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntGasCategoria> ObtenerLista(EntGasCategoria.Fields searchField, object searchValue);
		List<EntGasCategoria> ObtenerLista(EntGasCategoria.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntGasCategoria> ObtenerLista(EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntGasCategoria> ObtenerLista(EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntGasCategoria> ObtenerLista(Hashtable htbFiltro);
		List<EntGasCategoria> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntGasCategoria> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntGasCategoria> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntGasCategoria> ObtenerListaDesdeVista(String strVista);
		List<EntGasCategoria> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntGasCategoria> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntGasCategoria> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntGasCategoria> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntGasCategoria> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntGasCategoria> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntGasCategoria> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntGasCategoria> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntGasCategoria> ObtenerListaDesdeVista(String strVista, EntGasCategoria.Fields searchField, object searchValue);
		List<EntGasCategoria> ObtenerListaDesdeVista(String strVista, EntGasCategoria.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntGasCategoria> ObtenerListaDesdeVista(String strVista, EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntGasCategoria> ObtenerListaDesdeVista(String strVista, EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntGasCategoria> ObtenerCola();
		Queue<EntGasCategoria> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntGasCategoria> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntGasCategoria> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntGasCategoria> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntGasCategoria> ObtenerCola(EntGasCategoria.Fields searchField, object searchValue);
		Queue<EntGasCategoria> ObtenerCola(EntGasCategoria.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntGasCategoria> ObtenerCola(EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntGasCategoria> ObtenerCola(EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntGasCategoria> ObtenerPila();
		Stack<EntGasCategoria> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntGasCategoria> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntGasCategoria> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntGasCategoria> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntGasCategoria> ObtenerPila(EntGasCategoria.Fields searchField, object searchValue);
		Stack<EntGasCategoria> ObtenerPila(EntGasCategoria.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntGasCategoria> ObtenerPila(EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntGasCategoria> ObtenerPila(EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntGasCategoria obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntGasCategoria obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntGasCategoria obj, bool bValidar = true);
		bool Insert(EntGasCategoria obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntGasCategoria obj, bool bValidar = true);
		int Update(EntGasCategoria obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntGasCategoria obj, bool bValidar = true);
		int Delete(EntGasCategoria obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntGasCategoria obj);
		int InsertUpdate(EntGasCategoria obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntGasCategoria.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntGasCategoria.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntGasCategoria.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntGasCategoria.Fields valueField, EntGasCategoria.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntGasCategoria.Fields valueField, EntGasCategoria.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntGasCategoria.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntGasCategoria.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntGasCategoria.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntGasCategoria.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntGasCategoria.Fields valueField, String textField, EntGasCategoria.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntGasCategoria.Fields valueField, String textField, EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntGasCategoria.Fields valueField, EntGasCategoria.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntGasCategoria.Fields valueField, EntGasCategoria.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntGasCategoria.Fields valueField, EntGasCategoria.Fields textField, EntGasCategoria.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntGasCategoria.Fields valueField, EntGasCategoria.Fields textField, EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntGasCategoria.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntGasCategoria.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntGasCategoria.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntGasCategoria.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntGasCategoria.Fields valueField, EntGasCategoria.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntGasCategoria.Fields valueField, EntGasCategoria.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntGasCategoria.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntGasCategoria.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntGasCategoria.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntGasCategoria.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntGasCategoria.Fields valueField, String textField, EntGasCategoria.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntGasCategoria.Fields valueField, String textField, EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntGasCategoria.Fields valueField, EntGasCategoria.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntGasCategoria.Fields valueField, EntGasCategoria.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntGasCategoria.Fields valueField, EntGasCategoria.Fields textField, EntGasCategoria.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntGasCategoria.Fields valueField, EntGasCategoria.Fields textField, EntGasCategoria.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntGasCategoria.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntGasCategoria.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntGasCategoria.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntGasCategoria.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntGasCategoria.Fields refField);
		int FuncionesCount(EntGasCategoria.Fields refField, EntGasCategoria.Fields whereField, object valueField);
		int FuncionesCount(EntGasCategoria.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntGasCategoria.Fields refField);
		int FuncionesMin(EntGasCategoria.Fields refField, EntGasCategoria.Fields whereField, object valueField);
		int FuncionesMin(EntGasCategoria.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntGasCategoria.Fields refField);
		int FuncionesMax(EntGasCategoria.Fields refField, EntGasCategoria.Fields whereField, object valueField);
		int FuncionesMax(EntGasCategoria.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntGasCategoria.Fields refField);
		int FuncionesSum(EntGasCategoria.Fields refField, EntGasCategoria.Fields whereField, object valueField);
		int FuncionesSum(EntGasCategoria.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntGasCategoria.Fields refField);
		int FuncionesAvg(EntGasCategoria.Fields refField, EntGasCategoria.Fields whereField, object valueField);
		int FuncionesAvg(EntGasCategoria.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

