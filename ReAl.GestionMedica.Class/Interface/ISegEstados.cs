#region 
/***********************************************************************************************************
	NOMBRE:       ISegEstados
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla segestados

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface ISegEstados: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntSegEstados.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntSegEstados obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntSegEstados obj, string strPropiedad);
		EntSegEstados ObtenerObjetoInsertado(string strUsuCre);
		EntSegEstados ObtenerObjeto(string stringtablasta, string stringestadoses);
		EntSegEstados ObtenerObjeto(string stringtablasta, string stringestadoses, ref CTrans localTrans);
		EntSegEstados ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntSegEstados ObtenerObjeto(Hashtable htbFiltro);
		EntSegEstados ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntSegEstados ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntSegEstados ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntSegEstados ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntSegEstados ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntSegEstados ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntSegEstados ObtenerObjeto(EntSegEstados.Fields searchField, object searchValue);
		EntSegEstados ObtenerObjeto(EntSegEstados.Fields searchField, object searchValue, ref CTrans localTrans);
		EntSegEstados ObtenerObjeto(EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales);
		EntSegEstados ObtenerObjeto(EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntSegEstados> ObtenerDiccionario();
		Dictionary<String, EntSegEstados> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntSegEstados> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntSegEstados> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntSegEstados> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntSegEstados> ObtenerDiccionario(EntSegEstados.Fields searchField, object searchValue);
		Dictionary<String, EntSegEstados> ObtenerDiccionario(EntSegEstados.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntSegEstados> ObtenerDiccionario(EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntSegEstados> ObtenerDiccionario(EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegEstados> ObtenerLista();
		List<EntSegEstados> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegEstados> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegEstados> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegEstados> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegEstados> ObtenerLista(EntSegEstados.Fields searchField, object searchValue);
		List<EntSegEstados> ObtenerLista(EntSegEstados.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegEstados> ObtenerLista(EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegEstados> ObtenerLista(EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegEstados> ObtenerLista(Hashtable htbFiltro);
		List<EntSegEstados> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegEstados> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegEstados> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegEstados> ObtenerListaDesdeVista(String strVista);
		List<EntSegEstados> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegEstados> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegEstados> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegEstados> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegEstados> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntSegEstados> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegEstados> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegEstados> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegEstados> ObtenerListaDesdeVista(String strVista, EntSegEstados.Fields searchField, object searchValue);
		List<EntSegEstados> ObtenerListaDesdeVista(String strVista, EntSegEstados.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegEstados> ObtenerListaDesdeVista(String strVista, EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegEstados> ObtenerListaDesdeVista(String strVista, EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntSegEstados> ObtenerCola();
		Queue<EntSegEstados> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntSegEstados> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntSegEstados> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntSegEstados> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntSegEstados> ObtenerCola(EntSegEstados.Fields searchField, object searchValue);
		Queue<EntSegEstados> ObtenerCola(EntSegEstados.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntSegEstados> ObtenerCola(EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntSegEstados> ObtenerCola(EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntSegEstados> ObtenerPila();
		Stack<EntSegEstados> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntSegEstados> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntSegEstados> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntSegEstados> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntSegEstados> ObtenerPila(EntSegEstados.Fields searchField, object searchValue);
		Stack<EntSegEstados> ObtenerPila(EntSegEstados.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntSegEstados> ObtenerPila(EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntSegEstados> ObtenerPila(EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegEstados obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegEstados obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntSegEstados obj, bool bValidar = true);
		bool Insert(EntSegEstados obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntSegEstados obj, bool bValidar = true);
		int Update(EntSegEstados obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntSegEstados obj, bool bValidar = true);
		int Delete(EntSegEstados obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntSegEstados obj);
		int InsertUpdate(EntSegEstados obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntSegEstados.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegEstados.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegEstados.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegEstados.Fields valueField, EntSegEstados.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegEstados.Fields valueField, EntSegEstados.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegEstados.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegEstados.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegEstados.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegEstados.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegEstados.Fields valueField, String textField, EntSegEstados.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegEstados.Fields valueField, String textField, EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegEstados.Fields valueField, EntSegEstados.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegEstados.Fields valueField, EntSegEstados.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegEstados.Fields valueField, EntSegEstados.Fields textField, EntSegEstados.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegEstados.Fields valueField, EntSegEstados.Fields textField, EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntSegEstados.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntSegEstados.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegEstados.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegEstados.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegEstados.Fields valueField, EntSegEstados.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntSegEstados.Fields valueField, EntSegEstados.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegEstados.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntSegEstados.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegEstados.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegEstados.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegEstados.Fields valueField, String textField, EntSegEstados.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegEstados.Fields valueField, String textField, EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegEstados.Fields valueField, EntSegEstados.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegEstados.Fields valueField, EntSegEstados.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegEstados.Fields valueField, EntSegEstados.Fields textField, EntSegEstados.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegEstados.Fields valueField, EntSegEstados.Fields textField, EntSegEstados.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntSegEstados.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntSegEstados.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegEstados.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegEstados.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntSegEstados.Fields refField);
		int FuncionesCount(EntSegEstados.Fields refField, EntSegEstados.Fields whereField, object valueField);
		int FuncionesCount(EntSegEstados.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntSegEstados.Fields refField);
		int FuncionesMin(EntSegEstados.Fields refField, EntSegEstados.Fields whereField, object valueField);
		int FuncionesMin(EntSegEstados.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntSegEstados.Fields refField);
		int FuncionesMax(EntSegEstados.Fields refField, EntSegEstados.Fields whereField, object valueField);
		int FuncionesMax(EntSegEstados.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntSegEstados.Fields refField);
		int FuncionesSum(EntSegEstados.Fields refField, EntSegEstados.Fields whereField, object valueField);
		int FuncionesSum(EntSegEstados.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntSegEstados.Fields refField);
		int FuncionesAvg(EntSegEstados.Fields refField, EntSegEstados.Fields whereField, object valueField);
		int FuncionesAvg(EntSegEstados.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

