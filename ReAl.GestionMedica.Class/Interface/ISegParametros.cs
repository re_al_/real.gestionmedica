#region 
/***********************************************************************************************************
	NOMBRE:       ISegParametros
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla segparametros

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface ISegParametros: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntSegParametros.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntSegParametros obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntSegParametros obj, string strPropiedad);
		EntSegParametros ObtenerObjetoInsertado(string strUsuCre);
		EntSegParametros ObtenerObjeto(string stringsiglaspa);
		EntSegParametros ObtenerObjeto(string stringsiglaspa, ref CTrans localTrans);
		EntSegParametros ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntSegParametros ObtenerObjeto(Hashtable htbFiltro);
		EntSegParametros ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntSegParametros ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntSegParametros ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntSegParametros ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntSegParametros ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntSegParametros ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntSegParametros ObtenerObjeto(EntSegParametros.Fields searchField, object searchValue);
		EntSegParametros ObtenerObjeto(EntSegParametros.Fields searchField, object searchValue, ref CTrans localTrans);
		EntSegParametros ObtenerObjeto(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales);
		EntSegParametros ObtenerObjeto(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntSegParametros> ObtenerDiccionario();
		Dictionary<String, EntSegParametros> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntSegParametros> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntSegParametros> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntSegParametros> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntSegParametros> ObtenerDiccionario(EntSegParametros.Fields searchField, object searchValue);
		Dictionary<String, EntSegParametros> ObtenerDiccionario(EntSegParametros.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntSegParametros> ObtenerDiccionario(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntSegParametros> ObtenerDiccionario(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegParametros> ObtenerLista();
		List<EntSegParametros> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegParametros> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegParametros> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegParametros> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegParametros> ObtenerLista(EntSegParametros.Fields searchField, object searchValue);
		List<EntSegParametros> ObtenerLista(EntSegParametros.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegParametros> ObtenerLista(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegParametros> ObtenerLista(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegParametros> ObtenerLista(Hashtable htbFiltro);
		List<EntSegParametros> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegParametros> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegParametros> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegParametros> ObtenerListaDesdeVista(String strVista);
		List<EntSegParametros> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegParametros> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegParametros> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegParametros> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegParametros> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntSegParametros> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegParametros> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegParametros> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegParametros> ObtenerListaDesdeVista(String strVista, EntSegParametros.Fields searchField, object searchValue);
		List<EntSegParametros> ObtenerListaDesdeVista(String strVista, EntSegParametros.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegParametros> ObtenerListaDesdeVista(String strVista, EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegParametros> ObtenerListaDesdeVista(String strVista, EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntSegParametros> ObtenerCola();
		Queue<EntSegParametros> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntSegParametros> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntSegParametros> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntSegParametros> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntSegParametros> ObtenerCola(EntSegParametros.Fields searchField, object searchValue);
		Queue<EntSegParametros> ObtenerCola(EntSegParametros.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntSegParametros> ObtenerCola(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntSegParametros> ObtenerCola(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntSegParametros> ObtenerPila();
		Stack<EntSegParametros> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntSegParametros> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntSegParametros> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntSegParametros> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntSegParametros> ObtenerPila(EntSegParametros.Fields searchField, object searchValue);
		Stack<EntSegParametros> ObtenerPila(EntSegParametros.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntSegParametros> ObtenerPila(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntSegParametros> ObtenerPila(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegParametros obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegParametros obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntSegParametros obj, bool bValidar = true);
		bool Insert(EntSegParametros obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntSegParametros obj, bool bValidar = true);
		int Update(EntSegParametros obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntSegParametros obj, bool bValidar = true);
		int Delete(EntSegParametros obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntSegParametros obj);
		int InsertUpdate(EntSegParametros obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntSegParametros.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegParametros.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegParametros.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, String textField, EntSegParametros.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, String textField, EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, EntSegParametros.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntSegParametros.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntSegParametros.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegParametros.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegParametros.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, String textField, EntSegParametros.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, String textField, EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, EntSegParametros.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegParametros.Fields valueField, EntSegParametros.Fields textField, EntSegParametros.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntSegParametros.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntSegParametros.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegParametros.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegParametros.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntSegParametros.Fields refField);
		int FuncionesCount(EntSegParametros.Fields refField, EntSegParametros.Fields whereField, object valueField);
		int FuncionesCount(EntSegParametros.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntSegParametros.Fields refField);
		int FuncionesMin(EntSegParametros.Fields refField, EntSegParametros.Fields whereField, object valueField);
		int FuncionesMin(EntSegParametros.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntSegParametros.Fields refField);
		int FuncionesMax(EntSegParametros.Fields refField, EntSegParametros.Fields whereField, object valueField);
		int FuncionesMax(EntSegParametros.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntSegParametros.Fields refField);
		int FuncionesSum(EntSegParametros.Fields refField, EntSegParametros.Fields whereField, object valueField);
		int FuncionesSum(EntSegParametros.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntSegParametros.Fields refField);
		int FuncionesAvg(EntSegParametros.Fields refField, EntSegParametros.Fields whereField, object valueField);
		int FuncionesAvg(EntSegParametros.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

