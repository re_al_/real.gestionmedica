#region 
/***********************************************************************************************************
	NOMBRE:       IClaCie
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla clacie

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IClaCie: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntClaCie.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntClaCie obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntClaCie obj, string strPropiedad);
		EntClaCie ObtenerObjetoInsertado(string strUsuCre);
		EntClaCie ObtenerObjeto(string stringcodcie);
		EntClaCie ObtenerObjeto(string stringcodcie, ref CTrans localTrans);
		EntClaCie ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntClaCie ObtenerObjeto(Hashtable htbFiltro);
		EntClaCie ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntClaCie ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntClaCie ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntClaCie ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntClaCie ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntClaCie ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntClaCie ObtenerObjeto(EntClaCie.Fields searchField, object searchValue);
		EntClaCie ObtenerObjeto(EntClaCie.Fields searchField, object searchValue, ref CTrans localTrans);
		EntClaCie ObtenerObjeto(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales);
		EntClaCie ObtenerObjeto(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntClaCie> ObtenerDiccionario();
		Dictionary<String, EntClaCie> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntClaCie> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntClaCie> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntClaCie> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntClaCie> ObtenerDiccionario(EntClaCie.Fields searchField, object searchValue);
		Dictionary<String, EntClaCie> ObtenerDiccionario(EntClaCie.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntClaCie> ObtenerDiccionario(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntClaCie> ObtenerDiccionario(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntClaCie> ObtenerLista();
		List<EntClaCie> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntClaCie> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntClaCie> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntClaCie> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntClaCie> ObtenerLista(EntClaCie.Fields searchField, object searchValue);
		List<EntClaCie> ObtenerLista(EntClaCie.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntClaCie> ObtenerLista(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntClaCie> ObtenerLista(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntClaCie> ObtenerLista(Hashtable htbFiltro);
		List<EntClaCie> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntClaCie> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntClaCie> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntClaCie> ObtenerListaDesdeVista(String strVista);
		List<EntClaCie> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntClaCie> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntClaCie> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntClaCie> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntClaCie> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntClaCie> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntClaCie> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntClaCie> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntClaCie> ObtenerListaDesdeVista(String strVista, EntClaCie.Fields searchField, object searchValue);
		List<EntClaCie> ObtenerListaDesdeVista(String strVista, EntClaCie.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntClaCie> ObtenerListaDesdeVista(String strVista, EntClaCie.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntClaCie> ObtenerListaDesdeVista(String strVista, EntClaCie.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntClaCie> ObtenerCola();
		Queue<EntClaCie> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntClaCie> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntClaCie> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntClaCie> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntClaCie> ObtenerCola(EntClaCie.Fields searchField, object searchValue);
		Queue<EntClaCie> ObtenerCola(EntClaCie.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntClaCie> ObtenerCola(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntClaCie> ObtenerCola(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntClaCie> ObtenerPila();
		Stack<EntClaCie> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntClaCie> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntClaCie> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntClaCie> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntClaCie> ObtenerPila(EntClaCie.Fields searchField, object searchValue);
		Stack<EntClaCie> ObtenerPila(EntClaCie.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntClaCie> ObtenerPila(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntClaCie> ObtenerPila(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntClaCie obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntClaCie obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		int InsertUpdate(EntClaCie obj);
		int InsertUpdate(EntClaCie obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntClaCie.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntClaCie.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntClaCie.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntClaCie.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, String textField, EntClaCie.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, String textField, EntClaCie.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, EntClaCie.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, EntClaCie.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntClaCie.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntClaCie.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntClaCie.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntClaCie.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, String textField, EntClaCie.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, String textField, EntClaCie.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, EntClaCie.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntClaCie.Fields valueField, EntClaCie.Fields textField, EntClaCie.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntClaCie.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntClaCie.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntClaCie.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntClaCie.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntClaCie.Fields refField);
		int FuncionesCount(EntClaCie.Fields refField, EntClaCie.Fields whereField, object valueField);
		int FuncionesCount(EntClaCie.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntClaCie.Fields refField);
		int FuncionesMin(EntClaCie.Fields refField, EntClaCie.Fields whereField, object valueField);
		int FuncionesMin(EntClaCie.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntClaCie.Fields refField);
		int FuncionesMax(EntClaCie.Fields refField, EntClaCie.Fields whereField, object valueField);
		int FuncionesMax(EntClaCie.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntClaCie.Fields refField);
		int FuncionesSum(EntClaCie.Fields refField, EntClaCie.Fields whereField, object valueField);
		int FuncionesSum(EntClaCie.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntClaCie.Fields refField);
		int FuncionesAvg(EntClaCie.Fields refField, EntClaCie.Fields whereField, object valueField);
		int FuncionesAvg(EntClaCie.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

