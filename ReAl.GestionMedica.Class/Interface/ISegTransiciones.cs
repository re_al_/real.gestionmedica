#region 
/***********************************************************************************************************
	NOMBRE:       ISegTransiciones
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla segtransiciones

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface ISegTransiciones: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntSegTransiciones.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntSegTransiciones obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntSegTransiciones obj, string strPropiedad);
		EntSegTransiciones ObtenerObjetoInsertado(string strUsuCre);
		EntSegTransiciones ObtenerObjeto(string stringtablasta, string stringestadoinicialsts, string stringtransaccionstr, string stringestadofinalsts);
		EntSegTransiciones ObtenerObjeto(string stringtablasta, string stringestadoinicialsts, string stringtransaccionstr, string stringestadofinalsts, ref CTrans localTrans);
		EntSegTransiciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntSegTransiciones ObtenerObjeto(Hashtable htbFiltro);
		EntSegTransiciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntSegTransiciones ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntSegTransiciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntSegTransiciones ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntSegTransiciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntSegTransiciones ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntSegTransiciones ObtenerObjeto(EntSegTransiciones.Fields searchField, object searchValue);
		EntSegTransiciones ObtenerObjeto(EntSegTransiciones.Fields searchField, object searchValue, ref CTrans localTrans);
		EntSegTransiciones ObtenerObjeto(EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales);
		EntSegTransiciones ObtenerObjeto(EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntSegTransiciones> ObtenerDiccionario();
		Dictionary<String, EntSegTransiciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntSegTransiciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntSegTransiciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntSegTransiciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntSegTransiciones> ObtenerDiccionario(EntSegTransiciones.Fields searchField, object searchValue);
		Dictionary<String, EntSegTransiciones> ObtenerDiccionario(EntSegTransiciones.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntSegTransiciones> ObtenerDiccionario(EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntSegTransiciones> ObtenerDiccionario(EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegTransiciones> ObtenerLista();
		List<EntSegTransiciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegTransiciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegTransiciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegTransiciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegTransiciones> ObtenerLista(EntSegTransiciones.Fields searchField, object searchValue);
		List<EntSegTransiciones> ObtenerLista(EntSegTransiciones.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegTransiciones> ObtenerLista(EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegTransiciones> ObtenerLista(EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegTransiciones> ObtenerLista(Hashtable htbFiltro);
		List<EntSegTransiciones> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegTransiciones> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegTransiciones> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegTransiciones> ObtenerListaDesdeVista(String strVista);
		List<EntSegTransiciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegTransiciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegTransiciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegTransiciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegTransiciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntSegTransiciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegTransiciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegTransiciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegTransiciones> ObtenerListaDesdeVista(String strVista, EntSegTransiciones.Fields searchField, object searchValue);
		List<EntSegTransiciones> ObtenerListaDesdeVista(String strVista, EntSegTransiciones.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegTransiciones> ObtenerListaDesdeVista(String strVista, EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegTransiciones> ObtenerListaDesdeVista(String strVista, EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntSegTransiciones> ObtenerCola();
		Queue<EntSegTransiciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntSegTransiciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntSegTransiciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntSegTransiciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntSegTransiciones> ObtenerCola(EntSegTransiciones.Fields searchField, object searchValue);
		Queue<EntSegTransiciones> ObtenerCola(EntSegTransiciones.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntSegTransiciones> ObtenerCola(EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntSegTransiciones> ObtenerCola(EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntSegTransiciones> ObtenerPila();
		Stack<EntSegTransiciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntSegTransiciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntSegTransiciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntSegTransiciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntSegTransiciones> ObtenerPila(EntSegTransiciones.Fields searchField, object searchValue);
		Stack<EntSegTransiciones> ObtenerPila(EntSegTransiciones.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntSegTransiciones> ObtenerPila(EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntSegTransiciones> ObtenerPila(EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegTransiciones obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegTransiciones obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntSegTransiciones obj, bool bValidar = true);
		bool Insert(EntSegTransiciones obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntSegTransiciones obj, bool bValidar = true);
		int Update(EntSegTransiciones obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntSegTransiciones obj, bool bValidar = true);
		int Delete(EntSegTransiciones obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntSegTransiciones obj);
		int InsertUpdate(EntSegTransiciones obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntSegTransiciones.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegTransiciones.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegTransiciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransiciones.Fields valueField, EntSegTransiciones.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransiciones.Fields valueField, EntSegTransiciones.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransiciones.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransiciones.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransiciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransiciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransiciones.Fields valueField, String textField, EntSegTransiciones.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransiciones.Fields valueField, String textField, EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransiciones.Fields valueField, EntSegTransiciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransiciones.Fields valueField, EntSegTransiciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransiciones.Fields valueField, EntSegTransiciones.Fields textField, EntSegTransiciones.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransiciones.Fields valueField, EntSegTransiciones.Fields textField, EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntSegTransiciones.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntSegTransiciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegTransiciones.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegTransiciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTransiciones.Fields valueField, EntSegTransiciones.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntSegTransiciones.Fields valueField, EntSegTransiciones.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTransiciones.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntSegTransiciones.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTransiciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegTransiciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTransiciones.Fields valueField, String textField, EntSegTransiciones.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegTransiciones.Fields valueField, String textField, EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTransiciones.Fields valueField, EntSegTransiciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegTransiciones.Fields valueField, EntSegTransiciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTransiciones.Fields valueField, EntSegTransiciones.Fields textField, EntSegTransiciones.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegTransiciones.Fields valueField, EntSegTransiciones.Fields textField, EntSegTransiciones.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntSegTransiciones.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntSegTransiciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegTransiciones.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegTransiciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntSegTransiciones.Fields refField);
		int FuncionesCount(EntSegTransiciones.Fields refField, EntSegTransiciones.Fields whereField, object valueField);
		int FuncionesCount(EntSegTransiciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntSegTransiciones.Fields refField);
		int FuncionesMin(EntSegTransiciones.Fields refField, EntSegTransiciones.Fields whereField, object valueField);
		int FuncionesMin(EntSegTransiciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntSegTransiciones.Fields refField);
		int FuncionesMax(EntSegTransiciones.Fields refField, EntSegTransiciones.Fields whereField, object valueField);
		int FuncionesMax(EntSegTransiciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntSegTransiciones.Fields refField);
		int FuncionesSum(EntSegTransiciones.Fields refField, EntSegTransiciones.Fields whereField, object valueField);
		int FuncionesSum(EntSegTransiciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntSegTransiciones.Fields refField);
		int FuncionesAvg(EntSegTransiciones.Fields refField, EntSegTransiciones.Fields whereField, object valueField);
		int FuncionesAvg(EntSegTransiciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

