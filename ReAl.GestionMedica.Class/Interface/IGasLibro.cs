#region 
/***********************************************************************************************************
	NOMBRE:       IGasLibro
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla gaslibro

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IGasLibro: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntGasLibro.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntGasLibro obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntGasLibro obj, string strPropiedad);
		EntGasLibro ObtenerObjetoInsertado(string strUsuCre);
		EntGasLibro ObtenerObjeto(Int64 Int64idgli);
		EntGasLibro ObtenerObjeto(Int64 Int64idgli, ref CTrans localTrans);
		EntGasLibro ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntGasLibro ObtenerObjeto(Hashtable htbFiltro);
		EntGasLibro ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntGasLibro ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntGasLibro ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntGasLibro ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntGasLibro ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntGasLibro ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntGasLibro ObtenerObjeto(EntGasLibro.Fields searchField, object searchValue);
		EntGasLibro ObtenerObjeto(EntGasLibro.Fields searchField, object searchValue, ref CTrans localTrans);
		EntGasLibro ObtenerObjeto(EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales);
		EntGasLibro ObtenerObjeto(EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntGasLibro> ObtenerDiccionario();
		Dictionary<String, EntGasLibro> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntGasLibro> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntGasLibro> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntGasLibro> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntGasLibro> ObtenerDiccionario(EntGasLibro.Fields searchField, object searchValue);
		Dictionary<String, EntGasLibro> ObtenerDiccionario(EntGasLibro.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntGasLibro> ObtenerDiccionario(EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntGasLibro> ObtenerDiccionario(EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntGasLibro> ObtenerLista();
		List<EntGasLibro> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntGasLibro> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntGasLibro> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntGasLibro> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntGasLibro> ObtenerLista(EntGasLibro.Fields searchField, object searchValue);
		List<EntGasLibro> ObtenerLista(EntGasLibro.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntGasLibro> ObtenerLista(EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntGasLibro> ObtenerLista(EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntGasLibro> ObtenerLista(Hashtable htbFiltro);
		List<EntGasLibro> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntGasLibro> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntGasLibro> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntGasLibro> ObtenerListaDesdeVista(String strVista);
		List<EntGasLibro> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntGasLibro> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntGasLibro> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntGasLibro> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntGasLibro> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntGasLibro> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntGasLibro> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntGasLibro> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntGasLibro> ObtenerListaDesdeVista(String strVista, EntGasLibro.Fields searchField, object searchValue);
		List<EntGasLibro> ObtenerListaDesdeVista(String strVista, EntGasLibro.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntGasLibro> ObtenerListaDesdeVista(String strVista, EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntGasLibro> ObtenerListaDesdeVista(String strVista, EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntGasLibro> ObtenerCola();
		Queue<EntGasLibro> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntGasLibro> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntGasLibro> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntGasLibro> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntGasLibro> ObtenerCola(EntGasLibro.Fields searchField, object searchValue);
		Queue<EntGasLibro> ObtenerCola(EntGasLibro.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntGasLibro> ObtenerCola(EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntGasLibro> ObtenerCola(EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntGasLibro> ObtenerPila();
		Stack<EntGasLibro> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntGasLibro> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntGasLibro> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntGasLibro> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntGasLibro> ObtenerPila(EntGasLibro.Fields searchField, object searchValue);
		Stack<EntGasLibro> ObtenerPila(EntGasLibro.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntGasLibro> ObtenerPila(EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntGasLibro> ObtenerPila(EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntGasLibro obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntGasLibro obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntGasLibro obj, bool bValidar = true);
		bool Insert(EntGasLibro obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntGasLibro obj, bool bValidar = true);
		int Update(EntGasLibro obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntGasLibro obj, bool bValidar = true);
		int Delete(EntGasLibro obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntGasLibro obj);
		int InsertUpdate(EntGasLibro obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntGasLibro.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntGasLibro.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntGasLibro.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntGasLibro.Fields valueField, EntGasLibro.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntGasLibro.Fields valueField, EntGasLibro.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntGasLibro.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntGasLibro.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntGasLibro.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntGasLibro.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntGasLibro.Fields valueField, String textField, EntGasLibro.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntGasLibro.Fields valueField, String textField, EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntGasLibro.Fields valueField, EntGasLibro.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntGasLibro.Fields valueField, EntGasLibro.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntGasLibro.Fields valueField, EntGasLibro.Fields textField, EntGasLibro.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntGasLibro.Fields valueField, EntGasLibro.Fields textField, EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntGasLibro.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntGasLibro.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntGasLibro.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntGasLibro.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntGasLibro.Fields valueField, EntGasLibro.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntGasLibro.Fields valueField, EntGasLibro.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntGasLibro.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntGasLibro.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntGasLibro.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntGasLibro.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntGasLibro.Fields valueField, String textField, EntGasLibro.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntGasLibro.Fields valueField, String textField, EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntGasLibro.Fields valueField, EntGasLibro.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntGasLibro.Fields valueField, EntGasLibro.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntGasLibro.Fields valueField, EntGasLibro.Fields textField, EntGasLibro.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntGasLibro.Fields valueField, EntGasLibro.Fields textField, EntGasLibro.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntGasLibro.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntGasLibro.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntGasLibro.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntGasLibro.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntGasLibro.Fields refField);
		int FuncionesCount(EntGasLibro.Fields refField, EntGasLibro.Fields whereField, object valueField);
		int FuncionesCount(EntGasLibro.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntGasLibro.Fields refField);
		int FuncionesMin(EntGasLibro.Fields refField, EntGasLibro.Fields whereField, object valueField);
		int FuncionesMin(EntGasLibro.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntGasLibro.Fields refField);
		int FuncionesMax(EntGasLibro.Fields refField, EntGasLibro.Fields whereField, object valueField);
		int FuncionesMax(EntGasLibro.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntGasLibro.Fields refField);
		int FuncionesSum(EntGasLibro.Fields refField, EntGasLibro.Fields whereField, object valueField);
		int FuncionesSum(EntGasLibro.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntGasLibro.Fields refField);
		int FuncionesAvg(EntGasLibro.Fields refField, EntGasLibro.Fields whereField, object valueField);
		int FuncionesAvg(EntGasLibro.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

