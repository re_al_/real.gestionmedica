#region 
/***********************************************************************************************************
	NOMBRE:       IClaTipoplantillas
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla clatipoplantillas

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IClaTipoplantillas: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntClaTipoplantillas.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntClaTipoplantillas obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntClaTipoplantillas obj, string strPropiedad);
		EntClaTipoplantillas ObtenerObjetoInsertado(string strUsuCre);
		EntClaTipoplantillas ObtenerObjeto(int intidctp);
		EntClaTipoplantillas ObtenerObjeto(int intidctp, ref CTrans localTrans);
		EntClaTipoplantillas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntClaTipoplantillas ObtenerObjeto(Hashtable htbFiltro);
		EntClaTipoplantillas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntClaTipoplantillas ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntClaTipoplantillas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntClaTipoplantillas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntClaTipoplantillas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntClaTipoplantillas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntClaTipoplantillas ObtenerObjeto(EntClaTipoplantillas.Fields searchField, object searchValue);
		EntClaTipoplantillas ObtenerObjeto(EntClaTipoplantillas.Fields searchField, object searchValue, ref CTrans localTrans);
		EntClaTipoplantillas ObtenerObjeto(EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales);
		EntClaTipoplantillas ObtenerObjeto(EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntClaTipoplantillas> ObtenerDiccionario();
		Dictionary<String, EntClaTipoplantillas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntClaTipoplantillas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntClaTipoplantillas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntClaTipoplantillas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntClaTipoplantillas> ObtenerDiccionario(EntClaTipoplantillas.Fields searchField, object searchValue);
		Dictionary<String, EntClaTipoplantillas> ObtenerDiccionario(EntClaTipoplantillas.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntClaTipoplantillas> ObtenerDiccionario(EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntClaTipoplantillas> ObtenerDiccionario(EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntClaTipoplantillas> ObtenerLista();
		List<EntClaTipoplantillas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntClaTipoplantillas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntClaTipoplantillas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntClaTipoplantillas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntClaTipoplantillas> ObtenerLista(EntClaTipoplantillas.Fields searchField, object searchValue);
		List<EntClaTipoplantillas> ObtenerLista(EntClaTipoplantillas.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntClaTipoplantillas> ObtenerLista(EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntClaTipoplantillas> ObtenerLista(EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntClaTipoplantillas> ObtenerLista(Hashtable htbFiltro);
		List<EntClaTipoplantillas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntClaTipoplantillas> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntClaTipoplantillas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntClaTipoplantillas> ObtenerListaDesdeVista(String strVista);
		List<EntClaTipoplantillas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntClaTipoplantillas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntClaTipoplantillas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntClaTipoplantillas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntClaTipoplantillas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntClaTipoplantillas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntClaTipoplantillas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntClaTipoplantillas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntClaTipoplantillas> ObtenerListaDesdeVista(String strVista, EntClaTipoplantillas.Fields searchField, object searchValue);
		List<EntClaTipoplantillas> ObtenerListaDesdeVista(String strVista, EntClaTipoplantillas.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntClaTipoplantillas> ObtenerListaDesdeVista(String strVista, EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntClaTipoplantillas> ObtenerListaDesdeVista(String strVista, EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntClaTipoplantillas> ObtenerCola();
		Queue<EntClaTipoplantillas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntClaTipoplantillas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntClaTipoplantillas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntClaTipoplantillas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntClaTipoplantillas> ObtenerCola(EntClaTipoplantillas.Fields searchField, object searchValue);
		Queue<EntClaTipoplantillas> ObtenerCola(EntClaTipoplantillas.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntClaTipoplantillas> ObtenerCola(EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntClaTipoplantillas> ObtenerCola(EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntClaTipoplantillas> ObtenerPila();
		Stack<EntClaTipoplantillas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntClaTipoplantillas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntClaTipoplantillas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntClaTipoplantillas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntClaTipoplantillas> ObtenerPila(EntClaTipoplantillas.Fields searchField, object searchValue);
		Stack<EntClaTipoplantillas> ObtenerPila(EntClaTipoplantillas.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntClaTipoplantillas> ObtenerPila(EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntClaTipoplantillas> ObtenerPila(EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntClaTipoplantillas obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntClaTipoplantillas obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntClaTipoplantillas obj, bool bValidar = true);
		bool Insert(EntClaTipoplantillas obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntClaTipoplantillas obj, bool bValidar = true);
		int Update(EntClaTipoplantillas obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntClaTipoplantillas obj, bool bValidar = true);
		int Delete(EntClaTipoplantillas obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntClaTipoplantillas obj);
		int InsertUpdate(EntClaTipoplantillas obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntClaTipoplantillas.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntClaTipoplantillas.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntClaTipoplantillas.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaTipoplantillas.Fields valueField, EntClaTipoplantillas.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntClaTipoplantillas.Fields valueField, EntClaTipoplantillas.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaTipoplantillas.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntClaTipoplantillas.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaTipoplantillas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntClaTipoplantillas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaTipoplantillas.Fields valueField, String textField, EntClaTipoplantillas.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntClaTipoplantillas.Fields valueField, String textField, EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaTipoplantillas.Fields valueField, EntClaTipoplantillas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntClaTipoplantillas.Fields valueField, EntClaTipoplantillas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaTipoplantillas.Fields valueField, EntClaTipoplantillas.Fields textField, EntClaTipoplantillas.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntClaTipoplantillas.Fields valueField, EntClaTipoplantillas.Fields textField, EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntClaTipoplantillas.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntClaTipoplantillas.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntClaTipoplantillas.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntClaTipoplantillas.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaTipoplantillas.Fields valueField, EntClaTipoplantillas.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntClaTipoplantillas.Fields valueField, EntClaTipoplantillas.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaTipoplantillas.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntClaTipoplantillas.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaTipoplantillas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntClaTipoplantillas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaTipoplantillas.Fields valueField, String textField, EntClaTipoplantillas.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntClaTipoplantillas.Fields valueField, String textField, EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaTipoplantillas.Fields valueField, EntClaTipoplantillas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntClaTipoplantillas.Fields valueField, EntClaTipoplantillas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaTipoplantillas.Fields valueField, EntClaTipoplantillas.Fields textField, EntClaTipoplantillas.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntClaTipoplantillas.Fields valueField, EntClaTipoplantillas.Fields textField, EntClaTipoplantillas.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntClaTipoplantillas.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntClaTipoplantillas.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntClaTipoplantillas.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntClaTipoplantillas.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntClaTipoplantillas.Fields refField);
		int FuncionesCount(EntClaTipoplantillas.Fields refField, EntClaTipoplantillas.Fields whereField, object valueField);
		int FuncionesCount(EntClaTipoplantillas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntClaTipoplantillas.Fields refField);
		int FuncionesMin(EntClaTipoplantillas.Fields refField, EntClaTipoplantillas.Fields whereField, object valueField);
		int FuncionesMin(EntClaTipoplantillas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntClaTipoplantillas.Fields refField);
		int FuncionesMax(EntClaTipoplantillas.Fields refField, EntClaTipoplantillas.Fields whereField, object valueField);
		int FuncionesMax(EntClaTipoplantillas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntClaTipoplantillas.Fields refField);
		int FuncionesSum(EntClaTipoplantillas.Fields refField, EntClaTipoplantillas.Fields whereField, object valueField);
		int FuncionesSum(EntClaTipoplantillas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntClaTipoplantillas.Fields refField);
		int FuncionesAvg(EntClaTipoplantillas.Fields refField, EntClaTipoplantillas.Fields whereField, object valueField);
		int FuncionesAvg(EntClaTipoplantillas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

