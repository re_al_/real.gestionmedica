#region 
/***********************************************************************************************************
	NOMBRE:       ISegRolespagina
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla segrolespagina

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface ISegRolespagina: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntSegRolespagina.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntSegRolespagina obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntSegRolespagina obj, string strPropiedad);
		EntSegRolespagina ObtenerObjetoInsertado(string strUsuCre);
		EntSegRolespagina ObtenerObjeto(int introlsro, int intpaginaspg);
		EntSegRolespagina ObtenerObjeto(int introlsro, int intpaginaspg, ref CTrans localTrans);
		EntSegRolespagina ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntSegRolespagina ObtenerObjeto(Hashtable htbFiltro);
		EntSegRolespagina ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntSegRolespagina ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntSegRolespagina ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntSegRolespagina ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntSegRolespagina ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntSegRolespagina ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntSegRolespagina ObtenerObjeto(EntSegRolespagina.Fields searchField, object searchValue);
		EntSegRolespagina ObtenerObjeto(EntSegRolespagina.Fields searchField, object searchValue, ref CTrans localTrans);
		EntSegRolespagina ObtenerObjeto(EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales);
		EntSegRolespagina ObtenerObjeto(EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntSegRolespagina> ObtenerDiccionario();
		Dictionary<String, EntSegRolespagina> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntSegRolespagina> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntSegRolespagina> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntSegRolespagina> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntSegRolespagina> ObtenerDiccionario(EntSegRolespagina.Fields searchField, object searchValue);
		Dictionary<String, EntSegRolespagina> ObtenerDiccionario(EntSegRolespagina.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntSegRolespagina> ObtenerDiccionario(EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntSegRolespagina> ObtenerDiccionario(EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegRolespagina> ObtenerLista();
		List<EntSegRolespagina> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegRolespagina> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegRolespagina> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegRolespagina> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegRolespagina> ObtenerLista(EntSegRolespagina.Fields searchField, object searchValue);
		List<EntSegRolespagina> ObtenerLista(EntSegRolespagina.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegRolespagina> ObtenerLista(EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegRolespagina> ObtenerLista(EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegRolespagina> ObtenerLista(Hashtable htbFiltro);
		List<EntSegRolespagina> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegRolespagina> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegRolespagina> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegRolespagina> ObtenerListaDesdeVista(String strVista);
		List<EntSegRolespagina> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegRolespagina> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegRolespagina> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegRolespagina> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegRolespagina> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntSegRolespagina> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegRolespagina> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegRolespagina> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegRolespagina> ObtenerListaDesdeVista(String strVista, EntSegRolespagina.Fields searchField, object searchValue);
		List<EntSegRolespagina> ObtenerListaDesdeVista(String strVista, EntSegRolespagina.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegRolespagina> ObtenerListaDesdeVista(String strVista, EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegRolespagina> ObtenerListaDesdeVista(String strVista, EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntSegRolespagina> ObtenerCola();
		Queue<EntSegRolespagina> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntSegRolespagina> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntSegRolespagina> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntSegRolespagina> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntSegRolespagina> ObtenerCola(EntSegRolespagina.Fields searchField, object searchValue);
		Queue<EntSegRolespagina> ObtenerCola(EntSegRolespagina.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntSegRolespagina> ObtenerCola(EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntSegRolespagina> ObtenerCola(EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntSegRolespagina> ObtenerPila();
		Stack<EntSegRolespagina> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntSegRolespagina> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntSegRolespagina> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntSegRolespagina> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntSegRolespagina> ObtenerPila(EntSegRolespagina.Fields searchField, object searchValue);
		Stack<EntSegRolespagina> ObtenerPila(EntSegRolespagina.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntSegRolespagina> ObtenerPila(EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntSegRolespagina> ObtenerPila(EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegRolespagina obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegRolespagina obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntSegRolespagina obj, bool bValidar = true);
		bool Insert(EntSegRolespagina obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntSegRolespagina obj, bool bValidar = true);
		int Update(EntSegRolespagina obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntSegRolespagina obj, bool bValidar = true);
		int Delete(EntSegRolespagina obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntSegRolespagina obj);
		int InsertUpdate(EntSegRolespagina obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntSegRolespagina.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegRolespagina.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegRolespagina.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolespagina.Fields valueField, EntSegRolespagina.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolespagina.Fields valueField, EntSegRolespagina.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolespagina.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolespagina.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolespagina.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolespagina.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolespagina.Fields valueField, String textField, EntSegRolespagina.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolespagina.Fields valueField, String textField, EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolespagina.Fields valueField, EntSegRolespagina.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolespagina.Fields valueField, EntSegRolespagina.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolespagina.Fields valueField, EntSegRolespagina.Fields textField, EntSegRolespagina.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolespagina.Fields valueField, EntSegRolespagina.Fields textField, EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntSegRolespagina.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntSegRolespagina.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegRolespagina.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegRolespagina.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRolespagina.Fields valueField, EntSegRolespagina.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntSegRolespagina.Fields valueField, EntSegRolespagina.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRolespagina.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntSegRolespagina.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRolespagina.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegRolespagina.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRolespagina.Fields valueField, String textField, EntSegRolespagina.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegRolespagina.Fields valueField, String textField, EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRolespagina.Fields valueField, EntSegRolespagina.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegRolespagina.Fields valueField, EntSegRolespagina.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRolespagina.Fields valueField, EntSegRolespagina.Fields textField, EntSegRolespagina.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegRolespagina.Fields valueField, EntSegRolespagina.Fields textField, EntSegRolespagina.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntSegRolespagina.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntSegRolespagina.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegRolespagina.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegRolespagina.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntSegRolespagina.Fields refField);
		int FuncionesCount(EntSegRolespagina.Fields refField, EntSegRolespagina.Fields whereField, object valueField);
		int FuncionesCount(EntSegRolespagina.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntSegRolespagina.Fields refField);
		int FuncionesMin(EntSegRolespagina.Fields refField, EntSegRolespagina.Fields whereField, object valueField);
		int FuncionesMin(EntSegRolespagina.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntSegRolespagina.Fields refField);
		int FuncionesMax(EntSegRolespagina.Fields refField, EntSegRolespagina.Fields whereField, object valueField);
		int FuncionesMax(EntSegRolespagina.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntSegRolespagina.Fields refField);
		int FuncionesSum(EntSegRolespagina.Fields refField, EntSegRolespagina.Fields whereField, object valueField);
		int FuncionesSum(EntSegRolespagina.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntSegRolespagina.Fields refField);
		int FuncionesAvg(EntSegRolespagina.Fields refField, EntSegRolespagina.Fields whereField, object valueField);
		int FuncionesAvg(EntSegRolespagina.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

