#region 
/***********************************************************************************************************
	NOMBRE:       ISegRoles
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla segroles

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface ISegRoles: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntSegRoles.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntSegRoles obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntSegRoles obj, string strPropiedad);
		EntSegRoles ObtenerObjetoInsertado(string strUsuCre);
		EntSegRoles ObtenerObjeto(int introlsro);
		EntSegRoles ObtenerObjeto(int introlsro, ref CTrans localTrans);
		EntSegRoles ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntSegRoles ObtenerObjeto(Hashtable htbFiltro);
		EntSegRoles ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntSegRoles ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntSegRoles ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntSegRoles ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntSegRoles ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntSegRoles ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntSegRoles ObtenerObjeto(EntSegRoles.Fields searchField, object searchValue);
		EntSegRoles ObtenerObjeto(EntSegRoles.Fields searchField, object searchValue, ref CTrans localTrans);
		EntSegRoles ObtenerObjeto(EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales);
		EntSegRoles ObtenerObjeto(EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntSegRoles> ObtenerDiccionario();
		Dictionary<String, EntSegRoles> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntSegRoles> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntSegRoles> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntSegRoles> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntSegRoles> ObtenerDiccionario(EntSegRoles.Fields searchField, object searchValue);
		Dictionary<String, EntSegRoles> ObtenerDiccionario(EntSegRoles.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntSegRoles> ObtenerDiccionario(EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntSegRoles> ObtenerDiccionario(EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegRoles> ObtenerLista();
		List<EntSegRoles> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegRoles> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegRoles> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegRoles> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegRoles> ObtenerLista(EntSegRoles.Fields searchField, object searchValue);
		List<EntSegRoles> ObtenerLista(EntSegRoles.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegRoles> ObtenerLista(EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegRoles> ObtenerLista(EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegRoles> ObtenerLista(Hashtable htbFiltro);
		List<EntSegRoles> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegRoles> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegRoles> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegRoles> ObtenerListaDesdeVista(String strVista);
		List<EntSegRoles> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegRoles> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegRoles> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegRoles> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegRoles> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntSegRoles> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegRoles> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegRoles> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegRoles> ObtenerListaDesdeVista(String strVista, EntSegRoles.Fields searchField, object searchValue);
		List<EntSegRoles> ObtenerListaDesdeVista(String strVista, EntSegRoles.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegRoles> ObtenerListaDesdeVista(String strVista, EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegRoles> ObtenerListaDesdeVista(String strVista, EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntSegRoles> ObtenerCola();
		Queue<EntSegRoles> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntSegRoles> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntSegRoles> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntSegRoles> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntSegRoles> ObtenerCola(EntSegRoles.Fields searchField, object searchValue);
		Queue<EntSegRoles> ObtenerCola(EntSegRoles.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntSegRoles> ObtenerCola(EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntSegRoles> ObtenerCola(EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntSegRoles> ObtenerPila();
		Stack<EntSegRoles> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntSegRoles> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntSegRoles> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntSegRoles> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntSegRoles> ObtenerPila(EntSegRoles.Fields searchField, object searchValue);
		Stack<EntSegRoles> ObtenerPila(EntSegRoles.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntSegRoles> ObtenerPila(EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntSegRoles> ObtenerPila(EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegRoles obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegRoles obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntSegRoles obj, bool bValidar = true);
		bool Insert(EntSegRoles obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntSegRoles obj, bool bValidar = true);
		int Update(EntSegRoles obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntSegRoles obj, bool bValidar = true);
		int Delete(EntSegRoles obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntSegRoles obj);
		int InsertUpdate(EntSegRoles obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntSegRoles.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegRoles.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegRoles.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRoles.Fields valueField, EntSegRoles.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegRoles.Fields valueField, EntSegRoles.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRoles.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegRoles.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRoles.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegRoles.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRoles.Fields valueField, String textField, EntSegRoles.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegRoles.Fields valueField, String textField, EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRoles.Fields valueField, EntSegRoles.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegRoles.Fields valueField, EntSegRoles.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRoles.Fields valueField, EntSegRoles.Fields textField, EntSegRoles.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegRoles.Fields valueField, EntSegRoles.Fields textField, EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntSegRoles.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntSegRoles.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegRoles.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegRoles.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRoles.Fields valueField, EntSegRoles.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntSegRoles.Fields valueField, EntSegRoles.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRoles.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntSegRoles.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRoles.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegRoles.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRoles.Fields valueField, String textField, EntSegRoles.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegRoles.Fields valueField, String textField, EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRoles.Fields valueField, EntSegRoles.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegRoles.Fields valueField, EntSegRoles.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRoles.Fields valueField, EntSegRoles.Fields textField, EntSegRoles.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegRoles.Fields valueField, EntSegRoles.Fields textField, EntSegRoles.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntSegRoles.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntSegRoles.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegRoles.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegRoles.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntSegRoles.Fields refField);
		int FuncionesCount(EntSegRoles.Fields refField, EntSegRoles.Fields whereField, object valueField);
		int FuncionesCount(EntSegRoles.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntSegRoles.Fields refField);
		int FuncionesMin(EntSegRoles.Fields refField, EntSegRoles.Fields whereField, object valueField);
		int FuncionesMin(EntSegRoles.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntSegRoles.Fields refField);
		int FuncionesMax(EntSegRoles.Fields refField, EntSegRoles.Fields whereField, object valueField);
		int FuncionesMax(EntSegRoles.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntSegRoles.Fields refField);
		int FuncionesSum(EntSegRoles.Fields refField, EntSegRoles.Fields whereField, object valueField);
		int FuncionesSum(EntSegRoles.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntSegRoles.Fields refField);
		int FuncionesAvg(EntSegRoles.Fields refField, EntSegRoles.Fields whereField, object valueField);
		int FuncionesAvg(EntSegRoles.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

