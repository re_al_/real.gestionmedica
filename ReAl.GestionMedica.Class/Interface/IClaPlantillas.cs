#region 
/***********************************************************************************************************
	NOMBRE:       IClaPlantillas
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla claplantillas

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        16/07/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IClaPlantillas: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntClaPlantillas.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntClaPlantillas obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntClaPlantillas obj, string strPropiedad);
		EntClaPlantillas ObtenerObjetoInsertado(string strUsuCre);
		EntClaPlantillas ObtenerObjeto(int intidcpl);
		EntClaPlantillas ObtenerObjeto(int intidcpl, ref CTrans localTrans);
		EntClaPlantillas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntClaPlantillas ObtenerObjeto(Hashtable htbFiltro);
		EntClaPlantillas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntClaPlantillas ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntClaPlantillas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntClaPlantillas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntClaPlantillas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntClaPlantillas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntClaPlantillas ObtenerObjeto(EntClaPlantillas.Fields searchField, object searchValue);
		EntClaPlantillas ObtenerObjeto(EntClaPlantillas.Fields searchField, object searchValue, ref CTrans localTrans);
		EntClaPlantillas ObtenerObjeto(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales);
		EntClaPlantillas ObtenerObjeto(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntClaPlantillas> ObtenerDiccionario();
		Dictionary<String, EntClaPlantillas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntClaPlantillas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntClaPlantillas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntClaPlantillas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntClaPlantillas> ObtenerDiccionario(EntClaPlantillas.Fields searchField, object searchValue);
		Dictionary<String, EntClaPlantillas> ObtenerDiccionario(EntClaPlantillas.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntClaPlantillas> ObtenerDiccionario(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntClaPlantillas> ObtenerDiccionario(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntClaPlantillas> ObtenerLista();
		List<EntClaPlantillas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntClaPlantillas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntClaPlantillas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntClaPlantillas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntClaPlantillas> ObtenerLista(EntClaPlantillas.Fields searchField, object searchValue);
		List<EntClaPlantillas> ObtenerLista(EntClaPlantillas.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntClaPlantillas> ObtenerLista(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntClaPlantillas> ObtenerLista(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntClaPlantillas> ObtenerLista(Hashtable htbFiltro);
		List<EntClaPlantillas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntClaPlantillas> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntClaPlantillas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista);
		List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, EntClaPlantillas.Fields searchField, object searchValue);
		List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, EntClaPlantillas.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntClaPlantillas> ObtenerListaDesdeVista(String strVista, EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntClaPlantillas> ObtenerCola();
		Queue<EntClaPlantillas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntClaPlantillas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntClaPlantillas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntClaPlantillas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntClaPlantillas> ObtenerCola(EntClaPlantillas.Fields searchField, object searchValue);
		Queue<EntClaPlantillas> ObtenerCola(EntClaPlantillas.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntClaPlantillas> ObtenerCola(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntClaPlantillas> ObtenerCola(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntClaPlantillas> ObtenerPila();
		Stack<EntClaPlantillas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntClaPlantillas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntClaPlantillas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntClaPlantillas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntClaPlantillas> ObtenerPila(EntClaPlantillas.Fields searchField, object searchValue);
		Stack<EntClaPlantillas> ObtenerPila(EntClaPlantillas.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntClaPlantillas> ObtenerPila(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntClaPlantillas> ObtenerPila(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntClaPlantillas obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntClaPlantillas obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntClaPlantillas obj, bool bValidar = true);
		bool Insert(EntClaPlantillas obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntClaPlantillas obj, bool bValidar = true);
		int Update(EntClaPlantillas obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntClaPlantillas obj, bool bValidar = true);
		int Delete(EntClaPlantillas obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntClaPlantillas obj);
		int InsertUpdate(EntClaPlantillas obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntClaPlantillas.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntClaPlantillas.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntClaPlantillas.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, String textField, EntClaPlantillas.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, String textField, EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField, EntClaPlantillas.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntClaPlantillas.Fields valueField, EntClaPlantillas.Fields textField, EntClaPlantillas.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntClaPlantillas.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntClaPlantillas.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntClaPlantillas.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntClaPlantillas.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntClaPlantillas.Fields refField);
		int FuncionesCount(EntClaPlantillas.Fields refField, EntClaPlantillas.Fields whereField, object valueField);
		int FuncionesCount(EntClaPlantillas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntClaPlantillas.Fields refField);
		int FuncionesMin(EntClaPlantillas.Fields refField, EntClaPlantillas.Fields whereField, object valueField);
		int FuncionesMin(EntClaPlantillas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntClaPlantillas.Fields refField);
		int FuncionesMax(EntClaPlantillas.Fields refField, EntClaPlantillas.Fields whereField, object valueField);
		int FuncionesMax(EntClaPlantillas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntClaPlantillas.Fields refField);
		int FuncionesSum(EntClaPlantillas.Fields refField, EntClaPlantillas.Fields whereField, object valueField);
		int FuncionesSum(EntClaPlantillas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntClaPlantillas.Fields refField);
		int FuncionesAvg(EntClaPlantillas.Fields refField, EntClaPlantillas.Fields whereField, object valueField);
		int FuncionesAvg(EntClaPlantillas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

