#region 
/***********************************************************************************************************
	NOMBRE:       IPacHistoriaclinica1
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla pachistoriaclinica1

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IPacHistoriaclinica1: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntPacHistoriaclinica1.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntPacHistoriaclinica1 obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntPacHistoriaclinica1 obj, string strPropiedad);
		EntPacHistoriaclinica1 ObtenerObjetoInsertado(string strUsuCre);
		EntPacHistoriaclinica1 ObtenerObjeto(Int64 Int64idppa);
		EntPacHistoriaclinica1 ObtenerObjeto(Int64 Int64idppa, ref CTrans localTrans);
		EntPacHistoriaclinica1 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntPacHistoriaclinica1 ObtenerObjeto(Hashtable htbFiltro);
		EntPacHistoriaclinica1 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntPacHistoriaclinica1 ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntPacHistoriaclinica1 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntPacHistoriaclinica1 ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntPacHistoriaclinica1 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntPacHistoriaclinica1 ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntPacHistoriaclinica1 ObtenerObjeto(EntPacHistoriaclinica1.Fields searchField, object searchValue);
		EntPacHistoriaclinica1 ObtenerObjeto(EntPacHistoriaclinica1.Fields searchField, object searchValue, ref CTrans localTrans);
		EntPacHistoriaclinica1 ObtenerObjeto(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales);
		EntPacHistoriaclinica1 ObtenerObjeto(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario();
		Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(EntPacHistoriaclinica1.Fields searchField, object searchValue);
		Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(EntPacHistoriaclinica1.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntPacHistoriaclinica1> ObtenerDiccionario(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacHistoriaclinica1> ObtenerLista();
		List<EntPacHistoriaclinica1> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacHistoriaclinica1> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacHistoriaclinica1> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacHistoriaclinica1> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacHistoriaclinica1> ObtenerLista(EntPacHistoriaclinica1.Fields searchField, object searchValue);
		List<EntPacHistoriaclinica1> ObtenerLista(EntPacHistoriaclinica1.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacHistoriaclinica1> ObtenerLista(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacHistoriaclinica1> ObtenerLista(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacHistoriaclinica1> ObtenerLista(Hashtable htbFiltro);
		List<EntPacHistoriaclinica1> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacHistoriaclinica1> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacHistoriaclinica1> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista);
		List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica1.Fields searchField, object searchValue);
		List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica1.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacHistoriaclinica1> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntPacHistoriaclinica1> ObtenerCola();
		Queue<EntPacHistoriaclinica1> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntPacHistoriaclinica1> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntPacHistoriaclinica1> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntPacHistoriaclinica1> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntPacHistoriaclinica1> ObtenerCola(EntPacHistoriaclinica1.Fields searchField, object searchValue);
		Queue<EntPacHistoriaclinica1> ObtenerCola(EntPacHistoriaclinica1.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntPacHistoriaclinica1> ObtenerCola(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntPacHistoriaclinica1> ObtenerCola(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntPacHistoriaclinica1> ObtenerPila();
		Stack<EntPacHistoriaclinica1> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntPacHistoriaclinica1> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntPacHistoriaclinica1> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntPacHistoriaclinica1> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntPacHistoriaclinica1> ObtenerPila(EntPacHistoriaclinica1.Fields searchField, object searchValue);
		Stack<EntPacHistoriaclinica1> ObtenerPila(EntPacHistoriaclinica1.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntPacHistoriaclinica1> ObtenerPila(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntPacHistoriaclinica1> ObtenerPila(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacHistoriaclinica1 obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacHistoriaclinica1 obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntPacHistoriaclinica1 obj, bool bValidar = true);
		bool Insert(EntPacHistoriaclinica1 obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntPacHistoriaclinica1 obj, bool bValidar = true);
		int Update(EntPacHistoriaclinica1 obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntPacHistoriaclinica1 obj);
		int InsertUpdate(EntPacHistoriaclinica1 obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntPacHistoriaclinica1.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacHistoriaclinica1.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, String textField, EntPacHistoriaclinica1.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, String textField, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, EntPacHistoriaclinica1.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntPacHistoriaclinica1.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica1.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, String textField, EntPacHistoriaclinica1.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, String textField, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, EntPacHistoriaclinica1.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica1.Fields valueField, EntPacHistoriaclinica1.Fields textField, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntPacHistoriaclinica1.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica1.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica1.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntPacHistoriaclinica1.Fields refField);
		int FuncionesCount(EntPacHistoriaclinica1.Fields refField, EntPacHistoriaclinica1.Fields whereField, object valueField);
		int FuncionesCount(EntPacHistoriaclinica1.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntPacHistoriaclinica1.Fields refField);
		int FuncionesMin(EntPacHistoriaclinica1.Fields refField, EntPacHistoriaclinica1.Fields whereField, object valueField);
		int FuncionesMin(EntPacHistoriaclinica1.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntPacHistoriaclinica1.Fields refField);
		int FuncionesMax(EntPacHistoriaclinica1.Fields refField, EntPacHistoriaclinica1.Fields whereField, object valueField);
		int FuncionesMax(EntPacHistoriaclinica1.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntPacHistoriaclinica1.Fields refField);
		int FuncionesSum(EntPacHistoriaclinica1.Fields refField, EntPacHistoriaclinica1.Fields whereField, object valueField);
		int FuncionesSum(EntPacHistoriaclinica1.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntPacHistoriaclinica1.Fields refField);
		int FuncionesAvg(EntPacHistoriaclinica1.Fields refField, EntPacHistoriaclinica1.Fields whereField, object valueField);
		int FuncionesAvg(EntPacHistoriaclinica1.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

