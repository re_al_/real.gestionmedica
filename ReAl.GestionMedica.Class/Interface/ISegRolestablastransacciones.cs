#region 
/***********************************************************************************************************
	NOMBRE:       ISegRolestablastransacciones
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla segrolestablastransacciones

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface ISegRolestablastransacciones: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntSegRolestablastransacciones.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntSegRolestablastransacciones obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntSegRolestablastransacciones obj, string strPropiedad);
		EntSegRolestablastransacciones ObtenerObjetoInsertado(string strUsuCre);
		EntSegRolestablastransacciones ObtenerObjeto(int introlsro, string stringtablasta, string stringtransaccionstr);
		EntSegRolestablastransacciones ObtenerObjeto(int introlsro, string stringtablasta, string stringtransaccionstr, ref CTrans localTrans);
		EntSegRolestablastransacciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntSegRolestablastransacciones ObtenerObjeto(Hashtable htbFiltro);
		EntSegRolestablastransacciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntSegRolestablastransacciones ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntSegRolestablastransacciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntSegRolestablastransacciones ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntSegRolestablastransacciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntSegRolestablastransacciones ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntSegRolestablastransacciones ObtenerObjeto(EntSegRolestablastransacciones.Fields searchField, object searchValue);
		EntSegRolestablastransacciones ObtenerObjeto(EntSegRolestablastransacciones.Fields searchField, object searchValue, ref CTrans localTrans);
		EntSegRolestablastransacciones ObtenerObjeto(EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		EntSegRolestablastransacciones ObtenerObjeto(EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntSegRolestablastransacciones> ObtenerDiccionario();
		Dictionary<String, EntSegRolestablastransacciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntSegRolestablastransacciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntSegRolestablastransacciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntSegRolestablastransacciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntSegRolestablastransacciones> ObtenerDiccionario(EntSegRolestablastransacciones.Fields searchField, object searchValue);
		Dictionary<String, EntSegRolestablastransacciones> ObtenerDiccionario(EntSegRolestablastransacciones.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntSegRolestablastransacciones> ObtenerDiccionario(EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntSegRolestablastransacciones> ObtenerDiccionario(EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegRolestablastransacciones> ObtenerLista();
		List<EntSegRolestablastransacciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegRolestablastransacciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegRolestablastransacciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegRolestablastransacciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegRolestablastransacciones> ObtenerLista(EntSegRolestablastransacciones.Fields searchField, object searchValue);
		List<EntSegRolestablastransacciones> ObtenerLista(EntSegRolestablastransacciones.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegRolestablastransacciones> ObtenerLista(EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegRolestablastransacciones> ObtenerLista(EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegRolestablastransacciones> ObtenerLista(Hashtable htbFiltro);
		List<EntSegRolestablastransacciones> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegRolestablastransacciones> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegRolestablastransacciones> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegRolestablastransacciones> ObtenerListaDesdeVista(String strVista);
		List<EntSegRolestablastransacciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegRolestablastransacciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegRolestablastransacciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegRolestablastransacciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegRolestablastransacciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntSegRolestablastransacciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegRolestablastransacciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegRolestablastransacciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegRolestablastransacciones> ObtenerListaDesdeVista(String strVista, EntSegRolestablastransacciones.Fields searchField, object searchValue);
		List<EntSegRolestablastransacciones> ObtenerListaDesdeVista(String strVista, EntSegRolestablastransacciones.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegRolestablastransacciones> ObtenerListaDesdeVista(String strVista, EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegRolestablastransacciones> ObtenerListaDesdeVista(String strVista, EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntSegRolestablastransacciones> ObtenerCola();
		Queue<EntSegRolestablastransacciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntSegRolestablastransacciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntSegRolestablastransacciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntSegRolestablastransacciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntSegRolestablastransacciones> ObtenerCola(EntSegRolestablastransacciones.Fields searchField, object searchValue);
		Queue<EntSegRolestablastransacciones> ObtenerCola(EntSegRolestablastransacciones.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntSegRolestablastransacciones> ObtenerCola(EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntSegRolestablastransacciones> ObtenerCola(EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntSegRolestablastransacciones> ObtenerPila();
		Stack<EntSegRolestablastransacciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntSegRolestablastransacciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntSegRolestablastransacciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntSegRolestablastransacciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntSegRolestablastransacciones> ObtenerPila(EntSegRolestablastransacciones.Fields searchField, object searchValue);
		Stack<EntSegRolestablastransacciones> ObtenerPila(EntSegRolestablastransacciones.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntSegRolestablastransacciones> ObtenerPila(EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntSegRolestablastransacciones> ObtenerPila(EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegRolestablastransacciones obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegRolestablastransacciones obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntSegRolestablastransacciones obj, bool bValidar = true);
		bool Insert(EntSegRolestablastransacciones obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntSegRolestablastransacciones obj, bool bValidar = true);
		int Update(EntSegRolestablastransacciones obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntSegRolestablastransacciones obj, bool bValidar = true);
		int Delete(EntSegRolestablastransacciones obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntSegRolestablastransacciones obj);
		int InsertUpdate(EntSegRolestablastransacciones obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntSegRolestablastransacciones.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegRolestablastransacciones.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolestablastransacciones.Fields valueField, EntSegRolestablastransacciones.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolestablastransacciones.Fields valueField, EntSegRolestablastransacciones.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolestablastransacciones.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolestablastransacciones.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolestablastransacciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolestablastransacciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolestablastransacciones.Fields valueField, String textField, EntSegRolestablastransacciones.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolestablastransacciones.Fields valueField, String textField, EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolestablastransacciones.Fields valueField, EntSegRolestablastransacciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolestablastransacciones.Fields valueField, EntSegRolestablastransacciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolestablastransacciones.Fields valueField, EntSegRolestablastransacciones.Fields textField, EntSegRolestablastransacciones.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegRolestablastransacciones.Fields valueField, EntSegRolestablastransacciones.Fields textField, EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntSegRolestablastransacciones.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegRolestablastransacciones.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRolestablastransacciones.Fields valueField, EntSegRolestablastransacciones.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntSegRolestablastransacciones.Fields valueField, EntSegRolestablastransacciones.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRolestablastransacciones.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntSegRolestablastransacciones.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRolestablastransacciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegRolestablastransacciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRolestablastransacciones.Fields valueField, String textField, EntSegRolestablastransacciones.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegRolestablastransacciones.Fields valueField, String textField, EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRolestablastransacciones.Fields valueField, EntSegRolestablastransacciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegRolestablastransacciones.Fields valueField, EntSegRolestablastransacciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegRolestablastransacciones.Fields valueField, EntSegRolestablastransacciones.Fields textField, EntSegRolestablastransacciones.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegRolestablastransacciones.Fields valueField, EntSegRolestablastransacciones.Fields textField, EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntSegRolestablastransacciones.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegRolestablastransacciones.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegRolestablastransacciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntSegRolestablastransacciones.Fields refField);
		int FuncionesCount(EntSegRolestablastransacciones.Fields refField, EntSegRolestablastransacciones.Fields whereField, object valueField);
		int FuncionesCount(EntSegRolestablastransacciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntSegRolestablastransacciones.Fields refField);
		int FuncionesMin(EntSegRolestablastransacciones.Fields refField, EntSegRolestablastransacciones.Fields whereField, object valueField);
		int FuncionesMin(EntSegRolestablastransacciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntSegRolestablastransacciones.Fields refField);
		int FuncionesMax(EntSegRolestablastransacciones.Fields refField, EntSegRolestablastransacciones.Fields whereField, object valueField);
		int FuncionesMax(EntSegRolestablastransacciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntSegRolestablastransacciones.Fields refField);
		int FuncionesSum(EntSegRolestablastransacciones.Fields refField, EntSegRolestablastransacciones.Fields whereField, object valueField);
		int FuncionesSum(EntSegRolestablastransacciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntSegRolestablastransacciones.Fields refField);
		int FuncionesAvg(EntSegRolestablastransacciones.Fields refField, EntSegRolestablastransacciones.Fields whereField, object valueField);
		int FuncionesAvg(EntSegRolestablastransacciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

