#region 
/***********************************************************************************************************
	NOMBRE:       ISegUsuariosrestriccion
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla segusuariosrestriccion

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface ISegUsuariosrestriccion: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntSegUsuariosrestriccion.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntSegUsuariosrestriccion obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntSegUsuariosrestriccion obj, string strPropiedad);
		EntSegUsuariosrestriccion ObtenerObjetoInsertado(string strUsuCre);
		EntSegUsuariosrestriccion ObtenerObjeto(int intidsur);
		EntSegUsuariosrestriccion ObtenerObjeto(int intidsur, ref CTrans localTrans);
		EntSegUsuariosrestriccion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntSegUsuariosrestriccion ObtenerObjeto(Hashtable htbFiltro);
		EntSegUsuariosrestriccion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntSegUsuariosrestriccion ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntSegUsuariosrestriccion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntSegUsuariosrestriccion ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntSegUsuariosrestriccion ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntSegUsuariosrestriccion ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntSegUsuariosrestriccion ObtenerObjeto(EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		EntSegUsuariosrestriccion ObtenerObjeto(EntSegUsuariosrestriccion.Fields searchField, object searchValue, ref CTrans localTrans);
		EntSegUsuariosrestriccion ObtenerObjeto(EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		EntSegUsuariosrestriccion ObtenerObjeto(EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntSegUsuariosrestriccion> ObtenerDiccionario();
		Dictionary<String, EntSegUsuariosrestriccion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntSegUsuariosrestriccion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntSegUsuariosrestriccion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntSegUsuariosrestriccion> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntSegUsuariosrestriccion> ObtenerDiccionario(EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		Dictionary<String, EntSegUsuariosrestriccion> ObtenerDiccionario(EntSegUsuariosrestriccion.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntSegUsuariosrestriccion> ObtenerDiccionario(EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntSegUsuariosrestriccion> ObtenerDiccionario(EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegUsuariosrestriccion> ObtenerLista();
		List<EntSegUsuariosrestriccion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegUsuariosrestriccion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegUsuariosrestriccion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegUsuariosrestriccion> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegUsuariosrestriccion> ObtenerLista(EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		List<EntSegUsuariosrestriccion> ObtenerLista(EntSegUsuariosrestriccion.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegUsuariosrestriccion> ObtenerLista(EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegUsuariosrestriccion> ObtenerLista(EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegUsuariosrestriccion> ObtenerLista(Hashtable htbFiltro);
		List<EntSegUsuariosrestriccion> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegUsuariosrestriccion> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegUsuariosrestriccion> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegUsuariosrestriccion> ObtenerListaDesdeVista(String strVista);
		List<EntSegUsuariosrestriccion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegUsuariosrestriccion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegUsuariosrestriccion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegUsuariosrestriccion> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegUsuariosrestriccion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntSegUsuariosrestriccion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegUsuariosrestriccion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegUsuariosrestriccion> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegUsuariosrestriccion> ObtenerListaDesdeVista(String strVista, EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		List<EntSegUsuariosrestriccion> ObtenerListaDesdeVista(String strVista, EntSegUsuariosrestriccion.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegUsuariosrestriccion> ObtenerListaDesdeVista(String strVista, EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegUsuariosrestriccion> ObtenerListaDesdeVista(String strVista, EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntSegUsuariosrestriccion> ObtenerCola();
		Queue<EntSegUsuariosrestriccion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntSegUsuariosrestriccion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntSegUsuariosrestriccion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntSegUsuariosrestriccion> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntSegUsuariosrestriccion> ObtenerCola(EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		Queue<EntSegUsuariosrestriccion> ObtenerCola(EntSegUsuariosrestriccion.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntSegUsuariosrestriccion> ObtenerCola(EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntSegUsuariosrestriccion> ObtenerCola(EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntSegUsuariosrestriccion> ObtenerPila();
		Stack<EntSegUsuariosrestriccion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntSegUsuariosrestriccion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntSegUsuariosrestriccion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntSegUsuariosrestriccion> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntSegUsuariosrestriccion> ObtenerPila(EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		Stack<EntSegUsuariosrestriccion> ObtenerPila(EntSegUsuariosrestriccion.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntSegUsuariosrestriccion> ObtenerPila(EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntSegUsuariosrestriccion> ObtenerPila(EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegUsuariosrestriccion obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegUsuariosrestriccion obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntSegUsuariosrestriccion obj, bool bValidar = true);
		bool Insert(EntSegUsuariosrestriccion obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntSegUsuariosrestriccion obj, bool bValidar = true);
		int Update(EntSegUsuariosrestriccion obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntSegUsuariosrestriccion obj, bool bValidar = true);
		int Delete(EntSegUsuariosrestriccion obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntSegUsuariosrestriccion obj);
		int InsertUpdate(EntSegUsuariosrestriccion obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegUsuariosrestriccion.Fields valueField, EntSegUsuariosrestriccion.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegUsuariosrestriccion.Fields valueField, EntSegUsuariosrestriccion.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegUsuariosrestriccion.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegUsuariosrestriccion.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegUsuariosrestriccion.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegUsuariosrestriccion.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegUsuariosrestriccion.Fields valueField, String textField, EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegUsuariosrestriccion.Fields valueField, String textField, EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegUsuariosrestriccion.Fields valueField, EntSegUsuariosrestriccion.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegUsuariosrestriccion.Fields valueField, EntSegUsuariosrestriccion.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegUsuariosrestriccion.Fields valueField, EntSegUsuariosrestriccion.Fields textField, EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegUsuariosrestriccion.Fields valueField, EntSegUsuariosrestriccion.Fields textField, EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegUsuariosrestriccion.Fields valueField, EntSegUsuariosrestriccion.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntSegUsuariosrestriccion.Fields valueField, EntSegUsuariosrestriccion.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegUsuariosrestriccion.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntSegUsuariosrestriccion.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegUsuariosrestriccion.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegUsuariosrestriccion.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegUsuariosrestriccion.Fields valueField, String textField, EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegUsuariosrestriccion.Fields valueField, String textField, EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegUsuariosrestriccion.Fields valueField, EntSegUsuariosrestriccion.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegUsuariosrestriccion.Fields valueField, EntSegUsuariosrestriccion.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegUsuariosrestriccion.Fields valueField, EntSegUsuariosrestriccion.Fields textField, EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegUsuariosrestriccion.Fields valueField, EntSegUsuariosrestriccion.Fields textField, EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegUsuariosrestriccion.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegUsuariosrestriccion.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntSegUsuariosrestriccion.Fields refField);
		int FuncionesCount(EntSegUsuariosrestriccion.Fields refField, EntSegUsuariosrestriccion.Fields whereField, object valueField);
		int FuncionesCount(EntSegUsuariosrestriccion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntSegUsuariosrestriccion.Fields refField);
		int FuncionesMin(EntSegUsuariosrestriccion.Fields refField, EntSegUsuariosrestriccion.Fields whereField, object valueField);
		int FuncionesMin(EntSegUsuariosrestriccion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntSegUsuariosrestriccion.Fields refField);
		int FuncionesMax(EntSegUsuariosrestriccion.Fields refField, EntSegUsuariosrestriccion.Fields whereField, object valueField);
		int FuncionesMax(EntSegUsuariosrestriccion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntSegUsuariosrestriccion.Fields refField);
		int FuncionesSum(EntSegUsuariosrestriccion.Fields refField, EntSegUsuariosrestriccion.Fields whereField, object valueField);
		int FuncionesSum(EntSegUsuariosrestriccion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntSegUsuariosrestriccion.Fields refField);
		int FuncionesAvg(EntSegUsuariosrestriccion.Fields refField, EntSegUsuariosrestriccion.Fields whereField, object valueField);
		int FuncionesAvg(EntSegUsuariosrestriccion.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

