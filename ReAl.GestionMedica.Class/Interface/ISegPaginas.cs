#region 
/***********************************************************************************************************
	NOMBRE:       ISegPaginas
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla segpaginas

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface ISegPaginas: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntSegPaginas.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntSegPaginas obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntSegPaginas obj, string strPropiedad);
		EntSegPaginas ObtenerObjetoInsertado(string strUsuCre);
		EntSegPaginas ObtenerObjeto(int intpaginaspg);
		EntSegPaginas ObtenerObjeto(int intpaginaspg, ref CTrans localTrans);
		EntSegPaginas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntSegPaginas ObtenerObjeto(Hashtable htbFiltro);
		EntSegPaginas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntSegPaginas ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntSegPaginas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntSegPaginas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntSegPaginas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntSegPaginas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntSegPaginas ObtenerObjeto(EntSegPaginas.Fields searchField, object searchValue);
		EntSegPaginas ObtenerObjeto(EntSegPaginas.Fields searchField, object searchValue, ref CTrans localTrans);
		EntSegPaginas ObtenerObjeto(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales);
		EntSegPaginas ObtenerObjeto(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntSegPaginas> ObtenerDiccionario();
		Dictionary<String, EntSegPaginas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntSegPaginas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntSegPaginas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntSegPaginas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntSegPaginas> ObtenerDiccionario(EntSegPaginas.Fields searchField, object searchValue);
		Dictionary<String, EntSegPaginas> ObtenerDiccionario(EntSegPaginas.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntSegPaginas> ObtenerDiccionario(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntSegPaginas> ObtenerDiccionario(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegPaginas> ObtenerLista();
		List<EntSegPaginas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegPaginas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegPaginas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegPaginas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegPaginas> ObtenerLista(EntSegPaginas.Fields searchField, object searchValue);
		List<EntSegPaginas> ObtenerLista(EntSegPaginas.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegPaginas> ObtenerLista(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegPaginas> ObtenerLista(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegPaginas> ObtenerLista(Hashtable htbFiltro);
		List<EntSegPaginas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegPaginas> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegPaginas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegPaginas> ObtenerListaDesdeVista(String strVista);
		List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, EntSegPaginas.Fields searchField, object searchValue);
		List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, EntSegPaginas.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegPaginas> ObtenerListaDesdeVista(String strVista, EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntSegPaginas> ObtenerCola();
		Queue<EntSegPaginas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntSegPaginas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntSegPaginas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntSegPaginas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntSegPaginas> ObtenerCola(EntSegPaginas.Fields searchField, object searchValue);
		Queue<EntSegPaginas> ObtenerCola(EntSegPaginas.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntSegPaginas> ObtenerCola(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntSegPaginas> ObtenerCola(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntSegPaginas> ObtenerPila();
		Stack<EntSegPaginas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntSegPaginas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntSegPaginas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntSegPaginas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntSegPaginas> ObtenerPila(EntSegPaginas.Fields searchField, object searchValue);
		Stack<EntSegPaginas> ObtenerPila(EntSegPaginas.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntSegPaginas> ObtenerPila(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntSegPaginas> ObtenerPila(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegPaginas obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegPaginas obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntSegPaginas obj, bool bValidar = true);
		bool Insert(EntSegPaginas obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntSegPaginas obj, bool bValidar = true);
		int Update(EntSegPaginas obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntSegPaginas obj, bool bValidar = true);
		int Delete(EntSegPaginas obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntSegPaginas obj);
		int InsertUpdate(EntSegPaginas obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntSegPaginas.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegPaginas.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegPaginas.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, String textField, EntSegPaginas.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, String textField, EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, EntSegPaginas.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntSegPaginas.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntSegPaginas.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegPaginas.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegPaginas.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, String textField, EntSegPaginas.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, String textField, EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, EntSegPaginas.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegPaginas.Fields valueField, EntSegPaginas.Fields textField, EntSegPaginas.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntSegPaginas.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntSegPaginas.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegPaginas.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegPaginas.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntSegPaginas.Fields refField);
		int FuncionesCount(EntSegPaginas.Fields refField, EntSegPaginas.Fields whereField, object valueField);
		int FuncionesCount(EntSegPaginas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntSegPaginas.Fields refField);
		int FuncionesMin(EntSegPaginas.Fields refField, EntSegPaginas.Fields whereField, object valueField);
		int FuncionesMin(EntSegPaginas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntSegPaginas.Fields refField);
		int FuncionesMax(EntSegPaginas.Fields refField, EntSegPaginas.Fields whereField, object valueField);
		int FuncionesMax(EntSegPaginas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntSegPaginas.Fields refField);
		int FuncionesSum(EntSegPaginas.Fields refField, EntSegPaginas.Fields whereField, object valueField);
		int FuncionesSum(EntSegPaginas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntSegPaginas.Fields refField);
		int FuncionesAvg(EntSegPaginas.Fields refField, EntSegPaginas.Fields whereField, object valueField);
		int FuncionesAvg(EntSegPaginas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

