#region 
/***********************************************************************************************************
	NOMBRE:       IPacConsultas
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla pacconsultas

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IPacConsultas: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntPacConsultas.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntPacConsultas obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntPacConsultas obj, string strPropiedad);
		EntPacConsultas ObtenerObjetoInsertado(string strUsuCre);
		EntPacConsultas ObtenerObjeto(Int64 Int64idpco);
		EntPacConsultas ObtenerObjeto(Int64 Int64idpco, ref CTrans localTrans);
		EntPacConsultas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntPacConsultas ObtenerObjeto(Hashtable htbFiltro);
		EntPacConsultas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntPacConsultas ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntPacConsultas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntPacConsultas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntPacConsultas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntPacConsultas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntPacConsultas ObtenerObjeto(EntPacConsultas.Fields searchField, object searchValue);
		EntPacConsultas ObtenerObjeto(EntPacConsultas.Fields searchField, object searchValue, ref CTrans localTrans);
		EntPacConsultas ObtenerObjeto(EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales);
		EntPacConsultas ObtenerObjeto(EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntPacConsultas> ObtenerDiccionario();
		Dictionary<String, EntPacConsultas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntPacConsultas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntPacConsultas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntPacConsultas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntPacConsultas> ObtenerDiccionario(EntPacConsultas.Fields searchField, object searchValue);
		Dictionary<String, EntPacConsultas> ObtenerDiccionario(EntPacConsultas.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntPacConsultas> ObtenerDiccionario(EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntPacConsultas> ObtenerDiccionario(EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacConsultas> ObtenerLista();
		List<EntPacConsultas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacConsultas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacConsultas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacConsultas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacConsultas> ObtenerLista(EntPacConsultas.Fields searchField, object searchValue);
		List<EntPacConsultas> ObtenerLista(EntPacConsultas.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacConsultas> ObtenerLista(EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacConsultas> ObtenerLista(EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacConsultas> ObtenerLista(Hashtable htbFiltro);
		List<EntPacConsultas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacConsultas> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacConsultas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacConsultas> ObtenerListaDesdeVista(String strVista);
		List<EntPacConsultas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacConsultas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacConsultas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacConsultas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacConsultas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntPacConsultas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacConsultas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacConsultas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacConsultas> ObtenerListaDesdeVista(String strVista, EntPacConsultas.Fields searchField, object searchValue);
		List<EntPacConsultas> ObtenerListaDesdeVista(String strVista, EntPacConsultas.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacConsultas> ObtenerListaDesdeVista(String strVista, EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacConsultas> ObtenerListaDesdeVista(String strVista, EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntPacConsultas> ObtenerCola();
		Queue<EntPacConsultas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntPacConsultas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntPacConsultas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntPacConsultas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntPacConsultas> ObtenerCola(EntPacConsultas.Fields searchField, object searchValue);
		Queue<EntPacConsultas> ObtenerCola(EntPacConsultas.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntPacConsultas> ObtenerCola(EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntPacConsultas> ObtenerCola(EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntPacConsultas> ObtenerPila();
		Stack<EntPacConsultas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntPacConsultas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntPacConsultas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntPacConsultas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntPacConsultas> ObtenerPila(EntPacConsultas.Fields searchField, object searchValue);
		Stack<EntPacConsultas> ObtenerPila(EntPacConsultas.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntPacConsultas> ObtenerPila(EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntPacConsultas> ObtenerPila(EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacConsultas obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacConsultas obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntPacConsultas obj, bool bValidar = true);
		bool Insert(EntPacConsultas obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntPacConsultas obj, bool bValidar = true);
		int Update(EntPacConsultas obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntPacConsultas obj, bool bValidar = true);
		int Delete(EntPacConsultas obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntPacConsultas obj);
		int InsertUpdate(EntPacConsultas obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntPacConsultas.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacConsultas.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacConsultas.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacConsultas.Fields valueField, EntPacConsultas.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntPacConsultas.Fields valueField, EntPacConsultas.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacConsultas.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntPacConsultas.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacConsultas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntPacConsultas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacConsultas.Fields valueField, String textField, EntPacConsultas.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntPacConsultas.Fields valueField, String textField, EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacConsultas.Fields valueField, EntPacConsultas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntPacConsultas.Fields valueField, EntPacConsultas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacConsultas.Fields valueField, EntPacConsultas.Fields textField, EntPacConsultas.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntPacConsultas.Fields valueField, EntPacConsultas.Fields textField, EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntPacConsultas.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntPacConsultas.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacConsultas.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacConsultas.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacConsultas.Fields valueField, EntPacConsultas.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntPacConsultas.Fields valueField, EntPacConsultas.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacConsultas.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntPacConsultas.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacConsultas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacConsultas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacConsultas.Fields valueField, String textField, EntPacConsultas.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacConsultas.Fields valueField, String textField, EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacConsultas.Fields valueField, EntPacConsultas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacConsultas.Fields valueField, EntPacConsultas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacConsultas.Fields valueField, EntPacConsultas.Fields textField, EntPacConsultas.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacConsultas.Fields valueField, EntPacConsultas.Fields textField, EntPacConsultas.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntPacConsultas.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntPacConsultas.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacConsultas.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacConsultas.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntPacConsultas.Fields refField);
		int FuncionesCount(EntPacConsultas.Fields refField, EntPacConsultas.Fields whereField, object valueField);
		int FuncionesCount(EntPacConsultas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntPacConsultas.Fields refField);
		int FuncionesMin(EntPacConsultas.Fields refField, EntPacConsultas.Fields whereField, object valueField);
		int FuncionesMin(EntPacConsultas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntPacConsultas.Fields refField);
		int FuncionesMax(EntPacConsultas.Fields refField, EntPacConsultas.Fields whereField, object valueField);
		int FuncionesMax(EntPacConsultas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntPacConsultas.Fields refField);
		int FuncionesSum(EntPacConsultas.Fields refField, EntPacConsultas.Fields whereField, object valueField);
		int FuncionesSum(EntPacConsultas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntPacConsultas.Fields refField);
		int FuncionesAvg(EntPacConsultas.Fields refField, EntPacConsultas.Fields whereField, object valueField);
		int FuncionesAvg(EntPacConsultas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

