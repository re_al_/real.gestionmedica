#region 
/***********************************************************************************************************
	NOMBRE:       IPacCertificados
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla paccertificados

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        16/05/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IPacCertificados: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntPacCertificados.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntPacCertificados obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntPacCertificados obj, string strPropiedad);
		EntPacCertificados ObtenerObjetoInsertado(string strUsuCre);
		EntPacCertificados ObtenerObjeto(Int64 Int64idpcm);
		EntPacCertificados ObtenerObjeto(Int64 Int64idpcm, ref CTrans localTrans);
		EntPacCertificados ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntPacCertificados ObtenerObjeto(Hashtable htbFiltro);
		EntPacCertificados ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntPacCertificados ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntPacCertificados ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntPacCertificados ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntPacCertificados ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntPacCertificados ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntPacCertificados ObtenerObjeto(EntPacCertificados.Fields searchField, object searchValue);
		EntPacCertificados ObtenerObjeto(EntPacCertificados.Fields searchField, object searchValue, ref CTrans localTrans);
		EntPacCertificados ObtenerObjeto(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales);
		EntPacCertificados ObtenerObjeto(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntPacCertificados> ObtenerDiccionario();
		Dictionary<String, EntPacCertificados> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntPacCertificados> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntPacCertificados> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntPacCertificados> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntPacCertificados> ObtenerDiccionario(EntPacCertificados.Fields searchField, object searchValue);
		Dictionary<String, EntPacCertificados> ObtenerDiccionario(EntPacCertificados.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntPacCertificados> ObtenerDiccionario(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntPacCertificados> ObtenerDiccionario(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacCertificados> ObtenerLista();
		List<EntPacCertificados> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacCertificados> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacCertificados> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacCertificados> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacCertificados> ObtenerLista(EntPacCertificados.Fields searchField, object searchValue);
		List<EntPacCertificados> ObtenerLista(EntPacCertificados.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacCertificados> ObtenerLista(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacCertificados> ObtenerLista(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacCertificados> ObtenerLista(Hashtable htbFiltro);
		List<EntPacCertificados> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacCertificados> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacCertificados> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacCertificados> ObtenerListaDesdeVista(String strVista);
		List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, EntPacCertificados.Fields searchField, object searchValue);
		List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, EntPacCertificados.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacCertificados> ObtenerListaDesdeVista(String strVista, EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntPacCertificados> ObtenerCola();
		Queue<EntPacCertificados> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntPacCertificados> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntPacCertificados> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntPacCertificados> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntPacCertificados> ObtenerCola(EntPacCertificados.Fields searchField, object searchValue);
		Queue<EntPacCertificados> ObtenerCola(EntPacCertificados.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntPacCertificados> ObtenerCola(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntPacCertificados> ObtenerCola(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntPacCertificados> ObtenerPila();
		Stack<EntPacCertificados> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntPacCertificados> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntPacCertificados> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntPacCertificados> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntPacCertificados> ObtenerPila(EntPacCertificados.Fields searchField, object searchValue);
		Stack<EntPacCertificados> ObtenerPila(EntPacCertificados.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntPacCertificados> ObtenerPila(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntPacCertificados> ObtenerPila(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacCertificados obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacCertificados obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		int InsertUpdate(EntPacCertificados obj);
		int InsertUpdate(EntPacCertificados obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntPacCertificados.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacCertificados.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacCertificados.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, String textField, EntPacCertificados.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, String textField, EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField, EntPacCertificados.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacCertificados.Fields valueField, EntPacCertificados.Fields textField, EntPacCertificados.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntPacCertificados.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntPacCertificados.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacCertificados.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacCertificados.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntPacCertificados.Fields refField);
		int FuncionesCount(EntPacCertificados.Fields refField, EntPacCertificados.Fields whereField, object valueField);
		int FuncionesCount(EntPacCertificados.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntPacCertificados.Fields refField);
		int FuncionesMin(EntPacCertificados.Fields refField, EntPacCertificados.Fields whereField, object valueField);
		int FuncionesMin(EntPacCertificados.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntPacCertificados.Fields refField);
		int FuncionesMax(EntPacCertificados.Fields refField, EntPacCertificados.Fields whereField, object valueField);
		int FuncionesMax(EntPacCertificados.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntPacCertificados.Fields refField);
		int FuncionesSum(EntPacCertificados.Fields refField, EntPacCertificados.Fields whereField, object valueField);
		int FuncionesSum(EntPacCertificados.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntPacCertificados.Fields refField);
		int FuncionesAvg(EntPacCertificados.Fields refField, EntPacCertificados.Fields whereField, object valueField);
		int FuncionesAvg(EntPacCertificados.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

