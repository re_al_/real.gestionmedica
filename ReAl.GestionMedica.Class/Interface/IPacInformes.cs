#region 
/***********************************************************************************************************
	NOMBRE:       IPacInformes
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla pacinformes

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        16/07/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IPacInformes: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntPacInformes.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntPacInformes obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntPacInformes obj, string strPropiedad);
		EntPacInformes ObtenerObjetoInsertado(string strUsuCre);
		EntPacInformes ObtenerObjeto(Int64 Int64idpin);
		EntPacInformes ObtenerObjeto(Int64 Int64idpin, ref CTrans localTrans);
		EntPacInformes ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntPacInformes ObtenerObjeto(Hashtable htbFiltro);
		EntPacInformes ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntPacInformes ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntPacInformes ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntPacInformes ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntPacInformes ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntPacInformes ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntPacInformes ObtenerObjeto(EntPacInformes.Fields searchField, object searchValue);
		EntPacInformes ObtenerObjeto(EntPacInformes.Fields searchField, object searchValue, ref CTrans localTrans);
		EntPacInformes ObtenerObjeto(EntPacInformes.Fields searchField, object searchValue, string strParamAdicionales);
		EntPacInformes ObtenerObjeto(EntPacInformes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntPacInformes> ObtenerDiccionario();
		Dictionary<String, EntPacInformes> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntPacInformes> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntPacInformes> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntPacInformes> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntPacInformes> ObtenerDiccionario(EntPacInformes.Fields searchField, object searchValue);
		Dictionary<String, EntPacInformes> ObtenerDiccionario(EntPacInformes.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntPacInformes> ObtenerDiccionario(EntPacInformes.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntPacInformes> ObtenerDiccionario(EntPacInformes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacInformes> ObtenerLista();
		List<EntPacInformes> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacInformes> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacInformes> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacInformes> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacInformes> ObtenerLista(EntPacInformes.Fields searchField, object searchValue);
		List<EntPacInformes> ObtenerLista(EntPacInformes.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacInformes> ObtenerLista(EntPacInformes.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacInformes> ObtenerLista(EntPacInformes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacInformes> ObtenerLista(Hashtable htbFiltro);
		List<EntPacInformes> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacInformes> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacInformes> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacInformes> ObtenerListaDesdeVista(String strVista);
		List<EntPacInformes> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacInformes> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacInformes> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacInformes> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacInformes> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntPacInformes> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacInformes> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacInformes> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacInformes> ObtenerListaDesdeVista(String strVista, EntPacInformes.Fields searchField, object searchValue);
		List<EntPacInformes> ObtenerListaDesdeVista(String strVista, EntPacInformes.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacInformes> ObtenerListaDesdeVista(String strVista, EntPacInformes.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacInformes> ObtenerListaDesdeVista(String strVista, EntPacInformes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntPacInformes> ObtenerCola();
		Queue<EntPacInformes> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntPacInformes> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntPacInformes> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntPacInformes> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntPacInformes> ObtenerCola(EntPacInformes.Fields searchField, object searchValue);
		Queue<EntPacInformes> ObtenerCola(EntPacInformes.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntPacInformes> ObtenerCola(EntPacInformes.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntPacInformes> ObtenerCola(EntPacInformes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntPacInformes> ObtenerPila();
		Stack<EntPacInformes> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntPacInformes> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntPacInformes> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntPacInformes> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntPacInformes> ObtenerPila(EntPacInformes.Fields searchField, object searchValue);
		Stack<EntPacInformes> ObtenerPila(EntPacInformes.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntPacInformes> ObtenerPila(EntPacInformes.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntPacInformes> ObtenerPila(EntPacInformes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacInformes obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacInformes obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntPacInformes obj, bool bValidar = true);
		bool Insert(EntPacInformes obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntPacInformes obj, bool bValidar = true);
		int Update(EntPacInformes obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntPacInformes obj, bool bValidar = true);
		int Delete(EntPacInformes obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntPacInformes obj);
		int InsertUpdate(EntPacInformes obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntPacInformes.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntPacInformes.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacInformes.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacInformes.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacInformes.Fields valueField, EntPacInformes.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntPacInformes.Fields valueField, EntPacInformes.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacInformes.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntPacInformes.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacInformes.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacInformes.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacInformes.Fields valueField, String textField, EntPacInformes.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacInformes.Fields valueField, String textField, EntPacInformes.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacInformes.Fields valueField, EntPacInformes.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacInformes.Fields valueField, EntPacInformes.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacInformes.Fields valueField, EntPacInformes.Fields textField, EntPacInformes.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacInformes.Fields valueField, EntPacInformes.Fields textField, EntPacInformes.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntPacInformes.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntPacInformes.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacInformes.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacInformes.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntPacInformes.Fields refField);
		int FuncionesCount(EntPacInformes.Fields refField, EntPacInformes.Fields whereField, object valueField);
		int FuncionesCount(EntPacInformes.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntPacInformes.Fields refField);
		int FuncionesMin(EntPacInformes.Fields refField, EntPacInformes.Fields whereField, object valueField);
		int FuncionesMin(EntPacInformes.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntPacInformes.Fields refField);
		int FuncionesMax(EntPacInformes.Fields refField, EntPacInformes.Fields whereField, object valueField);
		int FuncionesMax(EntPacInformes.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntPacInformes.Fields refField);
		int FuncionesSum(EntPacInformes.Fields refField, EntPacInformes.Fields whereField, object valueField);
		int FuncionesSum(EntPacInformes.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntPacInformes.Fields refField);
		int FuncionesAvg(EntPacInformes.Fields refField, EntPacInformes.Fields whereField, object valueField);
		int FuncionesAvg(EntPacInformes.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

