#region 
/***********************************************************************************************************
	NOMBRE:       IPacHistoriaclinica2
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla pachistoriaclinica2

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IPacHistoriaclinica2: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntPacHistoriaclinica2.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntPacHistoriaclinica2 obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntPacHistoriaclinica2 obj, string strPropiedad);
		EntPacHistoriaclinica2 ObtenerObjetoInsertado(string strUsuCre);
		EntPacHistoriaclinica2 ObtenerObjeto(Int64 Int64idppa);
		EntPacHistoriaclinica2 ObtenerObjeto(Int64 Int64idppa, ref CTrans localTrans);
		EntPacHistoriaclinica2 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntPacHistoriaclinica2 ObtenerObjeto(Hashtable htbFiltro);
		EntPacHistoriaclinica2 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntPacHistoriaclinica2 ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntPacHistoriaclinica2 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntPacHistoriaclinica2 ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntPacHistoriaclinica2 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntPacHistoriaclinica2 ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntPacHistoriaclinica2 ObtenerObjeto(EntPacHistoriaclinica2.Fields searchField, object searchValue);
		EntPacHistoriaclinica2 ObtenerObjeto(EntPacHistoriaclinica2.Fields searchField, object searchValue, ref CTrans localTrans);
		EntPacHistoriaclinica2 ObtenerObjeto(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales);
		EntPacHistoriaclinica2 ObtenerObjeto(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario();
		Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(EntPacHistoriaclinica2.Fields searchField, object searchValue);
		Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(EntPacHistoriaclinica2.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntPacHistoriaclinica2> ObtenerDiccionario(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacHistoriaclinica2> ObtenerLista();
		List<EntPacHistoriaclinica2> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacHistoriaclinica2> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacHistoriaclinica2> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacHistoriaclinica2> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacHistoriaclinica2> ObtenerLista(EntPacHistoriaclinica2.Fields searchField, object searchValue);
		List<EntPacHistoriaclinica2> ObtenerLista(EntPacHistoriaclinica2.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacHistoriaclinica2> ObtenerLista(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacHistoriaclinica2> ObtenerLista(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacHistoriaclinica2> ObtenerLista(Hashtable htbFiltro);
		List<EntPacHistoriaclinica2> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacHistoriaclinica2> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacHistoriaclinica2> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista);
		List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica2.Fields searchField, object searchValue);
		List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica2.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacHistoriaclinica2> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntPacHistoriaclinica2> ObtenerCola();
		Queue<EntPacHistoriaclinica2> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntPacHistoriaclinica2> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntPacHistoriaclinica2> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntPacHistoriaclinica2> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntPacHistoriaclinica2> ObtenerCola(EntPacHistoriaclinica2.Fields searchField, object searchValue);
		Queue<EntPacHistoriaclinica2> ObtenerCola(EntPacHistoriaclinica2.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntPacHistoriaclinica2> ObtenerCola(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntPacHistoriaclinica2> ObtenerCola(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntPacHistoriaclinica2> ObtenerPila();
		Stack<EntPacHistoriaclinica2> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntPacHistoriaclinica2> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntPacHistoriaclinica2> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntPacHistoriaclinica2> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntPacHistoriaclinica2> ObtenerPila(EntPacHistoriaclinica2.Fields searchField, object searchValue);
		Stack<EntPacHistoriaclinica2> ObtenerPila(EntPacHistoriaclinica2.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntPacHistoriaclinica2> ObtenerPila(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntPacHistoriaclinica2> ObtenerPila(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacHistoriaclinica2 obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacHistoriaclinica2 obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntPacHistoriaclinica2 obj, bool bValidar = true);
		bool Insert(EntPacHistoriaclinica2 obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntPacHistoriaclinica2 obj, bool bValidar = true);
		int Update(EntPacHistoriaclinica2 obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntPacHistoriaclinica2 obj);
		int InsertUpdate(EntPacHistoriaclinica2 obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntPacHistoriaclinica2.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacHistoriaclinica2.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, String textField, EntPacHistoriaclinica2.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, String textField, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, EntPacHistoriaclinica2.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntPacHistoriaclinica2.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica2.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, String textField, EntPacHistoriaclinica2.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, String textField, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, EntPacHistoriaclinica2.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica2.Fields valueField, EntPacHistoriaclinica2.Fields textField, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntPacHistoriaclinica2.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica2.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica2.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntPacHistoriaclinica2.Fields refField);
		int FuncionesCount(EntPacHistoriaclinica2.Fields refField, EntPacHistoriaclinica2.Fields whereField, object valueField);
		int FuncionesCount(EntPacHistoriaclinica2.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntPacHistoriaclinica2.Fields refField);
		int FuncionesMin(EntPacHistoriaclinica2.Fields refField, EntPacHistoriaclinica2.Fields whereField, object valueField);
		int FuncionesMin(EntPacHistoriaclinica2.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntPacHistoriaclinica2.Fields refField);
		int FuncionesMax(EntPacHistoriaclinica2.Fields refField, EntPacHistoriaclinica2.Fields whereField, object valueField);
		int FuncionesMax(EntPacHistoriaclinica2.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntPacHistoriaclinica2.Fields refField);
		int FuncionesSum(EntPacHistoriaclinica2.Fields refField, EntPacHistoriaclinica2.Fields whereField, object valueField);
		int FuncionesSum(EntPacHistoriaclinica2.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntPacHistoriaclinica2.Fields refField);
		int FuncionesAvg(EntPacHistoriaclinica2.Fields refField, EntPacHistoriaclinica2.Fields whereField, object valueField);
		int FuncionesAvg(EntPacHistoriaclinica2.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

