#region 
/***********************************************************************************************************
	NOMBRE:       ISegTablas
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla segtablas

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface ISegTablas: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntSegTablas.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntSegTablas obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntSegTablas obj, string strPropiedad);
		EntSegTablas ObtenerObjetoInsertado(string strUsuCre);
		EntSegTablas ObtenerObjeto(string stringtablasta);
		EntSegTablas ObtenerObjeto(string stringtablasta, ref CTrans localTrans);
		EntSegTablas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntSegTablas ObtenerObjeto(Hashtable htbFiltro);
		EntSegTablas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntSegTablas ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntSegTablas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntSegTablas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntSegTablas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntSegTablas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntSegTablas ObtenerObjeto(EntSegTablas.Fields searchField, object searchValue);
		EntSegTablas ObtenerObjeto(EntSegTablas.Fields searchField, object searchValue, ref CTrans localTrans);
		EntSegTablas ObtenerObjeto(EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales);
		EntSegTablas ObtenerObjeto(EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntSegTablas> ObtenerDiccionario();
		Dictionary<String, EntSegTablas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntSegTablas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntSegTablas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntSegTablas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntSegTablas> ObtenerDiccionario(EntSegTablas.Fields searchField, object searchValue);
		Dictionary<String, EntSegTablas> ObtenerDiccionario(EntSegTablas.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntSegTablas> ObtenerDiccionario(EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntSegTablas> ObtenerDiccionario(EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegTablas> ObtenerLista();
		List<EntSegTablas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegTablas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegTablas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegTablas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegTablas> ObtenerLista(EntSegTablas.Fields searchField, object searchValue);
		List<EntSegTablas> ObtenerLista(EntSegTablas.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegTablas> ObtenerLista(EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegTablas> ObtenerLista(EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegTablas> ObtenerLista(Hashtable htbFiltro);
		List<EntSegTablas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegTablas> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegTablas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegTablas> ObtenerListaDesdeVista(String strVista);
		List<EntSegTablas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegTablas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegTablas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegTablas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegTablas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntSegTablas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegTablas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegTablas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegTablas> ObtenerListaDesdeVista(String strVista, EntSegTablas.Fields searchField, object searchValue);
		List<EntSegTablas> ObtenerListaDesdeVista(String strVista, EntSegTablas.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegTablas> ObtenerListaDesdeVista(String strVista, EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegTablas> ObtenerListaDesdeVista(String strVista, EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntSegTablas> ObtenerCola();
		Queue<EntSegTablas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntSegTablas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntSegTablas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntSegTablas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntSegTablas> ObtenerCola(EntSegTablas.Fields searchField, object searchValue);
		Queue<EntSegTablas> ObtenerCola(EntSegTablas.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntSegTablas> ObtenerCola(EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntSegTablas> ObtenerCola(EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntSegTablas> ObtenerPila();
		Stack<EntSegTablas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntSegTablas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntSegTablas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntSegTablas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntSegTablas> ObtenerPila(EntSegTablas.Fields searchField, object searchValue);
		Stack<EntSegTablas> ObtenerPila(EntSegTablas.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntSegTablas> ObtenerPila(EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntSegTablas> ObtenerPila(EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegTablas obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegTablas obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntSegTablas obj, bool bValidar = true);
		bool Insert(EntSegTablas obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntSegTablas obj, bool bValidar = true);
		int Update(EntSegTablas obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntSegTablas obj, bool bValidar = true);
		int Delete(EntSegTablas obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntSegTablas obj);
		int InsertUpdate(EntSegTablas obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntSegTablas.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegTablas.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegTablas.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTablas.Fields valueField, EntSegTablas.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegTablas.Fields valueField, EntSegTablas.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTablas.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegTablas.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTablas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegTablas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTablas.Fields valueField, String textField, EntSegTablas.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegTablas.Fields valueField, String textField, EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTablas.Fields valueField, EntSegTablas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegTablas.Fields valueField, EntSegTablas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTablas.Fields valueField, EntSegTablas.Fields textField, EntSegTablas.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegTablas.Fields valueField, EntSegTablas.Fields textField, EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntSegTablas.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntSegTablas.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegTablas.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegTablas.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTablas.Fields valueField, EntSegTablas.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntSegTablas.Fields valueField, EntSegTablas.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTablas.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntSegTablas.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTablas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegTablas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTablas.Fields valueField, String textField, EntSegTablas.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegTablas.Fields valueField, String textField, EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTablas.Fields valueField, EntSegTablas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegTablas.Fields valueField, EntSegTablas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTablas.Fields valueField, EntSegTablas.Fields textField, EntSegTablas.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegTablas.Fields valueField, EntSegTablas.Fields textField, EntSegTablas.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntSegTablas.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntSegTablas.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegTablas.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegTablas.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntSegTablas.Fields refField);
		int FuncionesCount(EntSegTablas.Fields refField, EntSegTablas.Fields whereField, object valueField);
		int FuncionesCount(EntSegTablas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntSegTablas.Fields refField);
		int FuncionesMin(EntSegTablas.Fields refField, EntSegTablas.Fields whereField, object valueField);
		int FuncionesMin(EntSegTablas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntSegTablas.Fields refField);
		int FuncionesMax(EntSegTablas.Fields refField, EntSegTablas.Fields whereField, object valueField);
		int FuncionesMax(EntSegTablas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntSegTablas.Fields refField);
		int FuncionesSum(EntSegTablas.Fields refField, EntSegTablas.Fields whereField, object valueField);
		int FuncionesSum(EntSegTablas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntSegTablas.Fields refField);
		int FuncionesAvg(EntSegTablas.Fields refField, EntSegTablas.Fields whereField, object valueField);
		int FuncionesAvg(EntSegTablas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

