#region 
/***********************************************************************************************************
	NOMBRE:       ISegAplicaciones
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla segaplicaciones

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface ISegAplicaciones: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntSegAplicaciones.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntSegAplicaciones obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntSegAplicaciones obj, string strPropiedad);
		EntSegAplicaciones ObtenerObjetoInsertado(string strUsuCre);
		EntSegAplicaciones ObtenerObjeto(string stringaplicacionsap);
		EntSegAplicaciones ObtenerObjeto(string stringaplicacionsap, ref CTrans localTrans);
		EntSegAplicaciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntSegAplicaciones ObtenerObjeto(Hashtable htbFiltro);
		EntSegAplicaciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntSegAplicaciones ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntSegAplicaciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntSegAplicaciones ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntSegAplicaciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntSegAplicaciones ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntSegAplicaciones ObtenerObjeto(EntSegAplicaciones.Fields searchField, object searchValue);
		EntSegAplicaciones ObtenerObjeto(EntSegAplicaciones.Fields searchField, object searchValue, ref CTrans localTrans);
		EntSegAplicaciones ObtenerObjeto(EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales);
		EntSegAplicaciones ObtenerObjeto(EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntSegAplicaciones> ObtenerDiccionario();
		Dictionary<String, EntSegAplicaciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntSegAplicaciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntSegAplicaciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntSegAplicaciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntSegAplicaciones> ObtenerDiccionario(EntSegAplicaciones.Fields searchField, object searchValue);
		Dictionary<String, EntSegAplicaciones> ObtenerDiccionario(EntSegAplicaciones.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntSegAplicaciones> ObtenerDiccionario(EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntSegAplicaciones> ObtenerDiccionario(EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegAplicaciones> ObtenerLista();
		List<EntSegAplicaciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegAplicaciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegAplicaciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegAplicaciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegAplicaciones> ObtenerLista(EntSegAplicaciones.Fields searchField, object searchValue);
		List<EntSegAplicaciones> ObtenerLista(EntSegAplicaciones.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegAplicaciones> ObtenerLista(EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegAplicaciones> ObtenerLista(EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegAplicaciones> ObtenerLista(Hashtable htbFiltro);
		List<EntSegAplicaciones> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegAplicaciones> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegAplicaciones> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegAplicaciones> ObtenerListaDesdeVista(String strVista);
		List<EntSegAplicaciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegAplicaciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegAplicaciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegAplicaciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegAplicaciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntSegAplicaciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegAplicaciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegAplicaciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegAplicaciones> ObtenerListaDesdeVista(String strVista, EntSegAplicaciones.Fields searchField, object searchValue);
		List<EntSegAplicaciones> ObtenerListaDesdeVista(String strVista, EntSegAplicaciones.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegAplicaciones> ObtenerListaDesdeVista(String strVista, EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegAplicaciones> ObtenerListaDesdeVista(String strVista, EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntSegAplicaciones> ObtenerCola();
		Queue<EntSegAplicaciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntSegAplicaciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntSegAplicaciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntSegAplicaciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntSegAplicaciones> ObtenerCola(EntSegAplicaciones.Fields searchField, object searchValue);
		Queue<EntSegAplicaciones> ObtenerCola(EntSegAplicaciones.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntSegAplicaciones> ObtenerCola(EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntSegAplicaciones> ObtenerCola(EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntSegAplicaciones> ObtenerPila();
		Stack<EntSegAplicaciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntSegAplicaciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntSegAplicaciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntSegAplicaciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntSegAplicaciones> ObtenerPila(EntSegAplicaciones.Fields searchField, object searchValue);
		Stack<EntSegAplicaciones> ObtenerPila(EntSegAplicaciones.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntSegAplicaciones> ObtenerPila(EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntSegAplicaciones> ObtenerPila(EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegAplicaciones obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegAplicaciones obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntSegAplicaciones obj, bool bValidar = true);
		bool Insert(EntSegAplicaciones obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntSegAplicaciones obj, bool bValidar = true);
		int Update(EntSegAplicaciones obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntSegAplicaciones obj, bool bValidar = true);
		int Delete(EntSegAplicaciones obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntSegAplicaciones obj);
		int InsertUpdate(EntSegAplicaciones obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntSegAplicaciones.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegAplicaciones.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegAplicaciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegAplicaciones.Fields valueField, EntSegAplicaciones.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegAplicaciones.Fields valueField, EntSegAplicaciones.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegAplicaciones.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegAplicaciones.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegAplicaciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegAplicaciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegAplicaciones.Fields valueField, String textField, EntSegAplicaciones.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegAplicaciones.Fields valueField, String textField, EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegAplicaciones.Fields valueField, EntSegAplicaciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegAplicaciones.Fields valueField, EntSegAplicaciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegAplicaciones.Fields valueField, EntSegAplicaciones.Fields textField, EntSegAplicaciones.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegAplicaciones.Fields valueField, EntSegAplicaciones.Fields textField, EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntSegAplicaciones.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntSegAplicaciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegAplicaciones.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegAplicaciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegAplicaciones.Fields valueField, EntSegAplicaciones.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntSegAplicaciones.Fields valueField, EntSegAplicaciones.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegAplicaciones.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntSegAplicaciones.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegAplicaciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegAplicaciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegAplicaciones.Fields valueField, String textField, EntSegAplicaciones.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegAplicaciones.Fields valueField, String textField, EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegAplicaciones.Fields valueField, EntSegAplicaciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegAplicaciones.Fields valueField, EntSegAplicaciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegAplicaciones.Fields valueField, EntSegAplicaciones.Fields textField, EntSegAplicaciones.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegAplicaciones.Fields valueField, EntSegAplicaciones.Fields textField, EntSegAplicaciones.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntSegAplicaciones.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntSegAplicaciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegAplicaciones.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegAplicaciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntSegAplicaciones.Fields refField);
		int FuncionesCount(EntSegAplicaciones.Fields refField, EntSegAplicaciones.Fields whereField, object valueField);
		int FuncionesCount(EntSegAplicaciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntSegAplicaciones.Fields refField);
		int FuncionesMin(EntSegAplicaciones.Fields refField, EntSegAplicaciones.Fields whereField, object valueField);
		int FuncionesMin(EntSegAplicaciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntSegAplicaciones.Fields refField);
		int FuncionesMax(EntSegAplicaciones.Fields refField, EntSegAplicaciones.Fields whereField, object valueField);
		int FuncionesMax(EntSegAplicaciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntSegAplicaciones.Fields refField);
		int FuncionesSum(EntSegAplicaciones.Fields refField, EntSegAplicaciones.Fields whereField, object valueField);
		int FuncionesSum(EntSegAplicaciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntSegAplicaciones.Fields refField);
		int FuncionesAvg(EntSegAplicaciones.Fields refField, EntSegAplicaciones.Fields whereField, object valueField);
		int FuncionesAvg(EntSegAplicaciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

