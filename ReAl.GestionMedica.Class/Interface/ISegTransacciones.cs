#region 
/***********************************************************************************************************
	NOMBRE:       ISegTransacciones
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla segtransacciones

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface ISegTransacciones: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntSegTransacciones.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntSegTransacciones obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntSegTransacciones obj, string strPropiedad);
		EntSegTransacciones ObtenerObjetoInsertado(string strUsuCre);
		EntSegTransacciones ObtenerObjeto(string stringtablasta, string stringtransaccionstr);
		EntSegTransacciones ObtenerObjeto(string stringtablasta, string stringtransaccionstr, ref CTrans localTrans);
		EntSegTransacciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntSegTransacciones ObtenerObjeto(Hashtable htbFiltro);
		EntSegTransacciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntSegTransacciones ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntSegTransacciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntSegTransacciones ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntSegTransacciones ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntSegTransacciones ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntSegTransacciones ObtenerObjeto(EntSegTransacciones.Fields searchField, object searchValue);
		EntSegTransacciones ObtenerObjeto(EntSegTransacciones.Fields searchField, object searchValue, ref CTrans localTrans);
		EntSegTransacciones ObtenerObjeto(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		EntSegTransacciones ObtenerObjeto(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntSegTransacciones> ObtenerDiccionario();
		Dictionary<String, EntSegTransacciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntSegTransacciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntSegTransacciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntSegTransacciones> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntSegTransacciones> ObtenerDiccionario(EntSegTransacciones.Fields searchField, object searchValue);
		Dictionary<String, EntSegTransacciones> ObtenerDiccionario(EntSegTransacciones.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntSegTransacciones> ObtenerDiccionario(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntSegTransacciones> ObtenerDiccionario(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegTransacciones> ObtenerLista();
		List<EntSegTransacciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegTransacciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegTransacciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegTransacciones> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegTransacciones> ObtenerLista(EntSegTransacciones.Fields searchField, object searchValue);
		List<EntSegTransacciones> ObtenerLista(EntSegTransacciones.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegTransacciones> ObtenerLista(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegTransacciones> ObtenerLista(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegTransacciones> ObtenerLista(Hashtable htbFiltro);
		List<EntSegTransacciones> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegTransacciones> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegTransacciones> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista);
		List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, EntSegTransacciones.Fields searchField, object searchValue);
		List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, EntSegTransacciones.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntSegTransacciones> ObtenerListaDesdeVista(String strVista, EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntSegTransacciones> ObtenerCola();
		Queue<EntSegTransacciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntSegTransacciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntSegTransacciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntSegTransacciones> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntSegTransacciones> ObtenerCola(EntSegTransacciones.Fields searchField, object searchValue);
		Queue<EntSegTransacciones> ObtenerCola(EntSegTransacciones.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntSegTransacciones> ObtenerCola(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntSegTransacciones> ObtenerCola(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntSegTransacciones> ObtenerPila();
		Stack<EntSegTransacciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntSegTransacciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntSegTransacciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntSegTransacciones> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntSegTransacciones> ObtenerPila(EntSegTransacciones.Fields searchField, object searchValue);
		Stack<EntSegTransacciones> ObtenerPila(EntSegTransacciones.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntSegTransacciones> ObtenerPila(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntSegTransacciones> ObtenerPila(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegTransacciones obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntSegTransacciones obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntSegTransacciones obj, bool bValidar = true);
		bool Insert(EntSegTransacciones obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntSegTransacciones obj, bool bValidar = true);
		int Update(EntSegTransacciones obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntSegTransacciones obj, bool bValidar = true);
		int Delete(EntSegTransacciones obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntSegTransacciones obj);
		int InsertUpdate(EntSegTransacciones obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntSegTransacciones.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegTransacciones.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntSegTransacciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, String textField, EntSegTransacciones.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, String textField, EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, EntSegTransacciones.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntSegTransacciones.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntSegTransacciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegTransacciones.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntSegTransacciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, String textField, EntSegTransacciones.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, String textField, EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, EntSegTransacciones.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntSegTransacciones.Fields valueField, EntSegTransacciones.Fields textField, EntSegTransacciones.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntSegTransacciones.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntSegTransacciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegTransacciones.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntSegTransacciones.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntSegTransacciones.Fields refField);
		int FuncionesCount(EntSegTransacciones.Fields refField, EntSegTransacciones.Fields whereField, object valueField);
		int FuncionesCount(EntSegTransacciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntSegTransacciones.Fields refField);
		int FuncionesMin(EntSegTransacciones.Fields refField, EntSegTransacciones.Fields whereField, object valueField);
		int FuncionesMin(EntSegTransacciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntSegTransacciones.Fields refField);
		int FuncionesMax(EntSegTransacciones.Fields refField, EntSegTransacciones.Fields whereField, object valueField);
		int FuncionesMax(EntSegTransacciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntSegTransacciones.Fields refField);
		int FuncionesSum(EntSegTransacciones.Fields refField, EntSegTransacciones.Fields whereField, object valueField);
		int FuncionesSum(EntSegTransacciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntSegTransacciones.Fields refField);
		int FuncionesAvg(EntSegTransacciones.Fields refField, EntSegTransacciones.Fields whereField, object valueField);
		int FuncionesAvg(EntSegTransacciones.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

