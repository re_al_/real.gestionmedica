#region 
/***********************************************************************************************************
	NOMBRE:       IClaEstadocivil
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla claestadocivil

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IClaEstadocivil: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntClaEstadocivil.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntClaEstadocivil obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntClaEstadocivil obj, string strPropiedad);
		EntClaEstadocivil ObtenerObjetoInsertado(string strUsuCre);
		EntClaEstadocivil ObtenerObjeto(int intidcec);
		EntClaEstadocivil ObtenerObjeto(int intidcec, ref CTrans localTrans);
		EntClaEstadocivil ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntClaEstadocivil ObtenerObjeto(Hashtable htbFiltro);
		EntClaEstadocivil ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntClaEstadocivil ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntClaEstadocivil ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntClaEstadocivil ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntClaEstadocivil ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntClaEstadocivil ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntClaEstadocivil ObtenerObjeto(EntClaEstadocivil.Fields searchField, object searchValue);
		EntClaEstadocivil ObtenerObjeto(EntClaEstadocivil.Fields searchField, object searchValue, ref CTrans localTrans);
		EntClaEstadocivil ObtenerObjeto(EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales);
		EntClaEstadocivil ObtenerObjeto(EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntClaEstadocivil> ObtenerDiccionario();
		Dictionary<String, EntClaEstadocivil> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntClaEstadocivil> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntClaEstadocivil> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntClaEstadocivil> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntClaEstadocivil> ObtenerDiccionario(EntClaEstadocivil.Fields searchField, object searchValue);
		Dictionary<String, EntClaEstadocivil> ObtenerDiccionario(EntClaEstadocivil.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntClaEstadocivil> ObtenerDiccionario(EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntClaEstadocivil> ObtenerDiccionario(EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntClaEstadocivil> ObtenerLista();
		List<EntClaEstadocivil> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntClaEstadocivil> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntClaEstadocivil> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntClaEstadocivil> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntClaEstadocivil> ObtenerLista(EntClaEstadocivil.Fields searchField, object searchValue);
		List<EntClaEstadocivil> ObtenerLista(EntClaEstadocivil.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntClaEstadocivil> ObtenerLista(EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntClaEstadocivil> ObtenerLista(EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntClaEstadocivil> ObtenerLista(Hashtable htbFiltro);
		List<EntClaEstadocivil> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntClaEstadocivil> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntClaEstadocivil> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntClaEstadocivil> ObtenerListaDesdeVista(String strVista);
		List<EntClaEstadocivil> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntClaEstadocivil> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntClaEstadocivil> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntClaEstadocivil> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntClaEstadocivil> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntClaEstadocivil> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntClaEstadocivil> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntClaEstadocivil> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntClaEstadocivil> ObtenerListaDesdeVista(String strVista, EntClaEstadocivil.Fields searchField, object searchValue);
		List<EntClaEstadocivil> ObtenerListaDesdeVista(String strVista, EntClaEstadocivil.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntClaEstadocivil> ObtenerListaDesdeVista(String strVista, EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntClaEstadocivil> ObtenerListaDesdeVista(String strVista, EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntClaEstadocivil> ObtenerCola();
		Queue<EntClaEstadocivil> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntClaEstadocivil> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntClaEstadocivil> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntClaEstadocivil> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntClaEstadocivil> ObtenerCola(EntClaEstadocivil.Fields searchField, object searchValue);
		Queue<EntClaEstadocivil> ObtenerCola(EntClaEstadocivil.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntClaEstadocivil> ObtenerCola(EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntClaEstadocivil> ObtenerCola(EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntClaEstadocivil> ObtenerPila();
		Stack<EntClaEstadocivil> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntClaEstadocivil> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntClaEstadocivil> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntClaEstadocivil> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntClaEstadocivil> ObtenerPila(EntClaEstadocivil.Fields searchField, object searchValue);
		Stack<EntClaEstadocivil> ObtenerPila(EntClaEstadocivil.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntClaEstadocivil> ObtenerPila(EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntClaEstadocivil> ObtenerPila(EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntClaEstadocivil obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntClaEstadocivil obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntClaEstadocivil obj, bool bValidar = true);
		bool Insert(EntClaEstadocivil obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntClaEstadocivil obj, bool bValidar = true);
		int Update(EntClaEstadocivil obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntClaEstadocivil obj, bool bValidar = true);
		int Delete(EntClaEstadocivil obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntClaEstadocivil obj);
		int InsertUpdate(EntClaEstadocivil obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntClaEstadocivil.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntClaEstadocivil.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntClaEstadocivil.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaEstadocivil.Fields valueField, EntClaEstadocivil.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntClaEstadocivil.Fields valueField, EntClaEstadocivil.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaEstadocivil.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntClaEstadocivil.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaEstadocivil.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntClaEstadocivil.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaEstadocivil.Fields valueField, String textField, EntClaEstadocivil.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntClaEstadocivil.Fields valueField, String textField, EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaEstadocivil.Fields valueField, EntClaEstadocivil.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntClaEstadocivil.Fields valueField, EntClaEstadocivil.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaEstadocivil.Fields valueField, EntClaEstadocivil.Fields textField, EntClaEstadocivil.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntClaEstadocivil.Fields valueField, EntClaEstadocivil.Fields textField, EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntClaEstadocivil.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntClaEstadocivil.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntClaEstadocivil.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntClaEstadocivil.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaEstadocivil.Fields valueField, EntClaEstadocivil.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntClaEstadocivil.Fields valueField, EntClaEstadocivil.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaEstadocivil.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntClaEstadocivil.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaEstadocivil.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntClaEstadocivil.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaEstadocivil.Fields valueField, String textField, EntClaEstadocivil.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntClaEstadocivil.Fields valueField, String textField, EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaEstadocivil.Fields valueField, EntClaEstadocivil.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntClaEstadocivil.Fields valueField, EntClaEstadocivil.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaEstadocivil.Fields valueField, EntClaEstadocivil.Fields textField, EntClaEstadocivil.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntClaEstadocivil.Fields valueField, EntClaEstadocivil.Fields textField, EntClaEstadocivil.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntClaEstadocivil.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntClaEstadocivil.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntClaEstadocivil.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntClaEstadocivil.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntClaEstadocivil.Fields refField);
		int FuncionesCount(EntClaEstadocivil.Fields refField, EntClaEstadocivil.Fields whereField, object valueField);
		int FuncionesCount(EntClaEstadocivil.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntClaEstadocivil.Fields refField);
		int FuncionesMin(EntClaEstadocivil.Fields refField, EntClaEstadocivil.Fields whereField, object valueField);
		int FuncionesMin(EntClaEstadocivil.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntClaEstadocivil.Fields refField);
		int FuncionesMax(EntClaEstadocivil.Fields refField, EntClaEstadocivil.Fields whereField, object valueField);
		int FuncionesMax(EntClaEstadocivil.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntClaEstadocivil.Fields refField);
		int FuncionesSum(EntClaEstadocivil.Fields refField, EntClaEstadocivil.Fields whereField, object valueField);
		int FuncionesSum(EntClaEstadocivil.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntClaEstadocivil.Fields refField);
		int FuncionesAvg(EntClaEstadocivil.Fields refField, EntClaEstadocivil.Fields whereField, object valueField);
		int FuncionesAvg(EntClaEstadocivil.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

