#region 
/***********************************************************************************************************
	NOMBRE:       IPacHistoriaclinica3
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla pachistoriaclinica3

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IPacHistoriaclinica3: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntPacHistoriaclinica3.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntPacHistoriaclinica3 obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntPacHistoriaclinica3 obj, string strPropiedad);
		EntPacHistoriaclinica3 ObtenerObjetoInsertado(string strUsuCre);
		EntPacHistoriaclinica3 ObtenerObjeto(Int64 Int64idppa);
		EntPacHistoriaclinica3 ObtenerObjeto(Int64 Int64idppa, ref CTrans localTrans);
		EntPacHistoriaclinica3 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntPacHistoriaclinica3 ObtenerObjeto(Hashtable htbFiltro);
		EntPacHistoriaclinica3 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntPacHistoriaclinica3 ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntPacHistoriaclinica3 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntPacHistoriaclinica3 ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntPacHistoriaclinica3 ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntPacHistoriaclinica3 ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntPacHistoriaclinica3 ObtenerObjeto(EntPacHistoriaclinica3.Fields searchField, object searchValue);
		EntPacHistoriaclinica3 ObtenerObjeto(EntPacHistoriaclinica3.Fields searchField, object searchValue, ref CTrans localTrans);
		EntPacHistoriaclinica3 ObtenerObjeto(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales);
		EntPacHistoriaclinica3 ObtenerObjeto(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario();
		Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(EntPacHistoriaclinica3.Fields searchField, object searchValue);
		Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(EntPacHistoriaclinica3.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntPacHistoriaclinica3> ObtenerDiccionario(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacHistoriaclinica3> ObtenerLista();
		List<EntPacHistoriaclinica3> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacHistoriaclinica3> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacHistoriaclinica3> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacHistoriaclinica3> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacHistoriaclinica3> ObtenerLista(EntPacHistoriaclinica3.Fields searchField, object searchValue);
		List<EntPacHistoriaclinica3> ObtenerLista(EntPacHistoriaclinica3.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacHistoriaclinica3> ObtenerLista(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacHistoriaclinica3> ObtenerLista(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacHistoriaclinica3> ObtenerLista(Hashtable htbFiltro);
		List<EntPacHistoriaclinica3> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacHistoriaclinica3> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacHistoriaclinica3> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista);
		List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica3.Fields searchField, object searchValue);
		List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica3.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacHistoriaclinica3> ObtenerListaDesdeVista(String strVista, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntPacHistoriaclinica3> ObtenerCola();
		Queue<EntPacHistoriaclinica3> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntPacHistoriaclinica3> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntPacHistoriaclinica3> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntPacHistoriaclinica3> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntPacHistoriaclinica3> ObtenerCola(EntPacHistoriaclinica3.Fields searchField, object searchValue);
		Queue<EntPacHistoriaclinica3> ObtenerCola(EntPacHistoriaclinica3.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntPacHistoriaclinica3> ObtenerCola(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntPacHistoriaclinica3> ObtenerCola(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntPacHistoriaclinica3> ObtenerPila();
		Stack<EntPacHistoriaclinica3> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntPacHistoriaclinica3> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntPacHistoriaclinica3> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntPacHistoriaclinica3> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntPacHistoriaclinica3> ObtenerPila(EntPacHistoriaclinica3.Fields searchField, object searchValue);
		Stack<EntPacHistoriaclinica3> ObtenerPila(EntPacHistoriaclinica3.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntPacHistoriaclinica3> ObtenerPila(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntPacHistoriaclinica3> ObtenerPila(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacHistoriaclinica3 obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacHistoriaclinica3 obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntPacHistoriaclinica3 obj, bool bValidar = true);
		bool Insert(EntPacHistoriaclinica3 obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntPacHistoriaclinica3 obj, bool bValidar = true);
		int Update(EntPacHistoriaclinica3 obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntPacHistoriaclinica3 obj);
		int InsertUpdate(EntPacHistoriaclinica3 obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntPacHistoriaclinica3.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacHistoriaclinica3.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, String textField, EntPacHistoriaclinica3.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, String textField, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, EntPacHistoriaclinica3.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntPacHistoriaclinica3.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica3.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, String textField, EntPacHistoriaclinica3.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, String textField, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, EntPacHistoriaclinica3.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacHistoriaclinica3.Fields valueField, EntPacHistoriaclinica3.Fields textField, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntPacHistoriaclinica3.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica3.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacHistoriaclinica3.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntPacHistoriaclinica3.Fields refField);
		int FuncionesCount(EntPacHistoriaclinica3.Fields refField, EntPacHistoriaclinica3.Fields whereField, object valueField);
		int FuncionesCount(EntPacHistoriaclinica3.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntPacHistoriaclinica3.Fields refField);
		int FuncionesMin(EntPacHistoriaclinica3.Fields refField, EntPacHistoriaclinica3.Fields whereField, object valueField);
		int FuncionesMin(EntPacHistoriaclinica3.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntPacHistoriaclinica3.Fields refField);
		int FuncionesMax(EntPacHistoriaclinica3.Fields refField, EntPacHistoriaclinica3.Fields whereField, object valueField);
		int FuncionesMax(EntPacHistoriaclinica3.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntPacHistoriaclinica3.Fields refField);
		int FuncionesSum(EntPacHistoriaclinica3.Fields refField, EntPacHistoriaclinica3.Fields whereField, object valueField);
		int FuncionesSum(EntPacHistoriaclinica3.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntPacHistoriaclinica3.Fields refField);
		int FuncionesAvg(EntPacHistoriaclinica3.Fields refField, EntPacHistoriaclinica3.Fields whereField, object valueField);
		int FuncionesAvg(EntPacHistoriaclinica3.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

