#region 
/***********************************************************************************************************
	NOMBRE:       IClaTiposangre
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla clatiposangre

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IClaTiposangre: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntClaTiposangre.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntClaTiposangre obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntClaTiposangre obj, string strPropiedad);
		EntClaTiposangre ObtenerObjetoInsertado(string strUsuCre);
		EntClaTiposangre ObtenerObjeto(int intidcts);
		EntClaTiposangre ObtenerObjeto(int intidcts, ref CTrans localTrans);
		EntClaTiposangre ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntClaTiposangre ObtenerObjeto(Hashtable htbFiltro);
		EntClaTiposangre ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntClaTiposangre ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntClaTiposangre ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntClaTiposangre ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntClaTiposangre ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntClaTiposangre ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntClaTiposangre ObtenerObjeto(EntClaTiposangre.Fields searchField, object searchValue);
		EntClaTiposangre ObtenerObjeto(EntClaTiposangre.Fields searchField, object searchValue, ref CTrans localTrans);
		EntClaTiposangre ObtenerObjeto(EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales);
		EntClaTiposangre ObtenerObjeto(EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntClaTiposangre> ObtenerDiccionario();
		Dictionary<String, EntClaTiposangre> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntClaTiposangre> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntClaTiposangre> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntClaTiposangre> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntClaTiposangre> ObtenerDiccionario(EntClaTiposangre.Fields searchField, object searchValue);
		Dictionary<String, EntClaTiposangre> ObtenerDiccionario(EntClaTiposangre.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntClaTiposangre> ObtenerDiccionario(EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntClaTiposangre> ObtenerDiccionario(EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntClaTiposangre> ObtenerLista();
		List<EntClaTiposangre> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntClaTiposangre> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntClaTiposangre> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntClaTiposangre> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntClaTiposangre> ObtenerLista(EntClaTiposangre.Fields searchField, object searchValue);
		List<EntClaTiposangre> ObtenerLista(EntClaTiposangre.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntClaTiposangre> ObtenerLista(EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntClaTiposangre> ObtenerLista(EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntClaTiposangre> ObtenerLista(Hashtable htbFiltro);
		List<EntClaTiposangre> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntClaTiposangre> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntClaTiposangre> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntClaTiposangre> ObtenerListaDesdeVista(String strVista);
		List<EntClaTiposangre> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntClaTiposangre> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntClaTiposangre> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntClaTiposangre> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntClaTiposangre> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntClaTiposangre> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntClaTiposangre> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntClaTiposangre> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntClaTiposangre> ObtenerListaDesdeVista(String strVista, EntClaTiposangre.Fields searchField, object searchValue);
		List<EntClaTiposangre> ObtenerListaDesdeVista(String strVista, EntClaTiposangre.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntClaTiposangre> ObtenerListaDesdeVista(String strVista, EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntClaTiposangre> ObtenerListaDesdeVista(String strVista, EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntClaTiposangre> ObtenerCola();
		Queue<EntClaTiposangre> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntClaTiposangre> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntClaTiposangre> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntClaTiposangre> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntClaTiposangre> ObtenerCola(EntClaTiposangre.Fields searchField, object searchValue);
		Queue<EntClaTiposangre> ObtenerCola(EntClaTiposangre.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntClaTiposangre> ObtenerCola(EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntClaTiposangre> ObtenerCola(EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntClaTiposangre> ObtenerPila();
		Stack<EntClaTiposangre> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntClaTiposangre> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntClaTiposangre> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntClaTiposangre> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntClaTiposangre> ObtenerPila(EntClaTiposangre.Fields searchField, object searchValue);
		Stack<EntClaTiposangre> ObtenerPila(EntClaTiposangre.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntClaTiposangre> ObtenerPila(EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntClaTiposangre> ObtenerPila(EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntClaTiposangre obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntClaTiposangre obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntClaTiposangre obj, bool bValidar = true);
		bool Insert(EntClaTiposangre obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntClaTiposangre obj, bool bValidar = true);
		int Update(EntClaTiposangre obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntClaTiposangre obj, bool bValidar = true);
		int Delete(EntClaTiposangre obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntClaTiposangre obj);
		int InsertUpdate(EntClaTiposangre obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntClaTiposangre.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntClaTiposangre.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntClaTiposangre.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaTiposangre.Fields valueField, EntClaTiposangre.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntClaTiposangre.Fields valueField, EntClaTiposangre.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaTiposangre.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntClaTiposangre.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaTiposangre.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntClaTiposangre.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaTiposangre.Fields valueField, String textField, EntClaTiposangre.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntClaTiposangre.Fields valueField, String textField, EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaTiposangre.Fields valueField, EntClaTiposangre.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntClaTiposangre.Fields valueField, EntClaTiposangre.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntClaTiposangre.Fields valueField, EntClaTiposangre.Fields textField, EntClaTiposangre.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntClaTiposangre.Fields valueField, EntClaTiposangre.Fields textField, EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntClaTiposangre.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntClaTiposangre.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntClaTiposangre.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntClaTiposangre.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaTiposangre.Fields valueField, EntClaTiposangre.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntClaTiposangre.Fields valueField, EntClaTiposangre.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaTiposangre.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntClaTiposangre.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaTiposangre.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntClaTiposangre.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaTiposangre.Fields valueField, String textField, EntClaTiposangre.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntClaTiposangre.Fields valueField, String textField, EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaTiposangre.Fields valueField, EntClaTiposangre.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntClaTiposangre.Fields valueField, EntClaTiposangre.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntClaTiposangre.Fields valueField, EntClaTiposangre.Fields textField, EntClaTiposangre.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntClaTiposangre.Fields valueField, EntClaTiposangre.Fields textField, EntClaTiposangre.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntClaTiposangre.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntClaTiposangre.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntClaTiposangre.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntClaTiposangre.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntClaTiposangre.Fields refField);
		int FuncionesCount(EntClaTiposangre.Fields refField, EntClaTiposangre.Fields whereField, object valueField);
		int FuncionesCount(EntClaTiposangre.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntClaTiposangre.Fields refField);
		int FuncionesMin(EntClaTiposangre.Fields refField, EntClaTiposangre.Fields whereField, object valueField);
		int FuncionesMin(EntClaTiposangre.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntClaTiposangre.Fields refField);
		int FuncionesMax(EntClaTiposangre.Fields refField, EntClaTiposangre.Fields whereField, object valueField);
		int FuncionesMax(EntClaTiposangre.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntClaTiposangre.Fields refField);
		int FuncionesSum(EntClaTiposangre.Fields refField, EntClaTiposangre.Fields whereField, object valueField);
		int FuncionesSum(EntClaTiposangre.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntClaTiposangre.Fields refField);
		int FuncionesAvg(EntClaTiposangre.Fields refField, EntClaTiposangre.Fields whereField, object valueField);
		int FuncionesAvg(EntClaTiposangre.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

