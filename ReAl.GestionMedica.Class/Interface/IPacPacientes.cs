#region 
/***********************************************************************************************************
	NOMBRE:       IPacPacientes
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla pacpacientes

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IPacPacientes: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntPacPacientes.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntPacPacientes obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntPacPacientes obj, string strPropiedad);
		EntPacPacientes ObtenerObjetoInsertado(string strUsuCre);
		EntPacPacientes ObtenerObjeto(Int64 Int64idppa);
		EntPacPacientes ObtenerObjeto(Int64 Int64idppa, ref CTrans localTrans);
		EntPacPacientes ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntPacPacientes ObtenerObjeto(Hashtable htbFiltro);
		EntPacPacientes ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntPacPacientes ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntPacPacientes ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntPacPacientes ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntPacPacientes ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntPacPacientes ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntPacPacientes ObtenerObjeto(EntPacPacientes.Fields searchField, object searchValue);
		EntPacPacientes ObtenerObjeto(EntPacPacientes.Fields searchField, object searchValue, ref CTrans localTrans);
		EntPacPacientes ObtenerObjeto(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales);
		EntPacPacientes ObtenerObjeto(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntPacPacientes> ObtenerDiccionario();
		Dictionary<String, EntPacPacientes> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntPacPacientes> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntPacPacientes> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntPacPacientes> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntPacPacientes> ObtenerDiccionario(EntPacPacientes.Fields searchField, object searchValue);
		Dictionary<String, EntPacPacientes> ObtenerDiccionario(EntPacPacientes.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntPacPacientes> ObtenerDiccionario(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntPacPacientes> ObtenerDiccionario(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacPacientes> ObtenerLista();
		List<EntPacPacientes> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacPacientes> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacPacientes> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacPacientes> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacPacientes> ObtenerLista(EntPacPacientes.Fields searchField, object searchValue);
		List<EntPacPacientes> ObtenerLista(EntPacPacientes.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacPacientes> ObtenerLista(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacPacientes> ObtenerLista(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacPacientes> ObtenerLista(Hashtable htbFiltro);
		List<EntPacPacientes> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacPacientes> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacPacientes> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacPacientes> ObtenerListaDesdeVista(String strVista);
		List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, EntPacPacientes.Fields searchField, object searchValue);
		List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, EntPacPacientes.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacPacientes> ObtenerListaDesdeVista(String strVista, EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntPacPacientes> ObtenerCola();
		Queue<EntPacPacientes> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntPacPacientes> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntPacPacientes> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntPacPacientes> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntPacPacientes> ObtenerCola(EntPacPacientes.Fields searchField, object searchValue);
		Queue<EntPacPacientes> ObtenerCola(EntPacPacientes.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntPacPacientes> ObtenerCola(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntPacPacientes> ObtenerCola(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntPacPacientes> ObtenerPila();
		Stack<EntPacPacientes> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntPacPacientes> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntPacPacientes> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntPacPacientes> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntPacPacientes> ObtenerPila(EntPacPacientes.Fields searchField, object searchValue);
		Stack<EntPacPacientes> ObtenerPila(EntPacPacientes.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntPacPacientes> ObtenerPila(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntPacPacientes> ObtenerPila(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacPacientes obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacPacientes obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntPacPacientes obj, bool bValidar = true);
		bool Insert(EntPacPacientes obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntPacPacientes obj, bool bValidar = true);
		int Update(EntPacPacientes obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntPacPacientes obj, bool bValidar = true);
		int Delete(EntPacPacientes obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntPacPacientes obj);
		int InsertUpdate(EntPacPacientes obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntPacPacientes.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacPacientes.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacPacientes.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, String textField, EntPacPacientes.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, String textField, EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, EntPacPacientes.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntPacPacientes.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntPacPacientes.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacPacientes.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacPacientes.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, String textField, EntPacPacientes.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, String textField, EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, EntPacPacientes.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacPacientes.Fields valueField, EntPacPacientes.Fields textField, EntPacPacientes.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntPacPacientes.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntPacPacientes.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacPacientes.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacPacientes.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntPacPacientes.Fields refField);
		int FuncionesCount(EntPacPacientes.Fields refField, EntPacPacientes.Fields whereField, object valueField);
		int FuncionesCount(EntPacPacientes.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntPacPacientes.Fields refField);
		int FuncionesMin(EntPacPacientes.Fields refField, EntPacPacientes.Fields whereField, object valueField);
		int FuncionesMin(EntPacPacientes.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntPacPacientes.Fields refField);
		int FuncionesMax(EntPacPacientes.Fields refField, EntPacPacientes.Fields whereField, object valueField);
		int FuncionesMax(EntPacPacientes.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntPacPacientes.Fields refField);
		int FuncionesSum(EntPacPacientes.Fields refField, EntPacPacientes.Fields whereField, object valueField);
		int FuncionesSum(EntPacPacientes.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntPacPacientes.Fields refField);
		int FuncionesAvg(EntPacPacientes.Fields refField, EntPacPacientes.Fields whereField, object valueField);
		int FuncionesAvg(EntPacPacientes.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

