#region 
/***********************************************************************************************************
	NOMBRE:       IPacMultimedia
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla pacmultimedia

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        27/02/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IPacMultimedia: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntPacMultimedia.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntPacMultimedia obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntPacMultimedia obj, string strPropiedad);
		EntPacMultimedia ObtenerObjetoInsertado(string strUsuCre);
		EntPacMultimedia ObtenerObjeto(Int64 Int64idpmu);
		EntPacMultimedia ObtenerObjeto(Int64 Int64idpmu, ref CTrans localTrans);
		EntPacMultimedia ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntPacMultimedia ObtenerObjeto(Hashtable htbFiltro);
		EntPacMultimedia ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntPacMultimedia ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntPacMultimedia ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntPacMultimedia ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntPacMultimedia ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntPacMultimedia ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntPacMultimedia ObtenerObjeto(EntPacMultimedia.Fields searchField, object searchValue);
		EntPacMultimedia ObtenerObjeto(EntPacMultimedia.Fields searchField, object searchValue, ref CTrans localTrans);
		EntPacMultimedia ObtenerObjeto(EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales);
		EntPacMultimedia ObtenerObjeto(EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntPacMultimedia> ObtenerDiccionario();
		Dictionary<String, EntPacMultimedia> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntPacMultimedia> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntPacMultimedia> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntPacMultimedia> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntPacMultimedia> ObtenerDiccionario(EntPacMultimedia.Fields searchField, object searchValue);
		Dictionary<String, EntPacMultimedia> ObtenerDiccionario(EntPacMultimedia.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntPacMultimedia> ObtenerDiccionario(EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntPacMultimedia> ObtenerDiccionario(EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacMultimedia> ObtenerLista();
		List<EntPacMultimedia> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacMultimedia> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacMultimedia> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacMultimedia> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacMultimedia> ObtenerLista(EntPacMultimedia.Fields searchField, object searchValue);
		List<EntPacMultimedia> ObtenerLista(EntPacMultimedia.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacMultimedia> ObtenerLista(EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacMultimedia> ObtenerLista(EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacMultimedia> ObtenerLista(Hashtable htbFiltro);
		List<EntPacMultimedia> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacMultimedia> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacMultimedia> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacMultimedia> ObtenerListaDesdeVista(String strVista);
		List<EntPacMultimedia> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacMultimedia> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacMultimedia> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacMultimedia> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacMultimedia> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntPacMultimedia> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacMultimedia> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacMultimedia> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacMultimedia> ObtenerListaDesdeVista(String strVista, EntPacMultimedia.Fields searchField, object searchValue);
		List<EntPacMultimedia> ObtenerListaDesdeVista(String strVista, EntPacMultimedia.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacMultimedia> ObtenerListaDesdeVista(String strVista, EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacMultimedia> ObtenerListaDesdeVista(String strVista, EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntPacMultimedia> ObtenerCola();
		Queue<EntPacMultimedia> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntPacMultimedia> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntPacMultimedia> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntPacMultimedia> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntPacMultimedia> ObtenerCola(EntPacMultimedia.Fields searchField, object searchValue);
		Queue<EntPacMultimedia> ObtenerCola(EntPacMultimedia.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntPacMultimedia> ObtenerCola(EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntPacMultimedia> ObtenerCola(EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntPacMultimedia> ObtenerPila();
		Stack<EntPacMultimedia> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntPacMultimedia> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntPacMultimedia> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntPacMultimedia> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntPacMultimedia> ObtenerPila(EntPacMultimedia.Fields searchField, object searchValue);
		Stack<EntPacMultimedia> ObtenerPila(EntPacMultimedia.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntPacMultimedia> ObtenerPila(EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntPacMultimedia> ObtenerPila(EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacMultimedia obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacMultimedia obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntPacMultimedia obj, bool bValidar = true);
		bool Insert(EntPacMultimedia obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntPacMultimedia obj, bool bValidar = true);
		int Update(EntPacMultimedia obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntPacMultimedia obj, bool bValidar = true);
		int Delete(EntPacMultimedia obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntPacMultimedia obj);
		int InsertUpdate(EntPacMultimedia obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntPacMultimedia.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacMultimedia.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacMultimedia.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarDropDownList(ref DropDownList cmb);
		void CargarDropDownList(ref DropDownList cmb, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacMultimedia.Fields valueField, EntPacMultimedia.Fields textField);
		void CargarDropDownList(ref DropDownList cmb, EntPacMultimedia.Fields valueField, EntPacMultimedia.Fields textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacMultimedia.Fields valueField, String textField);
		void CargarDropDownList(ref DropDownList cmb, EntPacMultimedia.Fields valueField, String textField, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacMultimedia.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntPacMultimedia.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacMultimedia.Fields valueField, String textField, EntPacMultimedia.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntPacMultimedia.Fields valueField, String textField, EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacMultimedia.Fields valueField, EntPacMultimedia.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDropDownList(ref DropDownList cmb, EntPacMultimedia.Fields valueField, EntPacMultimedia.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarDropDownList(ref DropDownList cmb, EntPacMultimedia.Fields valueField, EntPacMultimedia.Fields textField, EntPacMultimedia.Fields searchField, object searchValue);
		void CargarDropDownList(ref DropDownList cmb, EntPacMultimedia.Fields valueField, EntPacMultimedia.Fields textField, EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarGridView(ref GridView dtg);
		void CargarGridView(ref GridView dtg, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, EntPacMultimedia.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, EntPacMultimedia.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarGridViewOr(ref GridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacMultimedia.Fields searchField, object searchValue);
		void CargarGridView(ref GridView dtg, ArrayList arrColumnas, EntPacMultimedia.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacMultimedia.Fields valueField, EntPacMultimedia.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntPacMultimedia.Fields valueField, EntPacMultimedia.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacMultimedia.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntPacMultimedia.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacMultimedia.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacMultimedia.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacMultimedia.Fields valueField, String textField, EntPacMultimedia.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacMultimedia.Fields valueField, String textField, EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacMultimedia.Fields valueField, EntPacMultimedia.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacMultimedia.Fields valueField, EntPacMultimedia.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacMultimedia.Fields valueField, EntPacMultimedia.Fields textField, EntPacMultimedia.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacMultimedia.Fields valueField, EntPacMultimedia.Fields textField, EntPacMultimedia.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntPacMultimedia.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntPacMultimedia.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacMultimedia.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacMultimedia.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntPacMultimedia.Fields refField);
		int FuncionesCount(EntPacMultimedia.Fields refField, EntPacMultimedia.Fields whereField, object valueField);
		int FuncionesCount(EntPacMultimedia.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntPacMultimedia.Fields refField);
		int FuncionesMin(EntPacMultimedia.Fields refField, EntPacMultimedia.Fields whereField, object valueField);
		int FuncionesMin(EntPacMultimedia.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntPacMultimedia.Fields refField);
		int FuncionesMax(EntPacMultimedia.Fields refField, EntPacMultimedia.Fields whereField, object valueField);
		int FuncionesMax(EntPacMultimedia.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntPacMultimedia.Fields refField);
		int FuncionesSum(EntPacMultimedia.Fields refField, EntPacMultimedia.Fields whereField, object valueField);
		int FuncionesSum(EntPacMultimedia.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntPacMultimedia.Fields refField);
		int FuncionesAvg(EntPacMultimedia.Fields refField, EntPacMultimedia.Fields whereField, object valueField);
		int FuncionesAvg(EntPacMultimedia.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

