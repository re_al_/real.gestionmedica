#region 
/***********************************************************************************************************
	NOMBRE:       IPacCirugias
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla paccirugias

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        16/07/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IPacCirugias: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntPacCirugias.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntPacCirugias obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntPacCirugias obj, string strPropiedad);
		EntPacCirugias ObtenerObjetoInsertado(string strUsuCre);
		EntPacCirugias ObtenerObjeto(Int64 Int64idpci);
		EntPacCirugias ObtenerObjeto(Int64 Int64idpci, ref CTrans localTrans);
		EntPacCirugias ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntPacCirugias ObtenerObjeto(Hashtable htbFiltro);
		EntPacCirugias ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntPacCirugias ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntPacCirugias ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntPacCirugias ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntPacCirugias ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntPacCirugias ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntPacCirugias ObtenerObjeto(EntPacCirugias.Fields searchField, object searchValue);
		EntPacCirugias ObtenerObjeto(EntPacCirugias.Fields searchField, object searchValue, ref CTrans localTrans);
		EntPacCirugias ObtenerObjeto(EntPacCirugias.Fields searchField, object searchValue, string strParamAdicionales);
		EntPacCirugias ObtenerObjeto(EntPacCirugias.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntPacCirugias> ObtenerDiccionario();
		Dictionary<String, EntPacCirugias> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntPacCirugias> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntPacCirugias> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntPacCirugias> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntPacCirugias> ObtenerDiccionario(EntPacCirugias.Fields searchField, object searchValue);
		Dictionary<String, EntPacCirugias> ObtenerDiccionario(EntPacCirugias.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntPacCirugias> ObtenerDiccionario(EntPacCirugias.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntPacCirugias> ObtenerDiccionario(EntPacCirugias.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacCirugias> ObtenerLista();
		List<EntPacCirugias> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacCirugias> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacCirugias> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacCirugias> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacCirugias> ObtenerLista(EntPacCirugias.Fields searchField, object searchValue);
		List<EntPacCirugias> ObtenerLista(EntPacCirugias.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacCirugias> ObtenerLista(EntPacCirugias.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacCirugias> ObtenerLista(EntPacCirugias.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacCirugias> ObtenerLista(Hashtable htbFiltro);
		List<EntPacCirugias> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacCirugias> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacCirugias> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacCirugias> ObtenerListaDesdeVista(String strVista);
		List<EntPacCirugias> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacCirugias> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacCirugias> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacCirugias> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacCirugias> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntPacCirugias> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacCirugias> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacCirugias> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacCirugias> ObtenerListaDesdeVista(String strVista, EntPacCirugias.Fields searchField, object searchValue);
		List<EntPacCirugias> ObtenerListaDesdeVista(String strVista, EntPacCirugias.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacCirugias> ObtenerListaDesdeVista(String strVista, EntPacCirugias.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacCirugias> ObtenerListaDesdeVista(String strVista, EntPacCirugias.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntPacCirugias> ObtenerCola();
		Queue<EntPacCirugias> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntPacCirugias> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntPacCirugias> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntPacCirugias> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntPacCirugias> ObtenerCola(EntPacCirugias.Fields searchField, object searchValue);
		Queue<EntPacCirugias> ObtenerCola(EntPacCirugias.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntPacCirugias> ObtenerCola(EntPacCirugias.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntPacCirugias> ObtenerCola(EntPacCirugias.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntPacCirugias> ObtenerPila();
		Stack<EntPacCirugias> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntPacCirugias> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntPacCirugias> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntPacCirugias> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntPacCirugias> ObtenerPila(EntPacCirugias.Fields searchField, object searchValue);
		Stack<EntPacCirugias> ObtenerPila(EntPacCirugias.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntPacCirugias> ObtenerPila(EntPacCirugias.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntPacCirugias> ObtenerPila(EntPacCirugias.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacCirugias obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacCirugias obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntPacCirugias obj, bool bValidar = true);
		bool Insert(EntPacCirugias obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntPacCirugias obj, bool bValidar = true);
		int Update(EntPacCirugias obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntPacCirugias obj, bool bValidar = true);
		int Delete(EntPacCirugias obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntPacCirugias obj);
		int InsertUpdate(EntPacCirugias obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntPacCirugias.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntPacCirugias.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacCirugias.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacCirugias.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacCirugias.Fields valueField, EntPacCirugias.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntPacCirugias.Fields valueField, EntPacCirugias.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacCirugias.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntPacCirugias.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacCirugias.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacCirugias.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacCirugias.Fields valueField, String textField, EntPacCirugias.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacCirugias.Fields valueField, String textField, EntPacCirugias.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacCirugias.Fields valueField, EntPacCirugias.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacCirugias.Fields valueField, EntPacCirugias.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacCirugias.Fields valueField, EntPacCirugias.Fields textField, EntPacCirugias.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacCirugias.Fields valueField, EntPacCirugias.Fields textField, EntPacCirugias.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntPacCirugias.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntPacCirugias.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacCirugias.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacCirugias.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntPacCirugias.Fields refField);
		int FuncionesCount(EntPacCirugias.Fields refField, EntPacCirugias.Fields whereField, object valueField);
		int FuncionesCount(EntPacCirugias.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntPacCirugias.Fields refField);
		int FuncionesMin(EntPacCirugias.Fields refField, EntPacCirugias.Fields whereField, object valueField);
		int FuncionesMin(EntPacCirugias.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntPacCirugias.Fields refField);
		int FuncionesMax(EntPacCirugias.Fields refField, EntPacCirugias.Fields whereField, object valueField);
		int FuncionesMax(EntPacCirugias.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntPacCirugias.Fields refField);
		int FuncionesSum(EntPacCirugias.Fields refField, EntPacCirugias.Fields whereField, object valueField);
		int FuncionesSum(EntPacCirugias.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntPacCirugias.Fields refField);
		int FuncionesAvg(EntPacCirugias.Fields refField, EntPacCirugias.Fields whereField, object valueField);
		int FuncionesAvg(EntPacCirugias.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

