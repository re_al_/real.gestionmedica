#region 
/***********************************************************************************************************
	NOMBRE:       IPacRecetas
	DESCRIPCION:
		Clase que define los metodos y operaciones sobre la Tabla pacrecetas

	REVISIONES:
		Ver        FECHA       Autor            Descripcion 
		---------  ----------  ---------------  ------------------------------------
		1.0        16/07/2021  R Alonzo Vera A  Creacion 

*************************************************************************************************************/
#endregion



#region
using System;
using System.Globalization;
using System.Threading;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using ReAl.GestionMedica.Class; 
using ReAl.GestionMedica.PgConn; 
using ReAl.GestionMedica.Class.Entidades;
using System.Windows.Forms;
using System.Web.UI.WebControls;
#endregion

namespace ReAl.GestionMedica.Class.Interface
{
	public interface IPacRecetas: IDisposable
	{
		string GetTableScript();
		dynamic GetColumnType(object valor,EntPacRecetas.Fields myField);
		dynamic GetColumnType(object valor, string strField);
		void SetDato(ref EntPacRecetas obj, string strPropiedad, dynamic dynValor);
		dynamic GetDato(ref EntPacRecetas obj, string strPropiedad);
		EntPacRecetas ObtenerObjetoInsertado(string strUsuCre);
		EntPacRecetas ObtenerObjeto(Int64 Int64idpre);
		EntPacRecetas ObtenerObjeto(Int64 Int64idpre, ref CTrans localTrans);
		EntPacRecetas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		EntPacRecetas ObtenerObjeto(Hashtable htbFiltro);
		EntPacRecetas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		EntPacRecetas ObtenerObjeto(Hashtable htbFiltro, ref CTrans localTrans);
		EntPacRecetas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		EntPacRecetas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales);
		EntPacRecetas ObtenerObjeto(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		EntPacRecetas ObtenerObjeto(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		EntPacRecetas ObtenerObjeto(EntPacRecetas.Fields searchField, object searchValue);
		EntPacRecetas ObtenerObjeto(EntPacRecetas.Fields searchField, object searchValue, ref CTrans localTrans);
		EntPacRecetas ObtenerObjeto(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales);
		EntPacRecetas ObtenerObjeto(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Dictionary<String, EntPacRecetas> ObtenerDiccionario();
		Dictionary<String, EntPacRecetas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Dictionary<String, EntPacRecetas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Dictionary<String, EntPacRecetas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Dictionary<String, EntPacRecetas> ObtenerDiccionario(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Dictionary<String, EntPacRecetas> ObtenerDiccionario(EntPacRecetas.Fields searchField, object searchValue);
		Dictionary<String, EntPacRecetas> ObtenerDiccionario(EntPacRecetas.Fields searchField, object searchValue, ref CTrans localTrans);
		Dictionary<String, EntPacRecetas> ObtenerDiccionario(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales);
		Dictionary<String, EntPacRecetas> ObtenerDiccionario(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacRecetas> ObtenerLista();
		List<EntPacRecetas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacRecetas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacRecetas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacRecetas> ObtenerLista(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacRecetas> ObtenerLista(EntPacRecetas.Fields searchField, object searchValue);
		List<EntPacRecetas> ObtenerLista(EntPacRecetas.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacRecetas> ObtenerLista(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacRecetas> ObtenerLista(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacRecetas> ObtenerLista(Hashtable htbFiltro);
		List<EntPacRecetas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacRecetas> ObtenerLista(Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacRecetas> ObtenerLista(Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		
		List<EntPacRecetas> ObtenerListaDesdeVista(String strVista);
		List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro);
		List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, ref CTrans localTrans);
		List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales);
		List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, Hashtable htbFiltro, string strParamAdicionales, ref CTrans localTrans);
		List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, EntPacRecetas.Fields searchField, object searchValue);
		List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, EntPacRecetas.Fields searchField, object searchValue, ref CTrans localTrans);
		List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales);
		List<EntPacRecetas> ObtenerListaDesdeVista(String strVista, EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Queue<EntPacRecetas> ObtenerCola();
		Queue<EntPacRecetas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Queue<EntPacRecetas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Queue<EntPacRecetas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Queue<EntPacRecetas> ObtenerCola(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Queue<EntPacRecetas> ObtenerCola(EntPacRecetas.Fields searchField, object searchValue);
		Queue<EntPacRecetas> ObtenerCola(EntPacRecetas.Fields searchField, object searchValue, ref CTrans localTrans);
		Queue<EntPacRecetas> ObtenerCola(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales);
		Queue<EntPacRecetas> ObtenerCola(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		
		Stack<EntPacRecetas> ObtenerPila();
		Stack<EntPacRecetas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		Stack<EntPacRecetas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, ref CTrans localTrans);
		Stack<EntPacRecetas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		Stack<EntPacRecetas> ObtenerPila(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales, ref CTrans localTrans);
		Stack<EntPacRecetas> ObtenerPila(EntPacRecetas.Fields searchField, object searchValue);
		Stack<EntPacRecetas> ObtenerPila(EntPacRecetas.Fields searchField, object searchValue, ref CTrans localTrans);
		Stack<EntPacRecetas> ObtenerPila(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales);
		Stack<EntPacRecetas> ObtenerPila(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales, ref CTrans localTrans);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacRecetas obj);
		int EjecutarSpDesdeObjeto(string strNombreSp, EntPacRecetas obj, ref CTrans localTrans);
		
		string CreatePk(string[] args);
		
		bool Insert(EntPacRecetas obj, bool bValidar = true);
		bool Insert(EntPacRecetas obj, ref CTrans localTrans, bool bValidar = true);
		int Update(EntPacRecetas obj, bool bValidar = true);
		int Update(EntPacRecetas obj, ref CTrans localTrans, bool bValidar = true);
		int Delete(EntPacRecetas obj, bool bValidar = true);
		int Delete(EntPacRecetas obj, ref CTrans localTrans, bool bValidar = true);
		int InsertUpdate(EntPacRecetas obj);
		int InsertUpdate(EntPacRecetas obj, ref CTrans localTrans);
		
		DataTable NuevoDataTable();
		DataTable NuevoDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable();
		DataTable ObtenerDataTable(String condicionesWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas);
		DataTable ObtenerDataTable(ArrayList arrColumnas, String strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTable(Hashtable htbFiltro);
		DataTable ObtenerDataTable(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		DataTable ObtenerDataTable(Hashtable htbFiltro, string strParametrosAdicionales);
		DataTable ObtenerDataTable(EntPacRecetas.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacRecetas.Fields searchField, object searchValue);
		DataTable ObtenerDataTable(ArrayList arrColumnas, EntPacRecetas.Fields searchField, object searchValue, string strParametrosAdicionales);
		DataTable ObtenerDataTableOr(ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		DataTable ObtenerDataTableOr(ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		
		void CargarComboBox(ref ComboBox cmb);
		void CargarComboBox(ref ComboBox cmb, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField);
		void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, String textField);
		void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, String textField, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, String textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, String textField, EntPacRecetas.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, String textField, EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParamAdicionales);
		void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField, EntPacRecetas.Fields searchField, object searchValue);
		void CargarComboBox(ref ComboBox cmb, EntPacRecetas.Fields valueField, EntPacRecetas.Fields textField, EntPacRecetas.Fields searchField, object searchValue, string strParamAdicionales);
		
		void CargarDataGrid(ref DataGridView dtg);
		void CargarDataGrid(ref DataGridView dtg, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, String condicionesWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, EntPacRecetas.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, EntPacRecetas.Fields searchField, object searchValue, string strParametrosAdicionales);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		void CargarDataGridOr(ref DataGridView dtg, ArrayList arrColumnas, ArrayList arrColumnasWhere, ArrayList arrValoresWhere, string strParametrosAdicionales);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacRecetas.Fields searchField, object searchValue);
		void CargarDataGrid(ref DataGridView dtg, ArrayList arrColumnas, EntPacRecetas.Fields searchField, object searchValue, string strParametrosAdicionales);
		
		int FuncionesCount(EntPacRecetas.Fields refField);
		int FuncionesCount(EntPacRecetas.Fields refField, EntPacRecetas.Fields whereField, object valueField);
		int FuncionesCount(EntPacRecetas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMin(EntPacRecetas.Fields refField);
		int FuncionesMin(EntPacRecetas.Fields refField, EntPacRecetas.Fields whereField, object valueField);
		int FuncionesMin(EntPacRecetas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesMax(EntPacRecetas.Fields refField);
		int FuncionesMax(EntPacRecetas.Fields refField, EntPacRecetas.Fields whereField, object valueField);
		int FuncionesMax(EntPacRecetas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesSum(EntPacRecetas.Fields refField);
		int FuncionesSum(EntPacRecetas.Fields refField, EntPacRecetas.Fields whereField, object valueField);
		int FuncionesSum(EntPacRecetas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
		int FuncionesAvg(EntPacRecetas.Fields refField);
		int FuncionesAvg(EntPacRecetas.Fields refField, EntPacRecetas.Fields whereField, object valueField);
		int FuncionesAvg(EntPacRecetas.Fields refField, ArrayList arrColumnasWhere, ArrayList arrValoresWhere);
	}
}

