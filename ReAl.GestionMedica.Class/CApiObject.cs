namespace ReAl.GestionMedica.Class
{
    public class CApiObject
    {
        public string Nombre { get; internal set; }
        public CBaseClass Datos { get; internal set; }
    }
}