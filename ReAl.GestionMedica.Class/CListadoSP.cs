namespace ReAl.GestionMedica.Class
{
    public class CListadoSP
    {
        public enum GasCategoria
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla GasCategoria
            /// </summary>
            SpGcaIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla GasCategoria
            /// </summary>
            SpGcaUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla GasCategoria
            /// </summary>
            SpGcaDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla GasCategoria
            /// </summary>
            spGcaInsUpd
        }

        public enum GasLibro
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla GasLibro
            /// </summary>
            SpGliIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla GasLibro
            /// </summary>
            SpGliUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla GasLibro
            /// </summary>
            SpGliDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla GasLibro
            /// </summary>
            spGliInsUpd
        }

        public enum ClaEstadocivil
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla ClaEstadocivil
            /// </summary>
            SpCecIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla ClaEstadocivil
            /// </summary>
            SpCecUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla ClaEstadocivil
            /// </summary>
            SpCecDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla ClaEstadocivil
            /// </summary>
            spCecInsUpd
        }

        public enum ClaPlantillas
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla ClaPlantillas
            /// </summary>
            SpCplIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla ClaPlantillas
            /// </summary>
            SpCplUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla ClaPlantillas
            /// </summary>
            SpCplDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla ClaPlantillas
            /// </summary>
            spCplInsUpd
        }

        public enum ClaPlantillasdet
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla ClaPlantillasdet
            /// </summary>
            SpCpdIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla ClaPlantillasdet
            /// </summary>
            SpCpdUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla ClaPlantillasdet
            /// </summary>
            SpCpdDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla ClaPlantillasdet
            /// </summary>
            spCpdInsUpd
        }

        public enum ClaTipoplantillas
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla ClaTipoplantillas
            /// </summary>
            SpCtpIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla ClaTipoplantillas
            /// </summary>
            SpCtpUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla ClaTipoplantillas
            /// </summary>
            SpCtpDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla ClaTipoplantillas
            /// </summary>
            spCtpInsUpd
        }

        public enum ClaTiposangre
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla ClaTiposangre
            /// </summary>
            SpCtsIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla ClaTiposangre
            /// </summary>
            SpCtsUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla ClaTiposangre
            /// </summary>
            SpCtsDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla ClaTiposangre
            /// </summary>
            spCtsInsUpd
        }

        public enum SegAplicaciones
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegAplicaciones
            /// </summary>
            SpSapIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegAplicaciones
            /// </summary>
            SpSapUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegAplicaciones
            /// </summary>
            SpSapDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegAplicaciones
            /// </summary>
            spSapInsUpd
        }

        public enum SegEstados
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegEstados
            /// </summary>
            SpSesIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegEstados
            /// </summary>
            SpSesUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegEstados
            /// </summary>
            SpSesDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegEstados
            /// </summary>
            spSesInsUpd
        }

        public enum SegMensajeserror
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegMensajeserror
            /// </summary>
            SpSmeIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegMensajeserror
            /// </summary>
            SpSmeUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegMensajeserror
            /// </summary>
            SpSmeDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegMensajeserror
            /// </summary>
            spSmeInsUpd
        }

        public enum SegPaginas
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegPaginas
            /// </summary>
            SpSpgIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegPaginas
            /// </summary>
            SpSpgUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegPaginas
            /// </summary>
            SpSpgDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegPaginas
            /// </summary>
            spSpgInsUpd
        }

        public enum SegParametros
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegParametros
            /// </summary>
            SpSpaIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegParametros
            /// </summary>
            SpSpaUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegParametros
            /// </summary>
            SpSpaDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegParametros
            /// </summary>
            spSpaInsUpd
        }

        public enum SegParametrosdet
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegParametrosdet
            /// </summary>
            SpSpdIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegParametrosdet
            /// </summary>
            SpSpdUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegParametrosdet
            /// </summary>
            SpSpdDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegParametrosdet
            /// </summary>
            spSpdInsUpd
        }

        public enum SegRoles
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegRoles
            /// </summary>
            SpSroIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegRoles
            /// </summary>
            SpSroUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegRoles
            /// </summary>
            SpSroDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegRoles
            /// </summary>
            spSroInsUpd
        }

        public enum SegRolespagina
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegRolespagina
            /// </summary>
            SpSrpIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegRolespagina
            /// </summary>
            SpSrpUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegRolespagina
            /// </summary>
            SpSrpDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegRolespagina
            /// </summary>
            spSrpInsUpd
        }

        public enum SegRolestablastransacciones
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegRolestablastransacciones
            /// </summary>
            SpSrtIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegRolestablastransacciones
            /// </summary>
            SpSrtUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegRolestablastransacciones
            /// </summary>
            SpSrtDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegRolestablastransacciones
            /// </summary>
            spSrtInsUpd
        }

        public enum SegTablas
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegTablas
            /// </summary>
            SpStaIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegTablas
            /// </summary>
            SpStaUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegTablas
            /// </summary>
            SpStaDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegTablas
            /// </summary>
            spStaInsUpd
        }

        public enum SegTransacciones
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegTransacciones
            /// </summary>
            SpStrIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegTransacciones
            /// </summary>
            SpStrUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegTransacciones
            /// </summary>
            SpStrDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegTransacciones
            /// </summary>
            spStrInsUpd
        }

        public enum SegTransiciones
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegTransiciones
            /// </summary>
            SpStsIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegTransiciones
            /// </summary>
            SpStsUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegTransiciones
            /// </summary>
            SpStsDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegTransiciones
            /// </summary>
            spStsInsUpd
        }

        public enum SegUsuarios
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegUsuarios
            /// </summary>
            SpSusIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegUsuarios
            /// </summary>
            SpSusUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegUsuarios
            /// </summary>
            SpSusDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegUsuarios
            /// </summary>
            spSusInsUpd
        }

        public enum SegUsuariosrestriccion
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla SegUsuariosrestriccion
            /// </summary>
            SpSurIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla SegUsuariosrestriccion
            /// </summary>
            SpSurUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla SegUsuariosrestriccion
            /// </summary>
            SpSurDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla SegUsuariosrestriccion
            /// </summary>
            spSurInsUpd
        }

        public enum PacPaciente
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla PacPaciente
            /// </summary>
            SpPacPacienteIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla PacPaciente
            /// </summary>
            SpPacPacienteUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla PacPaciente
            /// </summary>
            SpPacPacienteDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla PacPaciente
            /// </summary>
            spPacPacienteInsUpd
        }

        public enum PacPacientes
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla PacPacientes
            /// </summary>
            SpPpaIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla PacPacientes
            /// </summary>
            SpPpaUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla PacPacientes
            /// </summary>
            SpPpaDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla PacPacientes
            /// </summary>
            spPpaInsUpd
        }

        public enum PacCirugias
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla PacCirujias
            /// </summary>
            SpPciIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla PacCirujias
            /// </summary>
            SpPciUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla PacCirujias
            /// </summary>
            SpPciDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla PacCirujias
            /// </summary>
            spPciInsUpd
        }

        public enum PacInformes
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla PacInformes
            /// </summary>
            SpPinIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla PacInformes
            /// </summary>
            SpPinUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla PacInformes
            /// </summary>
            SpPinDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla PacInformes
            /// </summary>
            spPinInsUpd
        }

        public enum PacRecetas
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla PacRecetas
            /// </summary>
            SpPreIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla PacRecetas
            /// </summary>
            SpPreUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla PacRecetas
            /// </summary>
            SpPreDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla PacRecetas
            /// </summary>
            spPreInsUpd
        }

        public enum PacConsultas
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla PacConsultas
            /// </summary>
            SpPcoIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla PacConsultas
            /// </summary>
            SpPcoUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla PacConsultas
            /// </summary>
            SpPcoDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla PacConsultas
            /// </summary>
            spPcoInsUpd
        }

        public enum PacCertificados
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla PacCeretificados
            /// </summary>
            SpPcmIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla PacCeretificados
            /// </summary>
            SpPcmUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla PacCeretificados
            /// </summary>
            SpPcmDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla PacCeretificados
            /// </summary>
            spPcmInsUpd
        }

        public enum PacHistoriaclinica1
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla PacHistoriaclinica1
            /// </summary>
            SpPh1Ins,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla PacHistoriaclinica1
            /// </summary>
            SpPh1Upd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla PacHistoriaclinica1
            /// </summary>
            SpPh1Del,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla PacHistoriaclinica1
            /// </summary>
            spPh1InsUpd
        }

        public enum PacHistoriaclinica2
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla PacHistoriaclinica2
            /// </summary>
            SpPh2Ins,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla PacHistoriaclinica2
            /// </summary>
            SpPh2Upd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla PacHistoriaclinica2
            /// </summary>
            SpPh2Del,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla PacHistoriaclinica2
            /// </summary>
            spPh2InsUpd
        }

        public enum PacHistoriaclinica3
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla PacHistoriaclinica3
            /// </summary>
            SpPh3Ins,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla PacHistoriaclinica3
            /// </summary>
            SpPh3Upd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla PacHistoriaclinica3
            /// </summary>
            SpPh3Del,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla PacHistoriaclinica3
            /// </summary>
            spPh3InsUpd
        }

        public enum PacMultimedia
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla PacMultimedia
            /// </summary>
            SpPmuIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla PacMultimedia
            /// </summary>
            SpPmuUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla PacMultimedia
            /// </summary>
            SpPmuDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla PacMultimedia
            /// </summary>
            spPmuInsUpd
        }

        public enum CalCitas
        {
            /// <summary>
            ///     Proc para Insertar datos en la tabla CalCitas
            /// </summary>
            SpCciIns,

            /// <summary>
            ///     Proc para Actualizar datos en la tabla CalCitas
            /// </summary>
            SpCciUpd,

            /// <summary>
            ///     Proc para Eliminar datos en la tabla CalCitas
            /// </summary>
            SpCciDel,

            /// <summary>
            ///     Proc para Insertar y/o Actualizar datos en la tabla CalCitas
            /// </summary>
            SpCciInsUpd
        }
    }
}