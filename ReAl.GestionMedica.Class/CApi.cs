namespace ReAl.GestionMedica.Class
{
    public static class CApi
    {
        public enum Estado
        {
            ELABORADO,
            EJECUTADO,
            PROGRAMADO
        }

        public enum Transaccion
        {
            DESHABILITAR,
            MODIFICAR,
            CREAR,
            EJECUTAR,
            PROGRAMAR
        }
    }
}