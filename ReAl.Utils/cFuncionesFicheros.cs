﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace ReAl.Utils
{
    public static class cFuncionesFicheros
    {
        /// <summary>
        /// Crear un directorio
        /// </summary>
        /// <param name="pathDirectorio"></param>
        /// <returns></returns>
        public static bool CrearDirectorio(string pathDirectorio)
        {
            try
            {
                var directorio = new DirectoryInfo(pathDirectorio);
                if (!directorio.Exists)
                {
                    directorio.Create();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Borrar Un directorio
        /// </summary>
        /// <param name="pathDirectorio"></param>
        /// <returns></returns>
        public static bool BorrarDirectorio(string pathDirectorio)
        {
            try
            {
                var directorio = new DirectoryInfo(pathDirectorio);
                if (directorio.Exists)
                {
                    directorio.Delete();
                    return true;
                }
                else
                    return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Borrar un archivo fisico
        /// </summary>
        /// <param name="pathDirectorio"></param>
        /// <param name="pathNombreArchivo"></param>
        /// <returns></returns>
        public static bool BorrarArchivo(string pathDirectorio, string pathNombreArchivo)
        {
            if (File.Exists(pathDirectorio + "\\" + pathNombreArchivo))
            {
                File.Delete(pathDirectorio + "\\" + pathNombreArchivo);
                return true;
            }
            else
                return false;
        }

        /// <summary>
        /// Borrar archivos fisicos de una carpeta
        /// </summary>
        /// <param name="pathDirectorio"></param>
        /// <param name="pathNombreArchivo"></param>
        /// <returns></returns>
        public static void BorrarArchivos(string pathDirectorio, string tipoArchivo)
        {
            var dir = new DirectoryInfo(pathDirectorio);
            var file = dir.GetFiles(tipoArchivo);
            var inc = 0;
            while (inc < file.Length)
            {
                if (File.Exists(pathDirectorio + "\\" + file.GetValue(inc).ToString()))
                {
                    File.Delete(pathDirectorio + "\\" + file.GetValue(inc).ToString());
                    inc++;
                }
            }
        }

        /// <summary>
        /// Buscar el path de un directorio
        /// </summary>
        /// <returns></returns>
        public static string BuscarDirectorio()
        {
            var dialogoRuta = new FolderBrowserDialog();
            return (dialogoRuta.ShowDialog() == DialogResult.OK)
                ? dialogoRuta.SelectedPath : null;
        }

        public static string BuscarArchivo(string pathDirectorioInicio, string criterioFiltrado)
        {
            //Stream myStream = null;
            var pathArchivo = "";
            var openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = pathDirectorioInicio;
            openFileDialog1.Filter = criterioFiltrado;
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    pathArchivo = openFileDialog1.FileName;

                    //if ((myStream = openFileDialog1.OpenFile()) != null)
                    //{
                    //    using (myStream)
                    //    {
                    //        // Insert code to read the stream here.
                    //    }
                    //}
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: No se puede leer el archivo. " + ex.Message);
                    pathArchivo = null;
                }
            }
            return pathArchivo;
        }

        /// <summary>
        /// Busca archivos desde un path hallando las coincidencias en una lista
        /// </summary>
        /// <param name="pathDirectorio"></param>
        /// <param name="pathNombreArchivo"></param>
        /// <returns></returns>
        public static List<string> GenerarListaArchivos(string pathDirectorio, string patronFiltrado)
        {
            var listaResultado = new List<string>();
            var pilaCoincidencia = new Stack<string>();

            pilaCoincidencia.Push(pathDirectorio);

            while (pilaCoincidencia.Count > 0)
            {
                var directorio = pilaCoincidencia.Pop();
                try
                {
                    listaResultado.AddRange(Directory.GetFiles(directorio));

                    foreach (var nombreArchivo in Directory.GetDirectories(directorio))
                    {
                        pilaCoincidencia.Push(nombreArchivo);
                    }
                }
                catch
                {
                }
            }
            return listaResultado;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="nombreArchivo"></param>
        public static void CrearArchivo(string nombreArchivo)
        {
            var writer = File.CreateText(nombreArchivo);
            writer.Close();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="nombreArchivo"></param>
        /// <param name="texto"></param>
        public static void EscribirDentroArchivo(string nombreArchivo, string texto)
        {
            try
            {
                // esto inserta texto en un archivo existente, si el archivo no existe lo crea
                var writer = File.AppendText(nombreArchivo);
                writer.WriteLine(texto);
                writer.Close();
            }
            catch
            {
                Console.WriteLine("Error");
            }
        }

        /// <summary>
        ///     Funcion que realiza la copia recursiva de un directorio Origen a un path de destino
        /// </summary>
        /// <param name="sourceFolder" type="string">
        ///     <para>
        ///         Directorio Origen
        ///     </para>
        /// </param>
        /// <param name="destFolder" type="string">
        ///     <para>
        ///         Directorio Destino
        ///     </para>
        /// </param>
        public static void CopiarDirectorioRecursivo(string sourceFolder, string destFolder)
        {
            if (!Directory.Exists(destFolder))
                Directory.CreateDirectory(destFolder);

            var files = Directory.GetFiles(sourceFolder);

            foreach (var file in files)
            {
                var name = Path.GetFileName(file);
                var dest = Path.Combine(destFolder, name);
                File.Copy(file, dest);
            }
            var folders = Directory.GetDirectories(sourceFolder);

            foreach (var folder in folders)
            {
                var name = Path.GetFileName(folder);
                var dest = Path.Combine(destFolder, name);
                CopiarDirectorioRecursivo(folder, dest);
            }
        }

        /// <summary>
        ///     Funcion que abre un OpenFileDialog para poder seleccionar un archivo imagen
        /// </summary>
        /// <returns>
        ///     Cadena que representa la dirección fisica del archivo cargado
        /// </returns>
        public static string AbrirArchivoImagen()
        {
            try
            {
                var filePath = "";
                Stream myStream;
                var openFileDialog1 = new OpenFileDialog();

                openFileDialog1.InitialDirectory = Environment.SpecialFolder.DesktopDirectory.ToString();

                openFileDialog1.Filter = "Image files (*.jpg)|*.jpg|All image files (*.*)|*.*";
                openFileDialog1.FilterIndex = 1;
                openFileDialog1.RestoreDirectory = true;

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    myStream = openFileDialog1.OpenFile();

                    filePath = openFileDialog1.FileName;
                    myStream.Close();
                }

                return filePath;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return "";
            }
        }

        /// <summary>
        ///     Funcion que crea un archivo de texto plano
        /// </summary>
        /// <param name="filePath" type="string">
        ///     <para>
        ///         Path y nombre del archivo
        ///     </para>
        /// </param>
        /// <param name="fileData" type="string">
        ///     <para>
        ///         Contenido del archivo
        ///     </para>
        /// </param>
        public static void GuardarArchivo(string filePath, string fileData)
        {
            var writer = new StreamWriter(filePath);
            writer.Write(fileData);
            writer.Close();
        }
    }
}