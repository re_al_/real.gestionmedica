﻿using System;
using System.Collections;
using System.Net.Mail;
using System.Text;

namespace ReAl.Utils
{
    public class cFuncionesMail
    {
        private static void SendMailMessage(MailMessage mailMessage)
        {
            var mailHost = cParametrosUtils.MailHost;
            var smtpClient = new SmtpClient(mailHost, 25);
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.PickupDirectoryFromIis;
            smtpClient.Send(mailMessage);
        }

        public static void SendMeetingRequest(DateTime startTime, string mailDestino, string mailAsunto, string mailMsj)
        {
            try
            {
                startTime = startTime.AddHours(4);
                var sc = new SmtpClient(cParametrosUtils.MailHost);

                var msg = new MailMessage();
                msg.From = new MailAddress(cParametrosUtils.MailFrom, "Sistema");
                msg.To.Add(new MailAddress(mailDestino, mailDestino));
                msg.Subject = mailAsunto;
                msg.Body = mailMsj;

                var str = new StringBuilder();
                str.AppendLine("BEGIN:VCALENDAR");
                str.AppendLine("PRODID:-//Ahmed Abu Dagga Blog");
                str.AppendLine("VERSION:2.0");
                str.AppendLine("METHOD:REQUEST");
                str.AppendLine("BEGIN:VEVENT");
                str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", startTime));
                str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", startTime.AddMinutes(5)));
                str.AppendLine("LOCATION: Autoridad de Fiscalización y Control Social de Pensiones y Seguros (APS)");
                str.AppendLine(string.Format("UID:{0}", Guid.NewGuid()));
                str.AppendLine(string.Format("DESCRIPTION:{0}", msg.Body));
                str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", msg.Body));
                str.AppendLine(string.Format("SUMMARY:{0}", msg.Subject));
                str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", msg.From.Address));

                str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", msg.To[0].DisplayName, msg.To[0].Address));

                str.AppendLine("BEGIN:VALARM");
                str.AppendLine("TRIGGER:-PT15M");
                str.AppendLine("ACTION:DISPLAY");
                str.AppendLine("DESCRIPTION:Reminder");
                str.AppendLine("END:VALARM");
                str.AppendLine("END:VEVENT");
                str.AppendLine("END:VCALENDAR");
                var ct = new System.Net.Mime.ContentType("text/calendar");
                ct.Parameters.Add("method", "REQUEST");
                var avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), ct);
                msg.AlternateViews.Add(avCal);

                sc.Port = cParametrosUtils.MailPort;
                sc.Credentials = new System.Net.NetworkCredential(cParametrosUtils.MailUsername, cParametrosUtils.MailPassword);
                sc.Send(msg);
            }
            catch (Exception exp)
            {
                if (cParametrosUtils.MailNotificarError)
                    throw exp;
            }
        }

        public static void SendMeetingRequest(DateTime startTime, DateTime endTime, string mailDestino, string mailAsunto, string mailMsj)
        {
            try
            {
                startTime = startTime.AddHours(4);
                endTime = endTime.AddHours(4);
                var sc = new SmtpClient(cParametrosUtils.MailHost);

                var msg = new MailMessage();
                msg.From = new MailAddress(cParametrosUtils.MailFrom, "Sistema");
                msg.To.Add(new MailAddress(mailDestino, mailDestino));
                msg.Subject = mailAsunto;
                msg.Body = mailMsj;

                var str = new StringBuilder();
                str.AppendLine("BEGIN:VCALENDAR");
                str.AppendLine("PRODID:-//Ahmed Abu Dagga Blog");
                str.AppendLine("VERSION:2.0");
                str.AppendLine("METHOD:REQUEST");
                str.AppendLine("BEGIN:VEVENT");
                str.AppendLine(string.Format("DTSTART:{0:yyyyMMddTHHmmssZ}", startTime));
                str.AppendLine(string.Format("DTSTAMP:{0:yyyyMMddTHHmmssZ}", DateTime.UtcNow));
                str.AppendLine(string.Format("DTEND:{0:yyyyMMddTHHmmssZ}", endTime));
                str.AppendLine("LOCATION: Autoridad de Fiscalización y Control Social de Pensiones y Seguros (APS)");
                str.AppendLine(string.Format("UID:{0}", Guid.NewGuid()));
                str.AppendLine(string.Format("DESCRIPTION:{0}", msg.Body));
                str.AppendLine(string.Format("X-ALT-DESC;FMTTYPE=text/html:{0}", msg.Body));
                str.AppendLine(string.Format("SUMMARY:{0}", msg.Subject));
                str.AppendLine(string.Format("ORGANIZER:MAILTO:{0}", msg.From.Address));

                str.AppendLine(string.Format("ATTENDEE;CN=\"{0}\";RSVP=TRUE:mailto:{1}", msg.To[0].DisplayName, msg.To[0].Address));

                str.AppendLine("BEGIN:VALARM");
                str.AppendLine("TRIGGER:-PT15M");
                str.AppendLine("ACTION:DISPLAY");
                str.AppendLine("DESCRIPTION:Reminder");
                str.AppendLine("END:VALARM");
                str.AppendLine("END:VEVENT");
                str.AppendLine("END:VCALENDAR");
                var ct = new System.Net.Mime.ContentType("text/calendar");
                ct.Parameters.Add("method", "REQUEST");
                var avCal = AlternateView.CreateAlternateViewFromString(str.ToString(), ct);
                msg.AlternateViews.Add(avCal);

                sc.Port = cParametrosUtils.MailPort;
                sc.Credentials = new System.Net.NetworkCredential(cParametrosUtils.MailUsername, cParametrosUtils.MailPassword);
                sc.Send(msg);
            }
            catch (Exception exp)
            {
                if (cParametrosUtils.MailNotificarError)
                    throw exp;
            }
        }

        public static void SendMail(string mailDestino, string mailAsunto, string mailMsj, string strPathAdjunto)
        {
            try
            {
                var mail = new MailMessage();
                var smtpServer = new SmtpClient(cParametrosUtils.MailHost);

                mail.From = new MailAddress(cParametrosUtils.MailFrom);
                mail.To.Add(mailDestino);
                mail.Subject = mailAsunto;
                mail.Body = mailMsj;

                var attachment = new Attachment(strPathAdjunto);
                mail.Attachments.Add(attachment);

                smtpServer.Port = cParametrosUtils.MailPort;
                smtpServer.Credentials = new System.Net.NetworkCredential(cParametrosUtils.MailUsername, cParametrosUtils.MailPassword);
                //SmtpServer.EnableSsl = true;

                smtpServer.Send(mail);
            }
            catch (Exception exp)
            {
                if (cParametrosUtils.MailNotificarError)
                    throw exp;
            }
        }

        public static void SendMail(string mailDestino, string mailAsunto, string mailMsj)
        {
            try
            {
                var smtpServer = new SmtpClient(cParametrosUtils.MailHost, cParametrosUtils.MailPort)
                {
                    UseDefaultCredentials = true,
                    Credentials = new System.Net.NetworkCredential(cParametrosUtils.MailUsername, cParametrosUtils.MailPassword),
                    EnableSsl = true
                };
                var mail = new MailMessage();
                mail.From = new MailAddress(cParametrosUtils.MailFrom, "NeuroDiagnostico");
                mail.To.Add(mailDestino);
                mail.Subject = mailAsunto;
                mail.Body = mailMsj;
                mail.IsBodyHtml = true;

                smtpServer.Send(mail);
            }
            catch (Exception exp)
            {
                //if (cParametrosUtils.mailNotificarError)
                throw exp;
            }
        }

        public static void SendMail(ArrayList mailDestino, string mailAsunto, string mailMsj)
        {
            try
            {
                var mail = new MailMessage();
                var smtpServer = new SmtpClient(cParametrosUtils.MailHost);

                mail.From = new MailAddress(cParametrosUtils.MailFrom);

                foreach (string destinatario in mailDestino)
                {
                    mail.To.Add(destinatario);
                }
                mail.Subject = mailAsunto;
                mail.Body = mailMsj;

                smtpServer.Port = cParametrosUtils.MailPort;
                smtpServer.Credentials = new System.Net.NetworkCredential(cParametrosUtils.MailUsername, cParametrosUtils.MailPassword);
                //smtpServer.EnableSsl = true;
                //smtpServer.Timeout = 10;

                smtpServer.Send(mail);
            }
            catch (Exception exp)
            {
                if (cParametrosUtils.MailNotificarError)
                    throw exp;
            }
        }
    }
}