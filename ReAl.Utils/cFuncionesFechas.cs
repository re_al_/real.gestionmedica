#region

/* ****************************************************** */
/* GENERADO POR: ReAl ClassGenerator
/* SISTEMA: AP
/* AUTOR: R. Alonzo Vera
/* FECHA: 04/10/2010  -  18:15
/* ****************************************************** */

#endregion

using System;
using System.Globalization;

namespace ReAl.Utils
{
    /// <summary>
    /// Common DateTime Methods.
    /// </summary>
    ///
    public enum quarter
    {
        first = 1,
        second = 2,
        third = 3,
        fourth = 4
    }

    public enum month
    {
        january = 1,
        february = 2,
        march = 3,
        april = 4,
        may = 5,
        june = 6,
        july = 7,
        august = 8,
        september = 9,
        october = 10,
        november = 11,
        december = 12
    }

    public class cFuncionesFechas
    {
        #region Quarter

        public static DateTime GetFirstDayOfWeek(DateTime dayInWeek)
        {
            var defaultCultureInfo = CultureInfo.CurrentCulture;

            return GetFirstDayOfWeek(dayInWeek, defaultCultureInfo);
        }

        public static DateTime GetFirstDayOfWeek(DateTime dayInWeek, CultureInfo cultureInfo)
        {
            var firstDay = cultureInfo.DateTimeFormat.FirstDayOfWeek;
            var firstDayInWeek = dayInWeek.Date;
            while (firstDayInWeek.DayOfWeek != firstDay)
                firstDayInWeek = firstDayInWeek.AddDays(-1);

            return firstDayInWeek;
        }

        #endregion

        #region Quarter

        public static DateTime GetStartOfQuarter(int year, quarter qtr)
        {
            if (qtr == quarter.first)   // 1st Quarter = January 1 to March 31
                return new DateTime(year, 1, 1, 0, 0, 0, 0);
            else if (qtr == quarter.second) // 2nd Quarter = April 1 to June 30
                return new DateTime(year, 4, 1, 0, 0, 0, 0);
            else if (qtr == quarter.third) // 3rd Quarter = July 1 to September 30
                return new DateTime(year, 7, 1, 0, 0, 0, 0);
            else // 4th Quarter = October 1 to December 31
                return new DateTime(year, 10, 1, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfQuarter(int year, quarter qtr)
        {
            if (qtr == quarter.first)   // 1st Quarter = January 1 to March 31
                return new DateTime(year, 3, DateTime.DaysInMonth(year, 3), 23, 59, 59, 999);
            else if (qtr == quarter.second) // 2nd Quarter = April 1 to June 30
                return new DateTime(year, 6, DateTime.DaysInMonth(year, 6), 23, 59, 59, 999);
            else if (qtr == quarter.third) // 3rd Quarter = July 1 to September 30
                return new DateTime(year, 9, DateTime.DaysInMonth(year, 9), 23, 59, 59, 999);
            else // 4th Quarter = October 1 to December 31
                return new DateTime(year, 12, DateTime.DaysInMonth(year, 12), 23, 59, 59, 999);
        }

        public static quarter GetQuarter(month month)
        {
            if (month <= month.march)   // 1st Quarter = January 1 to March 31
                return quarter.first;
            else if ((month >= month.april) && (month <= month.june)) // 2nd Quarter = April 1 to June 30
                return quarter.second;
            else if ((month >= month.july) && (month <= month.september)) // 3rd Quarter = July 1 to September 30
                return quarter.third;
            else // 4th Quarter = October 1 to December 31
                return quarter.fourth;
        }

        public static DateTime GetEndOfLastQuarter()
        {
            if (DateTime.Now.Month <= (int)month.march) //go to last quarter of previous year
                return GetEndOfQuarter(DateTime.Now.Year - 1, GetQuarter(month.december));
            else //return last quarter of current year
                return GetEndOfQuarter(DateTime.Now.Year, GetQuarter((month)DateTime.Now.Month));
        }

        public static DateTime GetStartOfLastQuarter()
        {
            if (DateTime.Now.Month <= 3) //go to last quarter of previous year
                return GetStartOfQuarter(DateTime.Now.Year - 1, GetQuarter(month.december));
            else //return last quarter of current year
                return GetStartOfQuarter(DateTime.Now.Year, GetQuarter((month)DateTime.Now.Month));
        }

        public static DateTime GetStartOfCurrentQuarter()
        {
            return GetStartOfQuarter(DateTime.Now.Year, GetQuarter((month)DateTime.Now.Month));
        }

        public static DateTime GetEndOfCurrentQuarter()
        {
            return GetEndOfQuarter(DateTime.Now.Year, GetQuarter((month)DateTime.Now.Month));
        }

        #endregion

        #region Weeks

        public static DateTime GetStartOfLastWeek()
        {
            var daysToSubtract = (int)DateTime.Now.DayOfWeek + 7;
            var dt = DateTime.Now.Subtract(System.TimeSpan.FromDays(daysToSubtract));
            return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfLastWeek()
        {
            var dt = GetStartOfLastWeek().AddDays(6);
            return new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59, 999);
        }

        public static DateTime GetStartOfCurrentWeek()
        {
            var daysToSubtract = (int)DateTime.Now.DayOfWeek;
            var dt = DateTime.Now.Subtract(System.TimeSpan.FromDays(daysToSubtract));
            return new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfCurrentWeek()
        {
            var dt = GetStartOfCurrentWeek().AddDays(6);
            return new DateTime(dt.Year, dt.Month, dt.Day, 23, 59, 59, 999);
        }

        #endregion

        #region Months

        public static DateTime GetStartOfMonth(int month, int year)
        {
            return new DateTime(year, month, 1, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfMonth(int month, int year)
        {
            return new DateTime(year, month, DateTime.DaysInMonth(year, month), 23, 59, 59, 999);
        }

        public static DateTime GetStartOfLastMonth()
        {
            if (DateTime.Now.Month == 1)
                return GetStartOfMonth(12, DateTime.Now.Year - 1);
            else
                return GetStartOfMonth(DateTime.Now.Month - 1, DateTime.Now.Year);
        }

        public static DateTime GetEndOfLastMonth()
        {
            if (DateTime.Now.Month == 1)
                return GetEndOfMonth(12, DateTime.Now.Year - 1);
            else
                return GetEndOfMonth(DateTime.Now.Month - 1, DateTime.Now.Year);
        }

        public static DateTime GetStartOfCurrentMonth()
        {
            return GetStartOfMonth(DateTime.Now.Month, DateTime.Now.Year);
        }

        public static DateTime GetEndOfCurrentMonth()
        {
            return GetEndOfMonth(DateTime.Now.Month, DateTime.Now.Year);
        }

        #endregion

        #region Years

        public static DateTime GetStartOfYear(int year)
        {
            return new DateTime(year, 1, 1, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfYear(int year)
        {
            return new DateTime(year, 12, DateTime.DaysInMonth(year, 12), 23, 59, 59, 999);
        }

        public static DateTime GetStartOfLastYear()
        {
            return GetStartOfYear(DateTime.Now.Year - 1);
        }

        public static DateTime GetEndOfLastYear()
        {
            return GetEndOfYear(DateTime.Now.Year - 1);
        }

        public static DateTime GetStartOfCurrentYear()
        {
            return GetStartOfYear(DateTime.Now.Year);
        }

        public static DateTime GetEndOfCurrentYear()
        {
            return GetEndOfYear(DateTime.Now.Year);
        }

        #endregion

        #region Days

        public static DateTime GetStartOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 0, 0, 0, 0);
        }

        public static DateTime GetEndOfDay(DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 23, 59, 59, 999);
        }

        #endregion

        public static string GetDateLiteral(DateTime miFecha)
        {
            var strFecha = miFecha.Day.ToString();

            switch (miFecha.Month)
            {
                case 1:
                    strFecha = strFecha + " de Enero de ";
                    break;

                case 2:
                    strFecha = strFecha + " de Febrero de ";
                    break;

                case 3:
                    strFecha = strFecha + " de Marzo de ";
                    break;

                case 4:
                    strFecha = strFecha + " de Abril de ";
                    break;

                case 5:
                    strFecha = strFecha + " de Mayo de ";
                    break;

                case 6:
                    strFecha = strFecha + " de Junio de ";
                    break;

                case 7:
                    strFecha = strFecha + " de Julio de ";
                    break;

                case 8:
                    strFecha = strFecha + " de Agosto de ";
                    break;

                case 9:
                    strFecha = strFecha + " de Septiembre de ";
                    break;

                case 10:
                    strFecha = strFecha + " de Octubre de ";
                    break;

                case 11:
                    strFecha = strFecha + " de Noviembre de ";
                    break;

                case 12:
                    strFecha = strFecha + " de Diciembre de ";
                    break;
            }

            return strFecha + miFecha.Year;
        }

        /// <summary>
        ///     FUncion que devuelve la diferencia de dias, horas, minutos o segundos entre dos fechas
        /// </summary>
        /// <param name="fecha1" type="System.DateTime">
        ///     <para>
        ///         Fecha Inicial
        ///     </para>
        /// </param>
        /// <param name="fecha2" type="System.DateTime">
        ///     <para>
        ///         Fecha Final
        ///     </para>
        /// </param>
        /// <param name="type" type="string">
        ///     <para>
        ///         Tipo de diferencia:
        ///             "ss" - segundos
        ///             "mm" - minutos
        ///             "hh" - horas
        ///             "dd" - dias
        ///     </para>
        /// </param>
        /// <returns>
        ///     A int value...
        /// </returns>
        public static int DateDiff(DateTime fecha1, DateTime fecha2, string type)
        {
            var span = fecha1.Subtract(fecha2);

            switch (type)
            {
                case "ss":
                    return span.Seconds;

                case "mm":
                    return span.Minutes + (span.Hours * 60);

                case "hh":
                    return span.Hours + (span.Days * 24);

                case "dd":
                    return span.Days;

                default:
                    return 0;
            }
        }

        /// <summary>
        ///     Funcion que verifica di la primera fecha es mayor a la segunda (Evalua desde YEAR hasta SECOND)
        /// </summary>
        /// <param name="fecha1" type="DateTime">
        ///     <para>
        ///         Fecha 1 a evaluar
        ///     </para>
        /// </param>
        /// <param name="fecha2" type="DateTime">
        ///     <para>
        ///         Fecha 2 que se compara con la Fecha 1
        ///     </para>
        /// </param>
        public static bool DateEsMayor(DateTime fecha1, DateTime fecha2)
        {
            if (fecha1.Year > fecha2.Year)
                return true;
            if (fecha1.Year == fecha2.Year)
            {
                if (fecha1.Month > fecha2.Month)
                    return true;
                if (fecha1.Month == fecha2.Month)
                {
                    if (fecha1.Day > fecha2.Day)
                        return true;
                    if (fecha1.Day == fecha2.Day)
                    {
                        if (fecha1.Hour > fecha2.Hour)
                            return true;
                        if (fecha1.Hour == fecha2.Hour)
                        {
                            if (fecha1.Minute > fecha2.Minute)
                                return true;
                            if (fecha1.Minute == fecha2.Minute)
                            {
                                if (fecha1.Second >= fecha2.Second)
                                    return true;
                                return false;
                            }
                            return false;
                        }
                        return false;
                    }
                    return false;
                }
                return false;
            }
            return false;
        }

        /// <summary>
        ///     Funcion que determina si una hora es Mayor o Igual a otra
        /// </summary>
        /// <param name="hora1" type="System.DateTime">
        ///     <para>
        ///         Hora 1 a comparar
        ///     </para>
        /// </param>
        /// <param name="hora2" type="System.DateTime">
        ///     <para>
        ///         Hora 2 a comparar
        ///     </para>
        /// </param>
        /// <returns>
        ///     TRUE si el primer parametro pasado es mayor o igual
        ///     FALSE si el segundo parametro es mayor
        /// </returns>
        public static bool DateHoraEsMayorIgualQue(DateTime hora1, DateTime hora2)
        {
            if (hora1.Hour > hora2.Hour)
                return true;
            if (hora1.Hour == hora2.Hour)
            {
                if (hora1.Minute > hora2.Minute)
                    return true;
                if (hora1.Minute == hora2.Minute)
                    return true;
                return false;
            }
            return false;
        }

        /// <summary>
        ///     Funcion que determina si una hora es mayor a otra
        /// </summary>
        /// <param name="hora1" type="System.DateTime">
        ///     <para>
        ///         Hora 1 a comparar
        ///     </para>
        /// </param>
        /// <param name="hora2" type="System.DateTime">
        ///     <para>
        ///         Hora 2 a comparar
        ///     </para>
        /// </param>
        /// <returns>
        ///     TRUE si el primer parametro pasado es Mayor
        ///     FALSE si el segundo parametro es mayor
        /// </returns>
        public static bool DateHoraEsMayorQue(DateTime hora1, DateTime hora2)
        {
            if (hora1.Hour > hora2.Hour)
                return true;
            if (hora1.Hour == hora2.Hour)
            {
                if (hora1.Minute > hora2.Minute)
                    return true;
                if (hora1.Minute == hora2.Minute)
                    return false;
                return false;
            }
            return false;
        }

        /// <summary>
        ///     Funcion que determina si una hora es Menor o Igual a otra
        /// </summary>
        /// <param name="hora1" type="System.DateTime">
        ///     <para>
        ///         Hora 1 a comparar
        ///     </para>
        /// </param>
        /// <param name="hora2" type="System.DateTime">
        ///     <para>
        ///         Hora 2 a comparar
        ///     </para>
        /// </param>
        /// <returns>
        ///     TRUE si el primer parametro pasado es menor o igual
        ///     FALSE si el segundo parametro es menor
        /// </returns>
        public static bool DateHoraEsMenorIgualQue(DateTime hora1, DateTime hora2)
        {
            if (hora1.Hour < hora2.Hour)
                return true;
            if (hora1.Hour == hora2.Hour)
            {
                if (hora1.Minute < hora2.Minute)
                    return true;
                if (hora1.Minute == hora2.Minute)
                    return true;
                return false;
            }
            return false;
        }

        /// <summary>
        ///     Funcion que determina si una hora es menor a otra
        /// </summary>
        /// <param name="hora1" type="System.DateTime">
        ///     <para>
        ///         Hora 1 a comparar
        ///     </para>
        /// </param>
        /// <param name="hora2" type="System.DateTime">
        ///     <para>
        ///         Hora 2 a comparar
        ///     </para>
        /// </param>
        /// <returns>
        ///     TRUE si el primer parametro pasado es menor
        ///     FALSE si el segundo parametro es menor
        /// </returns>
        public static bool DateHoraEsMenorQue(DateTime hora1, DateTime hora2)
        {
            if (hora1.Hour < hora2.Hour)
                return true;
            if (hora1.Hour == hora2.Hour)
            {
                if (hora1.Minute < hora2.Minute)
                    return true;
                if (hora1.Minute == hora2.Minute)
                    return false;
                return false;
            }
            return false;
        }

        /// <summary>
        ///     Funcion que determina si una hora esta entre dos horas
        /// </summary>
        /// <param name="hora" type="System.DateTime">
        ///     <para>
        ///         Hora que se debe evaluar
        ///     </para>
        /// </param>
        /// <param name="horaInicio" type="System.DateTime">
        ///     <para>
        ///         Hora inicial del rango
        ///     </para>
        /// </param>
        /// <param name="horaFinal" type="System.DateTime">
        ///     <para>
        ///         Hora final del rango
        ///     </para>
        /// </param>
        /// <returns>
        ///     TRUE:  si el primer parametro esta entre los otros dos parametros
        ///     FALSE: si el primer parametro NO esta entre los otros dos parametros
        /// </returns>
        public static bool DateHoraEstaEnRango(DateTime hora, DateTime horaInicio, DateTime horaFinal)
        {
            return (DateEsMayor(hora, horaInicio) && DateEsMayor(horaFinal, hora));
        }
    }
}