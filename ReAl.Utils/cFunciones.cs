#region

/* ****************************************************** */
/* GENERADO POR: ReAl ClassGenerator
/* SISTEMA: AP
/* AUTOR: R. Alonzo Vera
/* FECHA: 04/10/2010  -  18:15
/* ****************************************************** */

#endregion

using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace ReAl.Utils
{
    public static class cFunciones
    {
        /// <summary>
        ///     Sentencia IF
        /// </summary>
        /// <param name="condicion" type="bool">
        ///     <para>
        ///         Condicion
        ///     </para>
        /// </param>
        /// <param name="truePart" type="string">
        ///     <para>
        ///         TRUE part
        ///     </para>
        /// </param>
        /// <param name="falsePart" type="string">
        ///     <para>
        ///         FALSE part
        ///     </para>
        /// </param>
        /// <returns>
        ///     Valor que varia dependiendo de la condicion
        /// </returns>
        public static object Iif(bool condicion, object truePart, object falsePart)
        {
            if (condicion)
                return truePart;
            return falsePart;
        }

        /// <summary>
        ///     Función que exporta un DataTable a una hoja excel temporal
        /// </summary>
        /// <param name="data" type="System.Data.DataTable">
        ///     <para>
        ///         DataTable para ser exportado
        ///     </para>
        /// </param>
        /// <param name="titulo" type="string">
        ///     <para>
        ///         Titulo de la hoja Excel
        ///     </para>
        /// </param>
        public static void ConvertDataTableToExcel(DataTable data, string titulo)
        {
            var separador = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;

            var sb = new StringBuilder();
            var aux = string.Empty;

            sb.Append(aux.Substring(0, aux.Length));

            aux = titulo;
            sb.AppendLine(aux.Substring(0, aux.Length));

            aux = "";
            sb.AppendLine(aux.Substring(0, aux.Length));
            aux = string.Empty;

            foreach (DataColumn col in data.Columns)
            {
                aux = (String.Format("{0}{1}" + separador, aux, col.ColumnName));
            }

            if (aux.Length == 0)
                sb.AppendLine(aux.Substring(0, aux.Length));
            else
                sb.AppendLine(aux.Substring(0, aux.Length - 1));
            aux = String.Empty;

            foreach (DataRow row in data.Rows)
            {
                foreach (DataColumn col in data.Columns)
                {
                    string celda;

                    if (row[col.ColumnName].ToString().StartsWith("01/01/0001"))
                        celda = row[col.ColumnName].ToString().Replace("01/01/0001", "");
                    else if (row[col.ColumnName].ToString().EndsWith("00:00"))
                        celda = row[col.ColumnName].ToString().Replace("00:00", "");
                    else if (row[col.ColumnName].ToString().EndsWith("12:00:00 a.m."))
                        celda = row[col.ColumnName].ToString().Replace("12:00:00 a.m.", "");
                    else
                        celda = row[col.ColumnName].ToString();

                    aux = (String.Format("{0}{1}" + separador, aux, celda));
                }

                if (aux.Length == 0)
                    sb.AppendLine(aux.Substring(0, aux.Length));
                else
                    sb.AppendLine(aux.Substring(0, aux.Length - 1));
                aux = String.Empty;
            }

            //CREAMOS UN ARCHIVO TEMPORAL
            var fileName = string.Format("{0}{1}.csv", Environment.GetEnvironmentVariable("TEMP"), Guid.NewGuid());
            var str = new StreamWriter(fileName, false, Encoding.UTF8);

            str.Write(sb);
            str.Close();

            Process.Start("excel", fileName);
        }

        /// <summary>
        ///     Funcion que UNE dos DataTables en uno solo
        /// </summary>
        /// <param name="first" type="DataTable">
        ///     <para>
        ///         Primer DataTable
        ///     </para>
        /// </param>
        /// <param name="second" type="DataTable">
        ///     <para>
        ///         Segundo DataTable
        ///     </para>
        /// </param>
        public static DataTable UnionDataTable(DataTable first, DataTable second)
        {
            //Result table
            var table = new DataTable("Union");
            //Build new columns
            var newcolumns = new DataColumn[first.Columns.Count];

            for (var i = 0; i < first.Columns.Count; i++)
            {
                newcolumns[i] = new DataColumn(first.Columns[i].ColumnName,
                first.Columns[i].DataType);
            }
            //add new columns to result table
            table.Columns.AddRange(newcolumns);
            table.BeginLoadData();

            //Load data from first table
            foreach (DataRow row in first.Rows)
            {
                table.LoadDataRow(row.ItemArray, true);
            }

            //Load data from second table
            foreach (DataRow row in second.Rows)
            {
                table.LoadDataRow(row.ItemArray, true);
            }
            table.EndLoadData();
            return table;
        }

        /// <summary>
        ///     Funcion que evalua si un dia es laboral o no
        /// </summary>
        /// <param name="fecha" type="System.DateTime">
        ///     <para>
        ///         Fecha a ser evaluada
        ///     </para>
        /// </param>
        /// <returns>
        ///     Valor TRUE si el dia es laboral, FALSE si no es dia laboral
        /// </returns>
        public static bool EsDiaLaboral(DateTime fecha)
        {
            //Domingo   = 0
            //Lunes     = 1
            //Martes    = 2
            //Miercoles = 3
            //Jueves    = 4
            //Viernes   = 5
            //Sabado    = 6
            var day = (int)fecha.DayOfWeek;

            if (day > 0 && day < 6)
                return true;
            else
                return false;
        }
    }
}