﻿using System;
using System.Net.NetworkInformation;

namespace ReAl.Utils
{
    public static class cFuncionesRed
    {
        /// <summary>
        /// returns the mac address of the first operation nic found.
        /// </summary>
        /// <returns></returns>
        public static string GetMacAddress()
        {
            var macAddresses = "";

            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus == OperationalStatus.Up)
                {
                    macAddresses += nic.GetPhysicalAddress().ToString();
                    break;
                }
            }
            return macAddresses;
        }

        /// <summary>
        /// returns the mac address of the NIC with max speed.
        /// </summary>
        /// <returns></returns>
        public static string GetMacAddressMaxSpeed()
        {
            const int minMacAddrLength = 12;
            var macAddress = "";
            long maxSpeed = -1;

            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                var tempMac = nic.GetPhysicalAddress().ToString();
                if (nic.Speed > maxSpeed && !String.IsNullOrEmpty(tempMac) && tempMac.Length >= minMacAddrLength)
                {
                    maxSpeed = nic.Speed;
                    macAddress = tempMac;
                }
            }
            return macAddress;
        }
    }
}