﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;

namespace ReAl.Utils
{
    public static class cFuncionesObjetos
    {
        public static byte[] SerializeDataRow(DataRow row)
        {
            if (row == default(DataRow))
                return default(byte[]);

            var memStream = new MemoryStream();
            var serializer = new XmlSerializer(typeof(object[]));
            serializer.Serialize(memStream, row.ItemArray);

            return memStream.ToArray();
        }

        public static void DeserializeDataRow(DataTable table, byte[] rowBytes)
        {
            if (table == default(DataTable) || rowBytes == default(byte[]))
                return;

            var memStream = new MemoryStream(rowBytes);
            var serializer = new XmlSerializer(typeof(object[]));
            table.Rows.Add((object[])serializer.Deserialize(memStream));
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static DataTable ConvertToDataTable(Object o)
        {
            var properties = o.GetType().GetProperties();
            var dt = CreateDataTable(properties);
            FillData(properties, dt, o);
            return dt;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static DataTable ConvertToDataTable(Object[] array)
        {
            var properties = array.GetType().GetElementType().GetProperties();
            var dt = CreateDataTable(properties);

            if (array.Length != 0)
            {
                foreach (var o in array)
                    FillData(properties, dt, o);
            }

            return dt;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="properties"></param>
        /// <returns></returns>
        private static DataTable CreateDataTable(PropertyInfo[] properties)
        {
            var dt = new DataTable();
            DataColumn dc = null;

            foreach (var pi in properties)
            {
                dc = new DataColumn();
                dc.ColumnName = pi.Name;
                dc.DataType = pi.PropertyType;

                dt.Columns.Add(dc);
            }

            return dt;
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="properties"></param>
        /// <param name="dt"></param>
        /// <param name="o"></param>
        private static void FillData(PropertyInfo[] properties, DataTable dt, Object o)
        {
            var dr = dt.NewRow();

            foreach (var pi in properties)
                dr[pi.Name] = pi.GetValue(o, null);

            dt.Rows.Add(dr);
        }

        public static DataTable ToDataTable<T>(IList<T> data)
        {
            try
            {
                var props = TypeDescriptor.GetProperties(typeof(T));
                var table = new DataTable();
                for (var i = 0; i < props.Count; i++)
                {
                    var prop = props[i];
                    if (prop.PropertyType.FullName.ToUpper().Contains("NULL"))
                        table.Columns.Add(prop.Name);
                    else
                        table.Columns.Add(prop.Name);
                }
                var values = new object[props.Count];
                foreach (var item in data)
                {
                    for (var i = 0; i < values.Length; i++)
                    {
                        values[i] = props[i].GetValue(item).ToString().ToUpper();
                    }
                    table.Rows.Add(values);
                }
                return table;
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }

        public static List<T> ToCollection<T>(DataTable dataTable) where T : new()
        {
            var miLista = new List<T>();

            //Obtenemos los nombres de las columnas
            var columnsNames = new List<string>();
            foreach (DataColumn dataColumn in dataTable.Columns)
                columnsNames.Add(dataColumn.ColumnName);

            //Populamos la lista
            var obj = new T();
            var columnname = "";
            var value = "";
            PropertyInfo[] properties;
            properties = typeof(T).GetProperties();

            foreach (DataRow row in dataTable.Rows)
            {
                foreach (var objProperty in properties)
                {
                    columnname = columnsNames.Find(name => name.ToLower() == objProperty.Name.ToLower());
                    if (!string.IsNullOrEmpty(columnname))
                    {
                        value = row[columnname].ToString();
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (Nullable.GetUnderlyingType(objProperty.PropertyType) != null)
                            {
                                value = row[columnname].ToString().Replace("$", "").Replace(",", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(Nullable.GetUnderlyingType(objProperty.PropertyType).ToString())), null);
                            }
                            else
                            {
                                value = row[columnname].ToString().Replace("%", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(objProperty.PropertyType.ToString())), null);
                            }
                        }
                    }
                }
            }
            return miLista;
        }
    }
}