﻿using System;
using System.Windows.Forms;

namespace ReAl.RicherTextBox
{
    public partial class FindForm : Form
    {
        int lastFound = 0;
        RichTextBox rtbInstance = null;
        public RichTextBox RtbInstance
        {
            set { this.rtbInstance = value; }
            get { return this.rtbInstance; }
        }

        public string InitialText
        {
            set { this.txtSearchText.Text = value; }
            get { return this.txtSearchText.Text; }
        }

        public FindForm()
        {
            this.InitializeComponent();
            this.TopMost = true;
        }

        void rtbInstance_SelectionChanged(object sender, EventArgs e)
        {
            this.lastFound = this.rtbInstance.SelectionStart;
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            this.rtbInstance.SelectionChanged -= this.rtbInstance_SelectionChanged;
            this.Close();
        }

        private void btnFindNext_Click(object sender, EventArgs e)
        {
            var options = RichTextBoxFinds.None;
            if (this.chkMatchCase.Checked) options |= RichTextBoxFinds.MatchCase;
            if (this.chkWholeWord.Checked) options |= RichTextBoxFinds.WholeWord;

            var index = this.rtbInstance.Find(this.txtSearchText.Text, this.lastFound, options);
            this.lastFound += this.txtSearchText.Text.Length;
            if (index >= 0)
            {
                this.rtbInstance.Parent.Focus();
            }
            else
            {
                MessageBox.Show("Search string not found", "Find", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.lastFound = 0;
            }

        }

        private void FindForm_Load(object sender, EventArgs e)
        {
            if (this.rtbInstance != null)
                this.rtbInstance.SelectionChanged += new EventHandler(this.rtbInstance_SelectionChanged);
        }
    }
}
