﻿using System;
using System.Windows.Forms;

namespace ReAl.RicherTextBox
{
    public partial class ReplaceForm : FindForm
    {
        public new RichTextBox RtbInstance
        {
            set
            {
                base.RtbInstance = value;
            }
            get
            {
                return base.RtbInstance;
            }
        }

        public new string InitialText
        {
            set
            {
                base.InitialText = value;
            }
        }


        public ReplaceForm()
        {
            this.InitializeComponent();
        }

        private void btnReplace_Click(object sender, EventArgs e)
        {
            if (this.RtbInstance.SelectionLength > 0)
            {
                var start = this.RtbInstance.SelectionStart;
                var len = this.RtbInstance.SelectionLength;
                this.RtbInstance.Text = this.RtbInstance.Text.Remove(start, len);
                this.RtbInstance.Text = this.RtbInstance.Text.Insert(start, this.txtReplace.Text);
                this.RtbInstance.Focus();
            }
        }
    }
}
