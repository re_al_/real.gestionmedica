﻿using System;
using System.Collections;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReAl.Utils;

namespace ReAl.GestionMedica.Test
{
    
    
    /// <summary>
    ///This is a test class for cFuncionesMailTest and is intended
    ///to contain all cFuncionesMailTest Unit Tests
    ///</summary>
    [TestClass()]
    public class cFuncionesMailTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for sendMail
        ///</summary>
        [TestMethod()]
        public void sendMailTest()
        {
            try
            {
                string mailDestino = "7.re.al.7@gmail.com";
                string mailAsunto = "Prueba desde GMAIL";
                string mailMsj = "Hola esta es una Prueba";
                cFuncionesMail.SendMail(mailDestino, mailAsunto, mailMsj);
            }
            catch (Exception exp)
            {
                Assert.Inconclusive(exp.Message);    
            }
            
            
        }

        /// <summary>
        ///A test for sendMail
        ///</summary>
        [TestMethod()]
        public void sendMailTest1()
        {
            ArrayList mailDestino = null; // TODO: Initialize to an appropriate value
            string mailAsunto = string.Empty; // TODO: Initialize to an appropriate value
            string mailMsj = string.Empty; // TODO: Initialize to an appropriate value
            cFuncionesMail.SendMail(mailDestino, mailAsunto, mailMsj);
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }
    }
}
