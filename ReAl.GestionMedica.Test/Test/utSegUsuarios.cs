#region 
/***********************************************************************************************************
	NOMBRE:       utSegUsuarios
	DESCRIPCION:
		Clase que implmenta los metodos y operaciones sobre la Tabla SegUsuarios

	REVISIONES:
		Ver        FECHA       Autor            Descripción 
		---------  ----------  ---------------  ------------------------------------
		1.0        29/05/2013  R Alonzo Vera A  Creación 

*************************************************************************************************************/
#endregion



#region

using System.Collections.Generic;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReAl.GestionMedica.Conn; //Reference Microsoft.VisualStudio.QualityTools.UnitTestFramework

#endregion

namespace ReAl.GestionMedica.Test.Test
{
	[TestClass()]
	public class utSegUsuarios
	{
		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		[TestInitialize()]
		public void MyTestInitialize()
		{
			cParametros.bChangeUserOnLogon = cParametrosTest.bChangeUserOnLogon;
			cParametros.bUseIntegratedSecurity = cParametrosTest.bUseIntegratedSecurity;
			cParametros.server = cParametrosTest.server;
			cParametros.puerto = cParametrosTest.puerto;
			cParametros.userDefault = cParametrosTest.userDefault;
			cParametros.passDefault = cParametrosTest.passDefault;
			cParametros.bd = cParametrosTest.bd;
			cParametros.schema = cParametrosTest.schema;
		}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion

		/// <summary>
		///Prueba para el metodo Insert(rnSegUsuarios)
		///</summary>
		[TestMethod()]
		public void InsertTest()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			entSegUsuarios obj = new entSegUsuarios();
			bool expected = true;
			bool actual;
			
			//obj.idsus = 0;
			//obj.idsus = 0;
			//obj.loginsus = "";
			//obj.loginsus = "";
			//obj.passsus = "";
			//obj.passsus = "";
			//obj.nombresus = "";
			//obj.nombresus = "";
			//obj.apellidosus = "";
			//obj.apellidosus = "";
			//obj.vigentesus = 0;
			//obj.vigentesus = 0;
			//obj.fechavigentesus = null;
			//obj.fechavigentesus = null;
			//obj.fechapasssus = null;
			//obj.fechapasssus = null;
			//obj.usucresus = "";
			//obj.usucresus = "";
			
			actual = target.Insert(obj);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Insert(rnSegUsuarios, cTrans)
		///</summary>
		[TestMethod()]
		public void InsertTestTrans()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			entSegUsuarios obj = new entSegUsuarios();
			cTrans localTrans = new cTrans();
			bool expected = true;
			bool actual;
			
			//obj.idsus = 0;
			//obj.idsus = 0;
			//obj.loginsus = "";
			//obj.loginsus = "";
			//obj.passsus = "";
			//obj.passsus = "";
			//obj.nombresus = "";
			//obj.nombresus = "";
			//obj.apellidosus = "";
			//obj.apellidosus = "";
			//obj.vigentesus = 0;
			//obj.vigentesus = 0;
			//obj.fechavigentesus = null;
			//obj.fechavigentesus = null;
			//obj.fechapasssus = null;
			//obj.fechapasssus = null;
			//obj.usucresus = "";
			//obj.usucresus = "";
			
			actual = target.Insert(obj, ref localTrans);
			Assert.AreEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegUsuarios)
		///</summary>
		[TestMethod()]
		public void UpdateTest()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			entSegUsuarios obj = new entSegUsuarios();
			int expected = 1;
			int actual;
			
			//obj.idsus = 0;
			//obj.loginsus = "";
			//obj.passsus = "";
			//obj.nombresus = "";
			//obj.apellidosus = "";
			//obj.vigentesus = 0;
			//obj.fechavigentesus = null;
			//obj.fechapasssus = null;
			//obj.apitransaccionsus = "";
			//obj.usumodsus = null;
			
			actual = target.Update(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Update(rnSegUsuarios, cTrans)
		///</summary>
		[TestMethod()]
		public void UpdateTestTrans()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			entSegUsuarios obj = new entSegUsuarios();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.idsus = 0;
			//obj.loginsus = "";
			//obj.passsus = "";
			//obj.nombresus = "";
			//obj.apellidosus = "";
			//obj.vigentesus = 0;
			//obj.fechavigentesus = null;
			//obj.fechapasssus = null;
			//obj.apitransaccionsus = "";
			//obj.usumodsus = null;
			
			actual = target.Update(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegUsuarios)
		///</summary>
		[TestMethod()]
		public void DeleteTest()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			entSegUsuarios obj = new entSegUsuarios();
			int expected = 1;
			int actual;
			
			//obj.idsus = 0;
			
			actual = target.Delete(obj);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el metodo Delete(rnSegUsuarios, cTrans)
		///</summary>
		[TestMethod()]
		public void DeleteTestTrans()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			entSegUsuarios obj = new entSegUsuarios();
			cTrans localTrans = new cTrans();
			int expected = 0;
			int actual;
			
			//obj.idsus = 0;
			
			actual = target.Delete(obj, ref localTrans);
			Assert.AreNotEqual(expected, actual);
		}
		
		/// <summary>
		///Prueba para el método ObtenerLista()
		///</summary>
		[TestMethod()]
		public void ObtenerListaTest()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			List<entSegUsuarios> notExpected = null;
			List<entSegUsuarios> actual = target.ObtenerLista();
			Assert.AreNotEqual(notExpected, actual);
		}
		
		/// <summary>
		///Prueba para el método CargarDataTable()
		///</summary>
		[TestMethod()]
		public void CargarDataTableTest()
		{
			rnSegUsuarios target = new rnSegUsuarios();
			int notExpected = 0; 
			int actualColumns = target.CargarDataTable().Columns.Count;
			Assert.AreNotEqual(notExpected, actualColumns);
		}
		
	}
}

