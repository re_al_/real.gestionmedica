create or replace function tr_pacpacientes_af_upd() returns trigger
    language plpgsql
as
$$
declare
   vlaux  pacpacientes.idppa%type;
BEGIN
   --***********************************************************************************
   --**  FUNCION:        Funcion creada para la ejecucion del Trigger del Tipo UPDATE
   --**  CREADO POR:     R Alonzo Vera A
   --**  FECHA CREACION: 26/06/2013
   --***********************************************************************************    
   
   
  
   --perform spph1ins(NEW.idppa, CURRENT_USER::CHARACTER VARYING);
   perform spph1ins(NEW.idppa, new.usumod::CHARACTER VARYING);
   
   --perform spph2ins(NEW.idppa, CURRENT_USER::CHARACTER VARYING);
   perform spph2ins(NEW.idppa, new.usucre::CHARACTER VARYING);
   
   --perform spph3ins(NEW.idppa, CURRENT_USER::CHARACTER VARYING);
   perform spph3ins(NEW.idppa, new.usucre::CHARACTER VARYING);
   
  
   return new;
end;
$$;

--alter function tr_pacpacientes_af_upd() owner to postgres;

