create or replace function tr_segusuariosrestriccion_ins() returns trigger
    language plpgsql
as
$$
declare
   vlsentencia  segtransacciones.sentenciastr%type;
   vlestadofinal  segestados.estadoses%type;
   vlcontador INTEGER;
   vlloginsus CHARACTER VARYING;
   vlrolsro CHARACTER VARYING;
begin
   --***********************************************************************************
   --**  FUNCION:        Funcion creada para la ejecucion del Trigger del Tipo UPDATE
   --**  CREADO POR:     R Alonzo Vera A
   --**  FECHA CREACION: 14/05/2013
   --***********************************************************************************
            
   -- Recuperar la sentencia definida para esta transacción
   vlsentencia := f_seg_get_sentencia(UPPER(TG_RELNAME),UPPER(NEW.Apitransaccionsur));
   
   IF -- No se ha encontrado la SENTENCIA
   vlsentencia IS NULL OR vlsentencia = '' THEN       
           raise exception '%','SEG-00008##'|| UPPER(NEW.Apitransaccionsur) ||'##' || UPPER(TG_RELNAME) || '##';
   END IF;
   
   IF -- La sentencia definida para esta transacción debería ser INSERT 
   UPPER(vlsentencia) <> UPPER(TG_OP) THEN
           raise exception '%','SEG-00003##'|| UPPER(TG_RELNAME) ||'##' || new.Apiestadosur || '##' || new.Apitransaccionsur || '##';
   END IF;

   --Verificar que el Rol Activo del usuario tenga la transaccion habilitada
   --vlcontador := f_seg_get_rol_tabla_transaccion(CURRENT_USER::CHARACTER VARYING,UPPER(TG_RELNAME),UPPER(NEW.Apitransaccionsur));
   vlcontador := f_seg_get_rol_tabla_transaccion(new.usucresur::CHARACTER VARYING,UPPER(TG_RELNAME),UPPER(NEW.apitransaccionsur));
     
   IF -- El Rol Activo no tiene los permisos necesarios
   vlcontador = 0 THEN
           raise EXCEPTION '%','SEG-00007##' || UPPER(NEW.Apitransaccionsur) || '##'|| UPPER(TG_RELNAME) ||'##';
   END IF;
     
   -- Recuperar el estado FINAL al que se llega con esta transacción y este estado inicial
   vlestadofinal := f_seg_get_estado_final(UPPER(TG_RELNAME), 'INICIAL', UPPER(NEW.Apitransaccionsur));
   
   
   IF -- Verificar que el estado final existe 
   vlestadofinal IS NULL OR vlestadofinal = '' THEN
           raise exception '%','SEG-00006##'|| UPPER(NEW.Apitransaccionsur) ||'##INICIAL##' || UPPER(TG_RELNAME) || '##';
   END IF;
           
   -- la transacción y la transición son correctas para la tabla, registrar datos de auditoria de modificación del registro
   new.Apiestadosur := vlestadofinal;
   --new.Usucresur := current_user;
   new.Feccresur := CURRENT_TIMESTAMP;
   
  --***********************************************************************************
  -- reglas de negocio definidas para la tabla segusuariosrestriccion (por transacción)
  --***********************************************************************************
   
  IF NEW.Apitransaccionsur = 'CREAR' THEN
  	SELECT s.loginsus INTO vlloginsus FROM segusuarios s WHERE s.idsus = NEW.idsus;
  	
  	SELECT s.siglasro INTO vlrolsro FROM segroles s WHERE s.rolsro = NEW.rolsro;
  	
	perform f_seg_grant_rol(vlloginsus,vlrolsro);
  END IF;

 
   
  --***********************************************************************************
   return new;
end;
$$;

--alter function tr_segusuariosrestriccion_ins() owner to postgres;

