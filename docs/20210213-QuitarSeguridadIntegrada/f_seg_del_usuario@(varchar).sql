create or replace function f_seg_del_usuario(pausuario character varying) returns void
    language plpgsql
as
$$
declare
   vlinstruccion  varchar(300);
begin
   vlinstruccion := 'REVOKE ALL PRIVILEGES ON DATABASE integrate FROM '|| upper(pausuario) ||';DROP USER ' || upper(pausuario) || ';';
   --execute vlinstruccion;
   return;
end;
$$;

--alter function f_seg_del_usuario(varchar) owner to postgres;

