create or replace function tr_pachistoriaclinica1_ins() returns trigger
    language plpgsql
as
$$
declare
   vlsentencia  segtransacciones.sentenciastr%type;
   vlestadofinal  segestados.estadoses%type;
   vlcontador INTEGER;
begin
   --***********************************************************************************
   --**  FUNCION:        Funcion creada para la ejecucion del Trigger del Tipo UPDATE
   --**  CREADO POR:     R Alonzo Vera A
   --**  FECHA CREACION: 26/06/2013
   --***********************************************************************************
            
   -- Recuperar la sentencia definida para esta transacción
   vlsentencia := f_seg_get_sentencia(UPPER(TG_RELNAME),UPPER(NEW.Apitransaccion));
   
   IF -- No se ha encontrado la SENTENCIA
   vlsentencia IS NULL OR vlsentencia = '' THEN       
           raise exception '%','SEG-00008##'|| UPPER(NEW.Apitransaccion) ||'##' || UPPER(TG_RELNAME) || '##';
   END IF;
   
   IF -- La sentencia definida para esta transacción debería ser INSERT 
   UPPER(vlsentencia) <> UPPER(TG_OP) THEN
           raise exception '%','SEG-00003##'|| UPPER(TG_RELNAME) ||'##' || new.Apiestado || '##' || new.Apitransaccion || '##';
   END IF;

   
   --Verificar que el Rol Activo del usuario tenga la transaccion habilitada
   --vlcontador := f_seg_get_rol_tabla_transaccion(CURRENT_USER::CHARACTER VARYING,UPPER(TG_RELNAME),UPPER(NEW.Apitransaccion));
   vlcontador := f_seg_get_rol_tabla_transaccion(new.usucre::CHARACTER VARYING,UPPER(TG_RELNAME),UPPER(NEW.Apitransaccion));
   
    
   IF -- El Rol Activo no tiene los permisos necesarios
   vlcontador = 0 THEN
           raise EXCEPTION '%','SEG-00007##' || UPPER(NEW.Apitransaccion) || '##'|| UPPER(TG_RELNAME) ||'##';
   END IF;
     
   -- Recuperar el estado FINAL al que se llega con esta transacción y este estado inicial
   vlestadofinal := f_seg_get_estado_final(UPPER(TG_RELNAME), 'INICIAL', UPPER(NEW.Apitransaccion));
   
   
   IF -- Verificar que el estado final existe 
   vlestadofinal IS NULL OR vlestadofinal = '' THEN
           raise exception '%','SEG-00006##'|| UPPER(NEW.Apitransaccion) ||'##INICIAL##' || UPPER(TG_RELNAME) || '##';
   END IF;
           
   -- la transacción y la transición son correctas para la tabla, registrar datos de auditoria de modificación del registro
   new.Apiestado := vlestadofinal;
   --new.Usucre := current_user;
   new.Feccre := CURRENT_TIMESTAMP;
   
  --***********************************************************************************
  -- reglas de negocio definidas para la tabla pachistoriaclinica1 (por transacción)
  --***********************************************************************************
   
 
   
  --***********************************************************************************
   return new;
end;
$$;

--alter function tr_pachistoriaclinica1_ins() owner to postgres;

