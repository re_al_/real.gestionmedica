create or replace function f_seg_crear_usuario(pausuario character varying, papass character varying) returns void
    language plpgsql
as
$$
declare
   vlinstruccion  varchar(300);
begin
   vlinstruccion := 'CREATE USER ' || pausuario || ' LOGIN NOCREATEDB SUPERUSER NOCREATEROLE PASSWORD ''' || papass || '''';
   --execute vlinstruccion;
  -- crear otra instrucci??n para asignarle grant de conexi??n al nuevo usuario
   vlinstruccion := null;
   --vlinstruccion := 'grant "connect" to "' || upper(pausuario) || '"';
   --execute vlinstruccion;
   return;
end;
$$;

--alter function f_seg_crear_usuario(varchar, varchar) owner to postgres;

