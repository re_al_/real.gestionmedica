create or replace function f_seg_grant_rol(pausuario character varying, parol character varying) returns void
    language plpgsql
as
$$
declare
   vlinstruccion  varchar(300);
begin
   vlinstruccion := 'GRANT ' || upper(parol) || ' TO ' || upper(pausuario) || ';';
   --execute vlinstruccion;
   return;
end;
$$;

--alter function f_seg_grant_rol(varchar, varchar) owner to postgres;

