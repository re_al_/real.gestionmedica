create or replace function tr_segusuarios_ins() returns trigger
    language plpgsql
as
$$
declare
   vlsentencia  segtransacciones.sentenciastr%type;
   vlestadofinal  segestados.estadoses%type;
   vlcontador INTEGER;
begin
   --***********************************************************************************
   --**  FUNCION:        Funcion creada para la ejecucion del Trigger del Tipo UPDATE
   --**  CREADO POR:     R Alonzo Vera A
   --**  FECHA CREACION: 14/05/2013
   --***********************************************************************************
            
   -- Recuperar la sentencia definida para esta transacción
   vlsentencia := f_seg_get_sentencia(UPPER(TG_RELNAME),UPPER(NEW.Apitransaccionsus));
   
   IF -- No se ha encontrado la SENTENCIA
   vlsentencia IS NULL OR vlsentencia = '' THEN       
           raise exception '%','SEG-00008##'|| UPPER(NEW.Apitransaccionsus) ||'##' || UPPER(TG_RELNAME) || '##';
   END IF;
   
   IF -- La sentencia definida para esta transacción debería ser INSERT 
   UPPER(vlsentencia) <> UPPER(TG_OP) THEN
           raise exception '%','SEG-00003##'|| UPPER(TG_RELNAME) ||'##' || new.Apiestadosus || '##' || new.Apitransaccionsus || '##';
   END IF;

   --Verificar que el Rol Activo del usuario tenga la transaccion habilitada
   --vlcontador := f_seg_get_rol_tabla_transaccion(CURRENT_USER::CHARACTER VARYING,UPPER(TG_RELNAME),UPPER(NEW.Apitransaccionsus));
   vlcontador := f_seg_get_rol_tabla_transaccion(new.usucresus::CHARACTER VARYING,UPPER(TG_RELNAME),UPPER(NEW.apitransaccionsus));
     
   IF -- El Rol Activo no tiene los permisos necesarios
   vlcontador = 0 THEN
           raise EXCEPTION '%','SEG-00007##' || UPPER(NEW.Apitransaccionsus) || '##'|| UPPER(TG_RELNAME) ||'##';
   END IF;
     
   -- Recuperar el estado FINAL al que se llega con esta transacción y este estado inicial
   vlestadofinal := f_seg_get_estado_final(UPPER(TG_RELNAME), 'INICIAL', UPPER(NEW.Apitransaccionsus));
   
   
   IF -- Verificar que el estado final existe 
   vlestadofinal IS NULL OR vlestadofinal = '' THEN
           raise exception '%','SEG-00006##'|| UPPER(NEW.Apitransaccionsus) ||'##INICIAL##' || UPPER(TG_RELNAME) || '##';
   END IF;
           
   -- la transacción y la transición son correctas para la tabla, registrar datos de auditoria de modificación del registro
   new.Apiestadosus := vlestadofinal;
   --new.Usucresus := current_user;
   new.Feccresus := CURRENT_TIMESTAMP;
   
  --***********************************************************************************
  -- reglas de negocio definidas para la tabla segusuarios (por transacción)
  --***********************************************************************************
   
  IF NEW.Apitransaccionsus = 'CREAR' THEN
	perform f_seg_crear_usuario(NEW.loginsus, NEW.passsus) ;	
  END IF;

 
   
  --***********************************************************************************
   return new;
end;
$$;

--alter function tr_segusuarios_ins() owner to postgres;

