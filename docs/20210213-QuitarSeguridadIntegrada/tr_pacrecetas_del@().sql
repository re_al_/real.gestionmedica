create or replace function tr_pacrecetas_del() returns trigger
    language plpgsql
as
$$
declare
   vlsentencia  segtransacciones.sentenciastr%type;
   vlestadofinal  segestados.estadoses%type;
   vlcontador INTEGER;
begin
             
   -- Recuperar la sentencia definida para esta transacción
   vlsentencia := f_seg_get_sentencia(UPPER(TG_RELNAME),'ELIMINAR');
   
   IF -- No se ha encontrado la SENTENCIA 
   vlsentencia IS NULL OR vlsentencia = '' THEN   
           raise exception '%','SEG-00008##ELIMINAR##' || UPPER(TG_RELNAME) || '##';
   END IF;
   
   IF -- La sentencia definida para esta transacción debería ser DELETE 
   vlsentencia <> upper(TG_OP) THEN
        raise exception '%','SEG-00003##'|| UPPER(TG_RELNAME) ||'##FINAL##ELIMINAR##';
   END IF;

   --Verificar que el Rol Activo del usuario tenga la transaccion habilitada
   --vlcontador := f_seg_get_rol_tabla_transaccion(CURRENT_USER::CHARACTER VARYING,UPPER(TG_RELNAME),'ELIMINAR');
   vlcontador := f_seg_get_rol_tabla_transaccion(old.usumod::CHARACTER VARYING,UPPER(TG_RELNAME),'ELIMINAR');
     
   IF --Si no existe el permiso para el Rol del Usuario Actual 
   vlcontador = 0 THEN
           raise EXCEPTION '%','SEG-00007##ELIMINAR##'|| UPPER(TG_RELNAME) ||'##';
   END IF;
     
   -- Recuperar el estado FINAL al que se llega con esta transacción y este estado inicial
   vlestadofinal := f_seg_get_estado_final(UPPER(TG_RELNAME), UPPER(OLD.Apiestado), 'ELIMINAR');
   
   -- Verificar que el estado final existe
   IF vlestadofinal IS NULL OR vlestadofinal = '' THEN
           raise exception '%','SEG-00006##ELIMINAR##' || UPPER(OLD.Apiestado) || '##' || UPPER(TG_RELNAME) || '##';
   END IF;
           
   
  --***********************************************************************************
  -- reglas de negocio definidas para la tabla pacrecetas (por transacción)
  --***********************************************************************************
    
   
  --***********************************************************************************
   return old;
end;
$$;

--alter function tr_pacrecetas_del() owner to postgres;

