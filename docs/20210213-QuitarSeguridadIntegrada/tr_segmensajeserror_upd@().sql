create or replace function tr_segmensajeserror_upd() returns trigger
    language plpgsql
as
$$
declare
   vlsentencia  segtransacciones.sentenciastr%type;
   vlestadofinal  segestados.estadoses%type;
   vlcontador INTEGER;
BEGIN
   --***********************************************************************************
   --**  FUNCION:        Funcion creada para la ejecucion del Trigger del Tipo UPDATE
   --**  CREADO POR:     R Alonzo Vera A
   --**  FECHA CREACION: 14/05/2013
   --***********************************************************************************    
    
   -- Verificar que no se haya modificado el usuario de CREACION del registro
   IF -- Se ha modificado el usuario de creación, desplegar ERROR
   NEW.Usucresme <> OLD.Usucresme
   THEN 
           raise exception '%','SEG-00009';
   END IF;
   
   -- Verificar que no se haya modificado fecha de CREACION del registro
   IF -- Se ha modificado la fecha de creación, desplegar ERROR
   NEW.Feccresme <> OLD.Feccresme
   THEN 
           raise exception '%','SEG-00009';
   END IF;
          
   -- Recuperar la sentencia definida para esta transacción
   vlsentencia := f_seg_get_sentencia(UPPER(TG_RELNAME),UPPER(NEW.Apitransaccionsme));
   
   IF -- No se ha encontrado la SENTENCIA
   vlsentencia IS NULL OR vlsentencia = '' THEN
        raise exception '%','SEG-00008##'|| UPPER(NEW.Apitransaccionsme) ||'##' || UPPER(TG_RELNAME) || '##';
   END IF;
   
   IF -- La sentencia definida para esta transacción debería ser UPDATE 
   vlsentencia <> TG_OP THEN
           raise exception '%','SEG-00003##'|| UPPER(TG_RELNAME) ||'##' || new.Apiestadosme || '##' || new.Apitransaccionsme || '##';
   END IF;

   --Verificar que el Rol Activo del usuario tenga la transaccion habilitada
   --vlcontador := f_seg_get_rol_tabla_transaccion(CURRENT_USER::CHARACTER VARYING,UPPER(TG_RELNAME),UPPER(NEW.Apitransaccionsme));
   vlcontador := f_seg_get_rol_tabla_transaccion(new.usumodsme::CHARACTER VARYING,UPPER(TG_RELNAME),UPPER(NEW.apitransaccionsme));
     
   IF --Si no existe el permiso para el Rol del Usuario Actual
   vlcontador = 0 THEN
        raise EXCEPTION '%','SEG-00007##' || UPPER(NEW.Apitransaccionsme) || '##'|| UPPER(TG_RELNAME) ||'##';
   END IF;
     
   -- Recuperar el estado FINAL al que se llega con esta transacción y este estado inicial
   vlestadofinal := f_seg_get_estado_final(UPPER(TG_RELNAME), UPPER(OLD.Apiestadosme), UPPER(NEW.Apitransaccionsme));
   
   -- Verificar que el estado final existe
   IF vlestadofinal IS NULL OR vlestadofinal = '' THEN
           raise exception '%','SEG-00006##'|| UPPER(NEW.Apitransaccionsme) ||'##' || UPPER(OLD.Apiestadosme) || '##' || UPPER(TG_RELNAME) || '##';
   END IF;
           
   -- la transacción y la transición son correctas para la tabla, registrar datos de auditoria de modificación del registro
   new.Apiestadosme := vlestadofinal;
   --new.Usumodsme := current_user;
   new.Fecmodsme := CURRENT_TIMESTAMP;
   
  --***********************************************************************************
  -- reglas de negocio definidas para la tabla segmensajeserror (por transacción)
  --***********************************************************************************
   
  IF NEW.Apitransaccionsme = 'MODIFICAR' THEN

  END IF;

 
   
  --***********************************************************************************
   return new;
end;
$$;

--alter function tr_segmensajeserror_upd() owner to postgres;

