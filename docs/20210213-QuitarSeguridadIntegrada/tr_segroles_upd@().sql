create or replace function tr_segroles_upd() returns trigger
    language plpgsql
as
$$
declare
   vlsentencia  segtransacciones.sentenciastr%type;
   vlestadofinal  segestados.estadoses%type;
   vlcontador INTEGER;
BEGIN
   --***********************************************************************************
   --**  FUNCION:        Funcion creada para la ejecucion del Trigger del Tipo UPDATE
   --**  CREADO POR:     R Alonzo Vera A
   --**  FECHA CREACION: 14/05/2013
   --***********************************************************************************    
    
   -- Verificar que no se haya modificado el usuario de CREACION del registro
   IF -- Se ha modificado el usuario de creación, desplegar ERROR
   NEW.Usucresro <> OLD.Usucresro
   THEN 
           raise exception '%','SEG-00009';
   END IF;
   
   -- Verificar que no se haya modificado fecha de CREACION del registro
   IF -- Se ha modificado la fecha de creación, desplegar ERROR
   NEW.Feccresro <> OLD.Feccresro
   THEN 
           raise exception '%','SEG-00009';
   END IF;
          
   -- Recuperar la sentencia definida para esta transacción
   vlsentencia := f_seg_get_sentencia(UPPER(TG_RELNAME),UPPER(NEW.Apitransaccionsro));
   
   IF -- No se ha encontrado la SENTENCIA
   vlsentencia IS NULL OR vlsentencia = '' THEN
        raise exception '%','SEG-00008##'|| UPPER(NEW.Apitransaccionsro) ||'##' || UPPER(TG_RELNAME) || '##';
   END IF;
   
   IF -- La sentencia definida para esta transacción debería ser UPDATE 
   vlsentencia <> TG_OP THEN
           raise exception '%','SEG-00003##'|| UPPER(TG_RELNAME) ||'##' || new.Apiestadosro || '##' || new.Apitransaccionsro || '##';
   END IF;

   --Verificar que el Rol Activo del usuario tenga la transaccion habilitada
   vlcontador := f_seg_get_rol_tabla_transaccion(CURRENT_USER::CHARACTER VARYING,UPPER(TG_RELNAME),UPPER(NEW.Apitransaccionsro));
   vlcontador := f_seg_get_rol_tabla_transaccion(new.usumodsro::CHARACTER VARYING,UPPER(TG_RELNAME),UPPER(NEW.apitransaccionsro));
     
   IF --Si no existe el permiso para el Rol del Usuario Actual
   vlcontador = 0 THEN
        raise EXCEPTION '%','SEG-00007##' || UPPER(NEW.Apitransaccionsro) || '##'|| UPPER(TG_RELNAME) ||'##';
   END IF;
     
   -- Recuperar el estado FINAL al que se llega con esta transacción y este estado inicial
   vlestadofinal := f_seg_get_estado_final(UPPER(TG_RELNAME), UPPER(OLD.Apiestadosro), UPPER(NEW.Apitransaccionsro));
   
   -- Verificar que el estado final existe
   IF vlestadofinal IS NULL OR vlestadofinal = '' THEN
           raise exception '%','SEG-00006##'|| UPPER(NEW.Apitransaccionsro) ||'##' || UPPER(OLD.Apiestadosro) || '##' || UPPER(TG_RELNAME) || '##';
   END IF;
           
   -- la transacción y la transición son correctas para la tabla, registrar datos de auditoria de modificación del registro
   new.Apiestadosro := vlestadofinal;
   --new.Usumodsro := current_user;
   new.Fecmodsro := CURRENT_TIMESTAMP;
   
  --***********************************************************************************
  -- reglas de negocio definidas para la tabla segroles (por transacción)
  --***********************************************************************************
   
  IF NEW.Apitransaccionsro = 'MODIFICAR' THEN

  END IF;

 
   
  --***********************************************************************************
   return new;
end;
$$;

--alter function tr_segroles_upd() owner to postgres;

