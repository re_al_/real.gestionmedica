create or replace function f_seg_del_rol(pausuario character varying, parol character varying) returns void
    language plpgsql
as
$$
declare
   vlinstruccion  varchar(300);
begin
   vlinstruccion := 'REVOKE ' || upper(parol) || ' FROM ' || upper(pausuario) || ';';
   --execute vlinstruccion;
   return;
end;
$$;

--alter function f_seg_del_rol(varchar, varchar) owner to postgres;

