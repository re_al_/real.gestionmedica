create or replace function tr_segparametros_upd() returns trigger
    language plpgsql
as
$$
declare
   vlsentencia  segtransacciones.sentenciastr%type;
   vlestadofinal  segestados.estadoses%type;
   vlcontador INTEGER;
BEGIN
   --***********************************************************************************
   --**  FUNCION:        Funcion creada para la ejecucion del Trigger del Tipo UPDATE
   --**  CREADO POR:     R Alonzo Vera A
   --**  FECHA CREACION: 14/05/2013
   --***********************************************************************************    
    
   -- Verificar que no se haya modificado el usuario de CREACION del registro
   IF -- Se ha modificado el usuario de creación, desplegar ERROR
   NEW.Usucrespa <> OLD.Usucrespa
   THEN 
           raise exception '%','SEG-00009';
   END IF;
   
   -- Verificar que no se haya modificado fecha de CREACION del registro
   IF -- Se ha modificado la fecha de creación, desplegar ERROR
   NEW.Feccrespa <> OLD.Feccrespa
   THEN 
           raise exception '%','SEG-00009';
   END IF;
          
   -- Recuperar la sentencia definida para esta transacción
   vlsentencia := f_seg_get_sentencia(UPPER(TG_RELNAME),UPPER(NEW.Apitransaccionspa));
   
   IF -- No se ha encontrado la SENTENCIA
   vlsentencia IS NULL OR vlsentencia = '' THEN
        raise exception '%','SEG-00008##'|| UPPER(NEW.Apitransaccionspa) ||'##' || UPPER(TG_RELNAME) || '##';
   END IF;
   
   IF -- La sentencia definida para esta transacción debería ser UPDATE 
   vlsentencia <> TG_OP THEN
           raise exception '%','SEG-00003##'|| UPPER(TG_RELNAME) ||'##' || new.Apiestadospa || '##' || new.Apitransaccionspa || '##';
   END IF;

   --Verificar que el Rol Activo del usuario tenga la transaccion habilitada
   --vlcontador := f_seg_get_rol_tabla_transaccion(CURRENT_USER::CHARACTER VARYING,UPPER(TG_RELNAME),UPPER(NEW.Apitransaccionspa));
   vlcontador := f_seg_get_rol_tabla_transaccion(new.usumodspa::CHARACTER VARYING,UPPER(TG_RELNAME),UPPER(NEW.apitransaccionspa));
     
   IF --Si no existe el permiso para el Rol del Usuario Actual
   vlcontador = 0 THEN
        raise EXCEPTION '%','SEG-00007##' || UPPER(NEW.Apitransaccionspa) || '##'|| UPPER(TG_RELNAME) ||'##';
   END IF;
     
   -- Recuperar el estado FINAL al que se llega con esta transacción y este estado inicial
   vlestadofinal := f_seg_get_estado_final(UPPER(TG_RELNAME), UPPER(OLD.Apiestadospa), UPPER(NEW.Apitransaccionspa));
   
   -- Verificar que el estado final existe
   IF vlestadofinal IS NULL OR vlestadofinal = '' THEN
           raise exception '%','SEG-00006##'|| UPPER(NEW.Apitransaccionspa) ||'##' || UPPER(OLD.Apiestadospa) || '##' || UPPER(TG_RELNAME) || '##';
   END IF;
           
   -- la transacción y la transición son correctas para la tabla, registrar datos de auditoria de modificación del registro
   new.Apiestadospa := vlestadofinal;
   --new.Usumodspa := current_user;
   new.Fecmodspa := CURRENT_TIMESTAMP;
   
  --***********************************************************************************
  -- reglas de negocio definidas para la tabla segparametros (por transacción)
  --***********************************************************************************
   
  IF NEW.Apitransaccionspa = 'MODIFICAR' THEN

  END IF;

 
   
  --***********************************************************************************
   return new;
end;
$$;

--alter function tr_segparametros_upd() owner to postgres;

