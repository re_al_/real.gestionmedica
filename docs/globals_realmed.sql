--
-- PostgreSQL database cluster dump
--

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE administrador;
ALTER ROLE administrador WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION;
CREATE ROLE asistente;
ALTER ROLE asistente WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION;
CREATE ROLE doctor;
ALTER ROLE doctor WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION;
CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION PASSWORD 'md5486307f10356310754d00af10e950481';
CREATE ROLE rvera;
ALTER ROLE rvera WITH SUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION PASSWORD 'md59ccff03b7d6b5a25316ade288d6356c9';
CREATE ROLE secretaria;
ALTER ROLE secretaria WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB NOLOGIN NOREPLICATION;


--
-- Role memberships
--

GRANT administrador TO rvera GRANTED BY postgres;




--
-- PostgreSQL database cluster dump complete
--

