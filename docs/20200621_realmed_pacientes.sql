-- FUNCTION: public.spppaselgrid()

-- DROP FUNCTION public.spppaselgrid();

CREATE OR REPLACE FUNCTION public.spppaselgrid(
	)
    RETURNS TABLE(idppa bigint, "Nombres" character varying, "Ap.Paterno" character varying, "Ap.Materno" character varying, "Sexo" character varying, "FecNac" date, "TipoSangre" character varying, "EstCivil" character varying, "Ocupacion" character varying, "Telefonos" character varying, "Seguro" character varying, "Empresa" character varying, "UltimaVisita" character varying)
    LANGUAGE 'sql'

    COST 100
    VOLATILE
    ROWS 1000
AS $BODY$



select
    ppa.idppa,
    ppa.nombresppa as Nombres,
    ppa.appaternoppa as ApPaterno,
    ppa.apmaternoppa as ApMaterno,
    case ppa.generoppa when true then 'M' when false then 'F' else '-' end as sexo,
    ppa.fechanac::date as FecNac,
    ctp.descripcioncts::character varying AS "TipoSangre",
    cec.descripcioncec::character varying AS "EstCivil",
    ppa.ocupacionppa AS Ocupacion,
    ppa.telefonosppa AS Telefonos,
    ppa.seguroppa as Seguro,
    ppa.empresappa AS Empresa,
	pco.fechapco::character varying || ' - ' || pco.horapco::character varying AS "UltimaVisita"
from pacpacientes ppa
JOIN claestadocivil cec ON cec.idcec = ppa.idcec
JOIN clatiposangre ctp ON ctp.idcts = ppa.idcts
LEFT JOIN (select p.*, row_number() over(partition by p.idppa order by p.fechapco, p.horapco desc) as rn
      from pacconsultas p order by p.idppa) pco on ppa.idppa = pco.idppa AND pco.rn = 1

$BODY$;

ALTER FUNCTION public.spppaselgrid()
    OWNER TO postgres;
