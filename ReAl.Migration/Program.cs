﻿// See https://aka.ms/new-console-template for more information

using System.Collections;
using System.Drawing;
using System.Drawing.Imaging;
using System.Formats.Tar;
using System.Net;
using System.Windows.Forms;
using ReAl.GestionMedica.BackendConnector;
using ReAl.GestionMedica.BackendConnector.Controllers;
using ReAl.GestionMedica.BackendConnector.Entities;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.Migration;



var rnBd = new RnPacHistoriaclinica3();
var rnRest = new PatHistoryneuro3Controller();
BackendParameters.JwtToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE3MDc1NjAyMjgsImlzcyI6Imh0dHBzOi8vbWFyY29hMTM2LnNnLWhvc3QuY29tIiwibmJmIjoxNzA3NTYwMjI4LCJleHAiOjE3MDgyODAyMjgsImRhdGEiOnsidXNlcklkIjoxLCJsb2dpbiI6InJ2ZXJhIiwic2NvcGVzIjpbeyJpZF91c2Vyc19yZXN0cmljdGlvbiI6MywiaWRfd29ya3BsYWNlcyI6MSwiaWRfcm9sZXMiOjAsImlkX3VzZXJzIjoxLCJyb2xlX2FjdGl2ZSI6MSwiYXBpX3VzdWNyZSI6InJ2ZXJhIiwiYXBpX3VzdW1vZCI6bnVsbCwiYXBpX3VzdWRlbCI6bnVsbCwiYXBpX3RyYW5zYWN0aW9uIjoiQ1JFQVRFIiwiYXBpX3N0YXR1cyI6IkFDVElWRSJ9XX19.CM_UsGSuwJYO11sQVe-KsxWu70r_Cq4z_n_D2IR05FE";
var credential = CFirebaseHelper.FirebaseLogin();
//Migrate history clinic
var strFiltr = "AND h6imagen is null";

var arrWhere = new ArrayList();
arrWhere.Add(1);

Stream stream = File.Open(@"D:/Neuro-Images/Default.jpeg", FileMode.Open);

//foreach (var obj in rnBd.ObtenerLista(arrWhere, arrWhere, strFiltr))
//{
    // TEST:
    var obj = rnBd.ObtenerObjeto(EntPacHistoriaclinica3.Fields.idppa, 9005331);
    //if (obj.h6imagen != null)
    //{
        try
        {
            var objHistRest = rnRest.GetById(obj.idppa);

            if (string.IsNullOrEmpty(objHistRest.H6Imagenurl))
            {
                var strExtension = "jpeg";
                var strFile = obj.idppa + "." + strExtension;
                //File.WriteAllBytes(@"D:/Neuro-Images/" + strFile, obj.h6imagen);
                //Stream stream = File.Open(@"D:/Neuro-Images/" + strFile, FileMode.Open);

                var strFileName = obj.idppa + "." + strExtension;
                var fireBaseUrl = CFirebaseHelper.FirebaseUploadAsync(credential, stream, strFileName, "history-images");

                objHistRest.H6Imagenurl = await fireBaseUrl;
                rnRest.Create(objHistRest);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }
    //}
//}

Console.WriteLine("Hello, World!");

static Image GenerarMemoryStreamImagen(byte[] bytesImagen)
{
    var msImagen = new MemoryStream();
    msImagen.Write(bytesImagen, 0, bytesImagen.Length);
    msImagen.Position = 0;
    return Image.FromStream(msImagen);
}