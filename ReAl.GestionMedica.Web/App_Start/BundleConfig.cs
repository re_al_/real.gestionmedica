﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;

namespace ReAl.GestionMedica.Web
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkID=303951
        public static void RegisterBundles(BundleCollection bundles)
        {
            

            // El orden es muy importante para el funcionamiento de estos archivos ya que tienen dependencias explícitas
            bundles.Add(new ScriptBundle("~/bundles/MsAjaxJs").Include(
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjax.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxApplicationServices.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxTimer.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxWebForms.js"));

            // Use la versión de desarrollo de Modernizr para desarrollar y aprender. Luego, cuando esté listo
            // para la producción, use la herramienta de compilación disponible en https://modernizr.com para seleccionar solo las pruebas que necesite
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                            "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                            "~/assets/medilab/vendor/jquery/jquery.min.js",
                            "~/assets/medilab/vendor/bootstrap/js/bootstrap.bundle.min.js",
                            "~/assets/medilab/vendor/jquery.easing/jquery.easing.min.js",
                            "~/assets/medilab/vendor/php-email-form/validate.js",
                            "~/assets/medilab/vendor/venobox/venobox.min.js",
                            "~/assets/medilab/vendor/waypoints/jquery.waypoints.min.js",
                            "~/assets/medilab/vendor/counterup/counterup.min.js",
                            "~/assets/medilab/vendor/owl.carousel/owl.carousel.min.js",
                            "~/assets/medilab/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include(
                            "~/assets/medilab/vendor/bootstrap/css/bootstrap.min.css",
                            "~/assets/medilab/vendor/icofont/icofont.min.css",
                            "~/assets/medilab/vendor/boxicons/css/boxicons.min.css",
                            "~/assets/medilab/vendor/venobox/venobox.css",
                            "~/assets/medilab/vendor/animate.css/animate.min.css",
                            "~/assets/medilab/vendor/remixicon/remixicon.css",
                            "~/assets/medilab/vendor/owl.carousel/assets/owl.carousel.min.css",
                            "~/assets/medilab/vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css"));

            bundles.Add(new ScriptBundle("~/bundles/privatejs").Include(
                            "~/assets/sbadmin2/vendor/jquery/jquery.min.js",
                            "~/assets/sbadmin2/vendor/bootstrap/js/bootstrap.bundle.min.js",
                            "~/assets/sbadmin2/vendor/jquery-easing/jquery.easing.min.js",
                            "~/assets/sbadmin2/js/sb-admin-2.js"));

            bundles.Add(new StyleBundle("~/bundles/privatecss").Include(
                            "~/assets/sbadmin2/css/sb-admin-2.css"));
        }
    }
}