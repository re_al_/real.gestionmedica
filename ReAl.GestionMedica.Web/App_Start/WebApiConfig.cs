﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;

namespace ReAl.GestionMedica.Web
{
    public static class WebApiConfig
    {
        /// <summary>
        /// Registrar la configuración
        /// </summary>
        /// <param name="config"></param>
        public static void Register(HttpConfiguration config)
        {
            //KB: http://stackoverflow.com/questions/26649361/options-405-method-not-allowed-web-api-2
            //Necesario para evitar error Http.405
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            // To return json return
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
        }
    }
}