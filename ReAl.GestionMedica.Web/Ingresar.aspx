﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ingresar.aspx.cs" Inherits="ReAl.GestionMedica.Web.Ingresar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Login - SB Admin Pro</title>

    <link rel="icon" type="image/x-icon" href="/assets/sbadmin2/img/favicon.png" />
</head>
<body class="bg-primary">
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/css/sb-admin-2.css") %>" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/fontawesome-free/js/all.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/feather/feather.min.js") %>"></script>

    <div id="layoutAuthentication">
        <div id="layoutAuthentication_content">
            <main>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-11">
                            <!-- Basic login form-->
                            <div class="card o-hidden border-0 shadow-lg my-5">
                                <div class="card-body p-0">
                                    <div class="row" >
                                        <div class="col-lg-6 d-none d-lg-block">
                                            <img src="/assets/sbadmin2/img/login.jpg" 
                                                 class="img-fluid" alt="Responsive image"
                                                style="width: auto; height: 100%; max-width: 100%;"/>
                                        </div>
                                        <div class="col-lg-6">
                                            <!-- Login form-->
                                            <form id="form1" runat="server">
                                                <div class="p-5">
                                                    <div class="text-center">
                                                        <h1 class="logo mr-auto"><a href="Default.aspx">NeuroDiagnóstico</a></h1>
                                                        <h1 class="h4 text-gray-900 mb-4">Bienvenido</h1>
                                                    </div>                                                    
                                                    <div class="text-center">
                                                        Si desea agendar una cita, ingrese en esta sección:
                                                    </div>
                                                    <div class="user">
                                                        <!-- Form Group (email address)-->
                                                        <div class="form-group">
                                                            <label class="small mb-1" for="txtIdPpa">ID acceso</label>
                                                            <asp:TextBox ID="txtIdPpa" runat="server"
                                                                CssClass="form-control" ClientIDMode="Static"
                                                                PlaceHolder="Ingrese ID de acceso..."></asp:TextBox>
                                                        </div>                                                        
                                                        <asp:Button Class="btn btn-primary btn-block"
                                                        OnClick="btnIngreso_Click"
                                                        ID="btnIngreso" runat="server" Text="Agendar cita" />
                                                    </div>  
                                                    <hr/>
                                                    <div class="text-center">
                                                        Si tiene usuario y contraseña, ingrese en esta sección:
                                                    </div>
                                                    <div class="user">
                                                        <!-- Form Group (email address)-->
                                                        <div class="form-group">
                                                            <label class="small mb-1" for="txtUsuario">Usuario</label>
                                                            <asp:TextBox ID="txtUsuario" runat="server"
                                                                CssClass="form-control" ClientIDMode="Static"
                                                                PlaceHolder="Ingrese nombre de usuario..."></asp:TextBox>
                                                        </div>
                                                        <!-- Form Group (password)-->
                                                        <div class="form-group">
                                                            <label class="small mb-1" for="txtPass">Contraseña</label>
                                                            <asp:TextBox ID="txtPass" runat="server"
                                                                CssClass="form-control" ClientIDMode="Static"
                                                                TextMode="Password" PlaceHolder="Contraseña"></asp:TextBox>
                                                        </div>
                                                        <asp:Button Class="btn btn-primary btn-block"
                                                        OnClick="btnLogin_OnClick"
                                                        ID="btnLogin" runat="server" Text="Ingresar" />
                                                    </div>                                                                                                        
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/jquery/jquery.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap/js/bootstrap.bundle.min.js") %>"></script>
</body>
</html>