﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Descargas.aspx.cs" Inherits="ReAl.GestionMedica.Web.Descargas" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Affiliate Dashboard - SB Admin Pro</title>

    <link rel="icon" type="image/x-icon" href="/assets/sbadmin2/img/favicon.png" />
</head>
<body class="nav-fixed">
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/css/sb-admin-2.css") %>" rel="stylesheet" />
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/fontawesome-free/js/all.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/feather/feather.min.js") %>"></script>
    <form id="form1" runat="server">
        <div id="layoutSidenav">
            <div id="layoutSidenav_content">
                <main>
                    <!-- Main page content-->
                    <div class="container mt-5">
                        <!-- Custom page header alternative example-->
                        <div class="d-flex justify-content-between align-items-sm-center flex-column flex-sm-row mb-4">
                            <div class="mr-4 mb-3 mb-sm-0">
                                <h1 class="mb-0">Descargas</h1>
                            </div>
                        </div>
                        <!-- Illustration dashboard card example-->
                        <div class="card card-waves mb-4 mt-5">
                            <div class="card-body p-5">
                                <div class="row align-items-center justify-content-between">
                                    <div class="col">
                                        <h2 class="text-primary">Bienvenido!</h2>
                                        <p class="text-gray-700">
                                            Neurodiagnóstico es un sistema integrado que le permite gestionar sus pacientes y los datos relacionados en cada consulta.
                                        </p>
                                        <a class="btn btn-primary p-3" href="Ingresar.aspx">Ingresar
                                            <i class="ml-1" data-feather="arrow-right"></i>
                                        </a>
                                    </div>
                                    <div class="col d-none d-lg-block mt-xxl-n4">
                                        <img class="img-fluid px-xl-4 mt-xxl-n5" src="<%= ResolveClientUrl("~/assets/sbadmin2/img/illustrations/statistics.svg") %>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-6 col-md-6 mb-4">
                                <!-- Dashboard info widget 1-->
                                <div class="card border-top-0 border-bottom-0 border-right-0 border-left-lg border-primary h-100">
                                    <div class="card-body">
                                        <div class="d-flex align-items-center">
                                            <div class="flex-grow-1">
                                                <div class="small font-weight-bold text-primary mb-1">GestionMedica x64</div>
                                                <div class="h5">Aplicación para Windows</div>
                                                <div class="text-xs font-weight-bold text-success d-inline-flex align-items-center">
                                                    <i class="mr-1" data-feather="download"></i>
                                                    <a href="<%= ResolveClientUrl("~/App_Downloads/GestionMedica.Setup.x64.msi") %>">Descargar</a>
                                                </div>
                                            </div>
                                            <div class="ml-2"><i class="fas fa-tag fa-2x text-gray-200"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-6 mb-4">
                                <!-- Dashboard info widget 2-->
                                <div class="card border-top-0 border-bottom-0 border-right-0 border-left-lg border-secondary h-100">
                                    <div class="card-body">
                                        <div class="d-flex align-items-center">
                                            <div class="flex-grow-1">
                                                <div class="small font-weight-bold text-secondary mb-1">GestionMédica x86</div>
                                                <div class="h5">Aplicación para Windows</div>
                                                <div class="text-xs font-weight-bold text-danger d-inline-flex align-items-center">
                                                    <i class="mr-1" data-feather="download"></i>
                                                    <a href="<%= ResolveClientUrl("~/App_Downloads/GestionMedica.Setup.x86.msi") %>">Descargar</a>
                                                </div>
                                            </div>
                                            <div class="ml-2"><i class="fas fa-tag fa-2x text-gray-200"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </div>
    </form>
    <!-- Bootstrap core JavaScript-->
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/jquery/jquery.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap/js/bootstrap.bundle.min.js") %>"></script>
    <!-- Core plugin JavaScript-->
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/js/sb-admin-2.js") %>"></script>
    <!-- Custom scripts for all pages-->
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/prism/js/prism-core.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/prism/js/prism-autoloader.min.js") %>"></script>
</body>
</html>