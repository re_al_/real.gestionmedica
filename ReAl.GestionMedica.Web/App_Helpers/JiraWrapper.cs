﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Web;
using Atlassian.Jira;
using ReAl.Utils;

namespace ReAl.GestionMedica.Web.App_Helpers
{
    /// <summary>
    /// Descripción breve de BitBucketWrapper
    /// </summary>
    public static class JiraWrapper
    {
        //KB: https://id.atlassian.com/manage-profile/security/api-tokens

        #region Fields

        private const string StrJiraPassword = "AlBZnoazZS24GYUqklmy5089";
        private const string StrJiraProject = "RFD";
        private const string StrJiraServer = "https://re-al.atlassian.net";
        private const string StrJiraUser = "re-al-@outlook.com";

        #endregion Fields

        #region Methods

        public static async void AddIssueOnJira(Exception exp)
        {
            try
            {
                var jira = Jira.CreateRestClient(StrJiraServer, StrJiraUser, StrJiraPassword);
                //var jiraUser = jira.Users.GetMyselfAsync();
                //var issue2 = await jira.Issues.GetIssueAsync("RFD-1");

                var issue = jira.CreateIssue(StrJiraProject);
                //issue.Type = "Bug";
                issue.Priority = "1";
                issue.Type = "10010";
                issue.Labels.Add("App");
                issue.Summary = exp.Message;
                issue.Description = CrearIssue(exp, false);

                await issue.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// Funcion para crear la descripcion del error en el gestor de incidencias
        /// </summary>
        /// <param name="exp">Excepcion orgien para el ticket</param>
        /// <returns>Cadena con la información basica del ticket. Esta en formato markdown</returns>
        public static string CrearIssue(Exception exp, bool bGuardaArchivo = true)
        {
            var trace = new StackTrace(exp, true);
            var stackFrame = trace.GetFrame(trace.FrameCount - 1);

            var strIssue = new StringBuilder();

            strIssue.AppendLine("*Nombre Aplicacion:* " + Assembly.GetExecutingAssembly() + Environment.NewLine);
            strIssue.AppendLine("*Nombre Equipo:* " + Environment.MachineName + Environment.NewLine);
            strIssue.AppendLine("*Nombre Dominio:* " + Environment.UserDomainName + Environment.NewLine);
            strIssue.AppendLine("*Nombre Usuario SO:* " + Environment.UserName + Environment.NewLine);

            /* TODO: Agregar al usuario
            if (CParamGoogleApi.GAppsUsuario == null)
                strIssue.AppendLine("*Nombre Usuario Aplicacion:* " + CParametrosApp.strCorreoExterno + Environment.NewLine);
            else
                strIssue.AppendLine("*Nombre Usuario Aplicacion:* " + CParamGoogleApi.GAppsUsuario.Email);
            */

            strIssue.AppendLine("---------------------------------------" + Environment.NewLine);
            var strBuildTime =
                new DateTime(2000, 1, 1).AddDays(
                    Assembly.GetExecutingAssembly().GetName().Version.Build).ToShortDateString();
            var timeSpanProcTime = Process.GetCurrentProcess().TotalProcessorTime;

            strIssue.AppendLine("*Fecha y Hora del Error:* " + DateTime.Now + Environment.NewLine);
            strIssue.AppendLine("*Fecha y Hora de Ejecucion:* " + Process.GetCurrentProcess().StartTime +
                                Environment.NewLine);
            strIssue.AppendLine("*Fecha del Build:* " + strBuildTime + Environment.NewLine);
            strIssue.AppendLine("*Sistema Operativo:* " + Environment.OSVersion.VersionString + Environment.NewLine);
            strIssue.AppendLine("*Plataforma:* " + Environment.OSVersion.Platform + Environment.NewLine);
            strIssue.AppendLine("*Tiempo de Ejecucion de la Aplicacion:* " +
                                $"{timeSpanProcTime.TotalHours:0} hours {timeSpanProcTime.TotalMinutes:0} mins {timeSpanProcTime.TotalSeconds:0} secs" +
                                Environment.NewLine);
            strIssue.AppendLine("*PID:* " + Process.GetCurrentProcess().Id + Environment.NewLine);
            strIssue.AppendLine("*Thread Count:* " + Process.GetCurrentProcess().Threads.Count + Environment.NewLine);
            strIssue.AppendLine("*Thread Id:* " + Thread.CurrentThread.ManagedThreadId +
                                Environment.NewLine);
            strIssue.AppendLine("*Nombre del Proceso:* " + Process.GetCurrentProcess().ProcessName +
                                Environment.NewLine);
            strIssue.AppendLine("*CLR Version:* " + Environment.Version + Environment.NewLine);
            strIssue.AppendLine("---------------------------------------" + Environment.NewLine);

            var ex = exp;
            for (var j = 0; ex != null; ex = ex.InnerException, j++)
            {
                strIssue.AppendLine("*Tipo #" + j + ":* " + ex.GetType() + Environment.NewLine);
                if (!string.IsNullOrEmpty(ex.Message))
                    strIssue.AppendLine("*Mensaje #" + j + ":* " + ex.Message + Environment.NewLine);
                if (!string.IsNullOrEmpty(ex.Source))
                    strIssue.AppendLine("*Origen #" + j + ":* " + ex.Source + Environment.NewLine);
                if (!string.IsNullOrEmpty(ex.HelpLink))
                    strIssue.AppendLine("*Enlace de ayuda #" + j + ":* " + ex.HelpLink + Environment.NewLine);
                if (ex.TargetSite != null)
                    strIssue.AppendLine("*Target Site #" + j + ":* " + ex.TargetSite + Environment.NewLine);
                foreach (DictionaryEntry de in ex.Data)
                    strIssue.AppendLine("*" + de.Key + ":* " + de.Value + Environment.NewLine);
                strIssue.AppendLine("---------------------------------------" +
                                    Environment.NewLine);
            }

            strIssue.AppendLine("*StackTrace:*" + Environment.NewLine);
            strIssue.AppendLine(exp.StackTrace + Environment.NewLine);

            strIssue.AppendLine("---------------------------------------" + Environment.NewLine);
            strIssue.AppendLine("*Source:*" + Environment.NewLine);
            strIssue.AppendLine(exp.Source + Environment.NewLine);

            strIssue.AppendLine("---------------------------------------" + Environment.NewLine);
            strIssue.AppendLine("*File Name:* " + stackFrame.GetFileName() + Environment.NewLine);
            strIssue.AppendLine("*Class Name:* " + stackFrame.GetMethod().DeclaringType + Environment.NewLine);
            strIssue.AppendLine("*Method Name:* " + stackFrame.GetMethod().Name + Environment.NewLine);
            strIssue.AppendLine("*Line Number:* " + stackFrame.GetFileLineNumber() + Environment.NewLine);
            strIssue.AppendLine("*Column Number:* " + stackFrame.GetFileColumnNumber() + Environment.NewLine);

            //Se almacena el documento
            if (bGuardaArchivo)
            {
                string errorDirectory = HttpRuntime.AppDomainAppPath + "App_Logs\\";

                if (!Directory.Exists(errorDirectory))
                    Directory.CreateDirectory(errorDirectory);

                cFuncionesFicheros.GuardarArchivo(
                    errorDirectory + "Log_" + DateTime.Now.ToString("yyyyMMdd_HHmmss_ffffff") + ".md",
                    strIssue.ToString());
            }

            return strIssue.ToString();
        }

        #endregion Methods
    }
}