﻿using System;
using System.Collections;
using System.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Oauth2.v2.Data;
using ReAl.GestionMedica.Class.Entidades;

namespace ReAl.GestionMedica.Web.App_Helpers
{
    /// <summary>
    /// Descripción breve de SessionHandler
    /// </summary>
    public class SessionHandler : System.Web.UI.Page
    {
        #region Properties

        public EntCalCitas AppCita
        {
            get
            {
                var sessionData = (EntCalCitas)Session["CalCitas"];
                return sessionData;
            }
            set
            {
                Session["CalCitas"] = value;
            }
        }

        public Exception AppException
        {
            get
            {
                var sessionData = (Exception)Session["AppException"];
                return sessionData;
            }
            set
            {
                Session["AppException"] = value;
            }
        }

        public EntPacPacientes AppPaciente
        {
            get
            {
                var sessionData = (EntPacPacientes)Session["PacPacientes"];
                return sessionData;
            }
            set
            {
                Session["PacPacientes"] = value;
            }
        }

        public EntSegUsuariosrestriccion AppRestriccion
        {
            get
            {
                var sessionData = (EntSegUsuariosrestriccion)Session["SegUsuariorestriccion"];
                return sessionData;
            }
            set
            {
                Session["SegUsuariorestriccion"] = value;
            }
        }

        public EntSegRoles AppRol
        {
            get
            {
                var sessionData = (EntSegRoles)Session["SegRol"];
                return sessionData;
            }
            set
            {
                Session["SegRol"] = value;
            }
        }

        public EntSegUsuarios AppUsuario
        {
            get
            {
                var sessionData = (EntSegUsuarios)Session["SegUsuario"];
                return sessionData;
            }
            set
            {
                Session["SegUsuario"] = value;
            }
        }

        public ArrayList ArrMenu
        {
            get
            {
                ArrayList sessionData = (ArrayList)Session["arrMenu"];
                return sessionData;
            }
            set
            {
                Session["arrMenu"] = value;
            }
        }

        public DataTable DtMenu
        {
            get
            {
                DataTable sessionData = (DataTable)Session["dtMenu"];
                return sessionData;
            }
            set
            {
                Session["dtMenu"] = value;
            }
        }

        public UserCredential GAppsCredential
        {
            get
            {
                UserCredential sessionData = (UserCredential)Session["GAppsCredential"];
                return sessionData;
            }
            set
            {
                Session["GAppsCredential"] = value;
            }
        }

        public Userinfo GAppsUsuario
        {
            get
            {
                Userinfo sessionData = (Userinfo)Session["GAppsUsuario"];
                return sessionData;
            }
            set
            {
                Session["GAppsUsuario"] = value;
            }
        }

        #endregion Properties

        #region Methods

        /// <summary>
        /// Funcion para eliminar las variables de sesion
        /// </summary>
        public void SessionClear()
        {
            DtMenu = null;
            ArrMenu = null;
            AppUsuario = null;
            AppRestriccion = null;
            AppRol = null;
            AppException = null;
        }

        #endregion Methods
    }
}