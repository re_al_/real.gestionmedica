﻿using System;
using System.Collections;
using System.Data;
using DevExpress.Web;
using DevExpress.Web.ASPxScheduler.Dialogs;
using DevExpress.Web.ASPxScheduler.Internal;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;

namespace ReAl.GestionMedica.Web.App_Helpers
{
    public class CustomAppointmentPacEditDialogViewModel : AppointmentEditDialogViewModel<CustomAppointmentPacEditDialogViewModel>
    {
        /*
        [DialogFieldViewSettings(Caption = "Paciente", EditorType = DialogFieldEditorType.ComboBox)]
        public string idppa { get; set; }
        */

        #region Methods

        public override void Load(AppointmentFormController appointmentController)
        {
            try
            {
                base.Load(appointmentController);

                var miSesion = new SessionHandler();
                var rn = new RnVista();
                var arrParamName = new ArrayList();
                arrParamName.Add(EntPacPacientes.Fields.cippa.ToString());
                var arrParamValue = new ArrayList();
                arrParamValue.Add(miSesion.AppUsuario.loginsus.ToString());
                var dtPacientes = rn.ObtenerDatosProcAlm("spppaselcmbci", arrParamName, arrParamValue);

                SetEditorTypeFor(m => m.Location, DialogFieldEditorType.ComboBox);
                SetDataItemsFor(m => m.Location, (addItemDelegate) =>
                {
                    addItemDelegate("Consultorio", "Consultorio");
                    addItemDelegate("Clinica", "Clinica");
                });

                SetEditorTypeFor(m => m.StatusKey, DialogFieldEditorType.ComboBox);
                SetDataItemsFor(m => m.StatusKey, (addItemDelegate) =>
                {
                    foreach (DataRow row in dtPacientes.Rows)
                    {
                        addItemDelegate(row["NombreCompleto"].ToString(), row["idppa"].ToString());
                    }
                });

                SetEditorTypeFor(m => m.LabelKey, DialogFieldEditorType.ComboBox);
                SetDataItemsFor(m => m.LabelKey, (addItemDelegate) =>
                {
                    addItemDelegate("Solicitado", "0");
                    addItemDelegate("No se presento", "1");
                    addItemDelegate("Programado", "2");
                    addItemDelegate("Atendido", "3");
                });

                //SetEditorVisibilityCondition(m => m.LabelKey, false);
                if (IsNewAppointment)
                {
                    //Valores por defecto para nuevos
                    StatusKey = miSesion.AppUsuario.idsus.ToString();
                    LabelKey = "0";
                    Location = "Consultorio";

                    SetEditorEnabledCondition(m => m.StatusKey, false);
                    SetEditorEnabledCondition(m => m.LabelKey, false);
                    SetEditorEnabledCondition(m => m.Location, false);
                }
                else
                {
                    string strStatus = appointmentController.StatusKey.ToString();
                    StatusKey = strStatus.ToString();
                    string strLabel = appointmentController.LabelKey.ToString();
                    LabelKey = strLabel.ToString();

                    SetEditorEnabledCondition(m => m.StatusKey, false);
                    SetEditorEnabledCondition(m => m.StartTime, false);
                    SetEditorEnabledCondition(m => m.EndTime, false);
                    SetEditorEnabledCondition(m => m.LabelKey, false);
                    SetEditorEnabledCondition(m => m.Location, false);

                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp);
            }
        }

        #endregion Methods
    }
}