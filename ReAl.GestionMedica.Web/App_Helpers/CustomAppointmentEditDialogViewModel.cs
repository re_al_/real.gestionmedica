﻿using System;
using System.Data;
using DevExpress.Web;
using DevExpress.Web.ASPxScheduler.Dialogs;
using DevExpress.Web.ASPxScheduler.Internal;
using ReAl.GestionMedica.Class.Modelo;

namespace ReAl.GestionMedica.Web.App_Helpers
{
    public class CustomAppointmentEditDialogViewModel : AppointmentEditDialogViewModel<CustomAppointmentEditDialogViewModel>
    {
        /*
        [DialogFieldViewSettings(Caption = "Paciente", EditorType = DialogFieldEditorType.ComboBox)]
        public string idppa { get; set; }
        */

        #region Methods

        public override void Load(AppointmentFormController appointmentController)
        {
            try
            {
                base.Load(appointmentController);

                var rn = new RnVista();
                var dtPacientes = rn.ObtenerDatosProcAlm("spppaselcmb");

                SetEditorTypeFor(m => m.Location, DialogFieldEditorType.ComboBox);
                SetDataItemsFor(m => m.Location, (addItemDelegate) =>
                {
                    addItemDelegate("Consultorio", "Consultorio");
                    addItemDelegate("Clinica", "Clinica");
                });

                SetEditorTypeFor(m => m.StatusKey, DialogFieldEditorType.ComboBox);
                SetDataItemsFor(m => m.StatusKey, (addItemDelegate) =>
                {
                    foreach (DataRow row in dtPacientes.Rows)
                    {
                        addItemDelegate(row["NombreCompleto"].ToString(), row["idppa"].ToString());
                    }
                });

                SetEditorTypeFor(m => m.LabelKey, DialogFieldEditorType.ComboBox);
                SetDataItemsFor(m => m.LabelKey, (addItemDelegate) =>
                {
                    addItemDelegate("Solicitado", "0");
                    addItemDelegate("No se presento", "1");
                    addItemDelegate("Programado", "2");
                    addItemDelegate("Atendido", "3");
                });

                //SetEditorVisibilityCondition(m => m.LabelKey, false);
                if (IsNewAppointment)
                {
                    //Valores por defecto para nuevos
                    LabelKey = "0";
                    Location = "Consultorio";
                }
                else
                {
                    string strAux = appointmentController.StatusKey.ToString();
                    StatusKey = strAux.ToString();
                    string strAux2 = appointmentController.LabelKey.ToString();
                    LabelKey = strAux2.ToString();

                    SetEditorEnabledCondition(m => m.StatusKey, false);
                    SetEditorEnabledCondition(m => m.StartTime, false);
                    SetEditorEnabledCondition(m => m.EndTime, false);
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp);
            }
        }

        #endregion Methods
    }
}