using System;
using System.Text.RegularExpressions;
using System.Web;
using ReAl.Utils;

namespace ReAl.GestionMedica.Web.App_Helpers
{
    public static class RtfHelper
    {
        #region Methods

        public static string HtmlToPlainText(string html)
        {
            const string tagWhiteSpace = @"(>|$)(\W|\n|\r)+<";//matches one or more (white space or line breaks) between '>' and '<'
            const string stripFormatting = @"<[^>]*(>|$)";//match any character between '<' and '>', even when end tag is missing
            const string lineBreak = @"<(br|BR)\s{0,1}\/{0,1}>";//matches: <br>,<br/>,<br />,<BR>,<BR/>,<BR />

            var lineBreakRegex = new Regex(lineBreak, RegexOptions.Multiline);
            var stripFormattingRegex = new Regex(stripFormatting, RegexOptions.Multiline);
            var tagWhiteSpaceRegex = new Regex(tagWhiteSpace, RegexOptions.Multiline);

            var text = html;
            //Decode html specific characters
            text = System.Net.WebUtility.HtmlDecode(text);
            //Remove tag whitespace/line breaks
            text = tagWhiteSpaceRegex.Replace(text, "><");
            //Replace <br /> with line breaks
            text = lineBreakRegex.Replace(text, Environment.NewLine + Environment.NewLine);
            //Strip formatting
            text = stripFormattingRegex.Replace(text, string.Empty);

            //Tabs
            text = text.Replace("\t", "  ");

            return text;
        }

        public static byte[] TextoToRtf(HttpServerUtility Server, string strTexto)
        {
            //return new byte[10];
            RicherTextBox.RicherTextBox txtAux = new RicherTextBox.RicherTextBox();
            txtAux.Rtf = HtmlToPlainText(strTexto);
            txtAux.rtbDocument.SaveFile(Server.MapPath("~/Uploads/temp.rtf"));
            var rtf = cFuncionesImagenes.ImageReadBinaryFile(Server.MapPath("~/Uploads/temp.rtf"));
            return rtf;
        }

        #endregion Methods
    }
}