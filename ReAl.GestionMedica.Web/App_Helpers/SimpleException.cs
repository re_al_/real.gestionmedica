﻿using System;
using System.Runtime.Serialization;

namespace ReAl.GestionMedica.Web.App_Helpers
{
    [Serializable]
    public class SimpleException : Exception
    {
        #region Constructors

        public SimpleException()
        { }

        public SimpleException(string message)
            : base(message) { }

        public SimpleException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public SimpleException(string message, Exception innerException)
            : base(message, innerException) { }

        public SimpleException(string format, Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

        protected SimpleException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }

        #endregion Constructors
    }
}