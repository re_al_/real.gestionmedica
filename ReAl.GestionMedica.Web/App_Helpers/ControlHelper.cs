using System.Web.UI.WebControls;

namespace ReAl.GestionMedica.Web.App_Helpers
{
    public static class ControlHelper
    {
        #region Methods

        /// <summary>
        /// Funcion que se encarga de implementar los estilos del plugin js http://bootstrap-table.wenzhixin.net.cn
        /// </summary>
        /// <param name="dtgListado">GridView en el que se implementaran los estilos</param>
        /// <param name="bShowExport">Valor que indica si se mostrara el boton de exportar</param>
        /// <param name="bShowFilter">Valor quye indica si se muestran cabeceras de filtro</param>
        /// <param name="bAplyStyles">Valor quye indica si se aplican estilos</param>
        public static void CrearEstilosGrid(ref GridView dtgListado, bool bShowExport = true, bool bShowFilter = false, bool bAplyStyles = true, bool bStickyHeader = false, string strColumnaColor = "", string strFuncionJsColor = "")
        {
            if (bShowExport)
            {
                dtgListado.Attributes.Add("data-show-export", "true");
                dtgListado.Attributes.Add("data-export-types", "['csv','txt','doc','excel']");
            }
            if (bShowFilter == true)
            {
                dtgListado.Attributes.Add("data-filter-control", "true");
                dtgListado.Attributes.Add("data-filter-show-clear", "true");
                dtgListado.Attributes.Add("data-search", "false");
            }
            if (bStickyHeader)
            {
                dtgListado.Attributes.Add("data-sticky-header", "true");
                dtgListado.Attributes.Add("data-sticky-header-offset-y", "51px");
            }

            if (bAplyStyles)
                if (dtgListado.Rows.Count > 0)
                {
                    dtgListado.HeaderRow.TableSection = TableRowSection.TableHeader;
                    dtgListado.HeaderRow.Attributes.Add("class", "th_background_1");

                    if (dtgListado.Columns.Count == 0)
                    {
                        for (var i = 0; i < dtgListado.Rows[0].Cells.Count; i++)
                        {
                            string strCabecera = dtgListado.HeaderRow.Cells[i].Text;
                            if (strCabecera == "ACCIONES")
                            {
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-searchable", "false");
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-sortable", "false");
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-width", "150px");
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-align", "center");
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-valign", "middle");
                            }
                            else
                            {
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-valign", "middle");
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-sortable", "true");
                                if (bShowFilter == true)
                                {
                                    dtgListado.HeaderRow.Cells[i].Attributes.Add("data-filter-control", "input");
                                    dtgListado.HeaderRow.Cells[i].Attributes.Add("data-filter-control-placeholder",
                                        dtgListado.Columns[i].HeaderText);
                                }
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-field", strCabecera);
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-title-tooltip", strCabecera);
                                if (strCabecera.EndsWith("%") || strCabecera.StartsWith("%"))
                                {
                                    dtgListado.HeaderRow.Cells[i].Attributes.Add("data-cell-style", "cellStyle");
                                    dtgListado.HeaderRow.Cells[i].Attributes.Add("data-halign", "right");
                                    dtgListado.HeaderRow.Cells[i].Attributes.Add("data-align", "right");
                                }
                                if (!string.IsNullOrEmpty(strColumnaColor) && strCabecera.Contains(strColumnaColor))
                                {
                                    dtgListado.HeaderRow.Cells[i].Attributes.Add("data-cell-style", strFuncionJsColor);
                                    dtgListado.HeaderRow.Cells[i].Attributes.Add("data-halign", "center");
                                    dtgListado.HeaderRow.Cells[i].Attributes.Add("data-align", "center");
                                }
                            }
                        }
                    }
                    else
                    {
                        for (var i = 0; i < dtgListado.Columns.Count; i++)
                        {
                            if (dtgListado.Columns[i].HeaderText.ToUpper() == "ACCIONES")
                            {
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-searchable", "false");
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-sortable", "false");
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-width", "150px");
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-align", "center");
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-valign", "middle");
                            }
                            else
                            {
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-valign", "middle");
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-sortable", "true");
                                if (bShowFilter == true)
                                {
                                    dtgListado.HeaderRow.Cells[i].Attributes.Add("data-filter-control", "input");
                                    dtgListado.HeaderRow.Cells[i].Attributes.Add("data-filter-control-placeholder",
                                        dtgListado.Columns[i].HeaderText);
                                }
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-field", dtgListado.Columns[i].HeaderText);
                                dtgListado.HeaderRow.Cells[i].Attributes.Add("data-title-tooltip", dtgListado.Columns[i].HeaderText);

                                string strCabecera = dtgListado.HeaderRow.Cells[i].Text;
                                if (strCabecera.EndsWith("%") || strCabecera.StartsWith("%"))
                                {
                                    if (!string.IsNullOrEmpty(dtgListado.HeaderRow.Cells[i].Text))
                                    {
                                        dtgListado.HeaderRow.Cells[i].Attributes.Add("data-cell-style", "cellStyle");
                                        dtgListado.HeaderRow.Cells[i].Attributes.Add("data-halign", "right");
                                        dtgListado.HeaderRow.Cells[i].Attributes.Add("data-align", "right");
                                    }
                                }
                                if (!string.IsNullOrEmpty(strColumnaColor) && strCabecera.Contains(strColumnaColor))
                                {
                                    dtgListado.HeaderRow.Cells[i].Attributes.Add("data-cell-style", strFuncionJsColor);
                                    dtgListado.HeaderRow.Cells[i].Attributes.Add("data-halign", "center");
                                    dtgListado.HeaderRow.Cells[i].Attributes.Add("data-align", "center");
                                }
                            }
                        }
                    }
                }
        }

        #endregion Methods
    }
}