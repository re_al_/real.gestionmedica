﻿using System.Collections;
using System.Data;
using ReAl.GestionMedica.Class.Modelo;

namespace ReAl.GestionMedica.Web.App_Helpers
{
    public class DetailsBuilder
    {
        #region Methods

        public DataSet ObtenerReporteSp(string strProcAlm, ArrayList arrNomParam, ArrayList arrParam, ArrayList arrHide)
        {
            var rn = new RnVista();
            var dtPopUp = rn.ObtenerDatosProcAlm(strProcAlm, arrNomParam, arrParam);
            dtPopUp.Columns[0].ColumnName = "id";

            var dtListado = dtPopUp.Copy();

            foreach (string strColumna in arrHide)
            {
                dtListado.Columns.Remove(strColumna);
            }

            var dsRpt = new DataSet();
            dsRpt.Tables.Add(dtPopUp);
            dsRpt.Tables.Add(dtListado);

            return dsRpt;
        }

        public DataSet ObtenerReporteVista(string strVista, ArrayList arrNomParam, ArrayList arrParam, ArrayList arrHide)
        {
            var rn = new RnVista();
            var dtPopUp = rn.ObtenerDatos(strVista, arrNomParam, arrParam);
            dtPopUp.Columns[0].ColumnName = "id";

            var dtListado = dtPopUp.Copy();

            foreach (string strColumna in arrHide)
            {
                dtListado.Columns.Remove(strColumna);
            }

            var dsRpt = new DataSet();
            dsRpt.Tables.Add(dtPopUp);
            dsRpt.Tables.Add(dtListado);

            return dsRpt;
        }

        public DataSet ObtenerReporteVista(DataTable dtPopUp, ArrayList arrHide)
        {
            var dtListado = dtPopUp.Copy();

            foreach (string strColumna in arrHide)
            {
                dtListado.Columns.Remove(strColumna);
            }

            var dsRpt = new DataSet();
            dsRpt.Tables.Add(dtPopUp);
            dsRpt.Tables.Add(dtListado);

            return dsRpt;
        }

        #endregion Methods
    }
}