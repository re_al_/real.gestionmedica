using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Web;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Web;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Oauth2.v2;
using Google.Apis.Oauth2.v2.Data;
using Google.Apis.PeopleService.v1;
using Google.Apis.PeopleService.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using Event = Google.Apis.Calendar.v3.Data.Event;
using File = Google.Apis.Drive.v3.Data.File;

namespace ReAl.GestionMedica.Web.App_Helpers
{
    public class CGoogleApi
    {
        #region Fields

        private const string GoogleClientId = "1077726740916-rkjss1ipce3lnrke7b8c7uhjggd10g8g.apps.googleusercontent.com";
        private const string GoogleSecretKey = "-A9s1WkQHtt2fCcmaNy0GPIE";
        private const string NombreCarpeta = "GestionMedica";

        #endregion Fields

        #region Methods

        public string BuscarCarpeta(DriveService service, string strNombreCarpeta)
        {
            FilesResource.ListRequest request = service.Files.List();
            request.Q = "mimeType = 'application/vnd.google-apps.folder' and name = '" + strNombreCarpeta + "'";
            var response = request.Execute();
            return response.Files.Count == 1 ? response.Files[0].Id : string.Empty;
        }

        public string CrearCarpeta(DriveService service, string strNombreCarpeta)
        {
            var fileMetadata = new File { Name = strNombreCarpeta, MimeType = "application/vnd.google-apps.folder" };

            var request = service.Files.Create(fileMetadata);
            request.Fields = "id";
            var file = request.Execute();
            return file.Id;
        }

        public Event CrearEvento(CalendarService service, DateTime inicio, DateTime final, string subject,
                    string description)
        {
            var miEvento = new Event();
            miEvento.Summary = subject;
            miEvento.Description = description;

            EventDateTime start = new EventDateTime();
            start.DateTime = inicio;
            miEvento.Start = start;

            EventDateTime finish = new EventDateTime();
            finish.DateTime = final;
            miEvento.End = finish;

            String calendarId = "primary";
            EventsResource.InsertRequest request = service.Events.Insert(miEvento, calendarId);

            Event createdEvent = request.Execute();
            Console.WriteLine("Event created: {0}", createdEvent.HtmlLink);

            return createdEvent;
        }

        public CalendarService CrearServicioCalendar(UserCredential credential)
        {
            // Create Drive API service.
            var service = new CalendarService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = CParamGoogleApi.ApplicationName,
            });
            return service;
        }

        /// <summary>
        /// Create Drive API service.
        /// </summary>
        /// <returns></returns>
        public DriveService CrearServicioDrive(UserCredential credential)
        {
            // Create Drive API service.
            var service = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = CParamGoogleApi.ApplicationName,
            });
            return service;
        }

        public GmailService CrearServicioGmail(UserCredential credential)
        {
            // Create People API service.
            var service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = CParamGoogleApi.ApplicationName,
            });
            return service;
        }

        /// <summary>
        /// Create Drive API service.
        /// </summary>
        /// <returns></returns>
        public PeopleServiceService CrearServicioPeople(UserCredential credential)
        {
            // Create People API service.
            var service = new PeopleServiceService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = CParamGoogleApi.ApplicationName,
            });
            return service;
        }

        public void DescargarArchivo(DriveService service, string strFileId, string strPathNombre)
        {
            var request = service.Files.Get(strFileId);
            var strNombre = service.Files.Get(strFileId).Execute().Name;
            var streamDownload = new System.IO.MemoryStream();

            Console.WriteLine("Descag¿rgando archivo con ID" + strFileId + " - " + request);

            // Add a handler which will be notified on progress changes.
            // It will notify on each chunk download and when the
            // download is completed or failed.
            request.MediaDownloader.ProgressChanged += (Google.Apis.Download.IDownloadProgress progress) =>
            {
                switch (progress.Status)
                {
                    case Google.Apis.Download.DownloadStatus.Downloading:
                        {
                            Console.WriteLine(progress.BytesDownloaded);
                            break;
                        }
                    case Google.Apis.Download.DownloadStatus.Completed:
                        {
                            Console.WriteLine("Download complete.");
                            //cFuncionesFicheros.GuardarStream(streamDownload, strPathNombre + strNombre);
                            break;
                        }
                    case Google.Apis.Download.DownloadStatus.Failed:
                        {
                            Console.WriteLine("Download failed.");
                            break;
                        }
                }
            };
            request.Download(streamDownload);
        }

        public Message EnviarCorreo(string strTo, string trSubject, string strBodyText, GmailService service)
        {
            var plainText = "To: " + strTo + "\r\n" +
                            "Subject: " + trSubject + "\r\n" +
                            "Content-Type: text/html; charset=us-ascii\r\n\r\n" +
                            strBodyText;

            var newMsg = new Google.Apis.Gmail.v1.Data.Message { Raw = Base64UrlEncode(plainText.ToString()) };

            var respuesta = service.Users.Messages.Send(newMsg, "me").Execute();
            Console.WriteLine($"Respuesta: " + respuesta.Id);
            Console.WriteLine($"Respuesta: " + respuesta);
            return respuesta;
        }

        public Permission InsertPermission(DriveService service, String fileId, String who, String type, String role)
        {
            Permission newPermission = new Permission();
            newPermission.Type = type;
            newPermission.Role = role;
            newPermission.EmailAddress = who;
            try
            {
                return service.Permissions.Create(newPermission, fileId).Execute();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
            }
            return null;
        }

        public IList<Google.Apis.Drive.v3.Data.File> ListarArchivos(DriveService service)
        {
            // Define parameters of request.
            var listRequest = service.Files.List();
            listRequest.PageSize = 100;
            listRequest.Fields = "nextPageToken, files(id, name)";

            // List files.
            var files = listRequest.Execute()
                .Files;
            Console.WriteLine("Files:");
            if (files != null && files.Count > 0)
            {
                foreach (var file in files)
                {
                    Console.WriteLine("{0} ** {1} ** {2} ({3})", file.Name, file.OriginalFilename, file.Kind, file.Id);
                }
            }
            else
            {
                Console.WriteLine("No files found.");
            }

            return files;
        }

        public IList<Person> ListarContactos(PeopleServiceService service)
        {
            var peopleRequest =
                service.People.Connections.List("people/me");
            peopleRequest.PersonFields = "names,emailAddresses";
            peopleRequest.PageSize = 2000;
            peopleRequest.SortOrder = PeopleResource.ConnectionsResource.ListRequest.SortOrderEnum.FIRSTNAMEASCENDING;
            var connectionsResponse = peopleRequest.Execute();
            var connections = connectionsResponse.Connections;
            return connections;
        }

        public List<Event> ListarEventos(CalendarService service)
        {
            List<Event> misEventos = new List<Event>();

            // Define parameters of request.
            EventsResource.ListRequest request = service.Events.List("primary");
            request.TimeMin = DateTime.Now;
            request.ShowDeleted = false;
            request.SingleEvents = true;
            request.MaxResults = 20;
            request.OrderBy = EventsResource.ListRequest.OrderByEnum.StartTime;

            // List events.
            Events events = request.Execute();
            if (events.Items != null && events.Items.Count > 0)
            {
                foreach (var eventItem in events.Items)
                {
                    string when = eventItem.Start.DateTime.ToString();
                    if (String.IsNullOrEmpty(when))
                    {
                        when = eventItem.Start.Date;
                    }
                    misEventos.Add(eventItem);
                }
            }

            return misEventos;
        }

        public UserCredential ObtenerCrearCredencialDesktop(string strPath, string strCredentialName)
        {
            try
            {
                if (!Directory.Exists(strPath))
                {
                    Directory.CreateDirectory(strPath);
                }
                CancellationTokenSource cts = new CancellationTokenSource();
                cts.CancelAfter(TimeSpan.FromSeconds(20));
                CancellationToken ct = cts.Token;

                UserCredential credential;
                using (var stream =
                    new FileStream(strPath + "/" + "credentials.json", FileMode.Open, FileAccess.Read))
                {
                    // The file token.json stores the user's access and refresh tokens, and is created
                    // automatically when the authorization flow completes for the first time.
                    var credPath = strPath + "/" + strCredentialName + "_token.json";
                    credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                        GoogleClientSecrets.Load(stream).Secrets,
                        CParamGoogleApi.Scopes,
                        "user",
                        ct, //CancellationToken.None,
                        new FileDataStore(credPath, true)).Result;
                    Console.WriteLine($"Credential file saved to: " + credPath);
                }

                if (ct.IsCancellationRequested) return null;

                return credential;
            }
            catch (Exception e)
            {
                if (!Directory.Exists(strPath))
                {
                    Directory.Delete(strPath);
                }
            }

            return null;
        }

        public UserCredential ObtenerCrearCredencialWeb(string strPath, string strCredentialName)
        {
            GoogleAuthorizationCodeFlow flow;
            using (var stream =
                new FileStream(strPath + "/" + "credentials-web.json", FileMode.Open, FileAccess.Read))
            {
                var credPath = strPath + "/" + strCredentialName + "_token.json";
                flow =
                    new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
                    {
                        ClientSecrets = GoogleClientSecrets.Load(stream).Secrets,
                        Scopes = CParamGoogleApi.Scopes,
                        DataStore = new FileDataStore(credPath, true)
                    });
            }

            var result = new AuthorizationCodeWebApp(flow, "", "").AuthorizeAsync(strCredentialName,
                CancellationToken.None).Result;

            return result.Credential;
        }

        public Userinfo ObtenerUsuario(UserCredential credential)
        {
            //Para el usuario
            var oauthSerivce = new Oauth2Service(new BaseClientService.Initializer { HttpClientInitializer = credential });
            //CParamGoogleApi.GAppsUsuario =  oauthSerivce.Userinfo.Get().Execute();
            var usuario = oauthSerivce.Userinfo.Get().Execute();

            Console.WriteLine($"Usuario: " + credential.UserId);
            Console.WriteLine($"Usuario: " + usuario);
            return usuario;
        }

        public File SubirArchivo(DriveService service, FileInfo fileArchivo, string strDescripcion)
        {
            //Verificamos si existe la carpeta
            var strFolderId = BuscarCarpeta(service, NombreCarpeta);
            if (string.IsNullOrEmpty(strFolderId))
                strFolderId = CrearCarpeta(service, NombreCarpeta);

            //var fileMetadata = new Google.Apis.Drive. File();
            var fileMetadata = new File()
            {
                Name = fileArchivo.Name,
                Description = strDescripcion,
                Parents = new List<string> { strFolderId }
            };
            FilesResource.CreateMediaUpload request;
            using (var stream = new FileStream(fileArchivo.FullName, FileMode.Open))
            {
                request = service.Files.Create(fileMetadata, stream, MimeMapping.GetMimeMapping(fileArchivo.FullName));
                request.Fields = "id, webViewLink, size";
                request.Upload();
            }
            var file = request.ResponseBody;
            Console.WriteLine("File ID: " + file.Id);
            Console.WriteLine("File ID: " + file.WebViewLink);
            return file;
        }

        public bool VerificarUsuario(string strPath, string strCredentialName)
        {
            if (!Directory.Exists(strPath))
            {
                Directory.CreateDirectory(strPath);
            }
            var credPath = strPath + "/" + strCredentialName + "_token.json";

            return Directory.Exists(credPath);
        }

        private string Base64UrlEncode(string input)
        {
            var inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            return Convert.ToBase64String(inputBytes).Replace("+", "-").Replace("/", "_").Replace("=", "");
        }

        #endregion Methods
    }
}