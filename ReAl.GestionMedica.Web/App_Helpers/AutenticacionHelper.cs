﻿using System;
using System.Collections;
using System.Data;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.Utils;

namespace ReAl.GestionMedica.Web.App_Helpers
{
    /// <summary>
    /// Descripción breve de AutenticacionHelper
    /// </summary>
    public class AutenticacionHelper
    {
        #region Methods

        public static bool AutenticarUsuario(ref string strValidacion, string strUsuaario, string strPass, ref EntSegUsuarios miUsuario, ref EntSegUsuariosrestriccion miUsuarioRestriccion, ref EntSegRoles miRol)
        {
            bool bProcede = false;
            try
            {
                miUsuario = new EntSegUsuarios();

                var rnUsuarioVerificacion = new RnSegUsuarios();

                var arrNombreParametros = new ArrayList();
                arrNombreParametros.Add("UPPER(" + EntSegUsuarios.Fields.loginsus + ")");
                var arrParametros = new ArrayList();
                arrParametros.Add("UPPER('" + strUsuaario + "')");

                var eUsuario = rnUsuarioVerificacion.ObtenerObjeto(arrNombreParametros, arrParametros);

                if (eUsuario != null)
                {
                    if (eUsuario.passsus.ToUpper() == cFuncionesEncriptacion.GenerarMd5(strPass).ToUpper())
                    {
                        if (eUsuario.vigentesus == 1)
                        {
                            var dtFechaVigente = (eUsuario.fechavigentesus == null
                                                           ? DateTime.Now
                                                           : (DateTime)eUsuario.fechavigentesus);
                            if (cFuncionesFechas.DateDiff(dtFechaVigente, DateTime.Now, "dd") >= 0)
                            {
                                //Obtenemos la lista de grupos o roles
                                var rnRestriccion = new RnSegUsuariosrestriccion();
                                var rnRol = new RnSegRoles();

                                var listaRoles =
                                    rnRestriccion.ObtenerLista(
                                        EntSegUsuariosrestriccion.Fields.idsus, "'" + eUsuario.idsus + "'");

                                //Verificamos que existan roles
                                if (listaRoles.Count > 0)
                                {
                                    foreach (var rolRestriccion in listaRoles)
                                    {
                                        if (rolRestriccion.rolactivosur == 1)
                                        {
                                            miUsuarioRestriccion = rolRestriccion;
                                            miRol = rnRol.ObtenerObjeto(rolRestriccion.rolsro);
                                        }
                                    }

                                    //Verificamos que exista un Rol Activo
                                    if (miRol == null)
                                    {
                                        strValidacion = "No existe un Rol Activo para este usuario";
                                        bProcede = false;
                                    }
                                    else
                                    {
                                        miUsuario = eUsuario;
                                        //Redireccionamos
                                        bProcede = true;
                                    }
                                }
                                else
                                {
                                    strValidacion = "No existen Roles para este Usuario";
                                }
                            }
                            else
                            {
                                strValidacion = "El Usuario ya no está activo.";
                            }
                        }
                        else
                        {
                            strValidacion = "El Usuario ya no está activo.";
                        }
                    }
                    else
                    {
                        strValidacion = "La contraseña ingresada no es correcta.";
                    }
                }
                else
                {
                    strValidacion = "Verifique Nombre de Usuario y Contraseña.";
                }
            }
            catch (Exception exp)
            {
                strValidacion = exp.Message;
                //errores_control1.cargar("Error", UtilErrores.Tipos.critico, exp);;
            }

            return bProcede;
        }

        public static bool AutenticarPaciente(ref string strValidacion, string strCiPpa, ref EntPacPacientes miPaciente, ref EntSegUsuarios miUsuario, ref EntSegUsuariosrestriccion miUsuarioRestriccion, ref EntSegRoles miRol)
        {
            bool bProcede = false;
            try
            {
                miUsuario = new EntSegUsuarios();

                var rnPacienteVerificacion = new RnPacPacientes();

                var arrNombreParametros = new ArrayList();
                arrNombreParametros.Add("" + EntPacPacientes.Fields.cippa + "");
                var arrParametros = new ArrayList();
                arrParametros.Add("'" + strCiPpa + "'");

                var ePaciente = rnPacienteVerificacion.ObtenerObjeto(arrNombreParametros, arrParametros);

                if (ePaciente != null)
                {
                    //Obtenemos el rol de los pacientes
                    var rnPacienteRol = new RnSegRoles();
                    miRol = rnPacienteRol.ObtenerObjeto(10);
                    
                    //Verificamos que exista un Rol Activo
                    if (miRol == null)
                    {
                        strValidacion = "No existe un Rol Activo para este usuario";
                        bProcede = false;
                    }
                    else
                    {
                        miUsuarioRestriccion = new EntSegUsuariosrestriccion();
                        miUsuarioRestriccion.idsus = 0;
                        miUsuarioRestriccion.rolsro = miRol.rolsro;
                        miUsuarioRestriccion.rolactivosur = 1;

                        miUsuario.idsus = ePaciente.idppa;
                        miUsuario.loginsus = strCiPpa;
                        miUsuario.nombresus = ePaciente.nombresppa;
                        miUsuario.apellidosus = (ePaciente.appaternoppa + " " + ePaciente.apmaternoppa).Trim();                                                
                        //Redireccionamos
                        bProcede = true;
                    }
                }
                else
                {
                    strValidacion = "El ID de acceso del paciente no existe o no está habilitado.";
                }
            }
            catch (Exception exp)
            {
                strValidacion = exp.Message;
                //errores_control1.cargar("Error", UtilErrores.Tipos.critico, exp);;
            }

            return bProcede;
        }

        public static DataTable ObtenerMenu()
        {
            var rn = new RnVista();
            var miSesion = new SessionHandler();

            if (miSesion.DtMenu == null)
            {
                if (miSesion.AppRol == null)
                    return null;

                ArrayList arrColWhere = new ArrayList();
                arrColWhere.Add(EntSegRoles.Fields.rolsro.ToString());
                ArrayList arrValWhere = new ArrayList();
                arrValWhere.Add(miSesion.AppRol.rolsro);

                DataTable dtMenu = rn.ObtenerDatos("vw_seg_menu_lis", arrColWhere, arrValWhere);
                //Creamos el diccionario con las reglas
                var arrPags = new ArrayList();
                foreach (DataRow dr in dtMenu.Rows)
                    arrPags.Add(dr[EntSegPaginas.Fields.nombrespg.ToString()].ToString());

                miSesion.DtMenu = dtMenu;
                miSesion.ArrMenu = arrPags;
                return dtMenu;
            }
            return miSesion.DtMenu;
        }

        public static bool WebValidarUsuario(ref string strValidacion, ref System.Web.UI.WebControls.TextBox txtUsuario, ref System.Web.UI.WebControls.TextBox txtPass)
        {
            bool bProcede = true;
            bool pTieneFoco = false;

            //Validamos el Login
            if (string.IsNullOrEmpty(txtUsuario.Text))
            {
                strValidacion = Environment.NewLine + "Debe ingresar el Nombre de Usuario";
                bProcede = false;
                if (!pTieneFoco) txtUsuario.Focus();
            }
            //Validamos el Password
            if (string.IsNullOrEmpty(txtPass.Text))
            {
                strValidacion = Environment.NewLine + "Debe ingresar su Contraseña";
                bProcede = false;
                if (!pTieneFoco) txtPass.Focus();
            }

            return bProcede;
        }

        public static bool WebValidarPaciente(ref string strValidacion, ref System.Web.UI.WebControls.TextBox txtCiPpa)
        {
            bool bProcede = true;
            bool pTieneFoco = false;

            //Validamos el Login
            if (string.IsNullOrEmpty(txtCiPpa.Text))
            {
                strValidacion = Environment.NewLine + "Debe ingresar el ID de acceso del paciente";
                bProcede = false;
                if (!pTieneFoco) txtCiPpa.Focus();
            }

            return bProcede;
        }

        #endregion Methods
    }
}