using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Drive.v3;
using Google.Apis.Gmail.v1;
using Google.Apis.Oauth2.v2.Data;
using Google.Apis.PeopleService.v1;

namespace ReAl.GestionMedica.Web.App_Helpers
{
    public static class CParamGoogleApi
    {
        #region Fields

        public static string ApplicationName = "gestionmedica";

        // If modifying these scopes, delete your previously saved credentials
        // at ~/.credentials/docs.googleapis.com-dotnet-quickstart.json
        public static string[] Scopes =
        {
            //DocsService.Scope.DocumentsReadonly,
            //SheetsService.Scope.SpreadsheetsReadonly,
            PeopleServiceService.Scope.ContactsReadonly,
            DriveService.Scope.DriveReadonly,
            DriveService.Scope.Drive,
            DriveService.Scope.DriveFile,
            CalendarService.Scope.Calendar,
            CalendarService.Scope.CalendarEvents,
            "email",
            "profile"
        };

        #endregion Fields
    }
}