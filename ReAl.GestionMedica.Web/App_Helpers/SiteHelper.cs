﻿using System;
using System.Collections;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;

namespace ReAl.GestionMedica.Web.App_Helpers
{
    public class SiteHelper
    {
        #region Methods

        public static string GetMainPage(EntSegRoles objRol, ArrayList arrMenu)
        {
            string strRol = objRol.siglasro.ToUpper();
            if (arrMenu == null)
            {
                if ((strRol.Contains("MEDICO") || strRol.Contains("DOCTOR") || strRol.Contains("DR")))
                    return "~/CAL/LDashboardMedico.aspx";
                else
                {
                    return "~/CAL/LAgenda.aspx";
                }
            }
            else
            {
                if ((strRol.Contains("MEDICO") || strRol.Contains("DOCTOR") || strRol.Contains("DR")) &&
                    arrMenu.Contains("LDashboardMedico"))
                    return "~/CAL/LDashboardMedico.aspx";
                else
                {
                    return "~/CAL/LAgenda.aspx";
                }
            }
        }

        public static void VerificarCita(SessionHandler miSesion)
        {
            if (miSesion.AppCita == null || miSesion.AppCita.idcci == 0)
            {
                //Creamos la cita
                RnCalCitas rnCit = new RnCalCitas();
                EntCalCitas objCit = new EntCalCitas();
                objCit.idppa = miSesion.AppPaciente.idppa;
                objCit.asuntocci = "Cita automática";
                objCit.descripcioncci = "";
                objCit.iniciocci = DateTime.Now;
                objCit.finalcci = DateTime.Now.AddMinutes(5);
                objCit.lugarcci = "Consultorio";
                objCit.labelcci = 2; //Programado
                objCit.statuscci = 2;
                objCit.motivofaltacci = "";
                objCit.usuasignacion = miSesion.AppUsuario.loginsus;
                objCit.usucre = miSesion.AppUsuario.loginsus;
                rnCit.Insert(objCit);

                //Obtenemos la cita insertada
                miSesion.AppCita = rnCit.ObtenerObjetoInsertado(miSesion.AppUsuario.loginsus);
            }
        }

        #endregion Methods
    }
}