﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Npgsql;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.Web.App_Helpers;
using ReAl.Utils;

namespace ReAl.GestionMedica.Web
{
    public partial class PrivateSite : System.Web.UI.MasterPage
    {
        #region Properties

        /// <summary>
        /// Variable que almacena el mensaje de ERROR que se muestra en la pagina maestra
        /// </summary>
        public string Localerror
        {
            get
            {
                if (Request["error"] == null)
                    return "";
                return Request["error"];
            }
        }

        /// <summary>
        /// Variable que almacena el mensaje que se muestran en la pagina maestra
        /// </summary>
        public string Localmsg
        {
            get
            {
                if (Request["msg"] == null)
                    return "";
                return Request["msg"];
            }
        }

        #endregion Properties

        #region Methods

        public void MostrarPopUp(Page anfitrion, Exception exp)
        {
            try
            {
                JiraWrapper.CrearIssue(exp);
                var miSesion = new SessionHandler();
                miSesion.AppException = exp;
                if (exp.GetType() == typeof(SimpleException))
                {
                    lblErrorMensaje.Text = exp.Message;
                    lblErrorDescripcion.Text = "";
                    btnErrorReportar.Visible = false;
                }
                else if (exp is PostgresException)
                {
                    var rn = new RnSegMensajeserror();
                    var obj = new EntSegMensajeserror();

                    var sqlExp = (PostgresException)exp;

                    var strCodigoError = sqlExp.Message.Replace("P0001: ", "");

                    obj = rn.ObtenerObjeto(EntSegMensajeserror.Fields.aplicacionerrorsme, "'" + strCodigoError.Substring(0, 9) + "'");

                    if (obj != null)
                    {
                        var descripcionMsg = obj.descripcionsme;
                        var errorMsg = sqlExp.Message;

                        while (descripcionMsg.Contains("%n"))
                        {
                            var indiceInicio = errorMsg.IndexOf("##");
                            errorMsg = cFuncionesStrings.ReplaceFirst(errorMsg, "##", " ");
                            var indiceFinal = errorMsg.IndexOf("##");
                            if (indiceFinal < 0)
                            {
                                indiceFinal = errorMsg.Length;
                            }
                            var strVariable = errorMsg.Substring(indiceInicio, indiceFinal - indiceInicio);
                            descripcionMsg = cFuncionesStrings.ReplaceFirst(descripcionMsg, "%n", strVariable);
                        }

                        lblErrorCausa.Text = "<b>Causa:</b> " + obj.causasme + "<br/>";
                        lblErrorAccion.Text = "<b>Acción</b>: " + obj.accionsme + "<br/>";
                        lblErrorComentario.Text = "<b>Comentario</b>: " + obj.comentariosme + "<br/>";
                        lblErrorOrigen.Text = "<b>Origen</b>: " + obj.origensme + "<br/>";

                        lblErrorMensaje.Text = "<b>" + obj.aplicacionerrorsme + ":</b> " + descripcionMsg;
                    }
                    else
                    {
                        lblErrorMensaje.Text = "Se ha producido un error. Revise el Detalle adjunto.";
                        lblErrorDescripcion.Text = sqlExp.Message;
                    }
                }
                else
                {
                    lblErrorMensaje.Text = exp.Message;
                    lblErrorDescripcion.Text = exp.StackTrace;
                }
                ScriptManager.RegisterStartupScript(anfitrion, anfitrion.GetType(), "myModalError", "$('#myModalError').appendTo('body').modal('show');", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "myModalError", "$('#myModalError').modal('show');", true);
            }
            catch (Exception e2)
            {
                Debug.Print(e2.Message);
            }
        }

        protected void btnErrorReportar_OnClick(object sender, EventArgs e)
        {
            try
            {
                //Para BitBucket
                var miSesion = new SessionHandler();
                JiraWrapper.AddIssueOnJira(miSesion.AppException);
            }
            catch (Exception exp)
            {
                Debug.Print(exp.Message);
            }
        }

        protected void btnRecargar_OnClick(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void lnkCambiarPass_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/SEG/LPassword.aspx");
        }

        protected void lnkCerrar_OnClick(object sender, EventArgs e)
        {
            var miSesion = new SessionHandler();
            miSesion.SessionClear();
            Session.Clear();
            Session.Abandon();
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }

        protected void lnkUserProfile_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/SEG/LPerfil.aspx");
        }

        protected void LoginStatus_LoggedOut(object sender, EventArgs e)
        {
            Session.Clear();
            Session.Abandon();
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //Tratamos de Obtener el Proyecto del Usuario
            var miSesion = new SessionHandler();
            if (miSesion.AppUsuario == null)
            {
                miSesion.SessionClear();
                Session.Clear();
                Session.Abandon();
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                try
                {
                    lnkUserProfile.Visible = miSesion.AppRol.rolsro != 10;
                    lnkCambiarPass.Visible = miSesion.AppRol.rolsro != 10;

                    lblUsuario.Text = miSesion.AppUsuario.nombresus + " " +
                                      miSesion.AppUsuario.apellidosus;
                    lblUsuario2.Text = miSesion.AppUsuario.nombresus + " " +
                                       miSesion.AppUsuario.apellidosus;
                    lblRolActivo.Text = "<b>Rol: </b>" + miSesion.AppRol.siglasro;
                    lblRolActivo2.Text = " -" + miSesion.AppRol.siglasro + "- ";

                    CargarMenuVertical(miSesion.AppPaciente);

                    //Los mensajes de llegada
                    if (!string.IsNullOrEmpty(Localmsg))
                    {
                        lblMsg.Text = Localmsg;
                    }

                    if (!string.IsNullOrEmpty(Localerror))
                    {
                        pnlError.Visible = true;
                        lblError.Text = Localerror;
                    }

                    if (miSesion.GAppsUsuario != null)
                    {
                        picUserProfile.Src = miSesion.GAppsUsuario.Picture;
                        picUserProfileMain.Src = miSesion.GAppsUsuario.Picture;
                    }
                }
                catch (Exception expIgnored)
                {
                    Debug.WriteLine(expIgnored.Message);
                }
            }
        }

        /// <summary>
        /// Funcion que obtiene y arma el menu vertical del sistema en base al modulo activo
        /// </summary>
        private void CargarMenuVertical(EntPacPacientes paciente)
        {
            try
            {
                var dtMenu = AutenticacionHelper.ObtenerMenu();
                foreach (DataRow drMenu in dtMenu.Rows)
                {
                    if (drMenu[EntSegPaginas.Fields.aplicacionsap.ToString()].ToString() == "PAC" && (paciente == null || paciente.idppa == 0))
                        continue;

                    if (string.IsNullOrEmpty(drMenu[EntSegPaginas.Fields.paginapadrespg.ToString()].ToString()))
                    {
                        var divControl = new HtmlGenericControl();
                        divControl.InnerHtml = drMenu[EntSegPaginas.Fields.nombremenuspg.ToString()].ToString();
                        divControl.TagName = "div";
                        divControl.Attributes["class"] = "sidenav-menu-heading";
                        accordionSidenav.Controls.Add(divControl);
                    }
                    else
                    {
                        var aControl = new HtmlGenericControl();
                        aControl.InnerHtml = drMenu[EntSegPaginas.Fields.nombremenuspg.ToString()].ToString();
                        aControl.TagName = "a";
                        aControl.Attributes["class"] = "nav-link";
                        aControl.Attributes["href"] =
                            ResolveClientUrl(
                                drMenu[EntSegPaginas.Fields.aplicacionsap.ToString()] + "\\" +
                                drMenu[EntSegPaginas.Fields.nombrespg.ToString()]);
                        aControl.InnerHtml = "<div class='nav-link-icon'><i data-feather='" +
                                             drMenu["iconospg"] +
                                             "'></i></div>" +
                                             drMenu[EntSegPaginas.Fields.nombremenuspg.ToString()];
                        accordionSidenav.Controls.Add(aControl);
                    }
                }
            }
            catch (Exception exp)
            {
                MostrarPopUp(Page, exp);
            }
        }

        #endregion Methods
    }
}