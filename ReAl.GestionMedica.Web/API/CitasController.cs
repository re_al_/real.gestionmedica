﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using ReAl.GestionMedica.Class.Modelo;

namespace ReAl.GestionMedica.Web.API
{
    public class CitasController : ApiController
    {
        /// <summary>
        /// Objeto para el Cuadro de mando
        /// </summary>
        public class EntCitas
        {
            /// <summary>
            /// Mes de seguimiento
            /// </summary>
            public string Mes { get; set; }

            /// <summary>
            /// Citas acumuladas
            /// </summary>
            public decimal Citas { get; set; }
        }

        // GET api/<controller>/5
        public IQueryable<EntCitas> Get()
        {
            RnVista rn = new RnVista();

            var arrNomParam = new ArrayList();
            arrNomParam.Add("login");
            var arrValParam = new ArrayList();
            arrValParam.Add("rvera");
            DataTable dtReporte = rn.ObtenerDatosProcAlm("spapicitas", arrNomParam, arrValParam);

            var columnNames = dtReporte.Columns.Cast<DataColumn>()
                .Select(c => c.ColumnName)
                .ToList();
            var properties = typeof(EntCitas).GetProperties();

            //Devolvemos el dt
            return dtReporte.AsEnumerable().Select(row =>
            {
                var objT = Activator.CreateInstance<EntCitas>();
                foreach (var pro in properties)
                {
                    if (columnNames.Contains(pro.Name))
                    {
                        PropertyInfo pI = objT.GetType().GetProperty(pro.Name);
                        if (pI != null)
                            pro.SetValue(objT,
                                row[pro.Name] == DBNull.Value
                                    ? null
                                    : Convert.ChangeType(row[pro.Name], pI.PropertyType));
                    }
                }
                return objT;
            }).ToList().AsQueryable();
        }
    }
}