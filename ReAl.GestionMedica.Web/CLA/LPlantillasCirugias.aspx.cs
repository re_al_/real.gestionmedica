﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Web.App_Helpers;
using ReAl.Utils;

namespace ReAl.GestionMedica.Web.CLA
{
    public partial class LPlantillasCirugias : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                SessionHandler miSesion = new SessionHandler();

                if (miSesion.ArrMenu != null)
                    if (!miSesion.ArrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath.Split('/').Last()))
                        Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));

                if (!Page.IsPostBack)
                {
                    CargarListado();
                    txtNewCuerpocpl.Toolbar = "Basic";
                    txtNewCuerpocpl.DefaultLanguage = "es";

                    txtEditCuerpocpl.Toolbar = "Basic";
                    txtEditCuerpocpl.DefaultLanguage = "es";
                }
                else
                {
                    CargarListado();
                    ControlHelper.CrearEstilosGrid(ref dtgListado);
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        public void CargarListado()
        {
            try
            {
                SessionHandler miSesion = new SessionHandler();
                String strNombreSp = "SpCplSelGrid";

                var arrNomParam = new ArrayList();
                arrNomParam.Add(EntClaPlantillas.Fields.idctp.ToString());
                var arrValParam = new ArrayList();
                arrValParam.Add(2);

                var rn = new RnVista();
                var dt = rn.ObtenerDatosProcAlm(strNombreSp, arrNomParam, arrValParam);
                dtgListado.DataSource = dt;
                dtgListado.DataBind();
                ControlHelper.CrearEstilosGrid(ref dtgListado);
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        protected void btnAtras_OnClick(object sender, EventArgs e)
        {
            SessionHandler miSesion = new SessionHandler();
            Response.Redirect(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));
        }

        protected void btnNuevo_OnClick(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "newModal", "$('#newModal').modal('show');", true);
        }

        protected void dtgListado_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            String strRedireccion = "";
            try
            {
                if (e.CommandName.Equals("detalles"))
                {
                    string strId = e.CommandArgument.ToString();

                    //Filtramos el Dataset
                    DataView dv = ((DataTable)dtgListado.DataSource).DefaultView;
                    dv.RowFilter = EntClaPlantillas.Fields.idctp + " = " + strId;

                    DataTable detailTable = dv.ToTable();

                    dtgDetalles.DataSource = detailTable;
                    dtgDetalles.DataBind();
                    dtgDetalles.HeaderRow.TableSection = TableRowSection.TableHeader;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#currentdetail').appendTo('body').modal('show');", true);
                }
                else if (e.CommandName.Equals("modificar"))
                {
                    var strId = e.CommandArgument.ToString();
                    var rn = new RnClaPlantillas();
                    EntClaPlantillas objPag = rn.ObtenerObjeto(rn.GetColumnType(strId, EntClaPlantillas.Fields.idctp));
                    hdnIdDatos.Value = objPag.idcpl.ToString();

                    txtEdit_nombrecpl.Text = objPag.nombrecpl;
                    txtEdit_descripcioncpl.Text = objPag.descripcioncpl;
                    txtEdit_altocpl.Text = objPag.altocpl.ToString();
                    txtEdit_anchocpl.Text = objPag.anchocpl.ToString();
                    txtEdit_margensupcpl.Text = objPag.margensupcpl.ToString();
                    txtEdit_margeninfcpl.Text = objPag.margeninfcpl.ToString();
                    txtEdit_margendercpl.Text = objPag.margendercpl.ToString();
                    txtEdit_margenizqcpl.Text = objPag.margenizqcpl.ToString();
                    txtEditCuerpocpl.Text = objPag.htmlcpl;
                    lblResult.Visible = false;

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("$('#editModal').modal('show');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);
                }
                else if (e.CommandName.Equals("eliminar"))
                {
                    string strId = e.CommandArgument.ToString();

                    var rn = new RnClaPlantillas();
                    var obj = rn.ObtenerObjeto(rn.GetColumnType(strId, EntClaPlantillas.Fields.idctp));

                    if (obj != null)
                    {
                        rn.Delete(obj);
                        strRedireccion = "~/PAC/LRecetas";
                    }
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (!string.IsNullOrEmpty(strRedireccion))
                Response.Redirect(strRedireccion);
        }

        protected void btnNewGuardar_OnClick(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var miSesion = new SessionHandler();
                var rn = new RnClaPlantillas();
                var objPag = new EntClaPlantillas();

                objPag.idctp = 2;
                objPag.nombrecpl = rn.GetColumnType(txtNew_nombrecpl.Text, EntClaPlantillas.Fields.nombrecpl);
                objPag.descripcioncpl = rn.GetColumnType(txtNew_descripcioncpl.Text, EntClaPlantillas.Fields.descripcioncpl);
                objPag.altocpl = rn.GetColumnType(txtNew_altocpl.Text, EntClaPlantillas.Fields.altocpl);
                objPag.anchocpl = rn.GetColumnType(txtNew_anchocpl.Text, EntClaPlantillas.Fields.anchocpl);
                objPag.margensupcpl = rn.GetColumnType(txtNew_margensupcpl.Text, EntClaPlantillas.Fields.margensupcpl);
                objPag.margeninfcpl = rn.GetColumnType(txtNew_margeninfcpl.Text, EntClaPlantillas.Fields.margeninfcpl);
                objPag.margendercpl = rn.GetColumnType(txtNew_margendercpl.Text, EntClaPlantillas.Fields.margendercpl);
                objPag.margenizqcpl = rn.GetColumnType(txtNew_margenizqcpl.Text, EntClaPlantillas.Fields.margenizqcpl);
                objPag.htmlcpl = txtNewCuerpocpl.Text;
                objPag.textocpl = RtfHelper.HtmlToPlainText(txtEditCuerpocpl.Text);
                objPag.rtfcpl = RtfHelper.TextoToRtf(Server, txtEditCuerpocpl.Text);

                objPag.usucre = miSesion.AppUsuario.loginsus;

                rn.Insert(objPag);
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (bProcede)
                Response.Redirect(
                    Request.AppRelativeCurrentExecutionFilePath +
                    "?msg=Se ha creado el registro satisfactoriamente");
        }

        protected void btnEditaGuardar_OnClick(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var miSesion = new SessionHandler();
                var rn = new RnClaPlantillas();
                var objPag = rn.ObtenerObjeto(rn.GetColumnType(hdnIdDatos.Value, EntClaPlantillas.Fields.idctp));

                objPag.idctp = 2;
                objPag.nombrecpl = rn.GetColumnType(txtEdit_nombrecpl.Text, EntClaPlantillas.Fields.nombrecpl);
                objPag.descripcioncpl = rn.GetColumnType(txtEdit_descripcioncpl.Text, EntClaPlantillas.Fields.descripcioncpl);
                objPag.altocpl = rn.GetColumnType(txtEdit_altocpl.Text, EntClaPlantillas.Fields.altocpl);
                objPag.anchocpl = rn.GetColumnType(txtEdit_anchocpl.Text, EntClaPlantillas.Fields.anchocpl);
                objPag.margensupcpl = rn.GetColumnType(txtEdit_margensupcpl.Text, EntClaPlantillas.Fields.margensupcpl);
                objPag.margeninfcpl = rn.GetColumnType(txtEdit_margeninfcpl.Text, EntClaPlantillas.Fields.margeninfcpl);
                objPag.margendercpl = rn.GetColumnType(txtEdit_margendercpl.Text, EntClaPlantillas.Fields.margendercpl);
                objPag.margenizqcpl = rn.GetColumnType(txtEdit_margenizqcpl.Text, EntClaPlantillas.Fields.margenizqcpl);
                objPag.htmlcpl = txtNewCuerpocpl.Text;
                objPag.textocpl = RtfHelper.HtmlToPlainText(txtEditCuerpocpl.Text);
                objPag.rtfcpl = RtfHelper.TextoToRtf(Server, txtEditCuerpocpl.Text);

                objPag.apitransaccion = CApi.Transaccion.MODIFICAR.ToString();
                objPag.usumod = miSesion.AppUsuario.loginsus;

                rn.Update(objPag);
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }
            if (bProcede)
                Response.Redirect(
                    Request.AppRelativeCurrentExecutionFilePath +
                    "?msg=Se ha modificado el registro satisfactoriamente");
        }

        protected void btnImprimir_OnClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}