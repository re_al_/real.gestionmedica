﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Web.App_Helpers;
using ReAl.Utils;

namespace ReAl.GestionMedica.Web.CLA
{
    public partial class LCategorias : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                SessionHandler miSesion = new SessionHandler();

                if (miSesion.ArrMenu != null)
                    if (!miSesion.ArrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath.Split('/').Last()))
                        //Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));
                        Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));

                if (!Page.IsPostBack)
                {
                    CargarListado();
                }
                else
                {
                    CargarListado();
                    ControlHelper.CrearEstilosGrid(ref dtgListado);
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        public void CargarListado()
        {
            try
            {
                var rn = new RnGasCategoria();
                var dt = rn.ObtenerDataTable();
                dtgListado.DataSource = dt;
                dtgListado.DataBind();
                ControlHelper.CrearEstilosGrid(ref dtgListado);
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        protected void btnAtras_OnClick(object sender, EventArgs e)
        {
            SessionHandler miSesion = new SessionHandler();
            Response.Redirect(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));
        }

        protected void btnNuevo_OnClick(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "newModal", "$('#newModal').modal('show');", true);
        }

        protected void dtgListado_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            String strRedireccion = "";
            try
            {
                if (e.CommandName.Equals("detalles"))
                {
                    string strId = e.CommandArgument.ToString();

                    //Filtramos el Dataset
                    DataView dv = ((DataTable)dtgListado.DataSource).DefaultView;
                    dv.RowFilter = EntGasCategoria.Fields.idgca + " = " + strId;

                    DataTable detailTable = dv.ToTable();

                    dtgDetalles.DataSource = detailTable;
                    dtgDetalles.DataBind();
                    dtgDetalles.HeaderRow.TableSection = TableRowSection.TableHeader;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#currentdetail').appendTo('body').modal('show');", true);
                }
                else if (e.CommandName.Equals("modificar"))
                {
                    var strId = e.CommandArgument.ToString();
                    var rn = new RnGasCategoria();
                    EntGasCategoria objPag = rn.ObtenerObjeto(rn.GetColumnType(strId, EntGasCategoria.Fields.idgca));
                    hdnIdDatos.Value = objPag.idgca.ToString();

                    txtEdit_descripciongca.Text = objPag.descripciongca;
                    lblResult.Visible = false;

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("$('#editModal').modal('show');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);
                }
                else if (e.CommandName.Equals("eliminar"))
                {
                    string strId = e.CommandArgument.ToString();

                    var rn = new RnGasCategoria();
                    EntGasCategoria obj = rn.ObtenerObjeto(rn.GetColumnType(strId, EntGasCategoria.Fields.idgca));

                    if (obj != null)
                    {
                        rn.Delete(obj);
                        strRedireccion = "~/PAC/LRecetas";
                    }
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (!string.IsNullOrEmpty(strRedireccion))
                Response.Redirect(strRedireccion);
        }

        protected void btnNewGuardar_OnClick(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var miSesion = new SessionHandler();
                var rn = new RnGasCategoria();
                EntGasCategoria objPag = new EntGasCategoria();

                objPag.descripciongca = rn.GetColumnType(txtNew_descripciongca.Text, EntGasCategoria.Fields.descripciongca);

                objPag.usucre = miSesion.AppUsuario.loginsus;

                rn.Insert(objPag);
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (bProcede)
                Response.Redirect(
                    Request.AppRelativeCurrentExecutionFilePath +
                    "?msg=Se ha creado el registro satisfactoriamente");
        }

        protected void btnEditaGuardar_OnClick(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var miSesion = new SessionHandler();
                var rn = new RnGasCategoria();
                EntGasCategoria objPag = rn.ObtenerObjeto(rn.GetColumnType(hdnIdDatos.Value, EntGasCategoria.Fields.idgca));

                objPag.idgca = 2;
                objPag.descripciongca = rn.GetColumnType(txtEdit_descripciongca.Text, EntGasCategoria.Fields.descripciongca);

                objPag.apitransaccion = CApi.Transaccion.MODIFICAR.ToString();
                objPag.usumod = miSesion.AppUsuario.loginsus;

                rn.Update(objPag);
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }
            if (bProcede)
                Response.Redirect(
                    Request.AppRelativeCurrentExecutionFilePath +
                    "?msg=Se ha modificado el registro satisfactoriamente");
        }

        protected void btnImprimir_OnClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}