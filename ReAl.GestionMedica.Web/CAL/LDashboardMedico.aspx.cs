﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraScheduler;
using Google.Apis.Calendar.v3.Data;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Web.App_Helpers;

namespace ReAl.GestionMedica.Web.CAL
{
    public partial class LDashboardMedico : System.Web.UI.Page
    {
        #region Methods

        protected void btnActualizar_OnClick(object sender, EventArgs e)
        {
            CargarListado();
        }

        protected void dtCitas_OnItemCommand(object source, RepeaterCommandEventArgs e)
        {
            RepeaterItem rpItem = (RepeaterItem)(((LinkButton)e.CommandSource).NamingContainer);
            string strRedireccion = "";
            try
            {
                SessionHandler miSesion = new SessionHandler();
                RnPacPacientes rnPac = new RnPacPacientes();
                RnCalCitas rnCit = new RnCalCitas();
                if (e.CommandName.Equals("seleccionar"))
                {
                    //Obtenemos la cita seleccionada
                    miSesion.AppCita = rnCit.ObtenerObjeto(rnCit.GetColumnType(e.CommandArgument, EntCalCitas.Fields.idcci));
                    miSesion.AppPaciente = rnPac.ObtenerObjeto(rnPac.GetColumnType(miSesion.AppCita.idppa, EntPacPacientes.Fields.idppa));
                    strRedireccion = "~/PAC/LHistoria";
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
            if (!string.IsNullOrEmpty(strRedireccion))
                Response.Redirect(strRedireccion);
        }

        protected void lnkPacientes_OnClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        protected void lnkReporteGastos_OnClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        protected void lnkReporteIngresos_OnClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        protected void Page_Init(object sender, EventArgs e)
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                SessionHandler miSesion = new SessionHandler();

                //if (miSesion.ArrMenu != null)
                //    if (!miSesion.ArrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath.Split('/').Last()))
                //        Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));

                if (!Page.IsPostBack)
                {
                    dtpNewFechapci.Text = DateTime.Now.ToString(CParametros.ParFormatoFecha);
                    CargarListado();
                    CargarIndicadores();

                    if (miSesion.GAppsUsuario != null)
                    {
                        btnSync.Visible = true;
                    }
                }
                else
                {
                    CargarListado();
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        private void CargarIndicadores()
        {
            try
            {
                SessionHandler miSesion = new SessionHandler();
                var rn = new RnVista();
                var arrNomParam = new ArrayList();
                arrNomParam.Add("login");
                var arrValParam = new ArrayList();
                arrValParam.Add(miSesion.AppUsuario.loginsus);

                var dt = rn.ObtenerDatosProcAlm("spdashboard", arrNomParam, arrValParam);

                foreach (DataRow row in dt.Rows)
                {
                    string strTipo = row["tipo"].ToString();
                    if (strTipo == "Ingresos") litTotalIngresos.Text = string.Format("{0:#.00}", row["total"]);
                    if (strTipo == "Gastos") litTotalGastos.Text = string.Format("{0:#.00}", row["total"]);
                    if (strTipo == "Citas") litTotalCitas.Text = row["total"].ToString();
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        private void CargarListado()
        {
            try
            {
                if (string.IsNullOrEmpty(dtpNewFechapci.Text))
                {
                    dtpNewFechapci.Text = DateTime.Now.ToString(CParametros.ParFormatoFecha);
                }

                SessionHandler miSesion = new SessionHandler();
                var rn = new RnVista();
                var arrNomParam = new ArrayList();
                arrNomParam.Add("usuasignacion");
                arrNomParam.Add("fecha");
                var arrValParam = new ArrayList();
                arrValParam.Add(miSesion.AppUsuario.loginsus);
                arrValParam.Add(dtpNewFechapci.Text);

                var dt = rn.ObtenerDatosProcAlm("spcciseldashboard", arrNomParam, arrValParam);
                dtCitas.DataSource = dt;
                dtCitas.DataBind();
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        private void SincronizarEventos()
        {
            try
            {
                SessionHandler miSesion = new SessionHandler();

                //Citas sistema
                var rn = new RnVista();
                var arrNomParam = new ArrayList();
                arrNomParam.Add("usuasignacion");
                arrNomParam.Add("fecha");
                var arrValParam = new ArrayList();
                arrValParam.Add(miSesion.AppUsuario.loginsus);
                arrValParam.Add(dtpNewFechapci.Text);

                var dt = rn.ObtenerDatosProcAlm("spcciseldashboard", arrNomParam, arrValParam);

                //Citas google
                var google = new CGoogleApi();
                var service = google.CrearServicioCalendar(miSesion.GAppsCredential);
                var eventos = google.ListarEventos(service);

                foreach (DataRow row in dt.Rows)
                {
                    bool bExiste = false;
                    foreach (Event evento in eventos)
                    {
                        if (row["asuntocci"].ToString().Equals(evento.Summary))
                            bExiste = true;
                    }

                    if (!bExiste)
                    {
                        google.CrearEvento(service,
                            DateTime.Parse(row["iniciocci"].ToString()),
                            DateTime.Parse(row["finalcci"].ToString()), row["asuntocci"].ToString(),
                            row["descripcioncci"].ToString());
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        #endregion Methods

        protected void btnSync_OnClick(object sender, EventArgs e)
        {
            SincronizarEventos();
        }
    }
}