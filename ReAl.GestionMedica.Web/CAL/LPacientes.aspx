﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrivateSite.Master" AutoEventWireup="true" CodeBehind="LPacientes.aspx.cs" Inherits="ReAl.GestionMedica.Web.CAL.LPacientes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="Server">
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/css/bootstrap-select.min.css") %>" rel="stylesheet">
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.css") %>" rel="stylesheet">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <!-- Page Heading -->
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
        <div class="container-fluid">
            <div class="page-header-content pt-4">
                <div class="row align-items-center justify-content-between">
                    <div class="col-auto mt-4">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="users"></i></div>
                            Pacientes
                        </h1>
                        <div class="page-header-subtitle">
                        </div>
                    </div>
                    <%--<div class="col-12 col-xl-auto mt-4">Optional page header content</div>--%>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-fluid">
        <div class="card mt-n10">
            <div class="card-header">
                Registro de Pacientes
            </div>
            <div class="card-body">
                <p>
                    <!-- Botones de Accion -->
                    <asp:LinkButton ID="btnAtras" runat="server" Text="<i class='fa fa-arrow-left'></i>" ToolTip="Atras"
                        CssClass="btn btn-secondary " class="btn btn-secondary" OnClick="btnAtras_OnClick" />
                    <asp:LinkButton ID="btnNuevo" runat="server" Text="<i class='fa fa-plus-circle'></i>"
                        ToolTip="Nuevo registro"
                        CssClass="btn btn-secondary" class="btn btn-secondary" OnClick="btnNuevo_OnClick" />
                    <asp:LinkButton ID="btnBuscar" runat="server" Text="<i class='fa fa-search'></i>"
                        ToolTip="Buscar registro"
                        CssClass="btn btn-secondary" class="btn btn-secondary" OnClick="btnBuscar_OnClick" />
                    <asp:LinkButton ID="btnImprimir" runat="server" Text="<i class='fa fa-print'></i>"
                        ToolTip="Imprimir registros"
                        CssClass="btn btn-secondary" class="btn btn-secondary" OnClick="btnImprimir_OnClick" />
                </p>
                <div class="mb-4"></div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="input-group input-group-joined">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i data-feather="search"></i>
                                </span>
                            </div>
                            <asp:TextBox ID="txtBuscarNombre" runat="server" CssClass="form-control" type="text"
                                placeholder="Buscar por nombre..." aria-label="Search" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group input-group-joined">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i data-feather="search"></i>
                                </span>
                            </div>
                            <asp:TextBox ID="txtBuscarApPaterno" runat="server" CssClass="form-control" type="text"
                                placeholder="Buscar por apellido paterno..." aria-label="Search" />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group input-group-joined">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <i data-feather="search"></i>
                                </span>
                            </div>
                            <asp:TextBox ID="txtBuscarApMaterno" runat="server" CssClass="form-control" type="text"
                                placeholder="Buscar por apellido paterno..." aria-label="Search" />
                        </div>
                    </div>
                </div>
                <div class="mb-4"></div>
                <div class="text-sm">
                    <asp:GridView ID="dtgListado" runat="server" AutoGenerateColumns="False"
                        data-toggle="table" data-show-columns="true" data-pagination="true"
                        data-search="false" data-show-toggle="true" data-sortable="true"
                        data-page-size="25" data-pagination-v-align="both" data-show-export="true"
                        DataKeyNames="" OnRowCommand="dtgListado_OnRowCommand"
                        CssClass="table table-striped table-bordered table-hover">

                        <Columns>
                            <asp:BoundField ReadOnly="True" DataField="idppa" HeaderText="ID" ShowHeader="false">
                                <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Nombres" HeaderText="Nombres" ShowHeader="false">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Ap.Paterno" HeaderText="Ap.Paterno" ShowHeader="false">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Ap.Materno" HeaderText="Ap.Materno" ShowHeader="false">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Telefonos" HeaderText="Telefonos" ShowHeader="false" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Seguro" HeaderText="Seguro" ShowHeader="false" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Empresa" HeaderText="Empresa" ShowHeader="false" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="UltimaVisita" HeaderText="UltimaVisita" ShowHeader="false" HtmlEncode="False" DataFormatString="{0:yyyy/MM/dd HH:mm}">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:TemplateField HeaderText="Acciones">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="ImbSeleccionar" CommandName="seleccionar" CommandArgument='<%# Bind("idppa") %>' runat="server"
                                        ToolTip="Seleccionar paciente"
                                        CssClass="btn btn-success btn-sm" Text="<i class='fa fa-check-square'></i>" />

                                    <asp:LinkButton ID="ImbAgendar" CommandName="agendar" CommandArgument='<%# Bind("idppa") %>' runat="server"
                                        ToolTip="Agendar cita" Visible="False"
                                        CssClass="btn btn-primary btn-sm" Text="<i class='fa fa-calendar-plus'></i>" />

                                    <asp:LinkButton ID="ImbModificar" CommandName="modificar" CommandArgument='<%# Bind("idppa") %>' runat="server"
                                        ToolTip="Modificar el registro"
                                        CssClass="btn btn-secondary btn-sm" Text="<i class='fa fa-edit'></i>" />

                                    <asp:LinkButton ID="ImbEliminar" CommandName="eliminar" CommandArgument='<%# Bind("idppa") %>' runat="server"
                                        ToolTip="Eliminar el registro" OnClientClick="return confirm('¿Esta seguro que desea DAR DE BAJA el registro?');"
                                        CssClass="btn btn-danger btn-sm" Text="<i class='fa fa-eraser'></i>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

    <!-- Detail Modal -->
    <div class="modal fade" id="currentdetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <asp:UpdatePanel ID="updModaleDetail" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Detalle del registro</h4>
                        </div>
                        <div class="modal-body">
                            <asp:UpdatePanel ID="updVerDetalle" runat="server">
                                <ContentTemplate>
                                    <asp:DetailsView ID="dtgDetalles" runat="server"
                                        CssClass="table table-striped table-bordered table-hover"
                                        FieldHeaderStyle-Font-Bold="true" AutoGenerateRows="True">
                                        <Fields>
                                        </Fields>
                                    </asp:DetailsView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="dtgListado" EventName="RowCommand" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- New Modal -->
    <div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <asp:UpdatePanel ID="updModalNew" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header bg-success">
                            <h5 class="modal-title text-white">Registro de nuevo paciente</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body text-sm"> 
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="LabelNew1" runat="server">Nombres:</label>
                                        <asp:TextBox ID="txtNew_nombresppa"
                                            data-parsley-maxlength="200" required=""
                                            data-parsley-group="validation-new"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="200" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelNew2" runat="server">Ap. Paterno:</label>
                                        <asp:TextBox ID="txtNew_appaternoppa"
                                            data-parsley-maxlength="200"
                                            data-parsley-group="validation-new"
                                            data-parsley-required="false"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="200" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelNew3" runat="server">Ap. Materno:</label>
                                        <asp:TextBox ID="txtNew_apmaternoppa"
                                            data-parsley-maxlength="200"
                                            data-parsley-group="validation-new"
                                            data-parsley-required="false"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="200" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelNew5" runat="server">Fecha Nacimiento:</label>
                                        <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                                            <asp:TextBox ID="txtNew_fechanac" runat="server"
                                                required="" MaxLength="10" data-target="#datetimepicker1"
                                                data-parsley-group="validation-new" data-toggle="datetimepicker"
                                                data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYYY"
                                                Class="form-control datetimepicker-input" CssClass="form-control datetimepicker-input" />
                                            <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelNew4" runat="server">Género:</label>
                                        <asp:DropDownList ID="ddlNew_genero" name="ddlNew_genero" runat="server"
                                            class="form-control selectpicker" data-live-search="true"
                                            data-select-on-tab="true" data-size="10"
                                            CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelNew6" runat="server">Tipo de Sangre:</label>
                                        <asp:DropDownList ID="ddlNew_idcts" name="ddlNew_idcts" runat="server"
                                            class="form-control selectpicker" data-live-search="true"
                                            data-select-on-tab="true" data-size="10"
                                            CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelNew7" runat="server">Estado Civil:</label>
                                        <asp:DropDownList ID="ddlNew_idcec" name="ddlNew_idcec" runat="server"
                                            class="form-control selectpicker" data-live-search="true"
                                            data-select-on-tab="true" data-size="10"
                                            CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="LabelNew10" runat="server">Domicilio:</label>
                                        <asp:TextBox ID="txtNew_domicilioppa"
                                                     data-parsley-maxlength="500"
                                                     data-parsley-group="validation-new"
                                                     data-parsley-required="false"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="500" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelNew12" runat="server">Telefonos:</label>
                                        <asp:TextBox ID="txtNew_telefonosppa"
                                                     data-parsley-maxlength="200"
                                                     data-parsley-group="validation-new"
                                                     data-parsley-required="false"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="200" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelNew13" runat="server">Email:</label>
                                        <asp:TextBox ID="txtNew_emailppa"
                                                     data-parsley-maxlength="200"
                                                     data-parsley-group="validation-new"
                                                     data-parsley-required="false"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="200" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelNew14" runat="server">Empresa:</label>
                                        <asp:TextBox ID="txtNew_empresappa"
                                                     data-parsley-maxlength="500"
                                                     data-parsley-group="validation-new"
                                                     data-parsley-required="false"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="500" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelNew8" runat="server">Ocupación:</label>
                                        <asp:TextBox ID="txtNew_ocupacionppa"
                                            data-parsley-maxlength="300"
                                            data-parsley-group="validation-new"
                                            data-parsley-required="false"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="300" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelNew15" runat="server">Seguro:</label>
                                        <asp:TextBox ID="txtNew_seguroppa"
                                                     data-parsley-maxlength="200"
                                                     data-parsley-group="validation-new"
                                                     data-parsley-required="false"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="200" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelNew9" runat="server">Procedencia:</label>
                                        <asp:TextBox ID="txtNew_procedenciappa"
                                                     data-parsley-maxlength="300"
                                                     data-parsley-group="validation-new"
                                                     data-parsley-required="false"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="300" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelNew11" runat="server">Dominancia:</label>
                                        <asp:TextBox ID="txtNew_dominanciappa"
                                            data-parsley-maxlength="100"
                                            data-parsley-group="validation-new"
                                            data-parsley-required="false"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="100" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="Label4" runat="server">Religión:</label>
                                        <asp:TextBox ID="txtNew_religionppa"
                                            data-parsley-maxlength="200"
                                            data-parsley-group="validation-new"
                                            data-parsley-required="false"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="200" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelNew16" runat="server">Remite:</label>
                                        <asp:TextBox ID="txtNew_remiteppa"
                                            data-parsley-maxlength="500"
                                            data-parsley-group="validation-new"
                                            data-parsley-required="false"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="500" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelNew17" runat="server">Informante:</label>
                                        <asp:TextBox ID="txtNew_informanteppa"
                                            data-parsley-maxlength="500"
                                            data-parsley-group="validation-new"
                                            data-parsley-required="false"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="500" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-warning" type="button" data-dismiss="modal">Cerrar</button>
                            <asp:Button ID="btnNewGuardar" runat="server" Text="Registrar nuevo"
                                OnClientClick="return $('#aspnetForm').parsley().validate({group: 'validation-new'});"
                                CssClass="btn btn-success" OnClick="btnNewGuardar_OnClick" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnNuevo" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnNewGuardar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <asp:UpdatePanel ID="updModaleEdit" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header bg-secondary">
                            <h5 class="modal-title text-white" id="exampleModalCenterTitle">Editar registro de paciente</h5>
                            <asp:HiddenField ID="hdnIdDatos" runat="server" />
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body text-sm">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="LabelEdit1" runat="server">Nombres:</label>
                                        <asp:TextBox ID="txtEdit_nombresppa"
                                            data-parsley-maxlength="200" required=""
                                            data-parsley-group="validation-edit"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="200" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit2" runat="server">Ap. Paterno:</label>
                                        <asp:TextBox ID="txtEdit_appaternoppa"
                                            data-parsley-maxlength="200"
                                            data-parsley-group="validation-edit"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="200" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit3" runat="server">Ap. Materno:</label>
                                        <asp:TextBox ID="txtEdit_apmaternoppa"
                                            data-parsley-maxlength="200"
                                            data-parsley-group="validation-edit"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="200" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit5" runat="server">Fecha Nacimiento:</label>
                                        <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                                            <asp:TextBox ID="txtEdit_fechanac" runat="server"
                                                required="" MaxLength="10" data-target="#datetimepicker2"
                                                data-parsley-group="validation-edit" data-toggle="datetimepicker"
                                                data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYYY"
                                                Class="form-control datetimepicker-input" CssClass="form-control datetimepicker-input" />
                                            <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit4" runat="server">Género:</label>
                                        <asp:DropDownList ID="ddlEdit_genero" runat="server"
                                            class="form-control selectpicker" data-live-search="true"
                                            data-select-on-tab="true" data-size="10"
                                            CssClass="form-control">
                                            <asp:ListItem Value="1" Selected="True">Masculino</asp:ListItem>
                                            <asp:ListItem Value="0">Femenino</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit6" runat="server">Tipo de Sangre:</label>
                                        <asp:DropDownList ID="ddlEdit_idcts" runat="server"
                                            class="form-control selectpicker" data-live-search="true"
                                            data-select-on-tab="true" data-size="10"
                                            CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit7" runat="server">Estado Civil:</label>
                                        <asp:DropDownList ID="ddlEdit_idcec" runat="server"
                                            class="form-control selectpicker" data-live-search="true"
                                            data-select-on-tab="true" data-size="10"
                                            CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="LabelEdit10" runat="server">Domicilio:</label>
                                        <asp:TextBox ID="txtEdit_domicilioppa"
                                            data-parsley-maxlength="500"
                                            data-parsley-group="validation-edit"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="500" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit12" runat="server">Telefonos:</label>
                                        <asp:TextBox ID="txtEdit_telefonosppa"
                                            data-parsley-maxlength="200"
                                            data-parsley-group="validation-edit"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="200" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit13" runat="server">Email:</label>
                                        <asp:TextBox ID="txtEdit_emailppa"
                                                     data-parsley-maxlength="200"
                                                     data-parsley-group="validation-edit"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="200" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit14" runat="server">Empresa:</label>
                                        <asp:TextBox ID="txtEdit_empresappa"
                                                     data-parsley-maxlength="500"
                                                     data-parsley-group="validation-edit"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="500" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit8" runat="server">Ocupación:</label>
                                        <asp:TextBox ID="txtEdit_ocupacionppa"
                                            data-parsley-maxlength="300"
                                            data-parsley-group="validation-edit"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="300" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit15" runat="server">Seguro:</label>
                                        <asp:TextBox ID="txtEdit_seguroppa"
                                                     data-parsley-maxlength="200"
                                                     data-parsley-group="validation-edit"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="200" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit9" runat="server">Procedencia:</label>
                                        <asp:TextBox ID="txtEdit_procedenciappa"
                                                     data-parsley-maxlength="300"
                                                     data-parsley-group="validation-edit"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="300" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit11" runat="server">Dominancia:</label>
                                        <asp:TextBox ID="txtEdit_dominanciappa"
                                            data-parsley-maxlength="100"
                                            data-parsley-group="validation-edit"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="100" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit18" runat="server">Religión:</label>
                                        <asp:TextBox ID="txtEdit_religionppa"
                                            data-parsley-maxlength="200"
                                            data-parsley-group="validation-edit"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="200" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit16" runat="server">Remite:</label>
                                        <asp:TextBox ID="txtEdit_remiteppa"
                                            data-parsley-maxlength="500"
                                            data-parsley-group="validation-edit"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="500" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label id="LabelEdit17" runat="server">Informante:</label>
                                        <asp:TextBox ID="txtEdit_informanteppa"
                                            data-parsley-maxlength="500"
                                            data-parsley-group="validation-edit"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="500" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-warning" type="button" data-dismiss="modal">Cancelar</button>
                            <asp:Label ID="lblResult" Visible="false" runat="server" />
                            <asp:Button ID="btnEditaGuardar" runat="server" Text="Guardar Cambios"
                                OnClientClick="return $('#aspnetForm').parsley().validate({group: 'validation-edit'});"
                                CssClass="btn btn-secondary" OnClick="btnEditaGuardar_OnClick" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="dtgListado" EventName="RowCommand" />
                    <asp:AsyncPostBackTrigger ControlID="btnEditaGuardar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="Server">
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/popper/popper.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/locale/bootstrap-table-es-MX.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/js/bootstrap-select.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/moment/moment.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.js") %>"></script>

    <script>
        $(document).ready(function () {
            $('select').selectpicker();
            $('[data-toggle="popover"]').popover();
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        function pageLoad() {
            $(function () {
                $('select').selectpicker();
                $('[data-toggle="popover"]').popover();
                $('#datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#datetimepicker2').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        }
    </script>
</asp:Content>