﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Web.App_Helpers;

namespace ReAl.GestionMedica.Web.CAL
{
    public partial class LPacientes : System.Web.UI.Page
    {
        public void CargarListado(string nombresppa, string appaternoppa, string apmaternoppa)
        {
            try
            {
                var miSesion = new SessionHandler();
                var strNombreSp = "spppaselgridbuscarseparado";

                var arrNomParam = new ArrayList();
                arrNomParam.Add(EntPacPacientes.Fields.nombresppa.ToString());
                arrNomParam.Add(EntPacPacientes.Fields.appaternoppa.ToString());
                arrNomParam.Add(EntPacPacientes.Fields.apmaternoppa.ToString());
                var arrValParam = new ArrayList();
                arrValParam.Add(nombresppa);
                arrValParam.Add(appaternoppa);
                arrValParam.Add(apmaternoppa);

                var rn = new RnVista();
                var dt = rn.ObtenerDatosProcAlm(strNombreSp, arrNomParam, arrValParam);
                dtgListado.DataSource = dt;
                dtgListado.DataBind();
                ControlHelper.CrearEstilosGrid(ref dtgListado);
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        protected void btnAtras_OnClick(object sender, EventArgs e)
        {
            SessionHandler miSesion = new SessionHandler();
            Response.Redirect(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));
        }

        protected void btnEditaGuardar_OnClick(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var miSesion = new SessionHandler();
                var rn = new RnPacPacientes();
                EntPacPacientes objPag = rn.ObtenerObjeto(rn.GetColumnType(hdnIdDatos.Value, EntPacPacientes.Fields.idppa));
                objPag.nombresppa = rn.GetColumnType(txtEdit_nombresppa.Text, EntPacPacientes.Fields.nombresppa);
                objPag.appaternoppa = rn.GetColumnType(txtEdit_appaternoppa.Text, EntPacPacientes.Fields.appaternoppa);
                objPag.apmaternoppa = rn.GetColumnType(txtEdit_apmaternoppa.Text, EntPacPacientes.Fields.apmaternoppa);
                objPag.generoppa = ddlNew_genero.SelectedValue == "1";
                objPag.fechanac = rn.GetColumnType(txtEdit_fechanac.Text, EntPacPacientes.Fields.fechanac);
                objPag.idcts = rn.GetColumnType(ddlEdit_idcts.SelectedValue, EntPacPacientes.Fields.idcts);
                objPag.idcec = rn.GetColumnType(ddlEdit_idcec.SelectedValue, EntPacPacientes.Fields.idcec);
                objPag.ocupacionppa = rn.GetColumnType(txtEdit_ocupacionppa.Text, EntPacPacientes.Fields.ocupacionppa);
                objPag.procedenciappa = rn.GetColumnType(txtEdit_procedenciappa.Text, EntPacPacientes.Fields.procedenciappa);
                objPag.domicilioppa = rn.GetColumnType(txtEdit_domicilioppa.Text, EntPacPacientes.Fields.domicilioppa);
                objPag.dominanciappa = rn.GetColumnType(txtEdit_dominanciappa.Text, EntPacPacientes.Fields.dominanciappa);
                objPag.telefonosppa = rn.GetColumnType(txtEdit_telefonosppa.Text, EntPacPacientes.Fields.telefonosppa);
                objPag.emailppa = rn.GetColumnType(txtEdit_emailppa.Text, EntPacPacientes.Fields.emailppa);
                objPag.religionppa = rn.GetColumnType(txtEdit_religionppa.Text, EntPacPacientes.Fields.religionppa);
                objPag.empresappa = rn.GetColumnType(txtEdit_empresappa.Text, EntPacPacientes.Fields.empresappa);
                objPag.seguroppa = rn.GetColumnType(txtEdit_seguroppa.Text, EntPacPacientes.Fields.seguroppa);
                objPag.remiteppa = rn.GetColumnType(txtEdit_remiteppa.Text, EntPacPacientes.Fields.remiteppa);
                objPag.informanteppa = rn.GetColumnType(txtEdit_informanteppa.Text, EntPacPacientes.Fields.informanteppa);

                objPag.apitransaccion = CApi.Transaccion.MODIFICAR.ToString();
                objPag.usumod = miSesion.AppUsuario.loginsus;

                rn.Update(objPag);
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }
            if (bProcede)
                Response.Redirect(
                    Request.AppRelativeCurrentExecutionFilePath +
                    "?msg=Se ha modificado el registro satisfactoriamente");
        }

        protected void btnImprimir_OnClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        protected void btnNewGuardar_OnClick(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var miSesion = new SessionHandler();
                var rn = new RnPacPacientes();
                var objPag = new EntPacPacientes();
                objPag.nombresppa = rn.GetColumnType(txtNew_nombresppa.Text, EntPacPacientes.Fields.nombresppa);
                objPag.appaternoppa = rn.GetColumnType(txtNew_appaternoppa.Text, EntPacPacientes.Fields.appaternoppa);
                objPag.apmaternoppa = rn.GetColumnType(txtNew_apmaternoppa.Text, EntPacPacientes.Fields.apmaternoppa);
                objPag.generoppa = ddlNew_genero.SelectedValue == "1";
                objPag.fechanac = rn.GetColumnType(txtNew_fechanac.Text, EntPacPacientes.Fields.fechanac);
                objPag.idcts = rn.GetColumnType(ddlNew_idcts.SelectedValue, EntPacPacientes.Fields.idcts);
                objPag.idcec = rn.GetColumnType(ddlNew_idcec.SelectedValue, EntPacPacientes.Fields.idcec);
                objPag.ocupacionppa = rn.GetColumnType(txtNew_ocupacionppa.Text, EntPacPacientes.Fields.ocupacionppa);
                objPag.procedenciappa = rn.GetColumnType(txtNew_procedenciappa.Text, EntPacPacientes.Fields.procedenciappa);
                objPag.domicilioppa = rn.GetColumnType(txtNew_domicilioppa.Text, EntPacPacientes.Fields.domicilioppa);
                objPag.dominanciappa = rn.GetColumnType(txtNew_dominanciappa.Text, EntPacPacientes.Fields.dominanciappa);
                objPag.telefonosppa = rn.GetColumnType(txtNew_telefonosppa.Text, EntPacPacientes.Fields.telefonosppa);
                objPag.emailppa = rn.GetColumnType(txtNew_emailppa.Text, EntPacPacientes.Fields.emailppa);
                objPag.religionppa = rn.GetColumnType(txtNew_religionppa.Text, EntPacPacientes.Fields.religionppa);
                objPag.empresappa = rn.GetColumnType(txtNew_empresappa.Text, EntPacPacientes.Fields.empresappa);
                objPag.seguroppa = rn.GetColumnType(txtNew_seguroppa.Text, EntPacPacientes.Fields.seguroppa);
                objPag.remiteppa = rn.GetColumnType(txtNew_remiteppa.Text, EntPacPacientes.Fields.remiteppa);
                objPag.informanteppa = rn.GetColumnType(txtNew_informanteppa.Text, EntPacPacientes.Fields.informanteppa);

                objPag.usucre = miSesion.AppUsuario.loginsus;
                rn.Insert(objPag);
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (bProcede)
                Response.Redirect(
                    Request.AppRelativeCurrentExecutionFilePath +
                    "?msg=Se ha creado el registro satisfactoriamente");
        }

        protected void btnNuevo_OnClick(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "newModal", "$('#newModal').modal('show');", true);
        }

        protected void dtgListado_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            var strRedireccion = "";
            try
            {
                if (e.CommandName.Equals("detalles"))
                {
                    var strId = e.CommandArgument.ToString();

                    //Filtramos el Dataset
                    var dv = ((DataTable)dtgListado.DataSource).DefaultView;
                    dv.RowFilter = EntPacRecetas.Fields.idpre + " = " + strId;

                    var detailTable = dv.ToTable();

                    dtgDetalles.DataSource = detailTable;
                    dtgDetalles.DataBind();
                    dtgDetalles.HeaderRow.TableSection = TableRowSection.TableHeader;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#currentdetail').appendTo('body').modal('show');", true);
                }
                else if (e.CommandName.Equals("agendar"))
                {
                }
                else if (e.CommandName.Equals("seleccionar"))
                {
                    SessionHandler miSesion = new SessionHandler();
                    var strId = e.CommandArgument.ToString();

                    var rn = new RnPacPacientes();
                    miSesion.AppPaciente = rn.ObtenerObjeto(rn.GetColumnType(strId, EntPacPacientes.Fields.idppa));
                    strRedireccion = "~/PAC/LHistoria";
                }
                else if (e.CommandName.Equals("modificar"))
                {
                    var strId = e.CommandArgument.ToString();
                    var rn = new RnPacPacientes();
                    var objPag = rn.ObtenerObjeto(rn.GetColumnType(strId, EntPacPacientes.Fields.idppa));
                    hdnIdDatos.Value = objPag.idppa.ToString();
                    txtEdit_nombresppa.Text = objPag.nombresppa;
                    txtEdit_appaternoppa.Text = objPag.appaternoppa;
                    txtEdit_apmaternoppa.Text = objPag.apmaternoppa;
                    ddlEdit_genero.SelectedValue = objPag.generoppa ? "1" : "0";
                    txtEdit_fechanac.Text = objPag.fechanac.ToString(CParametros.ParFormatoFecha);
                    ddlEdit_idcts.SelectedValue = objPag.idcts.ToString();
                    ddlEdit_idcec.SelectedValue = objPag.idcec.ToString();
                    txtEdit_ocupacionppa.Text = objPag.ocupacionppa;
                    txtEdit_procedenciappa.Text = objPag.procedenciappa;
                    txtEdit_domicilioppa.Text = objPag.domicilioppa;
                    txtEdit_dominanciappa.Text = objPag.dominanciappa;
                    txtEdit_telefonosppa.Text = objPag.telefonosppa;
                    txtEdit_emailppa.Text = objPag.emailppa;
                    txtEdit_religionppa.Text = objPag.religionppa;
                    txtEdit_empresappa.Text = objPag.empresappa;
                    txtEdit_seguroppa.Text = objPag.seguroppa;
                    txtEdit_remiteppa.Text = objPag.remiteppa;
                    txtEdit_informanteppa.Text = objPag.informanteppa;
                    lblResult.Visible = false;

                    var sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("$('#editModal').modal('show');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);
                }
                else if (e.CommandName.Equals("eliminar"))
                {
                    var strId = e.CommandArgument.ToString();

                    var rn = new RnPacRecetas();
                    SessionHandler miSesion = new SessionHandler();
                    var obj = rn.ObtenerObjeto(rn.GetColumnType(strId, EntPacRecetas.Fields.idpre));

                    if (obj != null)
                    {
                        obj.apitransaccion = CApi.Transaccion.DESHABILITAR.ToString();
                        obj.usumod = miSesion.AppUsuario.loginsus;
                        rn.Update(obj);
                        strRedireccion = "~/PAC/LPacientes";
                    }
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (!string.IsNullOrEmpty(strRedireccion))
                Response.Redirect(strRedireccion);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                var miSesion = new SessionHandler();

                if (miSesion.ArrMenu != null)
                    if (!miSesion.ArrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath.Split('/').Last()))
                        Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));

                if (!Page.IsPostBack)
                {
                    CargarCmbGenero();
                    CargarCmbidcts();
                    CargarCmbidcec();
                    CargarListado("","","");
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        private void CargarCmbGenero()
        {
            try
            {
                var rn = new RnVista();
                var dt = rn.ObtenerDatosProcAlm("spppagenero");
                ddlNew_genero.DataValueField = "idgenero";
                ddlNew_genero.DataTextField = "Genero";
                ddlNew_genero.DataSource = dt;
                ddlNew_genero.DataBind();

                ddlEdit_genero.DataValueField = "idgenero";
                ddlEdit_genero.DataTextField = "Genero";
                ddlEdit_genero.DataSource = dt;
                ddlEdit_genero.DataBind();
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        private void CargarCmbidcec()
        {
            try
            {
                var rn = new RnClaEstadocivil();
                var dt = rn.ObtenerDataTable();
                ddlNew_idcec.DataValueField = EntClaEstadocivil.Fields.idcec.ToString();
                ddlNew_idcec.DataTextField = EntClaEstadocivil.Fields.descripcioncec.ToString();
                ddlNew_idcec.DataSource = dt;
                ddlNew_idcec.DataBind();

                ddlEdit_idcec.DataValueField = EntClaEstadocivil.Fields.idcec.ToString();
                ddlEdit_idcec.DataTextField = EntClaEstadocivil.Fields.descripcioncec.ToString();
                ddlEdit_idcec.DataSource = dt;
                ddlEdit_idcec.DataBind();
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        private void CargarCmbidcts()
        {
            try
            {
                var rn = new RnClaTiposangre();
                var dt = rn.ObtenerDataTable();
                ddlNew_idcts.DataValueField = EntClaTiposangre.Fields.idcts.ToString();
                ddlNew_idcts.DataTextField = EntClaTiposangre.Fields.descripcioncts.ToString();
                ddlNew_idcts.DataSource = dt;
                ddlNew_idcts.DataBind();

                ddlEdit_idcts.DataValueField = EntClaTiposangre.Fields.idcts.ToString();
                ddlEdit_idcts.DataTextField = EntClaTiposangre.Fields.descripcioncts.ToString();
                ddlEdit_idcts.DataSource = dt;
                ddlEdit_idcts.DataBind();
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        protected void btnBuscar_OnClick(object sender, EventArgs e)
        {
            CargarListado(txtBuscarNombre.Text, txtBuscarApPaterno.Text, txtBuscarApMaterno.Text);
        }
    }
}