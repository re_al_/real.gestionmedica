﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraScheduler;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Web.App_Helpers;

namespace ReAl.GestionMedica.Web.CAL
{
    public partial class LAgendaPaciente : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            var dialog = SchedulerDateNavigator.OptionsDialogs.AppointmentDialog.UseViewModel<CustomAppointmentPacEditDialogViewModel>();
            dialog.GenerateDefaultLayoutElements();

            //var pacientes = dialog.FindLayoutElement("idppa");
            var pacientes = dialog.FindLayoutElement("StatusKey");
            pacientes.ColSpanLg = 12;
            pacientes.ColSpanXl = 12;
            pacientes.ColSpanMd = 12;
            dialog.InsertBefore(pacientes, dialog.FindLayoutElement("Subject"));

            var doctor = dialog.FindLayoutElement("ResourceIds");
            dialog.InsertAfter(doctor, dialog.FindLayoutElement("Subject"));

            //var label = dialog.FindLayoutElement("LabelKey");
            //dialog.InsertAfter(label, dialog.FindLayoutElement("ResourceIds"));

            var lugar = dialog.FindLayoutElement("Location");
            dialog.InsertAfter(lugar, dialog.FindLayoutElement("ResourceIds"));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SchedulerDateNavigator.OptionsView.ResourceColorFillArea = DevExpress.Web.ASPxScheduler.ResourceColorFillArea.TimeCells;
            SchedulerDateNavigator.DataBind();
            if (!IsPostBack)
                this.SchedulerDateNavigator.Start = DateTime.Today;
        }

        protected void ObjectDataSourceResources_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            if (Session["ObjectDataSourceResources"] == null)
            {
                Session["ObjectDataSourceResources"] = new ResourceDataSource();
            }
            e.ObjectInstance = Session["ObjectDataSourceResources"];
        }

        protected void ObjectDataSourceAppointment_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            if (Session["ObjectDataSourceAppointment"] == null)
            {
                Session["ObjectDataSourceAppointment"] = new AppointmentsPacDataSource();
            }
            e.ObjectInstance = Session["ObjectDataSourceAppointment"];
        }

        protected void SchedulerDateNavigator_InitAppointmentDisplayText(object sender, AppointmentDisplayTextEventArgs e)
        {
            Appointment apt = e.Appointment;
            e.Text = String.Format("{0} ({1})", apt.Subject, apt.Location);
            e.Description = String.Format("{0}", apt.Description);
        }
    }

    public class AppointmentsPacDataSource
    {
        public DataTable GetAppointmentsPacDataTable()
        {
            var miSesion = new SessionHandler();
            var rn = new RnVista();
            var arrNomParam = new ArrayList();
            arrNomParam.Add(EntPacPacientes.Fields.idppa.ToString());
            var arrValParam = new ArrayList();
            arrValParam.Add(miSesion.AppUsuario.idsus);
            var dtResources = rn.ObtenerDatosProcAlm("spcciselpacscheduler", arrNomParam, arrValParam);
            return dtResources;
            /*
            if (HttpContext.Current.Session["AppointmentsPacDataSource"] == null)
            {
                var rn = new RnVista();
                var dtAppointments = rn.ObtenerDatosProcAlm("spcciselscheduler"); ;
                HttpContext.Current.Session["AppointmentsPacDataSource"] = dtAppointments;
            }
            return HttpContext.Current.Session["AppointmentsPacDataSource"] as System.Data.DataTable;
            */
        }        

        public void UpdateCustomAppointments(DateTime iniciocci, DateTime finalcci, Int64 idcci, object idppa, int labelcci, string descripcioncci, string lugarcci, string usuasignacion, string asuntocci)
        {
            var strMsg = "";
            try
            {
                var miSesion = new SessionHandler();
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

                var dtpInicio = iniciocci.Date + iniciocci.TimeOfDay;
                var dtpFinal = finalcci.Date + finalcci.TimeOfDay;

                Int64 idApt = idcci;                
                var rn = new RnCalCitas();
                var obj = rn.ObtenerObjeto(idApt);
                if (obj != null)
                {
                    //Editar registros cuyo LabelKey = 0 //Solicitado
                    if (labelcci == 0)
                    {
                        obj.asuntocci = asuntocci;
                        obj.descripcioncci = descripcioncci;
                        obj.iniciocci = dtpInicio;
                        obj.finalcci = dtpFinal;
                        obj.lugarcci = lugarcci;
                        obj.labelcci = labelcci;
                        obj.statuscci = 2;
                        obj.usuasignacion = usuasignacion;

                        obj.usumod = "rvera";
                        obj.apitransaccion = CApi.Transaccion.MODIFICAR.ToString();
                        rn.Update(obj);
                        strMsg = "?msg=Se ha actualizado su cita";                        
                    }
                    else
                    {
                        strMsg = "?error=No se puede editar esta Cita";
                    }
                }
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp);
            }
            //if (_bHasError)
            //{
                //HttpContext.Current.Response.Redirect("~/CAL/LAgendaPaciente.aspx" + strMsg);
            //}
        }

        public void InsertCustomAppointments(DateTime iniciocci, DateTime finalcci, object idppa, string labelcci, string descripcioncci, string lugarcci, string usuasignacion, string asuntocci)
        {
            bool bProcede = false;
            try
            {
                var miSesion = new SessionHandler();
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("es-BO");
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("es-BO");

                var dtpInicio = iniciocci.Date + iniciocci.TimeOfDay;
                var dtpFinal = finalcci.Date + finalcci.TimeOfDay;

                //Insertamos el registro
                var rn = new RnCalCitas();
                var obj = new EntCalCitas();
                obj.idppa = long.Parse(idppa.ToString());
                obj.asuntocci = asuntocci;
                obj.descripcioncci = descripcioncci;
                obj.iniciocci = dtpInicio;
                obj.finalcci = dtpFinal;
                obj.lugarcci = lugarcci;
                obj.labelcci = int.Parse(labelcci);
                obj.statuscci = 2;
                obj.motivofaltacci = "";
                obj.usuasignacion = usuasignacion;
                obj.usucre = "rvera";

                rn.Insert(obj);
                bProcede = true;
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp);
            }
            if (bProcede)
            {
            }
        }

        public void DeleteCustomAppointments(Int64 idcci)
        {
            bool bProcede = false;
            try
            {
                var rn = new RnCalCitas();
                var obj = rn.ObtenerObjeto(idcci);
                rn.Delete(obj);
            }
            catch (Exception exp)
            {
                Console.WriteLine(exp);
            }
            if (bProcede)
            {
            }
        }
    }
}