﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrivateSite.Master" AutoEventWireup="true" CodeBehind="LDashboardMedico.aspx.cs" Inherits="ReAl.GestionMedica.Web.CAL.LDashboardMedico" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="Server">
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/css/bootstrap-select.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.css") %>" rel="stylesheet" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
        <div class="container">
            <div class="page-header-content pt-4">
                <div class="row align-items-center justify-content-between">
                    <div class="col-auto mt-4">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="activity"></i></div>
                            Dashboard
                        </h1>
                        <div class="page-header-subtitle">
                            Bienvenido a NeuroDiagnostico
                        </div>
                    </div>
                    <div class="col-12 col-xl-auto mt-4">
                        <div class="form-inline">
                            <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                                <asp:TextBox ID="dtpNewFechapci" runat="server"
                                    required="" MaxLength="10" data-target="#datetimepicker1"
                                    data-parsley-group="validation-new" data-toggle="datetimepicker"
                                    data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYYY"
                                    Class="form-control datetimepicker-input" CssClass="form-control datetimepicker-input" />
                                <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                            <asp:Button runat="server" ID="btnActualizar" CssClass="btn btn-success"
                                OnClick="btnActualizar_OnClick"
                                Text="Ir" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container mt-n10">
        <div class="row">
            <div class="col-md-8 col-xxl-8 col-xl-8 mb-4">
                <div class="card card-header-actions h-100">
                    <div class="card-header">
                        Citas programadas
                        <div>
                            <asp:LinkButton runat="server" ID="btnSync" Visible="False" Text="Sincronizar Google" CssClass="btn btn-secondary btn-sm" OnClick="btnSync_OnClick" />
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="timeline timeline-xs">
                            <asp:Repeater ID="dtCitas" runat="server" OnItemCommand="dtCitas_OnItemCommand">
                                <ItemTemplate>
                                    <div class="timeline-item">
                                        <div class="timeline-item-marker">
                                            <div class="timeline-item-marker-text">
                                                <%# DataBinder.Eval(Container.DataItem, "hora") %>
                                            </div>
                                            <div class="timeline-item-marker-indicator bg-blue"></div>
                                        </div>
                                        <div class="timeline-item-content">
                                            <asp:LinkButton ID="lnkSeleccionar" CssClass="text-dark"
                                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "idcci") %>'
                                                CommandName="seleccionar"
                                                runat="server"><%#Eval("descripcion")%></asp:LinkButton>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
            <div class=" col-md-4 col-xxl-4 col-xl-4 mb-4">
                <div class="card bg-primary text-white mb-4">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="mr-3">
                                <div class="text-white-75 small">Citas en el mes</div>
                                <div class="text-lg font-weight-bold">
                                    <asp:Literal runat="server" ID="litTotalCitas" Text="0"></asp:Literal>
                                </div>
                            </div>
                            <i class="feather-xl text-white-50" data-feather="calendar"></i>
                        </div>
                    </div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <asp:LinkButton runat="server" ID="lnkPacientes" Text="Ver pacientes"
                            CssClass="small text-white stretched-link"
                            OnClick="lnkPacientes_OnClick" />
                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
                <div class="card bg-success text-white mb-4">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="mr-3">
                                <div class="text-white-75 small">Ingresos en el mes</div>
                                <div class="text-lg font-weight-bold">
                                    <asp:Literal runat="server" ID="litTotalIngresos" Text="0.00"></asp:Literal>
                                </div>
                            </div>
                            <i class="feather-xl text-white-50" data-feather="dollar-sign"></i>
                        </div>
                    </div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <asp:LinkButton runat="server" ID="lnkReporteIngresos" Text="Ver reporte"
                            CssClass="small text-white stretched-link"
                            OnClick="lnkReporteIngresos_OnClick" />
                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
                <div class="card bg-warning text-white mb-4">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <div class="mr-3">
                                <div class="text-white-75 small">Gastos en el mes</div>
                                <div class="text-lg font-weight-bold">
                                    <asp:Literal runat="server" ID="litTotalGastos" Text="0.00"></asp:Literal>
                                </div>
                            </div>
                            <i class="feather-xl text-white-50" data-feather="dollar-sign"></i>
                        </div>
                    </div>
                    <div class="card-footer d-flex align-items-center justify-content-between">
                        <asp:LinkButton runat="server" ID="lnkReporteGastos" Text="Ver reporte"
                            CssClass="small text-white stretched-link"
                            OnClick="lnkReporteGastos_OnClick" />
                        <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Example Charts for Dashboard Demo-->
        <div class="row">
            <div class="col-xl-6 mb-4">
                <div class="card card-header-actions h-100">
                    <div class="card-header">
                        Consultas durante el año
                    </div>
                    <div class="card-body">

                        <div id="morris-linea"></div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 mb-4">
                <div class="card card-header-actions h-100">
                    <div class="card-header">
                        Ingresos - Gastos (en el mes)
                    </div>
                    <div class="card-body">

                        <div id="morris-torta"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="Server">
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/popper/popper.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/locale/bootstrap-table-es-MX.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/js/bootstrap-select.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/moment/moment.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.js") %>"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

    <%--<script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/raphael/raphael-min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/morrisjs/morris.min.js") %>"></script>--%>

    <script>
        function dibujarBarras() {
            var IndexToMonth = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"];

            $("#morris-linea").empty();
            $.getJSON("../API/Citas", function (json) { // callback function which gets called when your request completes.
                Morris.Line({
                    element: 'morris-linea',
                    data: json,
                    xkey: 'Mes',
                    xLabels: 'Mes',
                    xLabelFormat: function (x) {
                        var month = IndexToMonth[x.getMonth()];
                        var year = x.getFullYear();
                        return year + ' ' + month;
                    },
                    ykeys: ['Citas'],
                    labels: ['Citas'],
                    lineColors: ['blue'],
                    hideHover: 'auto',
                    resize: true
                });
            });
        }

        function dibujarTorta() {
            $("#morris-torta").empty();
            $.getJSON("../API/Gastos", function (json) { // callback function which gets called when your request completes.
                Morris.Donut({
                    element: 'morris-torta',
                    data: json,
                    resize: true
                });
            });
        }

        $(document).ready(function () {
            $('select').selectpicker();
            $('[data-toggle="popover"]').popover();
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        function pageLoad() {
            $(function () {
                $('select').selectpicker();
                $('[data-toggle="popover"]').popover();
                $('#datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                dibujarBarras();
                dibujarTorta();
            });
        }
    </script>
</asp:Content>