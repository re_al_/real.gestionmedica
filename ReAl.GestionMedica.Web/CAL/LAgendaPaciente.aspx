﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrivateSite.Master" AutoEventWireup="true" CodeBehind="LAgendaPaciente.aspx.cs" Inherits="ReAl.GestionMedica.Web.CAL.LAgendaPaciente"  UICulture="es" Culture="es-BO" %>

<%@ Register Assembly="DevExpress.Web.Bootstrap.v22.1" Namespace="DevExpress.Web.Bootstrap" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
        <div class="container-fluid">
            <div class="page-header-content pt-4">
                <div class="row align-items-center justify-content-between">
                    <div class="col-auto mt-4">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="file"></i></div>
                            Agenda
                        </h1>
                        <%--<div class="page-header-subtitle">Use this blank page as a starting point for creating new pages inside your project!</div>--%>
                    </div>
                    <%--<div class="col-12 col-xl-auto mt-4">Optional page header content</div>--%>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-fluid">
        <div class="card mt-n10">
            <div class="card-header">Revisa la agenda</div>
            <div class="card-body">
                <div class="datatable">
                    <dx:BootstrapScheduler ID="SchedulerDateNavigator" runat="server"
                        AppointmentDataSourceID="ObjectDataSourceAppointment"
                        ResourceDataSourceID="ObjectDataSourceResources"
                        ActiveViewType="Month"
                        OnInitAppointmentDisplayText="SchedulerDateNavigator_InitAppointmentDisplayText"
                        ClientIDMode="AutoID" GroupType="Resource">
                        <OptionsEditing AllowAppointmentDelete="All" />
                        <Storage>
                            <Appointments AutoRetrieveId="True">
                                <Mappings
                                    AppointmentId="idcci"
                                    End="finalcci"
                                    Start="iniciocci"
                                    Subject="asuntocci"
                                    Description="descripcioncci"
                                    Location="lugarcci"
                                    Status="idppa"
                                    Label="labelcci"
                                    ResourceId="usuasignacion" />
                                <Labels>
                                    <dx:BootstrapAppointmentLabel Text="Solicitado" 
                                        BackgroundCssClass="bg-primary" TextCssClass="text-white"></dx:BootstrapAppointmentLabel>                                    
                                    <dx:BootstrapAppointmentLabel Text="No se presento"
                                        BackgroundCssClass="bg-danger" TextCssClass="text-white"></dx:BootstrapAppointmentLabel>
                                    <dx:BootstrapAppointmentLabel Text="Programado"
                                        BackgroundCssClass="bg-success" TextCssClass="text-white"></dx:BootstrapAppointmentLabel>                                    
                                    <dx:BootstrapAppointmentLabel Text="Atendido"
                                        BackgroundCssClass="bg-secondary" TextCssClass="text-white"></dx:BootstrapAppointmentLabel>                                    
                                </Labels>
                                <Statuses>
                                    <dx:BootstrapAppointmentStatus Text="NotSet"
                                                                   Type="Custom" CssClass="bg-white"></dx:BootstrapAppointmentStatus>
                                    <dx:BootstrapAppointmentStatus Text="Confirmed"
                                                                   Type="Custom" CssClass="bg-success"></dx:BootstrapAppointmentStatus>
                                    <dx:BootstrapAppointmentStatus Text="Awaiting Confirmation"
                                                                   Type="Custom" CssClass="bg-primary"></dx:BootstrapAppointmentStatus>
                                    <dx:BootstrapAppointmentStatus Text="Cancelled"
                                                                   Type="Custom" CssClass="bg-secondary"></dx:BootstrapAppointmentStatus>
                                </Statuses>
                            </Appointments>
                            <Resources>
                                <Mappings
                                    Caption="Nombre"
                                    ResourceId="Login" />
                            </Resources>
                        </Storage>
                        <Views>
                            <DayView ResourcesPerPage="4" TimeScale="00:30:00">
                                <AppointmentDisplayOptions AppointmentAutoHeight="true" />
                                <VisibleTime Start="08:00:00"
                                    End="19:00:00"></VisibleTime>
                            </DayView>
                            <WorkWeekView ResourcesPerPage="1">
                            </WorkWeekView>
                            <WeekView Enabled="false" />
                            <MonthView ResourcesPerPage="1" ShowWeekend="false">
                                <AppointmentDisplayOptions StartTimeVisibility="Never" EndTimeVisibility="Never" StatusDisplayType="Bounds" ShowRecurrence="true" />
                            </MonthView>
                            <TimelineView ResourcesPerPage="2" IntervalCount="5">
                                <CellAutoHeightOptions Mode="FitToContent" />
                            </TimelineView>
                            <AgendaView Enabled="false" />
                        </Views>
                        <OptionsResourceNavigator Mode="Tokens">
                            <%--<SettingsTokens ShowResourceColor="true" />--%>
                        </OptionsResourceNavigator>
                    </dx:BootstrapScheduler>

                    <asp:ObjectDataSource ID="ObjectDataSourceResources" runat="server"
                        SelectMethod="GetResourcesDataTable"
                        TypeName="ReAl.GestionMedica.Web.CAL.ResourceDataSource"
                        OnObjectCreating="ObjectDataSourceResources_ObjectCreating"></asp:ObjectDataSource>

                    <asp:ObjectDataSource ID="ObjectDataSourceAppointment" runat="server"
                        SelectMethod="GetAppointmentsPacDataTable"
                        UpdateMethod="UpdateCustomAppointments"
                        InsertMethod="InsertCustomAppointments"
                        DeleteMethod="DeleteCustomAppointments"
                        TypeName="ReAl.GestionMedica.Web.CAL.AppointmentsPacDataSource"
                        OnObjectCreating="ObjectDataSourceAppointment_ObjectCreating"></asp:ObjectDataSource>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="Server">
</asp:Content>