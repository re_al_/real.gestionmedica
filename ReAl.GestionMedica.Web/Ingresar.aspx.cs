﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Web.App_Helpers;

namespace ReAl.GestionMedica.Web
{
    public partial class Ingresar : System.Web.UI.Page
    {
        #region Methods

        protected void btnLogin_OnClick(object sender, EventArgs e)
        {
            string returnUrl = "";
            bool bProcede = false;
            string strTextoValidacion = "";

            var miUsuario = new EntSegUsuarios();
            var miUsuarioRestriccion = new EntSegUsuariosrestriccion();
            var miRol = new EntSegRoles();
            if (AutenticacionHelper.WebValidarUsuario(ref strTextoValidacion, ref txtUsuario, ref txtPass) && AutenticacionHelper.AutenticarUsuario(ref strTextoValidacion, txtUsuario.Text, txtPass.Text, ref miUsuario, ref miUsuarioRestriccion, ref miRol))
            {
                SessionHandler miSesion = new SessionHandler();
                miSesion.AppUsuario = miUsuario;
                miSesion.AppRestriccion = miUsuarioRestriccion;
                miSesion.AppRol = miRol;

                int minutosSession = 1200;
                string sKey = txtUsuario.Text;
                string roles = miRol.siglasro;

                HttpContext.Current.Cache.Insert(sKey, sKey);

                // Se crea el ticket de autenticación
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(2,             //Version del Ticket
                                                        txtUsuario.Text,         //ID de usuario asociado al ticket
                                                        DateTime.Now,                           //fecha de creacion del ticket
                                                        DateTime.Now.AddMinutes(minutosSession),            //expiracion de la cookie
                                                        false,                   // Si el usuario cliquó en "Recuérdame" la cookie no expira.
                                                        roles,                                  // Almacena datos del usuario, en este caso los roles
                                                        FormsAuthentication.FormsCookiePath);   // El path de la cookie especificado en el Web.Config

                // Se encripta el ticket para añadir más seguridad
                string cticket = FormsAuthentication.Encrypt(ticket);

                //Creamos una cookie con el ticket encriptado
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, cticket);

                //Adicionamos la cookie creada al cliente
                Response.Cookies.Add(cookie);

                //Culture es-BO
                Session.LCID = 16394;

                //Cargamos el Menu
                miSesion.DtMenu = AutenticacionHelper.ObtenerMenu();

                returnUrl = SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu) + "?msg=Bienvenido al Sistema";

                //Guardamos el LOG
                /*
                RnSegLogs rnLog = new rnSegLogs();
                entSegLogs objLog = new entSegLogs();
                objLog.tipo = "INGRESO-OK";
                objLog.ip = getIp();
                objLog.url = Page.Request.Url.ToString();
                objLog.navegador = getBrowser();
                objLog.mensaje = "";
                objLog.usucre = miSesion.appUsuario.login;
                rnLog.Insert(objLog);
                */

                //Google

                var auth = new CGoogleApi();
                if (auth.VerificarUsuario(Server.MapPath("~/google/"), miSesion.AppUsuario.loginsus))
                {
                    try
                    {
                        //var credencial = auth.ObtenerCrearCredencial(Server.MapPath("~/google/"), miSesion.AppUsuario.loginsus);
                        var credencial = auth.ObtenerCrearCredencialWeb(Server.MapPath("~/google/"), miSesion.AppUsuario.loginsus);
                        miSesion.GAppsCredential = credencial;
                        miSesion.GAppsUsuario = auth.ObtenerUsuario(credencial);
                    }
                    catch (Exception exception)
                    {
                        var credPath = Server.MapPath("~/google/") + "/" + miSesion.AppUsuario.loginsus + "_token.json";
                        if (!Directory.Exists(credPath))
                        {
                            Directory.CreateDirectory(credPath);
                        }
                    }
                }

                bProcede = true;
            }
            else
            {
                //Guardamos el LOG
                /*
                rnSegLogs rnLog = new rnSegLogs();
                entSegLogs objLog = new entSegLogs();
                objLog.tipo = "INGRESO-ERROR";
                objLog.ip = getIp();
                objLog.url = Page.Request.Url.ToString();
                objLog.navegador = getBrowser();
                objLog.mensaje = strTextoValidacion;
                objLog.usucre = txtUsuario.Text;
                rnLog.Insert(objLog);
                */

                /*
                pnlError.Visible = true;
                lblError.Text = strTextoValidacion;
                */
            }

            if (bProcede)
            {
                if (returnUrl == null) returnUrl = "~/RES/Index.aspx";
                Response.Redirect(returnUrl);
            }
        }

        protected void btnIngreso_Click(object sender, EventArgs e)
        {
            string returnUrl = "";
            bool bProcede = false;
            string strTextoValidacion = "";

            var miPaciente = new EntPacPacientes();
            var miUsuario = new EntSegUsuarios();
            var miUsuarioRestriccion = new EntSegUsuariosrestriccion();
            var miRol = new EntSegRoles();
            if (AutenticacionHelper.WebValidarPaciente(ref strTextoValidacion, ref txtIdPpa) && AutenticacionHelper.AutenticarPaciente(ref strTextoValidacion, txtIdPpa.Text, ref miPaciente, ref miUsuario, ref miUsuarioRestriccion, ref miRol))
            {
                SessionHandler miSesion = new SessionHandler();
                miSesion.AppUsuario = miUsuario;
                miSesion.AppRestriccion = miUsuarioRestriccion;
                miSesion.AppRol = miRol;

                int minutosSession = 1200;
                string sKey = txtUsuario.Text;
                string roles = miRol.siglasro;

                HttpContext.Current.Cache.Insert(sKey, sKey);

                // Se crea el ticket de autenticación
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(2,             //Version del Ticket
                                                        txtUsuario.Text,         //ID de usuario asociado al ticket
                                                        DateTime.Now,                           //fecha de creacion del ticket
                                                        DateTime.Now.AddMinutes(minutosSession),            //expiracion de la cookie
                                                        false,                   // Si el usuario cliquó en "Recuérdame" la cookie no expira.
                                                        roles,                                  // Almacena datos del usuario, en este caso los roles
                                                        FormsAuthentication.FormsCookiePath);   // El path de la cookie especificado en el Web.Config

                // Se encripta el ticket para añadir más seguridad
                string cticket = FormsAuthentication.Encrypt(ticket);

                //Creamos una cookie con el ticket encriptado
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, cticket);

                //Adicionamos la cookie creada al cliente
                Response.Cookies.Add(cookie);

                //Culture es-BO
                Session.LCID = 16394;

                //Cargamos el Menu
                miSesion.DtMenu = AutenticacionHelper.ObtenerMenu();

                returnUrl = SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu) + "?msg=Bienvenido al Sistema";

                //Guardamos el LOG
                /*
                RnSegLogs rnLog = new rnSegLogs();
                entSegLogs objLog = new entSegLogs();
                objLog.tipo = "INGRESO-OK";
                objLog.ip = getIp();
                objLog.url = Page.Request.Url.ToString();
                objLog.navegador = getBrowser();
                objLog.mensaje = "";
                objLog.usucre = miSesion.appUsuario.login;
                rnLog.Insert(objLog);
                */
                bProcede = true;
            }
            else
            {
                //Guardamos el LOG
                /*
                rnSegLogs rnLog = new rnSegLogs();
                entSegLogs objLog = new entSegLogs();
                objLog.tipo = "INGRESO-ERROR";
                objLog.ip = getIp();
                objLog.url = Page.Request.Url.ToString();
                objLog.navegador = getBrowser();
                objLog.mensaje = strTextoValidacion;
                objLog.usucre = txtUsuario.Text;
                rnLog.Insert(objLog);
                */

                /*
                pnlError.Visible = true;
                lblError.Text = strTextoValidacion;
                */
            }

            if (bProcede)
            {                
                Response.Redirect("~/CAL/LAgendaPaciente.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #endregion Methods

    }
}