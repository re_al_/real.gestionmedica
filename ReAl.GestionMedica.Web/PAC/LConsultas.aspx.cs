﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Web.App_Helpers;
using ReAl.Utils;

namespace ReAl.GestionMedica.Web.PAC
{
    public partial class LConsultas : System.Web.UI.Page
    {
        #region Methods

        public void CargarListado()
        {
            try
            {
                SessionHandler miSesion = new SessionHandler();
                String strNombreSp = "spdcoselgrid";

                var arrNomParam = new ArrayList();
                arrNomParam.Add(EntPacPacientes.Fields.idppa.ToString());
                var arrValParam = new ArrayList();
                arrValParam.Add(miSesion.AppPaciente.idppa);

                var rn = new RnVista();
                var dt = rn.ObtenerDatosProcAlm(strNombreSp, arrNomParam, arrValParam);
                dtgListado.DataSource = dt;
                dtgListado.DataBind();
                ControlHelper.CrearEstilosGrid(ref dtgListado);
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        protected void btnAtras_OnClick(object sender, EventArgs e)
        {
            SessionHandler miSesion = new SessionHandler();
            Response.Redirect(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));
        }

        protected void btnCertificados_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LCertificados.aspx");
        }

        protected void btnCirugias_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LCirugias.aspx");
        }

        protected void btnConsultas_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LConsultas.aspx");
        }

        protected void btnEditaGuardar_OnClick(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var miSesion = new SessionHandler();
                var rn = new RnPacConsultas();
                var objPag = rn.ObtenerObjeto(rn.GetColumnType(hdnIdDatos.Value, EntPacConsultas.Fields.idpco));

                objPag.fechapco = rn.GetColumnType(dtpEditFechapco.Text, EntPacConsultas.Fields.fechapco);
                objPag.horapco = rn.GetColumnType(dtpEditHorapco.Text, EntPacConsultas.Fields.horapco);
                objPag.cuadroclinicopco = rn.GetColumnType(txtEditCuadroclinicopco.Text, EntPacConsultas.Fields.cuadroclinicopco);
                objPag.diagnosticopco = rn.GetColumnType(txtEditDiagnosticopco.Text, EntPacConsultas.Fields.diagnosticopco);
                objPag.idcie = rn.GetColumnType(ddlEditIdcie.SelectedValue, EntPacConsultas.Fields.idcie);
                objPag.conductapco = rn.GetColumnType(txtEditConductapco.Text, EntPacConsultas.Fields.conductapco);
                objPag.tratamientopco = rn.GetColumnType(txtEditTratamientopco.Text, EntPacConsultas.Fields.tratamientopco);
                objPag.apitransaccion = CApi.Transaccion.MODIFICAR.ToString();
                objPag.usumod = miSesion.AppUsuario.loginsus;

                rn.Update(objPag);
                SiteHelper.VerificarCita(miSesion);
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }
            if (bProcede)
                Response.Redirect(
                    Request.AppRelativeCurrentExecutionFilePath +
                    "?msg=Se ha modificado el registro satisfactoriamente");
        }

        protected void btnHistoria_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LHistoria.aspx");
        }

        protected void btnImprimir_OnClick(object sender, EventArgs e)
        {
            try
            {
                SessionHandler miSesion = new SessionHandler();
                string strTitulo = "Consultas: " + miSesion.AppPaciente.nombresppa + " " + miSesion.AppPaciente.appaternoppa;
                String strNombreSp = "spdcoselgrid";

                var arrNomParam = new ArrayList();
                arrNomParam.Add(EntPacPacientes.Fields.idppa.ToString());
                var arrValParam = new ArrayList();
                arrValParam.Add(miSesion.AppPaciente.idppa);

                var rn = new RnVista();
                var dtReporte = rn.ObtenerDatosProcAlm(strNombreSp, arrNomParam, arrValParam);

                foreach (var dataTableCol in dtReporte.Columns.Cast<DataColumn>().ToList())
                {
                    if (dataTableCol.ColumnName.ToUpper().Contains("idpco".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("apiestado".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("apitransaccion".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("usucre".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("feccre".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("usumod".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("fecmod".ToUpper()))
                        dtReporte.Columns.Remove(dataTableCol.ColumnName);
                }

                String strNombreReporte = "Reporte-" + DateTime.Now.ToString("yyyy-MM-dd HH-mm") + ".xlsx";
                string template = Server.MapPath("~/Templates/Reporte.xlsx");

                using (XLWorkbook wb = new XLWorkbook(template))
                {
                    wb.Worksheets.Worksheet(1).Cell(5, 1).Value = strTitulo;
                    wb.Worksheets.Worksheet(1).Cell(6, 1).Value = "Elaborado por: " + miSesion.AppUsuario.nombresus + " " + miSesion.AppUsuario.apellidosus;
                    wb.Worksheets.Worksheet(1).Cell(7, 1).Value = "Fecha: " + DateTime.Now.ToString(CParametros.ParFormatoFechaHora);

                    wb.Worksheets.Worksheet(1).Cell(9, 1).InsertTable(dtReporte);
                    wb.Worksheets.Worksheet(1).Table("Table1").ShowAutoFilter = true;
                    wb.Worksheets.Worksheet(1).Columns(2, 2 + dtReporte.Columns.Count).AdjustToContents();
                    wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    wb.Style.Font.Bold = true;

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename=\"" + strNombreReporte + "\"");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }
        }

        protected void btnInformes_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LInformes.aspx");
        }

        protected void btnLiberar_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LLiberar.aspx");
        }

        protected void btnMultimedia_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LMultimedia.aspx");
        }

        protected void btnNewGuardar_OnClick(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var miSesion = new SessionHandler();
                var rn = new RnPacConsultas();
                var objPag = new EntPacConsultas();
                objPag.idppa = miSesion.AppPaciente.idppa;
                objPag.fechapco = rn.GetColumnType(dtpNewFechapco.Text, EntPacConsultas.Fields.fechapco);
                objPag.horapco = rn.GetColumnType(dtpNewHorapco.Text, EntPacConsultas.Fields.horapco);
                objPag.cuadroclinicopco = rn.GetColumnType(txtNewCuadroclinicopco.Text, EntPacConsultas.Fields.cuadroclinicopco);
                objPag.diagnosticopco = rn.GetColumnType(txtNewDiagnosticopco.Text, EntPacConsultas.Fields.diagnosticopco);
                objPag.idcie = rn.GetColumnType(ddlNewIdcie.SelectedValue, EntPacConsultas.Fields.idcie);
                objPag.conductapco = rn.GetColumnType(txtNewConductapco.Text, EntPacConsultas.Fields.conductapco);
                objPag.tratamientopco = rn.GetColumnType(txtNewTratamientopco.Text, EntPacConsultas.Fields.tratamientopco);

                objPag.usucre = miSesion.AppUsuario.loginsus;

                rn.Insert(objPag);
                SiteHelper.VerificarCita(miSesion);
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (bProcede)
                Response.Redirect(
                    Request.AppRelativeCurrentExecutionFilePath +
                    "?msg=Se ha creado el registro satisfactoriamente");
        }

        protected void btnNuevo_OnClick(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "newModal", "$('#newModal').modal('show');", true);
        }

        protected void btnRecetas_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LRecetas.aspx");
        }

        protected void dtgListado_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            String strRedireccion = "";
            try
            {
                if (e.CommandName.Equals("detalles"))
                {
                    string strId = e.CommandArgument.ToString();

                    //Filtramos el Dataset
                    DataView dv = ((DataTable)dtgListado.DataSource).DefaultView;
                    dv.RowFilter = EntPacConsultas.Fields.idpco + " = " + strId;

                    DataTable detailTable = dv.ToTable();

                    dtgDetalles.DataSource = detailTable;
                    dtgDetalles.DataBind();
                    dtgDetalles.HeaderRow.TableSection = TableRowSection.TableHeader;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#currentdetail').appendTo('body').modal('show');", true);
                }
                else if (e.CommandName.Equals("modificar"))
                {
                    var strId = e.CommandArgument.ToString();
                    var rn = new RnPacConsultas();
                    EntPacConsultas objPag = rn.ObtenerObjeto(rn.GetColumnType(strId, EntPacConsultas.Fields.idpco));
                    hdnIdDatos.Value = objPag.idpco.ToString();
                    dtpEditFechapco.Text = ((DateTime)objPag.fechapco).Date.ToString(CParametros.ParFormatoFecha);
                    dtpEditHorapco.Text = ((DateTime)objPag.horapco).ToString("HH:mm");
                    txtEditCuadroclinicopco.Text = objPag.cuadroclinicopco;
                    txtEditDiagnosticopco.Text = objPag.diagnosticopco;
                    ddlEditIdcie.SelectedValue = objPag.idcie;
                    txtEditConductapco.Text = objPag.conductapco;
                    txtEditTratamientopco.Text = objPag.tratamientopco;
                    lblResult.Visible = false;

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("$('#editModal').modal('show');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);
                }
                else if (e.CommandName.Equals("eliminar"))
                {
                    string strId = e.CommandArgument.ToString();

                    var rn = new RnPacConsultas();
                    var obj = rn.ObtenerObjeto(rn.GetColumnType(strId, EntPacConsultas.Fields.idpco));

                    if (obj != null)
                    {
                        rn.Delete(obj);
                        strRedireccion = "~/PAC/LConsultas?msg=Se ha ELIMINADO el registro satisfactoriamente";
                    }
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (!string.IsNullOrEmpty(strRedireccion))
                Response.Redirect(strRedireccion);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                SessionHandler miSesion = new SessionHandler();
                //if (miSesion.ArrMenu != null)
                //    if (!miSesion.ArrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath.Split('/').Last()))
                //        Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));

                if (miSesion.AppPaciente == null)
                    Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));

                if (!Page.IsPostBack)
                {
                    litPacienteTit.Text = miSesion.AppPaciente.nombresppa + " " + miSesion.AppPaciente.appaternoppa + " " +
                                          miSesion.AppPaciente.apmaternoppa;

                    litPacienteSub.Text = miSesion.AppPaciente.nombresppa + " " + miSesion.AppPaciente.appaternoppa + " " +
                                          miSesion.AppPaciente.apmaternoppa;

                    dtpNewFechapco.Text = DateTime.Now.ToString(CParametros.ParFormatoFecha);
                    dtpNewHorapco.Text = DateTime.Now.ToString(CParametros.ParFormatoFechaHora).Replace(dtpNewFechapco.Text, "");
                    CargarCmbidcie();
                    CargarListado();
                }
                else
                {
                    CargarListado();
                    ControlHelper.CrearEstilosGrid(ref dtgListado);
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        private void CargarCmbidcie()
        {
            try
            {
                var rn = new RnClaCie();
                var dt = rn.ObtenerDataTable();
                ddlNewIdcie.DataValueField = EntClaCie.Fields.codcie.ToString();
                ddlNewIdcie.DataTextField = EntClaCie.Fields.desccie.ToString();
                ddlNewIdcie.DataSource = dt;
                ddlNewIdcie.DataBind();

                ddlEditIdcie.DataValueField = EntClaCie.Fields.codcie.ToString();
                ddlEditIdcie.DataTextField = EntClaCie.Fields.desccie.ToString();
                ddlEditIdcie.DataSource = dt;
                ddlEditIdcie.DataBind();

                if (dt.Rows.Count > 0)
                {
                    ddlNewIdcie.SelectedValue = "-N/A-";
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        #endregion Methods
    }
}