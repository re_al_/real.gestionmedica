﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrivateSite.Master" AutoEventWireup="true" CodeBehind="LInformes.aspx.cs" Inherits="ReAl.GestionMedica.Web.PAC.LInformes" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="Server">
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/css/bootstrap-select.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.css") %>" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- Page Heading -->
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
        <div class="container-fluid">
            <div class="page-header-content pt-4">
                <div class="row align-items-center justify-content-between">
                    <div class="col-auto mt-4">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="user"></i></div>
                            <asp:Literal ID="litPacienteTit" runat="server" />
                        </h1>
                        <div class="page-header-subtitle">
                            <asp:LinkButton runat="server" ID="btnHistoria" CssClass="btn btn-light lift"
                                        Text="Historia" OnClick="btnHistoria_OnClick" />
                            <asp:LinkButton runat="server" ID="btnConsultas" CssClass="btn btn-light lift"
                                        Text="Consultas" OnClick="btnConsultas_OnClick" />
                            <asp:LinkButton runat="server" ID="btnRecetas" CssClass="btn btn-light lift"
                                        Text="Recetas" OnClick="btnRecetas_OnClick" />
                            <asp:LinkButton runat="server" ID="btnCirugias" CssClass="btn btn-light lift"
                                        Text="Cirugias" OnClick="btnCirugias_OnClick" />
                            <asp:LinkButton runat="server" ID="btnInformes" CssClass="btn btn-success lift"
                                        Text="Informes" OnClick="btnInformes_OnClick" />
                            <asp:LinkButton runat="server" ID="btnMultimedia" CssClass="btn btn-light lift"
                                        Text="Multimedia" OnClick="btnMultimedia_OnClick" />
                            <asp:LinkButton runat="server" ID="btnCertificados" CssClass="btn btn-light lift"
                                            Text="Certificados" OnClick="btnCertificados_OnClick" />
                            <asp:LinkButton runat="server" ID="btnLiberar" CssClass="btn btn-danger lift"
                                        Text="Liberar" OnClick="btnLiberar_OnClick" />
                        </div>
                    </div>
                    <%--<div class="col-12 col-xl-auto mt-4">Optional page header content</div>--%>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-fluid">
        <div class="card mt-n10">
            <div class="card-header">
                Informes médicos para:
                <asp:Literal ID="litPacienteSub" runat="server" />
            </div>
            <div class="card-body">
                <p>
                    <!-- Botones de Accion -->
                    <asp:LinkButton ID="btnAtras" runat="server" Text="<i class='fa fa-arrow-left'></i>" ToolTip="Atras"
                        CssClass="btn btn-secondary " class="btn btn-secondary" OnClick="btnAtras_OnClick" />
                    <asp:LinkButton ID="btnNuevo" runat="server" Text="<i class='fa fa-plus-circle'></i>"
                        ToolTip="Nuevo registro"
                        CssClass="btn btn-secondary" class="btn btn-secondary" OnClick="btnNuevo_OnClick" />
                    <asp:LinkButton ID="btnImprimir" runat="server" Text="<i class='fa fa-print'></i>"
                        ToolTip="Imprimir registros"
                        CssClass="btn btn-secondary" class="btn btn-secondary" OnClick="btnImprimir_OnClick" />
                </p>
                <div class="mb-4"></div>
                <div class="text-sm">
                    <asp:GridView ID="dtgListado" runat="server" AutoGenerateColumns="False"
                        data-toggle="table" data-show-columns="true" data-pagination="true"
                        data-search="true" data-show-toggle="true" data-sortable="true"
                        data-page-size="25" data-pagination-v-align="both" data-show-export="true"
                        DataKeyNames="" OnRowCommand="dtgListado_OnRowCommand"
                        CssClass="table table-striped table-bordered table-hover">

                        <Columns>
                            <asp:BoundField ReadOnly="True" DataField="Plantilla" HeaderText="Plantilla" ShowHeader="false">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Fecha" HeaderText="Fecha" ShowHeader="false" HtmlEncode="False" DataFormatString="{0:dd/MM/yyyy}">
                                <ItemStyle HorizontalAlign="Justify"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Texto" HeaderText="Texto" ShowHeader="false" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:TemplateField HeaderText="Acciones">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="ImbAdjunto" CommandName="imprimir" runat="server"
                                                    CommandArgument='<%# Bind("idpin") %>'
                                                    ToolTip="Imprimir"
                                                    CssClass="btn btn-info btn-sm" Text="<i class='fa fa-print'></i>" />

                                    <asp:LinkButton ID="ImbModificar" CommandName="modificar" CommandArgument='<%# Bind("idpin") %>' runat="server"
                                        ToolTip="Modificar el registro"
                                        CssClass="btn btn-secondary btn-sm" Text="<i class='fa fa-edit'></i>" />

                                    <asp:LinkButton ID="ImbEliminar" CommandName="eliminar" CommandArgument='<%# Bind("idpin") %>' runat="server"
                                        ToolTip="Eliminar el registro" OnClientClick="return confirm('¿Esta seguro que desea eliminar el registro?');"
                                        CssClass="btn btn-danger btn-sm" Text="<i class='fa fa-eraser'></i>" />
                                    <button type="button" class="btn btn-warning btn-sm" data-container="body"
                                            data-toggle="popover"
                                            data-placement="left" data-html="true" title="Auditoria"
                                            data-content='<b>Usuario Creacion:</b> <%# Eval("usucre") %> <br />
                                                        <b>Fecha Creacion:</b> <%# Eval("feccre") %> <br />
                                                        <b>Usuario Modificacion:</b> <%# Eval("usumod") %> <br />
                                                        <b>Fecha Modificacion:</b> <%# Eval("fecmod") %> <br />
                                                        <b>Estado:</b> <%# Eval("apiestado") %> '>
                                        <i class='fa fa-search-plus'></i>
                                    </button>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

    <!-- Detail Modal -->
    <div class="modal fade" id="currentdetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <asp:UpdatePanel ID="updModaleDetail" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Detalle del registro</h4>
                        </div>
                        <div class="modal-body">
                            <asp:UpdatePanel ID="updVerDetalle" runat="server">
                                <ContentTemplate>
                                    <asp:DetailsView ID="dtgDetalles" runat="server"
                                                     CssClass="table table-striped table-bordered table-hover"
                                                     FieldHeaderStyle-Font-Bold="true" AutoGenerateRows="True">
                                        <Fields>
                                        </Fields>
                                    </asp:DetailsView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="dtgListado" EventName="RowCommand" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- New Modal -->
    <div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <asp:UpdatePanel ID="updModalNew" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header bg-success">
                            <h5 class="modal-title text-white">Nuevo Informe Médico</h5>
                            <button class="close text-white" type="button" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="lblNewIdcpl" >Plantilla:</label>
                                        <asp:DropDownList ID="cmbNewIdcpl" runat="server"
                                                          AutoPostBack="True"
                                                          class="form-control selectpicker" data-live-search="true"
                                                          data-select-on-tab="true" data-size="15"
                                                          data-none-selected-text="No existen datos"
                                                          OnSelectedIndexChanged="cmbNewIdcpl_OnSelectedIndexChanged"
                                                          CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label ID="lblNewFechapin">Fecha:</label>
                                        <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                                            <asp:TextBox id="dtpNewFechapin" runat="server"
                                                         required="" MaxLength="10" data-target="#datetimepicker1"
                                                         data-parsley-group="validation-new" data-toggle="datetimepicker"
                                                         data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYYY"
                                                         Class="form-control datetimepicker-input" CssClass="form-control datetimepicker-input" />
                                            <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <CKEditor:CKEditorControl ID="txtNewCuerpocpl" runat="server"
                                                                  ToolbarCanCollapse="False"
                                                                  BasePath="/assets/sbadmin2/vendor/ckeditor/" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-warning" type="button" data-dismiss="modal">Cerrar</button>
                            <asp:Button ID="btnNewGuardar" runat="server" Text="Registrar nuevo"
                                        OnClientClick="return $('#aspnetForm').parsley().validate({group: 'validation-new'});"
                                        CssClass="btn btn-success" OnClick="btnNewGuardar_OnClick" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnNuevo" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnNewGuardar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <asp:UpdatePanel ID="updModaleEdit" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header bg-secondary">
                            <h5 class="modal-title text-white" id="exampleModalCenterTitle">Editar informe médico</h5>
                            <asp:HiddenField ID="hdnIdDatos" runat="server" />
                            <button class="close text-white" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="lblNewIdcpl" >Plantilla:</label>
                                        <asp:DropDownList ID="cmbEditIdcpl" runat="server"
                                                          AutoPostBack="True"
                                                          class="form-control selectpicker" data-live-search="true"
                                                          data-select-on-tab="true" data-size="15"
                                                          data-none-selected-text="No existen datos"
                                                          OnSelectedIndexChanged="cmbEditIdcpl_OnSelectedIndexChanged"
                                                          CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label ID="lblNewFechapin">Fecha:</label>
                                        <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                                            <asp:TextBox id="dtpEditFechapin" runat="server"
                                                         required="" MaxLength="10" data-target="#datetimepicker2"
                                                         data-parsley-group="validation-edit" data-toggle="datetimepicker"
                                                         data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYYY"
                                                         Class="form-control datetimepicker-input" CssClass="form-control datetimepicker-input" />
                                            <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <CKEditor:CKEditorControl ID="txtEditCuerpocpl" runat="server"
                                                                  ToolbarCanCollapse="False"
                                                                  BasePath="/assets/sbadmin2/vendor/ckeditor/" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-warning" type="button" data-dismiss="modal">Cancelar</button>
                            <asp:Label ID="lblResult" Visible="false" runat="server" />
                            <asp:Button ID="btnEditaGuardar" runat="server" Text="Guardar Cambios" CssClass="btn btn-secondary" OnClick="btnEditaGuardar_OnClick" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="dtgListado" EventName="RowCommand" />
                    <asp:AsyncPostBackTrigger ControlID="btnEditaGuardar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="Server">
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/popper/popper.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/locale/bootstrap-table-es-MX.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/js/bootstrap-select.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/moment/moment.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.js") %>"></script>

    <script>
        $(document).ready(function () {
            $('select').selectpicker();
            $('[data-toggle="popover"]').popover();
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        function pageLoad() {
            $(function () {
                $('select').selectpicker();
                $('[data-toggle="popover"]').popover();
                $('#datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#datetimepicker2').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        }
    </script>
</asp:Content>