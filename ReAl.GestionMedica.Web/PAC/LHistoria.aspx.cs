﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Web.App_Helpers;
using ReAl.Utils;

namespace ReAl.GestionMedica.Web.PAC
{
    public partial class LHistoria : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                SessionHandler miSesion = new SessionHandler();
                //if (miSesion.ArrMenu != null)
                //    if (!miSesion.ArrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath.Split('/').Last()))
                //        Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));

                if (miSesion.AppPaciente == null)
                    Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));

                if (!Page.IsPostBack)
                {
                    litPacienteTit.Text = miSesion.AppPaciente.nombresppa + " " + miSesion.AppPaciente.appaternoppa + " " +
                                          miSesion.AppPaciente.apmaternoppa;

                    litPacienteSub.Text = miSesion.AppPaciente.nombresppa + " " + miSesion.AppPaciente.appaternoppa + " " +
                                          miSesion.AppPaciente.apmaternoppa;

                    CargarDatosHistoria();
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        private void CargarDatosHistoria()
        {
            try
            {
                SessionHandler miSesion = new SessionHandler();
                var rn1 = new RnPacHistoriaclinica1();
                var rn2 = new RnPacHistoriaclinica2();
                var rn3 = new RnPacHistoriaclinica3();

                EntPacHistoriaclinica1 objPag1 = rn1.ObtenerObjeto(miSesion.AppPaciente.idppa);
                EntPacHistoriaclinica2 objPag2 = rn2.ObtenerObjeto(miSesion.AppPaciente.idppa);
                EntPacHistoriaclinica3 objPag3 = rn3.ObtenerObjeto(miSesion.AppPaciente.idppa);

                txtNew_h1motivophc.Text = objPag1.h1motivophc;
                txtNew_h1enfermedadphc.Text = objPag1.h1enfermedadphc;
                txtNew_h1cirujiasphc.Text = objPag1.h1cirujiasphc;
                txtNew_h1infeccionesphc.Text = objPag1.h1infeccionesphc;
                txtNew_h1traumatismosphc.Text = objPag1.h1traumatismosphc;
                txtNew_h1cardiovascularphc.Text = objPag1.h1cardiovascularphc;
                txtNew_h1metabolicophc.Text = objPag1.h1metabolicophc;
                txtNew_h1toxicophc.Text = objPag1.h1toxicophc;
                txtNew_h1familiarphc.Text = objPag1.h1familiarphc;
                txtNew_h1ginecoobsphc.Text = objPag1.h1ginecoobsphc;
                txtNew_h1otrosphc.Text = objPag1.h1otrosphc;
                txtNew_h1embarazosphc.Text = objPag1.h1embarazosphc.ToString();
                txtNew_h1gestaphc.Text = objPag1.h1gestaphc.ToString();
                txtNew_h1abortosphc.Text = objPag1.h1abortosphc.ToString();
                txtNew_h1mesntruacionesphc.Text = objPag1.h1mesntruacionesphc;
                txtNew_h1fumphc.Text = objPag1.h1fumphc;
                txtNew_h1fupphc.Text = objPag1.h1fupphc;
                txtNew_h1revisionphc.Text = objPag1.h1revisionphc;
                txtNew_h1examenfisicophc.Text = objPag1.h1examenfisicophc;
                txtNew_h1taphc.Text = objPag1.h1taphc;
                txtNew_h1fcphc.Text = objPag1.h1fcphc;
                txtNew_h1frphc.Text = objPag1.h1frphc;
                txtNew_h1tempphc.Text = objPag1.h1tempphc;
                txtNew_h1tallaphc.Text = objPag1.h1tallaphc;
                txtNew_h1pesophc.Text = objPag1.h1pesophc;
                txtNew_h1pcphc.Text = objPag1.h1pcphc;
                txtNew_h2cabezaphc.Text = objPag1.h2cabezaphc;
                txtNew_h2cuellophc.Text = objPag1.h2cuellophc;
                txtNew_h2toraxphc.Text = objPag1.h2toraxphc;
                txtNew_h2abdomenphc.Text = objPag1.h2abdomenphc;
                txtNew_h2genitourinariophc.Text = objPag1.h2genitourinariophc;
                txtNew_h2extremidadesphc.Text = objPag1.h2extremidadesphc;
                txtNew_h2conalertaphc.Text = objPag1.h2conalertaphc;
                txtNew_h2conconfusionphc.Text = objPag1.h2conconfusionphc;
                txtNew_h2consomnolenciaphc.Text = objPag1.h2consomnolenciaphc;
                txtNew_h2conestuforphc.Text = objPag1.h2conestuforphc;
                txtNew_h2concomaphc.Text = objPag1.h2concomaphc;
                txtNew_h2oriespaciophc.Text = objPag1.h2oriespaciophc;
                txtNew_h2orilugarphc.Text = objPag1.h2orilugarphc;
                txtNew_h2oripersonaphc.Text = objPag1.h2oripersonaphc;
                txtNew_h2razideaphc.Text = objPag1.h2razideaphc;
                txtNew_h2razjuiciophc.Text = objPag1.h2razjuiciophc;
                txtNew_h2razabstraccionphc.Text = objPag1.h2razabstraccionphc;
                txtNew_h2conocimientosgenerales.Text = objPag1.h2conocimientosgenerales;
                txtNew_h2atgdesatentophc.Text = objPag1.h2atgdesatentophc;
                txtNew_h2atgfluctuantephc.Text = objPag1.h2atgfluctuantephc;
                txtNew_h2atgnegativophc.Text = objPag1.h2atgnegativophc;
                txtNew_h2atgnegligentephc.Text = objPag1.h2atgnegligentephc;
                txtNew_h2pagconfusionphc.Text = objPag1.h2pagconfusionphc;
                txtNew_h2pagdeliriophc.Text = objPag1.h2pagdeliriophc;
                txtNew_h2paedelusionesphc.Text = objPag1.h2paedelusionesphc;
                txtNew_h2paeilsuionesphc.Text = objPag1.h2paeilsuionesphc;
                txtNew_h2paealucinacionesphc.Text = objPag1.h2paealucinacionesphc;
                txtNew_h3conrecientephc.Text = objPag1.h3conrecientephc;
                txtNew_h3conremotaphc.Text = objPag1.h3conremotaphc;
                txtNew_h3afeeuforicophc.Text = objPag1.h3afeeuforicophc;
                txtNew_h3afeplanophc.Text = objPag1.h3afeplanophc;
                txtNew_h3afedeprimidophc.Text = objPag1.h3afedeprimidophc;
                txtNew_h3afelabilphc.Text = objPag1.h3afelabilphc;
                txtNew_h3afehostilphc.Text = objPag1.h3afehostilphc;
                txtNew_h3afeinadecuadophc.Text = objPag1.h3afeinadecuadophc;
                txtNew_h3hdoizquierdophc.Text = objPag1.h3hdoizquierdophc;
                txtNew_h3hdoderechophc.Text = objPag1.h3hdoderechophc;
                txtNew_h3cgeexpresionphc.Text = objPag1.h3cgeexpresionphc;
                txtNew_h3cgenominacionphc.Text = objPag1.h3cgenominacionphc;
                txtNew_h3cgerepeticionphc.Text = objPag1.h3cgerepeticionphc;
                txtNew_h3cgecomprensionphc.Text = objPag1.h3cgecomprensionphc;
                txtNew_h3apebrocaphc.Text = objPag1.h3apebrocaphc;
                txtNew_h3apeconduccionphc.Text = objPag1.h3apeconduccionphc;
                txtNew_h3apewernickephc.Text = objPag1.h3apewernickephc;
                txtNew_h3apeglobalphc.Text = objPag1.h3apeglobalphc;
                txtNew_h3apeanomiaphc.Text = objPag1.h3apeanomiaphc;
                txtNew_h3atrmotoraphc.Text = objPag1.h3atrmotoraphc;
                txtNew_h3atrsensitivaphc.Text = objPag1.h3atrsensitivaphc;
                txtNew_h3atrmixtaphc.Text = objPag1.h3atrmixtaphc;
                txtNew_h3ataexpresionphc.Text = objPag1.h3ataexpresionphc;
                txtNew_h3atacomprensionphc.Text = objPag1.h3atacomprensionphc;
                txtNew_h3atamixtaphc.Text = objPag1.h3atamixtaphc;
                txtNew_h3aprmotoraphc.Text = objPag1.h3aprmotoraphc;
                txtNew_h3aprsensorialphc.Text = objPag1.h3aprsensorialphc;
                txtNew_h3lesalexiaphc.Text = objPag1.h3lesalexiaphc;
                txtNew_h3lesagrafiaphc.Text = objPag1.h3lesagrafiaphc;
                txtNew_h3lpdacalculiaphc.Text = objPag1.h3lpdacalculiaphc;
                txtNew_h3lpdagrafiaphc.Text = objPag1.h3lpdagrafiaphc;
                txtNew_h3lpddesorientacionphc.Text = objPag1.h3lpddesorientacionphc;
                txtNew_h3lpdagnosiaphc.Text = objPag1.h3lpdagnosiaphc;
                txtNew_h3agnauditivaphc.Text = objPag1.h3agnauditivaphc;
                txtNew_h3agnvisualphc.Text = objPag1.h3agnvisualphc;
                txtNew_h3agntactilphc.Text = objPag1.h3agntactilphc;
                txtNew_h3aprideacionalphc.Text = objPag1.h3aprideacionalphc;
                txtNew_h3aprideomotoraphc.Text = objPag1.h3aprideomotoraphc;
                txtNew_h3aprdesatencionphc.Text = objPag1.h3aprdesatencionphc;
                txtNew_h3anosognosiaphc.Text = objPag1.h3anosognosiaphc;

                txtNew_h4olfnormalphc.Text = objPag2.h4olfnormalphc;
                txtNew_h4olfanosmiaphc.Text = objPag2.h4olfanosmiaphc;
                txtNew_h4optscodphc.Text = objPag2.h4optscodphc;
                txtNew_h4optscoiphc.Text = objPag2.h4optscoiphc;
                txtNew_h4optccodphc.Text = objPag2.h4optccodphc;
                txtNew_h4optccoiphc.Text = objPag2.h4optccoiphc;
                txtNew_h4fondoodphc.Text = objPag2.h4fondoodphc;
                txtNew_h4fondooiphc.Text = objPag2.h4fondooiphc;
                txtNew_h4campimetriaphc.Text = objPag2.h4campimetriaphc;
                txtNew_h4prosisphc.Text = objPag2.h4prosisphc;
                txtNew_h4posicionojosphc.Text = objPag2.h4posicionojosphc;
                txtNew_h4exoftalmiaphc.Text = objPag2.h4exoftalmiaphc;
                txtNew_h4hornerphc.Text = objPag2.h4hornerphc;
                txtNew_h4movocularesphc.Text = objPag2.h4movocularesphc;
                txtNew_h4nistagmuxphc.Text = objPag2.h4nistagmuxphc;
                txtNew_h4diplopia1phc.Text = objPag2.h4diplopia1phc;
                txtNew_h4diplopia2phc.Text = objPag2.h4diplopia2phc;
                txtNew_h4diplopia3phc.Text = objPag2.h4diplopia3phc;
                txtNew_h4diplopia4phc.Text = objPag2.h4diplopia4phc;
                txtNew_h4diplopia5phc.Text = objPag2.h4diplopia5phc;
                txtNew_h4diplopia6phc.Text = objPag2.h4diplopia6phc;
                txtNew_h4diplopia7phc.Text = objPag2.h4diplopia7phc;
                txtNew_h4diplopia8phc.Text = objPag2.h4diplopia8phc;
                txtNew_h4diplopia9phc.Text = objPag2.h4diplopia9phc;
                txtNew_h4convergenciaphc.Text = objPag2.h4convergenciaphc;
                txtNew_h4acomodacionodphc.Text = objPag2.h4acomodacionodphc;
                txtNew_h4acomodacionoiphc.Text = objPag2.h4acomodacionoiphc;
                txtNew_h4pupulasodphc.Text = objPag2.h4pupulasodphc;
                txtNew_h4pupulasoiphc.Text = objPag2.h4pupulasoiphc;
                txtNew_h4formaphc.Text = objPag2.h4formaphc;
                txtNew_h4fotomotorodphc.Text = objPag2.h4fotomotorodphc;
                txtNew_h4fotomotoroiphc.Text = objPag2.h4fotomotoroiphc;
                txtNew_h4consensualdaiphc.Text = objPag2.h4consensualdaiphc;
                txtNew_h4consensualiadphc.Text = objPag2.h4consensualiadphc;
                txtNew_h4senfacialderphc.Text = objPag2.h4senfacialderphc;
                txtNew_h4senfacializqphc.Text = objPag2.h4senfacializqphc;

                txtNew_h4reflejoderphc.Text = objPag2.h4reflejoderphc;
                txtNew_h4reflejoizqphc.Text = objPag2.h4reflejoizqphc;
                txtNew_h4aberturabocaphc.Text = objPag2.h4aberturabocaphc;
                txtNew_h4movmasticatoriosphc.Text = objPag2.h4movmasticatoriosphc;
                txtNew_h4refmentonianophc.Text = objPag2.h4refmentonianophc;
                txtNew_h4facsimetriaphc.Text = objPag2.h4facsimetriaphc;
                txtNew_h4facmovimientosphc.Text = objPag2.h4facmovimientosphc;
                txtNew_h4facparalisisphc.Text = objPag2.h4facparalisisphc;
                txtNew_h4faccentralphc.Text = objPag2.h4faccentralphc;
                txtNew_h4facperifericaphc.Text = objPag2.h4facperifericaphc;
                txtNew_h4facgustophc.Text = objPag2.h4facgustophc;

                txtNew_h5otoscopiaphc.Text = objPag2.h5otoscopiaphc;
                txtNew_h5aguaudiderphc.Text = objPag2.h5aguaudiderphc;
                txtNew_h5aguaudiizqphc.Text = objPag2.h5aguaudiizqphc;
                txtNew_h5weberphc.Text = objPag2.h5weberphc;
                txtNew_h5rinnephc.Text = objPag2.h5rinnephc;
                txtNew_h5pruebaslabphc.Text = objPag2.h5pruebaslabphc;
                txtNew_h5elevpaladarphc.Text = objPag2.h5elevpaladarphc;
                txtNew_h5uvulaphc.Text = objPag2.h5uvulaphc;
                txtNew_h5refnauceosophc.Text = objPag2.h5refnauceosophc;
                txtNew_h5deglucionphc.Text = objPag2.h5deglucionphc;
                txtNew_h5tonovozphc.Text = objPag2.h5tonovozphc;
                txtNew_h5esternocleidomastoideophc.Text = objPag2.h5esternocleidomastoideophc;
                txtNew_h5trapeciophc.Text = objPag2.h5trapeciophc;
                txtNew_h5desviacionphc.Text = objPag2.h5desviacionphc;
                txtNew_h5atrofiaphc.Text = objPag2.h5atrofiaphc;
                txtNew_h5fasciculacionphc.Text = objPag2.h5fasciculacionphc;
                txtNew_h5fuerzaphc.Text = objPag2.h5fuerzaphc;
                txtNew_h5marchaphc.Text = objPag2.h5marchaphc;
                txtNew_h5tonophc.Text = objPag2.h5tonophc;
                txtNew_h5volumenphc.Text = objPag2.h5volumenphc;
                txtNew_h5fasciculacionesphc.Text = objPag2.h5fasciculacionesphc;
                txtNew_h5fuemuscularphc.Text = objPag2.h5fuemuscularphc;
                txtNew_h5movinvoluntariosphc.Text = objPag2.h5movinvoluntariosphc;
                txtNew_h5equilibratoriaphc.Text = objPag2.h5equilibratoriaphc;
                txtNew_h5rombergphc.Text = objPag2.h5rombergphc;
                txtNew_h5dednarderphc.Text = objPag2.h5dednarderphc;
                txtNew_h5dednarizqphc.Text = objPag2.h5dednarizqphc;
                txtNew_h5deddedderphc.Text = objPag2.h5deddedderphc;
                txtNew_h5deddedizqphc.Text = objPag2.h5deddedizqphc;
                txtNew_h5talrodderphc.Text = objPag2.h5talrodderphc;
                txtNew_h5talrodizqphc.Text = objPag2.h5talrodizqphc;
                txtNew_h5movrapidphc.Text = objPag2.h5movrapidphc;
                txtNew_h5rebotephc.Text = objPag2.h5rebotephc;
                txtNew_h5habilidadespecifphc.Text = objPag2.h5habilidadespecifphc;

                /*
                txtNew_h6reflejos01phc.Text = objPag3.h6reflejos01phc;
                txtNew_h6reflejos02phc.Text = objPag3.h6reflejos02phc;
                txtNew_h6reflejos03phc.Text = objPag3.h6reflejos03phc;
                txtNew_h6reflejos04phc.Text = objPag3.h6reflejos04phc;
                txtNew_h6reflejos05phc.Text = objPag3.h6reflejos05phc;
                txtNew_h6reflejos06phc.Text = objPag3.h6reflejos06phc;
                txtNew_h6reflejos07phc.Text = objPag3.h6reflejos07phc;
                txtNew_h6reflejos08phc.Text = objPag3.h6reflejos08phc;
                txtNew_h6reflejos09phc.Text = objPag3.h6reflejos09phc;
                txtNew_h6reflejos10phc.Text = objPag3.h6reflejos10phc;
                */
                txtNew_h6miotbicderphc.Text = objPag3.h6miotbicderphc;
                txtNew_h6miotbicizqphc.Text = objPag3.h6miotbicizqphc;
                txtNew_h6miottriderphc.Text = objPag3.h6miottriderphc;
                txtNew_h6miottriizqphc.Text = objPag3.h6miottriizqphc;
                txtNew_h6miotestderphc.Text = objPag3.h6miotestderphc;
                txtNew_h6miotestizqphc.Text = objPag3.h6miotestizqphc;
                txtNew_h6miotpatderphc.Text = objPag3.h6miotpatderphc;
                txtNew_h6miotpatizqphc.Text = objPag3.h6miotpatizqphc;
                txtNew_h6miotaquderphc.Text = objPag3.h6miotaquderphc;
                txtNew_h6miotaquizqphc.Text = objPag3.h6miotaquizqphc;
                txtNew_h6patpladerphc.Text = objPag3.h6patpladerphc;
                txtNew_h6patplaizqphc.Text = objPag3.h6patplaizqphc;
                txtNew_h6pathofderphc.Text = objPag3.h6pathofderphc;
                txtNew_h6pathofizqphc.Text = objPag3.h6pathofizqphc;
                txtNew_h6pattroderphc.Text = objPag3.h6pattroderphc;
                txtNew_h6pattroizqphc.Text = objPag3.h6pattroizqphc;
                txtNew_h6patprederphc.Text = objPag3.h6patprederphc;
                txtNew_h6patpreizqphc.Text = objPag3.h6patpreizqphc;
                txtNew_h6patpmenderphc.Text = objPag3.h6patpmenderphc;
                txtNew_h6patpmenizqphc.Text = objPag3.h6patpmenizqphc;
                txtNew_h6supbicderphc.Text = objPag3.h6supbicderphc;
                txtNew_h6supbicizqphc.Text = objPag3.h6supbicizqphc;
                txtNew_h6suptriderphc.Text = objPag3.h6suptriderphc;
                txtNew_h6suptriizqphc.Text = objPag3.h6suptriizqphc;
                txtNew_h6supestderphc.Text = objPag3.h6supestderphc;
                txtNew_h6supestizqphc.Text = objPag3.h6supestizqphc;
                txtNew_h6otrmaxilarphc.Text = objPag3.h6otrmaxilarphc;
                txtNew_h6otrglabelarphc.Text = objPag3.h6otrglabelarphc;
                txtNew_h6sistsensitivophc.Text = objPag3.h6sistsensitivophc;
                txtNew_h6dolorprofphc.Text = objPag3.h6dolorprofphc;
                txtNew_h6posicionphc.Text = objPag3.h6posicionphc;
                txtNew_h6vibracionphc.Text = objPag3.h6vibracionphc;
                txtNew_h6dospuntosphc.Text = objPag3.h6dospuntosphc;
                txtNew_h6extincionphc.Text = objPag3.h6extincionphc;
                txtNew_h6estereognosiaphc.Text = objPag3.h6estereognosiaphc;
                txtNew_h6grafestesiaphc.Text = objPag3.h6grafestesiaphc;
                txtNew_h6localizacionphc.Text = objPag3.h6localizacionphc;
                //txtNew_h6imagen.Text = objPag3.h6imagen;
                txtNew_h7rigideznucaphc.Text = objPag3.h7rigideznucaphc;
                txtNew_h7sigkerningphc.Text = objPag3.h7sigkerningphc;
                txtNew_h7fotofobiaphc.Text = objPag3.h7fotofobiaphc;
                txtNew_h7sigbrudzinskiphc.Text = objPag3.h7sigbrudzinskiphc;
                txtNew_h7hiperestesiaphc.Text = objPag3.h7hiperestesiaphc;
                txtNew_h7pulcarotideosphc.Text = objPag3.h7pulcarotideosphc;
                txtNew_h7pultemporalesphc.Text = objPag3.h7pultemporalesphc;
                txtNew_h7cabezaphc.Text = objPag3.h7cabezaphc;
                txtNew_h7cuellophc.Text = objPag3.h7cuellophc;
                txtNew_h7nerviosperifphc.Text = objPag3.h7nerviosperifphc;
                txtNew_h7colvertebralphc.Text = objPag3.h7colvertebralphc;
                txtNew_h7resumenphc.Text = objPag3.h7resumenphc;
                txtNew_h7diagnosticosphc.Text = objPag3.h7diagnosticosphc;
                txtNew_h7conductaphc.Text = objPag3.h7conductaphc;
                txtNew_h7tratamientophc.Text = objPag3.h7tratamientophc;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        protected void btnAtras_OnClick(object sender, EventArgs e)
        {
            SessionHandler miSesion = new SessionHandler();
            Response.Redirect(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));
        }

        protected void btnHistoria_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LHistoria.aspx");
        }

        protected void btnConsultas_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LConsultas.aspx");
        }

        protected void btnRecetas_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LRecetas.aspx");
        }

        protected void btnCirugias_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LCirugias.aspx");
        }

        protected void btnInformes_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LInformes.aspx");
        }

        protected void btnMultimedia_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LMultimedia.aspx");
        }

        protected void btnLiberar_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LLiberar.aspx");
        }

        protected void btnGuardar_OnClick(object sender, EventArgs e)
        {
            bool bProcede = false;
            var myTrans = new CTrans();
            try
            {
                SessionHandler miSesion = new SessionHandler();
                var rn1 = new RnPacHistoriaclinica1();
                var rn2 = new RnPacHistoriaclinica2();
                var rn3 = new RnPacHistoriaclinica3();

                EntPacHistoriaclinica1 objPag1 = rn1.ObtenerObjeto(miSesion.AppPaciente.idppa);
                EntPacHistoriaclinica2 objPag2 = rn2.ObtenerObjeto(miSesion.AppPaciente.idppa);
                EntPacHistoriaclinica3 objPag3 = rn3.ObtenerObjeto(miSesion.AppPaciente.idppa);

                //pachistoriaclinica1
                objPag1.h1motivophc = rn1.GetColumnType(txtNew_h1motivophc.Text, EntPacHistoriaclinica1.Fields.h1motivophc);
                objPag1.h1enfermedadphc = rn1.GetColumnType(txtNew_h1enfermedadphc.Text, EntPacHistoriaclinica1.Fields.h1enfermedadphc);
                objPag1.h1cirujiasphc = rn1.GetColumnType(txtNew_h1cirujiasphc.Text, EntPacHistoriaclinica1.Fields.h1cirujiasphc);
                objPag1.h1infeccionesphc = rn1.GetColumnType(txtNew_h1infeccionesphc.Text, EntPacHistoriaclinica1.Fields.h1infeccionesphc);
                objPag1.h1traumatismosphc = rn1.GetColumnType(txtNew_h1traumatismosphc.Text, EntPacHistoriaclinica1.Fields.h1traumatismosphc);
                objPag1.h1cardiovascularphc = rn1.GetColumnType(txtNew_h1cardiovascularphc.Text, EntPacHistoriaclinica1.Fields.h1cardiovascularphc);
                objPag1.h1metabolicophc = rn1.GetColumnType(txtNew_h1metabolicophc.Text, EntPacHistoriaclinica1.Fields.h1metabolicophc);
                objPag1.h1toxicophc = rn1.GetColumnType(txtNew_h1toxicophc.Text, EntPacHistoriaclinica1.Fields.h1toxicophc);
                objPag1.h1familiarphc = rn1.GetColumnType(txtNew_h1familiarphc.Text, EntPacHistoriaclinica1.Fields.h1familiarphc);
                objPag1.h1ginecoobsphc = rn1.GetColumnType(txtNew_h1ginecoobsphc.Text, EntPacHistoriaclinica1.Fields.h1ginecoobsphc);
                objPag1.h1otrosphc = rn1.GetColumnType(txtNew_h1otrosphc.Text, EntPacHistoriaclinica1.Fields.h1otrosphc);
                objPag1.h1embarazosphc = rn1.GetColumnType(txtNew_h1embarazosphc.Text, EntPacHistoriaclinica1.Fields.h1embarazosphc);
                objPag1.h1gestaphc = rn1.GetColumnType(txtNew_h1gestaphc.Text, EntPacHistoriaclinica1.Fields.h1gestaphc);
                objPag1.h1abortosphc = rn1.GetColumnType(txtNew_h1abortosphc.Text, EntPacHistoriaclinica1.Fields.h1abortosphc);
                objPag1.h1mesntruacionesphc = rn1.GetColumnType(txtNew_h1mesntruacionesphc.Text, EntPacHistoriaclinica1.Fields.h1mesntruacionesphc);
                objPag1.h1fumphc = rn1.GetColumnType(txtNew_h1fumphc.Text, EntPacHistoriaclinica1.Fields.h1fumphc);
                objPag1.h1fupphc = rn1.GetColumnType(txtNew_h1fupphc.Text, EntPacHistoriaclinica1.Fields.h1fupphc);
                objPag1.h1revisionphc = rn1.GetColumnType(txtNew_h1revisionphc.Text, EntPacHistoriaclinica1.Fields.h1revisionphc);
                objPag1.h1examenfisicophc = rn1.GetColumnType(txtNew_h1examenfisicophc.Text, EntPacHistoriaclinica1.Fields.h1examenfisicophc);
                objPag1.h1taphc = rn1.GetColumnType(txtNew_h1taphc.Text, EntPacHistoriaclinica1.Fields.h1taphc);
                objPag1.h1fcphc = rn1.GetColumnType(txtNew_h1fcphc.Text, EntPacHistoriaclinica1.Fields.h1fcphc);
                objPag1.h1frphc = rn1.GetColumnType(txtNew_h1frphc.Text, EntPacHistoriaclinica1.Fields.h1frphc);
                objPag1.h1tempphc = rn1.GetColumnType(txtNew_h1tempphc.Text, EntPacHistoriaclinica1.Fields.h1tempphc);
                objPag1.h1tallaphc = rn1.GetColumnType(txtNew_h1tallaphc.Text, EntPacHistoriaclinica1.Fields.h1tallaphc);
                objPag1.h1pesophc = rn1.GetColumnType(txtNew_h1pesophc.Text, EntPacHistoriaclinica1.Fields.h1pesophc);
                objPag1.h1pcphc = rn1.GetColumnType(txtNew_h1pcphc.Text, EntPacHistoriaclinica1.Fields.h1pcphc);
                objPag1.h2cabezaphc = rn1.GetColumnType(txtNew_h2cabezaphc.Text, EntPacHistoriaclinica1.Fields.h2cabezaphc);
                objPag1.h2cuellophc = rn1.GetColumnType(txtNew_h2cuellophc.Text, EntPacHistoriaclinica1.Fields.h2cuellophc);
                objPag1.h2toraxphc = rn1.GetColumnType(txtNew_h2toraxphc.Text, EntPacHistoriaclinica1.Fields.h2toraxphc);
                objPag1.h2abdomenphc = rn1.GetColumnType(txtNew_h2abdomenphc.Text, EntPacHistoriaclinica1.Fields.h2abdomenphc);
                objPag1.h2genitourinariophc = rn1.GetColumnType(txtNew_h2genitourinariophc.Text, EntPacHistoriaclinica1.Fields.h2genitourinariophc);
                objPag1.h2extremidadesphc = rn1.GetColumnType(txtNew_h2extremidadesphc.Text, EntPacHistoriaclinica1.Fields.h2extremidadesphc);
                objPag1.h2conalertaphc = rn1.GetColumnType(txtNew_h2conalertaphc.Text, EntPacHistoriaclinica1.Fields.h2conalertaphc);
                objPag1.h2conconfusionphc = rn1.GetColumnType(txtNew_h2conconfusionphc.Text, EntPacHistoriaclinica1.Fields.h2conconfusionphc);
                objPag1.h2consomnolenciaphc = rn1.GetColumnType(txtNew_h2consomnolenciaphc.Text, EntPacHistoriaclinica1.Fields.h2consomnolenciaphc);
                objPag1.h2conestuforphc = rn1.GetColumnType(txtNew_h2conestuforphc.Text, EntPacHistoriaclinica1.Fields.h2conestuforphc);
                objPag1.h2concomaphc = rn1.GetColumnType(txtNew_h2concomaphc.Text, EntPacHistoriaclinica1.Fields.h2concomaphc);
                objPag1.h2oriespaciophc = rn1.GetColumnType(txtNew_h2oriespaciophc.Text, EntPacHistoriaclinica1.Fields.h2oriespaciophc);
                objPag1.h2orilugarphc = rn1.GetColumnType(txtNew_h2orilugarphc.Text, EntPacHistoriaclinica1.Fields.h2orilugarphc);
                objPag1.h2oripersonaphc = rn1.GetColumnType(txtNew_h2oripersonaphc.Text, EntPacHistoriaclinica1.Fields.h2oripersonaphc);
                objPag1.h2razideaphc = rn1.GetColumnType(txtNew_h2razideaphc.Text, EntPacHistoriaclinica1.Fields.h2razideaphc);
                objPag1.h2razjuiciophc = rn1.GetColumnType(txtNew_h2razjuiciophc.Text, EntPacHistoriaclinica1.Fields.h2razjuiciophc);
                objPag1.h2razabstraccionphc = rn1.GetColumnType(txtNew_h2razabstraccionphc.Text, EntPacHistoriaclinica1.Fields.h2razabstraccionphc);
                objPag1.h2conocimientosgenerales = rn1.GetColumnType(txtNew_h2conocimientosgenerales.Text, EntPacHistoriaclinica1.Fields.h2conocimientosgenerales);
                objPag1.h2atgdesatentophc = rn1.GetColumnType(txtNew_h2atgdesatentophc.Text, EntPacHistoriaclinica1.Fields.h2atgdesatentophc);
                objPag1.h2atgfluctuantephc = rn1.GetColumnType(txtNew_h2atgfluctuantephc.Text, EntPacHistoriaclinica1.Fields.h2atgfluctuantephc);
                objPag1.h2atgnegativophc = rn1.GetColumnType(txtNew_h2atgnegativophc.Text, EntPacHistoriaclinica1.Fields.h2atgnegativophc);
                objPag1.h2atgnegligentephc = rn1.GetColumnType(txtNew_h2atgnegligentephc.Text, EntPacHistoriaclinica1.Fields.h2atgnegligentephc);
                objPag1.h2pagconfusionphc = rn1.GetColumnType(txtNew_h2pagconfusionphc.Text, EntPacHistoriaclinica1.Fields.h2pagconfusionphc);
                objPag1.h2pagdeliriophc = rn1.GetColumnType(txtNew_h2pagdeliriophc.Text, EntPacHistoriaclinica1.Fields.h2pagdeliriophc);
                objPag1.h2paedelusionesphc = rn1.GetColumnType(txtNew_h2paedelusionesphc.Text, EntPacHistoriaclinica1.Fields.h2paedelusionesphc);
                objPag1.h2paeilsuionesphc = rn1.GetColumnType(txtNew_h2paeilsuionesphc.Text, EntPacHistoriaclinica1.Fields.h2paeilsuionesphc);
                objPag1.h2paealucinacionesphc = rn1.GetColumnType(txtNew_h2paealucinacionesphc.Text, EntPacHistoriaclinica1.Fields.h2paealucinacionesphc);
                objPag1.h3conrecientephc = rn1.GetColumnType(txtNew_h3conrecientephc.Text, EntPacHistoriaclinica1.Fields.h3conrecientephc);
                objPag1.h3conremotaphc = rn1.GetColumnType(txtNew_h3conremotaphc.Text, EntPacHistoriaclinica1.Fields.h3conremotaphc);
                objPag1.h3afeeuforicophc = rn1.GetColumnType(txtNew_h3afeeuforicophc.Text, EntPacHistoriaclinica1.Fields.h3afeeuforicophc);
                objPag1.h3afeplanophc = rn1.GetColumnType(txtNew_h3afeplanophc.Text, EntPacHistoriaclinica1.Fields.h3afeplanophc);
                objPag1.h3afedeprimidophc = rn1.GetColumnType(txtNew_h3afedeprimidophc.Text, EntPacHistoriaclinica1.Fields.h3afedeprimidophc);
                objPag1.h3afelabilphc = rn1.GetColumnType(txtNew_h3afelabilphc.Text, EntPacHistoriaclinica1.Fields.h3afelabilphc);
                objPag1.h3afehostilphc = rn1.GetColumnType(txtNew_h3afehostilphc.Text, EntPacHistoriaclinica1.Fields.h3afehostilphc);
                objPag1.h3afeinadecuadophc = rn1.GetColumnType(txtNew_h3afeinadecuadophc.Text, EntPacHistoriaclinica1.Fields.h3afeinadecuadophc);
                objPag1.h3hdoizquierdophc = rn1.GetColumnType(txtNew_h3hdoizquierdophc.Text, EntPacHistoriaclinica1.Fields.h3hdoizquierdophc);
                objPag1.h3hdoderechophc = rn1.GetColumnType(txtNew_h3hdoderechophc.Text, EntPacHistoriaclinica1.Fields.h3hdoderechophc);
                objPag1.h3cgeexpresionphc = rn1.GetColumnType(txtNew_h3cgeexpresionphc.Text, EntPacHistoriaclinica1.Fields.h3cgeexpresionphc);
                objPag1.h3cgenominacionphc = rn1.GetColumnType(txtNew_h3cgenominacionphc.Text, EntPacHistoriaclinica1.Fields.h3cgenominacionphc);
                objPag1.h3cgerepeticionphc = rn1.GetColumnType(txtNew_h3cgerepeticionphc.Text, EntPacHistoriaclinica1.Fields.h3cgerepeticionphc);
                objPag1.h3cgecomprensionphc = rn1.GetColumnType(txtNew_h3cgecomprensionphc.Text, EntPacHistoriaclinica1.Fields.h3cgecomprensionphc);
                objPag1.h3apebrocaphc = rn1.GetColumnType(txtNew_h3apebrocaphc.Text, EntPacHistoriaclinica1.Fields.h3apebrocaphc);
                objPag1.h3apeconduccionphc = rn1.GetColumnType(txtNew_h3apeconduccionphc.Text, EntPacHistoriaclinica1.Fields.h3apeconduccionphc);
                objPag1.h3apewernickephc = rn1.GetColumnType(txtNew_h3apewernickephc.Text, EntPacHistoriaclinica1.Fields.h3apewernickephc);
                objPag1.h3apeglobalphc = rn1.GetColumnType(txtNew_h3apeglobalphc.Text, EntPacHistoriaclinica1.Fields.h3apeglobalphc);
                objPag1.h3apeanomiaphc = rn1.GetColumnType(txtNew_h3apeanomiaphc.Text, EntPacHistoriaclinica1.Fields.h3apeanomiaphc);
                objPag1.h3atrmotoraphc = rn1.GetColumnType(txtNew_h3atrmotoraphc.Text, EntPacHistoriaclinica1.Fields.h3atrmotoraphc);
                objPag1.h3atrsensitivaphc = rn1.GetColumnType(txtNew_h3atrsensitivaphc.Text, EntPacHistoriaclinica1.Fields.h3atrsensitivaphc);
                objPag1.h3atrmixtaphc = rn1.GetColumnType(txtNew_h3atrmixtaphc.Text, EntPacHistoriaclinica1.Fields.h3atrmixtaphc);
                objPag1.h3ataexpresionphc = rn1.GetColumnType(txtNew_h3ataexpresionphc.Text, EntPacHistoriaclinica1.Fields.h3ataexpresionphc);
                objPag1.h3atacomprensionphc = rn1.GetColumnType(txtNew_h3atacomprensionphc.Text, EntPacHistoriaclinica1.Fields.h3atacomprensionphc);
                objPag1.h3atamixtaphc = rn1.GetColumnType(txtNew_h3atamixtaphc.Text, EntPacHistoriaclinica1.Fields.h3atamixtaphc);
                objPag1.h3aprmotoraphc = rn1.GetColumnType(txtNew_h3aprmotoraphc.Text, EntPacHistoriaclinica1.Fields.h3aprmotoraphc);
                objPag1.h3aprsensorialphc = rn1.GetColumnType(txtNew_h3aprsensorialphc.Text, EntPacHistoriaclinica1.Fields.h3aprsensorialphc);
                objPag1.h3lesalexiaphc = rn1.GetColumnType(txtNew_h3lesalexiaphc.Text, EntPacHistoriaclinica1.Fields.h3lesalexiaphc);
                objPag1.h3lesagrafiaphc = rn1.GetColumnType(txtNew_h3lesagrafiaphc.Text, EntPacHistoriaclinica1.Fields.h3lesagrafiaphc);
                objPag1.h3lpdacalculiaphc = rn1.GetColumnType(txtNew_h3lpdacalculiaphc.Text, EntPacHistoriaclinica1.Fields.h3lpdacalculiaphc);
                objPag1.h3lpdagrafiaphc = rn1.GetColumnType(txtNew_h3lpdagrafiaphc.Text, EntPacHistoriaclinica1.Fields.h3lpdagrafiaphc);
                objPag1.h3lpddesorientacionphc = rn1.GetColumnType(txtNew_h3lpddesorientacionphc.Text, EntPacHistoriaclinica1.Fields.h3lpddesorientacionphc);
                objPag1.h3lpdagnosiaphc = rn1.GetColumnType(txtNew_h3lpdagnosiaphc.Text, EntPacHistoriaclinica1.Fields.h3lpdagnosiaphc);
                objPag1.h3agnauditivaphc = rn1.GetColumnType(txtNew_h3agnauditivaphc.Text, EntPacHistoriaclinica1.Fields.h3agnauditivaphc);
                objPag1.h3agnvisualphc = rn1.GetColumnType(txtNew_h3agnvisualphc.Text, EntPacHistoriaclinica1.Fields.h3agnvisualphc);
                objPag1.h3agntactilphc = rn1.GetColumnType(txtNew_h3agntactilphc.Text, EntPacHistoriaclinica1.Fields.h3agntactilphc);
                objPag1.h3aprideacionalphc = rn1.GetColumnType(txtNew_h3aprideacionalphc.Text, EntPacHistoriaclinica1.Fields.h3aprideacionalphc);
                objPag1.h3aprideomotoraphc = rn1.GetColumnType(txtNew_h3aprideomotoraphc.Text, EntPacHistoriaclinica1.Fields.h3aprideomotoraphc);
                objPag1.h3aprdesatencionphc = rn1.GetColumnType(txtNew_h3aprdesatencionphc.Text, EntPacHistoriaclinica1.Fields.h3aprdesatencionphc);
                objPag1.h3anosognosiaphc = rn1.GetColumnType(txtNew_h3anosognosiaphc.Text, EntPacHistoriaclinica1.Fields.h3anosognosiaphc);
                objPag1.usumod = miSesion.AppUsuario.loginsus;
                objPag1.apitransaccion = CApi.Transaccion.MODIFICAR.ToString();

                //pachistoriaclinica2
                objPag2.h4olfnormalphc = rn2.GetColumnType(txtNew_h4olfnormalphc.Text, EntPacHistoriaclinica2.Fields.h4olfnormalphc);
                objPag2.h4olfanosmiaphc = rn2.GetColumnType(txtNew_h4olfanosmiaphc.Text, EntPacHistoriaclinica2.Fields.h4olfanosmiaphc);
                objPag2.h4optscodphc = rn2.GetColumnType(txtNew_h4optscodphc.Text, EntPacHistoriaclinica2.Fields.h4optscodphc);
                objPag2.h4optscoiphc = rn2.GetColumnType(txtNew_h4optscoiphc.Text, EntPacHistoriaclinica2.Fields.h4optscoiphc);
                objPag2.h4optccodphc = rn2.GetColumnType(txtNew_h4optccodphc.Text, EntPacHistoriaclinica2.Fields.h4optccodphc);
                objPag2.h4optccoiphc = rn2.GetColumnType(txtNew_h4optccoiphc.Text, EntPacHistoriaclinica2.Fields.h4optccoiphc);
                objPag2.h4fondoodphc = rn2.GetColumnType(txtNew_h4fondoodphc.Text, EntPacHistoriaclinica2.Fields.h4fondoodphc);
                objPag2.h4fondooiphc = rn2.GetColumnType(txtNew_h4fondooiphc.Text, EntPacHistoriaclinica2.Fields.h4fondooiphc);
                objPag2.h4campimetriaphc = rn2.GetColumnType(txtNew_h4campimetriaphc.Text, EntPacHistoriaclinica2.Fields.h4campimetriaphc);
                objPag2.h4prosisphc = rn2.GetColumnType(txtNew_h4prosisphc.Text, EntPacHistoriaclinica2.Fields.h4prosisphc);
                objPag2.h4posicionojosphc = rn2.GetColumnType(txtNew_h4posicionojosphc.Text, EntPacHistoriaclinica2.Fields.h4posicionojosphc);
                objPag2.h4exoftalmiaphc = rn2.GetColumnType(txtNew_h4exoftalmiaphc.Text, EntPacHistoriaclinica2.Fields.h4exoftalmiaphc);
                objPag2.h4hornerphc = rn2.GetColumnType(txtNew_h4hornerphc.Text, EntPacHistoriaclinica2.Fields.h4hornerphc);
                objPag2.h4movocularesphc = rn2.GetColumnType(txtNew_h4movocularesphc.Text, EntPacHistoriaclinica2.Fields.h4movocularesphc);
                objPag2.h4nistagmuxphc = rn2.GetColumnType(txtNew_h4nistagmuxphc.Text, EntPacHistoriaclinica2.Fields.h4nistagmuxphc);
                objPag2.h4diplopia1phc = rn2.GetColumnType(txtNew_h4diplopia1phc.Text, EntPacHistoriaclinica2.Fields.h4diplopia1phc);
                objPag2.h4diplopia2phc = rn2.GetColumnType(txtNew_h4diplopia2phc.Text, EntPacHistoriaclinica2.Fields.h4diplopia2phc);
                objPag2.h4diplopia3phc = rn2.GetColumnType(txtNew_h4diplopia3phc.Text, EntPacHistoriaclinica2.Fields.h4diplopia3phc);
                objPag2.h4diplopia4phc = rn2.GetColumnType(txtNew_h4diplopia4phc.Text, EntPacHistoriaclinica2.Fields.h4diplopia4phc);
                objPag2.h4diplopia5phc = rn2.GetColumnType(txtNew_h4diplopia5phc.Text, EntPacHistoriaclinica2.Fields.h4diplopia5phc);
                objPag2.h4diplopia6phc = rn2.GetColumnType(txtNew_h4diplopia6phc.Text, EntPacHistoriaclinica2.Fields.h4diplopia6phc);
                objPag2.h4diplopia7phc = rn2.GetColumnType(txtNew_h4diplopia7phc.Text, EntPacHistoriaclinica2.Fields.h4diplopia7phc);
                objPag2.h4diplopia8phc = rn2.GetColumnType(txtNew_h4diplopia8phc.Text, EntPacHistoriaclinica2.Fields.h4diplopia8phc);
                objPag2.h4diplopia9phc = rn2.GetColumnType(txtNew_h4diplopia9phc.Text, EntPacHistoriaclinica2.Fields.h4diplopia9phc);
                objPag2.h4convergenciaphc = rn2.GetColumnType(txtNew_h4convergenciaphc.Text, EntPacHistoriaclinica2.Fields.h4convergenciaphc);
                objPag2.h4acomodacionodphc = rn2.GetColumnType(txtNew_h4acomodacionodphc.Text, EntPacHistoriaclinica2.Fields.h4acomodacionodphc);
                objPag2.h4acomodacionoiphc = rn2.GetColumnType(txtNew_h4acomodacionoiphc.Text, EntPacHistoriaclinica2.Fields.h4acomodacionoiphc);
                objPag2.h4pupulasodphc = rn2.GetColumnType(txtNew_h4pupulasodphc.Text, EntPacHistoriaclinica2.Fields.h4pupulasodphc);
                objPag2.h4pupulasoiphc = rn2.GetColumnType(txtNew_h4pupulasoiphc.Text, EntPacHistoriaclinica2.Fields.h4pupulasoiphc);
                objPag2.h4formaphc = rn2.GetColumnType(txtNew_h4formaphc.Text, EntPacHistoriaclinica2.Fields.h4formaphc);
                objPag2.h4fotomotorodphc = rn2.GetColumnType(txtNew_h4fotomotorodphc.Text, EntPacHistoriaclinica2.Fields.h4fotomotorodphc);
                objPag2.h4fotomotoroiphc = rn2.GetColumnType(txtNew_h4fotomotoroiphc.Text, EntPacHistoriaclinica2.Fields.h4fotomotoroiphc);
                objPag2.h4consensualdaiphc = rn2.GetColumnType(txtNew_h4consensualdaiphc.Text, EntPacHistoriaclinica2.Fields.h4consensualdaiphc);
                objPag2.h4consensualiadphc = rn2.GetColumnType(txtNew_h4consensualiadphc.Text, EntPacHistoriaclinica2.Fields.h4consensualiadphc);
                objPag2.h4senfacialderphc = rn2.GetColumnType(txtNew_h4senfacialderphc.Text, EntPacHistoriaclinica2.Fields.h4senfacialderphc);
                objPag2.h4senfacializqphc = rn2.GetColumnType(txtNew_h4senfacializqphc.Text, EntPacHistoriaclinica2.Fields.h4senfacializqphc);

                objPag2.h4reflejoderphc = rn2.GetColumnType(txtNew_h4reflejoderphc.Text, EntPacHistoriaclinica2.Fields.h4reflejoderphc);
                objPag2.h4reflejoizqphc = rn2.GetColumnType(txtNew_h4reflejoizqphc.Text, EntPacHistoriaclinica2.Fields.h4reflejoizqphc);
                objPag2.h4aberturabocaphc = rn2.GetColumnType(txtNew_h4aberturabocaphc.Text, EntPacHistoriaclinica2.Fields.h4aberturabocaphc);
                objPag2.h4movmasticatoriosphc = rn2.GetColumnType(txtNew_h4movmasticatoriosphc.Text, EntPacHistoriaclinica2.Fields.h4movmasticatoriosphc);
                objPag2.h4refmentonianophc = rn2.GetColumnType(txtNew_h4refmentonianophc.Text, EntPacHistoriaclinica2.Fields.h4refmentonianophc);
                objPag2.h4facsimetriaphc = rn2.GetColumnType(txtNew_h4facsimetriaphc.Text, EntPacHistoriaclinica2.Fields.h4facsimetriaphc);
                objPag2.h4facmovimientosphc = rn2.GetColumnType(txtNew_h4facmovimientosphc.Text, EntPacHistoriaclinica2.Fields.h4facmovimientosphc);
                objPag2.h4facparalisisphc = rn2.GetColumnType(txtNew_h4facparalisisphc.Text, EntPacHistoriaclinica2.Fields.h4facparalisisphc);
                objPag2.h4faccentralphc = rn2.GetColumnType(txtNew_h4faccentralphc.Text, EntPacHistoriaclinica2.Fields.h4faccentralphc);
                objPag2.h4facperifericaphc = rn2.GetColumnType(txtNew_h4facperifericaphc.Text, EntPacHistoriaclinica2.Fields.h4facperifericaphc);
                objPag2.h4facgustophc = rn2.GetColumnType(txtNew_h4facgustophc.Text, EntPacHistoriaclinica2.Fields.h4facgustophc);

                objPag2.h5otoscopiaphc = rn2.GetColumnType(txtNew_h5otoscopiaphc.Text, EntPacHistoriaclinica2.Fields.h5otoscopiaphc);
                objPag2.h5aguaudiderphc = rn2.GetColumnType(txtNew_h5aguaudiderphc.Text, EntPacHistoriaclinica2.Fields.h5aguaudiderphc);
                objPag2.h5aguaudiizqphc = rn2.GetColumnType(txtNew_h5aguaudiizqphc.Text, EntPacHistoriaclinica2.Fields.h5aguaudiizqphc);
                objPag2.h5weberphc = rn2.GetColumnType(txtNew_h5weberphc.Text, EntPacHistoriaclinica2.Fields.h5weberphc);
                objPag2.h5rinnephc = rn2.GetColumnType(txtNew_h5rinnephc.Text, EntPacHistoriaclinica2.Fields.h5rinnephc);
                objPag2.h5pruebaslabphc = rn2.GetColumnType(txtNew_h5pruebaslabphc.Text, EntPacHistoriaclinica2.Fields.h5pruebaslabphc);
                objPag2.h5elevpaladarphc = rn2.GetColumnType(txtNew_h5elevpaladarphc.Text, EntPacHistoriaclinica2.Fields.h5elevpaladarphc);
                objPag2.h5uvulaphc = rn2.GetColumnType(txtNew_h5uvulaphc.Text, EntPacHistoriaclinica2.Fields.h5uvulaphc);
                objPag2.h5refnauceosophc = rn2.GetColumnType(txtNew_h5refnauceosophc.Text, EntPacHistoriaclinica2.Fields.h5refnauceosophc);
                objPag2.h5deglucionphc = rn2.GetColumnType(txtNew_h5deglucionphc.Text, EntPacHistoriaclinica2.Fields.h5deglucionphc);
                objPag2.h5tonovozphc = rn2.GetColumnType(txtNew_h5tonovozphc.Text, EntPacHistoriaclinica2.Fields.h5tonovozphc);
                objPag2.h5esternocleidomastoideophc = rn2.GetColumnType(txtNew_h5esternocleidomastoideophc.Text, EntPacHistoriaclinica2.Fields.h5esternocleidomastoideophc);
                objPag2.h5trapeciophc = rn2.GetColumnType(txtNew_h5trapeciophc.Text, EntPacHistoriaclinica2.Fields.h5trapeciophc);
                objPag2.h5desviacionphc = rn2.GetColumnType(txtNew_h5desviacionphc.Text, EntPacHistoriaclinica2.Fields.h5desviacionphc);
                objPag2.h5atrofiaphc = rn2.GetColumnType(txtNew_h5atrofiaphc.Text, EntPacHistoriaclinica2.Fields.h5atrofiaphc);
                objPag2.h5fasciculacionphc = rn2.GetColumnType(txtNew_h5fasciculacionphc.Text, EntPacHistoriaclinica2.Fields.h5fasciculacionphc);
                objPag2.h5fuerzaphc = rn2.GetColumnType(txtNew_h5fuerzaphc.Text, EntPacHistoriaclinica2.Fields.h5fuerzaphc);
                objPag2.h5marchaphc = rn2.GetColumnType(txtNew_h5marchaphc.Text, EntPacHistoriaclinica2.Fields.h5marchaphc);
                objPag2.h5tonophc = rn2.GetColumnType(txtNew_h5tonophc.Text, EntPacHistoriaclinica2.Fields.h5tonophc);
                objPag2.h5volumenphc = rn2.GetColumnType(txtNew_h5volumenphc.Text, EntPacHistoriaclinica2.Fields.h5volumenphc);
                objPag2.h5fasciculacionesphc = rn2.GetColumnType(txtNew_h5fasciculacionesphc.Text, EntPacHistoriaclinica2.Fields.h5fasciculacionesphc);
                objPag2.h5fuemuscularphc = rn2.GetColumnType(txtNew_h5fuemuscularphc.Text, EntPacHistoriaclinica2.Fields.h5fuemuscularphc);
                objPag2.h5movinvoluntariosphc = rn2.GetColumnType(txtNew_h5movinvoluntariosphc.Text, EntPacHistoriaclinica2.Fields.h5movinvoluntariosphc);
                objPag2.h5equilibratoriaphc = rn2.GetColumnType(txtNew_h5equilibratoriaphc.Text, EntPacHistoriaclinica2.Fields.h5equilibratoriaphc);
                objPag2.h5rombergphc = rn2.GetColumnType(txtNew_h5rombergphc.Text, EntPacHistoriaclinica2.Fields.h5rombergphc);
                objPag2.h5dednarderphc = rn2.GetColumnType(txtNew_h5dednarderphc.Text, EntPacHistoriaclinica2.Fields.h5dednarderphc);
                objPag2.h5dednarizqphc = rn2.GetColumnType(txtNew_h5dednarizqphc.Text, EntPacHistoriaclinica2.Fields.h5dednarizqphc);
                objPag2.h5deddedderphc = rn2.GetColumnType(txtNew_h5deddedderphc.Text, EntPacHistoriaclinica2.Fields.h5deddedderphc);
                objPag2.h5deddedizqphc = rn2.GetColumnType(txtNew_h5deddedizqphc.Text, EntPacHistoriaclinica2.Fields.h5deddedizqphc);
                objPag2.h5talrodderphc = rn2.GetColumnType(txtNew_h5talrodderphc.Text, EntPacHistoriaclinica2.Fields.h5talrodderphc);
                objPag2.h5talrodizqphc = rn2.GetColumnType(txtNew_h5talrodizqphc.Text, EntPacHistoriaclinica2.Fields.h5talrodizqphc);
                objPag2.h5movrapidphc = rn2.GetColumnType(txtNew_h5movrapidphc.Text, EntPacHistoriaclinica2.Fields.h5movrapidphc);
                objPag2.h5rebotephc = rn2.GetColumnType(txtNew_h5rebotephc.Text, EntPacHistoriaclinica2.Fields.h5rebotephc);
                objPag2.h5habilidadespecifphc = rn2.GetColumnType(txtNew_h5habilidadespecifphc.Text, EntPacHistoriaclinica2.Fields.h5habilidadespecifphc);
                objPag2.usumod = miSesion.AppUsuario.loginsus;
                objPag2.apitransaccion = CApi.Transaccion.MODIFICAR.ToString();

                //pachistoriaclinica3
                /*
                objPag3.h6reflejos01phc = rn3.GetColumnType(txtNew_h6reflejos01phc.Text, EntPacHistoriaclinica3.Fields.h6reflejos01phc);
                objPag3.h6reflejos02phc = rn3.GetColumnType(txtNew_h6reflejos02phc.Text, EntPacHistoriaclinica3.Fields.h6reflejos02phc);
                objPag3.h6reflejos03phc = rn3.GetColumnType(txtNew_h6reflejos03phc.Text, EntPacHistoriaclinica3.Fields.h6reflejos03phc);
                objPag3.h6reflejos04phc = rn3.GetColumnType(txtNew_h6reflejos04phc.Text, EntPacHistoriaclinica3.Fields.h6reflejos04phc);
                objPag3.h6reflejos05phc = rn3.GetColumnType(txtNew_h6reflejos05phc.Text, EntPacHistoriaclinica3.Fields.h6reflejos05phc);
                objPag3.h6reflejos06phc = rn3.GetColumnType(txtNew_h6reflejos06phc.Text, EntPacHistoriaclinica3.Fields.h6reflejos06phc);
                objPag3.h6reflejos07phc = rn3.GetColumnType(txtNew_h6reflejos07phc.Text, EntPacHistoriaclinica3.Fields.h6reflejos07phc);
                objPag3.h6reflejos08phc = rn3.GetColumnType(txtNew_h6reflejos08phc.Text, EntPacHistoriaclinica3.Fields.h6reflejos08phc);
                objPag3.h6reflejos09phc = rn3.GetColumnType(txtNew_h6reflejos09phc.Text, EntPacHistoriaclinica3.Fields.h6reflejos09phc);
                objPag3.h6reflejos10phc = rn3.GetColumnType(txtNew_h6reflejos10phc.Text, EntPacHistoriaclinica3.Fields.h6reflejos10phc);
                */
                objPag3.h6miotbicderphc = rn3.GetColumnType(txtNew_h6miotbicderphc.Text, EntPacHistoriaclinica3.Fields.h6miotbicderphc);
                objPag3.h6miotbicizqphc = rn3.GetColumnType(txtNew_h6miotbicizqphc.Text, EntPacHistoriaclinica3.Fields.h6miotbicizqphc);
                objPag3.h6miottriderphc = rn3.GetColumnType(txtNew_h6miottriderphc.Text, EntPacHistoriaclinica3.Fields.h6miottriderphc);
                objPag3.h6miottriizqphc = rn3.GetColumnType(txtNew_h6miottriizqphc.Text, EntPacHistoriaclinica3.Fields.h6miottriizqphc);
                objPag3.h6miotestderphc = rn3.GetColumnType(txtNew_h6miotestderphc.Text, EntPacHistoriaclinica3.Fields.h6miotestderphc);
                objPag3.h6miotestizqphc = rn3.GetColumnType(txtNew_h6miotestizqphc.Text, EntPacHistoriaclinica3.Fields.h6miotestizqphc);
                objPag3.h6miotpatderphc = rn3.GetColumnType(txtNew_h6miotpatderphc.Text, EntPacHistoriaclinica3.Fields.h6miotpatderphc);
                objPag3.h6miotpatizqphc = rn3.GetColumnType(txtNew_h6miotpatizqphc.Text, EntPacHistoriaclinica3.Fields.h6miotpatizqphc);
                objPag3.h6miotaquderphc = rn3.GetColumnType(txtNew_h6miotaquderphc.Text, EntPacHistoriaclinica3.Fields.h6miotaquderphc);
                objPag3.h6miotaquizqphc = rn3.GetColumnType(txtNew_h6miotaquizqphc.Text, EntPacHistoriaclinica3.Fields.h6miotaquizqphc);
                objPag3.h6patpladerphc = rn3.GetColumnType(txtNew_h6patpladerphc.Text, EntPacHistoriaclinica3.Fields.h6patpladerphc);
                objPag3.h6patplaizqphc = rn3.GetColumnType(txtNew_h6patplaizqphc.Text, EntPacHistoriaclinica3.Fields.h6patplaizqphc);
                objPag3.h6pathofderphc = rn3.GetColumnType(txtNew_h6pathofderphc.Text, EntPacHistoriaclinica3.Fields.h6pathofderphc);
                objPag3.h6pathofizqphc = rn3.GetColumnType(txtNew_h6pathofizqphc.Text, EntPacHistoriaclinica3.Fields.h6pathofizqphc);
                objPag3.h6pattroderphc = rn3.GetColumnType(txtNew_h6pattroderphc.Text, EntPacHistoriaclinica3.Fields.h6pattroderphc);
                objPag3.h6pattroizqphc = rn3.GetColumnType(txtNew_h6pattroizqphc.Text, EntPacHistoriaclinica3.Fields.h6pattroizqphc);
                objPag3.h6patprederphc = rn3.GetColumnType(txtNew_h6patprederphc.Text, EntPacHistoriaclinica3.Fields.h6patprederphc);
                objPag3.h6patpreizqphc = rn3.GetColumnType(txtNew_h6patpreizqphc.Text, EntPacHistoriaclinica3.Fields.h6patpreizqphc);
                objPag3.h6patpmenderphc = rn3.GetColumnType(txtNew_h6patpmenderphc.Text, EntPacHistoriaclinica3.Fields.h6patpmenderphc);
                objPag3.h6patpmenizqphc = rn3.GetColumnType(txtNew_h6patpmenizqphc.Text, EntPacHistoriaclinica3.Fields.h6patpmenizqphc);
                objPag3.h6supbicderphc = rn3.GetColumnType(txtNew_h6supbicderphc.Text, EntPacHistoriaclinica3.Fields.h6supbicderphc);
                objPag3.h6supbicizqphc = rn3.GetColumnType(txtNew_h6supbicizqphc.Text, EntPacHistoriaclinica3.Fields.h6supbicizqphc);
                objPag3.h6suptriderphc = rn3.GetColumnType(txtNew_h6suptriderphc.Text, EntPacHistoriaclinica3.Fields.h6suptriderphc);
                objPag3.h6suptriizqphc = rn3.GetColumnType(txtNew_h6suptriizqphc.Text, EntPacHistoriaclinica3.Fields.h6suptriizqphc);
                objPag3.h6supestderphc = rn3.GetColumnType(txtNew_h6supestderphc.Text, EntPacHistoriaclinica3.Fields.h6supestderphc);
                objPag3.h6supestizqphc = rn3.GetColumnType(txtNew_h6supestizqphc.Text, EntPacHistoriaclinica3.Fields.h6supestizqphc);
                objPag3.h6otrmaxilarphc = rn3.GetColumnType(txtNew_h6otrmaxilarphc.Text, EntPacHistoriaclinica3.Fields.h6otrmaxilarphc);
                objPag3.h6otrglabelarphc = rn3.GetColumnType(txtNew_h6otrglabelarphc.Text, EntPacHistoriaclinica3.Fields.h6otrglabelarphc);
                objPag3.h6sistsensitivophc = rn3.GetColumnType(txtNew_h6sistsensitivophc.Text, EntPacHistoriaclinica3.Fields.h6sistsensitivophc);
                objPag3.h6dolorprofphc = rn3.GetColumnType(txtNew_h6dolorprofphc.Text, EntPacHistoriaclinica3.Fields.h6dolorprofphc);
                objPag3.h6posicionphc = rn3.GetColumnType(txtNew_h6posicionphc.Text, EntPacHistoriaclinica3.Fields.h6posicionphc);
                objPag3.h6vibracionphc = rn3.GetColumnType(txtNew_h6vibracionphc.Text, EntPacHistoriaclinica3.Fields.h6vibracionphc);
                objPag3.h6dospuntosphc = rn3.GetColumnType(txtNew_h6dospuntosphc.Text, EntPacHistoriaclinica3.Fields.h6dospuntosphc);
                objPag3.h6extincionphc = rn3.GetColumnType(txtNew_h6extincionphc.Text, EntPacHistoriaclinica3.Fields.h6extincionphc);
                objPag3.h6estereognosiaphc = rn3.GetColumnType(txtNew_h6estereognosiaphc.Text, EntPacHistoriaclinica3.Fields.h6estereognosiaphc);
                objPag3.h6grafestesiaphc = rn3.GetColumnType(txtNew_h6grafestesiaphc.Text, EntPacHistoriaclinica3.Fields.h6grafestesiaphc);
                objPag3.h6localizacionphc = rn3.GetColumnType(txtNew_h6localizacionphc.Text, EntPacHistoriaclinica3.Fields.h6localizacionphc);
                //objPag3.h6imagen = rn3.GetColumnType(txtNew_h6imagen.Text, EntPacHistoriaclinica3.Fields.h6imagen);
                objPag3.h7rigideznucaphc = rn3.GetColumnType(txtNew_h7rigideznucaphc.Text, EntPacHistoriaclinica3.Fields.h7rigideznucaphc);
                objPag3.h7sigkerningphc = rn3.GetColumnType(txtNew_h7sigkerningphc.Text, EntPacHistoriaclinica3.Fields.h7sigkerningphc);
                objPag3.h7fotofobiaphc = rn3.GetColumnType(txtNew_h7fotofobiaphc.Text, EntPacHistoriaclinica3.Fields.h7fotofobiaphc);
                objPag3.h7sigbrudzinskiphc = rn3.GetColumnType(txtNew_h7sigbrudzinskiphc.Text, EntPacHistoriaclinica3.Fields.h7sigbrudzinskiphc);
                objPag3.h7hiperestesiaphc = rn3.GetColumnType(txtNew_h7hiperestesiaphc.Text, EntPacHistoriaclinica3.Fields.h7hiperestesiaphc);
                objPag3.h7pulcarotideosphc = rn3.GetColumnType(txtNew_h7pulcarotideosphc.Text, EntPacHistoriaclinica3.Fields.h7pulcarotideosphc);
                objPag3.h7pultemporalesphc = rn3.GetColumnType(txtNew_h7pultemporalesphc.Text, EntPacHistoriaclinica3.Fields.h7pultemporalesphc);
                objPag3.h7cabezaphc = rn3.GetColumnType(txtNew_h7cabezaphc.Text, EntPacHistoriaclinica3.Fields.h7cabezaphc);
                objPag3.h7cuellophc = rn3.GetColumnType(txtNew_h7cuellophc.Text, EntPacHistoriaclinica3.Fields.h7cuellophc);
                objPag3.h7nerviosperifphc = rn3.GetColumnType(txtNew_h7nerviosperifphc.Text, EntPacHistoriaclinica3.Fields.h7nerviosperifphc);
                objPag3.h7colvertebralphc = rn3.GetColumnType(txtNew_h7colvertebralphc.Text, EntPacHistoriaclinica3.Fields.h7colvertebralphc);
                objPag3.h7resumenphc = rn3.GetColumnType(txtNew_h7resumenphc.Text, EntPacHistoriaclinica3.Fields.h7resumenphc);
                objPag3.h7diagnosticosphc = rn3.GetColumnType(txtNew_h7diagnosticosphc.Text, EntPacHistoriaclinica3.Fields.h7diagnosticosphc);
                objPag3.h7conductaphc = rn3.GetColumnType(txtNew_h7conductaphc.Text, EntPacHistoriaclinica3.Fields.h7conductaphc);
                objPag3.h7tratamientophc = rn3.GetColumnType(txtNew_h7tratamientophc.Text, EntPacHistoriaclinica3.Fields.h7tratamientophc);
                objPag3.usumod = miSesion.AppUsuario.loginsus;
                objPag3.apitransaccion = CApi.Transaccion.MODIFICAR.ToString();

                rn1.Update(objPag1, ref myTrans);
                rn2.Update(objPag2, ref myTrans);
                rn3.Update(objPag3, ref myTrans);
                myTrans.ConfirmarTransaccion();

                SiteHelper.VerificarCita(miSesion);
                bProcede = true;
            }
            catch (Exception exp)
            {
                myTrans.AnularTransaccion();
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        protected void btnCertificados_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LCertificados.aspx");
        }
    }
}