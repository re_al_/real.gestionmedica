﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrivateSite.Master" AutoEventWireup="true" CodeBehind="LMultimedia.aspx.cs" Inherits="ReAl.GestionMedica.Web.PAC.LMultimedia" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v22.1, Version=22.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v22.1, Version=22.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxe" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v22.1, Version=22.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.Bootstrap" Assembly="DevExpress.Web.Bootstrap.v22.1, Version=22.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>
<%@ Register TagPrefix="dxwgv" Namespace="DevExpress.Web.Bootstrap" Assembly="DevExpress.Web.Bootstrap.v22.1, Version=22.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="Server">
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/css/bootstrap-select.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.css") %>" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- Page Heading -->
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
        <div class="container-fluid">
            <div class="page-header-content pt-4">
                <div class="row align-items-center justify-content-between">
                    <div class="col-auto mt-4">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="user"></i></div>
                            <asp:Literal ID="litPacienteTit" runat="server" />
                        </h1>
                        <div class="page-header-subtitle">
                            <asp:LinkButton runat="server" ID="btnHistoria" CssClass="btn btn-light lift"
                                Text="Historia" OnClick="btnHistoria_OnClick" />
                            <asp:LinkButton runat="server" ID="btnConsultas" CssClass="btn btn-light lift"
                                Text="Consultas" OnClick="btnConsultas_OnClick" />
                            <asp:LinkButton runat="server" ID="btnRecetas" CssClass="btn btn-light lift"
                                Text="Recetas" OnClick="btnRecetas_OnClick" />
                            <asp:LinkButton runat="server" ID="btnCirugias" CssClass="btn btn-light lift"
                                Text="Cirugias" OnClick="btnCirugias_OnClick" />
                            <asp:LinkButton runat="server" ID="btnInformes" CssClass="btn btn-light lift"
                                Text="Informes" OnClick="btnInformes_OnClick" />
                            <asp:LinkButton runat="server" ID="btnMultimedia" CssClass="btn btn-success lift"
                                Text="Multimedia" OnClick="btnMultimedia_OnClick" />
                            <asp:LinkButton runat="server" ID="btnCertificados" CssClass="btn btn-light lift"
                                Text="Certificados" OnClick="btnCertificados_OnClick" />
                            <asp:LinkButton runat="server" ID="btnLiberar" CssClass="btn btn-danger lift"
                                Text="Liberar" OnClick="btnLiberar_OnClick" />
                        </div>
                    </div>
                    <%--<div class="col-12 col-xl-auto mt-4">Optional page header content</div>--%>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-fluid">
        <div class="card mt-n10">
            <div class="card-header">
                Archivos multimedia para:
                <asp:Literal ID="litPacienteSub" runat="server" />
            </div>
            <div class="card-body">
                <p>
                    <!-- Botones de Accion -->
                    <asp:LinkButton ID="btnAtras" runat="server" Text="<i class='fa fa-arrow-left'></i>" ToolTip="Atras"
                        CssClass="btn btn-secondary " class="btn btn-secondary" OnClick="btnAtras_OnClick" />
                    <asp:LinkButton ID="btnNuevo" runat="server" Text="<i class='fa fa-plus-circle'></i>"
                        ToolTip="Nuevo registro"
                        CssClass="btn btn-secondary" class="btn btn-secondary" OnClick="btnNuevo_OnClick" />
                </p>
                <div class="mb-4"></div>
                <div class="text-sm">
                    <dx:BootstrapGridView ID="dtgListado" runat="server" Width="100%"
                        OnCustomButtonCallback="dtgListado_OnCustomButtonCallback" KeyFieldName="idpmu"
                        CssClasses="table table-striped table-bordered table-hover"
                                          ClientInstanceName="dtgListado">
                        <ClientSideEvents EndCallback="OnEndCallback" />
                        <Columns>
                            <dx:BootstrapGridViewDataColumn FieldName="Fecha" />
                            <dx:BootstrapGridViewDataColumn FieldName="Observaciones" />
                            <dx:BootstrapGridViewBinaryImageColumn FieldName="imagenpmu" Caption="Imagen" Width="350px">
                                <PropertiesBinaryImage ImageHeight="350px" />
                            </dx:BootstrapGridViewBinaryImageColumn>
                            <dx:BootstrapGridViewCommandColumn ShowEditButton="false" ShowSelectCheckbox="false" Width="100" MaxWidth="100" Caption="Acciones">
                                <CustomButtons>
                                    <dx:BootstrapGridViewCommandColumnCustomButton IconCssClass="fa fa-download" CssClass="text-info" ID="Descargar" />
                                    <dx:BootstrapGridViewCommandColumnCustomButton IconCssClass="fa fa-edit" CssClass="text-secondary" ID="Modificar" />
                                    <dx:BootstrapGridViewCommandColumnCustomButton IconCssClass="fa fa-eraser" CssClass="text-danger" ID="Eliminar" />
                                </CustomButtons>
                            </dx:BootstrapGridViewCommandColumn>
                        </Columns>
                        <%--BeginHide--%>
                        <SettingsPager NumericButtonCount="4" PageSize="10">
                            <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                        </SettingsPager>
                        <%--EndHide--%>
                        <%--<SettingsSearchPanel Visible="True" ShowApplyButton="True" ShowClearButton="True" />--%>
                        <SettingsBootstrap Striped="true" />
                        <SettingsBehavior EnableRowHotTrack="true" />
                        <CssClasses Table="table table-striped table-bordered table-hover" HeaderRow="text-center text-dark table thead th" />
                    </dx:BootstrapGridView>
                </div>
            </div>
        </div>
    </div>

    <!-- Detail Modal -->
    <div class="modal fade" id="currentdetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <asp:UpdatePanel ID="updModaleDetail" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Detalle del registro</h4>
                        </div>
                        <div class="modal-body">
                            <asp:UpdatePanel ID="updVerDetalle" runat="server">
                                <ContentTemplate>
                                    <asp:DetailsView ID="dtgDetalles" runat="server"
                                        CssClass="table table-striped table-bordered table-hover"
                                        FieldHeaderStyle-Font-Bold="true" AutoGenerateRows="True">
                                        <Fields>
                                        </Fields>
                                    </asp:DetailsView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="dtgListado" EventName="RowCommand" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- New Modal -->
    <div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <asp:UpdatePanel ID="updModalNew" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header bg-success">
                            <h5 class="modal-title text-white">Nuevo archivo multimedia</h5>
                            <button class="close text-white" type="button" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="lblNewIdcpl">Archivo:</label>
                                        <ajaxToolkit:AsyncFileUpload ID="AsyncFileUpload1" runat="server"
                                            CssClass="file text-center center-block well well-sm"
                                            Class="file text-center center-block well well-sm"
                                            multiple="false"
                                            data-show-upload="false" data-show-caption="true"
                                            PersistFile="True" Width="300px" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="lblNewFechapmu">Fecha:</label>
                                        <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                                            <asp:TextBox ID="dtpNewFechapmu" runat="server"
                                                required="" MaxLength="10" data-target="#datetimepicker1"
                                                data-parsley-group="validation-new" data-toggle="datetimepicker"
                                                data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYYY"
                                                Class="form-control datetimepicker-input" CssClass="form-control datetimepicker-input" />
                                            <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label id="lblNewObservacionespmu" runat="server">Observaciones/comentarios:</label>
                                        <asp:TextBox ID="txtNewObservacionespmu"
                                            data-parsley-maxlength="5000" TextMode="MultiLine"
                                            data-parsley-group="validation-new"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="5000" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <asp:CheckBox runat="server" ID="chkSyncGoogle" Text="Subir archivo a GoogleDrive" Visible="False" />
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-warning" type="button" data-dismiss="modal">Cerrar</button>
                            <asp:Button ID="btnNewGuardar" runat="server" Text="Registrar nuevo"
                                OnClientClick="return $('#aspnetForm').parsley().validate({group: 'validation-new'});"
                                CssClass="btn btn-success" OnClick="btnNewGuardar_OnClick" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnNuevo" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnNewGuardar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <asp:UpdatePanel ID="updModaleEdit" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header bg-secondary">
                            <h5 class="modal-title text-white" id="exampleModalCenterTitle">Editar archivo multimedia</h5>
                            <asp:HiddenField ID="hdnIdDatos" runat="server" />
                            <button class="close text-white" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="lblNewFechapmu">Fecha:</label>
                                        <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                                            <asp:TextBox ID="dtpEditFechapmu" runat="server"
                                                required="" MaxLength="10" data-target="#datetimepicker2"
                                                data-parsley-group="validation-edit" data-toggle="datetimepicker"
                                                data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYYY"
                                                Class="form-control datetimepicker-input" CssClass="form-control datetimepicker-input" />
                                            <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label id="lblEditObservacionespmu" runat="server">Observaciones/comentarios:</label>
                                        <asp:TextBox ID="txtEditObservacionespmu" TextMode="MultiLine"
                                            data-parsley-maxlength="5000"
                                            data-parsley-group="validation-edit"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="5000" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-warning" type="button" data-dismiss="modal">Cancelar</button>
                            <asp:Label ID="lblResult" Visible="false" runat="server" />
                            <asp:Button ID="btnEditaGuardar" runat="server" Text="Guardar Cambios" CssClass="btn btn-secondary" OnClick="btnEditaGuardar_OnClick" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="dtgListado" EventName="RowCommand" />
                    <asp:AsyncPostBackTrigger ControlID="btnEditaGuardar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="Server">
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/popper/popper.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/locale/bootstrap-table-es-MX.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/js/bootstrap-select.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/moment/moment.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.js") %>"></script>

    <script>
        $(document).ready(function () {
            $('select').selectpicker();
            $('[data-toggle="popover"]').popover();
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        function pageLoad() {
            $(function () {
                $('select').selectpicker();
                $('[data-toggle="popover"]').popover();
                $('#datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#datetimepicker2').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        }

        function OnEndCallback(s, e) {
            if (dtgListado.cpAccion == "Modificar") {
                <%=hdnIdDatos.ClientID%>.value = dtgListado.cpId;
                <%=dtpEditFechapmu.ClientID%>.value = dtgListado.cpFecha;
                <%=txtEditObservacionespmu.ClientID%>.value = dtgListado.cpObs;
                $('#editModal').modal('show');
            }
            if (dtgListado.cpAccion == "Descargar") {
                window.open('PVerArchivo.aspx?id=' + dtgListado.cpId, '_blank');
            }
            if (dtgListado.cpAccion == "Eliminar") {
                window.location.href = "LMultimedia?msg=Se ha ELIMINADO el registro satisfactoriamente";
            }
        }
    </script>
</asp:Content>