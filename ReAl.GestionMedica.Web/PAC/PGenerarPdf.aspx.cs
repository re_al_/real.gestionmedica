﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.XtraReports.UI;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.Web.App_Helpers;
using ReAl.GestionMedica.Web.App_Reports;
using ReAl.Utils;

namespace ReAl.GestionMedica.Web.PAC
{
    public partial class PGenerarPdf : System.Web.UI.Page
    {
        private string _strTipRep = "";
        private EntPacRecetas _myPre = null;
        private EntPacCertificados _myPcm = null;
        private EntPacCirugias _myPci = null;
        private EntPacInformes _myPin = null;

        private EntClaPlantillas _myCpl = null;
        private readonly DateTime _dateFecIni = DateTime.Now;
        private readonly DateTime _dateFecFin = DateTime.Now;

        protected void Page_Load(object sender, EventArgs e)
        {
            var intId = 0;
            if (Request.QueryString["id"] != null)
                intId = Convert.ToInt32(Request.QueryString["id"]);

            if (Request.QueryString["tp"] != null)
                _strTipRep = Request.QueryString["tp"].ToString();

            switch (_strTipRep)
            {
                case "M":
                    var rnPcm = new RnPacCertificados();
                    _myPcm = rnPcm.ObtenerObjeto(intId);
                    if (_myPcm != null)
                    {
                        CargarReporteCertificadoMedico();
                    }
                    break;

                case "R":
                    var rnEre = new RnPacRecetas();
                    _myPre = rnEre.ObtenerObjeto(intId);
                    if (_myPre != null)
                    {
                        var rn = new RnClaPlantillas();
                        _myCpl = rn.ObtenerObjeto(int.Parse(_myPre.idcpl.ToString()));

                        CargarReporteVistaPrevia();
                    }
                    break;

                case "C":
                    var rnPci = new RnPacCirugias();
                    _myPci = rnPci.ObtenerObjeto(intId);
                    if (_myPci != null)
                    {
                        var rn = new RnClaPlantillas();
                        _myCpl = rn.ObtenerObjeto(int.Parse(_myPci.idcpl.ToString()));

                        CargarReporteVistaPrevia();
                    }
                    break;

                case "I":
                    var rnPin = new RnPacInformes();
                    _myPin = rnPin.ObtenerObjeto(intId);
                    if (_myPin != null)
                    {
                        var rn = new RnClaPlantillas();
                        _myCpl = rn.ObtenerObjeto(int.Parse(_myPin.idcpl.ToString()));

                        CargarReporteVistaPrevia();
                    }
                    break;
            }
        }

        private void CargarReporteCertificadoMedico()
        {
            SessionHandler miSession = new SessionHandler();
            var reporte = new rptPcmCertificadoSinMarca();

            //Parametros para no imprimir los WARNINGS del Reporte
            reporte.DefaultPrinterSettingsUsing.UseLandscape = false;
            reporte.DefaultPrinterSettingsUsing.UseMargins = false;
            reporte.DefaultPrinterSettingsUsing.UsePaperKind = false;
            reporte.RequestParameters = false;
            reporte.ShowPrintMarginsWarning = false;
            reporte.PrintingSystem.ShowMarginsWarning = false;

            //El Cuerpo
            var enc = new ASCIIEncoding();
            reporte.txtTexto.Rtf = enc.GetString(_myPcm.rtfpcm, 0, _myPcm.rtfpcm.Length);
            reporte.txtFecha.Text = "La Paz, " + cFuncionesFechas.GetDateLiteral((DateTime)_myPcm.fechapcm);
            reporte.txtMatricula.Text = _myPcm.matriculapcm;
            reporte.txtNombreMedico.Text = miSession.AppUsuario.nombresus + " " + miSession.AppUsuario.apellidosus;
            //printControl1.PrintingSystem = reporte.PrintingSystem;
            reporte.CreateDocument();
            reportViewer.OpenReport(reporte);
        }

        private void CargarReporteVistaPrevia()
        {
            var reporte = new rptPreview();
            reporte.ReportUnit = ReportUnit.TenthsOfAMillimeter;
            reporte.PaperKind = PaperKind.Custom;
            reporte.PageWidth = int.Parse((Math.Round(_myCpl.anchocpl * 100)).ToString());
            reporte.PageHeight = int.Parse((Math.Round(_myCpl.altocpl * 100)).ToString());
            reporte.Margins.Bottom = int.Parse((Math.Round(_myCpl.margeninfcpl * 100 + 100)).ToString());
            reporte.BottomMargin.Height = int.Parse((Math.Round(_myCpl.margeninfcpl * 100 + 100)).ToString());
            reporte.Margins.Top = int.Parse((Math.Round(_myCpl.margensupcpl * 100)).ToString());
            reporte.TopMargin.Height = int.Parse((Math.Round(_myCpl.margensupcpl * 100)).ToString());
            reporte.Margins.Left = int.Parse((Math.Round(_myCpl.margenizqcpl * 100)).ToString());
            reporte.Margins.Right = int.Parse((Math.Round(_myCpl.margendercpl * 100)).ToString());

            //Parametros para no imprimir los WARNINGS del Reporte
            reporte.DefaultPrinterSettingsUsing.UseLandscape = false;
            reporte.DefaultPrinterSettingsUsing.UseMargins = false;
            reporte.DefaultPrinterSettingsUsing.UsePaperKind = false;
            reporte.RequestParameters = false;
            reporte.ShowPrintMarginsWarning = false;
            reporte.PrintingSystem.ShowMarginsWarning = false;

            //Tamaño del TXT
            reporte.txtTexto.WidthF = reporte.PageWidth - reporte.Margins.Left - reporte.Margins.Right - 20;
            reporte.txtTexto.Width = reporte.PageWidth - reporte.Margins.Left - reporte.Margins.Right - 20;
            reporte.txtTexto.HeightF = int.Parse((Math.Round(_myCpl.altocpl * 100)).ToString()) - int.Parse((Math.Round(_myCpl.margeninfcpl * 100 + 100)).ToString()) - int.Parse((Math.Round(_myCpl.margensupcpl * 100)).ToString()) - 30;
            reporte.txtTexto.Height = int.Parse((Math.Round(_myCpl.altocpl * 100)).ToString()) - int.Parse((Math.Round(_myCpl.margeninfcpl * 100 + 100)).ToString()) - int.Parse((Math.Round(_myCpl.margensupcpl * 100)).ToString()) - 30;

            var enc = new ASCIIEncoding();

            switch (_strTipRep)
            {
                case "R":
                    //El Cuerpo
                    reporte.txtTexto.Rtf = enc.GetString(_myPre.rtfpre, 0, _myPre.rtfpre.Length);
                    //reporte.txtTexto.Html = _myPre.htmlpre;
                    reporte.txtFecha.Text = cFuncionesFechas.GetDateLiteral((DateTime)_myPre.fechapre);
                    //printControl1.PrintingSystem = reporte.PrintingSystem;
                    reporte.CreateDocument();
                    reportViewer.OpenReport(reporte);
                    break;

                case "C":
                    //El Cuerpo
                    reporte.txtTexto.Rtf = enc.GetString(_myPci.rtfpci, 0, _myPci.rtfpci.Length);
                    //reporte.txtTexto.Html = _myPci.htmlpci;
                    reporte.txtFecha.Text = cFuncionesFechas.GetDateLiteral((DateTime)_myPci.fechapci);
                    reporte.txtFecha.Visible = false;
                    //printControl1.PrintingSystem = reporte.PrintingSystem;
                    reporte.CreateDocument();
                    reportViewer.OpenReport(reporte);
                    break;

                case "I":
                    reporte.txtTexto.Rtf = enc.GetString(_myPin.rtfpin, 0, _myPin.rtfpin.Length);
                    //reporte.txtTexto.Html = _myPin.htmlpin;
                    reporte.txtFecha.Text = cFuncionesFechas.GetDateLiteral((DateTime)_myPin.fechapin);
                    reporte.txtFecha.Visible = false;
                    //printControl1.PrintingSystem = reporte.PrintingSystem;
                    reporte.CreateDocument();
                    reportViewer.OpenReport(reporte);
                    break;
            }
        }
    }
}