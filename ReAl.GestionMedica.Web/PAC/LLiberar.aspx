﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrivateSite.Master" AutoEventWireup="true" CodeBehind="LLiberar.aspx.cs" Inherits="ReAl.GestionMedica.Web.PAC.LLiberar" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="Server">
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/css/bootstrap-select.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.css") %>" rel="stylesheet" />
    <style>
        .bootstrap-select .dropdown-menu {
            max-width: 100%;
            border: 1px solid cornflowerblue;
            border-radius: 2px;
            box-shadow: 0 2px 8px #888;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

            .bootstrap-select .dropdown-menu li a span.text {
                display: inline-block;
                white-space: pre-wrap;
                word-wrap: break-word;
                max-width: 100%;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <!-- Main page content-->
    <div class="container mt-4">
        <!-- Invoice-->
        <div class="card invoice">
            <div class="card-header p-4 p-md-5 border-bottom-0 bg-gradient-primary-to-secondary text-white-50">
                <div class="row justify-content-between align-items-center">
                    <div class="col-12 col-lg-auto mb-5 mb-lg-0 text-center text-lg-left">
                        <!-- Invoice branding-->
                        <div class="h1 text-white">
                            <asp:Literal ID="litPacienteTit" runat="server" />
                        </div>
                        <asp:LinkButton runat="server" ID="btnHistoria" CssClass="btn btn-light lift"
                            Text="Historia" OnClick="btnHistoria_OnClick" />
                        <asp:LinkButton runat="server" ID="btnConsultas" CssClass="btn btn-light lift"
                            Text="Consultas" OnClick="btnConsultas_OnClick" />
                        <asp:LinkButton runat="server" ID="btnRecetas" CssClass="btn btn-light lift"
                            Text="Recetas" OnClick="btnRecetas_OnClick" />
                        <asp:LinkButton runat="server" ID="btnCirugias" CssClass="btn btn-light lift"
                            Text="Cirugias" OnClick="btnCirugias_OnClick" />
                        <asp:LinkButton runat="server" ID="btnInformes" CssClass="btn btn-light lift"
                            Text="Informes" OnClick="btnInformes_OnClick" />
                        <asp:LinkButton runat="server" ID="btnMultimedia" CssClass="btn btn-light lift"
                            Text="Multimedia" OnClick="btnMultimedia_OnClick" />
                        <asp:LinkButton runat="server" ID="btnLiberar" CssClass="btn btn-danger lift"
                            Text="Liberar" OnClick="btnLiberar_OnClick" />
                    </div>
                    <div class="col-12 col-lg-auto text-center text-lg-right">
                        <!-- Invoice details-->
                        <div class="h5 text-white">Terminar consulta</div>
                        <br />
                        January 1, 2020
                    </div>
                </div>
            </div>
            <div class="card-body p-4 p-md-5">
                <!-- Invoice table-->
                <div class="table-responsive">
                    <table class="table table-borderless mb-0">
                        <thead class="border-bottom">
                            <tr class="small text-uppercase text-muted">
                                <th scope="col">Concepto</th>
                                <th class="text-right" scope="col">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Invoice item 1-->
                            <tr class="border-bottom">
                                <td>
                                    <div class="font-weight-bold">
                                        <asp:Literal ID="litPacienteSub" runat="server" />
                                    </div>
                                    <div class="small text-muted d-none d-md-block">Consulta</div>
                                </td>
                                <td class="text-right font-weight-bold">
                                    <div class="input-group input-group-joined">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Total Bs.
                                            </span>
                                        </div>
                                        <asp:TextBox ID="txtNewMonto" name="txtNewMonto"
                                            placeholder="Monto total" TextMode="Number"
                                            data-parsley-maxlength="6" Text="0"
                                            data-parsley-group="validation-new"
                                            data-parsley-required="false"
                                            ClientIDMode="Static" onKeyUp="calcular();"
                                            Class="form-control" CssClass="form-control"
                                            runat="server" MaxLength="6" />
                                    </div>
                                </td>
                            </tr>
                            <!-- Invoice total-->
                        <tr>
                                <td class="text-right pb-0">&nbsp;
                                </td>
                                <td class="text-right pb-0">
                                    <asp:Button ID="btnFinalizar" runat="server" Text="Terminar consulta"
                                        OnClientClick="return $('#aspnetForm').parsley().validate({group: 'validation-new'});"
                                        CssClass="btn btn-success" OnClick="btnFinalizar_OnClick" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="Server">
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/popper/popper.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/locale/bootstrap-table-es-MX.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/js/bootstrap-select.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/moment/moment.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.js") %>"></script>

    <script>
        $(document).ready(function () {
            $('select').selectpicker();
            $('[data-toggle="popover"]').popover();
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker3').datetimepicker({
                format: 'LT'
            });
            $('#datetimepicker4').datetimepicker({
                format: 'LT'
            });
        });

        function pageLoad() {
            $(function () {
                $('select').selectpicker();
                $('[data-toggle="popover"]').popover();
                $('#datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#datetimepicker2').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#datetimepicker3').datetimepicker({
                    format: 'LT'
                });
                $('#datetimepicker4').datetimepicker({
                    format: 'LT'
                });
            });
        }

        
    </script>
</asp:Content>