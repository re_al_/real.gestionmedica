﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MimeTypes;
using ReAl.GestionMedica.Class.Modelo;

namespace ReAl.GestionMedica.Web.PAC
{
    public partial class PVerArchivo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var intId = 0;
            if (Request.QueryString["id"] != null)
                intId = Convert.ToInt32(Request.QueryString["id"]);

            var rnEre = new RnPacMultimedia();
            var objEre = rnEre.ObtenerObjeto(intId);
            if (objEre != null)
            {
                byte[] archivo = null;
                if (objEre.pathpmu == null)
                {
                    archivo = objEre.imagenpmu;
                }
                else
                {
                    archivo = File.ReadAllBytes(Server.MapPath("~/uploads/") + objEre.pathpmu);
                }

                Response.Charset = "";
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                if (objEre.extensionpmu.ToUpper().Contains(".XLSX"))
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                else if (objEre.extensionpmu.ToUpper().Contains(".XLS"))
                    Response.ContentType = "application/vnd.ms-excel";
                else if (objEre.extensionpmu.ToUpper().Contains(".PDF"))
                    Response.ContentType = "application/pdf";
                else
                {
                    string mimeType = "application/unknown";
                    if (objEre.pathpmu == null)
                    {
                        mimeType = MimeTypeMap.GetMimeType(objEre.extensionpmu);
                    }
                    else
                    {
                        mimeType = MimeMapping.GetMimeMapping(objEre.pathpmu);
                    }

                    Response.ContentType = mimeType;
                }
                //attachment
                Response.AddHeader("content-disposition", "inline;filename=\"" + objEre.pathpmu + "\"");
                Response.BinaryWrite(archivo);
                Response.Flush();
                Response.End();
            }
        }
    }
}