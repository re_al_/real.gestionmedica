﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PGenerarPdf.aspx.cs" Inherits="ReAl.GestionMedica.Web.PAC.PGenerarPdf" %>

<%@ Register Assembly="DevExpress.XtraReports.v22.1.Web.WebForms, Version=22.1.4.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.XtraReports.Web" TagPrefix="dx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <dx:ASPxWebDocumentViewer runat="server" ID="reportViewer">
            </dx:ASPxWebDocumentViewer>
        </div>
    </form>
</body>
</html>