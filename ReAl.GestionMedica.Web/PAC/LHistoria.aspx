﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrivateSite.Master" AutoEventWireup="true" CodeBehind="LHistoria.aspx.cs" Inherits="ReAl.GestionMedica.Web.PAC.LHistoria" ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="Server">
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/css/bootstrap-select.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.css") %>" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- Page Heading -->
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
        <div class="container-fluid">
            <div class="page-header-content pt-4">
                <div class="row align-items-center justify-content-between">
                    <div class="col-auto mt-4">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="user"></i></div>
                            <asp:Literal ID="litPacienteTit" runat="server" />
                        </h1>
                        <div class="page-header-subtitle">
                            <asp:LinkButton runat="server" ID="btnHistoria" CssClass="btn btn-success lift"
                                Text="Historia" OnClick="btnHistoria_OnClick" />
                            <asp:LinkButton runat="server" ID="btnConsultas" CssClass="btn btn-light lift"
                                Text="Consultas" OnClick="btnConsultas_OnClick" />
                            <asp:LinkButton runat="server" ID="btnRecetas" CssClass="btn btn-light lift"
                                Text="Recetas" OnClick="btnRecetas_OnClick" />
                            <asp:LinkButton runat="server" ID="btnCirugias" CssClass="btn btn-light lift"
                                Text="Cirugias" OnClick="btnCirugias_OnClick" />
                            <asp:LinkButton runat="server" ID="btnInformes" CssClass="btn btn-light lift"
                                Text="Informes" OnClick="btnInformes_OnClick" />
                            <asp:LinkButton runat="server" ID="btnMultimedia" CssClass="btn btn-light lift"
                                Text="Multimedia" OnClick="btnMultimedia_OnClick" />
                            <asp:LinkButton runat="server" ID="btnCertificados" CssClass="btn btn-light lift"
                                Text="Certificados" OnClick="btnCertificados_OnClick" />
                            <asp:LinkButton runat="server" ID="btnLiberar" CssClass="btn btn-danger lift"
                                Text="Liberar" OnClick="btnLiberar_OnClick" />
                        </div>
                    </div>
                    <%--<div class="col-12 col-xl-auto mt-4">Optional page header content</div>--%>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-fluid">
        <div class="card mt-n10">
            <div class="card-header">
                Historia clinica de:
                <asp:Literal ID="litPacienteSub" runat="server" />
            </div>
            <div class="card-body">
                <p>
                    <!-- Botones de Accion -->
                    <asp:LinkButton ID="btnAtras" runat="server" Text="<i class='fa fa-arrow-left'></i>" ToolTip="Atras"
                        CssClass="btn btn-secondary " class="btn btn-secondary" OnClick="btnAtras_OnClick" />
                    <asp:LinkButton ID="btnGuardar" runat="server" Text="<i class='fa fa-save'></i>"
                        ToolTip="Guardar historia"
                        CssClass="btn btn-success" class="btn btn-secondary" OnClick="btnGuardar_OnClick" />
                </p>
                <div class="mb-4"></div>
                <div class="card">
                    <div class="card-header border-bottom">
                        <ul class="nav nav-tabs card-header-tabs" id="cardTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="hoja1-tab" href="#hoja1" data-toggle="tab" role="tab" aria-controls="hoja1" aria-selected="true">Hoja 1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="hoja2-tab" href="#hoja2" data-toggle="tab" role="tab" aria-controls="hoja2" aria-selected="false">Hoja 2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="hoja3-tab" href="#hoja3" data-toggle="tab" role="tab" aria-controls="hoja3" aria-selected="false">Hoja 3</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="hoja4-tab" href="#hoja4" data-toggle="tab" role="tab" aria-controls="hoja4" aria-selected="false">Hoja 4</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="hoja5-tab" href="#hoja5" data-toggle="tab" role="tab" aria-controls="hoja5" aria-selected="false">Hoja 5</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="hoja6-tab" href="#hoja6" data-toggle="tab" role="tab" aria-controls="hoja6" aria-selected="false">Hoja 6</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="hoja7-tab" href="#hoja7" data-toggle="tab" role="tab" aria-controls="hoja7" aria-selected="false">Hoja 7</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="cardTabContent">
                            <div class="tab-pane fade show active" id="hoja1" role="tabpanel" aria-labelledby="hoja1-tab">
                                <div class="card-text text-sm">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label id="lbl_h1motivophc" runat="server">Motivo de consulta:</label>
                                                <asp:TextBox ID="txtNew_h1motivophc" TextMode="MultiLine"
                                                    data-parsley-maxlength="5000"
                                                    data-parsley-group="validation-new"
                                                    Class="form-control" CssClass="form-control"
                                                    runat="server" MaxLength="5000" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label id="lbl_h1enfermedadphc" runat="server">Enfermedad actual:</label>
                                                <asp:TextBox ID="txtNew_h1enfermedadphc" TextMode="MultiLine"
                                                    data-parsley-maxlength="5000"
                                                    data-parsley-group="validation-new"
                                                    Class="form-control" CssClass="form-control"
                                                    runat="server" MaxLength="5000" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">Antecedentes patológicos</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="lbl_h1cirujiasphc" runat="server">Cirugías:</label>
                                                    <asp:TextBox ID="txtNew_h1cirujiasphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label id="lbl_h1infeccionesphc" runat="server">Infecciones:</label>
                                                    <asp:TextBox ID="txtNew_h1infeccionesphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="lbl_h1traumatismosphc" runat="server">Traumatismos:</label>
                                                    <asp:TextBox ID="txtNew_h1traumatismosphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label id="lbl_h1cardiovascularphc" runat="server">Cardiovascular:</label>
                                                    <asp:TextBox ID="txtNew_h1cardiovascularphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="lbl_h1metabolicophc" runat="server">Metabólico:</label>
                                                    <asp:TextBox ID="txtNew_h1metabolicophc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label id="lbl_h1toxicophc" runat="server">Toxico:</label>
                                                    <asp:TextBox ID="txtNew_h1toxicophc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="lbl_h1familiarphc" runat="server">Familiar:</label>
                                                    <asp:TextBox ID="txtNew_h1familiarphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                                <div class="form-group">
                                                    <label id="lbl_h1ginecoobsphc" runat="server">Gineco obstétrico:</label>
                                                    <asp:TextBox ID="txtNew_h1ginecoobsphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label id="lbl_h1otrosphc" runat="server">Otros:</label>
                                                    <asp:TextBox ID="txtNew_h1otrosphc" TextMode="MultiLine" Rows="7"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h1embarazosphc" runat="server">Embarazos:</label>
                                                    <asp:TextBox ID="txtNew_h1embarazosphc"
                                                        data-parsley-maxlength="0"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="0" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h1gestaphc" runat="server">Gesta:</label>
                                                    <asp:TextBox ID="txtNew_h1gestaphc"
                                                        data-parsley-maxlength="0"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="0" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h1abortosphc" runat="server">Abortos:</label>
                                                    <asp:TextBox ID="txtNew_h1abortosphc"
                                                        data-parsley-maxlength="0"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="0" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h1mesntruacionesphc" runat="server">Menstruaciones:</label>
                                                    <asp:TextBox ID="txtNew_h1mesntruacionesphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h1fumphc" runat="server">FUM:</label>
                                                    <asp:TextBox ID="txtNew_h1fumphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h1fupphc" runat="server">FUP:</label>
                                                    <asp:TextBox ID="txtNew_h1fupphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label id="lbl_h1revisionphc" runat="server">Revisión por Sistemas:</label>
                                                    <asp:TextBox ID="txtNew_h1revisionphc" TextMode="MultiLine"
                                                        data-parsley-maxlength="5000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="5000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label id="lbl_h1examenfisicophc" runat="server">Examen físico general:</label>
                                                    <asp:TextBox ID="txtNew_h1examenfisicophc" TextMode="MultiLine"
                                                        data-parsley-maxlength="5000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="5000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h1taphc" runat="server">T/A:</label>
                                                    <asp:TextBox ID="txtNew_h1taphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h1fcphc" runat="server">FC:</label>
                                                    <asp:TextBox ID="txtNew_h1fcphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h1frphc" runat="server">FR:</label>
                                                    <asp:TextBox ID="txtNew_h1frphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h1tempphc" runat="server">TEMP:</label>
                                                    <asp:TextBox ID="txtNew_h1tempphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h1tallaphc" runat="server">TALLA:</label>
                                                    <asp:TextBox ID="txtNew_h1tallaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h1pesophc" runat="server">PESO:</label>
                                                    <asp:TextBox ID="txtNew_h1pesophc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h1pcphc" runat="server">PC:</label>
                                                    <asp:TextBox ID="txtNew_h1pcphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="hoja2" role="tabpanel" aria-labelledby="hoja2-tab">
                                <div class="card card-waves">
                                    <div class="card-header">Examen físico segmentario</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="lbl_h2cabezaphc" runat="server">Cabeza:</label>
                                                    <asp:TextBox ID="txtNew_h2cabezaphc" TextMode="MultiLine"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label id="lbl_h2cuellophc" runat="server">Cuello:</label>
                                                    <asp:TextBox ID="txtNew_h2cuellophc" TextMode="MultiLine"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="lbl_h2toraxphc" runat="server">Torax:</label>
                                                    <asp:TextBox ID="txtNew_h2toraxphc" TextMode="MultiLine"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label id="lbl_h2abdomenphc" runat="server">Abdomen:</label>
                                                    <asp:TextBox ID="txtNew_h2abdomenphc" TextMode="MultiLine"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="lbl_h2genitourinariophc" runat="server">Génito urinario:</label>
                                                    <asp:TextBox ID="txtNew_h2genitourinariophc" TextMode="MultiLine"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label id="lbl_h2extremidadesphc" runat="server">Extremidades:</label>
                                                    <asp:TextBox ID="txtNew_h2extremidadesphc" TextMode="MultiLine"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <h2 class="card-title">Examen Neurológico - Funciones cognoscitivas</h2>
                                <div class="card card-waves">
                                    <div class="card-header">I. Funciones intelectuales generales</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title">A. Estado de consciencia</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h2conalertaphc" runat="server">Alerta:</label>
                                                    <asp:TextBox ID="txtNew_h2conalertaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h2conconfusionphc" runat="server">Confusion:</label>
                                                    <asp:TextBox ID="txtNew_h2conconfusionphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h2consomnolenciaphc" runat="server">Somnolencia:</label>
                                                    <asp:TextBox ID="txtNew_h2consomnolenciaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h2conestuforphc" runat="server">Estufor:</label>
                                                    <asp:TextBox ID="txtNew_h2conestuforphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="lbl_h2concomaphc" runat="server">Coma:</label>
                                                    <asp:TextBox ID="txtNew_h2concomaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5 class="card-title">B. Orientación</h5>
                                                <div class="form-group">
                                                    <label id="lbl_h2oriespaciophc" runat="server">Espacio:</label>
                                                    <asp:TextBox ID="txtNew_h2oriespaciophc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>

                                                <div class="form-group">
                                                    <label id="lbl_h2orilugarphc" runat="server">Lugar:</label>
                                                    <asp:TextBox ID="txtNew_h2orilugarphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>

                                                <div class="form-group">
                                                    <label id="lbl_h2oripersonaphc" runat="server">Persona:</label>
                                                    <asp:TextBox ID="txtNew_h2oripersonaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="card-title">C. Razonamiento</h5>
                                                <div class="form-group">
                                                    <label id="lbl_h2razideaphc" runat="server">Idea:</label>
                                                    <asp:TextBox ID="txtNew_h2razideaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>

                                                <div class="form-group">
                                                    <label id="lbl_h2razjuiciophc" runat="server">Juicio:</label>
                                                    <asp:TextBox ID="txtNew_h2razjuiciophc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>

                                                <div class="form-group">
                                                    <label id="lbl_h2razabstraccionphc" runat="server">Abstraccion:</label>
                                                    <asp:TextBox ID="txtNew_h2razabstraccionphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5 class="card-title">D. Conocimientos generales</h5>
                                                <div class="form-group">
                                                    <label id="lbl_h2conocimientosgenerales" runat="server">Conocimientos generales:</label>
                                                    <asp:TextBox ID="txtNew_h2conocimientosgenerales" TextMode="MultiLine" Rows="7"
                                                        data-parsley-maxlength="5000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="5000" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="card-title">F.1. Percepción - Alteraciones generalizadas</h5>
                                                <div class="form-group">
                                                    <label id="lbl_h2pagconfusionphc" runat="server">Confusión generalizada:</label>
                                                    <asp:TextBox ID="txtNew_h2pagconfusionphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>

                                                <div class="form-group">
                                                    <label id="lbl_h2pagdeliriophc" runat="server">Delirio:</label>
                                                    <asp:TextBox ID="txtNew_h2pagdeliriophc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h5 class="card-title">E. Atención global</h5>
                                                <div class="form-group">
                                                    <label id="lbl_h2atgdesatentophc" runat="server">Desatento:</label>
                                                    <asp:TextBox ID="txtNew_h2atgdesatentophc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>

                                                <div class="form-group">
                                                    <label id="lbl_h2atgfluctuantephc" runat="server">Fluctuante:</label>
                                                    <asp:TextBox ID="txtNew_h2atgfluctuantephc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>

                                                <div class="form-group">
                                                    <label id="lbl_h2atgnegativophc" runat="server">Negativo:</label>
                                                    <asp:TextBox ID="txtNew_h2atgnegativophc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>

                                                <div class="form-group">
                                                    <label id="lbl_h2atgnegligentephc" runat="server">Negligente:</label>
                                                    <asp:TextBox ID="txtNew_h2atgnegligentephc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <h5 class="card-title">F.2. Percepción - Alteraciones específicas</h5>
                                                <div class="form-group">
                                                    <label id="lbl_h2paedelusionesphc" runat="server">Delusiones:</label>
                                                    <asp:TextBox ID="txtNew_h2paedelusionesphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>

                                                <div class="form-group">
                                                    <label id="lbl_h2paeilsuionesphc" runat="server">Ilsuiones:</label>
                                                    <asp:TextBox ID="txtNew_h2paeilsuionesphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>

                                                <div class="form-group">
                                                    <label id="lbl_h2paealucinacionesphc" runat="server">Alucinaciones:</label>
                                                    <asp:TextBox ID="txtNew_h2paealucinacionesphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="hoja3" role="tabpanel" aria-labelledby="hoja3-tab">
                                <h2 class="card-title">Examen Neurológico - Funciones cognoscitivas</h2>
                                <div class="card card-waves">
                                    <div class="card-header">II. Memoria y afecto</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title">A. Estado de consciencia</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label id="lbl_h3conrecientephc" runat="server">Reciente:</label>
                                                    <asp:TextBox ID="txtNew_h3conrecientephc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    <label id="lbl_h3conremotaphc" runat="server">Remota:</label>
                                                    <asp:TextBox ID="txtNew_h3conremotaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title">B. Afecto</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h3afeeuforicophc" runat="server">Eufórico:</label>
                                                    <asp:TextBox ID="txtNew_h3afeeuforicophc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h3afeplanophc" runat="server">Plano:</label>
                                                    <asp:TextBox ID="txtNew_h3afeplanophc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h3afedeprimidophc" runat="server">Deprimido:</label>
                                                    <asp:TextBox ID="txtNew_h3afedeprimidophc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h3afelabilphc" runat="server">Lábil:</label>
                                                    <asp:TextBox ID="txtNew_h3afelabilphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h3afehostilphc" runat="server">Hostil:</label>
                                                    <asp:TextBox ID="txtNew_h3afehostilphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label id="lbl_h3afeinadecuadophc" runat="server">Inadecuado:</label>
                                                    <asp:TextBox ID="txtNew_h3afeinadecuadophc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">III. Lenguaje y funciones relacionadas</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title">A. Hemisferio dominante</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3hdoizquierdophc" runat="server" Text="Izquierda:" />
                                                    <asp:TextBox ID="txtNew_h3hdoizquierdophc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3hdoderechophc" runat="server" Text="Derecho:" />
                                                    <asp:TextBox ID="txtNew_h3hdoderechophc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title">B. Lenguaje auditivo - Caracteristicas generales</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3cgeexpresionphc" runat="server" Text="Expresión:" />
                                                    <asp:TextBox ID="txtNew_h3cgeexpresionphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3cgenominacionphc" runat="server" Text="Nominación:" />
                                                    <asp:TextBox ID="txtNew_h3cgenominacionphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3cgerepeticionphc" runat="server" Text="Repetición:" />
                                                    <asp:TextBox ID="txtNew_h3cgerepeticionphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3cgecomprensionphc" runat="server" Text="Comprensión:" />
                                                    <asp:TextBox ID="txtNew_h3cgecomprensionphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title">B. Lenguaje auditivo - Afasias perislivianas</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3apebrocaphc" runat="server" Text="Broca:" />
                                                    <asp:TextBox ID="txtNew_h3apebrocaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3apeconduccionphc" runat="server" Text="Conduccion:" />
                                                    <asp:TextBox ID="txtNew_h3apeconduccionphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3apewernickephc" runat="server" Text="Wernicke:" />
                                                    <asp:TextBox ID="txtNew_h3apewernickephc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3apeglobalphc" runat="server" Text="Global:" />
                                                    <asp:TextBox ID="txtNew_h3apeglobalphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3apeanomiaphc" runat="server" Text="Anomia:" />
                                                    <asp:TextBox ID="txtNew_h3apeanomiaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title">B. Lenguaje auditivo - Afasias transcorticales</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3atrmotoraphc" runat="server" Text="Motora:" />
                                                    <asp:TextBox ID="txtNew_h3atrmotoraphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3atrsensitivaphc" runat="server" Text="Sensitiva:" />
                                                    <asp:TextBox ID="txtNew_h3atrsensitivaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3atrmixtaphc" runat="server" Text="Mixta:" />
                                                    <asp:TextBox ID="txtNew_h3atrmixtaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title">B. Lenguaje auditivo - Afasia talámica</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3ataexpresionphc" runat="server" Text="Expresion:" />
                                                    <asp:TextBox ID="txtNew_h3ataexpresionphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3atacomprensionphc" runat="server" Text="Comprension:" />
                                                    <asp:TextBox ID="txtNew_h3atacomprensionphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3atamixtaphc" runat="server" Text="Mixta:" />
                                                    <asp:TextBox ID="txtNew_h3atamixtaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title">B. Lenguaje auditivo - Aprosodias</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3aprmotoraphc" runat="server" Text="Motora:" />
                                                    <asp:TextBox ID="txtNew_h3aprmotoraphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3aprsensorialphc" runat="server" Text="Sensorial:" />
                                                    <asp:TextBox ID="txtNew_h3aprsensorialphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title">C. Lenguaje escrito</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3lesalexiaphc" runat="server" Text="Alexia:" />
                                                    <asp:TextBox ID="txtNew_h3lesalexiaphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3lesagrafiaphc" runat="server" Text="Agrafia:" />
                                                    <asp:TextBox ID="txtNew_h3lesagrafiaphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">IV. Funciones cognoscitivas localizadas</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title">A. Lóbulo parietal dominante</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3lpdacalculiaphc" runat="server" Text="Acalculia:" />
                                                    <asp:TextBox ID="txtNew_h3lpdacalculiaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3lpdagrafiaphc" runat="server" Text="Agrafia:" />
                                                    <asp:TextBox ID="txtNew_h3lpdagrafiaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3lpddesorientacionphc" runat="server" Text="Desorientacion Der-Izq:" />
                                                    <asp:TextBox ID="txtNew_h3lpddesorientacionphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3lpdagnosiaphc" runat="server" Text="Agnosia digital:" />
                                                    <asp:TextBox ID="txtNew_h3lpdagnosiaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title">B. Agnosias</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3agnauditivaphc" runat="server" Text="Auditiva:" />
                                                    <asp:TextBox ID="txtNew_h3agnauditivaphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3agnvisualphc" runat="server" Text="Visual:" />
                                                    <asp:TextBox ID="txtNew_h3agnvisualphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3agntactilphc" runat="server" Text="Tactil:" />
                                                    <asp:TextBox ID="txtNew_h3agntactilphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title">C. Apraxias</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3aprideacionalphc" runat="server" Text="Ideacional:" />
                                                    <asp:TextBox ID="txtNew_h3aprideacionalphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3aprideomotoraphc" runat="server" Text="Ideomotora:" />
                                                    <asp:TextBox ID="txtNew_h3aprideomotoraphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title">D. Anosognosia</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3anosognosiaphc" runat="server" Text="Anosognosia:" />
                                                    <asp:TextBox ID="txtNew_h3anosognosiaphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h3aprdesatencionphc" runat="server" Text="Desatencion tactil:" />
                                                    <asp:TextBox ID="txtNew_h3aprdesatencionphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="hoja4" role="tabpanel" aria-labelledby="hoja4-tab">
                                <h2 class="card-title">Pares craneales</h2>
                                <div class="card card-waves">
                                    <div class="card-header">I. Olfatorio</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4olfnormalphc" runat="server" Text="Normal:" />
                                                    <asp:TextBox ID="txtNew_h4olfnormalphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4olfanosmiaphc" runat="server" Text="Anosmia:" />
                                                    <asp:TextBox ID="txtNew_h4olfanosmiaphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">II. Optico</div>
                                    <div class="card-body text-sm">
                                        <div class="row align-items-center">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4agudezavisual_sc" runat="server" Text="Agudez visual: S/Correc." />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4optscodphc" runat="server" Text="OD:" />
                                                    <asp:TextBox ID="txtNew_h4optscodphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4optscoiphc" runat="server" Text="OI:" />
                                                    <asp:TextBox ID="txtNew_h4optscoiphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4agudezavisual_cc" runat="server" Text="C/Correc." />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4optccodphc" runat="server" Text="OD:" />
                                                    <asp:TextBox ID="txtNew_h4optccodphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4optccoiphc" runat="server" Text="OI:" />
                                                    <asp:TextBox ID="txtNew_h4optccoiphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row align-items-center">
                                            <div class="col-md-2">
                                                <asp:Literal ID="lbl_h4fondoojo" runat="server" Text="Fondo de ojo:" />
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4fondoodphc" runat="server" Text="Derecho:" />
                                                    <asp:TextBox ID="txtNew_h4fondoodphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4fondooiphc" runat="server" Text="Izquierdo:" />
                                                    <asp:TextBox ID="txtNew_h4fondooiphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4campimetriaphc" runat="server" Text="Campimetría por confrontación:" />
                                                    <asp:TextBox ID="txtNew_h4campimetriaphc" TextMode="MultiLine"
                                                        data-parsley-maxlength="5000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="5000" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">III. IV. V. Motor ocular común - Troclear - Motor ocular externo</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4prosisphc" runat="server" Text="Prosis:" />
                                                    <asp:TextBox ID="txtNew_h4prosisphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4posicionojosphc" runat="server" Text="Posicion de los ojos:" />
                                                    <asp:TextBox ID="txtNew_h4posicionojosphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h4exoftalmiaphc" runat="server" Text="Exoftalmia:" />
                                                            <asp:TextBox ID="txtNew_h4exoftalmiaphc"
                                                                data-parsley-maxlength="200"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="200" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h4hornerphc" runat="server" Text="Horner:" />
                                                            <asp:TextBox ID="txtNew_h4hornerphc"
                                                                data-parsley-maxlength="200"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="200" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h4movocularesphc" runat="server" Text="Movimientos oculares:" />
                                                            <asp:TextBox ID="txtNew_h4movocularesphc"
                                                                data-parsley-maxlength="200"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="200" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h4nistagmuxphc" runat="server" Text="Nistagmus:" />
                                                            <asp:TextBox ID="txtNew_h4nistagmuxphc"
                                                                data-parsley-maxlength="200"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="200" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h4convergenciaphc" runat="server" Text="Convergencia:" />
                                                            <asp:TextBox ID="txtNew_h4convergenciaphc"
                                                                data-parsley-maxlength="200"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="200" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h4acomodacionodphc" runat="server" Text="Reflejos de acomodación Der.:" />
                                                            <asp:TextBox ID="txtNew_h4acomodacionodphc"
                                                                data-parsley-maxlength="200"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="200" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h4acomodacionoiphc" runat="server" Text="Reflejos de acomodación Izq.:" />
                                                            <asp:TextBox ID="txtNew_h4acomodacionoiphc"
                                                                data-parsley-maxlength="200"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="200" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        Campo de diplopia:
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtNew_h4diplopia1phc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtNew_h4diplopia2phc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtNew_h4diplopia3phc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtNew_h4diplopia4phc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtNew_h4diplopia5phc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtNew_h4diplopia6phc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtNew_h4diplopia7phc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtNew_h4diplopia8phc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtNew_h4diplopia9phc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4pupulasodphc" runat="server" Text="Pupilas tamaño Der:" />
                                                    <asp:TextBox ID="txtNew_h4pupulasodphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4pupulasoiphc" runat="server" Text="Pupilas tamaño Izq:" />
                                                    <asp:TextBox ID="txtNew_h4pupulasoiphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4formaphc" runat="server" Text="Froma:" />
                                                    <asp:TextBox ID="txtNew_h4formaphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4fotomotorodphc" runat="server" Text="Reflejo fotomotor Der:" />
                                                    <asp:TextBox ID="txtNew_h4fotomotorodphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4fotomotoroiphc" runat="server" Text="Reflejo fotomotor Izq:" />
                                                    <asp:TextBox ID="txtNew_h4fotomotoroiphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4consensualdaiphc" runat="server" Text="Reflejo Consensual D a I:" />
                                                    <asp:TextBox ID="txtNew_h4consensualdaiphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4consensualiadphc" runat="server" Text="Reflejo Consensual I a D:" />
                                                    <asp:TextBox ID="txtNew_h4consensualiadphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">VI. Trigemio</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4senfacialderphc" runat="server" Text="Sensibilidad facial Der.:" />
                                                    <asp:TextBox ID="txtNew_h4senfacialderphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4senfacializqphc" runat="server" Text="Sensibilidad facial Izq.:" />
                                                    <asp:TextBox ID="txtNew_h4senfacializqphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4reflejoderphc" runat="server" Text="Reflejo corneano Der.:" />
                                                    <asp:TextBox ID="txtNew_h4reflejoderphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4reflejoizqphc" runat="server" Text="Reflejo corenano Izq.:" />
                                                    <asp:TextBox ID="txtNew_h4reflejoizqphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4aberturabocaphc" runat="server" Text="Abertura de la boca:" />
                                                    <asp:TextBox ID="txtNew_h4aberturabocaphc"
                                                        data-parsley-maxlength="500"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="500" />
                                                </div>
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4refmentonianophc" runat="server" Text="Reflejo mentoneano:" />
                                                    <asp:TextBox ID="txtNew_h4refmentonianophc"
                                                        data-parsley-maxlength="500"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="500" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4movmasticatoriosphc" runat="server" Text="Movimientos masticatorios:" />
                                                    <asp:TextBox ID="txtNew_h4movmasticatoriosphc"
                                                        data-parsley-maxlength="500" TextMode="MultiLine" Rows="7"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="500" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">VII. Facial</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4facsimetriaphc" runat="server" Text="Simetria:" />
                                                    <asp:TextBox ID="txtNew_h4facsimetriaphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4facmovimientosphc" runat="server" Text="Movimientos faciales:" />
                                                    <asp:TextBox ID="txtNew_h4facmovimientosphc"
                                                        data-parsley-maxlength="500"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="500" />
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4facparalisisphc" runat="server" Text="Parálisis:" />
                                                    <asp:TextBox ID="txtNew_h4facparalisisphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4faccentralphc" runat="server" Text="Central:" />
                                                    <asp:TextBox ID="txtNew_h4faccentralphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4facperifericaphc" runat="server" Text="Periférica:" />
                                                    <asp:TextBox ID="txtNew_h4facperifericaphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h4facgustophc" runat="server" Text="Gusto:" />
                                                    <asp:TextBox ID="txtNew_h4facgustophc"
                                                        data-parsley-maxlength="500"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="500" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="hoja5" role="tabpanel" aria-labelledby="hoja5-tab">
                                <h2 class="card-title">Pares craneales</h2>
                                <div class="card card-waves">
                                    <div class="card-header">VIII. Auditivo y vestibular</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5otoscopiaphc" runat="server" Text="Otoscopia:" />
                                                    <asp:TextBox ID="txtNew_h5otoscopiaphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5aguaudiderphc" runat="server" Text="Agudeza auditiva Der.:" />
                                                    <asp:TextBox ID="txtNew_h5aguaudiderphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5aguaudiizqphc" runat="server" Text="Agudeza auditiva Izq.:" />
                                                    <asp:TextBox ID="txtNew_h5aguaudiizqphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5weberphc" runat="server" Text="Weber:" />
                                                    <asp:TextBox ID="txtNew_h5weberphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5rinnephc" runat="server" Text="Rinne:" />
                                                    <asp:TextBox ID="txtNew_h5rinnephc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5pruebaslabphc" runat="server" Text="Pruebas laberínticas:" />
                                                    <asp:TextBox ID="txtNew_h5pruebaslabphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">IX. X. Glosofaringeo y vago</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5elevpaladarphc" runat="server" Text="Elevación del paladar:" />
                                                    <asp:TextBox ID="txtNew_h5elevpaladarphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5uvulaphc" runat="server" Text="Uvula:" />
                                                    <asp:TextBox ID="txtNew_h5uvulaphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5refnauceosophc" runat="server" Text="Reflejo nauceoso:" />
                                                    <asp:TextBox ID="txtNew_h5refnauceosophc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5deglucionphc" runat="server" Text="Deglucion:" />
                                                    <asp:TextBox ID="txtNew_h5deglucionphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5tonovozphc" runat="server" Text="Tono de la voz:" />
                                                    <asp:TextBox ID="txtNew_h5tonovozphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">XI. Espinal</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5esternocleidomastoideophc" runat="server" Text="Esternocleidomastoideo:" />
                                                    <asp:TextBox ID="txtNew_h5esternocleidomastoideophc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5trapeciophc" runat="server" Text="Trapecio:" />
                                                    <asp:TextBox ID="txtNew_h5trapeciophc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">XII. Hipogloso</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5desviacionphc" runat="server" Text="Desviacion:" />
                                                    <asp:TextBox ID="txtNew_h5desviacionphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5atrofiaphc" runat="server" Text="Atrofia:" />
                                                    <asp:TextBox ID="txtNew_h5atrofiaphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5fasciculacionphc" runat="server" Text="Fasciculacion:" />
                                                    <asp:TextBox ID="txtNew_h5fasciculacionphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5fuerzaphc" runat="server" Text="Fuerza:" />
                                                    <asp:TextBox ID="txtNew_h5fuerzaphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <h2 class="card-title">Sistema motor</h2>
                                <div class="card-text text-sm">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <asp:Literal ID="lbl_h5marchaphc" runat="server" Text="Marcha:" />
                                                <asp:TextBox ID="txtNew_h5marchaphc"
                                                    data-parsley-maxlength="1000"
                                                    data-parsley-group="validation-new"
                                                    Class="form-control" CssClass="form-control"
                                                    runat="server" MaxLength="1000" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card card-waves">
                                    <div class="card-header">Estado muscular</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5tonophc" runat="server" Text="Tono:" />
                                                    <asp:TextBox ID="txtNew_h5tonophc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5volumenphc" runat="server" Text="Volumen:" />
                                                    <asp:TextBox ID="txtNew_h5volumenphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5fasciculacionesphc" runat="server" Text="Fasciculaciones:" />
                                                    <asp:TextBox ID="txtNew_h5fasciculacionesphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card card-waves">
                                            <div class="card-header">Fuerza muscular</div>
                                            <div class="card-body text-sm">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtNew_h5fuemuscularphc"
                                                                TextMode="MultiLine" Rows="3"
                                                                data-parsley-maxlength="5000"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="5000" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card card-waves">
                                            <div class="card-header">Movimientos involuntarios</div>
                                            <div class="card-body text-sm">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtNew_h5movinvoluntariosphc"
                                                                TextMode="MultiLine" Rows="3"
                                                                data-parsley-maxlength="5000"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="5000" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">Coordinación</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5equilibratoriaphc" runat="server" Text="Equilibratoria:" />
                                                    <asp:TextBox ID="txtNew_h5equilibratoriaphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5rombergphc" runat="server" Text="Romberg:" />
                                                    <asp:TextBox ID="txtNew_h5rombergphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5dednarderphc" runat="server" Text="Dedo-Nariz Der.:" />
                                                    <asp:TextBox ID="txtNew_h5dednarderphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5dednarizqphc" runat="server" Text="Dedo-Nariz Izq.:" />
                                                    <asp:TextBox ID="txtNew_h5dednarizqphc"
                                                        data-parsley-maxlength="100"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="100" />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5deddedderphc" runat="server" Text="Dedo-Dedo Der.:" />
                                                    <asp:TextBox ID="txtNew_h5deddedderphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5deddedizqphc" runat="server" Text="Dedo-Dedo Izq.:" />
                                                    <asp:TextBox ID="txtNew_h5deddedizqphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5talrodderphc" runat="server" Text="Talon-Rodilla Der.:" />
                                                    <asp:TextBox ID="txtNew_h5talrodderphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5talrodizqphc" runat="server" Text="Talon-Rodilla Izq.:" />
                                                    <asp:TextBox ID="txtNew_h5talrodizqphc"
                                                        data-parsley-maxlength="200"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="200" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5movrapidphc" runat="server" Text="Movimientos rápidos alternos:" />
                                                    <asp:TextBox ID="txtNew_h5movrapidphc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="500"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="500" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5rebotephc" runat="server" Text="Rebote:" />
                                                    <asp:TextBox ID="txtNew_h5rebotephc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="500"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="500" />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h5habilidadespecifphc" runat="server" Text="Habilidad específica:" />
                                                    <asp:TextBox ID="txtNew_h5habilidadespecifphc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="500"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="500" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="hoja6" role="tabpanel" aria-labelledby="hoja6-tab">
                                <h2 class="card-title">Reflejos</h2>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card card-waves">
                                            <div class="card-header">Motaticos</div>
                                            <div class="card-body text-sm">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6miotbicderphc" runat="server" Text="Biceps Der:" />
                                                            <asp:TextBox ID="txtNew_h6miotbicderphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6miotbicizqphc" runat="server" Text="Biceps Izq:" />
                                                            <asp:TextBox ID="txtNew_h6miotbicizqphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6miottriderphc" runat="server" Text="Triceps Der:" />
                                                            <asp:TextBox ID="txtNew_h6miottriderphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6miottriizqphc" runat="server" Text="Triceps Izq:" />
                                                            <asp:TextBox ID="txtNew_h6miottriizqphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6miotestderphc" runat="server" Text="Estirolad Der:" />
                                                            <asp:TextBox ID="txtNew_h6miotestderphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6miotestizqphc" runat="server" Text="Estirolad Izq:" />
                                                            <asp:TextBox ID="txtNew_h6miotestizqphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6miotpatderphc" runat="server" Text="Patelar Der:" />
                                                            <asp:TextBox ID="txtNew_h6miotpatderphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6miotpatizqphc" runat="server" Text="Patelar Izq:" />
                                                            <asp:TextBox ID="txtNew_h6miotpatizqphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6miotaquderphc" runat="server" Text="Aquiliano Der:" />
                                                            <asp:TextBox ID="txtNew_h6miotaquderphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6miotaquizqphc" runat="server" Text="Aquiliano Izq:" />
                                                            <asp:TextBox ID="txtNew_h6miotaquizqphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card card-waves">
                                            <div class="card-header">Patológicos</div>
                                            <div class="card-body text-sm">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6patpladerphc" runat="server" Text="Plantar Der:" />
                                                            <asp:TextBox ID="txtNew_h6patpladerphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6patplaizqphc" runat="server" Text="Plantar Izq:" />
                                                            <asp:TextBox ID="txtNew_h6patplaizqphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6pathofderphc" runat="server" Text="Hoffman Der:" />
                                                            <asp:TextBox ID="txtNew_h6pathofderphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6pathofizqphc" runat="server" Text="Hoffman Izq:" />
                                                            <asp:TextBox ID="txtNew_h6pathofizqphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6pattroderphc" runat="server" Text="Trompa Der:" />
                                                            <asp:TextBox ID="txtNew_h6pattroderphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6pattroizqphc" runat="server" Text="Trompa Izq:" />
                                                            <asp:TextBox ID="txtNew_h6pattroizqphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6patprederphc" runat="server" Text="Prensión Der:" />
                                                            <asp:TextBox ID="txtNew_h6patprederphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6patpreizqphc" runat="server" Text="Prensión Izq:" />
                                                            <asp:TextBox ID="txtNew_h6patpreizqphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6patpmenderphc" runat="server" Text="Palm-Men Der:" />
                                                            <asp:TextBox ID="txtNew_h6patpmenderphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6patpmenizqphc" runat="server" Text="Palm-Men Izq:" />
                                                            <asp:TextBox ID="txtNew_h6patpmenizqphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card card-waves">
                                            <div class="card-header">Superficiales</div>
                                            <div class="card-body text-sm">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6supbicderphc" runat="server" Text="Biceps Der:" />
                                                            <asp:TextBox ID="txtNew_h6supbicderphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6supbicizqphc" runat="server" Text="Biceps Izq:" />
                                                            <asp:TextBox ID="txtNew_h6supbicizqphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6suptriderphc" runat="server" Text="Triceps Der:" />
                                                            <asp:TextBox ID="txtNew_h6suptriderphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6suptriizqphc" runat="server" Text="Triceps Izq:" />
                                                            <asp:TextBox ID="txtNew_h6suptriizqphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6supestderphc" runat="server" Text="Estirolad Der:" />
                                                            <asp:TextBox ID="txtNew_h6supestderphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6supestizqphc" runat="server" Text="Estirolad Izq:" />
                                                            <asp:TextBox ID="txtNew_h6supestizqphc"
                                                                data-parsley-maxlength="10"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="10" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6otrmaxilarphc" runat="server" Text="Maxilar:" />
                                                            <asp:TextBox ID="txtNew_h6otrmaxilarphc"
                                                                data-parsley-maxlength="100"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="100" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <asp:Literal ID="lbl_h6otrglabelarphc" runat="server" Text="Glabelar:" />
                                                            <asp:TextBox ID="txtNew_h6otrglabelarphc"
                                                                data-parsley-maxlength="100"
                                                                data-parsley-group="validation-new"
                                                                Class="form-control" CssClass="form-control"
                                                                runat="server" MaxLength="100" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <h2 class="card-title">Sistema sensitivo</h2>
                                <div class="card card-waves">
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtNew_h6sistsensitivophc"
                                                        data-parsley-maxlength="2000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="2000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h5 class="card-title">Use los siguientes simbolos: Dolor = D, Tacto = T, Temp = TP</h5>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h6dolorprofphc" runat="server" Text="Dolor profundo (Aquiles-Testículo):" />
                                                    <asp:TextBox ID="txtNew_h6dolorprofphc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="2000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="2000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h6posicionphc" runat="server" Text="Posición:" />
                                                    <asp:TextBox ID="txtNew_h6posicionphc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="2000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="2000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h6vibracionphc" runat="server" Text="Vibración:" />
                                                    <asp:TextBox ID="txtNew_h6vibracionphc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="2000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="2000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h6dospuntosphc" runat="server" Text="Dos puntos:" />
                                                    <asp:TextBox ID="txtNew_h6dospuntosphc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="2000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="2000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h6extincionphc" runat="server" Text="Extinción:" />
                                                    <asp:TextBox ID="txtNew_h6extincionphc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="2000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="2000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h6estereognosiaphc" runat="server" Text="Estereognosia:" />
                                                    <asp:TextBox ID="txtNew_h6estereognosiaphc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="2000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="2000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h6grafestesiaphc" runat="server" Text="Grafestesia:" />
                                                    <asp:TextBox ID="txtNew_h6grafestesiaphc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="2000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="2000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h6localizacionphc" runat="server" Text="Localizacion:" />
                                                    <asp:TextBox ID="txtNew_h6localizacionphc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="2000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="2000" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="hoja7" role="tabpanel" aria-labelledby="hoja7-tab">
                                <div class="card card-waves">
                                    <div class="card-header">Signos meningeos</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h7rigideznucaphc" runat="server" Text="Rigidez de nuca:" />
                                                    <asp:TextBox ID="txtNew_h7rigideznucaphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h7sigkerningphc" runat="server" Text="Signo de Kerning:" />
                                                    <asp:TextBox ID="txtNew_h7sigkerningphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h7fotofobiaphc" runat="server" Text="Fotofobia:" />
                                                    <asp:TextBox ID="txtNew_h7fotofobiaphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h7sigbrudzinskiphc" runat="server" Text="Signo de Brudzinski:" />
                                                    <asp:TextBox ID="txtNew_h7sigbrudzinskiphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h7hiperestesiaphc" runat="server" Text="Hiperestesia de globos oculares:" />
                                                    <asp:TextBox ID="txtNew_h7hiperestesiaphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">Examen vascular</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h7pulcarotideosphc" runat="server" Text="Palpación de pulsos carotideos:" />
                                                    <asp:TextBox ID="txtNew_h7pulcarotideosphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h7pultemporalesphc" runat="server" Text="Palpación de pulsos temporales superficiales:" />
                                                    <asp:TextBox ID="txtNew_h7pultemporalesphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h7cabezaphc" runat="server" Text="Auscultación de cabeza:" />
                                                    <asp:TextBox ID="txtNew_h7cabezaphc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:Literal ID="lbl_h7cuellophc" runat="server" Text="Auscultación de cuello:" />
                                                    <asp:TextBox ID="txtNew_h7cuellophc"
                                                        data-parsley-maxlength="1000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="1000" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">Examen nervios perifericos</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtNew_h7nerviosperifphc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="5000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="5000" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">Columna vertebral</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtNew_h7colvertebralphc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="5000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="5000" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">Resumen</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtNew_h7resumenphc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="5000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="5000" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">Diagnósticos</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtNew_h7diagnosticosphc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="5000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="5000" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">Conducta</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtNew_h7conductaphc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="5000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="5000" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="card card-waves">
                                    <div class="card-header">Tratamiento</div>
                                    <div class="card-body text-sm">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtNew_h7tratamientophc"
                                                        TextMode="MultiLine" Rows="3"
                                                        data-parsley-maxlength="5000"
                                                        data-parsley-group="validation-new"
                                                        Class="form-control" CssClass="form-control"
                                                        runat="server" MaxLength="5000" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="Server">
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/popper/popper.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/locale/bootstrap-table-es-MX.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/js/bootstrap-select.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/moment/moment.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.js") %>"></script>

    <script>
        $(document).ready(function () {
            $('select').selectpicker();
            $('[data-toggle="popover"]').popover();
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        function pageLoad() {
            $(function () {
                $('select').selectpicker();
                $('[data-toggle="popover"]').popover();
                $('#datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#datetimepicker2').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        }
    </script>
</asp:Content>