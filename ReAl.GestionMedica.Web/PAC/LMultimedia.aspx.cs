﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using DevExpress.Utils;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Web.App_Helpers;
using ReAl.Utils;
using DevExpress.Web;
using DevExpress.Web.Internal;

namespace ReAl.GestionMedica.Web.PAC
{
    public partial class LMultimedia : System.Web.UI.Page
    {
        #region Methods

        public void CargarListado()
        {
            try
            {
                SessionHandler miSesion = new SessionHandler();
                String strNombreSp = "sppmuselgrid";

                var arrNomParam = new ArrayList();
                arrNomParam.Add(EntPacPacientes.Fields.idppa.ToString());
                var arrValParam = new ArrayList();
                arrValParam.Add(miSesion.AppPaciente.idppa);

                var rn = new RnVista();
                var dt = rn.ObtenerDatosProcAlm(strNombreSp, arrNomParam, arrValParam);

                dtgListado.DataSource = dt;
                dtgListado.DataBind();
                dtgListado.EnablePagingGestures = AutoBoolean.False;
                dtgListado.SettingsPager.EnableAdaptivity = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        protected void btnAtras_OnClick(object sender, EventArgs e)
        {
            SessionHandler miSesion = new SessionHandler();
            Response.Redirect(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));
        }

        protected void btnCertificados_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LCertificados.aspx");
        }

        protected void btnCirugias_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LCirugias.aspx");
        }

        protected void btnConsultas_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LConsultas.aspx");
        }

        protected void btnEditaGuardar_OnClick(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var miSesion = new SessionHandler();
                var rn = new RnPacMultimedia();
                EntPacMultimedia objPag = rn.ObtenerObjeto(rn.GetColumnType(hdnIdDatos.Value, EntPacMultimedia.Fields.idpmu));
                objPag.observacionespmu = txtEditObservacionespmu.Text;
                objPag.fechapmu = rn.GetColumnType(dtpEditFechapmu.Text, EntPacMultimedia.Fields.fechapmu);
                objPag.apitransaccion = CApi.Transaccion.MODIFICAR.ToString();
                objPag.usumod = miSesion.AppUsuario.loginsus;

                rn.Update(objPag);
                SiteHelper.VerificarCita(miSesion);
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }
            if (bProcede)
                Response.Redirect(
                    Request.AppRelativeCurrentExecutionFilePath +
                    "?msg=Se ha modificado el registro satisfactoriamente");
        }

        protected void btnHistoria_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LHistoria.aspx");
        }

        protected void btnInformes_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LInformes.aspx");
        }

        protected void btnLiberar_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LLiberar.aspx");
        }

        protected void btnMultimedia_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LMultimedia.aspx");
        }

        protected void btnNewGuardar_OnClick(object sender, EventArgs e)
        {
            var bProcede = false;
            string strMensajeAdd = "";
            try
            {
                var filename = "";
                var miSesion = new SessionHandler();
                var rn = new RnPacMultimedia();
                var objPag = new EntPacMultimedia();
                objPag.idppa = miSesion.AppPaciente.idppa;
                objPag.fechapmu = rn.GetColumnType(dtpNewFechapmu.Text, EntPacMultimedia.Fields.fechapmu);
                objPag.observacionespmu = txtNewObservacionespmu.Text;
                objPag.usucre = miSesion.AppUsuario.loginsus;
                if (AsyncFileUpload1.HasFile)
                {
                    string[] validFileTypes = { "pdf", "png", "jpg", "jpeg" };
                    var strExtension = Path.GetExtension(AsyncFileUpload1.FileName);
                    var isValidFile = false;
                    for (var i = 0; i < validFileTypes.Length; i++)
                    {
                        if (strExtension.ToUpper() == "." + validFileTypes[i].ToUpper())
                        {
                            isValidFile = true;
                            break;
                        }
                    }

                    if (isValidFile)
                    {
                        //var byteArchivo = AsyncFileUpload1.FileBytes;
                        filename = "PMU_" + miSesion.AppPaciente.idppa + "_" +
                                       DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_" +
                                       Path.GetFileName(AsyncFileUpload1.FileName);
                        AsyncFileUpload1.SaveAs(Server.MapPath("~/Uploads/") + filename);
                        objPag.pathpmu = filename;
                        objPag.imagenpmu = cFuncionesImagenes.ImageReadBinaryFile(Server.MapPath("~/uploads/") + filename);
                        objPag.extensionpmu = filename.Substring(filename.LastIndexOf(".") + 1, filename.Length - filename.LastIndexOf(".") - 1);
                    }
                    else
                    {
                        throw new SimpleException("Solo se permite archivos de tipo " +
                                                  string.Join(",", validFileTypes));
                    }
                }

                rn.Insert(objPag);
                SiteHelper.VerificarCita(miSesion);

                //Sync GDrive
                if (chkSyncGoogle.Checked)
                {
                    var google = new CGoogleApi();
                    var service = google.CrearServicioDrive(miSesion.GAppsCredential);
                    string strNombreArchivo = miSesion.AppPaciente.appaternoppa + "_" + miSesion.AppPaciente.nombresppa + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + Path.GetFileName(AsyncFileUpload1.FileName);
                    string strDescripcion = "PACIENTE: " + miSesion.AppPaciente.appaternoppa + " " + miSesion.AppPaciente.nombresppa + Environment.NewLine +
                                            "FECHA: " + DateTime.Now.ToString(CParametros.ParFormatoFechaHora) + Environment.NewLine +
                                            "NOMBRE: " + Path.GetFileName(AsyncFileUpload1.FileName) + Environment.NewLine +
                                            "OBSERVACIONES: " + txtNewObservacionespmu.Text;
                    var info = new FileInfo(Server.MapPath("~/Uploads/") + filename);
                    var file = google.SubirArchivo(service, info, strDescripcion);
                    strMensajeAdd = " y se ha sincronizado en GoogleDrive en la carpeta GestionMedica.";
                }

                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (bProcede)
                Response.Redirect(
                    Request.AppRelativeCurrentExecutionFilePath +
                    "?msg=Se ha creado el registro satisfactoriamente" + strMensajeAdd);
        }

        protected void btnNuevo_OnClick(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "newModal", "$('#newModal').modal('show');", true);
        }

        protected void btnRecetas_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LRecetas.aspx");
        }

        protected void dtgListado_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            String strRedireccion = "";
            try
            {
                if (e.CommandName.Equals("detalles"))
                {
                    string strId = e.CommandArgument.ToString();

                    //Filtramos el Dataset
                    DataView dv = ((DataTable)dtgListado.DataSource).DefaultView;
                    dv.RowFilter = EntPacMultimedia.Fields.idpmu + " = " + strId;

                    DataTable detailTable = dv.ToTable();

                    dtgDetalles.DataSource = detailTable;
                    dtgDetalles.DataBind();
                    dtgDetalles.HeaderRow.TableSection = TableRowSection.TableHeader;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#currentdetail').appendTo('body').modal('show');", true);
                }
                else if (e.CommandName.Equals("modificar"))
                {
                    var strId = e.CommandArgument.ToString();
                    var rn = new RnPacMultimedia();
                    EntPacMultimedia objPag = rn.ObtenerObjeto(rn.GetColumnType(strId, EntPacMultimedia.Fields.idpmu));
                    hdnIdDatos.Value = objPag.idpmu.ToString();
                    dtpEditFechapmu.Text = DateTime.Parse(objPag.fechapmu.ToString()).ToString(CParametros.ParFormatoFecha);
                    txtEditObservacionespmu.Text = objPag.observacionespmu;
                    lblResult.Visible = false;

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("$('#editModal').modal('show');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);
                }
                else if (e.CommandName.Equals("descargar"))
                {
                    var strId = e.CommandArgument.ToString();
                    var rn = new RnPacMultimedia();
                    EntPacMultimedia obj = rn.ObtenerObjeto(int.Parse(strId));

                    if (obj != null)
                    {
                        if (File.Exists(Server.MapPath("~/uploads/") + obj.pathpmu))
                        {
                            var url = "PVerArchivo.aspx?id=" + e.CommandArgument;
                            var js = "window.open('" + ResolveUrl(url) + "', '_blank');";
                            ScriptManager.RegisterClientScriptBlock(this, GetType(), "OpenFile.aspx", js, true);
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(obj.pathpmu))
                                throw new SimpleException("El registro no tiene documento adjunto");
                            else
                                throw new SimpleException("No existe el archivo " + obj.pathpmu);
                        }
                    }
                }
                else if (e.CommandName.Equals("eliminar"))
                {
                    string strId = e.CommandArgument.ToString();

                    var rn = new RnPacMultimedia();
                    EntPacMultimedia obj = rn.ObtenerObjeto(rn.GetColumnType(strId, EntPacMultimedia.Fields.idpmu));

                    if (obj != null)
                    {
                        rn.Delete(obj);
                        strRedireccion = "~/PAC/LMultimedia?msg=Se ha ELIMINADO el registro satisfactoriamente";
                    }
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (!string.IsNullOrEmpty(strRedireccion))
                Response.Redirect(strRedireccion);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                SessionHandler miSesion = new SessionHandler();
                //if (miSesion.ArrMenu != null)
                //    if (!miSesion.ArrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath.Split('/').Last()))
                //        Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));

                if (miSesion.AppPaciente == null)
                    Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));

                if (!Page.IsPostBack)
                {
                    litPacienteTit.Text = miSesion.AppPaciente.nombresppa + " " + miSesion.AppPaciente.appaternoppa + " " +
                                          miSesion.AppPaciente.apmaternoppa;

                    litPacienteSub.Text = miSesion.AppPaciente.nombresppa + " " + miSesion.AppPaciente.appaternoppa + " " +
                                          miSesion.AppPaciente.apmaternoppa;

                    dtpNewFechapmu.Text = DateTime.Now.ToString(CParametros.ParFormatoFecha);
                    CargarListado();

                    if (miSesion.GAppsUsuario != null)
                    {
                        chkSyncGoogle.Visible = true;
                    }
                }
                else
                {
                    CargarListado();
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        #endregion Methods

        protected void dtgListado_OnCustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            try
            {
                if (e.ButtonID.Equals("Detalles"))
                {
                    string strId = dtgListado.GetRowValues(e.VisibleIndex, EntPacMultimedia.Fields.idpmu.ToString()).ToString();

                    //Filtramos el Dataset
                    DataView dv = ((DataTable)dtgListado.DataSource).DefaultView;
                    dv.RowFilter = EntPacMultimedia.Fields.idpmu + " = " + strId;

                    DataTable detailTable = dv.ToTable();

                    dtgDetalles.DataSource = detailTable;
                    dtgDetalles.DataBind();
                    dtgDetalles.HeaderRow.TableSection = TableRowSection.TableHeader;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#currentdetail').appendTo('body').modal('show');", true);
                }
                else if (e.ButtonID.Equals("Modificar"))
                {
                    string strId = dtgListado.GetRowValues(e.VisibleIndex, EntPacMultimedia.Fields.idpmu.ToString()).ToString();
                    var rn = new RnPacMultimedia();
                    EntPacMultimedia objPag = rn.ObtenerObjeto(rn.GetColumnType(strId, EntPacMultimedia.Fields.idpmu));
                    hdnIdDatos.Value = objPag.idpmu.ToString();
                    dtpEditFechapmu.Text = DateTime.Parse(objPag.fechapmu.ToString()).ToString(CParametros.ParFormatoFecha);
                    txtEditObservacionespmu.Text = objPag.observacionespmu;
                    lblResult.Visible = false;

                    dtgListado.JSProperties["cpAccion"] = e.ButtonID;
                    dtgListado.JSProperties["cpId"] = strId;
                    dtgListado.JSProperties["cpFecha"] = DateTime.Parse(objPag.fechapmu.ToString()).ToString(CParametros.ParFormatoFecha);
                    dtgListado.JSProperties["cpObs"] = objPag.observacionespmu;

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("$('#editModal').modal('show');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);
                }
                else if (e.ButtonID.Equals("Descargar"))
                {
                    string strId = dtgListado.GetRowValues(e.VisibleIndex, EntPacMultimedia.Fields.idpmu.ToString()).ToString();
                    var rn = new RnPacMultimedia();
                    EntPacMultimedia obj = rn.ObtenerObjeto(int.Parse(strId));

                    if (obj != null)
                    {
                        if (!File.Exists(Server.MapPath("~/uploads/") + obj.pathpmu))
                        {
                            var miSesion = new SessionHandler();
                            string filename = "PMU_" + miSesion.AppPaciente.idppa + "_" +
                                       DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_" +
                                       "migracion." + obj.extensionpmu;

                            File.WriteAllBytes(Server.MapPath("~/uploads/") + filename, obj.imagenpmu);
                            obj.pathpmu = filename;
                            obj.apitransaccion = CApi.Transaccion.MODIFICAR.ToString();
                            obj.usumod = miSesion.AppUsuario.loginsus;

                            rn.Update(obj);
                        }
                        //Abrimos el archivo
                        dtgListado.JSProperties["cpAccion"] = e.ButtonID;
                        dtgListado.JSProperties["cpId"] = strId;
                        var url = "PVerArchivo.aspx?id=" + strId;
                        var js = "window.open('" + ResolveUrl(url) + "', '_blank');";
                        ScriptManager.RegisterStartupScript(this, GetType(), "OpenFile.aspx", js, true);
                    }
                }
                else if (e.ButtonID.Equals("Eliminar"))
                {
                    string strId = dtgListado.GetRowValues(e.VisibleIndex, EntPacMultimedia.Fields.idpmu.ToString()).ToString();

                    var rn = new RnPacMultimedia();
                    EntPacMultimedia obj = rn.ObtenerObjeto(rn.GetColumnType(strId, EntPacMultimedia.Fields.idpmu));

                    if (obj != null)
                    {
                        rn.Delete(obj);
                        dtgListado.JSProperties["cpAccion"] = e.ButtonID;
                    }
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

        }
    }
}