﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrivateSite.Master" AutoEventWireup="true" CodeBehind="LConsultas.aspx.cs" Inherits="ReAl.GestionMedica.Web.PAC.LConsultas" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="Server">
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/css/bootstrap-select.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.css") %>" rel="stylesheet" />
    <style>
        .bootstrap-select .dropdown-menu {
            max-width: 100%;
            border: 1px solid cornflowerblue;
            border-radius: 2px;
            box-shadow: 0 2px 8px #888;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

            .bootstrap-select .dropdown-menu li a span.text {
                display: inline-block;
                white-space: pre-wrap;
                word-wrap: break-word;
                max-width: 100%;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- Page Heading -->
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
        <div class="container-fluid">
            <div class="page-header-content pt-4">
                <div class="row align-items-center justify-content-between">
                    <div class="col-auto mt-4">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="user"></i></div>
                            <asp:Literal ID="litPacienteTit" runat="server" />
                        </h1>
                        <div class="page-header-subtitle">
                            <asp:LinkButton runat="server" ID="btnHistoria" CssClass="btn btn-light lift"
                                           Text="Historia" OnClick="btnHistoria_OnClick" />
                            <asp:LinkButton runat="server" ID="btnConsultas" CssClass="btn btn-success lift"
                                           Text="Consultas" OnClick="btnConsultas_OnClick" />
                            <asp:LinkButton runat="server" ID="btnRecetas" CssClass="btn btn-light lift"
                                           Text="Recetas" OnClick="btnRecetas_OnClick" />
                            <asp:LinkButton runat="server" ID="btnCirugias" CssClass="btn btn-light lift"
                                           Text="Cirugias" OnClick="btnCirugias_OnClick" />
                            <asp:LinkButton runat="server" ID="btnInformes" CssClass="btn btn-light lift"
                                           Text="Informes" OnClick="btnInformes_OnClick" />
                            <asp:LinkButton runat="server" ID="btnMultimedia" CssClass="btn btn-light lift"
                                           Text="Multimedia" OnClick="btnMultimedia_OnClick" />
                            <asp:LinkButton runat="server" ID="btnCertificados" CssClass="btn btn-light lift"
                                            Text="Certificados" OnClick="btnCertificados_OnClick" />
                            <asp:LinkButton runat="server" ID="btnLiberar" CssClass="btn btn-danger lift"
                                           Text="Liberar" OnClick="btnLiberar_OnClick" />
                        </div>
                    </div>
                    <%--<div class="col-12 col-xl-auto mt-4">Optional page header content</div>--%>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-fluid">
        <div class="card mt-n10">
            <div class="card-header">
                Consultas para:
                <asp:Literal ID="litPacienteSub" runat="server" />
            </div>
            <div class="card-body">
                <p>
                    <!-- Botones de Accion -->
                    <asp:LinkButton ID="btnAtras" runat="server" Text="<i class='fa fa-arrow-left'></i>" ToolTip="Atras"
                        CssClass="btn btn-secondary " class="btn btn-secondary" OnClick="btnAtras_OnClick" />
                    <asp:LinkButton ID="btnNuevo" runat="server" Text="<i class='fa fa-plus-circle'></i>"
                        ToolTip="Nuevo registro"
                        CssClass="btn btn-secondary" class="btn btn-secondary" OnClick="btnNuevo_OnClick" />
                    <asp:LinkButton ID="btnImprimir" runat="server" Text="<i class='fa fa-print'></i>"
                        ToolTip="Imprimir registros"
                        CssClass="btn btn-secondary" class="btn btn-secondary" OnClick="btnImprimir_OnClick" />
                </p>
                <div class="mb-4"></div>
                <div class="text-sm">
                    <asp:GridView ID="dtgListado" runat="server" AutoGenerateColumns="False"
                        data-toggle="table" data-show-columns="true" data-pagination="true"
                        data-search="true" data-show-toggle="true" data-sortable="true"
                        data-page-size="25" data-pagination-v-align="both" data-show-export="true"
                        DataKeyNames="" OnRowCommand="dtgListado_OnRowCommand"
                        CssClass="table table-striped table-bordered table-hover">

                        <Columns>
                            <asp:BoundField ReadOnly="True" DataField="Fecha" HeaderText="Fecha" ShowHeader="false" HtmlEncode="False" DataFormatString="{0:yyyy/MM/dd}">
                                <ItemStyle HorizontalAlign="Justify"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Hora" HeaderText="Hora" ShowHeader="false" HtmlEncode="False" DataFormatString="{0:HH:mm}">
                                <ItemStyle HorizontalAlign="Justify"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="CuadroClinico" HeaderText="CuadroClinico" ShowHeader="false" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Diagnostico" HeaderText="Diagnostico" ShowHeader="false" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="CIE" HeaderText="CIE" ShowHeader="false" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Conducta" HeaderText="Conducta" ShowHeader="false" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Tratamiento" HeaderText="Tratamiento" ShowHeader="false" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:TemplateField HeaderText="Acciones">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="ImbModificar" CommandName="modificar" CommandArgument='<%# Bind("idpco") %>' runat="server"
                                        ToolTip="Modificar el registro"
                                        CssClass="btn btn-secondary btn-sm" Text="<i class='fa fa-edit'></i>" />

                                    <asp:LinkButton ID="ImbEliminar" CommandName="eliminar" CommandArgument='<%# Bind("idpco") %>' runat="server"
                                        ToolTip="Eliminar el registro" OnClientClick="return confirm('¿Esta seguro que desea eliminar el registro?');"
                                        CssClass="btn btn-danger btn-sm" Text="<i class='fa fa-eraser'></i>" />
                                    <button type="button" class="btn btn-warning btn-sm" data-container="body"
                                            data-toggle="popover"
                                            data-placement="left" data-html="true" title="Auditoria"
                                            data-content='<b>Usuario Creacion:</b> <%# Eval("usucre") %> <br />
                                                        <b>Fecha Creacion:</b> <%# Eval("feccre") %> <br />
                                                        <b>Usuario Modificacion:</b> <%# Eval("usumod") %> <br />
                                                        <b>Fecha Modificacion:</b> <%# Eval("fecmod") %> <br />
                                                        <b>Estado:</b> <%# Eval("apiestado") %> '>
                                        <i class='fa fa-search-plus'></i>
                                    </button>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

    <!-- Detail Modal -->
    <div class="modal fade" id="currentdetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <asp:UpdatePanel ID="updModaleDetail" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Detalle de la consulta</h4>
                        </div>
                        <div class="modal-body">
                            <asp:UpdatePanel ID="updVerDetalle" runat="server">
                                <ContentTemplate>
                                    <asp:DetailsView ID="dtgDetalles" runat="server"
                                                     CssClass="table table-striped table-bordered table-hover"
                                                     FieldHeaderStyle-Font-Bold="true" AutoGenerateRows="True">
                                        <Fields>
                                        </Fields>
                                    </asp:DetailsView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="dtgListado" EventName="RowCommand" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- New Modal -->
    <div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <asp:UpdatePanel ID="updModalNew" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header bg-success">
                            <h5 class="modal-title text-white">Nueva consulta</h5>
                            <button class="close text-white" type="button" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label ID="lblNewFechapco">Fecha:</label>
                                        <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                                            <asp:TextBox id="dtpNewFechapco" runat="server"
                                                         required="" MaxLength="10" data-target="#datetimepicker1"
                                                         data-parsley-group="validation-new" data-toggle="datetimepicker"
                                                         data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYYY"
                                                         Class="form-control datetimepicker-input" CssClass="form-control datetimepicker-input" />
                                            <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label ID="lblNewHorapco">Fecha:</label>
                                        <div class="input-group date" id="datetimepicker3" data-target-input="nearest">
                                            <asp:TextBox id="dtpNewHorapco" runat="server"
                                                         required="" MaxLength="10" data-target="#datetimepicker3"
                                                         data-parsley-group="validation-new" data-toggle="datetimepicker"
                                                         data-date-format="HH:mm" placeholder="HH:mm"
                                                         Class="form-control datetimepicker-input" CssClass="form-control datetimepicker-input" />
                                            <div class="input-group-append" data-target="#datetimepicker3" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label id="lblNewCuadroclinicopco" runat="server">Cuadro Clínico:</label>
                                        <asp:TextBox id="txtNewCuadroclinicopco" TextMode="MultiLine"
                                                     data-parsley-maxlength="2000"
                                                     data-parsley-group="validation-new"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="2000" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label id="lblNewIdcie" runat="server">CIE:</label>
                                        <asp:DropDownList ID="ddlNewIdcie" runat="server"
                                                          class="form-control selectpicker" data-live-search="true"
                                                          data-select-on-tab="true" data-size="10"
                                                          CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label id="lblNewDiagnosticopco" runat="server">Diagnostico:</label>
                                        <asp:TextBox id="txtNewDiagnosticopco" TextMode="MultiLine"
                                                     data-parsley-maxlength="2000"
                                                     data-parsley-group="validation-new"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="2000" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label id="lblNewConductapco" runat="server">Conducta:</label>
                                        <asp:TextBox id="txtNewConductapco"
                                                     data-parsley-maxlength="2000" TextMode="MultiLine"
                                                     data-parsley-group="validation-new"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="2000" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label id="lblNewTratamientopco" runat="server">Tratamiento:</label>
                                        <asp:TextBox id="txtNewTratamientopco"
                                                     data-parsley-maxlength="2000" TextMode="MultiLine"
                                                     data-parsley-group="validation-new"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="2000" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-warning" type="button" data-dismiss="modal">Cerrar</button>
                            <asp:Button ID="btnNewGuardar" runat="server" Text="Registrar nuevo"
                                        OnClientClick="return $('#aspnetForm').parsley().validate({group: 'validation-new'});"
                                        CssClass="btn btn-success" OnClick="btnNewGuardar_OnClick" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnNuevo" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnNewGuardar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <asp:UpdatePanel ID="updModaleEdit" runat="server">
                <ContentTemplate>
                    <div class="modal-content">
                        <div class="modal-header bg-secondary">
                            <h5 class="modal-title text-white" id="exampleModalCenterTitle">Editar consulta</h5>
                            <asp:HiddenField ID="hdnIdDatos" runat="server" />
                            <button class="close text-white" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label ID="lblEditFechapco">Fecha:</label>
                                        <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                                            <asp:TextBox id="dtpEditFechapco" runat="server"
                                                         required="" MaxLength="10" data-target="#datetimepicker2"
                                                         data-parsley-group="validation-edit" data-toggle="datetimepicker"
                                                         data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYYY"
                                                         Class="form-control datetimepicker-input" CssClass="form-control datetimepicker-input" />
                                            <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label ID="lblEditHorapco">Hora:</label>
                                        <div class="input-group date" id="datetimepicker4" data-target-input="nearest">
                                            <asp:TextBox id="dtpEditHorapco" runat="server"
                                                         required="" MaxLength="10" data-target="#datetimepicker4"
                                                         data-parsley-group="validation-edit" data-toggle="datetimepicker"
                                                         data-date-format="HH:mm" placeholder="HH:mm"
                                                         Class="form-control datetimepicker-input" CssClass="form-control datetimepicker-input" />
                                            <div class="input-group-append" data-target="#datetimepicker4" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-clock"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label id="lblEditCuadroclinicopco" runat="server">Cuadro Clínico:</label>
                                        <asp:TextBox id="txtEditCuadroclinicopco" TextMode="MultiLine"
                                                     data-parsley-maxlength="2000"
                                                     data-parsley-group="validation-edit"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="2000" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label id="lblEditIdcie" runat="server">CIE:</label>
                                        <asp:DropDownList ID="ddlEditIdcie" runat="server"
                                                          class="form-control selectpicker"
                                                          data-live-search="true"
                                                          data-none-selected-text="No ha seleccionado nada"
                                                          data-select-on-tab="true" data-size="10"
                                                          CssClass="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label id="lblEditDiagnosticopco" runat="server">Diagnostico:</label>
                                        <asp:TextBox id="txtEditDiagnosticopco" TextMode="MultiLine"
                                                     data-parsley-maxlength="2000"
                                                     data-parsley-group="validation-edit"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="2000" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label id="lblEditConductapco" runat="server">Conducta:</label>
                                        <asp:TextBox id="txtEditConductapco" TextMode="MultiLine"
                                                     data-parsley-maxlength="2000"
                                                     data-parsley-group="validation-edit"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="2000" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label id="lblEditTratamientopco" runat="server">Tratamiento:</label>
                                        <asp:TextBox id="txtEditTratamientopco" TextMode="MultiLine"
                                                     data-parsley-maxlength="2000"
                                                     data-parsley-group="validation-edit"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="2000" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-warning" type="button" data-dismiss="modal">Cancelar</button>
                            <asp:Label ID="lblResult" Visible="false" runat="server" />
                            <asp:Button ID="btnEditaGuardar" runat="server" Text="Guardar Cambios" CssClass="btn btn-secondary" OnClick="btnEditaGuardar_OnClick" />
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="dtgListado" EventName="RowCommand" />
                    <asp:AsyncPostBackTrigger ControlID="btnEditaGuardar" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="Server">
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/popper/popper.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/locale/bootstrap-table-es-MX.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/js/bootstrap-select.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/moment/moment.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.js") %>"></script>

    <script>
        $(document).ready(function () {
            $('select').selectpicker();
            $('[data-toggle="popover"]').popover();
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker3').datetimepicker({
                format: 'LT'
            });
            $('#datetimepicker4').datetimepicker({
                format: 'LT'
            });
        });

        function pageLoad() {
            $(function () {
                $('select').selectpicker();
                $('[data-toggle="popover"]').popover();
                $('#datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#datetimepicker2').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#datetimepicker3').datetimepicker({
                    format: 'LT'
                });
                $('#datetimepicker4').datetimepicker({
                    format: 'LT'
                });
            });
        }
    </script>
</asp:Content>