﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Web.App_Helpers;
using ReAl.Utils;

namespace ReAl.GestionMedica.Web.PAC
{
    public partial class LLiberar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                SessionHandler miSesion = new SessionHandler();
                //if (miSesion.ArrMenu != null)
                //    if (!miSesion.ArrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath.Split('/').Last()))
                //        Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));

                if (miSesion.AppPaciente == null)
                    Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));

                if (!Page.IsPostBack)
                {
                    litPacienteTit.Text = miSesion.AppPaciente.nombresppa + " " + miSesion.AppPaciente.appaternoppa + " " +
                                          miSesion.AppPaciente.apmaternoppa;

                    litPacienteSub.Text = miSesion.AppPaciente.nombresppa + " " + miSesion.AppPaciente.appaternoppa + " " +
                                          miSesion.AppPaciente.apmaternoppa;
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        protected void btnAtras_OnClick(object sender, EventArgs e)
        {
            SessionHandler miSesion = new SessionHandler();
            Response.Redirect(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));
        }

        protected void btnHistoria_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LHistoria.aspx");
        }

        protected void btnConsultas_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LConsultas.aspx");
        }

        protected void btnRecetas_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LRecetas.aspx");
        }

        protected void btnCirugias_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LCirugias.aspx");
        }

        protected void btnInformes_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LInformes.aspx");
        }

        protected void btnMultimedia_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LMultimedia.aspx");
        }

        protected void btnLiberar_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/PAC/LLiberar.aspx");
        }

        protected void btnFinalizar_OnClick(object sender, EventArgs e)
        {
            bool bProcede = false;
            var miSession = new SessionHandler();

            try
            {
                var rn = new RnGasLibro();

                //Insertamos el registro de cobro pendiente
                var obj = new EntGasLibro();
                if (miSession.AppCita == null)
                {
                    obj.idcci = null;
                    obj.fechagli = DateTime.Now;
                }
                else
                {
                    //Cambiamos el registro de la Cita
                    var rnCit = new RnCalCitas();
                    miSession.AppCita.labelcci = 3;
                    miSession.AppCita.apitransaccion = CApi.Transaccion.MODIFICAR.ToString();
                    miSession.AppCita.usumod = miSession.AppUsuario.loginsus;
                    rnCit.Update(miSession.AppCita);

                    obj.idcci = miSession.AppCita.idcci;
                    obj.fechagli = miSession.AppCita.iniciocci;
                }
                obj.idgca = 1;
                obj.nombregli = "Consulta Paciente";
                obj.descripciongli = "Paciente " + miSession.AppPaciente.nombresppa + " " + miSession.AppPaciente.appaternoppa + " " + miSession.AppPaciente.apmaternoppa;
                obj.tipogli = "I";
                obj.monto = Decimal.Parse(txtNewMonto.Text.ToString());
                obj.apiestado = CApi.Estado.PROGRAMADO.ToString();
                obj.apitransaccion = CApi.Transaccion.PROGRAMAR.ToString();
                obj.usucre = miSession.AppUsuario.loginsus;
                rn.Insert(obj);

                //Liberamos al paciente
                miSession.AppCita = new EntCalCitas();
                miSession.AppCita.idcci = 0;

                //Liberamos al Paciente
                miSession.AppPaciente = new EntPacPacientes();
                miSession.AppPaciente.idppa = 0;

                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }

            if (bProcede)
                Response.Redirect(SiteHelper.GetMainPage(miSession.AppRol, miSession.ArrMenu));
        }
    }
}