﻿using System;
using System.Globalization;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.Http;

namespace ReAl.GestionMedica.Web
{
    public class Global : HttpApplication
    {
        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            // Si se envía alguna información acerca del usuario
            if (HttpContext.Current.User != null)
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                    if (HttpContext.Current.User.Identity is FormsIdentity)
                    {
                        // Se recupera el ticket del usuario
                        var tkt = FormsAuthentication.Decrypt(Request.Cookies[FormsAuthentication.FormsCookieName].Value);

                        // Se recupera la información acerca del usuario, donde metimos la información de los roles
                        var roles = tkt.UserData.Split(';');

                        // Se recupera la identidad del usuario para recuperar sus roles
                        var id = (FormsIdentity)HttpContext.Current.User.Identity;

                        // Se crea un usuario con dichos roles
                        HttpContext.Current.User = new GenericPrincipal(id, roles);
                    }
        }

        private void Application_Start(object sender, EventArgs e)
        {
            // Código que se ejecuta al iniciar la aplicación
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            WebApiConfig.Register(GlobalConfiguration.Configuration);

            //KB: https://docs.microsoft.com/en-us/aspnet/web-api/overview/getting-started-with-aspnet-web-api/using-web-api-with-aspnet-web-forms
            RouteTable.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = System.Web.Http.RouteParameter.Optional }
            );

            //Para la documentacion del WebAPI
            //KB: https://docs.microsoft.com/en-us/aspnet/web-api/overview/getting-started-with-aspnet-web-api/creating-api-help-pages
            //AreaRegistration.RegisterAllAreas();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            try
            {
                Thread.CurrentThread.CurrentCulture = new CultureInfo(Request.UserLanguages[0], false);
                Thread.CurrentThread.CurrentCulture =
                    CultureInfo.CreateSpecificCulture(Thread.CurrentThread.CurrentCulture.Name);

                Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator = ".";
                Thread.CurrentThread.CurrentCulture.NumberFormat.NumberGroupSeparator = ",";
            }
            catch (Exception)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("es-ES");
            }
            finally
            {
                Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture;
            }
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            if (User.Identity.IsAuthenticated)
            {
                FormsAuthentication.SignOut();
                FormsAuthentication.RedirectToLoginPage();
            }
        }
    }
}