﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Web.App_Helpers;
using ReAl.Utils;

namespace ReAl.GestionMedica.Web.SEG
{
    public partial class LPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                SessionHandler miSesion = new SessionHandler();
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        protected void btnEditaGuardar_OnClick(object sender, EventArgs e)
        {
            SessionHandler miSesion = new SessionHandler();
            var bProcede = false;
            try
            {
                string strId = miSesion.AppUsuario.idsus.ToString();

                RnSegUsuarios rn = new RnSegUsuarios();
                EntSegUsuarios obj = rn.ObtenerObjeto(rn.GetColumnType(strId, EntSegUsuarios.Fields.idsus));

                //Cambiamos el Pass

                if (txtNew.Text == txtRepeat.Text)
                {
                    if (obj.passsus.ToUpper() == cFuncionesEncriptacion.GenerarMd5(txtActual.Text).ToUpper())
                    {
                        obj.passsus = cFuncionesEncriptacion.GenerarMd5(txtNew.Text).ToUpper();

                        obj.usumodsus = miSesion.AppUsuario.loginsus;
                        obj.apitransaccionsus = CApi.Transaccion.MODIFICAR.ToString();

                        rn.Update(obj);
                        bProcede = true;
                    }
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }
            if (bProcede)
                Response.Redirect(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu) +
                                  "?msg=Se ha actualizado satisfactoriamente su contraseña");
        }
    }
}