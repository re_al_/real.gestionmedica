﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrivateSite.Master" AutoEventWireup="true" CodeBehind="LUsuarios.aspx.cs" Inherits="ReAl.GestionMedica.Web.SEG.LUsuarios" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="Server">
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/css/bootstrap-select.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.css") %>" rel="stylesheet" />
    <style>
        .bootstrap-select .dropdown-menu {
            max-width: 100%;
            border: 1px solid cornflowerblue;
            border-radius: 2px;
            box-shadow: 0 2px 8px #888;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

            .bootstrap-select .dropdown-menu li a span.text {
                display: inline-block;
                white-space: pre-wrap;
                word-wrap: break-word;
                max-width: 100%;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <!-- Page Heading -->
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
        <div class="container-fluid">
            <div class="page-header-content pt-4">
                <div class="row align-items-center justify-content-between">
                    <div class="col-auto mt-4">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="users"></i></div>
                            Usuarios
                        </h1>
                        <div class="page-header-subtitle">
                        </div>
                    </div>
                    <%--<div class="col-12 col-xl-auto mt-4">Optional page header content</div>--%>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-fluid">
        <div class="card mt-n10">
            <div class="card-header">
                Registro de usuarios del sistema
            </div>
            <div class="card-body">
                <p>
                    <!-- Botones de Accion -->
                    <asp:LinkButton ID="btnAtras" runat="server" Text="<i class='fa fa-arrow-left'></i>" ToolTip="Atras"
                        CssClass="btn btn-secondary " class="btn btn-secondary" OnClick="btnAtras_OnClick" />
                    <asp:LinkButton ID="btnNuevo" runat="server" Text="<i class='fa fa-plus-circle'></i>"
                        ToolTip="Nuevo registro"
                        CssClass="btn btn-secondary" class="btn btn-secondary" OnClick="btnNuevo_OnClick" />
                    <asp:LinkButton ID="btnImprimir" runat="server" Text="<i class='fa fa-print'></i>"
                        ToolTip="Imprimir registros"
                        CssClass="btn btn-secondary" class="btn btn-secondary" OnClick="btnImprimir_OnClick" />
                </p>
                <div class="mb-4"></div>
                <div class="text-sm">
                    <asp:GridView ID="dtgListado" runat="server" AutoGenerateColumns="False"
                        data-toggle="table" data-show-columns="true" data-pagination="true"
                        data-search="true" data-show-toggle="true" data-sortable="true"
                        data-page-size="25" data-pagination-v-align="both" data-show-export="true"
                        DataKeyNames="" OnRowCommand="dtgListado_OnRowCommand"
                        CssClass="table table-striped table-bordered table-hover">

                        <columns>
                            <asp:BoundField ReadOnly="True" DataField="Login" HeaderText="Login" ShowHeader="false" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Nombre" HeaderText="Nombre" ShowHeader="false" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Rol" HeaderText="Rol" ShowHeader="false" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Vigente" HeaderText="Vigente" ShowHeader="false" HtmlEncode="False">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Vigencia" HeaderText="Vigencia" ShowHeader="false" HtmlEncode="False" DataFormatString="{0:yyyy/MM/dd}">
                                <ItemStyle HorizontalAlign="Justify"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:TemplateField HeaderText="Acciones">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="ImbModificar" CommandName="modificar" CommandArgument='<%# Bind("idsus") %>' runat="server"
                                        ToolTip="Modificar el registro"
                                        CssClass="btn btn-secondary btn-sm" Text="<i class='fa fa-edit'></i>" />

                                    <asp:LinkButton ID="ImbEliminar" CommandName="eliminar" CommandArgument='<%# Bind("idsus") %>' runat="server"
                                        ToolTip="Eliminar el registro" OnClientClick="return confirm('¿Esta seguro que desea eliminar el registro?');"
                                        CssClass="btn btn-danger btn-sm" Text="<i class='fa fa-eraser'></i>" />
                                    <button type="button" class="btn btn-warning btn-sm" data-container="body"
                                            data-toggle="popover"
                                            data-placement="left" data-html="true" title="Auditoria"
                                            data-content='<b>Usuario Creacion:</b> <%# Eval("usucresus") %> <br />
                                                        <b>Fecha Creacion:</b> <%# Eval("feccresus") %> <br />
                                                        <b>Usuario Modificacion:</b> <%# Eval("usumodsus") %> <br />
                                                        <b>Fecha Modificacion:</b> <%# Eval("fecmodsus") %> <br />
                                                        <b>Estado:</b> <%# Eval("apiestadosus") %> '>
                                        <i class='fa fa-search-plus'></i>
                                    </button>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>

    <!-- Detail Modal -->
    <div class="modal fade" id="currentdetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <asp:UpdatePanel ID="updModaleDetail" runat="server">
                <contenttemplate>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">Detalle del usuario</h4>
                        </div>
                        <div class="modal-body">
                            <asp:UpdatePanel ID="updVerDetalle" runat="server">
                                <ContentTemplate>
                                    <asp:DetailsView ID="dtgDetalles" runat="server"
                                                     CssClass="table table-striped table-bordered table-hover"
                                                     FieldHeaderStyle-Font-Bold="true" AutoGenerateRows="True">
                                        <Fields>
                                        </Fields>
                                    </asp:DetailsView>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="dtgListado" EventName="RowCommand" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </contenttemplate>
            </asp:UpdatePanel>
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- New Modal -->
    <div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <asp:UpdatePanel ID="updModalNew" runat="server">
                <contenttemplate>
                    <div class="modal-content">
                        <div class="modal-header bg-success">
                            <h5 class="modal-title text-white">Nuevo usuario</h5>
                            <button class="close text-white" type="button" data-dismiss="modal" aria-label="Cerrar"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:Literal id="lbl_loginsus" runat="server" text="Usuario:" />
                                        <asp:TextBox id="txtNew_loginsus"
                                                     data-parsley-maxlength="15" required=""
                                                     data-parsley-group="validation-new"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="15" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="form-group">
                                            <asp:Literal id="lbl_passsus" runat="server" text="Contraseña:" />
                                            <asp:TextBox id="txtNew_passsus" TextMode="Password"
                                                         data-parsley-maxlength="256" required=""
                                                         data-parsley-group="validation-new"
                                                         Class="form-control" CssClass="form-control"
                                                         runat="server" MaxLength="256" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:Literal id="lbl_nombresus" runat="server" text="Nombres:" />
                                        <asp:TextBox id="txtNew_nombresus"
                                                     data-parsley-maxlength="256" required=""
                                                     data-parsley-group="validation-new"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="256" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:Literal id="lbl_apellidosus" runat="server" text="Apellidos:" />
                                        <asp:TextBox id="txtNew_apellidosus"
                                                     data-parsley-maxlength="256" required=""
                                                     data-parsley-group="validation-new"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="256" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="lblNewIdsro" runat="server">Rol:</label>
                                        <asp:DropDownList ID="ddlNewIdsro" runat="server"
                                                          class="form-control selectpicker" data-live-search="true"
                                                          data-select-on-tab="true" data-size="10"
                                                          CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label ID="lbl_fechavigentesus">Fecha vigencia:</label>
                                        <div class="input-group date" id="datetimepicker1" data-target-input="nearest">
                                            <asp:TextBox id="txtNew_fechavigentesus" runat="server"
                                                         required="" MaxLength="10" data-target="#datetimepicker1"
                                                         data-parsley-group="validation-new" data-toggle="datetimepicker"
                                                         data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYYY"
                                                         Class="form-control datetimepicker-input" CssClass="form-control datetimepicker-input" />
                                            <div class="input-group-append" data-target="#datetimepicker1" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-warning" type="button" data-dismiss="modal">Cerrar</button>
                            <asp:Button ID="btnNewGuardar" runat="server" Text="Registrar nuevo"
                                        OnClientClick="return $('#aspnetForm').parsley().validate({group: 'validation-new'});"
                                        CssClass="btn btn-success" OnClick="btnNewGuardar_OnClick" />
                        </div>
                    </div>
                </contenttemplate>
                <triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnNuevo" EventName="Click" />
                    <asp:AsyncPostBackTrigger ControlID="btnNewGuardar" EventName="Click" />
                </triggers>
            </asp:UpdatePanel>
        </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <asp:UpdatePanel ID="updModaleEdit" runat="server">
                <contenttemplate>
                    <div class="modal-content">
                        <div class="modal-header bg-secondary">
                            <h5 class="modal-title text-white" id="exampleModalCenterTitle">Editar consulta</h5>
                            <asp:HiddenField ID="hdnIdDatos" runat="server" />
                            <button class="close text-white" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:Literal id="Literal1" runat="server" text="Usuario:" />
                                        <asp:TextBox id="txtEdit_loginsus" ReadOnly="True"
                                                     data-parsley-maxlength="15" required=""
                                                     data-parsley-group="validation-edit"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="15" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        &nbsp;
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:Literal id="Literal2" runat="server" text="Nombres:" />
                                        <asp:TextBox id="txtEdit_nombresus"
                                                     data-parsley-maxlength="256" required=""
                                                     data-parsley-group="validation-edit"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="256" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:Literal id="Literal3" runat="server" text="Apellidos:" />
                                        <asp:TextBox id="txtEdit_apellidosus"
                                                     data-parsley-maxlength="256" required=""
                                                     data-parsley-group="validation-edit"
                                                     Class="form-control" CssClass="form-control"
                                                     runat="server" MaxLength="256" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label id="Label1" runat="server">Rol:</label>
                                        <asp:DropDownList ID="ddlEditIdsro" runat="server"
                                                          class="form-control selectpicker" data-live-search="true"
                                                          data-select-on-tab="true" data-size="10"
                                                          CssClass="form-control" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label ID="lbl_fechavigentesus2">Fecha:</label>
                                        <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                                            <asp:TextBox id="txtEdit_fechavigentesus" runat="server"
                                                         required="" MaxLength="10" data-target="#datetimepicker2"
                                                         data-parsley-group="validation-edit" data-toggle="datetimepicker"
                                                         data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYYY"
                                                         Class="form-control datetimepicker-input" CssClass="form-control datetimepicker-input" />
                                            <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-warning" type="button" data-dismiss="modal">Cancelar</button>
                            <asp:Label ID="lblResult" Visible="false" runat="server" />
                            <asp:Button ID="btnEditaGuardar" runat="server" Text="Guardar Cambios" CssClass="btn btn-secondary"
                                        OnClientClick="return $('#aspnetForm').parsley().validate({group: 'validation-edit'});"
                                        OnClick="btnEditaGuardar_OnClick" />
                        </div>
                    </div>
                </contenttemplate>
                <triggers>
                    <asp:AsyncPostBackTrigger ControlID="dtgListado" EventName="RowCommand" />
                    <asp:AsyncPostBackTrigger ControlID="btnEditaGuardar" EventName="Click" />
                </triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="Server">
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/popper/popper.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/locale/bootstrap-table-es-MX.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/js/bootstrap-select.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/moment/moment.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.js") %>"></script>

    <script>
        $(document).ready(function () {
            $('select').selectpicker();
            $('[data-toggle="popover"]').popover();
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        function pageLoad() {
            $(function () {
                $('select').selectpicker();
                $('[data-toggle="popover"]').popover();
                $('#datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#datetimepicker2').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        }
    </script>
</asp:Content>