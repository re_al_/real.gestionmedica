﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrivateSite.Master" AutoEventWireup="true" CodeBehind="LPassword.aspx.cs" Inherits="ReAl.GestionMedica.Web.SEG.LPassword" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="Server">
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/css/bootstrap-select.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.css") %>" rel="stylesheet" />
    <style>
        .bootstrap-select .dropdown-menu {
            max-width: 100%;
            border: 1px solid cornflowerblue;
            border-radius: 2px;
            box-shadow: 0 2px 8px #888;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

            .bootstrap-select .dropdown-menu li a span.text {
                display: inline-block;
                white-space: pre-wrap;
                word-wrap: break-word;
                max-width: 100%;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-fluid">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="user"></i></div>
                            Configuracion de usuario
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="container mt-4">
        <!-- Account page navigation-->
        <nav class="nav nav-borders">
            <a class="nav-link" href="LPerfil.aspx">Perfil</a>
            <a class="nav-link active ml-0" href="LPassword.aspx">Seguridad</a>
        </nav>
        <hr class="mt-0 mb-4" />
        <div class="row">
            <div class="col-md-12 col-xl-12">
                <!-- Account details card-->
                <div class="card mb-4">
                    <div class="card-header">Cambiar contraseña de usuario</div>
                    <div class="card-body">
                        <!-- Form Row-->
                        <div class="form-row">
                            <!-- Form Group (first name)-->
                            <div class="form-group col-md-6">
                                <label id="lblPassActual" runat="server">Contrase&ntilde;a actual:</label>
                                <asp:TextBox ID="txtActual" runat="server" TextMode="Password"
                                    required="" data-parsley-group="validation-new"
                                    onkeypress="return textUtil.allowChars(this,event,true)"
                                    validtipo="password" oncopy="return false" onpaste="return false"
                                    Class="form-control" CssClass="form-control" />
                            </div>
                        </div>
                        <!-- Form Row-->
                        <div class="form-row">
                            <!-- Form Group (last name)-->
                            <div class="form-group col-md-6">
                                <label class="small mb-1" for="inputLastName">Contrase&ntilde;a nueva:</label>
                                <asp:TextBox ID="txtNew" runat="server" TextMode="Password"
                                    required="" data-parsley-group="validation-new"
                                    onkeypress="return textUtil.allowChars(this,event,true)"
                                    validtipo="password" oncopy="return false" onpaste="return false"
                                    Class="form-control" CssClass="form-control" />
                            </div>
                        </div>
                        <!-- Form Row        -->
                        <div class="form-row">
                            <!-- Form Group (organization name)-->
                            <div class="form-group col-md-6">
                                <label class="small mb-1" for="inputOrgName">Repetir contrase&ntilde;a nueva:</label>
                                <asp:TextBox ID="txtRepeat" runat="server" TextMode="Password"
                                             required="" data-parsley-group="validation-new"
                                             onkeypress="return textUtil.allowChars(this,event,true)"
                                             validtipo="password" oncopy="return false" onpaste="return false"
                                             Class="form-control" CssClass="form-control"/>
                            </div>
                        </div>
                        <!-- Save changes button-->
                        <asp:Button ID="btnEditaGuardar" runat="server" Text="Guardar Cambios" CssClass="btn btn-secondary"
                            OnClientClick="return $('#aspnetForm').parsley().validate({group: 'validation-edit'});"
                            OnClick="btnEditaGuardar_OnClick" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="Server">
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/popper/popper.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/locale/bootstrap-table-es-MX.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/js/bootstrap-select.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/moment/moment.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.js") %>"></script>

    <script>
        $(document).ready(function () {
            $('select').selectpicker();
            $('[data-toggle="popover"]').popover();
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        function pageLoad() {
            $(function () {
                $('select').selectpicker();
                $('[data-toggle="popover"]').popover();
                $('#datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#datetimepicker2').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        }
    </script>
</asp:Content>