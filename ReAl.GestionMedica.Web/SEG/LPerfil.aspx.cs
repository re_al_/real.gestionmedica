﻿using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Web.UI;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Web;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Web.App_Helpers;

namespace ReAl.GestionMedica.Web.SEG
{
    public partial class LPerfil : System.Web.UI.Page
    {
        #region Methods

        public void CargarDatosEntrar()
        {
            try
            {
                SessionHandler miSesion = new SessionHandler();
                string strId = miSesion.AppUsuario.idsus.ToString();

                var rn = new RnSegUsuarios();
                EntSegUsuarios objPag = rn.ObtenerObjeto(rn.GetColumnType(strId, EntSegUsuarios.Fields.idsus));
                txtEdit_loginsus.Text = objPag.loginsus;
                txtEdit_nombresus.Text = objPag.nombresus;
                txtEdit_apellidosus.Text = objPag.apellidosus;
                txtEdit_fechavigentesus.Text = objPag.fechavigentesus.ToString(CParametros.ParFormatoFecha);

                //Obtenemos el rol activo
                RnSegUsuariosrestriccion rnSur = new RnSegUsuariosrestriccion();
                ArrayList arrColWhere = new ArrayList();
                arrColWhere.Add(EntSegUsuariosrestriccion.Fields.idsus.ToString());
                arrColWhere.Add(EntSegUsuariosrestriccion.Fields.rolactivosur.ToString());
                ArrayList arrValWhere = new ArrayList();
                arrValWhere.Add(objPag.idsus);
                arrValWhere.Add(1);

                EntSegUsuariosrestriccion objSur = rnSur.ObtenerObjeto(arrColWhere, arrValWhere);
                ddlEditIdsro.SelectedValue = objSur.rolsro.ToString();
                ddlEditIdsro.Enabled = false;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        protected void btnEditaGuardar_OnClick(object sender, EventArgs e)
        {
            SessionHandler miSesion = new SessionHandler();
            var bProcede = false;
            try
            {
                string strId = miSesion.AppUsuario.idsus.ToString();

                RnSegUsuarios rn = new RnSegUsuarios();
                RnSegUsuariosrestriccion rnSur = new RnSegUsuariosrestriccion();
                EntSegUsuarios objPag = rn.ObtenerObjeto(rn.GetColumnType(strId, EntSegUsuarios.Fields.idsus));

                objPag.loginsus = rn.GetColumnType(txtEdit_loginsus.Text, EntSegUsuarios.Fields.loginsus);
                objPag.nombresus = rn.GetColumnType(txtEdit_nombresus.Text, EntSegUsuarios.Fields.nombresus);
                objPag.apellidosus = rn.GetColumnType(txtEdit_apellidosus.Text, EntSegUsuarios.Fields.apellidosus);
                //objPag.vigentesus = rn.GetColumnType(txtEdit_vigentesus.Text, EntSegUsuarios.Fields.vigentesus);
                objPag.fechavigentesus = rn.GetColumnType(txtEdit_fechavigentesus.Text, EntSegUsuarios.Fields.fechavigentesus);

                objPag.apitransaccionsus = CApi.Transaccion.MODIFICAR.ToString();
                objPag.usumodsus = miSesion.AppUsuario.loginsus;
                rn.Update(objPag);

                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }
            if (bProcede)
                Response.Redirect(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu) +
                                  "?msg=Se ha registrado satisfactoriamente los cambios");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                SessionHandler miSesion = new SessionHandler();

                if (!Page.IsPostBack)
                {
                    CargarCmbIdsro();
                    CargarDatosEntrar();
                }

                var auth = new CGoogleApi();
                if (auth.VerificarUsuario(Server.MapPath("~/google/"), miSesion.AppUsuario.loginsus))
                {
                    try
                    {
                        //var credencial = auth.ObtenerCrearCredencial(Server.MapPath("~/google/"), miSesion.AppUsuario.loginsus);
                        var credencial = auth.ObtenerCrearCredencialWeb(Server.MapPath("~/google/"), miSesion.AppUsuario.loginsus);
                        miSesion.GAppsCredential = credencial;
                        miSesion.GAppsUsuario = auth.ObtenerUsuario(credencial);
                        btnConectarSocial.Visible = false;
                    }
                    catch (Exception exception)
                    {
                        btnConectarSocial.Visible = true;
                        var credPath = Server.MapPath("~/google/") + "/" + miSesion.AppUsuario.loginsus + "_token.json";
                        if (!Directory.Exists(credPath))
                        {
                            Directory.CreateDirectory(credPath);
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        private void CargarCmbIdsro()
        {
            try
            {
                var rn = new RnSegRoles();
                var dt = rn.ObtenerDataTable();

                ddlEditIdsro.DataValueField = EntSegRoles.Fields.rolsro.ToString();
                ddlEditIdsro.DataTextField = EntSegRoles.Fields.siglasro.ToString();
                ddlEditIdsro.DataSource = dt;
                ddlEditIdsro.DataBind();
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        #endregion Methods

        protected void btnConectarSocial_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("~/SEG/LAuthGoogle.aspx");
        }
    }
}