﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrivateSite.Master" AutoEventWireup="true" CodeBehind="LPerfil.aspx.cs" Inherits="ReAl.GestionMedica.Web.SEG.LPerfil" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="Server">
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/css/bootstrap-select.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.css") %>" rel="stylesheet" />
    <style>
        .bootstrap-select .dropdown-menu {
            max-width: 100%;
            border: 1px solid cornflowerblue;
            border-radius: 2px;
            box-shadow: 0 2px 8px #888;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

            .bootstrap-select .dropdown-menu li a span.text {
                display: inline-block;
                white-space: pre-wrap;
                word-wrap: break-word;
                max-width: 100%;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <header class="page-header page-header-compact page-header-light border-bottom bg-white mb-4">
        <div class="container-fluid">
            <div class="page-header-content">
                <div class="row align-items-center justify-content-between pt-3">
                    <div class="col-auto mb-3">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="user"></i></div>
                            Configuracion de usuario
                        </h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="container mt-4">
        <!-- Account page navigation-->
        <nav class="nav nav-borders">
            <a class="nav-link active ml-0" href="LPerfil.aspx">Perfil</a>
            <a class="nav-link" href="LPassword.aspx">Seguridad</a>
        </nav>
        <hr class="mt-0 mb-4" />
        <div class="row">
            <div class="col-md-12 col-xl-12">
                <!-- Account details card-->
                <div class="card mb-4">
                    <div class="card-header">Detalles de cuenta de usuario</div>
                    <div class="card-body">
                        <!-- Form Group (username)-->
                        <div class="form-group">
                            <label class="small mb-1" for="inputUsername">Usuario:</label>
                            <asp:TextBox ID="txtEdit_loginsus" ReadOnly="True"
                                data-parsley-maxlength="15" required=""
                                data-parsley-group="validation-edit"
                                Class="form-control" CssClass="form-control"
                                runat="server" MaxLength="15" />
                        </div>
                        <!-- Form Row-->
                        <div class="form-row">
                            <!-- Form Group (first name)-->
                            <div class="form-group col-md-6">
                                <label class="small mb-1" for="inputFirstName">Nombres</label>
                                <asp:TextBox ID="txtEdit_nombresus"
                                    data-parsley-maxlength="256" required=""
                                    data-parsley-group="validation-edit"
                                    Class="form-control" CssClass="form-control"
                                    runat="server" MaxLength="256" />
                            </div>
                            <!-- Form Group (last name)-->
                            <div class="form-group col-md-6">
                                <label class="small mb-1" for="inputLastName">Apellidos</label>
                                <asp:TextBox ID="txtEdit_apellidosus"
                                    data-parsley-maxlength="256" required=""
                                    data-parsley-group="validation-edit"
                                    Class="form-control" CssClass="form-control"
                                    runat="server" MaxLength="256" />
                            </div>
                        </div>
                        <!-- Form Row        -->
                        <div class="form-row">
                            <!-- Form Group (organization name)-->
                            <div class="form-group col-md-6">
                                <label class="small mb-1" for="inputOrgName">Rol</label>
                                <asp:DropDownList ID="ddlEditIdsro" runat="server"
                                    class="form-control selectpicker" data-live-search="true"
                                    data-select-on-tab="true" data-size="10"
                                    CssClass="form-control" />
                            </div>
                            <!-- Form Group (location)-->
                            <div class="form-group col-md-6">
                                <label class="small mb-1" for="inputLocation">Fecha de Nacimiento</label>
                                <div class="input-group date" id="datetimepicker2" data-target-input="nearest">
                                    <asp:TextBox ID="txtEdit_fechavigentesus" runat="server"
                                        required="" MaxLength="10" data-target="#datetimepicker2"
                                        data-parsley-group="validation-edit" data-toggle="datetimepicker"
                                        data-date-format="DD/MM/YYYY" placeholder="DD/MM/YYYYY"
                                        Class="form-control datetimepicker-input" CssClass="form-control datetimepicker-input" />
                                    <div class="input-group-append" data-target="#datetimepicker2" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Save changes button-->
                        <asp:Button ID="btnEditaGuardar" runat="server" Text="Guardar Cambios" CssClass="btn btn-secondary"
                            OnClientClick="return $('#aspnetForm').parsley().validate({group: 'validation-edit'});"
                            OnClick="btnEditaGuardar_OnClick" />
                        <asp:Button ID="btnConectarSocial" runat="server" Text="Conectar Google" CssClass="btn btn-primary"
                            OnClick="btnConectarSocial_OnClick" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="Server">
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/popper/popper.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/locale/bootstrap-table-es-MX.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/js/bootstrap-select.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/moment/moment.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.js") %>"></script>

    <script>
        $(document).ready(function () {
            $('select').selectpicker();
            $('[data-toggle="popover"]').popover();
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        function pageLoad() {
            $(function () {
                $('select').selectpicker();
                $('[data-toggle="popover"]').popover();
                $('#datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#datetimepicker2').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        }
    </script>
</asp:Content>