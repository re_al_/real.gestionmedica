﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Web.App_Helpers;
using ReAl.Utils;

namespace ReAl.GestionMedica.Web.SEG
{
    public partial class LUsuarios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                SessionHandler miSesion = new SessionHandler();
                if (miSesion.ArrMenu != null)
                    if (!miSesion.ArrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath.Split('/').Last()))
                        Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));

                if (!Page.IsPostBack)
                {
                    CargarCmbIdsro();
                    CargarListado();
                }
                else
                {
                    CargarListado();
                    ControlHelper.CrearEstilosGrid(ref dtgListado);
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        private void CargarCmbIdsro()
        {
            try
            {
                var rn = new RnSegRoles();
                var dt = rn.ObtenerDataTable();
                ddlNewIdsro.DataValueField = EntSegRoles.Fields.rolsro.ToString();
                ddlNewIdsro.DataTextField = EntSegRoles.Fields.siglasro.ToString();
                ddlNewIdsro.DataSource = dt;
                ddlNewIdsro.DataBind();

                ddlEditIdsro.DataValueField = EntSegRoles.Fields.rolsro.ToString();
                ddlEditIdsro.DataTextField = EntSegRoles.Fields.siglasro.ToString();
                ddlEditIdsro.DataSource = dt;
                ddlEditIdsro.DataBind();
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        public void CargarListado()
        {
            try
            {
                SessionHandler miSesion = new SessionHandler();
                String strNombreSp = "SpSusSelGrid";

                var rn = new RnVista();
                var dt = rn.ObtenerDatosProcAlm(strNombreSp);
                dtgListado.DataSource = dt;
                dtgListado.DataBind();
                ControlHelper.CrearEstilosGrid(ref dtgListado);
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        protected void btnAtras_OnClick(object sender, EventArgs e)
        {
            SessionHandler miSesion = new SessionHandler();
            Response.Redirect(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));
        }

        protected void btnNuevo_OnClick(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "newModal", "$('#newModal').modal('show');", true);
        }

        protected void dtgListado_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            String strRedireccion = "";
            try
            {
                if (e.CommandName.Equals("detalles"))
                {
                    string strId = e.CommandArgument.ToString();

                    //Filtramos el Dataset
                    DataView dv = ((DataTable)dtgListado.DataSource).DefaultView;
                    dv.RowFilter = EntPacRecetas.Fields.idpre + " = " + strId;

                    DataTable detailTable = dv.ToTable();

                    dtgDetalles.DataSource = detailTable;
                    dtgDetalles.DataBind();
                    dtgDetalles.HeaderRow.TableSection = TableRowSection.TableHeader;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#currentdetail').appendTo('body').modal('show');", true);
                }
                else if (e.CommandName.Equals("modificar"))
                {
                    var strId = e.CommandArgument.ToString();
                    var rn = new RnSegUsuarios();
                    EntSegUsuarios objPag = rn.ObtenerObjeto(rn.GetColumnType(strId, EntSegUsuarios.Fields.idsus));
                    hdnIdDatos.Value = objPag.idsus.ToString();
                    txtEdit_loginsus.Text = objPag.loginsus;
                    txtEdit_nombresus.Text = objPag.nombresus;
                    txtEdit_apellidosus.Text = objPag.apellidosus;
                    txtEdit_fechavigentesus.Text = objPag.fechavigentesus.ToString(CParametros.ParFormatoFecha);
                    lblResult.Visible = false;

                    //Obtenemos el rol activo
                    RnSegUsuariosrestriccion rnSur = new RnSegUsuariosrestriccion();
                    ArrayList arrColWhere = new ArrayList();
                    arrColWhere.Add(EntSegUsuariosrestriccion.Fields.idsus.ToString());
                    arrColWhere.Add(EntSegUsuariosrestriccion.Fields.rolactivosur.ToString());
                    ArrayList arrValWhere = new ArrayList();
                    arrValWhere.Add(objPag.idsus);
                    arrValWhere.Add(1);

                    EntSegUsuariosrestriccion objSur = rnSur.ObtenerObjeto(arrColWhere, arrValWhere);
                    ddlEditIdsro.SelectedValue = objSur.rolsro.ToString();

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("$('#editModal').modal('show');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);
                }
                else if (e.CommandName.Equals("eliminar"))
                {
                    string strId = e.CommandArgument.ToString();

                    var rn = new RnPacRecetas();
                    var obj = rn.ObtenerObjeto(rn.GetColumnType(strId, EntPacRecetas.Fields.idpre));

                    if (obj != null)
                    {
                        rn.Delete(obj);
                        strRedireccion = "~/PAC/LRecetas";
                    }
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (!string.IsNullOrEmpty(strRedireccion))
                Response.Redirect(strRedireccion);
        }

        protected void btnNewGuardar_OnClick(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var miSesion = new SessionHandler();
                RnSegUsuarios rn = new RnSegUsuarios();
                RnSegUsuariosrestriccion rnSur = new RnSegUsuariosrestriccion();

                EntSegUsuarios objPag = new EntSegUsuarios();
                objPag.loginsus = rn.GetColumnType(txtNew_loginsus.Text, EntSegUsuarios.Fields.loginsus);
                objPag.passsus = cFuncionesEncriptacion.GenerarMd5(txtNew_passsus.Text).ToUpper();
                objPag.nombresus = rn.GetColumnType(txtNew_nombresus.Text, EntSegUsuarios.Fields.nombresus);
                objPag.apellidosus = rn.GetColumnType(txtNew_apellidosus.Text, EntSegUsuarios.Fields.apellidosus);
                objPag.vigentesus = 1;
                objPag.fechavigentesus = rn.GetColumnType(txtNew_fechavigentesus.Text, EntSegUsuarios.Fields.fechavigentesus);
                objPag.fechapasssus = DateTime.Now;

                objPag.usucresus = miSesion.AppUsuario.loginsus;
                rn.Insert(objPag);

                //Obtenemos el Id insertado
                EntSegUsuarios objNuevo = rn.ObtenerObjeto(EntSegUsuarios.Fields.loginsus, "'" + objPag.loginsus + "'");

                //Insertamos el rol seleccionado
                EntSegUsuariosrestriccion objSur = new EntSegUsuariosrestriccion();
                objSur.idsus = objNuevo.idsus;
                objSur.rolsro = int.Parse(ddlNewIdsro.SelectedValue.ToString());
                objSur.rolactivosur = 1;
                objSur.fechavigentesur = DateTime.Now.AddYears(20);
                objSur.usucresur = miSesion.AppUsuario.loginsus;
                rnSur.Insert(objSur);

                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (bProcede)
                Response.Redirect(
                    Request.AppRelativeCurrentExecutionFilePath +
                    "?msg=Se ha creado el registro satisfactoriamente");
        }

        protected void btnEditaGuardar_OnClick(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var miSesion = new SessionHandler();
                RnSegUsuarios rn = new RnSegUsuarios();
                RnSegUsuariosrestriccion rnSur = new RnSegUsuariosrestriccion();
                EntSegUsuarios objPag = rn.ObtenerObjeto(rn.GetColumnType(hdnIdDatos.Value, EntSegUsuarios.Fields.idsus));

                objPag.loginsus = rn.GetColumnType(txtEdit_loginsus.Text, EntSegUsuarios.Fields.loginsus);
                objPag.nombresus = rn.GetColumnType(txtEdit_nombresus.Text, EntSegUsuarios.Fields.nombresus);
                objPag.apellidosus = rn.GetColumnType(txtEdit_apellidosus.Text, EntSegUsuarios.Fields.apellidosus);
                //objPag.vigentesus = rn.GetColumnType(txtEdit_vigentesus.Text, EntSegUsuarios.Fields.vigentesus);
                objPag.fechavigentesus = rn.GetColumnType(txtEdit_fechavigentesus.Text, EntSegUsuarios.Fields.fechavigentesus);

                objPag.apitransaccionsus = CApi.Transaccion.MODIFICAR.ToString();
                objPag.usumodsus = miSesion.AppUsuario.loginsus;
                rn.Update(objPag);

                //Actualizamos el rol
                ArrayList arrColWhere = new ArrayList();
                arrColWhere.Add(EntSegUsuariosrestriccion.Fields.idsus.ToString());
                arrColWhere.Add(EntSegUsuariosrestriccion.Fields.rolactivosur.ToString());
                ArrayList arrValWhere = new ArrayList();
                arrValWhere.Add(objPag.idsus);
                arrValWhere.Add(1);

                EntSegUsuariosrestriccion objSur = rnSur.ObtenerObjeto(arrColWhere, arrValWhere);
                objSur.rolsro = int.Parse(ddlEditIdsro.SelectedValue.ToString());
                objSur.apitransaccionsur = CApi.Transaccion.MODIFICAR.ToString();
                objSur.usumodsur = miSesion.AppUsuario.loginsus;
                rnSur.Update(objSur);

                //Actualizamos los datos de la sesion actual
                if (miSesion.AppUsuario.idsus == objPag.idsus)
                {
                    miSesion.AppUsuario = objPag;
                    miSesion.AppRestriccion = objSur;

                    //Obtenemos el Rol
                    RnSegRoles rnSro = new RnSegRoles();
                    miSesion.AppRol = rnSro.ObtenerObjeto(objSur.rolsro);
                }

                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }
            if (bProcede)
                Response.Redirect(
                    Request.AppRelativeCurrentExecutionFilePath +
                    "?msg=Se ha modificado el registro satisfactoriamente");
        }

        protected void btnImprimir_OnClick(object sender, EventArgs e)
        {
            try
            {
                SessionHandler miSesion = new SessionHandler();
                string strTitulo = "Consultas: " + miSesion.AppPaciente.nombresppa + " " + miSesion.AppPaciente.appaternoppa;
                String strNombreSp = "spdcoselgrid";

                var arrNomParam = new ArrayList();
                arrNomParam.Add(EntPacPacientes.Fields.idppa.ToString());
                var arrValParam = new ArrayList();
                arrValParam.Add(miSesion.AppPaciente.idppa);

                var rn = new RnVista();
                var dtReporte = rn.ObtenerDatosProcAlm(strNombreSp, arrNomParam, arrValParam);

                foreach (var dataTableCol in dtReporte.Columns.Cast<DataColumn>().ToList())
                {
                    if (dataTableCol.ColumnName.ToUpper().Contains("idsus".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("apiestado".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("apitransaccion".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("usucre".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("feccre".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("usumod".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("fecmod".ToUpper()))
                        dtReporte.Columns.Remove(dataTableCol.ColumnName);
                }

                String strNombreReporte = "Reporte-" + DateTime.Now.ToString("yyyy-MM-dd HH-mm") + ".xlsx";
                string template = Server.MapPath("~/Templates/Reporte.xlsx");

                using (XLWorkbook wb = new XLWorkbook(template))
                {
                    wb.Worksheets.Worksheet(1).Cell(5, 1).Value = strTitulo;
                    wb.Worksheets.Worksheet(1).Cell(6, 1).Value = "Elaborado por: " + miSesion.AppUsuario.nombresus + " " + miSesion.AppUsuario.apellidosus;
                    wb.Worksheets.Worksheet(1).Cell(7, 1).Value = "Fecha: " + DateTime.Now.ToString(CParametros.ParFormatoFechaHora);

                    wb.Worksheets.Worksheet(1).Cell(9, 1).InsertTable(dtReporte);
                    wb.Worksheets.Worksheet(1).Table("Table1").ShowAutoFilter = true;
                    wb.Worksheets.Worksheet(1).Columns(2, 2 + dtReporte.Columns.Count).AdjustToContents();
                    wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    wb.Style.Font.Bold = true;

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename=\"" + strNombreReporte + "\"");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }
        }
    }
}