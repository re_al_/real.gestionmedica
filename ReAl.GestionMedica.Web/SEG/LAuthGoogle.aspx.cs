﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Auth.OAuth2.Flows;
using Google.Apis.Auth.OAuth2.Web;
using Google.Apis.Util.Store;
using ReAl.GestionMedica.Web.App_Helpers;

namespace ReAl.GestionMedica.Web.SEG
{
    public partial class LAuthGoogle : System.Web.UI.Page
    {
        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            //Google
            SessionHandler miSession = new SessionHandler();
            string strCredentialName = miSession.AppUsuario.loginsus;
            GoogleAuthorizationCodeFlow flow;
            using (var stream =
                new FileStream(Server.MapPath("~/google/") + "/" + "credentials-web.json", FileMode.Open, FileAccess.Read))
            {
                var credPath = Server.MapPath("~/google/") + "/" + strCredentialName + "_token.json";
                flow =
                    new GoogleAuthorizationCodeFlow(new GoogleAuthorizationCodeFlow.Initializer
                    {
                        ClientSecrets = GoogleClientSecrets.Load(stream).Secrets,
                        Scopes = CParamGoogleApi.Scopes,
                        DataStore = new FileDataStore(credPath, true)
                    });
            }

            var uri = Request.Url.ToString();
            var code = Request["code"];
            if (code != null)
            {
                var token = flow.ExchangeCodeForTokenAsync(strCredentialName, code,
                    uri.Substring(0, uri.IndexOf("?")), CancellationToken.None).Result;

                // Extract the right state.
                var oauthState = AuthWebUtility.ExtracRedirectFromState(
                    flow.DataStore, strCredentialName, Request["state"]).Result;
                Response.Redirect(oauthState);
            }
            else
            {
                var result = new AuthorizationCodeWebApp(flow, uri, uri).AuthorizeAsync(strCredentialName,
                    CancellationToken.None).Result;
                if (result.RedirectUri != null)
                {
                    // Redirect the user to the authorization server.
                    Response.Redirect(result.RedirectUri);
                }
                else
                {
                    // The data store contains the user credential, so the user has been already authenticated.
                    UserCredential credential = result.Credential;

                    var auth = new CGoogleApi();
                    miSession.GAppsCredential = credential;
                    miSession.GAppsUsuario = auth.ObtenerUsuario(credential);

                    Response.Redirect("~/SEG/LPerfil?msg=Se ha conectado su cuenta satisfactoriamente a Google");
                }
            }
        }

        #endregion Methods
    }
}