﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PrivateSite.Master" AutoEventWireup="true" CodeBehind="LCobrosPacientes.aspx.cs" Inherits="ReAl.GestionMedica.Web.GAS.LCobrosPacientes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StyleSection" runat="Server">
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/css/bootstrap-select.min.css") %>" rel="stylesheet" />
    <link href="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.css") %>" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <!-- Page Heading -->
    <header class="page-header page-header-dark bg-gradient-primary-to-secondary pb-10">
        <div class="container-fluid">
            <div class="page-header-content pt-4">
                <div class="row align-items-center justify-content-between">
                    <div class="col-auto mt-4">
                        <h1 class="page-header-title">
                            <div class="page-header-icon"><i data-feather="user"></i></div>
                            <asp:Literal ID="litTitulo" runat="server" Text="Registro de cobros paciente" />
                        </h1>
                    </div>
                    <%--<div class="col-12 col-xl-auto mt-4">Optional page header content</div>--%>
                </div>
            </div>
        </div>
    </header>
    <!-- Main page content-->
    <div class="container-fluid">
        <div class="card mt-n10">
            <div class="card-header">
                Registro de cobros paciente
            </div>
            <div class="card-body">
                <p>
                    <!-- Botones de Accion -->
                    <asp:LinkButton ID="btnAtras" runat="server" Text="<i class='fa fa-arrow-left'></i>" ToolTip="Atras"
                        CssClass="btn btn-secondary " class="btn btn-secondary" OnClick="btnAtras_OnClick" />
                    <asp:LinkButton ID="btnActualizar" runat="server" Text="<i class='fa fa-search'></i>"
                        ToolTip="Actualizar filtro"
                        CssClass="btn btn-secondary" class="btn btn-secondary" OnClick="btnActualizar_OnClick" />
                </p>
                <div class="mb-4"></div>
                <div class="text-sm">
                    <asp:GridView ID="dtgListado" runat="server" AutoGenerateColumns="False"
                        data-toggle="table" data-show-columns="true" data-pagination="true"
                        data-search="true" data-show-toggle="true" data-sortable="true"
                        data-page-size="25" data-pagination-v-align="both" data-show-export="true"
                        DataKeyNames="" OnRowCommand="dtgListado_OnRowCommand"
                        CssClass="table table-striped table-bordered table-hover">

                        <Columns>
                            <asp:BoundField ReadOnly="True" DataField="Fecha" HeaderText="Fecha" ShowHeader="false">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Doctor" HeaderText="Doctor" ShowHeader="false">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Nombre" HeaderText="Nombre Paciente" ShowHeader="false">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Apellidos" HeaderText="Apellidos Paciente" ShowHeader="false">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:BoundField ReadOnly="True" DataField="Monto" HeaderText="Monto" ShowHeader="false">
                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            </asp:BoundField>

                            <asp:TemplateField HeaderText="Acciones">
                                <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                <HeaderStyle></HeaderStyle>
                                <ItemTemplate>
                                    <asp:LinkButton ID="ImbModificar" CommandName="pagar" CommandArgument='<%# Bind("idgli") %>' runat="server"
                                        ToolTip="Registrar pago" OnClientClick="return confirm('¿Esta seguro que desea registrar el pago?');"
                                        CssClass="btn btn-success btn-sm" Text="<i class='fa fa-check'></i>" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptSection" runat="Server">
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/popper/popper.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/bootstrap-table.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-table/locale/bootstrap-table-es-MX.min.js") %>"></script>
    <script src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/bootstrap-select/js/bootstrap-select.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/moment/moment.min.js") %>"></script>
    <script type="text/javascript" src="<%= ResolveClientUrl("~/assets/sbadmin2/vendor/tempusdominus/tempusdominus-bootstrap-4.min.js") %>"></script>

    <script>
        $(document).ready(function () {
            $('select').selectpicker();
            $('[data-toggle="popover"]').popover();
            $('#datetimepicker1').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker2').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker3').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker7').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('#datetimepicker8').datetimepicker({
                useCurrent: false,
                format: 'DD/MM/YYYY'
            });
            $("#datetimepicker7").on("change.datetimepicker", function (e) {
                $('#datetimepicker8').datetimepicker('minDate', e.date);
            });
            $("#datetimepicker8").on("change.datetimepicker", function (e) {
                $('#datetimepicker7').datetimepicker('maxDate', e.date);
            });

        });

        function pageLoad() {
            $(function () {
                $('select').selectpicker();
                $('[data-toggle="popover"]').popover();
                $('#datetimepicker1').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#datetimepicker2').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#datetimepicker3').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#datetimepicker7').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('#datetimepicker8').datetimepicker({
                    format: 'DD/MM/YYYY',
                    useCurrent: false
                });
                $("#datetimepicker7").on("change.datetimepicker", function (e) {
                    $('#datetimepicker8').datetimepicker('minDate', e.date);
                });
                $("#datetimepicker8").on("change.datetimepicker", function (e) {
                    $('#datetimepicker7').datetimepicker('maxDate', e.date);
                });
            });
        }

        function cellStyleTipo(value, row, index) {
            return {
                //classes: classes[index / 2]
                css: {
                    "background-color": setColorNumber(value)
                }
            };

            return {};
        }

        function setColor(p) {
            if (p == "G") { return "rgb(255, 150, 150)"; }
            if (p == "I") { return "rgb(130, 200, 130)"; }
            return "rgb(255, 255, 255)";
        }

        function setColorNumber(p) {

            if (Number(p.replace('.', '').replace(',', '.')) < 0) { return "rgb(255, 150, 150)"; }
            if (Number(p.replace('.', '').replace(',', '.')) > 0) { return "rgb(130, 200, 130)"; }
            return "rgb(255, 255, 255)";
        }
    </script>
</asp:Content>