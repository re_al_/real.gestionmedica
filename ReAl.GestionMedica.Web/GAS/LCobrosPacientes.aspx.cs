﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Web.App_Helpers;
using ReAl.Utils;

namespace ReAl.GestionMedica.Web.GAS
{
    public partial class LCobrosPacientes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                SessionHandler miSesion = new SessionHandler();

                if (miSesion.ArrMenu != null)
                    if (!miSesion.ArrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath.Split('/').Last()))
                        Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));

                if (!Page.IsPostBack)
                {
                    CargarListado();
                }
                else
                {
                    CargarListado();
                    ControlHelper.CrearEstilosGrid(ref dtgListado);
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        public void CargarListado()
        {
            try
            {
                SessionHandler miSesion = new SessionHandler();
                String strNombreSp = "spgliselcobro";

                var rn = new RnVista();
                var dt = rn.ObtenerDatosProcAlm(strNombreSp);
                dtgListado.DataSource = dt;
                dtgListado.DataBind();
                ControlHelper.CrearEstilosGrid(ref dtgListado);
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        protected void btnAtras_OnClick(object sender, EventArgs e)
        {
            SessionHandler miSesion = new SessionHandler();
            Response.Redirect(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));
        }

        protected void dtgListado_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            String strRedireccion = "";
            try
            {
                if (e.CommandName.Equals("pagar"))
                {
                    SessionHandler miSesion = new SessionHandler();
                    var strId = e.CommandArgument.ToString();
                    var rn = new RnGasLibro();
                    EntGasLibro obj = rn.ObtenerObjeto(rn.GetColumnType(strId, EntGasLibro.Fields.idgli));

                    if (obj != null)
                    {
                        obj.apitransaccion = CApi.Transaccion.EJECUTAR.ToString();
                        obj.apiestado = CApi.Estado.EJECUTADO.ToString();
                        obj.usumod = miSesion.AppUsuario.loginsus;
                        rn.Update(obj);
                        strRedireccion = "~/GAS/LCobrosPacientes.aspx";
                    }
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (!string.IsNullOrEmpty(strRedireccion))
                Response.Redirect(strRedireccion);
        }

        protected void btnActualizar_OnClick(object sender, EventArgs e)
        {
            CargarListado();
        }
    }
}