﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using ReAl.GestionMedica.Class;
using ReAl.GestionMedica.Class.Entidades;
using ReAl.GestionMedica.Class.Modelo;
using ReAl.GestionMedica.PgConn;
using ReAl.GestionMedica.Web.App_Helpers;
using ReAl.Utils;

namespace ReAl.GestionMedica.Web.GAS
{
    public partial class LGastosIngresos : System.Web.UI.Page
    {
        #region Methods

        public void CargarListado()
        {
            try
            {
                if (string.IsNullOrEmpty(txtFecIni.Text.Trim())) txtFecIni.Text = DateTime.Now.ToString(CParametros.ParFormatoFecha);
                if (string.IsNullOrEmpty(txtFecFin.Text.Trim())) txtFecFin.Text = DateTime.Now.ToString(CParametros.ParFormatoFecha);

                SessionHandler miSesion = new SessionHandler();
                String strNombreSp = "spgliselgridweb";

                var arrNomParam = new ArrayList();
                arrNomParam.Add("fechaini");
                arrNomParam.Add("fechafin");
                var arrValParam = new ArrayList();
                arrValParam.Add(txtFecIni.Text);
                arrValParam.Add(txtFecFin.Text);

                var rn = new RnVista();
                var dt = rn.ObtenerDatosProcAlm(strNombreSp, arrNomParam, arrValParam);
                dtgListado.DataSource = dt;
                dtgListado.DataBind();
                ControlHelper.CrearEstilosGrid(ref dtgListado, true, false, true, false, "Monto", "cellStyleTipo");
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        protected void btnActualizar_OnClick(object sender, EventArgs e)
        {
            CargarListado();
        }

        protected void btnAtras_OnClick(object sender, EventArgs e)
        {
            SessionHandler miSesion = new SessionHandler();
            Response.Redirect(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));
        }

        protected void btnEditaGuardar_OnClick(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var miSesion = new SessionHandler();
                var rn = new RnGasLibro();
                EntGasLibro objPag = rn.ObtenerObjeto(rn.GetColumnType(hdnIdDatos.Value, EntGasLibro.Fields.idgli));

                objPag.nombregli = rn.GetColumnType(txtEdit_nombregli.Text, EntGasLibro.Fields.nombregli);
                objPag.descripciongli = rn.GetColumnType(txtEdit_descripciongli.Text, EntGasLibro.Fields.descripciongli);
                objPag.monto = rn.GetColumnType(txtEdit_montogli.Text, EntGasLibro.Fields.monto);
                objPag.idgca = rn.GetColumnType(cmbEditIdgca.SelectedValue, EntGasLibro.Fields.idgca);
                objPag.fechagli = rn.GetColumnType(dtpEditFechagli.Text, EntGasLibro.Fields.fechagli);

                objPag.apitransaccion = CApi.Transaccion.MODIFICAR.ToString();
                objPag.usumod = miSesion.AppUsuario.loginsus;

                rn.Update(objPag);
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }
            if (bProcede)
                Response.Redirect(
                    Request.AppRelativeCurrentExecutionFilePath +
                    "?msg=Se ha modificado el registro satisfactoriamente");
        }

        protected void btnImprimir_OnClick(object sender, EventArgs e)
        {
            try
            {
                SessionHandler miSesion = new SessionHandler();
                string strTitulo = "Libro Mayor";
                String strNombreSp = "spgliselgridweb";

                var arrNomParam = new ArrayList();
                arrNomParam.Add("fechaini");
                arrNomParam.Add("fechafin");
                var arrValParam = new ArrayList();
                arrValParam.Add(txtFecIni.Text);
                arrValParam.Add(txtFecFin.Text);

                var rn = new RnVista();
                var dtReporte = rn.ObtenerDatosProcAlm(strNombreSp, arrNomParam, arrValParam);

                foreach (var dataTableCol in dtReporte.Columns.Cast<DataColumn>().ToList())
                {
                    if (dataTableCol.ColumnName.ToUpper().Contains("idgli".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("apiestado".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("apitransaccion".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("usucre".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("feccre".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("usumod".ToUpper()) ||
                        dataTableCol.ColumnName.ToUpper().Contains("fecmod".ToUpper()))
                        dtReporte.Columns.Remove(dataTableCol.ColumnName);
                }

                String strNombreReporte = "Reporte-" + DateTime.Now.ToString("yyyy-MM-dd HH-mm") + ".xlsx";
                string template = Server.MapPath("~/Templates/Reporte.xlsx");

                using (XLWorkbook wb = new XLWorkbook(template))
                {
                    wb.Worksheets.Worksheet(1).Cell(5, 1).Value = strTitulo;
                    wb.Worksheets.Worksheet(1).Cell(6, 1).Value = "Elaborado por: " + miSesion.AppUsuario.nombresus + " " + miSesion.AppUsuario.apellidosus;
                    wb.Worksheets.Worksheet(1).Cell(7, 1).Value = "Fecha: " + DateTime.Now.ToString(CParametros.ParFormatoFechaHora);

                    wb.Worksheets.Worksheet(1).Cell(9, 1).InsertTable(dtReporte);
                    wb.Worksheets.Worksheet(1).Table("Table1").ShowAutoFilter = true;
                    wb.Worksheets.Worksheet(1).Columns(2, 2 + dtReporte.Columns.Count).AdjustToContents();
                    wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    wb.Style.Font.Bold = true;

                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    Response.AddHeader("content-disposition", "attachment;filename=\"" + strNombreReporte + "\"");

                    using (MemoryStream MyMemoryStream = new MemoryStream())
                    {
                        wb.SaveAs(MyMemoryStream);
                        MyMemoryStream.WriteTo(Response.OutputStream);
                        Response.Flush();
                        Response.End();
                    }
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }
        }

        protected void btnNewGuardar_OnClick(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var miSesion = new SessionHandler();
                var rn = new RnGasLibro();
                var objPag = new EntGasLibro();

                objPag.tipogli = "I";
                objPag.nombregli = rn.GetColumnType(txtNew_nombregli.Text, EntGasLibro.Fields.nombregli);
                objPag.descripciongli = rn.GetColumnType(txtNew_descripciongli.Text, EntGasLibro.Fields.descripciongli);
                objPag.fechagli = rn.GetColumnType(dtpNewFechagli.Text, EntGasLibro.Fields.fechagli);
                objPag.monto = rn.GetColumnType(txtNew_montogli.Text, EntGasLibro.Fields.monto);
                objPag.idgca = rn.GetColumnType(cmbNewIdgca.Text, EntGasLibro.Fields.idgca);
                objPag.apiestado = CApi.Estado.ELABORADO.ToString();
                objPag.apitransaccion = CApi.Transaccion.CREAR.ToString();

                objPag.usucre = miSesion.AppUsuario.loginsus;

                rn.Insert(objPag);
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (bProcede)
                Response.Redirect(
                    Request.AppRelativeCurrentExecutionFilePath +
                    "?msg=Se ha creado el registro satisfactoriamente");
        }

        protected void btnNewGuardarGas_OnClick(object sender, EventArgs e)
        {
            var bProcede = false;
            try
            {
                var miSesion = new SessionHandler();
                var rn = new RnGasLibro();
                var objPag = new EntGasLibro();

                objPag.tipogli = "G";
                objPag.nombregli = rn.GetColumnType(txtNew_nombregliGas.Text, EntGasLibro.Fields.nombregli);
                objPag.descripciongli = rn.GetColumnType(txtNew_descripciongliGas.Text, EntGasLibro.Fields.descripciongli);
                objPag.fechagli = rn.GetColumnType(dtpNewFechagliGas.Text, EntGasLibro.Fields.fechagli);
                objPag.monto = rn.GetColumnType(txtNew_montogliGas.Text, EntGasLibro.Fields.monto);
                objPag.idgca = rn.GetColumnType(cmbNewIdgcaGas.Text, EntGasLibro.Fields.idgca);
                objPag.apiestado = CApi.Estado.ELABORADO.ToString();
                objPag.apitransaccion = CApi.Transaccion.CREAR.ToString();

                objPag.usucre = miSesion.AppUsuario.loginsus;

                rn.Insert(objPag);
                bProcede = true;
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (bProcede)
                Response.Redirect(
                    Request.AppRelativeCurrentExecutionFilePath +
                    "?msg=Se ha creado el registro satisfactoriamente");
        }

        protected void btnNuevo_OnClick(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "newModal", "$('#newModal').modal('show');", true);
        }

        protected void btnNuevoGas_OnClick(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "newModal", "$('#newModalGas').modal('show');", true);
        }

        protected void dtgListado_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            String strRedireccion = "";
            try
            {
                if (e.CommandName.Equals("detalles"))
                {
                    string strId = e.CommandArgument.ToString();

                    //Filtramos el Dataset
                    DataView dv = ((DataTable)dtgListado.DataSource).DefaultView;
                    dv.RowFilter = EntGasLibro.Fields.idgli + " = " + strId;

                    DataTable detailTable = dv.ToTable();

                    dtgDetalles.DataSource = detailTable;
                    dtgDetalles.DataBind();
                    dtgDetalles.HeaderRow.TableSection = TableRowSection.TableHeader;

                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "myModal", "$('#currentdetail').appendTo('body').modal('show');", true);
                }
                else if (e.CommandName.Equals("modificar"))
                {
                    var strId = e.CommandArgument.ToString();
                    var rn = new RnGasLibro();
                    EntGasLibro objPag = rn.ObtenerObjeto(rn.GetColumnType(strId, EntGasLibro.Fields.idgli));
                    hdnIdDatos.Value = objPag.idgli.ToString();

                    txtEdit_nombregli.Text = objPag.nombregli;
                    txtEdit_descripciongli.Text = objPag.descripciongli;
                    cmbEditIdgca.SelectedValue = objPag.idgca.ToString();
                    dtpEditFechagli.Text = objPag.fechagli.ToString();
                    txtEdit_montogli.Text = objPag.monto.ToString();
                    lblResult.Visible = false;

                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(@"<script type='text/javascript'>");
                    sb.Append("$('#editModal').modal('show');");
                    sb.Append(@"</script>");
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "EditModalScript", sb.ToString(), false);
                }
                else if (e.CommandName.Equals("eliminar"))
                {
                    string strId = e.CommandArgument.ToString();

                    var rn = new RnGasLibro();
                    EntGasLibro obj = rn.ObtenerObjeto(rn.GetColumnType(strId, EntGasLibro.Fields.idgli));

                    if (obj != null)
                    {
                        rn.Delete(obj);
                        strRedireccion = "~/PAC/LRecetas";
                    }
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp); ;
            }

            if (!string.IsNullOrEmpty(strRedireccion))
                Response.Redirect(strRedireccion);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Validamos el salto directo
                SessionHandler miSesion = new SessionHandler();

                if (miSesion.ArrMenu != null)
                    if (!miSesion.ArrMenu.Contains(Request.AppRelativeCurrentExecutionFilePath.Split('/').Last()))
                        Server.Transfer(SiteHelper.GetMainPage(miSesion.AppRol, miSesion.ArrMenu));

                if (!Page.IsPostBack)
                {
                    txtFecIni.Text = DateTime.Now.ToString(CParametros.ParFormatoFecha);
                    txtFecFin.Text = DateTime.Now.ToString(CParametros.ParFormatoFecha);
                    dtpNewFechagli.Text = DateTime.Now.Date.ToString(CParametros.ParFormatoFecha);
                    dtpNewFechagliGas.Text = DateTime.Now.Date.ToString(CParametros.ParFormatoFecha);

                    CargarCmbidgca();
                    CargarListado();
                }
                else
                {
                    CargarListado();
                    ControlHelper.CrearEstilosGrid(ref dtgListado);
                }
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        private void CargarCmbidgca()
        {
            try
            {
                var rn = new RnGasCategoria();
                var dt = rn.ObtenerDataTable();
                cmbEditIdgca.DataValueField = EntGasCategoria.Fields.idgca.ToString();
                cmbEditIdgca.DataTextField = EntGasCategoria.Fields.descripciongca.ToString();
                cmbEditIdgca.DataSource = dt;
                cmbEditIdgca.DataBind();

                cmbNewIdgca.DataValueField = EntGasCategoria.Fields.idgca.ToString();
                cmbNewIdgca.DataTextField = EntGasCategoria.Fields.descripciongca.ToString();
                cmbNewIdgca.DataSource = dt;
                cmbNewIdgca.DataBind();

                cmbNewIdgcaGas.DataValueField = EntGasCategoria.Fields.idgca.ToString();
                cmbNewIdgcaGas.DataTextField = EntGasCategoria.Fields.descripciongca.ToString();
                cmbNewIdgcaGas.DataSource = dt;
                cmbNewIdgcaGas.DataBind();
            }
            catch (Exception exp)
            {
                ((PrivateSite)Master).MostrarPopUp(this, exp);
            }
        }

        #endregion Methods
    }
}