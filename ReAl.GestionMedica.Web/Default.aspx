﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ReAl.GestionMedica.Web._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .bg-login-image {
            background: url("/assets/sbadmin2/img/login.jpg");
            background-position: center;
            background-size: cover;
        }
    </style>

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us">
        <div class="container">

            <div class="row">
                <div class="col-lg-4 d-flex align-items-stretch">
                    <div class="content">
                        <h3>¿Porqué escogernos?</h3>
                        <p>
                            Destacamos por nuestra capacidad de innovación y desarrollo en el ámbito médico, rigiendonos por las Guías de Buenas prácticas clínicas y centrando nuestra actividad asistencial en procurar el bienestar de nuestros pacientes bajo los principios bioéticos fundamentales de autonomía, beneficiencia, no maleficiencia y justicia. Velando por la seguridad de los pacientes y del personal del consultorio, cumpliendo en forma estricta con los protocolos de bioseguridad contra el COVID-19
                        </p>
                    </div>
                </div>
                <div class="col-lg-8 d-flex align-items-stretch">
                    <div class="icon-boxes d-flex flex-column justify-content-center">
                        <div class="row">
                            <div class="col-xl-3 d-flex align-items-stretch">
                                <div class="icon-box mt-4 mt-xl-0">
                                    <i class="bx bx-receipt"></i>
                                    <h4>Calidad</h4>
                                    <p>
                                        Nuestro trabajo consiste en ofrecer la más alta calidad en los tratamientos médicos, para lo cual el factor humano es crítico.
                                    </p>
                                </div>
                            </div>
                            <div class="col-xl-3 d-flex align-items-stretch">
                                <div class="icon-box mt-4 mt-xl-0">
                                    <i class="bx bx-cube-alt"></i>
                                    <h4>Cuidado</h4>
                                    <p>Estamos orientados al cuidado de la salud entendida como prevención, diagnóstico, tratamiento y seguimiento del paciente.</p>
                                </div>
                            </div>
                            <div class="col-xl-3 d-flex align-items-stretch">
                                <div class="icon-box mt-4 mt-xl-0">
                                    <i class="bx bx-time"></i>
                                    <h4>Valoramos tu tiempo</h4>
                                    <p>Reduce los tiempos de espera, agendando tu cita sin tener que esperar días o semanas. </p>
                                </div>
                            </div>
                            <div class="col-xl-3 d-flex align-items-stretch">
                                <div class="icon-box mt-4 mt-xl-0">
                                    <i class="bx bx-camera-home"></i>
                                    <h4>Velamos por tu seguridad</h4>
                                    <p>Somos pioneros en Video Consultas y Tele Medicina.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End .content-->
                </div>
            </div>
        </div>
    </section>
    <!-- End Why Us Section -->

    <!-- ======= Counts Section =======
    <section id="counts" class="counts">
        <div class="container">

            <div class="row">

                <div class="col-lg-6 col-md-6">
                    <div class="count-box">
                        <i class="icofont-doctor-alt"></i>
                        <span data-toggle="counter-up">85</span>
                        <p>Pacientes</p>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 mt-5 mt-md-0">
                    <div class="count-box">
                        <i class="icofont-patient-file"></i>
                        <span data-toggle="counter-up">18</span>
                        <p>Citas</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    End Counts Section -->

    <!-- ======= Doctors Section ======= -->
    <section id="doctors" class="doctors">
        <div class="container">

            <div class="section-title">
                <h2>Doctor</h2>
                <p>&nbsp;</p>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="member d-flex align-items-start">
                        <div class="pic">
                            <img src="<%= ResolveClientUrl("~/assets/medilab/img/doctors/doctors-1.jpg") %>" class="img-fluid" alt="">
                        </div>
                        <div class="member-info">
                            <h4>Bernardo De Ferari</h4>
                            <span>Neurocirujano</span>
                            <p>Neurocirugía, Cirugía Cerebral, Cirugía de Columna Vertebral, Cirugía de Nervios Periféricos, NeuroEndoscopía, Neurología. </p>
                            <div class="social">
                                <%--<a href=""><i class="ri-twitter-fill"></i></a>
                                <a href=""><i class="ri-facebook-fill"></i></a>
                                <a href=""><i class="ri-instagram-fill"></i></a>--%>
                                <a href="https://www.linkedin.com/in/bernardo-de-ferari-amboni-0740303b/" target="_blank"><i class="ri-linkedin-box-fill"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Doctors Section -->

    <!-- ======= Frequently Asked Questions Section ======= -->
    <section id="faq" class="faq section-bg">
        <div class="container">

            <div class="section-title">
                <h2>Preguntas frecuentes</h2>
            </div>

            <div class="faq-list">
                <ul>
                    <li data-aos="fade-up">
                        <i class="bx bx-help-circle icon-help"></i><a data-toggle="collapse" class="collapse" href="#faq-list-1">¿Cómo puedo agendar una cita? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div id="faq-list-1" class="collapse show" data-parent=".faq-list">
                            <p>
                                Puede hacerlo a través de una lllamada telefónica a nuestros números de contacto, a través de la página web o incluso, a través de whatsapp.
                            </p>
                        </div>
                    </li>

                    <li data-aos="fade-up" data-aos-delay="100">
                        <i class="bx bx-help-circle icon-help"></i><a data-toggle="collapse" href="#faq-list-2" class="collapsed">¿Puedo pedir la cita para otra persona? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div id="faq-list-2" class="collapse" data-parent=".faq-list">
                            <p>
                                Sí, es posible. Solo es necesario que conozcas la información personal del paciente que es requerida para pedir la cita. Por ejemplo, nombre, documento de identidad, fecha de nacimiento, número de celular y género
                            </p>
                        </div>
                    </li>

                    <li data-aos="fade-up" data-aos-delay="200">
                        <i class="bx bx-help-circle icon-help"></i><a data-toggle="collapse" href="#faq-list-3" class="collapsed">¿Qué llevar a una primera consulta? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div id="faq-list-3" class="collapse" data-parent=".faq-list">
                            <p>
                                Se sugiere llevar todos los antecedentes médicos, exámenes y/o evaluaciones realizadas.
                            </p>
                        </div>
                    </li>

                    <li data-aos="fade-up" data-aos-delay="300">
                        <i class="bx bx-help-circle icon-help"></i><a data-toggle="collapse" href="#faq-list-4" class="collapsed">¿Cómo es una consulta con el neurólogo? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div id="faq-list-4" class="collapse" data-parent=".faq-list">
                            <p>
                                El especialista realiza una historia detallada sobre las molestias del paciente. En las enfermedades neurológicas es muy importante precisar los síntomas y la forma cómo se fueron presentando. Es útil llevar anotado todos los medicamentos que toma y los exámenes realizados previamente.
                            </p>
                        </div>
                    </li>

                    <li data-aos="fade-up" data-aos-delay="400">
                        <i class="bx bx-help-circle icon-help"></i><a data-toggle="collapse" href="#faq-list-5" class="collapsed">¿Qué es un examen neurológico?<i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                        <div id="faq-list-5" class="collapse" data-parent=".faq-list">
                            <p>
                                Es una evaluación de clínica de las diversas funciones del sistema nervioso central y periférico, desde el estado de la conciencia, lenguaje, funciones cognoscitivas, fuerza, sensibilidad, equilibrio, reflejos. Estas valoraciones varían de un caso a otro poniendo énfasis en aquellas manifestaciones físicas que sean relevantes para una enfermedad específica.
                            </p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!-- End Frequently Asked Questions Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container">

            <div class="section-title">
                <h2>Contacto</h2>
                <p>
                    Puede visitarnos en nuestro consultorio o llamar a nuestro número de contacto para agendar una cita
                </p>
            </div>
        </div>

        <div>
            <iframe style="border: 0; width: 100%; height: 350px;"
                src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7649.321766540182!2d-68.07892602186999!3d-16.543209266905947!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcb352071fdd35f54!2sEdificio%20Goya.%20Consultorio%20Dr.%20De%20Ferrari!5e0!3m2!1ses!2sbo!4v1614524685491!5m2!1ses!2sbo"
                frameborder="0" allowfullscreen></iframe>
        </div>

        <div class="container">
            <div class="row mt-5">
                <div class="col-lg-4">
                    <div class="info">
                        <div class="address">
                            <i class="icofont-google-map"></i>
                            <h4>Dirección:</h4>
                            <p>
                                Calle 21 No. 8390, Edificio Goya, Of.3. Calacoto
                                <br />
                                La Paz - Bolivia
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="info">
                        <div class="phone">
                            <i class="icofont-phone"></i>
                            <h4>Teléfonos:</h4>
                            <p>2797411 - 2775127 - 77748142 - 77298671</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="info">
                        <div class="phone">
                            <i class="icofont-whatsapp"></i>
                            <h4>Whatsapp:</h4>
                            <p>77748142</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Contact Section -->
</asp:Content>