ReAl Gestion Medica
=========

ReAl Gestion Medica es una aplicación de Gestión Médica realizado en CSharp.

  - Organiza la agenda de Citas
  - Permite exportar citas a archivos .iCal
  - Lleva el Historial Médico de cada paciente
  - Lleva un Histórico de recetas, informes médicos, consultas y archivos multimedia por cada Paciente
  - Puede realizar el Cobro de Consultas, y se tiene un módulo pequeño de Caja Chica
  - Restaura Backups de la Base de Datos a través de una herramienta con interfaz gráfica
  - Soporte para reporte de Issues y Sugerencias a través de BitBucket y Trello
  - Soporte para almacenar los archivos multimedia en [Google Drive]

Para obtener una copia del instalador de la Ultima versión, visitar el área de [descargas] [1]:


Version
----

  - 2.0

PostgreSql Version
----

  - PostgreSQL 9.4, compiled by Visual C++ build 1600, 32-bit


Configuracion de la BD (antes de restaurar el Backup)
----

~~~sql
  - CREATE ROLE administrador VALID UNTIL 'infinity';;
  - CREATE ROLE doctor VALID UNTIL 'infinity';
  - CREATE ROLE asistente VALID UNTIL 'infinity';
  - CREATE ROLE secretaria VALID UNTIL 'infinity';
  - CREATE ROLE admin LOGIN PASSWORD '18C6D818AE35A3E8279B5330EDA01498' SUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
~~~

Trello Board
----

  - [GestionMedica Board]
  
Reporistorio BitBucket
----

  - [GestionMedica Source]
  

Equipo de Desarrollo
-----------

ReAl Gestion Medica fue desarrollado por:

Ing. Reynaldo Alonzo Vera Arias 

  - [Pagina Web]
  - [Correo Electrónico]
  - [Twiter]



[1]:https://bitbucket.org/re_al_/gestionmedica/downloads
[Pagina Web]:http://real7.somee.com/
[Correo Electrónico]:mailto:7.re.al.7@gmail.com?Subject=INTEGRATEGestionMedica
[Twiter]:https://twitter.com/re_al_
[GestionMedica Board]:https://trello.com/b/HV7w6La7/gestionmedica
[GestionMedica Source]:https://bitbucket.org/re_al_/gestionmedica
[Google Drive]:https://drive.google.com/folderview?id=0B03JXoL8SnrMMzNuUUFRbVVPT2c&usp=sharing
